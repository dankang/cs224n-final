Namárie - Altariello nainie Lóriendesse
Ai! Laurie lantar lassi súrinen,
Yéni únótime ve rámar aldaron!
Yéni ve linte yuldar avánier
Mi oromardi lisse-miruvóreva
Andúne pella, Vardo tellumar
Nu luini yassen tintilar i eleni ómaryo airetári-lírinen.
Sí man i yulma nin enquantuva?
An sí Tintalle Varda oiolosseo
Ve fanyar máryat elentari ortane
Ar ilye tier unduláve lumbule;
Ar Sindanóriello caita mornie
i falmalinnar imbe met ar hísie
Untúpa calaciryo miri oiale.
Sí vanwa ná, rómello vanwa, Valimar!
Namárie! Nai hiruvalye Valimar,
Nai elye hiruva. Namárie!
Man cenuva fána cirya
métima hrestallo círa,
i fairi néce
ringa súmaryasse
ve maiwi yaimie?
Man tiruva fána cirya,
wilwarin wilwa,
ear-celumessen
rámainen elvie
ear falastala,
winga hlápula
rámar sisílala,
cále fifírula?
Man hlaruva rávea súre
ve tauri lillassie,
ninqui carcar yarra
isilme ilcalasse,
isilme pícalasse,
isilme lantalasse
ve loicolícuma;
raumo nurrua,
undume rúma?
Man cenuva lumbor ahosta
Menel acúna
ruxal’ ambonnar,
ear amortala,
undume hácala,
enwina lúme
elenillor pella
talta-taltala
atalantea mindonnar?
Man tiruva rácina cirya
ondolisse morne
nu fanyare rúcina,
anar púrea tihta
axor ilcalannar
métim’ auresse?
Man cenuva métim’ andúne?
Eldar ataformaiti,
epetai i hyarma ú ten ulca símaryassen.
úsie, an cé mo querne cendele númenna, ve senya,
i hyarma tentane Melcorello,
ar cé mo formenna tentanes Amanna.
Átaremma i ea han ea,
na aire esselya,
aranielya na tuluva,
na care indómelya
cemende tambe Erumande.
Ámen anta síra ilaurea massamma,
ar ámen apsene úcaremmar
sív' emme apsenet tien i úcarer emmen.
Álame tulya úsahtienna
mal áme etelehta ulcullo.
Násie.
Aia María quanta Eruanno
i Héru aselye
aistana elye imíca nissi
ar aistana i yáve mónalyo Yésus
Aire María Eruo ontaril
á hyame rámen úcarindor
sí ar lúmesse ya firuvamme
Heru órava omesse.
A Hrísto órava omesse.
Atar meneldea Eru órava omesse.
A Eruion Mardorunando Eru órava omesse.
A Aina Faire Eru órava omesse.
A Aina Neldie Eru Er órava omesse.
A Aina Maria arca atarme.
Aina Eruontarie arca atarme.
Aina Wende mi Wenderon arca atarme.
Amille Hristo arca atarme.
Amille Eruva lisseo arca atarme.
Ortírielyanna rucimme, Aina Eruontari.
Álalye nattira arcandemmar sangiemmassen,
ono alye eterúna me illume ilya raxellor,
alcarin Vénde ar manaquenta.
Namárie, Ríanna vanima, Heriméla!
Antanelye men melme ar alasse,
ar renuvammet oiale.
Namárie, Ríanna vanima, Ardalóte!
Coacalinalya firne ve lícuma súrinen,
nó melmemma len úva fire indommassen.
Namárie, Ríanna vanima, Indotári!
Sí wila Númenna rámainen laurie,
ar nai fealya seruva oialmaresse.
Hríveresse
Et marinyallo mallenna
vantan hríveresse helca,
nu fanyare fuinehiswa,
lumboinen Naira nurtaina.
Hláranye ringa Formessúre,
asúy' aldassen úlassie,
alussa olbalisse norne,
alamya ve Nuru-nainie.
Formessúre-yalme quéla,
ar Númello holtan hwesta
nísima asúya ninna,
ar nainie ahya lírinna.
Cénan tuilindo awile
Hyarmello úrima súre,
nu rámaryat circa-cante,
alir' aldannar úlassie.
Autar i lumbor, ar Naira
cénan anúta Númenna,
et Rómello Tilion orta,
ar undómess' elen síla.
Ar lómelinde-lírinen,
entúlan yanna ettullen,
nu menel elentintaina,
hríveo lómesse sina.
Roccalas, linda lóte nórelyo,
anvanya yelde Roccoliéva,
le calina ve Naira ilwesse;
le rín' anda laurea loxenen,
caltala ve i calime alcar,
Roccalas aranel turmawende.
Elye lantane melmesse sonen;
merilye melmerya, ness' aranel.
Arwen ea óress' Elessarwa;
náro veaner ar canya ohtar;
melilyes nan umiro mele le,
Utúlie'n i móre; autante
mahtien ohtasse hair' nóressen,
ar le hehtanente i maresse:
merintel tirie nissi, híni
i artassen mí táre oronti,
Merilye hire metta nyérelyo;
essenen Haldatir sí lelyalye
muilesse ve i sanya rocconer;
mí hiswe hendu perino cenne
quén ú estelo i mere fire
Nu qualin roccorya cait' i aran
i né ve atar len ar tornelyan;
arwa macilo matse yétalye
rúcim' ulundo acole caure;
cuina nér úva pusta Loicoher
Roccalas, umilye nér, nálye nís;
náro pold', úmea, morn' ar alta,
ture or caure ar Sauron or so;
ortanelye macil tárienna,
rierya lantane, alantiéro,
Mernelye fire ar harya alcar;
mahtanelye Heru Úlairion,
ar náro qualin nan sí caitalye
ar' aranelya, lá cenilyéro;
umilye hlare telde quettaryar,
Nálye laiw' ar nyérea cuilenen;
linyenwa nís quet' enwina nóle:
i mát i aranwa envinyatar.
Roccalas, collente len er cuile,
nan lá alasse ar ea-íre,
I melme arandurwa hirnelye;
vantanéro ar quentéro yo le
imb' aldar ar lóti mareryasse;
ar quentéro lenna meliro le;
sí nálye envinyanta melmenen
Sí avalye ture ve i tári;
nauvalye envinyatare ve so,
lá turmawende ar lá aranel,
meluvalye ilqua cuin' ar vanya;
ve meluvárol, meluvalye so,
Roccalas indis envinyatare.
Eresse Tol-Eresseasse,
Lillassea lótesse pella,
Ar esce lisceo, ar lasse
I hessa hlapula menello.
Arinya losse or lord 'aire
Fifíruva wilyasse hiswa,
Ar tiruvalye olor aire
Enyalien i vanwa iswa.
Ar erumesse hiruvalye
I metya harma sa haryalye:
I hesin yenion, alasse -
Eresse Tol-Eresseasse.
I lúme utúlie,
A tirno Silmarillion,
Tienna narwa
No rocco néca,
Yan i esse ná Qualme, -
Wilie haiyave
Númenna Wisto,
Ardallo melima.
Vainolelya unqua
Ar macilya rusta
Loráva nenessen
I caituvar nalle.
Ve laurea anna,
Hroalya racina
Antalye nóren
Sa varyane li.
Ná carna nilmonen
Tyarda avatyarima,
An nar cotumor qualini,
Naina, Valariande,
An i calwo ná firin,
Ar elenu vinye
Úsiluvar eldain.
Nán i Raice na cuine.
