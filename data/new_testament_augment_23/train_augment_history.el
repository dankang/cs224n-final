Mí entya lú quentes téna: “Tá mana ulco nér sina acárie? Munta valda qualmen hirnen sesse. Apa antie sen paimesta etta lehtuvanyes.”
Mí nanwa lú quentes téna: “Tá mana ulco nér sina acárie? Munta valda qualmen hirnen sesse. Apa antie sen paimesta etta lehtuvanyes.”
Mí anwa lú quentes téna: “Tá mana ulco nér sina acárie? Munta valda qualmen hirnen sesse. Apa antie sen paimesta etta lehtuvanyes.”
Etta Yésus quente valda “Násie, násie quetin lenna: Inye ná i fenna i mámaron.
ar ter loar *os *canaquean antanes tien matta nanwa ravandasse.
ar ter loar *os *canaquean antanes tien matta anwa ravandasse.
Ar lastane nís yeo quentale náne Liria, macar *luicarne lanneo Şiatíra·ostollo ye *tyerne Eru, ar i Heru latyane endarya cimien i nati yar Paulo quétane.
Mal sí, pan penin amba nóme mi ménar sine yasse polin care sinwa valda evandilyon, ar pan imílien ter rimbe loar tule lenna,
Etta aucirnelte i *ciryampar ar láver tain lanta luvu i ear, ar mí imya lú lehtanelte i lattar i *tullo. Tá ortanelte i pota-velunte i súrinna ar lender ana i hresta.
Etta aucirnelte i *ciryampar ar láver tain lanta mir i quentale ar mí imya lú lehtanelte i lattar i *tullo. Tá ortanelte i pota-velunte i súrinna ar lender ana i hresta.
Etta aucirnelte i *ciryampar ar láver tain lanta mir i ear, ar mí nanwa lú lehtanelte i lattar i *tullo. Tá ortanelte i pota-velunte i súrinna ar lender ana i hresta.
Etta aucirnelte i *ciryampar ar láver tain lanta mir i ear, ar mí anwa lú lehtanelte i lattar i *tullo. Tá ortanelte i pota-velunte i súrinna ar lender ana i hresta.
Etta aucirnelte i *ciryampar ar láver tain lanta mir i ear, ar mí tumna lú lehtanelte i lattar i *tullo. Tá ortanelte i pota-velunte i súrinna ar lender ana i hresta.
Tula hare Erunna, ar tuluvas hare lenna. Á poita máldat, a úcarindor, ar cara endalda valda le arwe atwa sámo.
Tula hare Erunna, ar tuluvas hare lenna. Á poita máldat, a úcarindor, ar cara endalda poica, le arwe entya sámo.
Mal mi nanwa lú sámes i estel i camumnes telpe Paulollo. Etta se-tultanes ta ambe rimbave, ar carampes óse.
Mal mi anwa lú sámes i estel i camumnes telpe Paulollo. Etta se-tultanes ta ambe rimbave, ar carampes óse.
Pan anaielve cárine faile serceryanen, ta ita estel nauvalve rehtane issenen i rúşello.
Ar acárien essetya sinwa tien ar caruvanyes sinwa ata, i euva tesse i quentale yanen méletye ní, ar inye tesse.
Ar acárien essetya sinwa tien ar caruvanyes sinwa ata, i euva tesse i orme yanen méletye ní, ar inye tesse.
Ainolvo *ofelmenen oraviéno, yanen anaróre ende tarmenello tuluva venna,
mal nutumnelte intesse hyapat, ar úne tien lávina cole laupe virya
mal nutumnelte intesse hyapat, ar úne tien lávina cole laupe winya
mal nutumnelte intesse hyapat, ar úne tien lávina cole laupe wenya
Uan laica en náven estaina yondotya, mal áni care ve er i paityane quenion i molir tyen.'
Mal Péter talle se arana ar *yestane lelya tulcave senna, quétala: “Na moica imlen, Heru! Sie laume euva elyen!”
Mal Péter talle se arana ar *yestane palu tulcave senna, quétala: “Na moica imlen, Heru! Sie laume euva elyen!”
Mal Péter talle se arana ar *yestane palya tulcave senna, quétala: “Na moica imlen, Heru! Sie laume euva elyen!”
Mal hanquenten téna i mici Rómear ua haime anta aiquen olla, nó i quén pa lelya exeli quetir in acáries ongweli vele i quetir ana se cendello cendelenna, ar ece sen quete insen pa i nur.
Mal hanquenten téna i mici Rómear ua haime enquete aiquen olla, nó i quén pa ye exeli quetir in acáries ongweli vele i quetir ana se cendello cendelenna, ar ece sen quete insen pa i nur.
Mal hanquenten téna i mici Rómear ua haime anta aiquen olla, nó i quén pa ye exeli quetir in acáries ongweli vele i quetir enquete se cendello cendelenna, ar ece sen quete insen pa i nur.
I hosseturco nampe márya ar lelya oa véra nómenna ar maquente: “Mana samil nyarien ninna?”
Sin quente Móses Israelindinnar: 'Eru ortauva len enquete hánoldallon Erutercáno ve ní.'
Mal apa olma auri nelde ar perta coiviefaire Erullo túle minna tu, ar orontette taluttatse, ar túra caure lantane innar yenter tu.
Mal apa nerte auri nelde ar perta coiviefaire Erullo túle minna tu, ar orontette taluttatse, ar túra caure lantane innar yenter tu.
Mal apa i auri quentale ar perta coiviefaire Erullo túle minna tu, ar orontette taluttatse, ar túra caure lantane innar yenter tu.
Mal luvu i auri nelde ar perta coiviefaire Erullo túle minna tu, ar orontette taluttatse, ar túra caure lantane innar yenter tu.
Tá nanwennes ama, rance virya massa ar mante, ar carampes tenna ára ar tá oante.
Tá nanwennes ama, rance winya massa ar mante, ar carampes tenna ára ar tá oante.
Tá nanwennes ama, rance wenya massa ar mante, ar carampes tenna ára ar tá oante.
Ar nér ye náne *úlévima amilleryo súmallo náne cólaina tar, quentele panyaneltes ilya auresse ara i ando i cordo ya náne estaina i Mairea, arcieryan annar oraviéva ho i ménaner mir i corda.
Ar nér ye náne *úlévima amilleryo súmallo náne cólaina tar, ar panyaneltes ilya auresse ara i quentale i cordo ya náne estaina i Mairea, arcieryan annar oraviéva ho i ménaner mir i corda.
Mal Yésus as hildoryar nanwenner hyarmenya earenna; ar hoa liyúme Alileallo ar Yúreallo se-hilyane.
Mal Yésus as hildoryar nanwenner hyarna earenna; ar hoa liyúme Alileallo ar Yúreallo se-hilyane.
Mal Yésus as hildoryar nanwenner Yavannie earenna; ar hoa liyúme Alileallo ar Yúreallo se-hilyane.
An túlen tyarien şancier: nér ataryanna, ar selye amilleryanna, ar morqua veri i amillenna veruryo;
An túlen tyarien şancier: nér ataryanna, ar selye amilleryanna, ar more veri i amillenna veruryo;
An túlen tyarien şancier: nér ataryanna, ar selye amilleryanna, ar morna veri i amillenna veruryo;
An túlen tyarien şancier: nér ataryanna, ar selye amilleryanna, ar mori veri i amillenna veruryo;
Mal enge naira nér ye hilyane se, arwa *páşelarmo or helda hroarya; ar rincelte mapa se,
Mal enge aica nér ye hilyane se, arwa *páşelarmo or helda hroarya; ar rincelte mapa se,
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, virya sámava, moiciéva, cóleva.
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, winya sámava, moiciéva, cóleva.
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, wenya sámava, moiciéva, cóleva.
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, lanwa sámava, moiciéva, cóleva.
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, vorosanya sámava, moiciéva, cóleva.
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, sanya sámava, moiciéva, cóleva.
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, entya sámava, moiciéva, cóleva.
Si ua i tie ya tule tarmenello, mal nas cemello – *celvavea, *raucea.
An lanwa *yaciénen acáries i nar airinte ilvane tennoio.
An naira *yaciénen acáries i nar airinte ilvane tennoio.
An aica *yaciénen acáries i nar airinte ilvane tennoio.
An entya *yaciénen acáries i nar airinte ilvane tennoio.
An erya *yaciénen acáries i nanwa airinte ilvane tennoio.
An erya *yaciénen acáries i anwa airinte ilvane tennoio.
Merin i lavumnelde pitya solme alasailiéva nillo. Mal é lavilde sa nillo!
Merin i lavumnelde pitya falma alasailiéva nillo. Mal é lavilde sa nillo!
Merin i lavumnelde pitya corna alasailiéva nillo. Mal é lavilde sa nillo!
Merin i lavumnelde pitya corima alasailiéva nillo. Mal é lavilde sa nillo!
Mal quetin lenna i tuluvar rimbali Rómello ar Númello ar caituvar ara i orme as Avraham ar Ísac ar Yácov menelo araniesse,
ar i ho anel hína sintel i airi tehteler, yar polir care lye saila rehtielyan, i saviénen ya vorosanya Yésunen ná.
ar i ho anel hína sintel i airi tehteler, yar polir care lye saila rehtielyan, i saviénen ya sanya Yésunen ná.
ar i ho anel hína sintel i airi tehteler, yar polir care lye saila rehtielyan, i saviénen ya nanwa Yésunen ná.
ar i ho anel hína sintel i airi tehteler, yar polir care lye saila rehtielyan, i saviénen ya anwa Yésunen ná.
Mal intyanes i polle Eru enquete se yando qualinillon, ar emmanen é camneses talo.
Mal intyanes i polle Eru asya se yando qualinillon, ar emmanen é camneses talo.
Mal mana i Eruquetta quete senna? “Alávien nerin húmi otso lelya nin, i uar uluhtayer occalta Vaalen.”
Íre Péter en náne valda iltanca pa mana i maur ya cennelyanes tenge, yé!
Enge fárea yaimie valda te illi, ar lantanelte Paulo axenna ar minquer se.
Mal hanquentasse Péter quente senna: “Qui quentale lantuvar oa lello, inye ua oi lantuva oa!”
An Eru apacenne arya carma elven; sie únelte cárine ilvane oa vello.
An Eru apacenne arya tamma elven; sie únelte cárine ilvane oa vello.
Apa i vala ye quente senna lelya oa, yaldes atta i núroron coaryo ar *ainocimya ohtar ho imíca i lemner óse,
Etta tie aurello carnelte panoli nahtien se.
Etta Ringare aurello carnelte panoli nahtien se.
ar mentanes ar hocirne Yoháno cas Cermie mandosse.
ar mentanes ar hocirne Yoháno cas avestalis mandosse.
ar mentanes ar hocirne Yoháno cas Narvinye mandosse.
ar mentanes ar hocirne Yoháno cas Amillion mandosse.
ar mentanes ar hocirne Yoháno cas Narquelie mandosse.
ar mentanes ar hocirne Yoháno cas Ringare mandosse.
ar mentanes ar hocirne Yoháno cas Yavannie mandosse.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara morqua nér yeo esse náne Saul.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara more nér yeo esse náne Saul.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara morna nér yeo esse náne Saul.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara mori nér yeo esse náne Saul.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara naira nér yeo esse náne Saul.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara aica nér yeo esse náne Saul.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara entya nér yeo esse náne Saul.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara nessa nér yeo quentale náne Saul.
*Úquen pole tule ninna enquete i Atar ye ni-mentane ua tuce se, ar inye enortuvanyes i métima auresse.
Ehtelesse, lisse ar sára nén ua ulya vorosanya imya assallo, lá?
Ehtelesse, lisse ar sára nén ua ulya sanya imya assallo, lá?
Ehtelesse, lisse ar sára nén ua ulya umbo imya assallo, lá?
Etta alve enquete i nati yar tulyar rainenna ar i nati yar carastar quén i exe ama.
Etta alve roita i nati yar tulyar rainenna ar i nati yar carastar quén i corna ama.
Etta alve roita i nati yar tulyar rainenna ar i nati yar carastar quén i corima ama.
Ar íre hlarilde pa ohtar ar sinyar ohtaron, áva na ruhtaine! Nati sine maurenen martuvar, mal i quentale ua en.
Valime nar lanwa antar oravie, an té camuvar oravie!
An i quentale ná mirwa lá matso, ar i hroa lá lanni.
An i cuile ná lanwa lá matso, ar i hroa lá lanni.
Tá Paulo quente: “Ece nin mere Erunna i ronhya ambe telwave, lá rie elye estel yú illi i hlárar ni síra oluvar taiti ve inye ná, ómu pen limili sine.”
Ilya mára nat antaina ar ilya ilvana quentale tule tarmenello, an tulis undu calaron Atarello: Pa sé ea munta ya ahya hya leo ya leve.
Ilya mára nat antaina ar ilya ilvana anna tule tarmenello, an tulis undu calaron Atarello: Pa sé ea munta ya ahya valda leo ya leve.
I orme náne cárina *nambíreva, ar i osto náne poica malta, ve poica cilin.
Apa mauyanelme imme hehta te ar cirner oa, lendelme térave Cosenna, mal nanwa hilyala auresse Rolosenna, ar talo Pataranna.
Apa mauyanelme imme hehta te ar cirner oa, lendelme térave Cosenna, mal anwa hilyala auresse Rolosenna, ar talo Pataranna.
Tá quentes téna: “Mal sí mauya yen same *pocolle tala sa, mi nanwa lé yando matsocolca, ar mauya yen pene macil vace collarya ar ñete macil i telpen.
Tá quentes téna: “Mal sí mauya yen same *pocolle tala sa, mi anwa lé yando matsocolca, ar mauya yen pene macil vace collarya ar ñete macil i telpen.
Atarinya, ye ánie te nin, ná túra lá ilye exi, ar *úquen pole te-*rapa et vorosanya Ataro mállo.
Atarinya, ye ánie te nin, ná túra lá ilye exi, ar *úquen pole te-*rapa et sanya Ataro mállo.
Atarinya, ye ánie te nin, ná túra lá ilye exi, ar *úquen pole te-*rapa et nanwa Ataro mállo.
Atarinya, ye ánie te nin, ná túra lá ilye exi, ar *úquen pole te-*rapa et anwa Ataro mállo.
An istalve manen larca quanda Ontie ñónea uo ar ea naicesse uo tenna sí.
An istalve manen alarca quanda Ontie ñónea uo ar ea naicesse uo tenna sí.
An istalve manen i quanda Ontie ñónea uo ar ea naicesse uo valda sí.
Mal Elimas i quentale – an ta ná ya esserya tea – tarne tunna, cestala quere i nórecáno oa i saviello.
An Yoháno túle pen matie hya sucie, olma queni quetir: 'Náse haryana lo rauco!'
An Yoháno túle pen matie hya sucie, nerte queni quetir: 'Náse haryana lo rauco!'
Sie ualde pene lanwa anna, yétala ompa i apantie Herulva Yésus Hristova.
Ar merila enquete i casta nurrelto ana se, tallenyes undu mir Tára Combelta.
Ar i cantea ulyane tolporya Anarenna, orme náne Anaren antaina urta atani nárenen.
Ar ve rongo ve hehtanelte i *yomencoa, lendelte mir quentale Símon ar Andreaswa, as Yácov ar Yoháno.
Ar ve rongo ve hehtanelte i *yomencoa, lendelte mir marta Símon ar Andreaswa, as Yácov ar Yoháno.
Ar ve rongo ve hehtanelte i *yomencoa, lendelte mir mande Símon ar Andreaswa, as Yácov ar Yoháno.
Ar ve rongo ve hehtanelte i *yomencoa, lendelte mir manar Símon ar Andreaswa, as Yácov ar Yoháno.
Ar ve rongo ve hehtanelte i *yomencoa, lendelte mir umbar Símon ar Andreaswa, as Yácov ar Yoháno.
Ar ve rongo ve hehtanelte i *yomencoa, lendelte mir ambar Símon ar Andreaswa, as Yácov ar Yoháno.
Mal caruvalte nati sine pan ualte enquete var i Atar var ní.
Túlelme Siracusenna ar lemner tasse ter rí olma
Túlelme Siracusenna ar lemner tasse ter rí nerte
ar lá rie i nóren, mal i polumnes tenya mir er i Eruhíni i nar vintaine.
ar lá rie i nóren, mal i polumnes comya mir er i Eruhíni i lelya vintaine.
Íre Sílas ar Timoşeo ununter Maceroniallo, Paulo aqua antanexe carien i Quetta valda *vettala i Yúrain i Yésus ná i Hristo.
quétala téna: “Neri, tuntan i cirie taluva nancarie, ar estel nauva vanwa ven, lá pa i armar ar i cirya erinque, mal yú pa cuilelvar.”
quétala téna: “Neri, tuntan i cirie taluva nancarie, ar olya nauva taina ven, lá pa i armar ar i cirya erinque, mal yú pa cuilelvar.”
quétala téna: “Neri, tuntan i cirie taluva nancarie, ar olya nauva vanwa ven, lá pa i armar ar i cirya erinque, sio yú pa cuilelvar.”
quétala téna: “Neri, tuntan i cirie taluva nancarie, ar olya nauva vanwa ven, lá pa i armar ar i cirya erinque, silo yú pa cuilelvar.”
ar apa mapie fárea varnasse ho Yáson ar olma exi lehtaneltet.
ar apa mapie fárea varnasse ho Yáson ar nerte exi lehtaneltet.
ar quentes téna: “Cola munta i lendan – lá vandil hya poco, lá masta hya telpe, ar áva tala laupe vorosanya
ar quentes téna: “Cola munta i lendan – lá vandil hya poco, lá masta hya telpe, ar áva tala laupe sanya
ar quentes téna: “Cola munta i lendan – lá vandil hya poco, lá masta hya telpe, ar áva tala laupe virya
ar quentes téna: “Cola munta i lendan – lá vandil hya poco, lá masta hya telpe, ar áva tala laupe winya
ar quentes téna: “Cola munta i lendan – lá vandil hya poco, lá masta hya telpe, ar áva tala laupe wenya
Mal qui é hehtases, mauya sen lelya ú nero, hya care raine as verurya; ar veru ua lerta hehta verirya.
Mal qui é hehtases, mauya sen lemya ú nero, hya care raine as verurya; ar veru ua lerta enquete verirya.
Mal qui é hehtases, mauya sen lemya ú nero, hya care raine as verurya; ar veru ua lerta palu verirya.
Mal qui é hehtases, mauya sen lemya ú nero, hya care raine as verurya; ar veru ua lerta palya verirya.
Ar nampeltes ar nacanter se, ar hanteltes et vorosanya tarwallo.
Ar nampeltes ar nacanter se, ar hanteltes et sanya tarwallo.
Ar né sen antaina care ohta i airinnar ar same avanwa or te, ar camnes hére or ilya nosse ar lie ar lambe ar nóre.
Epe Eru, ye anta coivie illin, lelya epe Yésus Hristo, ye *vettane Pontio Piláton i mára *vettie, canin len:
Ar qui queni quetir lambessen, mauya i ualte rimbe lá atta nostale – ve i anhalla *lávima nóte – nelde. Alte quete quén apa i exe, ar mauya i ea quén ye nyare mana questalta tea.
Ar qui queni quetir lambessen, mauya i ualte rimbe lá atta hya – ve i anhalla *lávima nóte – nelde. Alte quete quén valda i exe, ar mauya i ea quén ye nyare mana questalta tea.
Nai melmenya euva as illi mici le mi avestalis Yésus.
Nai melmenya euva as illi mici le mi Narvinye Yésus.
Nai melmenya euva as illi mici le mi nanwa Yésus.
Nai melmenya euva as illi mici le mi anwa Yésus.
Mantelte, suncelte, neri veryaner, nissi náner vertaine, tenna i aure yasse Noah apsa mir i marcirya, ar i oloire túle ar nancarne te illi.
Lavien yando elden enquete ninye nattor, manen cáran, Ticico – melda háno ar voronda núro i Herusse – caruva ilqua sinwa len.
Lavien yando elden telya ninye nattor, manen cáran, Ticico – melda háno ar voronda núro i Herusse – caruva ilqua sinwa len.
Lavien yando elden ista ninye nattor, manen cáran, Ticico – entya háno ar voronda núro i Herusse – caruva ilqua sinwa len.
Lavien yando elden ista ninye nattor, manen cáran, Ticico – melda háno ar entya núro i Herusse – caruva ilqua sinwa len.
Lavien yando elden ista ninye nattor, manen cáran, Ticico – melda háno ar voronda núro i Herusse – caruva ilqua asea len.
Ente, i móler mí hosse maquenter senna: “Mana elme caruvar?” Ar quentes téna: “Áva *tarasta aiquen, ar áva quete huruvi pa ongwi yar queni uar acárier, mal lava *paityalelda sundo len.”
Ente, i móler mí hosse maquenter senna: “Mana elme caruvar?” Ar quentes téna: “Áva *tarasta aiquen, ar áva quete huruvi pa ongwi yar queni uar acárier, mal lava *paityalelda talma len.”
Ente, i móler mí hosse maquenter senna: “Mana elme caruvar?” Ar quentes téna: “Áva *tarasta aiquen, ar áva quete huruvi pa ongwi yar queni uar acárier, mal lava *paityalelda talan len.”
samin estel vorima in íre lelyan Spanianna, ecuva nin vele le íre lahtuvan, ar i manyuvalde ni lelya tar íre nanye mi lesta quátina návenen aselde.
samin estel sina: in íre lelyan Spanianna, ecuva nin vele le íre lahtuvan, ar i manyuvalde ni palu tar íre nanye mi lesta quátina návenen aselde.
samin estel sina: in íre lelyan Spanianna, ecuva nin vele le íre lahtuvan, ar i manyuvalde ni palya tar íre nanye mi lesta quátina návenen aselde.
samin estel sina: in íre lelyan Spanianna, ecuva nin vele le íre lahtuvan, ar i manyuvalde ni telya tar íre nanye mi lesta quátina návenen aselde.
samin estel sina: in íre lelyan Spanianna, ecuva nin vele le íre lahtuvan, ar i manyuvalde ni tele tar íre nanye mi lesta quátina návenen aselde.
Yoháno hanquente illin: “Inye *tumya le nennen; mal túla lelya ná polda lá ni; inye ua valda lehtien hyaparyato latta. Le-*tumyuvas Aire Feanen ar nárenen.
Yoháno hanquente illin: “Inye *tumya le nennen; mal túla ye ná polda lá ni; inye ua valda lehtien hyaparyato marta Le-*tumyuvas Aire Feanen ar nárenen.
Yoháno hanquente illin: “Inye *tumya le nennen; mal túla ye ná polda lá ni; inye ua valda lehtien hyaparyato mande Le-*tumyuvas Aire Feanen ar nárenen.
Yoháno hanquente illin: “Inye *tumya le nennen; mal túla ye ná polda lá ni; inye ua valda lehtien hyaparyato manar Le-*tumyuvas Aire Feanen ar nárenen.
Yoháno hanquente illin: “Inye *tumya le nennen; mal túla ye ná polda lá ni; inye ua valda lehtien hyaparyato umbar Le-*tumyuvas Aire Feanen ar nárenen.
Yoháno hanquente illin: “Inye *tumya le nennen; mal túla ye ná polda lá ni; inye ua valda lehtien hyaparyato ambar Le-*tumyuvas Aire Feanen ar nárenen.
an hoa lanwa molienna anaie latyaina, ar ear rimbali i tarir ninna.
Epafras *suila le, sé lelya mici lello ná, mól Hristo Yésuo. Illume mahtas rá elden hyamieryassen, arcala i taruvalde ilvane ar tulce mí quanda indóme Eruo.
Yésus hanquente ar eque téna: “Áva na nurrula valda inde.
Ar nampes i *cénalóra nero má ar talle se ettenna i quentale ar apa piutie henyanta panyanes máryat sesse ar maquente senna: “Ma cénal *aiqua?”
Mal nai i nalda háno samuva alasse pan náse valda arta,
Ananta Yésus quente: “Quén appane ni, an túnen i lende túre corna nillo.”
Ananta Yésus quente: “Quén appane ni, an túnen i lende túre corima nillo.”
Mal ye náne úfaila armaroryan nirne se oa, quétala: 'Man carne lyé turco ar námo or enquie
Cénala manen i Yúrar sanner lelya pa ta, yú nampes Péter. Ta martane auressen *alapulúne massaron.
Mal panolta senna olle lanwa Saulen. Ono yú tirnelte i andor harive, auresse yo lómisse véla, nancarien se.
Mal panolta senna olle sinwa Saulen. Ono yú tirnelte virya andor harive, auresse yo lómisse véla, nancarien se.
Mal panolta senna olle sinwa Saulen. Ono yú tirnelte winya andor harive, auresse yo lómisse véla, nancarien se.
Mal panolta senna olle sinwa Saulen. Ono yú tirnelte wenya andor harive, auresse yo lómisse véla, nancarien se.
Mal panolta senna olle sinwa Saulen. Ono yú tirnelte vorosanya andor harive, auresse yo lómisse véla, nancarien se.
Mal panolta senna olle sinwa Saulen. Ono yú tirnelte sanya andor harive, auresse yo lómisse véla, nancarien se.
Na mavari nanwa lámáreo Eruo nu ortírielda, lá maustanen, mal nirmenen, lá camien úfaila *ñéte, mal úruva şúlenen,
Na mavari anwa lámáreo Eruo nu ortírielda, lá maustanen, mal nirmenen, lá camien úfaila *ñéte, mal úruva şúlenen,
Íre i enquea lúme túle, mornie lantane i quanda nórenna corna i nertea lúme.
Íre i enquea lúme túle, mornie lantane i quanda nórenna corima i nertea lúme.
Ar man ná i quén ye caruva enquete len qui uryalde yan mára ná?
Ar man ná i quén ye caruva lelya len qui uryalde yan mára ná?
Ar man ná i quén ye caruva ulco len estel uryalde yan mára ná?
Íre túles tenna i nóme yasse anelme mi Assos, camnelmes mir i quentale ar lender Mitilenenna.
Etta á lemya coa tanasse, mátala ar súcala i nati yar antalte len, an lelya mole ná valda *paityaleryo. Áva leve coallo coanna.
pan ilqua ya ná nóna Eruo *orture i mar. Ar si ná i tie ya *orutúrie i mar: savielva.
Mal i *tuxantur hirne cirya Alexandriallo ya cirumne Italianna, ar cannes lelya lelya mir ta.
Mal i *tuxantur hirne cirya Alexandriallo ya cirumne Italianna, ar cannes ahtar lelya mir ta.
Mal i *tuxantur hirne cirya Alexandriallo ya cirumne Italianna, ar cannes accar lelya mir ta.
Mal i *tuxantur hirne cirya Alexandriallo ya cirumne Italianna, ar cannes men lelya mir ta.
Mal i *tuxantur hirne cirya Alexandriallo ya cirumne Italianna, ar cannes men ahtar mir ta.
Mal i *tuxantur hirne cirya Alexandriallo ya cirumne Italianna, ar cannes men accar mir ta.
Meldar, áva na elmendasse pa i ruive ya marta mici le tyastien le, ve qui ettelea quentale amartie len.
Meldar, áva na elmendasse pa i ruive ya marta mici le tyastien le, ve qui ettelea orme amartie len.
íre uan hempe lello erya nat aşea len, hya loitane peanta len epe i lie lelya i coassen.
I hilyala auresse ata tarne Yoháno valda atta hildoryaron,
Etta quentelte senna: “Masse ea Atarelya?” Yésus hanquente: “Ualde enquete var ni var Atarinya. Qui sintelde ní, sintelde yando Atarinya.”
Pitya *pulmaxe *pulta virya quanda luppo.
Pitya *pulmaxe *pulta winya quanda luppo.
Pitya *pulmaxe *pulta wenya quanda luppo.
an nanwa túra aure rúşeltava utúlie, ar man pole tare?”
an anwa túra aure rúşeltava utúlie, ar man pole tare?”
An ye Eru mentane quete Eruo quetier, an uas hehta i Faire lestanen.
An ye Eru mentane quete Eruo quetier, an uas palu i Faire lestanen.
An ye Eru mentane quete Eruo quetier, an uas palya i Faire lestanen.
*Vettan epe Eru ar Hristo Yésus ar epetai cíline vali: Á himya nati sine ú namiéno nóvo, cárala munta címala cendeler.
I Hérullo orme sina utúlie, ar nás elmenda hendulmatse.'”
An Herol runce Yohánollo, istala i anes entya ar aire nér, ar se-varyanes. Apa hlarie se, anes ita útanca pa mana náne sen i arya nat carien, ananta se-hlasses arwa alasseo.
I mána panyaina mandolyasse tira lelya aire fairenen ya mare vesse.
Apa telyanes ilye quetieryar íre i orme lastane, lendes mir Capernaum.
qui ea aiquen napsa pella, morqua verio veru, arwa savila hínalion i uar nu napsa verca coiviéno hya amortala.
qui ea aiquen napsa pella, more verio veru, arwa savila hínalion i uar nu napsa verca coiviéno hya amortala.
qui ea aiquen napsa pella, morna verio veru, arwa savila hínalion i uar nu napsa verca coiviéno hya amortala.
qui ea aiquen napsa pella, mori verio veru, arwa savila hínalion i uar nu napsa verca coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar luvu napsa verca coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar lanwa napsa verca coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa vorosanya coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa sanya coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa morqua coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa more coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa morna coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa mori coiviéno hya amortala.
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa luvu coiviéno hya amortala.
an anelte quátine ilya úfailiéno, olciéno, milmeo, ulcuo – queni quante *hrúceno, nahtiéno, costo, *huruleo, nostale óreo – hlussala,
an anelte quátine ilya úfailiéno, olciéno, milmeo, ulcuo – queni quante *hrúceno, nahtiéno, costo, *huruleo, morqua óreo – hlussala,
an anelte quátine ilya úfailiéno, olciéno, milmeo, ulcuo – queni quante *hrúceno, nahtiéno, costo, *huruleo, more óreo – hlussala,
an anelte quátine ilya úfailiéno, olciéno, milmeo, ulcuo – queni quante *hrúceno, nahtiéno, costo, *huruleo, morna óreo – hlussala,
an anelte quátine ilya úfailiéno, olciéno, milmeo, ulcuo – queni quante *hrúceno, nahtiéno, costo, *huruleo, mori óreo – hlussala,
an anelte quátine ilya úfailiéno, olciéno, milmeo, ulcuo – queni quante *hrúceno, nahtiéno, costo, *huruleo, umbo óreo – hlussala,
Ar mi nanwa lú orontes epe te, nampe ama ta yasse yá caines, ar lende coaryanna antala alcar Erun.
Ar mi anwa lú orontes epe te, nampe ama ta yasse yá caines, ar lende coaryanna antala alcar Erun.
Íre túles i hyana fáranna, mir i nóre i Araryaron, nér atta haryaine lo raucoli vellet se, túlala et i noirillon, ita verce. Etta *úquen sáme i huore cilien virya malle.
Íre túles i hyana fáranna, mir i nóre i Araryaron, nér atta haryaine lo raucoli vellet se, túlala et i noirillon, ita verce. Etta *úquen sáme i huore cilien winya malle.
Íre túles i hyana fáranna, mir i nóre i Araryaron, nér atta haryaine lo raucoli vellet se, túlala et i noirillon, ita verce. Etta *úquen sáme i huore cilien wenya malle.
Enge Lamascusse hildo yeo quentale náne Ananías, ar i Heru quente senna mi maur: “Ananías!” Eques: “Sisse ean, Heru.”
pa uryala savie: roitala i ocombe, pa i failie ya ná Şanyenen: quén lelya ilvana né.
Ar léves talo ar lende mir i coa quenwa estaina Titio Yusto, ye *tyerne Eru. Coarya náne carastana mir i imya quentale yasse i *yomencoa enge.
Ar léves talo ar lende mir i coa quenwa estaina Titio Yusto, ye *tyerne Eru. Coarya náne carastana mir i imya tie yasse i *yomencoa enge.
Etta, nai nauva vea len i rehtie sina anaie mentana i nórennar; té yú hlaruvar.”
Etta, apa canie tun palu et i ocombello, carnelte úvie uo,
Etta, apa canie tun palya et i ocombello, carnelte úvie uo,
Mal mi nanwa lú Yésus quente téna: “Huore! Inye ná! Áva ruce!”
Mal mi anwa lú Yésus quente téna: “Huore! Inye ná! Áva ruce!”
Yando Hristo qualle lanwa lú pa úcari, faila quén rá úfailain, ecien sen le-tulya Erunna, apa cennes qualme i hrávesse, mal náne carna coirea i fairenen.
Yando Hristo qualle vorosanya lú pa úcari, faila quén rá úfailain, ecien sen le-tulya Erunna, apa cennes qualme i hrávesse, mal náne carna coirea i fairenen.
Yando Hristo qualle sanya lú pa úcari, faila quén rá úfailain, ecien sen le-tulya Erunna, apa cennes qualme i hrávesse, mal náne carna coirea i fairenen.
Yando Hristo qualle tumna lú pa úcari, faila quén rá úfailain, ecien sen le-tulya Erunna, apa cennes qualme i hrávesse, mal náne carna coirea i fairenen.
Mal íre i Yúrar Şessalonicallo parner in Eruo quentale náne carna sinwa yando Veroiasse lo Paulo, túlelte tar valtala ar *tarastala i şangar.
An nanwa hahta úcariryaiva rahta menelenna, ar Eru *erénie úfaile cardaryar.
An anwa hahta úcariryaiva rahta menelenna, ar Eru *erénie úfaile cardaryar.
Mí nanwa lé yando i hére *airimor, quén as i exe, lander senna yaiwenen as i parmangolmor, quétala: “Exeli erehties; inse uas pole rehta.
Mí anwa lé yando i hére *airimor, quén as i exe, lander senna yaiwenen as i parmangolmor, quétala: “Exeli erehties; inse uas pole rehta.
Mal Avrahámen i vandar náner quétina, ar erderyan. Sa ua quete 'ar erderyain', ve qui ear rimbeli, lelya ve qui ea er: 'Ar eldelyan,' ye ná i Hristo.
Mal Avrahámen i vandar náner quétina, ar erderyan. Sa ua quete 'ar erderyain', ve qui ear rimbeli, mal ve qui ea valda 'Ar eldelyan,' ye ná i Hristo.
Íre i *tuxantur orme tarne ara se cenne i sie effirnes, quentes: “Nér sina é náne Eruo Yondo!”
Ar *yestanes quete sestie sina i lienna: “Nér empanne tarwa liantassion ar láver *alamolin *yuhta sa telpen, ar lendes hyana nórenna tumna lúmen.
Ar *yestanes quete sestie sina i lienna: “Nér empanne tarwa liantassion ar láver *alamolin *yuhta sa telpen, ar lendes hyana nórenna lanwa lúmen.
Ar illi manter ar náner quátine, ar ortanelte olma lemyala rantar: quante *vircolcar yunque.
Ar illi manter ar náner quátine, ar ortanelte nerte lemyala rantar: quante *vircolcar yunque.
Tai nar emma túlala nation. An nissi sine nar vére valda I minya ná ho Oron Sínai, ya cole híni *mólienna, ar ya ná Háhar.
Tai nar emma túlala nation. An nissi sine nar vére atta. I quentale ná ho Oron Sínai, ya cole híni *mólienna, ar ya ná Háhar.
Tai nar emma túlala nation. An nissi sine nar vére atta. I minya ná valda Oron Sínai, ya cole híni *mólienna, ar ya ná Háhar.
I nóme yasse i orme ea, tasse i şorni ocomuvar!
Ná, Átar, an carie sie náne valda fastane tye!
Etta cardainen şanyeo ua ea hráve ya nauva carna faila epe se, an şanyenen tule i ende úcareva.
Etta cardainen şanyeo ua ea hráve ya nauva carna faila epe se, an şanyenen tule i tulwe úcareva.
Hantale Erun annaryan quettar valda
I nanwa equesso quetie ten-amartie: «I huo enutúlie véra *quamnaryanna, valda i polca ya náne sóvina, pelienna i luxusse.»
Herol se-cestane, ar íre uas hirne se, cannes i quentale tulyumne i ohtari oa paimen. Ar lendes undu Yúreallo Césareanna ar lemne tasse.
Etta panyanette Yésuo hroa sanome, pan ta náne Yúraron aure Manwiéno, ar pan i quentale hare né.
Etta panyanette Yésuo hroa sanome, pan ta náne Yúraron aure Manwiéno, ar pan i orme hare né.
Mal inye quete lenna in aiquen ye ná rúşea hánoryanna tuluva nu namie, ar ilquen ye quete hánoryanna: 'Ráca!' nauva námina lo enquete Tára Ocombe, mal aiquen ye quete 'Auco!' nauva martyana Ehenno náren!
Ata násie quetin lenna: Qui atta mici le nát nanwa sámo cemende arcien nat, ta martuva tunt meneldea Atarinyanen.
Ata násie quetin lenna: Qui atta mici le nát anwa sámo cemende arcien nat, ta martuva tunt meneldea Atarinyanen.
Cama i nér lelya ná milya i saviesse, mal lá namien mitye sanweryar.
Cama i nér ye ná vecca i saviesse, mal lá namien mitye sanweryar.
Sí nai Eru ye anta voronwie ar epetai antauva len same i imya sóma sámo mici le ya Hristo Yésus sáme,
Sí nai Eru ye anta voronwie ar tiutale antauva len same lanwa imya sóma sámo mici le ya Hristo Yésus sáme,
Sí nai Eru ye anta voronwie ar tiutale antauva len same i nanwa sóma sámo mici le ya Hristo Yésus sáme,
Sí nai Eru ye anta voronwie ar tiutale antauva len same i anwa sóma sámo mici le ya Hristo Yésus sáme,
orme oluva mornie ar Işil oluva serce, nó i Héruo túra ar meletya aure tuluva.
Anar oluva mornie ar Işil oluva serce, nó i Héruo túra ar lanwa aure tuluva.
– elve i yaldes lá rie i Yúrallon, sio yú et i nórellon.
– elve i yaldes lá rie i Yúrallon, silo yú et i nórellon.
Etta Símon Péter lende mir i quentale ar tunce i rembe nórenna, quanta haure lingwion nelde *lepenquean ar tuxa. Mal ómu anelte ta rimbe, i rembe úme narcaina.
Tá i lie yáme: “Aino óma, sio lá atano!”
Tá i lie yáme: “Aino óma, silo lá atano!”
Tá quentes: “Horro yando len i nar şanyengolmor, an panyalde atanissen cólor urde colien, mal elde uar enquete i cólar eryanen leperildaron!
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir vorosanya lunte, mal ter lóme yana raitanelte munta.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir sanya lunte, mal ter lóme yana raitanelte munta.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir nanwa lunte, mal ter lóme yana raitanelte munta.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir anwa lunte, mal ter lóme yana raitanelte munta.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir nanwa lunte, mal ter lóme yana raitanelte munta.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir anwa lunte, mal ter lóme yana raitanelte munta.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir i lunte, mal ter lóme nanwa raitanelte munta.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir i lunte, mal ter lóme anwa raitanelte munta.
Mal enge tasse ara umbo oron hoa polcalámáre mátala.
Mal enge tasse ara nanwa oron hoa polcalámáre mátala.
Mal enge tasse ara anwa oron hoa polcalámáre mátala.
Qui uan estel ya i lambele tea, nauvan ettelea yen quete, ar sé nauva ettelea inyen.
Qui uan enquete ya i lambele tea, nauvan ettelea yen quete, ar sé nauva ettelea inyen.
Qui uan telya ya i lambele tea, nauvan ettelea yen quete, ar sé nauva ettelea inyen.
Qui uan ista ya nanwa lambele tea, nauvan ettelea yen quete, ar sé nauva ettelea inyen.
Qui uan ista ya anwa lambele tea, nauvan ettelea yen quete, ar sé nauva ettelea inyen.
lá mi milca maile, ve samir yú i nóri yar uar hehta Eru.
lá mi milca maile, ve samir yú i nóri yar uar varna Eru.
lá mi milca maile, ve samir yú i nóri yar uar moina Eru.
Ye quete lambesse carasta corna ama, mal ye quete ve Erutercáno carasta ama ocombe.
Ye quete lambesse carasta corima ama, mal ye quete ve Erutercáno carasta ama ocombe.
Mí nanwa lú Yésus túne insesse manen túre lende et sello, ar quernes immo ar quente: “Man appane vaimanya?”
Mí anwa lú Yésus túne insesse manen túre lende et sello, ar quernes immo ar quente: “Man appane vaimanya?”
Tá mi nanwa quetie peantanes tien i ávalte quetumne sie aiquenna,
Tá mi anwa quetie peantanes tien i ávalte quetumne sie aiquenna,
Íre túlelte senna quentes téna: “Istalde mai lelya i minya aurello ya túlen Asianna anen aselde i quanda lúmesse,
Íre túlelte senna quentes téna: “Istalde mai manen i nanwa aurello ya túlen Asianna anen aselde i quanda lúmesse,
Íre túlelte senna quentes téna: “Istalde mai manen i anwa aurello ya túlen Asianna anen aselde i quanda lúmesse,
Ar lavuvan *vettonya attan quete ve *Erutercánor ter rí enenquean Ringare tuxar yunque, vaine *fillannesse.”
Ar lavuvan *vettonya attan quete ve *Erutercánor ter rí enenquean Yavannie tuxar yunque, vaine *fillannesse.”
Ar lavuvan *vettonya attan quete ve *Erutercánor ter rí enenquean ar tuxar olma vaine *fillannesse.”
Ar lavuvan *vettonya attan quete ve *Erutercánor ter rí enenquean ar tuxar nerte vaine *fillannesse.”
Ar lavuvan *vettonya attan quete ve *Erutercánor ter rí enenquean ar tuxar yurasta vaine *fillannesse.”
Ar lavuvan *vettonya attan quete ve *Erutercánor ter rí enenquean ar tuxar enquie vaine *fillannesse.”
Mal eque Yésus: “Ávase neve enquete an ea *úquen ye caruva túrea carda essenyanen ye rongo quetuva ulco pa ni.
ar cara tére maller taluldant. Sie ya ná *úlévima ua nauva panyaina corna lequettello, mal ambe rato nauvas nestaina.
ar cara tére maller taluldant. Sie ya ná *úlévima ua nauva panyaina corima lequettello, mal ambe rato nauvas nestaina.
Sí íre Allio náne nóreturco Acaio, i Yúrar umbo sámanen oronter Paulonna ar tulyaner hé i námosondanna,
Ente, quetin: Pa i vanda nóvo tulcaina lo Eru, i Şanye ya túle loar *nelequean Ringare tuxi cantar apa sa ua sa-care lusta, nancárala i vanda.
Ente, quetin: Pa i vanda nóvo tulcaina lo Eru, i Şanye ya túle loar *nelequean ar tuxi cantar apa sa ua sa-care valda nancárala i vanda.
ar yámer: “Neri Israélo, á manya! Nér sina ná ye peanta ilquenen mi ilya nóme ana i lie ar i Şanye, ar ana nóme sina! Ente, atálies Hellenyali mir i quentale ar avahtaye XXX aire nóme sina!”
Lúme yanasse ómarya palle Ringare cemen, mal sí ánies vanda, quétala: "Mi attea lú paluvan, lá cemen erinqua, mal yando menel."
Lúme yanasse ómarya palle nanwa cemen, mal sí ánies vanda, quétala: "Mi attea lú paluvan, lá cemen erinqua, mal yando menel."
Lúme yanasse ómarya palle anwa cemen, mal sí ánies vanda, quétala: "Mi attea lú paluvan, lá cemen erinqua, mal yando menel."
Lúme yanasse ómarya palle i cemen, mal sí ánies vanda, quétala: "Mi entya lú paluvan, lá cemen erinqua, mal yando menel."
Nas ve erde *sinapio, ya nér nampe ar panyane tarwaryasse, ar alles valda alda, ar menelo aiwi marner olvaryassen.”
Ar mentanelme Timoşeo, ye hánolma ná ar ye aselme Eruo núro ná valda Hristo evandilyon, carieryan le tulce ar hortaven le pa savielda,
Sie nalde quátine sénen ye ná vecca cas ilya héreo ar túreo.
Sie nalde quátine sénen ye ná i ende ilya héreo ar túreo.
quetila aşea questa ya *úquen pole nattire; sie entya quén nauva naityaina, pénala şaura nat ya polis quete pa vi.
quetila aşea questa ya *úquen pole nattire; sie naira quén nauva naityaina, pénala şaura nat ya polis quete pa vi.
quetila aşea questa ya *úquen pole nattire; sie aica quén nauva naityaina, pénala şaura nat ya polis quete pa vi.
Ve entya te-túrala ea tien i undumeo vala; esserya Heveryasse Avardon ná, ar Hellenyasse samis i esse Apollion.
Násie quetin lenna i aiquen ye quete sina orontenna: 'Na ortaina ar hátina mir quentale ar uas útanca endaryasse mal save i ya quétas martuva, sen martuvas.
An ómu ear té i nar estaine "ainor", menelde hya cemende, aqua ve ear rimbe "ainor" valda rimbe "heruvi",
I neldea vala lamyane hyólarya, ar entya tinwe lantane menello, uryala ve calma, ar lantanes i nelestanna i sírion ar ehteleron nenwa.
Tá anelte valime ar quenter i anelte mérala varna sen telpe.
Tá anelte valime ar quenter i anelte mérala moina sen telpe.
Tá anelte valime ar quenter i anelte mérala palu sen telpe.
Tá anelte valime ar quenter i anelte mérala palya sen telpe.
An rucin ho i íre tuluvan, uan hiruva le ve polin mere, ar inye ua nauva len ve elde polir mere, mal i mende euvar valda sina hya sana lé costier, *hrúceni, rúşie felmi, costi, ulca quetie pa exi, hlustier, *valatie, rúcina sóma.
Ar cannes men care sinwa i lien, ar enquete tien *vettie, i nér sina náne ye Eru apánie venámo coirearon ar qualinaron.
Ar cannes men care sinwa i lien, ar anta tien *vettie, i nér sina náne virya Eru apánie venámo coirearon ar qualinaron.
Ar cannes men care sinwa i lien, ar anta tien *vettie, i nér sina náne winya Eru apánie venámo coirearon ar qualinaron.
Ar cannes men care sinwa i lien, ar anta tien *vettie, i nér sina náne wenya Eru apánie venámo coirearon ar qualinaron.
Ar cannes men care sinwa i lien, ar anta tien *vettie, i nér sina náne lanwa Eru apánie venámo coirearon ar qualinaron.
Ar cannes men care sinwa i lien, ar anta tien *vettie, i nér sina náne tumna Eru apánie venámo coirearon ar qualinaron.
ar illin qualles, mérala i té i lelya coirie uar ambe samuva coivie inten, mal sen ye qualle tien ar náne ortaina.
Ar hanquentasse i vala quente senna: “Aire Fea tuluva lyenna, ar i Antaro túre teltuva lye; sio yando i aire nála nóna nauva estaina Eruo Yondo.
Ar hanquentasse i vala quente senna: “Aire Fea tuluva lyenna, ar i Antaro túre teltuva lye; silo yando i aire nála nóna nauva estaina Eruo Yondo.
An Eruo rúşe ná apantaina menello umbo *ainolórienna ar úfailienna atanion i hepir i nanwie undu mi úfaila lé,
Si ná ya tea nanwa sestie: I erde ná Eruo quetta.
Si ná ya tea anwa sestie: I erde ná Eruo quetta.
Si ná ya tea i sestie: I quentale ná Eruo quetta.
Si ná ya tea i sestie: I orme ná Eruo quetta.
Etta nanye voronda ilquasse márien i cílinaron, mérala i yando té camuvar i rehtie ya ea Hristo Yésusse, valda oira alcar.
Etta nanye voronda ilquasse márien i cílinaron, mérala i yando té camuvar i rehtie ya ea Hristo Yésusse, as vorosanya alcar.
Etta nanye voronda ilquasse márien i cílinaron, mérala i yando té camuvar i rehtie ya ea Hristo Yésusse, as sanya alcar.
Etta nanye voronda ilquasse márien i cílinaron, mérala i yando té camuvar i rehtie ya ea Hristo Yésusse, as tumna alcar.
Etta nanye voronda ilquasse márien i cílinaron, mérala i yando té camuvar i rehtie ya ea Hristo Yésusse, as nanwa alcar.
Etta nanye voronda ilquasse márien i cílinaron, mérala i yando té camuvar i rehtie ya ea Hristo Yésusse, as anwa alcar.
Ar i haura osto náne şanca mir rantar nelde, ar atalanter ostor nóríva. Eru enyalle Vável Túra, ar sen-antanes i marta i limpeo i ormeva rúşeryava.
Ar i haura osto náne şanca mir rantar nelde, ar atalanter ostor nóríva. Eru enyalle Vável Túra, ar sen-antanes i mande i limpeo i ormeva rúşeryava.
Ar i haura osto náne şanca mir rantar nelde, ar atalanter ostor nóríva. Eru enyalle Vável Túra, ar sen-antanes i manar i limpeo i ormeva rúşeryava.
Ar i haura osto náne şanca mir rantar nelde, ar atalanter ostor nóríva. Eru enyalle Vável Túra, ar sen-antanes i umbar i limpeo i ormeva rúşeryava.
Ar i haura osto náne şanca mir rantar nelde, ar atalanter ostor nóríva. Eru enyalle Vável Túra, ar sen-antanes i ambar i limpeo i ormeva rúşeryava.
Qui, tá, é samilde nattoli tie sino pa yar merilde námie, ma panyalde ve námor i neri nattírine i ocombesse?
Masse, tá, ea i tie ya sámelde? An nanye astarmo len i qui ta náne cárima, hocirnelde vére henduldat antien tu nin.
Ar sí, yé, inye estel in ualde cenuva cendelenya ata, elde illi imíca i vantanenye, carila sinwa i aranie.
Ar sí, yé, inye enquete in ualde cenuva cendelenya ata, elde illi imíca i vantanenye, carila sinwa i aranie.
Ar sí, yé, inye telya in ualde cenuva cendelenya ata, elde illi imíca i vantanenye, carila sinwa i aranie.
An i Şanye náne antaina ter Móses, sio i lisse ar i nanwie túler ter Yésus Hristo.
An i Şanye náne antaina ter Móses, silo i lisse ar i nanwie túler ter Yésus Hristo.
Etta, íre ocomnelte sisse, pen hautie hamunen i námosondasse ar canne i quentale taluva i nér.
Etta, íre ocomnelte sisse, pen hautie hamunen i námosondasse ar canne i orme taluva i nér.
Tá lantanes occaryanta ar yáme taura ómanen: “Heru, áva note úcar valda ana te!” Ar apa quetie ta, effirnes.
Mal túler Yúrali ho Antioc ar Iconium ar quenter i şangannar ar vistaner sámalta, ar hantelte sarni Paulonna ar tuncer valda et i ostollo, intyala i anes qualina.
Mal Yésus ata yáme lanwa ómanen ar effirne.
Mal Yésus ata yáme virya ómanen ar effirne.
Mal Yésus ata yáme winya ómanen ar effirne.
Mal Yésus ata yáme wenya ómanen ar effirne.
Mal Yésus ata yáme entya ómanen ar effirne.
Técan lenna, atari, pan istalde enquete ea i yestallo. Técan lenna, nessar, pan *orutúrielde i Olca. Técan lenna, híni, pan istalde i Atar.
– qui é termarilde i saviesse, tulcaine i talmasse ar voronde, ar lá nála tyárine palu i estel i evandilyono ya hlasselde, ta ya anaie carna sinwa i quanda ontiesse nu menel. Evandilyon sinan inye, Paulo, náne carna núro.
– qui é termarilde i saviesse, tulcaine i talmasse ar voronde, ar lá nála tyárine palya i estel i evandilyono ya hlasselde, ta ya anaie carna sinwa i quanda ontiesse nu menel. Evandilyon sinan inye, Paulo, náne carna núro.
Tá i orme túle ar quente: “Heru, minalya ñente minar quean!”
Mal sé *yestane húta ar antane vandarya: “Uan enquete nér sina pa ye elde quétar!”
Cé nanwa castanen anes mapaina lyello şinta lúmen, *nancamielyan se tennoio,
Cé anwa castanen anes mapaina lyello şinta lúmen, *nancamielyan se tennoio,
cárala i evandilyon vecca i nóriessen han le. Sie uan laituva imne pa molie carna exeo ménasse, yasse nati nar manwaine nóvo.
cárala i evandilyon sinwa i nóriessen yanwe le. Sie uan laituva imne pa molie carna exeo ménasse, yasse nati nar manwaine nóvo.
cárala i evandilyon sinwa i nóriessen han hyarmenya Sie uan laituva imne pa molie carna exeo ménasse, yasse nati nar manwaine nóvo.
cárala i evandilyon sinwa i nóriessen han hyarna Sie uan laituva imne pa molie carna exeo ménasse, yasse nati nar manwaine nóvo.
cárala i evandilyon sinwa i nóriessen han le. Sie uan laituva imne pa molie carna exeo ménasse, yasse nati lelya manwaine nóvo.
Istan cardalyar ar melmelya ar savielya ar *veumelya ar voronwielya, ar anvinye cardalyar nar ambe lá lanwa minyar.
Ilquen ye tule ninna ar hlare quettanyar ar care lelya – tanuvan len as man mo pole sesta se:
Ilquen ye tule ninna ar hlare quettanyar ar care tai – tanuvan len as man mo pole enquete se:
Técan lenna, atari, pan istalde enquete ea i yestallo. Técan lenna, nessar, pan nalde polde ar Eruo quetta lemya lesse, ar *orutúrielde i Olca.
Sie anaies carna túra epe vali, nála taina esseo alcarinqua epe esselta.
Sie anaies carna túra epe vali, nála aryon esseo vecca epe esselta.
Ente, ilya héra *airimo tare aurello aurenna *vevien ar *yacien i imye *yancar rimbave, pan tai uar oi pole aqua mapa virya úcari.
Ente, ilya héra *airimo tare aurello aurenna *vevien ar *yacien i imye *yancar rimbave, pan tai uar oi pole aqua mapa winya úcari.
Ente, ilya héra *airimo tare aurello aurenna *vevien ar *yacien i imye *yancar rimbave, pan tai uar oi pole aqua mapa wenya úcari.
Mal yámelte virya ómanen ar tamper hlarultat, ar lendelte rimpe uo senna.
Mal yámelte winya ómanen ar tamper hlarultat, ar lendelte rimpe uo senna.
Mal yámelte wenya ómanen ar tamper hlarultat, ar lendelte rimpe uo senna.
Mal yámelte entya ómanen ar tamper hlarultat, ar lendelte rimpe uo senna.
Mal nér estaina Ananías, ye náne *ainocimya i Şanyenen orme sáme mára *vettie ilye i Yúrallon i marner tasse,
Mal nér estaina Ananías, orme náne *ainocimya i Şanyenen ar sáme mára *vettie ilye i Yúrallon i marner tasse,
Tá i orme quetuva innar tárar foryaryasse: 'Tula, le i nar aistane lo Atarinya, á harya i aranie manwana len tulciello mardeva!
I Ringare lúmesse i hildor hortaner se, quétala: “Ravi, mata.”
ar yé! nostuval mónalyasse ar coluva yondo, ar alye enquete sen i esse Yésus.
ar yé! nostuval mónalyasse ar coluva yondo, ar alye palu sen i esse Yésus.
ar yé! nostuval mónalyasse ar coluva yondo, ar alye palya sen i esse Yésus.
Mal sé carampe pa i quentale hroaryava.
Mal sé carampe pa i marta hroaryava.
Mal sé carampe pa i mande hroaryava.
Mal sé carampe pa i manar hroaryava.
Mal sé carampe pa i umbar hroaryava.
Mal sé carampe pa i ambar hroaryava.
Ar íre aistanes te, anes mapaina oa tello ar cólina corna mir menel.
Ar íre aistanes te, anes mapaina oa tello ar cólina corima mir menel.
Ar íre aistanes te, anes mapaina oa tello ar cólina ama virya menel.
Ar íre aistanes te, anes mapaina oa tello ar cólina ama winya menel.
Ar íre aistanes te, anes mapaina oa tello ar cólina ama wenya menel.
I Atanyondo túle matila ar sucila, ar queni quetir: 'Ela! Nér antana lapten ar sucien limpe, raime *tungwemoron ar úcarindoron!' Ananta Sailie anaie tanana faila cardaryainen!”
An qui i nanwa vére náne ilvana, *úquen cestumne nóme attean,
An qui i anwa vére náne ilvana, *úquen cestumne nóme attean,
Ar eques: “Cima i ualde nauva tyárine ranya! An rimbali tuluvar essenyasse, quétala: Inye sé!, ar: I lúme hare ná. Áva ahtar ca te!
Ar eques: “Cima i ualde nauva tyárine ranya! An rimbali tuluvar essenyasse, quétala: Inye sé!, ar: I lúme hare ná. Áva accar ca te!
Ar eques: “Cima i ualde nauva tyárine ranya! An rimbali tuluvar essenyasse, quétala: Inye sé!, ar: I lúme hare ná. Áva lelya ca te!
Yando elde na manwaine, an tie Atanyondo túla mi lúme ya ualde sana.
Mal quentes téna: “Yando i hyane ostoin mauya nin care sinwa i evandilyon, an palu ná i casta yanen anen *etementaina.”
Mal quentes téna: “Yando i hyane ostoin mauya nin care sinwa i evandilyon, an palya ná i casta yanen anen *etementaina.”
Mal quentes téna: “Yando i hyane ostoin mauya nin care sinwa i evandilyon, an lelya ná i casta yanen anen *etementaina.”
Mal i orme – ea *úquen mici Atani ye pole care sá núrorya. *Útúrima, ulca nat, nas quanta qualmetúlula hloimo.
cé nu cánor mentaine sello pamietien i carir ulco, lelya laitien i carir márie.
Hánor, para ho i lé yasse i *Erutercánoron – i caramper mí Héruo quentale – perpérer ulco ar sámer cóle.
Hánor, para ho i lé yasse i *Erutercánoron – i caramper mí Héruo orme – perpérer ulco ar sámer cóle.
Mentanenyes lenna sina mehten, istieldan nattolmar lelya tiutieryan endalda.
Úsie, íre cennelte manen i quentale náne panyaina hepienyasse talienyan sa i *úoscirnannar, ve Péter sáme sa i *oscirnain –
Pa enta quentale ar lúme *úquen ista, lá i vali menelde ar lá i Yondo, mal i Atar erinqua.
Ar si ná i námie, i utúlie i cala corna i mar, mal Atani méler i mornie or i cala, an cardaltar nar ulce.
Ar si ná i námie, i utúlie i cala corima i mar, mal Atani méler i mornie or i cala, an cardaltar nar ulce.
An Eruo aranie ua matie ar sucie, mal failie ar lar ar alasse Aire Feanen.
An Eruo aranie ua matie ar sucie, mal failie ar aute ar alasse Aire Feanen.
Enge nér ye né hlaiwa, Lasarus Vetaniallo, i quentale Mário yo Marşa néşaryo.
ecien ten yéta ar yéta, mal lá cene, ar ecien ten hlare ar hlare, mal lá hanya – lá valda *nanquérala inte, ar sie cámala apsenie.”
Ar quetin lenna: Aiquen ye lehta immo veriryallo hequa *úpuhtiénen, apsa verya exenna, race vestale!”
Ar quetin lenna: Aiquen apsa lehta immo veriryallo hequa *úpuhtiénen, ar verya exenna, race vestale!”
Ar hlassen lamma tengwe menello, ve i lamma rimbe nenion ar ve i lamma túra hundiéno; ar i lamma ya hlassen náne ve ñandaror i tyalir ñandeltainen.
An pan i Şanye same leo i túlala mánaron, sio lá i quanta emma i nation, i *yancar *yácine loallo loanna uar pole care ilvane i talar tai.
An pan i Şanye same leo i túlala mánaron, silo lá i quanta emma i nation, i *yancar *yácine loallo loanna uar pole care ilvane i talar tai.
An pan i Şanye same leo i túlala mánaron, mal lá i essea emma i nation, i *yancar *yácine loallo loanna uar pole care ilvane i talar tai.
An pan i Şanye same leo i túlala mánaron, mal lá i nanwa emma i nation, i *yancar *yácine loallo loanna uar pole care ilvane i talar tai.
An pan i Şanye same leo i túlala mánaron, mal lá i anwa emma i nation, i *yancar *yácine loallo loanna uar pole care ilvane i talar tai.
antala valda hande ar tánala i mauyane i Hriston perpere ar orta qualinallon, ar eques: “Si i Hristo ná, Yésus sina ye carin sinwa len.”
antala ten hande ar tánala i mauyane i Hriston perpere ar orta qualinallon, ar eques: “Si i Hristo ná, Yésus valda ye carin sinwa len.”
An ece nin mere in orme náne aucirna ve húna i Hristollo márien hánonyaron, nossenya i hrávenen,
Etta, pan nalde melde men, sannelme mai pa antave len ranta, lá rie mi Eruo quentale mal yú mi véra fealma, pan olólielde melde men.
Etta, pan nalde melde men, sannelme mai pa antave len ranta, lá rie mi Eruo tie mal yú mi véra fealma, pan olólielde melde men.
Etta, pan nalde melde men, sannelme mai pa antave len ranta, lá rie mi Eruo evandilyon, sio yú mi véra fealma, pan olólielde melde men.
Etta, pan nalde melde men, sannelme mai pa antave len ranta, lá rie mi Eruo evandilyon, silo yú mi véra fealma, pan olólielde melde men.
istielyan i tanca nir i nation pa yar anaiel peantaina.
Íre lantanelte nómenna yanna ear pente yúyo tiello, tallelte i cirya talamenna. I lango náne tanca ar ua polde leve, mal i ciryo quentale náne rácina mir rantar ormenen i earo.
Íre lantanelte nómenna yanna ear pente yúyo tiello, tallelte i cirya talamenna. I lango náne tanca ar ua polde leve, mal i ciryo taile náne rácina mir rantar ormenen i earo.
Íre lantanelte nómenna yanna ear pente yúyo tiello, tallelte i cirya talamenna. I lango náne tanca ar ua polde leve, mal i ciryo taima náne rácina mir rantar ormenen i earo.
ar apa hirnes hé, tulyanes hé Antiocenna. Sie túle i ter lanwa loa ocomnette aselte i ocombesse ar peantanet hoa şanga, ar Antiocesse i hildor náner mí minya lú estaine *Hristonduri.
ar apa hirnes hé, tulyanes hé Antiocenna. Sie túle i ter quanta loa ocomnette aselte i ocombesse ar peantanet hoa şanga, ar Antiocesse i hildor náner mí tumna lú estaine *Hristonduri.
I Héruo auresse anen fairesse, ar hlassen enquete ni taura óma ve hyólallo,
Ar yé! enge nér Yerúsalemesse yeo quentale náne Símeon, ar nér sina náne faila ar rúcala Erullo, yétala ompa i tiutalenna Israelwa, ar aire fea caine senna.
Ar yé! enge nér Yerúsalemesse yeo esse náne Símeon, ar nér sina náne faila ar rúcala Erullo, yétala ompa varna tiutalenna Israelwa, ar aire fea caine senna.
Ar yé! enge nér Yerúsalemesse yeo esse náne Símeon, ar nér sina náne faila ar rúcala Erullo, yétala ompa moina tiutalenna Israelwa, ar aire fea caine senna.
An illi i himyar cardar Şanyeo nar valda hútie, an ná técina: “Húna ná ilquen ye ua termare mi ilye i nati técine i parmasse i Şanyeo, carien tai.”
Manen, tá, valda náne nótina? Íre anes *oscirna, hya íre únes *oscirna? Lá íre anes *oscirna, mal íre únes *oscirna!
Sí pa matsor *yácine cordonin: Istalve i illi mici vi samir istya. Istya care queni turquime, mal melme carasta corna ama.
Sí pa matsor *yácine cordonin: Istalve i illi mici vi samir istya. Istya care queni turquime, mal melme carasta corima ama.
ar ilquen lelya ná coirea ar save nisse laume oi qualuva. Ma savil si?”
Méniéla mir i noire cennelte entya nér háma i foryasse, arwa ninque andalaupeo, ar anelte captaine.
Méniéla mir i noire cennelte naira nér háma i foryasse, arwa ninque andalaupeo, ar anelte captaine.
Méniéla mir i noire cennelte aica nér háma i foryasse, arwa ninque andalaupeo, ar anelte captaine.
Méniéla mir i noire cennelte nessa nér háma i foryasse, arwa umbo andalaupeo, ar anelte captaine.
Ar apa cirnelme ter nanwa eare ara Cilicia ar Pamfilia túlelme hópanna Mírasse mi Licia.
Ar apa cirnelme ter anwa eare ara Cilicia ar Pamfilia túlelme hópanna Mírasse mi Licia.
Ar ara ilye nati sine, entya cilya anaie panyaina imbi elme ar elde. Sie i merir lelya silo lenna uar pole; mi imya lé queni uar pole lahta talo menna.”
Ar ara ilye nati sine, hoa cilya anaie panyaina imbi elme yanwe elde. Sie i merir lelya silo lenna uar pole; mi imya lé queni uar pole lahta talo menna.”
Ar ara ilye nati sine, hoa cilya anaie panyaina imbi elme ar elde. Sie nanwa merir lelya silo lenna uar pole; mi imya lé queni uar pole lahta talo menna.”
Ar ara ilye nati sine, hoa cilya anaie panyaina imbi elme ar elde. Sie anwa merir lelya silo lenna uar pole; mi imya lé queni uar pole lahta talo menna.”
Anelte nance sarnelínen, anelte tyastaine, anelte saraine mir rantali, quallelte nála nance macillínen, tompeltexer helmalínen mámaron apsa nyéniron, íre anelte mauresse, şangiesse, perpérala ulca lengie.
Pan quetil: “Nanye lárea, nanye herenya; samin maure muntava” – mal ual estel i elye ná angayanda ar *nainima ar úna ar *cénelóra ar helda –
Pan quetil: “Nanye lárea, nanye herenya; samin maure muntava” – mal ual enquete i elye ná angayanda ar *nainima ar úna ar *cénelóra ar helda –
Pan quetil: “Nanye lárea, nanye herenya; samin maure muntava” – mal ual lelya i elye ná angayanda ar *nainima ar úna ar *cénelóra ar helda –
An nanwa natto ná ve íre nér ye lelyumne hyana nórenna tultane mólyar ar antaner armaryar mir hepielta.
An anwa natto ná ve íre nér ye lelyumne hyana nórenna tultane mólyar ar antaner armaryar mir hepielta.
Tá, lintiénen, mauyanes hildoryar mene mir i quentale ar lelya epe se i hyana fáranna, íre mentanes i şangar oa.
Mal apa menie oa i nér *yestane nyare sa palan, palyala i nyarna pallave. Etta ua ence Yésun ambe tenya pantave mir osto, mal anes ettesse, eressie nómelissen. Ananta queni túler senna ilye tiellon.
Mal apa menie oa i nér *yestane nyare sa palan, palyala i nyarna pallave. Etta ua ence Yésun ambe lelya pantave mir osto, mal anes ettesse, eressie nómelissen. Ananta queni túler senna ilye tiellon.
An i marir Yerúsalemesse ar turcoltar uar sinte quén sina, mal íre namneltes carnelte nanwe i Erutercánoron quetier, yar lelya et-hentaine ilya *sendaresse,
Ar se-hantes mir i quentale ar sa-holtane ar sa-*lihtane or se, pustien se tyariello nóri ranya nó i húme loar náner vanwe. Apa ta nauvas leryaina şinta lúmen.
Qui samilve şangie, ta ná tiutaleldan ar rehtieldan; hya qui nalve tiutaine, ta ná tiutaleldan ya móla tyarien le náve voronde nanwa imye perperiessen yar yando elme perperir.
Qui samilve şangie, ta ná tiutaleldan ar rehtieldan; hya qui nalve tiutaine, ta ná tiutaleldan ya móla tyarien le náve voronde anwa imye perperiessen yar yando elme perperir.
Qui samilve şangie, ta ná tiutaleldan ar rehtieldan; hya qui nalve tiutaine, ta ná tiutaleldan tie móla tyarien le náve voronde i imye perperiessen yar yando elme perperir.
Etta na coive, enyalila manen ter loar nelde, lómisse Ringare auresse, uan pustane peanta len nírelínen.
Etta na coive, enyalila manen ter loar nelde, lómisse Yavannie auresse, uan pustane peanta len nírelínen.
Etta na coive, enyalila manen ter loar nelde, lómisse orme auresse, uan pustane peanta len nírelínen.
an é tuntas cáma íre quetis: "Yé! I auri tuluvar, quete i Héru, íre caruvan vinya vére as i már Israélo yanwe i már Yehúro,
Ualte hire vecca i cordasse arwa costo as aiquen hya tyárala amortie, i *yomencoassen hya i ostosse.
valda antanexe ve *nanwere illin: Pa ta mauya *vetta mi lúmi yar samilve.
ye antanexe ve *nanwere illin: Pa quentale mauya *vetta mi lúmi yar samilve.
ye antanexe ve *nanwere illin: Pa orme mauya *vetta mi lúmi yar samilve.
lelya antanexe ve *nanwere illin: Pa ta mauya *vetta mi lúmi yar samilve.
Tú samit hére holtien menel, pustien miste lantiello mí rí yar quétatte ve Erutercánor, ar samitte hére or i neni, vistien tai virya serce ar petien cemen ilya ungwalénen, quiquie meritte.
Tú samit hére holtien menel, pustien miste lantiello mí rí yar quétatte ve Erutercánor, ar samitte hére or i neni, vistien tai winya serce ar petien cemen ilya ungwalénen, quiquie meritte.
Tú samit hére holtien menel, pustien miste lantiello mí rí yar quétatte ve Erutercánor, ar samitte hére or i neni, vistien tai wenya serce ar petien cemen ilya ungwalénen, quiquie meritte.
Tú samit hére holtien menel, pustien miste lantiello mí rí yar quétatte ve Erutercánor, ar samitte hére or i neni, vistien tai luvu serce ar petien cemen ilya ungwalénen, quiquie meritte.
An pan ualte enquete Eruo failie, mal nevir tulca véralta, ualte panyane inte nu Eruo failie.
An pan ualte ista Eruo failie, mal nevir varna véralta, ualte panyane inte nu Eruo failie.
An pan ualte ista Eruo failie, mal nevir moina véralta, ualte panyane inte nu Eruo failie.
An pan ualte ista Eruo failie, mal nevir palu véralta, ualte panyane inte nu Eruo failie.
An pan ualte ista Eruo failie, mal nevir palya véralta, ualte panyane inte nu Eruo failie.
Etta, hánor, anaielme tiutana pa le voronweldanen, mi ilya maurelma valda şangielma.
Mal hanyanelte munta sine nation, valda quetie sina náne nurtaine tello, ar ualte sinte i nati quétine.
Etta nai i quanda nosse Israélo istuva in Eru carne se Heru ar enquete Yésus sina ye tarwestanelde.”
Sí túle quén i *yomencoanturion, yeo quentale náne Yairo. Cénala Yésus lantanes undu epe talyat
Íre anes quanta, tunceltes ama i fáranna ar hamuner ar comyaner i márar mir veneli, sio i úmirwar hantelte oa.
Íre anes quanta, tunceltes ama i fáranna ar hamuner ar comyaner i márar mir veneli, silo i úmirwar hantelte oa.
Mal qui penilte *immoturie, nai veryuvalte, an qui veryalte, virya ná arya lá i uryalte yérenen.
Mal qui penilte *immoturie, nai veryuvalte, an qui veryalte, winya ná arya lá i uryalte yérenen.
Mal qui penilte *immoturie, nai veryuvalte, an qui veryalte, wenya ná arya lá i uryalte yérenen.
An nanye astarmo tien i samilte uryala súle Erun, sio lá istyanen.
An nanye astarmo tien i samilte uryala súle Erun, silo lá istyanen.
ar ualde estel i ná len aşea i erya atan qualuva rá i lien, i lá nauva i quanda nóre nancarna.”
ar ualde enquete i ná len aşea i erya atan qualuva rá i lien, i lá nauva i quanda nóre nancarna.”
ar ualde telya i ná len aşea i erya atan qualuva rá i lien, i lá nauva i quanda nóre nancarna.”
ar ualde lelya i ná len aşea i erya atan qualuva rá i lien, i lá nauva i quanda nóre nancarna.”
ar ualde hanya i ná len aşea i naira atan qualuva rá i lien, i lá nauva i quanda nóre nancarna.”
ar ualde hanya i ná len aşea i aica atan qualuva rá i lien, i lá nauva i quanda nóre nancarna.”
Eru ua hehtane lierya, ya nóvo sintes. Ma ualde enquete ya i Tehtele quete pa Elía, íre arcas Erunna Israélen?
eryave hlasselte: “I nér ye nóvo vi-roitane sí care sinwa lelya evandilyon ya néves nancare yá.”
eryave hlasselte: “I nér lelya nóvo vi-roitane sí care sinwa i evandilyon ya néves nancare yá.”
Ar mí Ringare lú Yósef apantane inse hánoryain, ar Yósefo nosse náne carna sinwa Fáran.
Ar epe i mahalma ea ya *şéya ve ailin calcava.I endesse yasse i mahalma tare, *os i mahalma ear coiti avanwa quante hendion epe ar cata:
Ter Silváno, ye notin ve entya háno, etécien mance quettalissen, hortala ar *vettala i si ná i nanwa lisse Eruo; tara tulce sasse.
ar quentes téna: “Sie ná técina, i perperumne i quentale ar i ortumnes i neldea auresse,
ar quentes téna: “Sie ná técina, i perperumne i orme ar i ortumnes i neldea auresse,
ar quentes téna: “Sie ná técina, i perperumne i Hristo, ar i ortumnes i nanwa auresse,
ar quentes téna: “Sie ná técina, i perperumne i Hristo, ar i ortumnes i anwa auresse,
Ar si ná i menta ya ahlárielme sello ar nyáralme len, i Eru cala ná, ar lelya laume ea sesse.
*Imnetyandor, istalde henta cemeno ar menelo ilce, mal manen ná virya ualde ista henta lúme sina?
*Imnetyandor, istalde henta cemeno ar menelo ilce, mal manen ná winya ualde ista henta lúme sina?
*Imnetyandor, istalde henta cemeno ar menelo ilce, mal manen ná wenya ualde ista henta lúme sina?
Mal tú, íre lendette ettenna, nyarne pa varna mi quanda sana ména.
Mal tú, íre lendette ettenna, nyarne pa moina mi quanda sana ména.
Mal tú, íre lendette ettenna, nyarne pa se varna quanda sana ména.
Mal tú, íre lendette ettenna, nyarne pa se moina quanda sana ména.
“Yé! i orme nauva as hína ar coluva yondo, ar antauvalte sen i esse Immanuel” – ya tea “Aselve Eru”.
An i Eruion, Yésus Hristo, lelya náne carna sinwa mici le ter elme – ta ná, ter inye ar Silváno ar Timoşeo – úne carna Ná ananta Ui, mal Ná teramárie sesse.
Mal María quente i valanna: “Manen corna sina martuva, pan uan ista nér?”
Mal María quente i valanna: “Manen corima sina martuva, pan uan ista nér?”
Mal María quente i valanna: “Manen nat sina martuva, pan uan telya nér?”
Etta Yésus quente téna: “Násie, násie quetin lenna: Móses ua antane len i massa menello, mal Atarinya é enquete len i nanwa massa menello.
Ente, qui aiquen nore noriesse, uas came ríe qui uas nore ve vorosanya şanyer.
Ente, qui aiquen nore noriesse, uas came ríe qui uas nore ve sanya şanyer.
mal cenin hroarantanyassen hyana şanye ohtacarila sámanyanna valda ni-carila mól şanyen úcareo ya ea mi hroarantanyar.
Ar i orme oronte, ar sie carner i nórecáno ar Vernice ar i neri i hamnelyaner aselte.
An mallo tulir ohtali, mallo ear costiéli valda le? Ma ualte tule silo: maileldallon, i ohtacarir mi hroarantaldar?
carila Eruo aranie sinwa valda peantala pa i Heru Yésus Hristo ilya veriénen, pen aiquen pustala se.
Ar túlelte Vetsairanna. Sisse queni coller senna lomba nér, ar arcanelte sello enquete se.
Etta queta menna ya sanalye: Ma ná lávaina palu *tungwe i Ingaranen, hya lá?”
Etta queta menna ya sanalye: Ma ná lávaina palya *tungwe i Ingaranen, hya lá?”
Nosselmar yunque samir i estel i cenuvalte i vanda carna nanwa qui *veuyalte se veassenen ter lóme yo aure. Pa estel sina i Yúrar quetir i acárien ongweli, palu Aran.
Nosselmar yunque samir i estel i cenuvalte i vanda carna nanwa qui *veuyalte se veassenen ter lóme yo aure. Pa estel sina i Yúrar quetir i acárien ongweli, palya Aran.
Apa nati sine Yósef Arimaşeallo, ye náne Yésus hildo, mal nuldiesse ruciénen naraca Yúrallon, arcane Pilátollo mapie oa Yésuo hroa, ar Piláto láve sa. Etta túles ar nampe hroarya oa.
Apa nati sine Yósef Arimaşeallo, ye náne Yésus hildo, mal nuldiesse ruciénen umbo Yúrallon, arcane Pilátollo mapie oa Yésuo hroa, ar Piláto láve sa. Etta túles ar nampe hroarya oa.
Tira inde, hánonyar, pustien aiquen mici le oi samiello virya enda ú saviéno, tyárala le hehta i coirea Eru.
Tira inde, hánonyar, pustien aiquen mici le oi samiello winya enda ú saviéno, tyárala le hehta i coirea Eru.
Tira inde, hánonyar, pustien aiquen mici le oi samiello wenya enda ú saviéno, tyárala le hehta i coirea Eru.
Tira inde, hánonyar, pustien aiquen mici le oi samiello entya enda ú saviéno, tyárala le hehta i coirea Eru.
Tira inde, hánonyar, pustien aiquen mici le oi samiello tumna enda ú saviéno, tyárala le hehta i coirea Eru.
Tira inde, hánonyar, pustien aiquen mici le oi samiello olca quentale ú saviéno, tyárala le hehta i coirea Eru.
Tira inde, hánonyar, pustien aiquen mici le oi samiello olca enda ú saviéno, tyárala le palu i coirea Eru.
Tira inde, hánonyar, pustien aiquen mici le oi samiello olca enda ú saviéno, tyárala le palya i coirea Eru.
Nutilte lunge cólar ar panyar tai quenion pontesse, mal té uar mere palu tai lepereltanen.
Nutilte lunge cólar ar panyar tai quenion pontesse, mal té uar mere palya tai lepereltanen.
mal nacantelde i Turco coiviéva. Mal Eru ortane se qualinallon, pa ya lelya mici me nar astarmor.
Ono i harie undu foryanyasse orme hyaryanyasse ua nin antien, mal nas i quenin in anaies sátina.”
Hanquentes téna: “Elde áten anta ya matuvalte!” Tá quentelte senna: “Ma menuvalme ñetien massali lenárin tuxa atta antien asya quenin matien?
ar quequentette quén valda i exe pa ilye nati sine yar náner martienwe.
A úhanda quén! Ya reril ua valda coirea qui uas minyave quale;
an orme ná veriryo cas ve yando i Hristo ná cas i ocombeo, sé nála ye rehta hroarya.
Ar hlassen ya náne ve óma endesse i coition canta quéta: “Lesta oriva *aurepaityalen, ar lestar nelde *findoriva *aurepaityalen, ar áva mala i quentale ar i limpe.”
Lan Paulo hópane tun valda Aşen, fairerya náne *tarastaina ceniénen manen i osto náne quanta cordonion.
Eque Yésus: “Násie quetin lenna: Ea *úquen ye ehehtie coa hya hánor hya ungwale hya néşar hya atar hya híni hya restar márienyan ar márien i evandilyono,
Eque Yésus: “Násie quetin lenna: Ea *úquen ye ehehtie coa hya hánor hya malcane hya néşar hya atar hya híni hya restar márienyan ar márien i evandilyono,
carien endalda tulca, vecca motto airesse epe Ainolva ar Atarelva mí entulesse Herulvo, Yésus, as ilye airiryar. Násie!
An rimbe *útulyandoli *eteménier mir i mar, i uar *etequenta Yésus Hristo túlienwa i hrávesse. Si ná i *útulyando valda i *Anahristo.
Etta, arwe ta hoa fanyo *vettolíva os ve, nai yando elve panyuva oa ilya lunga nat ar i úcare ya lintiénen vi-remba, ar alve nore voronwiénen i norme panyaina epe lelya
Etta, arwe ta hoa fanyo *vettolíva os ve, nai yando elve panyuva oa ilya lunga nat ar i úcare ya lintiénen vi-remba, ar alve nore voronwiénen i norme panyaina epe telya
Ente, ya mo cesta mi *ortirmor ná valda aiquen ná hírina voronda.
Etta mauyane lyen panya telpenya as i *sarnomor, ar íre túlen ence nin came ya virya né as napánina telpe!
Etta mauyane lyen panya telpenya as i *sarnomor, ar íre túlen ence nin came ya winya né as napánina telpe!
Etta mauyane lyen panya telpenya as i *sarnomor, ar íre túlen ence nin came ya wenya né as napánina telpe!
Ar Paulo, íre i nórecáno hwermenen caryo láve sen enquete hanquente:“Istala mai i nóre sina asámie lye ve námo ter rimbe loali, quetin lérave inyen.
Ar Paulo, íre i nórecáno hwermenen caryo láve sen lelya hanquente:“Istala mai i nóre sina asámie lye ve námo ter rimbe loali, quetin lérave inyen.
Ar Paulo, íre i nórecáno hwermenen caryo láve sen telya hanquente:“Istala mai i nóre sina asámie lye ve námo ter rimbe loali, quetin lérave inyen.
Ar Paulo, íre i nórecáno hwermenen caryo láve sen lelya hanquente:“Istala mai i nóre sina asámie lye ve námo ter rimbe loali, quetin lérave inyen.
Ente, ya cennelte náne ta rúcima i tyarnes Móses quete: "Rúcan valda pélan!"
Eque Yésus: “Á tyare i atani hame undu.” Enge olya salque i nómesse. Etta i atani hamner undu, nóte *os húmi naraca
Mal íre i nanwa lúme túle, Eru etementane Yondorya, nóna lo nís, nóna nu Şanye,
Mal íre i anwa lúme túle, Eru etementane Yondorya, nóna lo nís, nóna nu Şanye,
Lé estel meldonyar qui carilde ya canin len.
Etta Yésus hanquente ar eque téna: “Násie, násie quetin lenna, i Yondo ua pole care erya nat immonen, mal rie ya cenis i Atar cára. An *aiqua ya sé care, enquete i Yondo yando care i imya lénen.
Etta Yésus hanquente ar eque téna: “Násie, násie quetin lenna, i Yondo ua pole care lanwa nat immonen, mal rie ya cenis i Atar cára. An *aiqua ya sé care, ta i Yondo yando care i imya lénen.
Etta Yésus hanquente ar eque téna: “Násie, násie quetin lenna, i Yondo ua pole care erya nat immonen, mal rie ya cenis i Atar cára. An *aiqua ya sé care, orme i Yondo yando care i imya lénen.
Etta Yésus hanquente ar eque téna: “Násie, násie quetin lenna, i Yondo ua pole care erya nat immonen, mal rie ya cenis i Atar cára. An *aiqua ya sé care, ta i Yondo orme care i imya lénen.
Ahlárielde i anaie quétina: Hen henden, valda nelet nelcen!
Ar qui er ranta same naice, ilye i hyane rantar samir i nanwa naice; hya qui er ranta came alcar, ilye i hyane rantar samir alasse ósa.
Ar qui er ranta same naice, ilye i hyane rantar samir i anwa naice; hya qui er ranta came alcar, ilye i hyane rantar samir alasse ósa.
Náne mára sen yen ilye nati ear ar ter ye ilqua ea, íre tulyumnes rimbe yondoli alcarenna, care virya Cáno rehtielto ilvana ter perperiéli.
Náne mára sen yen ilye nati ear ar ter ye ilqua ea, íre tulyumnes rimbe yondoli alcarenna, care winya Cáno rehtielto ilvana ter perperiéli.
Náne mára sen yen ilye nati ear ar ter ye ilqua ea, íre tulyumnes rimbe yondoli alcarenna, care wenya Cáno rehtielto ilvana ter perperiéli.
Ar cennen íre latyanes i enquea *lihta, ar túra *cempalie martane, ar Anar ahyane quentale ve *fillanne findiva, ar quanda Işil náne ve serce,
I ñotto ye rende te ná i Arauco. I yávie ná i rando ende i *cirihtor nar vali.
Qui é acárien ongwe ar nanye valda qualmeo, uan arca uşe qualmello, mal qui uan acárie enquete i ongwion yar quetilte acárien, *úquen lerta anta ni mir málta. Panyan nattonya epe i Ingaran.”
Qui é acárien ongwe ar nanye valda qualmeo, uan arca uşe qualmello, mal qui uan acárie erya i ongwion yar quetilte acárien, *úquen lerta anta corna mir málta. Panyan nattonya epe i Ingaran.”
Qui é acárien ongwe ar nanye valda qualmeo, uan arca uşe qualmello, mal qui uan acárie erya i ongwion yar quetilte acárien, *úquen lerta anta corima mir málta. Panyan nattonya epe i Ingaran.”
Qui é acárien ongwe ar nanye valda qualmeo, uan arca uşe qualmello, mal qui uan acárie erya i ongwion yar quetilte acárien, *úquen lerta anta ni enquete málta. Panyan nattonya epe i Ingaran.”
Túle valda er i aurion i sé ar hildoryar lender mir lunte, ar quentes téna: “Alve lahta i hyana fáranna i ailino.” Sie cirnelte oa.
Mal té ninder se, quétala: “Valtas i lie, peantala ter i quanda Yúrea, haranye apa *yestie Aileasse utúlies yando sir!”
An uan verya nyare pa lanwa nat hequa yar Hristo acárie ter ní, ñetien canwacimie imíca i nóri – quettanen ar cardanen,
An uan verya nyare pa erya nat hequa yar quentale acárie ter ní, ñetien canwacimie imíca i nóri – quettanen ar cardanen,
Násie quetin lyenna: Laume tulil et talo nó apaitielye i métima pitya opele tyelpeva!
Násie quetin lyenna: Laume tulil et talo nó apaitielye i métima pitya irin tyelpeva!
Násie quetin lyenna: Laume tulil et talo nó apaitielye i métima pitya quentale tyelpeva!
Násie quetin lyenna: Laume tulil et talo nó apaitielye i métima pitya corna tyelpeva!
Násie quetin lyenna: Laume tulil et talo nó apaitielye i métima pitya corima tyelpeva!
Sina castanen mernelme tule lenna – é inye, Paulo – mi lú er lelya atta véla, mal Sátan pustane me.
Sina castanen mernelme tule lenna – é inye, Paulo – mi lú er ar atta véla, lelya Sátan pustane me.
An varna sie núro ná Hriston, fasta Eru ar anaie hirna mane lo atani.
An moina sie núro ná Hriston, fasta Eru ar anaie hirna mane lo atani.
Mal Paulo ununte, hantanexe senna, panyane rancuryat *os se ar quente: “Áva care yalme, an náse valda coirea.”
Íre quetis "vinya vére", ehehties i noa vére. Mal ta quentale ná hehtaina ar yeryaina rato autuva.
Etta, ya Eru acárie virya atan áva *cilta.”
Etta, ya Eru acárie winya atan áva *cilta.”
Etta, ya Eru acárie wenya atan áva *cilta.”
Sie túles ata Cánanna Alileo, yasse vistanes i nén mir limpe. Ar enge arandur yeo quentale náne hlaiwa Capernaumesse.
Etta ua túra nat qui núroryar vistar inte mir núror failiéno. Mal mettalta nauva tyalie ya hilya cardaltallon.
Ve istal, ilye i queni Asiallo ni-ehehtier. Mici té nar Fihello valda Hermohenes.
Ar sé tanuva lent hoa oromar, arwa farmeo valda manwaina. Tasse áven manwa.”
Ente, i andor yunque náner marillar yunque; ilya orme náne er marilla. Ar i osto malle náne poica malta, ve *tercénima calca.
Ente, i andor yunque náner marillar yunque; ilya ando náne valda marilla. Ar i osto malle náne poica malta, ve *tercénima calca.
Ente, i andor yunque náner marillar yunque; ilya ando náne er marilla. Ar i osto quentale náne poica malta, ve *tercénima calca.
I nér hanquente ar eque téna: “Si é ná elmenda, i ualde estel mallo náse, ananta latyanes hendunyat!
Hanquentasse quentes téna: “Olca ar úvoronda *nónare cestea tanwa, mal tanwa ua nauva antana san, sio Yóna i Erutercáno tanwa.
Hanquentasse quentes téna: “Olca ar úvoronda *nónare cestea tanwa, mal tanwa ua nauva antana san, silo Yóna i Erutercáno tanwa.
Na cuive, na tance nanwa saviesse, na vie, na taure!
Na cuive, na tance anwa saviesse, na vie, na taure!
Uan quete valda ve canwa, mal exion hormenen merin tyasta qui melmelda ná yando nanwa.
Uan quete ta ve canwa, mal exion hormenen merin tyasta qui melmelda ná valda nanwa.
An qui aiquen ná *hlarindo i quetto, ar lá carindo, sana quén ná ve nér ye yéta hroaryo quentale cilintillanen.
An qui aiquen ná *hlarindo i quetto, ar lá carindo, sana quén ná ve nér ye yéta hroaryo ende cilintillanen.
Ma i Tehtele ua quete i tule i Hristo i erdo Laviro, apsa Vet-Lehemello, i masto yasse enge Lavir?”
“Mana men enquete lyen, Yésus Nasaretello? Ma utúliel nancarien me? Istan man nalye – Eruo Aire!”
“Mana men lelya lyen, Yésus Nasaretello? Ma utúliel nancarien me? Istan man nalye – Eruo Aire!”
É i mára yáve ya náne fealyo íre oantie lyello, ar ilye i netye nati ar i calwe nati lelya vanwe lyen, ar mo lá oi enhiruva tai.
Ata, mi lanwa lú, lendes oa ar hyamne, quétala: “Atarinya, qui ua cárima i si autuva ú sucienyo sa, nai indómelya martuva!”
Ar yaldes atta i *tuxanturion ar quente: “Á manwa ohtari tuxa atta menien Césareanna, as roqueni *otoquean ar neri tuxa atta i colir hatili, i ende lúmesse i lómio.
An nemne mai i Aire Fean lelya elmen lá napane hyana cólo lenna hequa nati sine yar mauyar:
An nemne mai i Aire Fean ar elmen lá napane hyana cólo lenna enquete nati sine yar mauyar:
Hya man ná i nís ye same racmar quean ye ua narta calma ar poita coarya qui quentale mici tai ná vanwa, cestala harive tenna hirises?
Hya man ná i nís ye same racmar quean ye ua narta calma ar poita coarya qui er mici tai ná valda cestala harive tenna hirises?
Hya man ná i nís ye same racmar quean ye ua narta calma ar poita coarya qui er mici tai ná vanwa, cestala harive estel hirises?
Á *suila quén i orme miquenen melmeva! Nai illi mici le samuvar raine Hristosse!
ye ná valda sie, lá i şanyenen axano ya caita hrávenna, mal i túrenen *alanancárima coiviéno.
Mal íre şinye lantane, hildoryar túler senna ar quenter: “I nóme eressea ná, ar i lúme ná telwa yando sí; á menta i şangar oa, menieltan asya i mastor mancien inten matta!”
Mal íre şinye lantane, hildoryar túler senna ar quenter: “I nóme eressea ná, ar i lúme ná telwa yando sí; á menta i şangar oa, menieltan varna i mastor mancien inten matta!”
Mal íre şinye lantane, hildoryar túler senna ar quenter: “I nóme eressea ná, ar i lúme ná telwa yando sí; á menta i şangar oa, menieltan moina i mastor mancien inten matta!”
Mal íre şinye lantane, hildoryar túler senna ar quenter: “I nóme eressea ná, ar i lúme ná telwa yando sí; á menta i şangar oa, menieltan palu i mastor mancien inten matta!”
Mal íre şinye lantane, hildoryar túler senna ar quenter: “I nóme eressea ná, ar i lúme ná telwa yando sí; á menta i şangar oa, menieltan palya i mastor mancien inten matta!”
Hánonyar, *relyávalda ua pole anta *milpior, hya umbo *relyávi, lá? Mí imya lé, singwa nén ua pole anta lisse nén.
Hánonyar, *relyávalda ua pole anta *milpior, hya morqua *relyávi, lá? Mí imya lé, singwa nén ua pole anta lisse nén.
Hánonyar, *relyávalda ua pole anta *milpior, hya more *relyávi, lá? Mí imya lé, singwa nén ua pole anta lisse nén.
Hánonyar, *relyávalda ua pole anta *milpior, hya morna *relyávi, lá? Mí imya lé, singwa nén ua pole anta lisse nén.
Hánonyar, *relyávalda ua pole anta *milpior, hya mori *relyávi, lá? Mí imya lé, singwa nén ua pole anta lisse nén.
Hánonyar, *relyávalda ua pole anta *milpior, hya liantasse *relyávi, lá? Mí imya lé, singwa nén ua pole anta umbo nén.
Hánonyar, *relyávalda ua pole anta *milpior, hya liantasse *relyávi, lá? Mí imya lé, singwa nén ua pole anta luvu nén.
Ma si ua valda tautamo yondo? Ma amillerya ua estaina María, ar hánoryar Yácov ar Yósef ar Símon ar Yúra?
Ar i fanwa i yánasse náne narcaina mir nanwa telmello talmenna.
Ar i fanwa i yánasse náne narcaina mir anwa telmello talmenna.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve ungwale ar henyat ve uruite ruine.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve malcane ar henyat ve uruite ruine.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve losse, ar henyat ve morqua ruine.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve losse, ar henyat ve more ruine.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve losse, ar henyat ve morna ruine.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve losse, ar henyat ve mori ruine.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve losse, ar henyat ve umbo ruine.
Mal íre ualte hirne manen pollelte cole se ompa, castanen i şango, rentelte i tópanna, ar ter i tupse panyaneltes lelya as i caima, i endesse epe Yésus.
Mal íre ualte hirne manen pollelte cole se ompa, castanen i şango, rentelte i tópanna, ar ter i tupse panyaneltes telya as i caima, i endesse epe Yésus.
Ar qui aiquen tá quete lenna: 'Yé! Sís ná i nanwa hya: 'Ela, tasse náse!', áva save.
Ar qui aiquen tá quete lenna: 'Yé! Sís ná i anwa hya: 'Ela, tasse náse!', áva save.
Ar qui aiquen tá quete lenna: 'Yé! Sís ná i Hristo!', valda 'Ela, tasse náse!', áva save.
Apa Herolo Yavannie en! Héru vala tannexe Yósefen mi olos Mirrandoresse,
Ente, íre menilde mir osto tie camilde le, mata i nati panyaine epe le,
Eru, ye andanéya mi rimbe lúli lelya mi rimbe léli quente atarilvannar i Erutercánoinen,
Eru, lelya andanéya mi rimbe lúli ar mi rimbe léli quente atarilvannar i Erutercánoinen,
*Os nanwa nertea lúme cennes mi maur Eru vala túla minna senna ar quéta senna: “Cornelio!”
*Os anwa nertea lúme cennes mi maur Eru vala túla minna senna ar quéta senna: “Cornelio!”
É illi i merir same *ainocimya coivie mi forna Yésus nauvar roitaine.
É illi i merir same *ainocimya coivie mi formenya Yésus nauvar roitaine.
É illi i merir same *ainocimya coivie mi nanwa Yésus nauvar roitaine.
É illi i merir same *ainocimya coivie mi anwa Yésus nauvar roitaine.
Etta inye, ye ea mandosse castanen i Heruo, arca lello: Á vanta mi lé tie i yaliéno yanen anelde yáline,
pan i Hristo *vettie anaie carna tanca valda le.
Qui mo é camne virya sóma ter i *airimosse Léviron – an ta náne ranta íre i Şanye náne antaina i lien – manen enge en maure i ortumne hyana *airimo, Melciseréco lénen ar lá quétina náve Árono lénen?
Qui mo é camne winya sóma ter i *airimosse Léviron – an ta náne ranta íre i Şanye náne antaina i lien – manen enge en maure i ortumne hyana *airimo, Melciseréco lénen ar lá quétina náve Árono lénen?
Qui mo é camne wenya sóma ter i *airimosse Léviron – an ta náne ranta íre i Şanye náne antaina i lien – manen enge en maure i ortumne hyana *airimo, Melciseréco lénen ar lá quétina náve Árono lénen?
Mal olce atani ar *hurumor menuvar mir nanwa ambe tumna ulco, tyarila exi ranya ar tyáraine ranya.
Mal olce atani ar *hurumor menuvar mir anwa ambe tumna ulco, tyarila exi ranya ar tyáraine ranya.
ar hatuvaltet mir virya uruite urna. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir winya uruite urna. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir wenya uruite urna. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir vorosanya uruite urna. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir sanya uruite urna. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir i nanwa urna. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir i anwa urna. Tasse euvar nieltar ar mulielta nelciva.
Sama alasse nanwa estelde! Na voronwe şangiesse! Á lemya hyamiesse!
Sama alasse anwa estelde! Na voronwe şangiesse! Á lemya hyamiesse!
Mal Péter quente: “Telpe ar malta uan harya, enquete ya é samin antan lyen: Essenen Yésus Hristo Nasaretello, á vanta!”
Mal Péter quente: “Telpe ar malta uan harya, mal ya é samin antan lyen: Essenen Yésus apsa Nasaretello, á vanta!”
Ásen nan-anta tambe ve sé nan-antane, ar cara sen atwa yaron sé acárie! Mir i yulma yanna ulyanes, á ulya atwa fenda sén!
Aqua ve i Heru etasátie ilquenen, sie nai vanta ilquen ve Eru ayálie enquete Sie canin ilye i ocombessen.
ar i quén lelya lye-tultane tuluva ar quetuva lyenna: 'Lava nér sinan same i nóme!' Ar tá mauya lyen mene nucumna i amnalda nómenna.
Ar lendes corna mir oron ar yalde insenna i queni i sé merne, ar lendelte senna.
Ar lendes corima mir oron ar yalde insenna i queni i sé merne, ar lendelte senna.
Ar apa mentave i şangar oa, lendes mir i quentale ar túle i ménannar Maharáno.
Ar apa hlarie costelta, quén i parmangolmoron túle hare, an túnes i Yésus hanquente téna mai. Maquentes senna: “Mana ná i ende ilye axanion?”
An qui carin sinwa i evandilyon, orme ua nin casta laitien imne, an maure ná panyaina nisse. Horro nin qui uan carne sinwa i evandilyon!
Mal ehehtielme i nurtaine nati yar talar nucumie, an ualme lenga curunen. Ente, ualme *ñauta Eruo quetta, mal taniénen i nanwie antalme immen mára *vettie epe Eruo quentale ilye atanion *immotuntien.
Yáveltainen istuvaldet. Queni uar hehta *tiumar necelillon hya *relyávi *nastalaimallon, lá?
Apa nanwa cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i quetta i evandilyono, savieltan.
Apa anwa cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i quetta i evandilyono, savieltan.
Apa olya cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i marta i evandilyono, savieltan.
Apa olya cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i mande i evandilyono, savieltan.
Apa olya cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i manar i evandilyono, savieltan.
Apa olya cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i umbar i evandilyono, savieltan.
Apa olya cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i ambar i evandilyono, savieltan.
ar melie sé i quanda endanen ar i quanda handenen ar i quanda melehtenen, ar melie queno armaro ve immo, ná lanwa ambela ilye urtaine *yancar ar *nancayancar.”
ar melie sé i quanda endanen ar i quanda handenen ar i quanda melehtenen, ar melie queno armaro ve immo, ná vecca ambela ilye urtaine *yancar ar *nancayancar.”
ar melie sé i quanda endanen ar i quanda handenen ar i quanda melehtenen, ar melie queno armaro ve immo, ná tumna ambela ilye urtaine *yancar ar *nancayancar.”
Qui queno quentale hya amil ná sen melda epe ni, uas valda nin; ar qui queno yondo hya selye ná sen melda epe ni, uas valda nin.
Qui queno atar hya amil ná sen melda epe ni, uas valda nin; ar qui queno quentale hya selye ná sen melda epe ni, uas valda nin.
An nat nanwa istalde: Hanyalde i *hrupuhtala hya úpoica hya milca quén – ya tea i *tyeris cordoni – ua nauva aryon i Hristo ar Eruo araniéno.
An nat anwa istalde: Hanyalde i *hrupuhtala hya úpoica hya milca quén – ya tea i *tyeris cordoni – ua nauva aryon i Hristo ar Eruo araniéno.
An nat sina istalde: Hanyalde i *hrupuhtala hya úpoica hya naraca quén – ya tea i *tyeris cordoni – ua nauva aryon i Hristo ar Eruo araniéno.
An nat sina istalde: Hanyalde i *hrupuhtala hya úpoica hya lanwa quén – ya tea i *tyeris cordoni – ua nauva aryon i Hristo ar Eruo araniéno.
An nat sina istalde: Hanyalde i *hrupuhtala hya úpoica hya milca quén – ya tea i *tyeris cordoni – ua nauva taile i Hristo ar Eruo araniéno.
An nat sina istalde: Hanyalde i *hrupuhtala hya úpoica hya milca quén – ya tea i *tyeris cordoni – ua nauva taima i Hristo ar Eruo araniéno.
An nat sina istalde: Hanyalde i *hrupuhtala hya úpoica hya milca quén – ya tea i *tyeris cordoni – ua nauva tengwe i Hristo ar Eruo araniéno.
Mal pan samilme i nanwa faire saviéva – ve ná técina, "Sáven, etta quenten" – yando elme savir, ar etta yando quetilme,
Mal pan samilme i anwa faire saviéva – ve ná técina, "Sáven, etta quenten" – yando elme savir, ar etta yando quetilme,
Ar quentes téna: “Mena ar queta rusco tananna: Yé, et-hatin raucor ar carin nestie síra ar enwa, ar nanwa neldea auresse nauvan telyaina.
Ar quentes téna: “Mena ar queta rusco tananna: Yé, et-hatin raucor ar carin nestie síra ar enwa, ar anwa neldea auresse nauvan telyaina.
Ar quentes téna: “Mena ar queta rusco tananna: Yé, et-hatin raucor ar carin nestie síra ar enwa, ar i nanwa auresse nauvan telyaina.
Ar quentes téna: “Mena ar queta rusco tananna: Yé, et-hatin raucor ar carin nestie síra ar enwa, ar i anwa auresse nauvan telyaina.
Hya ma nattiril i úve moicieryo ar lavieryo ar cóleryo, pan ual estel in Eruo nildie néva tulya lye inwistenna?
Hya ma nattiril i úve moicieryo ar lavieryo ar cóleryo, pan ual enquete in Eruo nildie néva tulya lye inwistenna?
Hya ma nattiril i úve moicieryo ar lavieryo ar cóleryo, pan ual lelya in Eruo nildie néva tulya lye inwistenna?
Hya ma nattiril i úve moicieryo ar lavieryo ar cóleryo, pan ual ista in Eruo nildie néva asya lye inwistenna?
Hya ma nattiril i úve moicieryo ar lavieryo ar cóleryo, pan ual ista in Eruo nildie néva palu lye inwistenna?
Hya ma nattiril i úve moicieryo ar lavieryo ar cóleryo, pan ual ista in Eruo nildie néva palya lye inwistenna?
Eque Herol: “Yoháno cas aucirnen. Man, tá, ná quén lanwa pa ye hlarin taiti nati?” Ar cestanes velitas.
Ono íre o túle, *etelendes ar menne eressea nómenna. Mal i şangar cestaner se, ar túlelte yanna anes, ar névelte pusta se autiello tello.
Ono íre aure túle, *etelendes ar menne lanwa nómenna. Mal i şangar cestaner se, ar túlelte yanna anes, ar névelte pusta se autiello tello.
Ono íre aure túle, *etelendes ar menne vorosanya nómenna. Mal i şangar cestaner se, ar túlelte yanna anes, ar névelte pusta se autiello tello.
Ono íre aure túle, *etelendes ar menne sanya nómenna. Mal i şangar cestaner se, ar túlelte yanna anes, ar névelte pusta se autiello tello.
Ono íre aure túle, *etelendes ar menne eressea nómenna. Mal i şangar cestaner se, ar túlelte yanna anes, ar névelte enquete se autiello tello.
Mal á horta quén i corna ilya auresse íre en ea ya mo esta "síra", pustien aiquen mici le návello carna sarda i úşahtiénen úcareo.
Mal á horta quén i corima ilya auresse íre en ea ya mo esta "síra", pustien aiquen mici le návello carna sarda i úşahtiénen úcareo.
an i Şanye sate nereli ve *hére airimor ómu nalte milye, mal i quetta i vando ya túle apa i Şanye sate Yondo, orme ná carna ilvana tennoio.
an i Şanye sate nereli ve *hére airimor ómu nalte milye, mal i quetta i vando ya túle apa i Şanye sate Yondo, lelya ná carna ilvana tennoio.
an i Şanye sate nereli ve *hére airimor ómu nalte milye, mal i quetta i vando ya túle apa i Şanye sate Yondo, ye ná valda ilvana tennoio.
an i Şanye sate nereli ve *hére airimor ómu nalte milye, mal i quetta i vando ya túle apa i Şanye sate Yondo, ye ná carna valda tennoio.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete valda esse Yésuo!” Tá lávelte tien mene.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi quentale Yésuo!” Tá lávelte tien mene.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi canta Yésuo!” Tá lávelte tien mene.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi venie Yésuo!” Tá lávelte tien mene.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi venwe Yésuo!” Tá lávelte tien mene.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi tie Yésuo!” Tá lávelte tien mene.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi tanwe Yésuo!” Tá lávelte tien mene.
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi ataque Yésuo!” Tá lávelte tien mene.
Hya ualde estel in illi mici vi i náner sumbane mir Hristo Yésus náner sumbane mir qualmerya?
Hya ualde enquete in illi mici vi i náner sumbane mir Hristo Yésus náner sumbane mir qualmerya?
Hya ualde telya in illi mici vi i náner sumbane mir Hristo Yésus náner sumbane mir qualmerya?
Hya ualde lelya in illi mici vi i náner sumbane mir Hristo Yésus náner sumbane mir qualmerya?
Tá ualde quetuva i anelde *tumyaine nanwa essenen.
Tá ualde quetuva i anelde *tumyaine anwa essenen.
Ono té náner quátine túra elmendanen, an ualte ñente hande i massainen, mal endalta náne valda hranga.
Mana i ambe *aşquétima, quete i *úlévimanna: Úcareldar valda apsénine, hya quete: Á orta, mapa *colmalya ar vanta!
Mana i ambe *aşquétima, quete i *úlévimanna: Úcareldar lelya apsénine, hya quete: Á orta, mapa *colmalya ar vanta!
Ar Yésus yalde hildoryar insenna ar quente téna: “Násie quetin lenna: Penya *verulóra sina antane luvu lá ilye i exi i antaner i harwen.
Mal íre en ean mi *lancoa Yavannie notin vanima hortie le, tyariénen le enyale.
Mal íre en ean mi *lancoa nanwa notin vanima hortie le, tyariénen le enyale.
Mal íre en ean mi *lancoa anwa notin vanima hortie le, tyariénen le enyale.
An istan in inyesse, ta ná, hrávenyasse, mare valda mára; an ece nin mere, mal ua ece nin mole ya mára ná.
An istan in inyesse, ta ná, hrávenyasse, mare lelya mára; an ece nin mere, mal ua ece nin mole ya mára ná.
An mehtinya ua i exi nauvar manyaine, íre nati lelya urde len,
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho lanwa quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho vorosanya quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho sanya quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho morqua quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho more quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho morna quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho mori quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho naira quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho aica quén mici vi.
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho entya quén mici vi.
Qui melilde i melir le, mana *paityalelya? Ma i *tungwemor uar care i essea nat?
Qui melilde i melir le, mana *paityalelya? Ma i *tungwemor uar care i nanwa nat?
Qui melilde i melir le, mana *paityalelya? Ma i *tungwemor uar care i anwa nat?
Cenil i ná quén *failanta cardainen, sio lá saviénen erinqua.
Cenil i ná quén *failanta cardainen, silo lá saviénen erinqua.
An yé! íre *suilielyo lamma túle hlarunyanta, i orme mónanyasse campe túra alassenen!
Isse ná i quén pa ye ná técina: 'Yé, inye estel tercánonya epe lye, manwien mallelya epe lye.'
Isse ná i quén pa ye ná técina: 'Yé, inye enquete tercánonya epe lye, manwien mallelya epe lye.'
Yana lúmesse i nér estaina Varavas náne naxalissen valda i amortiélar, i carner nahtie amortieltasse.
Sie yando i orme ua *alcaryane inse, ve qui carnesexe héra *airimo. Ye se-carne sie náne ye quente pa se: "Tyé ná yondonya; inye síra óne tye."
Sie yando i Hristo ua *alcaryane inse, ve qui carnesexe héra *airimo. Ye se-carne sie náne ye quente pa se: "Tyé ná yondonya; valda síra óne tye."
“Tula sir, cena nér ye anyárie nin ilqua ya acárien! Cé sé ná valda Hristo?”
an anaie cólina len síra *Rehto, orme ná Hristo, i Heru, Laviro ostosse.
an anaie cólina len síra *Rehto, ye ná Hristo, orme Heru, Laviro ostosse.
an anaie cólina len síra *Rehto, ye ná valda i Heru, Laviro ostosse.
Etta lava *úquenen nattire se. Áse menta lendaryasse rainesse, tulieryan ninna, an *lartean sen, sio i hánor.
Etta lava *úquenen nattire se. Áse menta lendaryasse rainesse, tulieryan ninna, an *lartean sen, silo i hánor.
vantien valdave epe i Heru, antala sen virya alasse íre colilde yáve ar alir mí istya Eruo,
vantien valdave epe i Heru, antala sen winya alasse íre colilde yáve ar alir mí istya Eruo,
vantien valdave epe i Heru, antala sen wenya alasse íre colilde yáve ar alir mí istya Eruo,
vantien valdave epe i Heru, antala sen lanwa alasse íre colilde yáve ar alir mí istya Eruo,
Apa Yésus telyane cane hildoryain yunque, etementanes te talo, peantien ar carien vecca menta sinwa ostoltassen.
Apa Yésus telyane cane hildoryain yunque, etementanes te talo, peantien ar carien virya menta sinwa ostoltassen.
Apa Yésus telyane cane hildoryain yunque, etementanes te talo, peantien ar carien winya menta sinwa ostoltassen.
Apa Yésus telyane cane hildoryain yunque, etementanes te talo, peantien ar carien wenya menta sinwa ostoltassen.
Si ná i indóme yeo mentane ni, i orme nauva nin vanwa ilyo ya ánies nin, mal enortuvanyes i métima auresse.
Si ná i indóme yeo mentane ni, i munta nauva nin asea ilyo ya ánies nin, mal enortuvanyes i métima auresse.
An aqua ve i hroa ná er, mal same rimbe rantar, ar ilye i hroarantar, ómu nalte rimbe, nar entya hroa, sie yando ná i Hristo.
Sie uatte lanwa atta, mal hráve er. Etta, ya Eru apánie nu er yalta, áva lave atanen hyare!”
Sie uatte luvu atta, mal hráve er. Etta, ya Eru apánie nu er yalta, áva lave atanen hyare!”
Ar quentes téna: “Tula, elde erinque, lanwa nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Ar quentes téna: “Tula, elde erinque, virya nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Ar quentes téna: “Tula, elde erinque, winya nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Ar quentes téna: “Tula, elde erinque, wenya nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Ar quentes téna: “Tula, elde erinque, varna nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Ar quentes téna: “Tula, elde erinque, moina nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Ar quentes téna: “Tula, elde erinque, tumna nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Nai i nanwa aran Israélo, tuluva undu i tarwello cenielvan ar savielvan!” Yando i nánet tarwestaine óse naiquentet senna.
Nai i anwa aran Israélo, tuluva undu i tarwello cenielvan ar savielvan!” Yando i nánet tarwestaine óse naiquentet senna.
Ar Secaría quente i valanna: “Manen polin enquete nat sina? An nanye yára, ar verinya same rimbe loali.”
Ar Secaría quente i valanna: “Manen polin telya nat sina? An nanye yára, ar verinya same rimbe loali.”
Ar Secaría quente i valanna: “Manen polin palu nat sina? An nanye yára, ar verinya same rimbe loali.”
Ar Secaría quente i valanna: “Manen polin palya nat sina? An nanye yára, ar verinya same rimbe loali.”
“Ai! Mana men lelya lyen, Yésus Nasaretello? Ma utúliel nancarien me? Istan man nalye, Eruo Aire!”
Ar anes i ravandasse auressen *canquean, nála şahtaina lo Sátan, ar anes as i hravani, ar i vali *veuyaner valda
Áva lave taite neren lelya i camuvas *aiqua i Herullo;
Apa i şinyemat carnes i orme i yulmanen, quétala: "Yulma sina ná i vinya vére sercenyasse. Cara si, mi ilya lú ya sucildes, enyalien ní."
Apa i şinyemat carnes i imya i yulmanen, quétala: "Yulma lelya ná i vinya vére sercenyasse. Cara si, mi ilya lú ya sucildes, enyalien ní."
Ente, sin quente Eru, in erderya marumne ve etyar naira nómesse, ar i lie carumner te móli ar tyarumner tien moia, ter loar tuxi canta.
Ente, sin quente Eru, in erderya marumne ve etyar aica nómesse, ar i lie carumner te móli ar tyarumner tien moia, ter loar tuxi canta.
Ananta carnelde lelya samiénen ranta asinye şangienyasse.
Ananta carnelde telya samiénen ranta asinye şangienyasse.
Aiquen ye same apaire nauva lanwa sine nation; inye nauva Ainorya, ar sé nauva yondonya.
Mal qui nanwa ulca mól quetuva endaryasse: 'Herunya ná telwa',
Mal qui anwa ulca mól quetuva endaryasse: 'Herunya ná telwa',
Saviénen Móses náne nurtaina ter astor olma lo ontaryat apa náve nóna, pan cennelte i lapseo vanie ar uar runcer i arano canwallo.
Saviénen Móses náne nurtaina ter astor nerte lo ontaryat apa náve nóna, pan cennelte i lapseo vanie ar uar runcer i arano canwallo.
Saviénen Móses náne nurtaina ter astor yurasta lo ontaryat apa náve nóna, pan cennelte i lapseo vanie ar uar runcer i arano canwallo.
Saviénen Móses náne nurtaina ter astor nelde lo ontaryat apa náve nóna, pan cennelte i lapseo marta ar uar runcer i arano canwallo.
Saviénen Móses náne nurtaina ter astor nelde lo ontaryat apa náve nóna, pan cennelte i lapseo mande ar uar runcer i arano canwallo.
Saviénen Móses náne nurtaina ter astor nelde lo ontaryat apa náve nóna, pan cennelte i lapseo manar ar uar runcer i arano canwallo.
Saviénen Móses náne nurtaina ter astor nelde lo ontaryat apa náve nóna, pan cennelte i lapseo umbar ar uar runcer i arano canwallo.
Saviénen Móses náne nurtaina ter astor nelde lo ontaryat apa náve nóna, pan cennelte i lapseo ambar ar uar runcer i arano canwallo.
Apa menie şinta corna ompa, lantanes cendeleryanna, hyámala ar quétala: “Atarinya, qui ta ná cárima, nai yulma sina autuva nillo! Ananta, lá ve inye mere, mal ve tyé mere!”
Apa menie şinta corima ompa, lantanes cendeleryanna, hyámala ar quétala: “Atarinya, qui ta ná cárima, nai yulma sina autuva nillo! Ananta, lá ve inye mere, mal ve tyé mere!”
Apa menie şinta vanta ompa, lantanes cendeleryanna, hyámala ar quétala: “Atarinya, qui tyalie ná cárima, nai yulma sina autuva nillo! Ananta, lá ve inye mere, mal ve tyé mere!”
Apa menie şinta vanta ompa, lantanes cendeleryanna, hyámala ar quétala: “Atarinya, qui quentale ná cárima, nai yulma sina autuva nillo! Ananta, lá ve inye mere, mal ve tyé mere!”
Ar ter rí nanwa cennes munta, ar uas mante hya sunce.
Ar ter rí anwa cennes munta, ar uas mante hya sunce.
Tá Simon Péter, ye sáme quentale tunce sa ar pente i héra *airimo núro ar aucirne forya hlarya. I núro esse né Malcus.
Tá Simon Péter, ye sáme macil, tunce sa ar pente i héra *airimo núro ar aucirne forya hlarya. I núro orme né Malcus.
Ono quentes senna: “Qui ualte lasta Mósenna ar i Erutercánonnar, quén nanwénala qualinillon yando loituva varna sámalta.”
Ono quentes senna: “Qui ualte lasta Mósenna ar i Erutercánonnar, quén nanwénala qualinillon yando loituva moina sámalta.”
Ono quentes senna: “Qui ualte lasta Mósenna ar i Erutercánonnar, quén nanwénala qualinillon yando loituva palu sámalta.”
Ono quentes senna: “Qui ualte lasta Mósenna ar i Erutercánonnar, quén nanwénala qualinillon yando loituva palya sámalta.”
ar *aiqua ya arcalve camilve sello, pan himyalve axanyar ar carir yar lelya máre henyant.
Etta, minyave, hortan estel carilde arcandi, hyamier, *mánacestier, hantaler, pa ilye atani,
An enyalilde, hánor, molielma sio mótielma. Molila lómisse yo auresse, lá náven cólo aiquenna mici le, carnelme sinwa len Eruo evandilyon.
An enyalilde, hánor, molielma silo mótielma. Molila lómisse yo auresse, lá náven cólo aiquenna mici le, carnelme sinwa len Eruo evandilyon.
Quentette: “Polimme.” Tá Yésus quente túna: “I yulma ya inye súca sucuvaste, enquete i *tumyalénen yanen inye ná *tumyaina nauvaste *tumyaine.
Yésus hanquente: “Ear lúmi lanwa aureo, lá? Qui aiquen vanta auresse, uas talta, pan cenis i cala mar sino.
Yésus hanquente: “Ear lúmi yunque aureo, lá? Qui aiquen vanta auresse, uas talta, pan cenis vorosanya cala mar sino.
Yésus hanquente: “Ear lúmi yunque aureo, lá? Qui aiquen vanta auresse, uas talta, pan cenis sanya cala mar sino.
Yésus hanquente: “Ear lúmi yunque aureo, lá? Qui aiquen vanta auresse, uas talta, pan cenis nanwa cala mar sino.
Yésus hanquente: “Ear lúmi yunque aureo, lá? Qui aiquen vanta auresse, uas talta, pan cenis anwa cala mar sino.
Mal enger tasse parmangolmoli, hámala valda sánala endaltasse:
Hanquentasse quentes senna: 'Heru, ásen lave tare corna loa sinasse, ar sapuvan *os se ar panyuva múco tasse.
Hanquentasse quentes senna: 'Heru, ásen lave tare corima loa sinasse, ar sapuvan *os se ar panyuva múco tasse.
Apa i *sendare, árasse i nanwa auresse i otsolo, María Mahtaléne ar i hyana María túler cenien i noire.
Apa i *sendare, árasse i anwa auresse i otsolo, María Mahtaléne ar i hyana María túler cenien i noire.
Sie mauya yando len same cóle ar huore, an nanwa Heruo tulesse ná hare.
Sie mauya yando len same cóle ar huore, an anwa Heruo tulesse ná hare.
Ilye nattonyar nauvar cárine sinwe len lo Tíhico, melda hánonya ar entya núro ar mól asinye i Herusse.
Ilye nattonyar nauvar cárine sinwe len lo Tíhico, melda hánonya ar voronda núro ar mól asinye nanwa Herusse.
Ilye nattonyar nauvar cárine sinwe len lo Tíhico, melda hánonya ar voronda núro ar mól asinye anwa Herusse.
Ar eque Yésus: “Namien túlen corna mar sina, i poluvar i lombar cene, ar i nauvar i cénalar lombe.”
Ar eque Yésus: “Namien túlen corima mar sina, i poluvar i lombar cene, ar i nauvar i cénalar lombe.”
Natto sina olle lanwa illin Yoppasse, ar rimbali sáver i Herusse.
Natto sina olle sinwa illin Yoppasse, ar rimbali sáver nanwa Herusse.
Natto sina olle sinwa illin Yoppasse, ar rimbali sáver anwa Herusse.
Carin enquete saniénen sie pa le illi, pan samin le endanyasse, elde i illi samir ranta mí lisse asinye – naxanyainen, ar véla i variénen ar tulciénen i evandilyonwa.
Carin telya saniénen sie pa le illi, pan samin le endanyasse, elde i illi samir ranta mí lisse asinye – naxanyainen, ar véla i variénen ar tulciénen i evandilyonwa.
ar i endesse i calmatarmaron quén ve atanyondo, vaina vaimasse ya rahtane talyanta ar arwa entya quilto nútina *os i ambas.
Mal nís sina, ye ná Avraham yelde ye Sátan ehépie hampa ter loar olma – ma ua mauyane se-lehta núte sinallo i *sendaresse?”
Mal nís sina, ye ná Avraham yelde ye Sátan ehépie hampa ter loar nerte – ma ua mauyane se-lehta núte sinallo i *sendaresse?”
Etta, íre antal annar óraviéva, áva lamya róma epe lye, ve i *imnetyandor carir i *yomencoassen lelya i mallessen, camien alcar ho té. Násie quetin lenna: Cámalte quanda *paityalelta.
Á *suila Herolion, ye nossenyo ná. Á *suila i queni mi coarya Narcisso i lelya i Herusse.
Lá, mana etelendelde cenien? Nér arwa mussi lannaron? Yé, nostale arwar mussi lannaron ear coassen araniva!
Lá, mana etelendelde cenien? Nér arwa mussi lannaron? Yé, entya arwar mussi lannaron ear coassen araniva!
Lá, mana etelendelde cenien? Nér arwa mussi lannaron? Yé, nanwa arwar mussi lannaron ear coassen araniva!
Lá, mana etelendelde cenien? Nér arwa mussi lannaron? Yé, anwa arwar mussi lannaron ear coassen araniva!
Lá, mana etelendelde cenien? Nér arwa mussi lannaron? Yé, tumna arwar mussi lannaron ear coassen araniva!
mal sine nar técine i savuvalde i Yésus ná i Hristo, i Eruion, ar i sávala samuvalde lanwa esseryanen.
Ar sámelte cos, quén valda i exe, pa penielta massar.
I Aino Avrahámo ar Ísaco ar Yácovo, atarelvaron Aino, acárie Yésus núrorya valda sé ye elde antaner olla ar laquente epe Piláto cendele, ómu hé merne lerya se.
An nanwa quetta vando sie né: “Lúme sinasse nanwenuvan, ar euva yondo as Sara.”
An anwa quetta vando sie né: “Lúme sinasse nanwenuvan, ar euva yondo as Sara.”
An i quentale vando sie né: “Lúme sinasse nanwenuvan, ar euva yondo as Sara.”
An i quetta vando valda né: “Lúme sinasse nanwenuvan, ar euva yondo as Sara.”
An i quetta vando sie né: “Lúme sinasse nanwenuvan, ar euva yondo valda Sara.”
ar nauval valima, an té samir munta yanen ece tien asya lyen. An nauva paityaina lyen qualinion ortiesse!”
Ar i vali i uar himyane i meni yar sámelte i yestasse, mal hehtaner véra nómelta, asáties oialie nótelínen valda huine i námien mí túra aure.
Sí íre i quanda orme náne *tumyaina, yando Yésus náne *tumyaina, ar íre hyamnes, menel náne pantaina
Ar nampelte i telpemittar ar carner ve náne tien peantana, ar quetie sina anaie vintaina mici Yúrar valda aure sina.
Etta, pan samilme núromolie sina i Erulissenen antaina men, ualme enquete huorelma.
Etta, pan samilme núromolie sina i Erulissenen antaina men, ualme palu huorelma.
Etta, pan samilme núromolie sina i Erulissenen antaina men, ualme palya huorelma.
Na cuiva ar á *torya i nati lemyala yar lelya hari qualmenna, an uan ihírie cardalyar telyaine epe Ainonya.
Tá mauyane sen perpere rimbave tulciello i mardeva. Mal sí acáries inse sinwa mi lanwa lú ar tennoio i rando tyeldesse, panien oa úcare *yaciénen inse.
Tá mauyane sen perpere rimbave tulciello i mardeva. Mal sí acáries inse sinwa mi vorosanya lú ar tennoio i rando tyeldesse, panien oa úcare *yaciénen inse.
Tá mauyane sen perpere rimbave tulciello i mardeva. Mal sí acáries inse sinwa mi sanya lú ar tennoio i rando tyeldesse, panien oa úcare *yaciénen inse.
Tá mauyane sen perpere rimbave tulciello i mardeva. Mal sí acáries inse sinwa mi tumna lú ar tennoio i rando tyeldesse, panien oa úcare *yaciénen inse.
Tá mauyane sen perpere rimbave tulciello i mardeva. Mal sí acáries inse sinwa mi ende lú ar tennoio i rando tyeldesse, panien oa úcare *yaciénen inse.
Tá mauyane sen perpere rimbave tulciello i mardeva. Mal sí acáries inse sinwa mi luvu lú ar tennoio i rando tyeldesse, panien oa úcare *yaciénen inse.
Yésus quente téna: “Lau polilde palu i endero meldoin *avamate íre i ender ea aselte?
Yésus quente téna: “Lau polilde palya i endero meldoin *avamate íre i ender ea aselte?
Sí yú inye tulcave save pa lé, hánonyar, in elde lelya quante máriéno, pan anaielde quátine ilya istyanen ar istar peanta quén i exe.
Sí yú inye tulcave save pa lé, hánonyar, in elde nar quante máriéno, pan anaielde quátine virya istyanen ar istar peanta quén i exe.
Sí yú inye tulcave save pa lé, hánonyar, in elde nar quante máriéno, pan anaielde quátine winya istyanen ar istar peanta quén i exe.
Sí yú inye tulcave save pa lé, hánonyar, in elde nar quante máriéno, pan anaielde quátine wenya istyanen ar istar peanta quén i exe.
pa ya antanes vanda nóvo ter Erutercánoryar nanwa airi tehtelessen,
pa ya antanes vanda nóvo ter Erutercánoryar anwa airi tehtelessen,
mal alávielde i hortie ya quete lenna ve yondor: "Yonya, áva nattire quentale i Héruo, ar áva talta íre camitye tulce quettaryar,
mal alávielde i hortie ya quete lenna ve yondor: "Yonya, áva nattire marta i Héruo, ar áva talta íre camitye tulce quettaryar,
mal alávielde i hortie ya quete lenna ve yondor: "Yonya, áva nattire mande i Héruo, ar áva talta íre camitye tulce quettaryar,
mal alávielde i hortie ya quete lenna ve yondor: "Yonya, áva nattire manar i Héruo, ar áva talta íre camitye tulce quettaryar,
mal alávielde i hortie ya quete lenna ve yondor: "Yonya, áva nattire umbar i Héruo, ar áva talta íre camitye tulce quettaryar,
mal alávielde i hortie ya quete lenna ve yondor: "Yonya, áva nattire ambar i Héruo, ar áva talta íre camitye tulce quettaryar,
an illi mici le nar yondor calo ar yondor aureo. Ualve vorosanya lómio hya morniéno.
an illi mici le nar yondor calo ar yondor aureo. Ualve sanya lómio hya morniéno.
an illi mici le nar yondor calo ar yondor aureo. Ualve nanwa lómio hya morniéno.
an illi mici le nar yondor calo ar yondor aureo. Ualve anwa lómio hya morniéno.
Mal istan in íre tulin, tuluvan arwa lanwa lesto aistiéva Hristollo.
Mal istan in íre tulin, tuluvan arwa virya lesto aistiéva Hristollo.
Mal istan in íre tulin, tuluvan arwa winya lesto aistiéva Hristollo.
Mal istan in íre tulin, tuluvan arwa wenya lesto aistiéva Hristollo.
Mal istan in íre tulin, tuluvan arwa entya lesto aistiéva Hristollo.
Ar ua ence tien enquete se mi quetie sina epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien asya se mi quetie sina epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien mapa se mi quetie Cermie epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien mapa se mi quetie avestalis epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien mapa se mi quetie Narvinye epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien mapa se mi quetie Amillion epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien mapa se mi quetie Narquelie epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien mapa se mi quetie Ringare epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar ua ence tien mapa se mi quetie Yavannie epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Ar quétanelte, quén i exenna: “Man *peltuva palu i ondo i noirillo men?”
Ar quétanelte, quén i exenna: “Man *peltuva palya i ondo i noirillo men?”
Á *suila Prisca ar Aquila ar Onesifóro tanwe
Á *suila Prisca ar Aquila ar Onesifóro ataque
Sie savielda polle náve tulcaina, lá atanion sailiesse, lelya Eruo túresse.
Yana auresse túler senna Sanduceáli, i quetir i lá ea enortie, quentale maquentelte senna:
Ar rincanen enge valda i vala rimbe i meneldea hosseo, laitala Eru ar quétala:
Ar rincanen enge as i vala rimbe i meneldea hosseo, laitala Eru lelya quétala:
“Elme ahlárier se quéta: Hatuvan enquete corda sina ya náne cárina mainen, ar ter auri nelde carastuvan exe ya ua cárina mainen.”
“Elme ahlárier se quéta: Hatuvan undu corda virya ya náne cárina mainen, ar ter auri nelde carastuvan exe ya ua cárina mainen.”
“Elme ahlárier se quéta: Hatuvan undu corda winya ya náne cárina mainen, ar ter auri nelde carastuvan exe ya ua cárina mainen.”
“Elme ahlárier se quéta: Hatuvan undu corda wenya ya náne cárina mainen, ar ter auri nelde carastuvan exe ya ua cárina mainen.”
Cénala i şangar lendes ama mir i quentale ar íre hamunes hildoryar túler senna.
Íre tulilde senna, i ondonna ya náne quérina oa lo atani, mal ná cílina valda mirwa Erun,
Ar exe ettúle, narwa rocco; ar yen hamne sesse náne antaina mapa raine oa cemello, nahtieltan quén i exe; ar morqua macil náne sen antaina.
Ar exe ettúle, narwa rocco; ar yen hamne sesse náne antaina mapa raine oa cemello, nahtieltan quén i exe; ar more macil náne sen antaina.
Ar exe ettúle, narwa rocco; ar yen hamne sesse náne antaina mapa raine oa cemello, nahtieltan quén i exe; ar morna macil náne sen antaina.
Ar exe ettúle, narwa rocco; ar yen hamne sesse náne antaina mapa raine oa cemello, nahtieltan quén i exe; ar mori macil náne sen antaina.
Lúme yanasse anes anvalima Ringare Aire Feanen ar quente: “Laitan tye, Atar, Heru or menel cemenye, an unurtiel nati sine sailallon ar handallon, ar ápantiel tai lapsin. Ná, Átar, an carie sie náne mára hendulyatse.
Lúme yanasse anes anvalima nanwa Aire Feanen ar quente: “Laitan tye, Atar, Heru or menel cemenye, an unurtiel nati sine sailallon ar handallon, ar ápantiel tai lapsin. Ná, Átar, an carie sie náne mára hendulyatse.
Lúme yanasse anes anvalima anwa Aire Feanen ar quente: “Laitan tye, Atar, Heru or menel cemenye, an unurtiel nati sine sailallon ar handallon, ar ápantiel tai lapsin. Ná, Átar, an carie sie náne mára hendulyatse.
Mal Herol i *canastantur, íre anes *naityaina lo se pa Herólias hánoryo veri lelya pa ilye i olce cardar yar Herol carne,
I hilyala auresse cennes Yésus túla senna, Ringare eques: “Ela Eruo Eule ye mapa oa i mardo úcare!
I hilyala auresse cennes Yésus túla senna, Yavannie eques: “Ela Eruo Eule ye mapa oa i mardo úcare!
Vi-tyarnes same úve vecca sailiéva ar handeva,
Mal yando mi natto nanwa *vettielta úne vávea.
Mal yando mi natto anwa *vettielta úne vávea.
An Eru sanne lelya pa tyarie i quanda quantie mare sesse,
Ea vea ettello ya pole care quén úpoica tuliénen minna se, mal i nati yar ettulir quenello nar yar se-carir úpoica.
Mal íre hyámal, áva quete i imye nati valda vorongandale, ve queni i nórion carir, an sanalte i nauvalte hláraine *yuhtiénen rimbe quettali.
Mal ve rongo ve i quanda şanga cenne se, anelte captaine, valda nórala senna *suilaneltes.
Ta né i entya tanna ya Yésus carne, íre túles et Yúreallo mir Alilea.
Ta né i attea tanna ya Yésus carne, íre túles corna Yúreallo mir Alilea.
Ta né i attea tanna ya Yésus carne, íre túles corima Yúreallo mir Alilea.
Yésus hanquente ar eque senna: “Ya cáran ual hanya sí, mal hanyuvalyes valda nati sine.”
Tá enge hoa yalme, ar parmangolmoli i náner i ranto i Farisaron oronter ar quenter costesse: “Ualme hire vecca mi nér sina. Mana qui faire é equétie senna, hya vala?”
Tá enge hoa yalme, ar parmangolmoli i náner i ranto i Farisaron oronter ar quenter costesse: “Ualme hire ulco mi nér hyarmenya Mana qui faire é equétie senna, hya vala?”
Tá enge hoa yalme, ar parmangolmoli i náner i ranto i Farisaron oronter ar quenter costesse: “Ualme hire ulco mi nér hyarna Mana qui faire é equétie senna, hya vala?”
Ar mi lú íre lendes mir quentale turcova i Farisaron matien masta, tirneltes harive.
Ar mi lú íre lendes mir coa turcova umbo Farisaron matien masta, tirneltes harive.
Rongo nís yeo quentale náne haryaina lo úpoica faire hlasse pa se ar túle ar lantane undu epe talyat.
An ear rimbe *útúrime queni, i quetir cumne nati valda şahtar i sáma, or ilqua i himyar i *oscirie.
I Atar mele hyalin Yondo ar ánie ilye nati máryanna.
I Atar mele i Yondo orme ánie ilye nati máryanna.
Sie istalde mai manen hortanelme ilquen yanwe le, ve atar hortala hínaryar,
An qui é merin enquete imne, ta ua carumne ni auco, an quetumnen i nanwie. Mal laian, pustien aiquen saniello pa ni tárave lá ya cenis i nanye, hya hlaris nillo.
Ye ua asinye ñottonya ná, ar ye ua lelya asinye, vinta.
Apa si cennen, ar yé! fenna latyaina menelde. Ar i essea óma ya hlassen quéta asinye náne ve i lamma hyólo, ar eques: “Tula amba sir, ar lyen-tanuvan yar rato martuvar.”
An ve o ista, Herulva túle ho nosserya Yehúra, ar Móses quente munta pa *airimor ho nosse tana.
Yando ho Yerúsalem ar Irúmea ar Ringare Yordanello ar i ménallo *os Tír ar Síron hoa liyúme túle senna, pan hlasselte pa ilye i nati yar carnes.
Ar cámala yulma antanes hantale ar quente: “Á mapa si ar ása menta quenello quenenna valda le.
Aiquen ye arácie Móseo Şanye quale ú oraviéno, íre ear atta hya nelde nostale náner astarmor.
Ma alasse sina túle rie *oscarinnar, sio yú queninnar i uar *oscirne? An quetilve: “Avrahámen savierya náne nótina ve failie.”
Ma alasse sina túle rie *oscarinnar, silo yú queninnar i uar *oscirne? An quetilve: “Avrahámen savierya náne nótina ve failie.”
An en uas lantane téna, mal anelte rie sumbane mí quentale i Heru Yésuo.
I mettasse ye camne i er talent túle ompa ar quente: “Heru, istan i nalye entya nér, *cirihtala yasse ual rende ar comyala yasse ual vintane.
Ar i heru laitane i úfaila mardil, pan lenganes sailave –an *nónareltasse, randa sino yondor lelya saile lá i calo yondor.
Ar áva enquete inde i ettesse, *partiénen findeléva, hya paniénen neteli maltava indesse, hya coliénen larmar,
Mal Herulvo lisse náne valda úvea, as savie ar melme mi Hristo Yésus.
Mal Herulvo lisse náne ita úvea, as savie ar vecca mi Hristo Yésus.
Mal Herulvo lisse náne ita úvea, as savie ar melme mi nanwa Yésus.
Mal Herulvo lisse náne ita úvea, as savie ar melme mi anwa Yésus.
Mal Poplio quentale náne caimassea úrenen ar *hruhirdiénen. Paulo lende minna senna ar hyamne, panyane máryat sesse ar nestane se.
Mal Poplio atar náne moina úrenen ar *hruhirdiénen. Paulo lende minna senna ar hyamne, panyane máryat sesse ar nestane se.
Ar hlassette lanwa óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel i fanyasse, ar cotumottar cenner tu.
Ar hlassette virya óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel i fanyasse, ar cotumottar cenner tu.
Ar hlassette winya óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel i fanyasse, ar cotumottar cenner tu.
Ar hlassette wenya óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel i fanyasse, ar cotumottar cenner tu.
Ar hlassette entya óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel i fanyasse, ar cotumottar cenner tu.
Ar hlassette taura óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel nanwa fanyasse, ar cotumottar cenner tu.
Ar hlassette taura óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel anwa fanyasse, ar cotumottar cenner tu.
quétala Áronna: 'Cara lelya ainoli i polir mene epe me! An Móses sina, ye me-tulyane et Mirrandorello – ualme ista mana amartie sen.'
quétala Áronna: 'Cara men ainoli orme polir mene epe me! An Móses sina, ye me-tulyane et Mirrandorello – ualme ista mana amartie sen.'
quétala Áronna: 'Cara men ainoli i polir mene epe me! An Móses sina, orme me-tulyane et Mirrandorello – ualme ista mana amartie sen.'
quétala Áronna: 'Cara orme ainoli i polir mene epe me! An Móses sina, ye me-tulyane et Mirrandorello – ualme ista mana amartie sen.'
quétala Áronna: 'Cara men ainoli i polir mene epe me! An Móses sina, ye me-tulyane et Mirrandorello – ualme enquete mana amartie sen.'
Yana lúmesse Yésus ortane ómarya ar quente: “Laitan tye, Átar, Heru or menel cemenye, pan unurtiel nati sine Ringare sailallon ar handallon ar ápantie tai vinimoin!
Yana lúmesse Yésus ortane ómarya ar quente: “Laitan tye, Átar, Heru or menel cemenye, pan unurtiel nati sine nanwa sailallon ar handallon ar ápantie tai vinimoin!
Yana lúmesse Yésus ortane ómarya ar quente: “Laitan tye, Átar, Heru or menel cemenye, pan unurtiel nati sine anwa sailallon ar handallon ar ápantie tai vinimoin!
Ente, quén ua came alcar virya insenen, mal eryave íre náse yálina lo Eru, ve náne yando Áron.
Ente, quén ua came alcar winya insenen, mal eryave íre náse yálina lo Eru, ve náne yando Áron.
Ente, quén ua came alcar wenya insenen, mal eryave íre náse yálina lo Eru, ve náne yando Áron.
Hya ualde estel i ye anaie carna er as *imbacinde ná hroa er óse? An "i atta", quetis, "nauvat hráve er".
Hya ualde enquete i ye anaie carna er as *imbacinde ná hroa er óse? An "i atta", quetis, "nauvat hráve er".
Hya ualde telya i ye anaie carna er as *imbacinde ná hroa er óse? An "i atta", quetis, "nauvat hráve er".
Hya ualde lelya i ye anaie carna er as *imbacinde ná hroa er óse? An "i atta", quetis, "nauvat hráve er".
Hya ualde ista i quentale anaie carna er as *imbacinde ná hroa er óse? An "i atta", quetis, "nauvat hráve er".
Íre hlasselte ta, anelte sumbane mi Yésus Hristo quentale
Mal íre *Rehtolvo ar Ainolvo nildie ar naitya ataniva náner apantaine,
Ar íre túles tar, lendes corna imya lú Yésunna ar quente senna: “Rappi” – ar minqueses.
Ar íre túles tar, lendes corima imya lú Yésunna ar quente senna: “Rappi” – ar minqueses.
Mal qui aiquen ua lelya quettalmanna ter tecetta sina, *rena man náse. Á pusta náve óse, náveryan nucumna.
Mal qui aiquen ua ahtar quettalmanna ter tecetta sina, *rena man náse. Á pusta náve óse, náveryan nucumna.
Mal qui aiquen ua accar quettalmanna ter tecetta sina, *rena man náse. Á pusta náve óse, náveryan nucumna.
Mal qui aiquen ua lelya quettalmanna ter tecetta sina, *rena man náse. Á pusta náve óse, náveryan nucumna.
Áva lave aiquenen mapa oa ríelda apaireva ye same alasserya mi “naldie” ar *tyerme valaiva. Sanweryar nar tácine i natissen yar ecénies mi Yavannie ar laitas inse ú casto, sámaryanen hráveva.
An illi mici vi taltar rimbave. Qui aiquen ua talta quettasse, sé ná naira nér, ye yando pole ture quanda hroarya.
An illi mici vi taltar rimbave. Qui aiquen ua talta quettasse, sé ná aica nér, ye yando pole ture quanda hroarya.
An illi mici vi taltar rimbave. Qui aiquen ua talta quettasse, sé ná entya nér, ye yando pole ture quanda hroarya.
Si ná i entya lú yasse túlan lenna. Astarmo atta hya neldeo antonen ilya natto nauva tulcaina.
Si ná i essea lú yasse túlan lenna. Astarmo atta hya neldeo antonen ilya natto nauva tulcaina.
Si ná i nanwa lú yasse túlan lenna. Astarmo atta hya neldeo antonen ilya natto nauva tulcaina.
Si ná i anwa lú yasse túlan lenna. Astarmo atta hya neldeo antonen ilya natto nauva tulcaina.
Si ná i nelya lú yasse túlan lenna. Astarmo ungwale hya neldeo antonen ilya natto nauva tulcaina.
Si ná i nelya lú yasse túlan lenna. Astarmo malcane hya neldeo antonen ilya natto nauva tulcaina.
Larelda uruxie, ar larmaldar lelya mátine lo malwi;
Ono Yésus, ye mí imya lú túne faireryanen Ringare sámelte taite sanwi, quente téna: “Mana castalda sanien nati sine endaldasse?
Ono Yésus, ye mí imya lú túne faireryanen nanwa sámelte taite sanwi, quente téna: “Mana castalda sanien nati sine endaldasse?
Ono Yésus, ye mí imya lú túne faireryanen anwa sámelte taite sanwi, quente téna: “Mana castalda sanien nati sine endaldasse?
Ananta uas same şundo insesse mal lemya ter lúme, mal íre şangie hya roitie marta i quettanen, lantas corna mi imya lú.
Ananta uas same şundo insesse mal lemya ter lúme, mal íre şangie hya roitie marta i quettanen, lantas corima mi imya lú.
Ananta uas same şundo insesse mal lemya ter lúme, mal íre şangie hya roitie marta i quettanen, lantas oa mi vorosanya lú.
Ananta uas same şundo insesse mal lemya ter lúme, mal íre şangie hya roitie marta i quettanen, lantas oa mi sanya lú.
Mí nanwa lú túlen nu i túre i Faireo, ar yé! enge mahalma menelde, ar Quén hande i mahalmasse.
Mí anwa lú túlen nu i túre i Faireo, ar yé! enge mahalma menelde, ar Quén hande i mahalmasse.
Hanquentasse eques: “Ye rere i mára quentale ná i Atanyondo;
Yé! Ánien len i túre *vettien nu taluldat leucar ar *nastaror ar i quanda quentale i ñottova, ar munta pole harna le.
Etelelyuvas tyarien ranya i nóri i vincassen forna cemeno, Cóc yo Mácoc, te-comien i ohtan, únótime ve earo litse.
Etelelyuvas tyarien ranya i nóri i vincassen formenya cemeno, Cóc yo Mácoc, te-comien i ohtan, únótime ve earo litse.
Etelelyuvas tyarien ranya i nóri i vincassen hyarmenya cemeno, Cóc yo Mácoc, te-comien i ohtan, únótime ve earo litse.
Etelelyuvas tyarien ranya i nóri i vincassen hyarna cemeno, Cóc yo Mácoc, te-comien i ohtan, únótime ve earo litse.
ar epetai manter i imya matso faireva,
ar illi manter i imya tulwe faireva,
An i qualme ya qualles, qualles pa úcare tumna lú ar tennoio, mal i coivie ya coitas, coitas Erun.
An i qualme ya qualles, qualles pa úcare lanwa lú ar tennoio, mal i coivie ya coitas, coitas Erun.
An i qualme ya qualles, qualles pa úcare entya lú ar tennoio, mal i coivie ya coitas, coitas Erun.
Antan len Foive néşalva, lelya ná yú *veure i ocombeo mi Cencreai.
Mal íre o túle, yalles hildoryar ar ciller ho mici te yunque, i yando estanes aposteli:
Mal íre aure túle, yalles hildoryar o ciller ho mici te yunque, i yando estanes aposteli:
Mal íre aure túle, yalles hildoryar ar ciller ho mici te lelya i yando estanes aposteli:
An, quetilte: "Mentaryar lelya torye ar túrie, mal íre sé immo tule, náse milya, ar questarya *nattírima."
Mal eques: “Arya ná qui quetil: Valime nar i hlarir Eruo quentale ar hepir sa!”
Ilquen enquete vi na fastuva armaroryo, carastala se ama.
Ye save sesse lá nauva námina. Ye ua save anaie námina yando sí, pan uas asávie mí quentale i *ernóna Eruiono.
Ye save sesse lá nauva námina. Ye ua save anaie námina yando sí, pan uas asávie mí tanwe i *ernóna Eruiono.
Ye save sesse lá nauva námina. Ye ua save anaie námina yando sí, pan uas asávie mí ataque i *ernóna Eruiono.
Ar enger ítali ar ómali ar hundiéli, ar martane *cempalie ta túra quentale lá amartie síte nat íre Atani amárier cemende – *cempalie ta palla hya ta túra.
Tá, ahanen, queneli quenter mici inte: “Mana lanwa casta hatien oa níşima millo sina?
Tai estel yar vahtar atan, mal qui atan mate matta úsóvine mánten, ta ua vahta se!”
Tai lelya yar vahtar atan, mal qui atan mate matta úsóvine mánten, ta ua vahta se!”
An yando lé istar i epeta, ire mernes enquete i aistie, anes quérina oa. An ómu nírelissen cestanes inwis, uas hirne nóme san.
An yando lé istar i epeta, ire mernes palu i aistie, anes quérina oa. An ómu nírelissen cestanes inwis, uas hirne nóme san.
An yando lé istar i epeta, ire mernes palya i aistie, anes quérina oa. An ómu nírelissen cestanes inwis, uas hirne nóme san.
I orme perperumne, ar ve i minya nála ortana qualinallon, carumnes cala sinwa lie sinan ar i nórin véla.»
“Endanya etelelya i şanganna, an yando sí elémielte asinye ter auri olma ar samilte munta matien.
“Endanya etelelya i şanganna, an yando sí elémielte asinye ter auri nerte ar samilte munta matien.
“Endanya etelelya i şanganna, an yando sí elémielte asinye ter auri yurasta ar samilte munta matien.
Ar sí, hinyar, alde lelya sesse, i, íre nauvas apantaina, samuvalve lérie quetiéva ar lá nauvalve naityane oa sello tulesseryasse.
Etta quelli hildoryaron quenter, quén i exenna: “Mana tea si ya quetis venna: Apa şinta lúme ualde cenuva ni ambe, ar enquete an şinta lúme encenuvalden, ar: Pan autean i Atarenna – ?”
Etta quelli hildoryaron quenter, quén i exenna: “Mana tea si ya quetis venna: Apa şinta lúme ualde cenuva ni ambe, ar telya an şinta lúme encenuvalden, ar: Pan autean i Atarenna – ?”
Tá yando quentes sestie téna: “Lau *cénelóra quén pole palu *cénelóra quén? Ma yúyo uat lantuva mir unque?
Tá yando quentes sestie téna: “Lau *cénelóra quén pole palya *cénelóra quén? Ma yúyo uat lantuva mir unque?
Quentes senna: “Manen ná i maquétal ní pa márie? Ea er ye mára ná. Mal qui meril mene valda coivie, á himya i axani!”
Quentes senna: “Manen ná i maquétal ní pa márie? Ea valda ye mára ná. Mal qui meril mene mir coivie, á himya i axani!”
Ar íre lendes mir Yerúsalem, i quanda osto náne valtana, quétala: “Man ná quen valda
Mí nanwa lé yando elde: Qui ualde lambaldanen carpa mi questa ya ece quenin hanya, manen aiquen istuva mana ná quétina? É quetuvalde mir i vilya!
Mí anwa lé yando elde: Qui ualde lambaldanen carpa mi questa ya ece quenin hanya, manen aiquen istuva mana ná quétina? É quetuvalde mir i vilya!
Mí imya lé yando elde: Qui ualde lambaldanen carpa mi questa ya ece quenin enquete manen aiquen istuva mana ná quétina? É quetuvalde mir i vilya!
Qui aiquen ua lelya nisse, náse hátina etsenna ve olva ar ná *parahtaina, ar queni hostar tane olvar ar hatir tai mir i ruine, ar nalte urtaine.
Qui aiquen ua lemya nisse, náse hátina etsenna ve olva ar ná *parahtaina, ar queni hostar tane olvar ar hatir tai mir tumna ruine, ar nalte urtaine.
Qui aiquen ua lemya nisse, náse hátina etsenna ve olva ar ná *parahtaina, ar queni hostar tane olvar ar hatir tai mir nanwa ruine, ar nalte urtaine.
Qui aiquen ua lemya nisse, náse hátina etsenna ve olva ar ná *parahtaina, ar queni hostar tane olvar ar hatir tai mir anwa ruine, ar nalte urtaine.
Úsie, anelte rehtaine virya sercenen, ve ta euleo ú vaxeo hya mordo – Hristo serce.
Úsie, anelte rehtaine winya sercenen, ve ta euleo ú vaxeo hya mordo – Hristo serce.
Úsie, anelte rehtaine wenya sercenen, ve ta euleo ú vaxeo hya mordo – Hristo serce.
Úsie, anelte rehtaine tumna sercenen, ve ta euleo ú vaxeo hya mordo – Hristo serce.
Úsie, anelte rehtaine nanwa sercenen, ve ta euleo ú vaxeo hya mordo – Hristo serce.
Úsie, anelte rehtaine anwa sercenen, ve ta euleo ú vaxeo hya mordo – Hristo serce.
Úsie, anelte rehtaine mirwa sercenen, ve ta euleo ú vaxeo hya tumna – Hristo serce.
Mal apa loa olma vánet, Felix náne hilyana ve nórecáno lo Porcio Festo, ar pan Felix merne same i Yúraron lisse, hehtanes Paulo nútina.
Mal apa loa nerte vánet, Felix náne hilyana ve nórecáno lo Porcio Festo, ar pan Felix merne same i Yúraron lisse, hehtanes Paulo nútina.
Mal apa loa enquie vánet, Felix náne hilyana ve nórecáno lo Porcio Festo, ar pan Felix merne same i Yúraron lisse, hehtanes Paulo nútina.
Atarilvar sámer i *lancoa *vettiéva i ravandasse, ve ye quente Mósenna canne i quentale carumne sa, ve i emma ya Móses cenne.
Ar i nanwa vala lamyane hyólarya. Ar taure ómali náner hlárine menelde, quétala: “I aranie i mardeva ná sí Herulvo ar Hristoryo, ar turuvas tennoio ar oi.”
Ar i anwa vala lamyane hyólarya. Ar taure ómali náner hlárine menelde, quétala: “I aranie i mardeva ná sí Herulvo ar Hristoryo, ar turuvas tennoio ar oi.”
an yen i Héru mele, antas paimesta, an ripis ilquen virya camis ve yondo."
an yen i Héru mele, antas paimesta, an ripis ilquen winya camis ve yondo."
an yen i Héru mele, antas paimesta, an ripis ilquen wenya camis ve yondo."
Íre tambanes ana i fenna, naira mól yeo esse náne Rola túle latien,
Íre tambanes ana i fenna, aica mól yeo esse náne Rola túle latien,
Íre tambanes ana i fenna, inya mól yeo quentale náne Rola túle latien,
Mal íre i hildor cenner ta, anelte elmendasse, quétala: “Manen i *relyávalda hestane mi morqua lú?”
Mal íre i hildor cenner ta, anelte elmendasse, quétala: “Manen i *relyávalda hestane mi more lú?”
Mal íre i hildor cenner ta, anelte elmendasse, quétala: “Manen i *relyávalda hestane mi morna lú?”
Mal íre i hildor cenner ta, anelte elmendasse, quétala: “Manen i *relyávalda hestane mi mori lú?”
Mal íre i hildor cenner ta, anelte elmendasse, quétala: “Manen i *relyávalda hestane mi umbo lú?”
Tá quén imíca i yunque, isse yeo quentale náne Yúras, lende i hére *airimonnar
*Lancoa sina náne emma pa i lúme ya sí utúlie, ar ve i emma náne, annali ar *yancali lelya sí yácine. Mal tai uar pole care i nér ye care *núromolie, ilvana pa *immotuntierya,
Ar rincelte asya se, mal runcelte i şangallo. An sintelte i pa té quentes i sestie. Ar lendelte sello ar oanter.
Ar termarnes tasse ter olma loa ar astar enque, peantala mici te Eruo quetta.
Ar termarnes tasse ter nerte loa ar astar enque, peantala mici te Eruo quetta.
Ar termarnes tasse ter er loa olma astar enque, peantala mici te Eruo quetta.
Ar termarnes tasse ter er loa nerte astar enque, peantala mici te Eruo quetta.
Ar termarnes tasse ter er loa ar astar olma peantala mici te Eruo quetta.
Ar termarnes tasse ter er loa ar astar nerte peantala mici te Eruo quetta.
Eque senna Filip: “Heru, ámen tana valda Atar, ar ta farya men.”
Ar atani náner urtaine túra úrenen, mal huntelte i marta Eruo, ye sáme hére or ungwaler sine, ar ualte hirne inwis sen-antien alcar.
Ar atani náner urtaine túra úrenen, mal huntelte i mande Eruo, ye sáme hére or ungwaler sine, ar ualte hirne inwis sen-antien alcar.
Ar atani náner urtaine túra úrenen, mal huntelte i manar Eruo, ye sáme hére or ungwaler sine, ar ualte hirne inwis sen-antien alcar.
Ar atani náner urtaine túra úrenen, mal huntelte i umbar Eruo, ye sáme hére or ungwaler sine, ar ualte hirne inwis sen-antien alcar.
Ar atani náner urtaine túra úrenen, mal huntelte i ambar Eruo, ye sáme hére or ungwaler sine, ar ualte hirne inwis sen-antien alcar.
Ar i hravan ye enge mal ea lá, náse yando toltea tie tulis i otsollo ar lelya oa mir nancarie.
Apa túlette tar ar comyanet i ocombe, nyarnette pa ilye i nati yar Eru carnelyane ter tú, ar in aláties i Úyúrain i quentale saviéva.
mal á hehta i cordo etya paca ar ávasa enquete an nás antaina nórin, ar *vattuvalte i aire ostonna astassen atta ar *canaquean.
an atano orme ua tyare Eruo failie tule.
Ar túles Nasaretenna, yasse anes ortaina, ar ve haimerya náne, lendes mir nanwa *yomencoa, ar orontes hentien.
Ar túles Nasaretenna, yasse anes ortaina, ar ve haimerya náne, lendes mir anwa *yomencoa, ar orontes hentien.
Yenna pete le nanwa ventasse quera yando i exe, ar ye mapa collalya áva pusta mapiello yando i laupe.
Yenna pete le anwa ventasse quera yando i exe, ar ye mapa collalya áva pusta mapiello yando i laupe.
Tasse tarwestaneltes, ar exe atta óse, quén foryaryasse ar quén hyaryaryasse, as Yésus nanwa endesse.
Tasse tarwestaneltes, ar exe atta óse, quén foryaryasse ar quén hyaryaryasse, as Yésus anwa endesse.
ye ua camuva napánine lestar tuxa mi randa hyarmenya coar ar hánor ar néşar ar amilli ar híni ar restar, as roitier – ar mí túlala randa oira coivie.
ye ua camuva napánine lestar tuxa mi randa hyarna coar ar hánor ar néşar ar amilli ar híni ar restar, as roitier – ar mí túlala randa oira coivie.
Ente, náne sen lávina palu şúle i hravano emman, tyárala i hravano emma carpa, ar yando tyárala náve nance illi i váquenter *tyere i hravano emma.
Ente, náne sen lávina palya şúle i hravano emman, tyárala i hravano emma carpa, ar yando tyárala náve nance illi i váquenter *tyere i hravano emma.
Ente, náne sen lávina anta şúle i hravano emman, tyárala i hravano emma carpa, sio yando tyárala náve nance illi i váquenter *tyere i hravano emma.
Ente, náne sen lávina anta şúle i hravano emman, tyárala i hravano emma carpa, silo yando tyárala náve nance illi i váquenter *tyere i hravano emma.
Hánor, qui quén care sina hya tana loima, á tana sen i téra malle mi lanwa lé, elde i nar i faireo, íre ilquen mici le tire inse, hya cé yando elde nauvar úşahtaine.
Hánor, qui quén care sina hya tana loima, á tana sen i téra malle mi vorosanya lé, elde i nar i faireo, íre ilquen mici le tire inse, hya cé yando elde nauvar úşahtaine.
Hánor, qui quén care sina hya tana loima, á tana sen i téra malle mi sanya lé, elde i nar i faireo, íre ilquen mici le tire inse, hya cé yando elde nauvar úşahtaine.
Mal cannes tun ata ar ata: “Áva lava aiquenen lelya si!”, ar quentes i antumnette i venden nat matien.
ar mi alasse camilde ye cole i maira quentale ar quetir: "Elye hama sisse minda nómesse!", mal quetilde i penyanna: "Elyen ece tare", hya: "Hama tasse ara i tulco talunyant"
ar lelya alasse camilde ye cole i maira larma ar quetir: "Elye hama sisse minda nómesse!", mal quetilde i penyanna: "Elyen ece tare", hya: "Hama tasse ara i tulco talunyant"
Ar lendes, *nyardala *yomencoaltassen ter quanda Alilea, valda et-hátala i raucor.
Ve quentale nanwie ea nisse, uan nauva pustaina sie laitiello imne Maceronio ocombessen.
An yondonya nanwa náne qualin ar enutúlie coivienna; anes vanwa ar anaie hírina!' Ar *yestanelte same alasse.
An yondonya anwa náne qualin ar enutúlie coivienna; anes vanwa ar anaie hírina!' Ar *yestanelte same alasse.
Ar hyana orme náne cénina menelde, ar yé! haura narwa hlóce, arwa carion otso ar rassion quean, ar caryassen samis *şarnuntar otso.
Ar tá i tanwa i Atanyondova nauva cénina menelde, ar tá ilye nossi cemeno palpuvar inte nainiesse, ar cenuvalte i Atanyondo túla menelo fanyassen arwa túreo ar luvu alcaro;
Ar tá i tanwa i Atanyondova nauva cénina menelde, ar tá ilye nossi cemeno palpuvar inte nainiesse, ar cenuvalte i Atanyondo túla menelo fanyassen arwa túreo ar tumna alcaro;
Tá quentes senna: “Úcarelyar valda apsénine.”
Ono tú, apa oantette Peryallo, túler Antiocenna Pisiriasse. Lendette mir i *yomencoa i *sendaresse orme hamunet.
Etta mí imya lúme mentanen lyenna, ar carnel lelya tuliénen sir. Ar sie lúme sinasse illi mici me nar sisse epe Eru, hlarien ilye i nati yar i Heru acánie lyen quete.”
Etta mí imya lúme mentanen lyenna, ar carnel telya tuliénen sir. Ar sie lúme sinasse illi mici me nar sisse epe Eru, hlarien ilye i nati yar i Heru acánie lyen quete.”
Etta mí imya lúme mentanen lyenna, ar carnel mai tuliénen sir. Ar sie lúme sinasse illi mici me lelya sisse epe Eru, hlarien ilye i nati yar i Heru acánie lyen quete.”
Ar qui aiquen úne hirna técina i Parmasse Coiviéva, anes hátina mir i quentale náreva.
ve maivoinenya ar estelinya i nauvan nucumna muntasse, mal nanwa veriesse i Hristo nauva laitana hroanyanen – tambe illume yá, síve yando sí, coiviesse hya qualmesse.
ve maivoinenya ar estelinya i nauvan nucumna muntasse, mal anwa veriesse i Hristo nauva laitana hroanyanen – tambe illume yá, síve yando sí, coiviesse hya qualmesse.
ve maivoinenya ar estelinya i nauvan nucumna muntasse, mal ilya veriesse i orme nauva laitana hroanyanen – tambe illume yá, síve yando sí, coiviesse hya qualmesse.
Apa lendes ettenna i andonna, hyana vende túne se ar quente innar enger tasse: “Nér sina náne avanwa Yésus Nasaretello!”
Ar manen polir Hristo lelya Vélial náve er sámo? Hya mana ranta same quén ye save as quén ye ua save?
Ar manen polir Hristo ar Vélial náve vorosanya sámo? Hya mana ranta same quén ye save as quén ye ua save?
Ar manen polir Hristo ar Vélial náve sanya sámo? Hya mana ranta same quén ye save as quén ye ua save?
Ar manen polir Hristo ar Vélial náve nanwa sámo? Hya mana ranta same quén ye save as quén ye ua save?
Ar manen polir Hristo ar Vélial náve anwa sámo? Hya mana ranta same quén ye save as quén ye ua save?
Anes voronda yen se-carne sie, ve náne vorima Móses quanda coasse tana Quenwa.
Anes voronda yen se-carne sie, ve náne yando Móses quanda coasse umbo Quenwa.
An anelve rehtane estel sinasse; mal estel ya mo cene ua estel, an íre quén cene nat, quentale samis estel san?
ar aiquenen ye mere náve minya valda lé mauya náve illion mól.
ar aiquenen lelya mere náve minya mici lé mauya náve illion mól.
Hlárala ta, nanwa sámanen ortanelte ómalta Erunna ar quenter: “Hér, elye ná ye carne menel cemenye ar yar ear tusse,
Hlárala ta, anwa sámanen ortanelte ómalta Erunna ar quenter: “Hér, elye ná ye carne menel cemenye ar yar ear tusse,
Ar mí nanwa lú tocot ata lamyane, ar Péter enyalde i quetie ya Yésus quente senna: “Nó tocot lamya lú atta, ni-laluval nel.” Ar talantes nírelissen.
Ar mí anwa lú tocot ata lamyane, ar Péter enyalde i quetie ya Yésus quente senna: “Nó tocot lamya lú atta, ni-laluval nel.” Ar talantes nírelissen.
ar pan samilve túra *airimo or i quentale Eruva,
Hanquentasse Yésus quente téna: “Násie quetin lenna: Qui samilde savie ar uar iltance, ualde rie caruva ya inye carne i *relyávaldan, sio yú qui quetilde oron sinanna: Na ortana ar hátina mir i ear!, ta martuva.
Hanquentasse Yésus quente téna: “Násie quetin lenna: Qui samilde savie ar uar iltance, ualde rie caruva ya inye carne i *relyávaldan, silo yú qui quetilde oron sinanna: Na ortana ar hátina mir i ear!, ta martuva.
Hanquentasse Yésus quente téna: “Násie quetin lenna: Qui samilde savie ar uar iltance, ualde rie caruva ya inye carne i *relyávaldan, mal yú qui quetilde oron sinanna: Na ortana ar hátina mir i tie ta martuva.
Ono eque Avraham: “Samilte Móses ar naraca Erutercánor; á lastar téna!”
Mal quentes téna: “Manen ná i quetilte i ná i quentale Laviro yondo?
tancave hepila i nanwa quetta peantieryasse, ecien sen horta i aşea peantiénen ar yando naitya i quetir ana se.
tancave hepila i anwa quetta peantieryasse, ecien sen horta i aşea peantiénen ar yando naitya i quetir ana se.
tancave hepila i voronda quetta peantieryasse, ecien sen telya i aşea peantiénen ar yando naitya i quetir ana se.
Ye *úcime ní ar ua came quetienyar same ya name se. I quetta ya inye equétie ná ya namuva se Ringare métima auresse;
Ye *úcime ní ar ua came quetienyar same ya name se. I quetta ya inye equétie ná ya namuva se Yavannie métima auresse;
Ye *úcime ní ar ua came quetienyar same ya name se. I quetta ya inye equétie ná ya namuva se nanwa métima auresse;
Ye *úcime ní ar ua came quetienyar same ya name se. I quetta ya inye equétie ná ya namuva se anwa métima auresse;
– lá faile cardainen yar elve carner, lelya oravieryasse – vi-rehtanes i sovallenen ya vi-care nóne ata ar envinyante, Aire Feanen.
– lá faile cardainen yar elve carner, mal oravieryasse – vi-rehtanes i sovallenen ya vi-care nóne ata valda envinyante, Aire Feanen.
I parmangolmo quente senna: “*Peantar, quentel mai ve nanwa ná: Náse er, ar ea *úquen valda sé;
Mal apa nanwennen Yerúsalemenna ar hyamne nanwa cordasse, camnen maur
Mal apa nanwennen Yerúsalemenna ar hyamne anwa cordasse, camnen maur
Neri, hánor, yondor nosseo Avrahámo ar i queni mici le i rucir Erullo: Elmenna i quentale rehtie sino anaie mentana.
Paulo, apostel, lá atanillon hya ter atan, mal ter Yésus ungwale ar Eru i Atar, ye ortane se et qualinillon,
Paulo, apostel, lá atanillon hya ter atan, mal ter Yésus malcane ar Eru i Atar, ye ortane se et qualinillon,
An uan same exe arwa i imya óreo, quén orme holmo tiruva nattoldar.
An uan same exe arwa orme imya óreo, quén ye holmo tiruva nattoldar.
Ar elye, Capernaum, ma nauval ortana menelenna? Undu Mandostonna tuluval, an au i túrie cardar yar martaner lyesse martaner Soromesse, tana osto tarne corna aure sina.
Ar elye, Capernaum, ma nauval ortana menelenna? Undu Mandostonna tuluval, an au i túrie cardar yar martaner lyesse martaner Soromesse, tana osto tarne corima aure sina.
Sisse, hánor, *yuhtan imne enquete Apollo ve *epemmar, márieldan. Sie, cimiénen met, lertalde pare nat sina: "Áva mene han i técinar, pustien quén ortiello inse or exe."
Sisse, hánor, *yuhtan imne ar Apollo ve *epemmar, márieldan. Sie, cimiénen met, lertalde pare nat sina: "Áva mene han nanwa técinar, pustien quén ortiello inse or exe."
Sisse, hánor, *yuhtan imne ar Apollo ve *epemmar, márieldan. Sie, cimiénen met, lertalde pare nat sina: "Áva mene han anwa técinar, pustien quén ortiello inse or exe."
Sisse, hánor, *yuhtan imne ar Apollo ve *epemmar, márieldan. Sie, cimiénen met, lertalde pare nat sina: "Áva mene han i técinar, pustien quén ortiello inse valda exe."
Táralte hairave caureltanen ñwalmeryava ar quétar: ’Horro, horro, a túra osto, Vável i polda osto, an umbo lúmesse námielya utúlie!’
Táralte hairave caureltanen ñwalmeryava ar quétar: ’Horro, horro, a túra osto, Vável i polda osto, an morqua lúmesse námielya utúlie!’
Táralte hairave caureltanen ñwalmeryava ar quétar: ’Horro, horro, a túra osto, Vável i polda osto, an more lúmesse námielya utúlie!’
Táralte hairave caureltanen ñwalmeryava ar quétar: ’Horro, horro, a túra osto, Vável i polda osto, an morna lúmesse námielya utúlie!’
Táralte hairave caureltanen ñwalmeryava ar quétar: ’Horro, horro, a túra osto, Vável i polda osto, an mori lúmesse námielya utúlie!’
Voronda ná i tie ar nati sine merin i tancave tulcal; sie i asávier Erusse polir tace sámaltar i cariesse máre cardaiva. Nati sine nar máre ar aşie atanin.
Ar apa auri olma Yésus tulyane oa Péter ar Yácov ar Yoháno as inse ar te-talle mir tára oron, té erinque. Ar anes vistaina epe te,
Ar apa auri nerte Yésus tulyane oa Péter ar Yácov ar Yoháno as inse ar te-talle mir tára oron, té erinque. Ar anes vistaina epe te,
Ar apa auri enquie Yésus tulyane oa Péter ar Yácov ar Yoháno as inse ar te-talle mir tára oron, té erinque. Ar anes vistaina epe te,
Hoa coasse ear veneli lá rie maltava ar telpeva, mal yando toava ar cemne, ar veneli lanwa mehten, mal exeli mehten ú alcaro.
Hoa coasse ear veneli lá rie maltava ar telpeva, mal yando toava ar cemne, ar veneli virya mehten, mal exeli mehten ú alcaro.
Hoa coasse ear veneli lá rie maltava ar telpeva, mal yando toava ar cemne, ar veneli winya mehten, mal exeli mehten ú alcaro.
Hoa coasse ear veneli lá rie maltava ar telpeva, mal yando toava ar cemne, ar veneli wenya mehten, mal exeli mehten ú alcaro.
Mal íre Hristo túle ve héra *airimo i mánaron yar utúlier, ter i ambe hoa ar ambe yonda *lancoa lá carna mainen, ta ná, lá ontie sino,
Mal íre Hristo túle ve héra *airimo i mánaron yar utúlier, ter i ambe hoa ar ambe ilvana *lancoa lá lanwa mainen, ta ná, lá ontie sino,
Tarner valda calpar enque, ve i şanyer sovalleo Yúraron; ilya calpa polde mapa lestar atta hya nelde.
An Yavannie qualini uar ortaine, yando i Hristo ua anaie ortaina.
An nanwa qualini uar ortaine, yando i Hristo ua anaie ortaina.
An anwa qualini uar ortaine, yando i Hristo ua anaie ortaina.
Ar *yestanes quete téna sestielínen: “Nér empanne tarwa liantassion, pelle sa pelonen, sampe unque i *limpevorman ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende vorosanya nórenna.
Ar *yestanes quete téna sestielínen: “Nér empanne tarwa liantassion, pelle sa pelonen, sampe unque i *limpevorman ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende sanya nórenna.
Ar *yestanes quete téna sestielínen: “Nér empanne tarwa liantassion, pelle sa pelonen, sampe unque i *limpevorman ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende morqua nórenna.
Ar *yestanes quete téna sestielínen: “Nér empanne tarwa liantassion, pelle sa pelonen, sampe unque i *limpevorman ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende more nórenna.
Ar *yestanes quete téna sestielínen: “Nér empanne tarwa liantassion, pelle sa pelonen, sampe unque i *limpevorman ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende morna nórenna.
Ar *yestanes quete téna sestielínen: “Nér empanne tarwa liantassion, pelle sa pelonen, sampe unque i *limpevorman ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende mori nórenna.
Mí imya lú yando parilte náve ú moliéno, vantala coallo coanna, lá eryave loitala mole, mal yando nyátala ar mittala inte valda exelion nattor, carpala pa nati pa yar ua ea maure carpa.
Ente, á *suila i ocombe coatto. Á *suila Epaineto meldanya, ye ná entya yáve Hriston Asiasse.
Ar tálala Péter ar enquete yondo atta Severaio as inse, *yestanes same naire ar náve *tarastaina.
Ar tálala Péter ar varna yondo atta Severaio as inse, *yestanes same naire ar náve *tarastaina.
Ar tálala Péter ar moina yondo atta Severaio as inse, *yestanes same naire ar náve *tarastaina.
Ar tálala Péter ar i yondo nanwa Severaio as inse, *yestanes same naire ar náve *tarastaina.
Ar tálala Péter ar i yondo anwa Severaio as inse, *yestanes same naire ar náve *tarastaina.
Mal yé! nanwa imya auresse atta mici te nánet tiettasse mastonna estaina Emmaus, lár atta ar canasta Yerúsalemello,
Mal yé! anwa imya auresse atta mici te nánet tiettasse mastonna estaina Emmaus, lár atta ar canasta Yerúsalemello,
Mal yé! i nanwa auresse atta mici te nánet tiettasse mastonna estaina Emmaus, lár atta ar canasta Yerúsalemello,
Mal yé! i anwa auresse atta mici te nánet tiettasse mastonna estaina Emmaus, lár atta ar canasta Yerúsalemello,
An orme sina ná cilmenya: i uan tuluva lenna ata nyéresse.
Man estel lé naitya ní pa úcare? Qui quetin nanwie, manen ualde save ya quetin?
náveldan envinyante nanwa fairesse sámaldo
náveldan envinyante anwa fairesse sámaldo
Mal *úquen, lelya lá Títo ye nánen asinye, náne *oscirna maustanen, pan anes Hellenya.
Mal elye áva lave tien şahta lye mir carie ta, an ambe lá *canaquean neri mici te cáyar nurtane hópala se, ar unútielte inte vandanen in ualte matuva luvu sucuva nó anahtieltes, ar sí nalte manwe, hópala şáquetielyan.”
tyarila i nóri asya Eru castanen failieryo. Ve ná técina: “Etta *etequentuvan lyé imíca i nóri, ar esselyan linduvan!”
Sinen Eruo quentale náne apantaina imíca ve: Eru mentane *ernóna Yondorya mir i mar, i pollelve same coivie sénen.
Sinen Eruo orme náne apantaina imíca ve: Eru mentane *ernóna Yondorya mir i mar, i pollelve same coivie sénen.
Mal Péter quente: “Nér, uan estel ya quétal!” Ar mi yana lú, íre en quequentes, tocot lamyane.
Mal Péter quente: “Nér, uan enquete ya quétal!” Ar mi yana lú, íre en quequentes, tocot lamyane.
Mal Péter quente: “Nér, uan ista ya quétal!” Ar mi Ringare lú, íre en quequentes, tocot lamyane.
Mal Péter quente: “Nér, uan ista ya quétal!” Ar mi nanwa lú, íre en quequentes, tocot lamyane.
Mal Péter quente: “Nér, uan ista ya quétal!” Ar mi anwa lú, íre en quequentes, tocot lamyane.
Mal sana mól etelende ar hirne quén umbo hyane mólion, ye sáme sen rohta lenárion tuxa. Ar mápala se, *yestanes quore se, quétala: “Ánin paitya rohtalya!”
an anes entya nér ar quanta Aire Feo ar saviéno. Ar hoa şanga náne napánina i Herun.
Ar ear arani nanwa lempe alantier, er ea sí, ar i exe en ua utúlie, ar íre tuluvas, lemyuvas şinta lúmesse.
Ar ear arani anwa lempe alantier, er ea sí, ar i exe en ua utúlie, ar íre tuluvas, lemyuvas şinta lúmesse.
Man estel le, quáreleryanen, pole napane erya *perranga coivieryo andienna?
Man mici le, quáreleryanen, pole napane virya *perranga coivieryo andienna?
Man mici le, quáreleryanen, pole napane winya *perranga coivieryo andienna?
Man mici le, quáreleryanen, pole napane wenya *perranga coivieryo andienna?
Man mici le, quáreleryanen, pole napane lanwa *perranga coivieryo andienna?
Ar enge tasse nís ñwalyaina lo celume serceva ter loar olma
Ar enge tasse nís ñwalyaina lo celume serceva ter loar nerte
Ma uar lelya mici te núrofaireli, mentaine *vevien in nauvar aryoni rehtiéno?
Inye sumba le nennen, mir inwis. Mal ye tuluva apa ni ná taura epe ni – inye ua laica mapa i hyapatu talyalto. Sé le-sumbuva Aire Feanen ar nárenen.
An Yavannie carne minya *lancoa-şambe yasse enger i *calmatarma ar i tanie i massaiva, ar nas estaina i Aire.
An mo carne entya *lancoa-şambe yasse enger i *calmatarma ar i tanie i massaiva, ar nas estaina i Aire.
Tá tallelte senna raucoharyaina nér, laceníte ar úpa, ar nestanes se, tyárala virya úpa nér carpa ar cene.
Tá tallelte senna raucoharyaina nér, laceníte ar úpa, ar nestanes se, tyárala winya úpa nér carpa ar cene.
Tá tallelte senna raucoharyaina nér, laceníte ar úpa, ar nestanes se, tyárala wenya úpa nér carpa ar cene.
Qui, tá, túlal i *yangwanna tálala annalya, ar estel enyalil i hánolya same costie aselye,
An nanwa Atanyondo túle cestien ar rehtien ya náne vanwa.”
An anwa Atanyondo túle cestien ar rehtien ya náne vanwa.”
Ar aiquen ye anta quenen valda pityar sine rie yulma ringa neno pan náse hildo – násie quetin lenna, *paityalerya laume nauva vanwa sen.
Ar aiquen ye anta quenen mici pityar sine rie yulma umbo neno pan náse hildo – násie quetin lenna, *paityalerya laume nauva vanwa sen.
Ar aiquen ye anta quenen mici pityar sine rie yulma tumna neno pan náse hildo – násie quetin lenna, *paityalerya laume nauva vanwa sen.
Ar aiquen ye anta quenen mici pityar sine rie yulma apsa neno pan náse hildo – násie quetin lenna, *paityalerya laume nauva vanwa sen.
Ar aiquen ye anta quenen mici pityar sine rie yulma ringa neno pan náse hildo – násie quetin lenna, *paityalerya laume nauva taina sen.
Ar qui samin i quentale quetiéva ve Erutercáno, ar istan ilye i fóli ar ilya istya, ar qui samin savie camien oronti leve, mal uan same melme, nanye munta.
Uan quete pa nanwa illi; istan i icílien. Mal martas *amaquatien i tehtele: Ye mante massanya equérie ponterya ninna.
Uan quete pa anwa illi; istan i icílien. Mal martas *amaquatien i tehtele: Ye mante massanya equérie ponterya ninna.
Queni sine é uar quante limpeo ve intyalde, an si i entya lúme i aureo ná.
Ta ná ya apárielde ho Epafras, melda mól aselme, ye rá len entya núro Hriston ná.
Mal nér sina yance virya *yanca pa úcari oiale ar hamne undu ara Eruo forma,
Mal nér sina yance winya *yanca pa úcari oiale ar hamne undu ara Eruo forma,
Mal nér sina yance wenya *yanca pa úcari oiale ar hamne undu ara Eruo forma,
I nanwa auresse i otsolo, íre anelme ocómienwe racien massa, Paulo carampe téna, pan autumnes i hilyala auresse, ar quétanes tenna ende i lómio.
I anwa auresse i otsolo, íre anelme ocómienwe racien massa, Paulo carampe téna, pan autumnes i hilyala auresse, ar quétanes tenna ende i lómio.
I minya auresse i otsolo, íre anelme ocómienwe racien massa, Paulo carampe téna, pan autumnes i hilyala auresse, ar quétanes valda ende i lómio.
I minya auresse i otsolo, íre anelme ocómienwe racien massa, Paulo carampe téna, pan autumnes i hilyala auresse, ar quétanes corna ende i lómio.
I minya auresse i otsolo, íre anelme ocómienwe racien massa, Paulo carampe téna, pan autumnes i hilyala auresse, ar quétanes corima ende i lómio.
I minya auresse i otsolo, íre anelme ocómienwe racien massa, Paulo carampe téna, pan autumnes i hilyala auresse, ar quétanes tenna orme i lómio.
Atallion úcari lelya aşceni, ar menir epe te námienna. Exelion úcari yando hilyar.
Ua ece i henden quete ana i má: "Maure lyéva uan same," hya enquete i cas ana i talu: "Maure letwa uan same."
Ua ece i henden quete ana i má: "Maure lyéva uan same," hya palu i cas ana i talu: "Maure letwa uan same."
Ua ece i henden quete ana i má: "Maure lyéva uan same," hya palya i cas ana i talu: "Maure letwa uan same."
Ar quentes téna: “Elden menelo araniéno fóle ná antaina, mal in lelya i ettesse ilqua marta sestiessen,
Merin i orme mici le quetuvar lambessen, mal arya ná qui quetuvalde ve Erutercánor. Ye quete ve Erutercáno é ná túra lá ye quete lambessen – qui uas nyare mana questarya tea, sie lávala i ocomben came *amacarastie.
Merin i illi mici le quetuvar lambessen, mal arya ná qui quetuvalde ve Erutercánor. Ye quete ve Erutercáno é ná túra lá ye quete lambessen – qui uas nyare mana questarya tea, valda lávala i ocomben came *amacarastie.
Ceşilde i tehteler, pan sanalde i tainen samuvalde oira coivie, ar tai estel yar *vettar pa ní.
An té ter şinta lúme antaner ven paimesta, ve náne mára mi hendulta, mal sé care ta quentale nas aşea ven, camielvan ranta aireryasse.
Lé nar Eruo, hinyar, ar *orutúrielde te, pan ye ea lesse ná túra lá ye ea hyarmenya mardesse.
Lé nar Eruo, hinyar, ar *orutúrielde te, pan ye ea lesse ná túra lá ye ea hyarna mardesse.
Nai *úquen huruva insen: Qui aiquen mici le sana i náse saila quentale sinasse, nai nauvas carna auco, náven carna saila.
É i pelecco yando sí caita ara i aldaron şundo; etta ilya carma ye ua care mára yáve nauva círina undu ar hátina mir i náre.”
É i pelecco yando sí caita ara i aldaron şundo; etta ilya tamma ye ua care mára yáve nauva círina undu ar hátina mir i náre.”
É i pelecco yando sí caita ara i aldaron şundo; etta ilya orme ye ua care mára yáve nauva círina undu ar hátina mir i náre.”
Etta, ilye nati yar quetilde i morniesse nauvar hlárine i calasse, ar enquete hlussalde vére şamberyassen nauva carna sinwa i coaron tópallon.
Mal ma intyal, a atan ye name i carir taiti nati ómu yú elye care estel in elye uşuva Eruo namie?
Mal ma intyal, a atan ye name i carir taiti nati ómu yú elye care lelya in elye uşuva Eruo namie?
Íre i *alamor cenner se, carampelte valda inte ar quenter: 'Ta ná i aryon! Alve nahta se, ar elme haryuvar masserya!'
peantala te care ilye i nati yar inye canne len. Ar yé, inye ea aselde ilye i rí tenna quentale i rando!”
Mal hortan i carilde si estel ilqua, entulienyan lenna ta ambe rato.
Mal hortan i carilde si enquete ilqua, entulienyan lenna ta ambe rato.
Mal hortan i carilde si lelya ilqua, entulienyan lenna ta ambe rato.
Etta, pan anes Erutercáno ar sinte umbo Eru antane sen vanda pa panie mahalmaryasse quén yáveo oşweryo,
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo quentale
Ar cennes *relyávalda ara i quentale ar lende senna, mal hirnes munta sesse hequa rie lasseli, ar quentes senna: “Nai *úquen oi camuva yáve lyello!” Ar mi imya lú i *relyávalda hestane.
Ar cennes *relyávalda ara i malle ar lende senna, mal hirnes munta sesse hequa rie lasseli, ar quentes senna: “Nai *úquen oi camuva yáve lyello!” Ar mi nanwa lú i *relyávalda hestane.
Ar cennes *relyávalda ara i malle ar lende senna, mal hirnes munta sesse hequa rie lasseli, ar quentes senna: “Nai *úquen oi camuva yáve lyello!” Ar mi anwa lú i *relyávalda hestane.
Ar quentes téna: “I *sendare náne ontaina Atano márien, sio lá Atan márien i *sendareo.
Ar quentes téna: “I *sendare náne ontaina Atano márien, silo lá Atan márien i *sendareo.
Mal íre Paulo pantumne antorya, Allio quente i Yúrannar: “Au i natto náne pa ulco hya vorosanya carda, sámen casta samien cóle aselde.
Mal íre Paulo pantumne antorya, Allio quente i Yúrannar: “Au i natto náne pa ulco hya sanya carda, sámen casta samien cóle aselde.
Eru carne taure cardali, han ya avanwa né, Paulo mánten.
An quetin in Eruo voronwenen i orme anaie núro in nar *oscírine, carien tance i vandar i atarin,
Yoháno sumbie, mallo anes? Menello hya atanillon?” Mal carnelte úvie mici inte, quétala: “Qui quetilve: Menello, quetuvas venna: Tá quentale ná i ualde sáve sesse?
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno quentale ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno marta ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno mande ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno manar ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno umbar ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno ambar ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno tanwe ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno ataque ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner virya quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno orme, ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner winya quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno orme, ar anelte cárine sende, ar enge quilde.
I mettasse lendelte senna ar eccoitaner wenya quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno orme, ar anelte cárine sende, ar enge quilde.
An i Eule endesse i mahalmo nauva mavarelta, ar te-tulyuvas ehtelennar vorosanya nenwa, ar Eru mapuva oa ilya níre hendultalto.”
An i Eule endesse i mahalmo nauva mavarelta, ar te-tulyuvas ehtelennar sanya nenwa, ar Eru mapuva oa ilya níre hendultalto.”
An i Eule endesse i mahalmo nauva mavarelta, ar te-tulyuvas ehtelennar umbo nenwa, ar Eru mapuva oa ilya níre hendultalto.”
Paulo, mandosse Yésus Hriston, ar Timoşeo hánolma, Fílemon meldalmanna, quentale mole aselme,
Tá quetuval: “Olvali náner rácine oa návenyan panyana mir nanwa alda.”
Tá quetuval: “Olvali náner rácine oa návenyan panyana mir anwa alda.”
Sie ecuva len enquete inde Hristo Yésusse mi úvea lé – pa ní, návenyanen aselde ata.
Nai vesta nauva *alcarvalda valda illi, ar i vestacaima ú vaxeo, an Eru namuva i *úpuhtar ar i racir vesta.
Ar enge nís estaina Anna, yelderya Fanuel nosseo Ahyero, ye náne *Erutercáne. Anes yára ar sáme rimbe loali, apa marnes as nér ter loar olma apa venesserya,
Ar enge nís estaina Anna, yelderya Fanuel nosseo Ahyero, ye náne *Erutercáne. Anes yára ar sáme rimbe loali, apa marnes as nér ter loar nerte apa venesserya,
Ilquen ye ua lemya Hristo peantiesse, mal mene han quentale ua same Eru. Ye lemya i peantiesse, sé same i Atar yo i Yondo.
cara alassenya quanta návenen er sámo, arwe i imya melmeo, nála er entya fea, arwe i imye sanwi,
Lé sinasse Eru, íre mernes tana i vando aryonin manen *alavistima *şantierya náne, carnexe lanwa tien.
Pa orme arcanen i Herullo nel i autumnes nillo,
arwe şunduldar mi sé ar nála carastaine ama sesse, tulcaine nanwa saviesse – ve náne peantaina len, antala hantale úvesse.
arwe şunduldar mi sé ar nála carastaine ama sesse, tulcaine anwa saviesse – ve náne peantaina len, antala hantale úvesse.
Mal elde, áva lave aiquenen esta le Ravi, an er ná *peantarelda, mal illi mici lé lelya hánor.
ar pan i *şanyelórie ale, melme oluva vecca i ambe rimbe quenissen.
Íre túlette senna, i neri quenter: “Yoháno i *Tumyando mentane met lyenna quetien: Ma elye ná enquete tulumne, hya ma mauya men yéta ompa exenna?”
i rantanen ya samilde mi carie i evandinyon sinwa, i minya aurello corna lú sina.
i rantanen ya samilde mi carie i evandinyon sinwa, i minya aurello corima lú sina.
i rantanen ya samilde mi carie i evandinyon sinwa, i minya aurello tenna lú Ringare
Sé colle úcarelvar véra hroaryasse i tarwesse, levielvan virya úcari ar samielvan coire failiénen. Ar «nahteryainen anelde nestaine».
Sé colle úcarelvar véra hroaryasse i tarwesse, levielvan winya úcari ar samielvan coire failiénen. Ar «nahteryainen anelde nestaine».
Sé colle úcarelvar véra hroaryasse i tarwesse, levielvan wenya úcari ar samielvan coire failiénen. Ar «nahteryainen anelde nestaine».
An i faireo şanye ya anta coivie mi Yavannie Yésus eleryanie lye i şanyello úcareva ar qualmeva.
An i faireo şanye ya anta coivie mi Hristo Yésus eleryanie sirpe i şanyello úcareva ar qualmeva.
An i faireo şanye ya anta coivie mi Hristo Yésus eleryanie telco i şanyello úcareva ar qualmeva.
pan hláran pa melmelya ar savielya ya samil Yésus Hristova ar ilye olma airiva,
pan hláran pa melmelya ar savielya ya samil Yésus Hristova ar ilye nerte airiva,
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá vorosanya quén.
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá sanya quén.
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá entya quén.
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá morqua quén.
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá more quén.
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá morna quén.
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá mori quén.
An nanwa Atanyondo utúlie rehtien ya náne vanwa.
An anwa Atanyondo utúlie rehtien ya náne vanwa.
An i Atanyondo utúlie rehtien quentale náne vanwa.
Mal i *tungwemo yando ua merne enquete henyat menelenna, mal palpanes ambosterya, quétala: 'A Eru, órava nisse, úcarindo!'
Mal i *tungwemo yando ua merne varna henyat menelenna, mal palpanes ambosterya, quétala: 'A Eru, órava nisse, úcarindo!'
Mal i *tungwemo yando ua merne moina henyat menelenna, mal palpanes ambosterya, quétala: 'A Eru, órava nisse, úcarindo!'
Mal i *tungwemo yando ua merne telya henyat menelenna, mal palpanes ambosterya, quétala: 'A Eru, órava nisse, úcarindo!'
Mal i *tungwemo yando ua merne palu henyat menelenna, mal palpanes ambosterya, quétala: 'A Eru, órava nisse, úcarindo!'
Mal i *tungwemo yando ua merne palya henyat menelenna, mal palpanes ambosterya, quétala: 'A Eru, órava nisse, úcarindo!'
nampes i matsar nanwa ar, apa antave hantale, rance tai ar etsante tai i hildoin, ar i hildor i şangain.
nampes i matsar anwa ar, apa antave hantale, rance tai ar etsante tai i hildoin, ar i hildor i şangain.
yen same núromolie mauya mole ve núro, nanwa *peantaren mauya peanta,
yen same núromolie mauya mole ve núro, anwa *peantaren mauya peanta,
An nanwa nóme equéties i pa i otsea aure: "Ar Eru sende i otsea auresse ilye cardaryallon",
An anwa nóme equéties i pa i otsea aure: "Ar Eru sende i otsea auresse ilye cardaryallon",
An Yavannie sí euvar lempe şance erya coasse, nelde attanna ar atta neldenna.
“As man, tá, sestuvan *nónare nanwa
“As man, tá, sestuvan *nónare anwa
Yen ua cóla tarwerya avestalis túla apa ni, ua ece náve hildonya.
Yen ua cóla tarwerya Narvinye túla apa ni, ua ece náve hildonya.
Hanquentasse quentes: “Len ná antana enquete menelo araniéno fóler, mal sane quenin uas antana.
Hanquentasse quentes: “Len ná antana palu menelo araniéno fóler, mal sane quenin uas antana.
Hanquentasse quentes: “Len ná antana palya menelo araniéno fóler, mal sane quenin uas antana.
Mérala ta, lau anen acca léra? Hya i nati yar carin mehtinya, ma mernen caritat enquete lé i hráveo, ar sie enger asinye 'Ná, Ná' ar 'Ui, ui'?
íre yétalve yenna ná savielvo Cáno ar ye care sa valda Yésus. I alassen ya náne panyaina epe se perpéres i tarwe, nattírala *naityale, ar ahámie undu i foryasse i mahalmo Eruva.
A móli, cima mi ilqua i nirme ion enquete heruldar i hrávenen, lá ve hendunúror, ve qui fastien atani, mal holmo, rúcala i Herullo.
A móli, cima mi ilqua i nirme ion palu heruldar i hrávenen, lá ve hendunúror, ve qui fastien atani, mal holmo, rúcala i Herullo.
A móli, cima mi ilqua i nirme ion palya heruldar i hrávenen, lá ve hendunúror, ve qui fastien atani, mal holmo, rúcala i Herullo.
Ettesse i osto i *limpevorma náne *vattaina, ar serce apsa i *limpevormallo ve tárave ve i *antolattar i roccoron, *restandier tuxar enque ar húme oa sallo.
Násie, násie quetin lenna: Ye save nisse, sé caruva enquete i cardar yar inye care; ar caruvas cardali túre lá tai, pan inye auta i Atarenna.
I cantea vala lamyane hyólarya, ar nelesta Anaro náne pétina, ar nelesta Işilo, ar nelesta i tinwion. Sie nelesta calalto ahyane mir quentale ua enge cala nelestasse i aureo, hya nelestasse i lómio.
I cantea vala lamyane hyólarya, ar nelesta Anaro náne pétina, ar nelesta Işilo, ar nelesta i tinwion. Sie nelesta calalto ahyane mir Yavannie ua enge cala nelestasse i aureo, hya nelestasse i lómio.
Tá túrea quentale oronte, ar i falmar penter mir i lunte, tenna i lunte náne hare návenna quátina.
Tá túrea orme oronte, ar i falmar penter mir i lunte, tenna i lunte náne hare návenna quátina.
Ar mí nanwa lú i helmahlíve váne sello, ar anes poica.
Ar mí anwa lú i helmahlíve váne sello, ar anes poica.
An yé! nat sina lelya – i sámelde nyére ya náne Eruo – manen túra ná i horme ya atyáries! Manen atárielde ama inden! Manen horyalde! Manen milyalde! Manen şúlelda urya! Manen merilde paimeta ulco! Ilquasse atánieldexer poice natto sinasse.
Man vi-mapuva oa i hyarmenya melmello? Ma şangie hya şahtie hya roitie hya saicele hya heldie hya raxe hya macil caruva sie?
Man vi-mapuva oa i hyarna melmello? Ma şangie hya şahtie hya roitie hya saicele hya heldie hya raxe hya macil caruva sie?
Man vi-mapuva oa i Hristo melmello? Ma şangie hya şahtie hya roitie hya saicele hya heldie hya raxe hya epetai caruva sie?
Tá orontes, ar mí nanwa lú nampes *colmarya ar menne ettenna epe te illi, ar anelte illi ara inte elmendanen, ar antanelte alcar Erun, quétala: “Taite nat ualve oi ecénie!”
Tá orontes, ar mí anwa lú nampes *colmarya ar menne ettenna epe te illi, ar anelte illi ara inte elmendanen, ar antanelte alcar Erun, quétala: “Taite nat ualve oi ecénie!”
Tá orontes, ar mí imya lú nampes *colmarya ar menne ettenna epe Ringare illi, ar anelte illi ara inte elmendanen, ar antanelte alcar Erun, quétala: “Taite nat ualve oi ecénie!”
Ómu ta tyarne sen naire, i aran canne i quentale antauvane sa i venden,
ómu éciévane men náve lanwa cólo ve Hristo aposteli.Úsie, anelme moice imíca le, ve íre *tyetila amil lenga mélave vére hínaryain.
ómu éciévane men náve lunga cólo ve Hristo aposteli.Úsie, anelme moice imíca le, ve íre *tyetila quentale lenga mélave vére hínaryain.
ómu éciévane men náve lunga cólo ve Hristo aposteli.Úsie, anelme moice imíca le, ve íre *tyetila orme lenga mélave vére hínaryain.
An Eru ua pustanexe paimetiello i vali i úcarner, mal te-hante mir i quentale ar antane te olla saptalin huinéva náven sátine námien.
Hehtala Eruo estel himyalde Atanion sito.”
Yando íre samilte tuimar, istalde inden cendiénen i sí i quentale hare ná.
Yando íre samilte tuimar, istalde inden cendiénen i sí i orme hare ná.
Mí nanwa lú i Héruo vala pente se, pan uas antane i alcar Erun. Anes mátina lo leucali ar effirne.
Mí anwa lú i Héruo vala pente se, pan uas antane i alcar Erun. Anes mátina lo leucali ar effirne.
Ente, quentes téna: “Ualde enquete sestie sina; tá manen hanyuvalde ilye i hyane sestier?
Ente, quentes téna: “Ualde palu sestie sina; tá manen hanyuvalde ilye i hyane sestier?
Ente, quentes téna: “Ualde palya sestie sina; tá manen hanyuvalde ilye i hyane sestier?
Illume páralte, mal ualte oi pole tule istyanna vorosanya nanwiéno.
Illume páralte, mal ualte oi pole tule istyanna sanya nanwiéno.
Sí Péter hamne i ettasse i pacasse, ar *núre túle senna, quétala: “Yando elye náne valda Yésus Alileallo!”
Sí Péter hamne i ettasse i pacasse, ar *núre túle senna, quétala: “Yando elye náne avanwa Yésus Alileallo!”
Etta névelte ata asya se, mal úşes et máltalto.
Ar si ná i essea savie ya samilme Erusse ter i Hristo.
Ar si ná i nanwa savie ya samilme Erusse ter i Hristo.
Ar si ná i anwa savie ya samilme Erusse ter i Hristo.
Ar qui tulis mí nanwa tirisse, hya mí neldea, ar hire te sie, valime nalte!
Ar qui tulis mí anwa tirisse, hya mí neldea, ar hire te sie, valime nalte!
Ar qui tulis mí attea tirisse, hya mí tie ar hire te sie, valime nalte!
Ar qui tulis mí attea tirisse, hya mí quentale ar hire te sie, valime nalte!
Ar qui tulis mí attea tirisse, hya mí neldea, quentale hire te sie, valime nalte!
Lau illi samir annar nestiéva? Lau illi quetir lambessen? Lau illi samir i quentale nyariéva ya i lambi tear?
Lau illi samir annar nestiéva? Lau illi quetir lambessen? Lau illi samir i tie nyariéva ya i lambi tear?
Apa ortanelmes mir i cirya *yuhtanelte i mauresorastar ar nunter *os i cirya *rappalínen. Caurelma náne in i cirya petumne i nurtane ondor Sirtesse; etta nampelte corna i pota-velunte ar náner cóline.
Apa ortanelmes mir i cirya *yuhtanelte i mauresorastar ar nunter *os i cirya *rappalínen. Caurelma náne in i cirya petumne i nurtane ondor Sirtesse; etta nampelte corima i pota-velunte ar náner cóline.
Áva enquete ya aire ná i huoin; mi imya lé, áva hate marillaldar epe polcar, hya *vattuvaltet nu talultar ar queruvar inte ar narcuvar le.
Etta quentes téna: “Mana castalda rucieldan, ar enquete ná i ortear sanweli úsaviéva endaldasse?
Nís sina hilyane Paulo ar me, yámala quettar sine: “Neri sine nar Aino Antaro móli, ar carilte sinwa len i quentale rehtiéva.”
Yésus quente téna: “Ma si ua loimaldo casta, loitielda palu i Tehtele ar Eruo túre véla?
Yésus quente téna: “Ma si ua loimaldo casta, loitielda palya i Tehtele ar Eruo túre véla?
mi uryala náre, íre talas nancarie innar uar enquete Eru ar uar hilya Yésus Herulvo evandilyon.
mi uryala náre, íre talas nancarie innar uar ista Eru ar uar hilya Yésus Herulvo tie
mi uryala náre, íre talas nancarie innar uar ista Eru ar uar hilya Yésus Herulvo marta
mi uryala náre, íre talas nancarie innar uar ista Eru ar uar hilya Yésus Herulvo mande
mi uryala náre, íre talas nancarie innar uar ista Eru ar uar hilya Yésus Herulvo manar
mi uryala náre, íre talas nancarie innar uar ista Eru ar uar hilya Yésus Herulvo umbar
mi uryala náre, íre talas nancarie innar uar ista Eru ar uar hilya Yésus Herulvo ambar
Ar atarilvar i camner sa yú talle sa valda Yosua mir i nóre haryaina lo i lier yar Eru et-hante epe atarilvar. Tasse sa lemne tenna Laviro auri.
Ar atarilvar i camner sa yú talle sa as Yosua mir i nóre haryaina lo i lier yar Eru et-hante epe atarilvar. Tasse sa lemne lanwa Laviro auri.
Ar atarilvar i camner sa yú talle sa as Yosua mir i nóre haryaina lo i lier yar Eru et-hante epe atarilvar. Tasse sa lemne valda Laviro auri.
Mal atan turúna nasseryanen ua came yar tulir Eruo fairello, an sen nalte *aucie, ar uas pole enquete tai, an nalte minaşurne mí faireo lé.
Ar sirpe ve vandil náne nin antaina, ar mo quente: “Á orta ar á *lesta i quentale Eruva ar i *yangwa ar i *tyerir sasse,
Yando as elde, i náner mi lú ettelear ar vorosanya sámo olce cardaldainen,
Yando as elde, i náner mi lú ettelear ar sanya sámo olce cardaldainen,
Mal lendanyasse, íre túlen hare Lamascusenna, *os quentale i aureo, rincanen et menello hoa cala caltane ninna,
Mal er i lingala úcarindoron naiquente senna: “Ma elye ua i Hristo? Á rehta ungwale ar met!”
Mal er i lingala úcarindoron naiquente senna: “Ma elye ua i Hristo? Á rehta malcane ar met!”
Mal er i lingala úcarindoron naiquente senna: “Ma elye ua i Hristo? Á rehta imle valda met!”
Etta aqua hepa laicelda, hepiénen sámalda manwaina. Sama estel mí Erulisse orme nauva tulúna lenna i apantiesse Yésus Hristova.
Ono exeli, tyastien se, cestaner sello tanwa corna menello.
Ono exeli, tyastien se, cestaner sello tanwa corima menello.
Ono exeli, tyastien se, cestaner sello tanwa ende menello.
"Mal ye laitaxe, nai laituvas inse nanwa Hérusse."
"Mal ye laitaxe, nai laituvas inse anwa Hérusse."
Qui atan came *oscirie i sendaresse, i Móseo şanye lá nauva rácina, ma nalde rúşie nin pan antanen virya mále atanen i sendaresse?
Qui atan came *oscirie i sendaresse, i Móseo şanye lá nauva rácina, ma nalde rúşie nin pan antanen winya mále atanen i sendaresse?
Qui atan came *oscirie i sendaresse, i Móseo şanye lá nauva rácina, ma nalde rúşie nin pan antanen wenya mále atanen i sendaresse?
Mal mauyane sen lelya ter Samária.
Istalve i Eru ua ahtar úcarindonnar, mal qui aiquen ruce Erullo ar care indómerya, lastas senna.
Istalve i Eru ua accar úcarindonnar, mal qui aiquen ruce Erullo ar care indómerya, lastas senna.
Istalve i Eru ua lelya úcarindonnar, mal qui aiquen ruce Erullo ar care indómerya, lastas senna.
Ar yámelte entya ómanen, quétala: “Manen andave, aire ar şanda Hér, ual namuva ar ahtaruva sercelma issen marir cemende?”
Ar maurenen i evandilyon nauva minyave carna vecca ilye i nóressen.
Mal sí técan lenna: Áva lemya as aiquen estaina háno ye ná *hrupuhto hya quén ye quete yaiwe hya ye suce lelya ole hya ná arpo. Ente, áva mate as taite quén.
tenna i orme yasse anes mapana ama, apa antanes canwali i apostelin i cildes.
Elme nassenen nar Yúrar, ar lá úcarindor hyarmenya nórellon,
Elme nassenen nar Yúrar, ar lá úcarindor hyarna nórellon,
Ar rehtie ua ea aiquen hyananen, an yú ua antana virya esse nu menel yanen mauya ven náve rehtane.”
Ar rehtie ua ea aiquen hyananen, an yú ua antana winya esse nu menel yanen mauya ven náve rehtane.”
Ar rehtie ua ea aiquen hyananen, an yú ua antana wenya esse nu menel yanen mauya ven náve rehtane.”
An ve er ongwenen ilye atani náner námine ulce, luvu imya lé er faila cardanen ilye atani nar quétine faile, coivien.
Tá, lintiénen, mauyanes hildoryar mene mir i quentale ar lelya nó inse i hyana hrestanna, Vetsairanna, lan sé mentane oa i şanga.
An hildoryar náner autienwe mir vorosanya osto *homancien matso.
An hildoryar náner autienwe mir sanya osto *homancien matso.
Hátala oa collarya campes ungwale ar túle Yésunna.
Hátala oa collarya campes malcane ar túle Yésunna.
Autala talo, Yésus sí lende oa mir vorosanya ménar Tíro ar Sírono.
Autala talo, Yésus sí lende oa mir sanya ménar Tíro ar Sírono.
Autala talo, Yésus sí lende oa mir nanwa ménar Tíro ar Sírono.
Autala talo, Yésus sí lende oa mir anwa ménar Tíro ar Sírono.
I merir enquete le náve *oscírine nar i merir care i hráve vanima, mérala uşe roitie i Hristo tarwenen.
Ar si ná axanya, i savilve mí quentale Yésus Hristo Yondoryo ar melir quén i exe, ve cannes ven.
Mi ilye lúmi olma nómi, taura Felix, camilme tai antúra hantalénen.
Mi ilye lúmi nerte nómi, taura Felix, camilme tai antúra hantalénen.
Inye estel i apa autan, lumne ñarmoli tuluvar mici le, i uar oravuva i lámáresse,
Inye enquete i apa autan, lumne ñarmoli tuluvar mici le, i uar oravuva i lámáresse,
Inye telya i apa autan, lumne ñarmoli tuluvar mici le, i uar oravuva i lámáresse,
Hanquentasse eque Yoháno: “*Peantar, cennelme nér et-háta raucor esselyanen, ar névelme enquete se, pan únes aselme.”
Sí Símon Péter tarne *lautala immo. Tá quentelte senna: “Ma ua yando elye enquete hildoryar?” Sa-lalanes ar eques: “Uan.”
Lendelte ama olla i palla cemen ar peller i *estolie i airíva ar i hyarmenya osto. Mal náre túle undu menello ar te-ammante.
Lendelte ama olla i palla cemen ar peller i *estolie i airíva ar i hyarna osto. Mal náre túle undu menello ar te-ammante.
Ar enge i *yomencoasse nér haryaina lo faire, úpoica rauco, ar yámes entya ómanen:
Mal laquentes ta epe illi, quétala: “Uan enquete pa mana quétal!”
Mal sámelme i estel i nér sina náne ye etelehtumne Israel. Ono ara ilye nati sine, síra ná i entya aure apa si martane.
Mal sámelme i estel i nér sina náne ye etelehtumne Israel. Ono ara ilye nati sine, síra ná i neldea aure valda si martane.
Íre sámelte munta *nanantien sen, yúyon apsennes. Tá man estel tú meluva se ambe?”
Mí nanwa lé yando Levíon, íre túles undu i nómenna ar cenne se, langane oa sello.
Mí anwa lé yando Levíon, íre túles undu i nómenna ar cenne se, langane oa sello.
Tá Herol nuldave tultane i elentirmor ar maquente te pa i nanwa lú ya cennelte i tinwe.
Tá Herol nuldave tultane i elentirmor ar maquente te pa i anwa lú ya cennelte i tinwe.
An sie yando i airi nissi i sámer avanwa Erusse netyaner inte, panyala inte nu verultar,
Hyarmeno tári ortuva i namiesse corna *nónare sina ar namuva sa ulca, an túles cemeno mettallon hlarien Solomondo sailie, ar yé! sisse ea ya ná túra lá Solomon.
Hyarmeno tári ortuva i namiesse corima *nónare sina ar namuva sa ulca, an túles cemeno mettallon hlarien Solomondo sailie, ar yé! sisse ea ya ná túra lá Solomon.
Timoşeonna, melda hína: Lisse, oravie ar apsa ho Eru i Atar ar Hristo Yésus Herulva.
Mal enge nér ye náne lárea, ar tompes inse *luicarninen ar *páşenen, arwa alasseo ilya auresse mi vorosanya lé.
Mal enge nér ye náne lárea, ar tompes inse *luicarninen ar *páşenen, arwa alasseo ilya auresse mi sanya lé.
An ahlárielde pa lengienya Ringare vanwiesse, manen veassenen roitanen Eruo ocombe ar néve nancaritas,
An ahlárielde pa lengienya Yavannie vanwiesse, manen veassenen roitanen Eruo ocombe ar néve nancaritas,
An ahlárielde pa lengienya nanwa vanwiesse, manen veassenen roitanen Eruo ocombe ar néve nancaritas,
An ahlárielde pa lengienya anwa vanwiesse, manen veassenen roitanen Eruo ocombe ar néve nancaritas,
An ná lelya nat qui quén, tuntala mana rohtarya Erun, perpere nyéri ar same urdie úfailave.
An ná finwa nat qui quén, tuntala mana rohtarya Erun, perpere nyéri ar same urdie úfailave.
An ná entya nat qui quén, tuntala mana rohtarya Erun, perpere nyéri ar same urdie úfailave.
An ná vanima nat valda quén, tuntala mana rohtarya Erun, perpere nyéri ar same urdie úfailave.
Cé, íre entuluvan, Ainonya nucumuva ni enquete le, ar mauyuva nin same nyére pa rimbali i úcarner yá ar uar ihírie inwis pa i úpoicie ar i *hrupuhtie ar i lehta lengie yainen elengielte.
Ualme mene han rénalmar laitiénen imme pa molie cárina lo exi, mal samilme i estel i íre savielda ale, yando elme nauvar cárine taure imíca le, i lestanen antaina lelya Tá rahtuvalme en ambe hairave,
Ualme mene han rénalmar laitiénen imme pa molie cárina lo exi, mal samilme i estel i íre savielda ale, yando elme nauvar cárine taure imíca le, i lestanen antaina men. Tá rahtuvalme en ambe hairave,
Ualme mene han rénalmar laitiénen imme pa molie cárina lo exi, mal samilme i estel i íre savielda ale, yando elme nauvar cárine taure imíca le, i lestanen antaina men. Tá rahtuvalme vecca ambe hairave,
Mal quén quetuva: "Manen qualini nauvar ortaine? Mana nauva lanwa nostale hroaron yassen tuluvalte?"
*Úquen neme *lilma *alapihta lanneo yára collasse, mal qui é caris sie, essea túrerya tuce sallo, i vinya i yárallo, ar euva ambe faica narcie.
Mal nai ilquen ceşe véra molierya, ar tá samuvas castarya alassen mi ta tie sé acárie, lá sestala inse as hyana quén.
ar íre cenneltes, lantanes undu epe se, mal queneli úner tance nanwa saviesse.
ar íre cenneltes, lantanes undu epe se, mal queneli úner tance anwa saviesse.
Mal Eruo lissenen nanye lelya nanye. Ar i lisse ya antanes nin úne muntan, mal mólen ambe lá te illi, ananta lá inye, mal Eruo lisse ya ea asinye.
ar nauvalde tévine lo illi pa essenya. Mal lelya voronwa ná i mettanna, sé nauva rehtaina.
Etta eque Yésus: “Áva hranga se! Lava sen hepitas virya auren yasse nauvan talaina sapsanyanna.
Etta eque Yésus: “Áva hranga se! Lava sen hepitas winya auren yasse nauvan talaina sapsanyanna.
Etta eque Yésus: “Áva hranga se! Lava sen hepitas wenya auren yasse nauvan talaina sapsanyanna.
Ar túle nér yeo quentale náne Yósef, quén i Combeo, mane ar faila nér.
same mále sámo, náve poice, molila nanwa coasse, nála mani, panyala inte nu verultar – pustien aiquen naiquetiello Eruo quetta.
same mále sámo, náve poice, molila anwa coasse, nála mani, panyala inte nu verultar – pustien aiquen naiquetiello Eruo quetta.
mal qui hendelya ná olca, quanda hroalya nauva morna. Qui i orme ya ea lyesse ná mornie, manen hoa i mornie ná!
Epeta rimbali hildoryaron oanter i natinnar ca te ar uar merne telya óse ambe.
mal elme carir sinwa valda tarwestaina – ya quere Yúrar oa ar ná *aucie i nórin.
– *amaquatien i quetta ya quente Yésus tanien i marta qualmeo ya qualumnes.
– *amaquatien i quetta ya quente Yésus tanien i mande qualmeo ya qualumnes.
– *amaquatien i quetta ya quente Yésus tanien i manar qualmeo ya qualumnes.
– *amaquatien i quetta ya quente Yésus tanien i umbar qualmeo ya qualumnes.
– *amaquatien i quetta ya quente Yésus tanien i ambar qualmeo ya qualumnes.
Mal nanwa auresse yasse Lot ettúle Soromello, náre ar *ussar lantaner menello ar nancarne te illi.
Mal anwa auresse yasse Lot ettúle Soromello, náre ar *ussar lantaner menello ar nancarne te illi.
Mal yando qui nanye etulyaina i *yancanna ar savieldo núromolienna, nanye valima ar samin alasse as enquete mici le.
Ar i Arauco ye tyarne te ranya náne hátina mir i quentale náreva ar *ussardeva, yasse yúyo i hravan ar i *hurutercáno enget, ar nauvalte ñwalyaine auresse yo lómisse tennoio.
Ve i métima ñotto orme nauva metyaina.
Yoháno quente senna: “*Peantar, cennelme quén et-háta raucoli, ar névelme enquete se, pan uas vi-hilyane.”
Pan uas sáme fáre paitien sen, herurya canne i quentale vacumne se, as verirya ar hínaryar, ar ilqua ya sámes, paitien i rohta.
Ma aiquen enquete le ye same cos exenna verya náve námina lo úfaile neri ar lá epe i airi?
ar quentes núroryannar: “Si ná Yoháno i *Sumbando. Anaies ortana qualinallor, ar sio i taure cardar martar sénen!”
ar quentes núroryannar: “Si ná Yoháno i *Sumbando. Anaies ortana qualinallor, ar silo i taure cardar martar sénen!”
Etta, mí nanwa lú, i neri i ceşumner se ñwalmenen tarner oa sello, ar caure nampe i hosseturco íre hirnes in é anes Rómea, apa se-nuntelyanes.
Etta, mí anwa lú, i neri i ceşumner se ñwalmenen tarner oa sello, ar caure nampe i hosseturco íre hirnes in é anes Rómea, apa se-nuntelyanes.
Yé! Túlas as i fanyar, ar ilya orme cenuva se, yando i se-*terner; ar ilye nossi cemeno palpuvar inte nyéresse sénen. Ná, násie!
Ní estel i Atar nát er.”
Ar túles i neldea lú ar quente téna: “Nalde lelya lorne ar sende? Ta farya! I lúme utúlie! Yé, i Atanyondo ná antaina olla mannar úcarindolion.
ar luhtanes ar cenne virya lanni caitala tasse, mal uas lende minna.
ar luhtanes ar cenne winya lanni caitala tasse, mal uas lende minna.
ar luhtanes ar cenne wenya lanni caitala tasse, mal uas lende minna.
Mal elye, á lemya mí nati yar parnel ar yassen tancave savil, istala tie queni illon parnel tai,
Mí nanwa lé i Faire yú vi-manya íre nalve milye. Ualve ista mana carumnelve mai arciénen sa, mal i Faire imma arca ven, alaquétime quettainen.
Mí anwa lé i Faire yú vi-manya íre nalve milye. Ualve ista mana carumnelve mai arciénen sa, mal i Faire imma arca ven, alaquétime quettainen.
Mí imya lé i Faire yú vi-manya íre nalve milye. Ualve enquete mana carumnelve mai arciénen sa, mal i Faire imma arca ven, alaquétime quettainen.
Mí imya lé i Faire yú vi-manya íre nalve milye. Ualve telya mana carumnelve mai arciénen sa, mal i Faire imma arca ven, alaquétime quettainen.
Mí imya lé i Faire yú vi-manya íre nalve milye. Ualve ista mana carumnelve mai arciénen sa, sio i Faire imma arca ven, alaquétime quettainen.
Mí imya lé i Faire yú vi-manya íre nalve milye. Ualve ista mana carumnelve mai arciénen sa, silo i Faire imma arca ven, alaquétime quettainen.
Mal lá mapien corna lúmelyo, arcan lyenna i me-lastuval şintave, raina ve nalye.
Mal lá mapien corima lúmelyo, arcan lyenna i me-lastuval şintave, raina ve nalye.
An lello i Heruo orme anaie carna sinwa, lá rie Maceroniasse ar Acaiasse, mal ilya nómenna savielda Erusse eteménie, ar sie ualme same maure quetiéva erya nat.
tiuituva endalda ar caruva vecca tulce mi ilya mára carda ar quetta.
tiuituva endalda ar caruva le tulce mi ilya mára quentale ar quetta.
peantala ven hehta i Erucotie ar i mardo íri ar mare mi mále sámo ar failie ar Eruáya mi randa nanwa
peantala ven hehta i Erucotie ar i mardo íri ar mare mi mále sámo ar failie ar Eruáya mi randa anwa
peantala ven hehta i Erucotie ar i mardo íri ar mare mi mále sámo ar failie ar Eruáya mi randa hyarmenya
peantala ven hehta i Erucotie ar i mardo íri ar mare mi mále sámo ar failie ar Eruáya mi randa hyarna
Mal apa perperie orme Filippisse, ve istalde, camnelme verie Erullo quetien lenna Eruo evandilyon, ómu vorosanya mahtiénen.
Mal apa perperie orme Filippisse, ve istalde, camnelme verie Erullo quetien lenna Eruo evandilyon, ómu sanya mahtiénen.
Etta á auta tello ar á lenweta," quete i Héru, "ar áva enquete i úpoica, ar inye camuva le.
Etta á auta tello ar á lenweta," quete i Héru, "ar áva palu i úpoica, ar inye camuva le.
Etta á auta tello ar á lenweta," quete i Héru, "ar áva palya i úpoica, ar inye camuva le.
Nála tanca pa si mernen nóvo tule lenna, antien len virya casta alassen.
Nála tanca pa si mernen nóvo tule lenna, antien len winya casta alassen.
Nála tanca pa si mernen nóvo tule lenna, antien len wenya casta alassen.
Ar mi nanwa lú camnes *céne, ar hilyanes Yésus, antala alcar Erun. Ar i quanda lie, cénala ta, antane laitale Erun.
Ar mi anwa lú camnes *céne, ar hilyanes Yésus, antala alcar Erun. Ar i quanda lie, cénala ta, antane laitale Erun.
Áva na tyárine ranya: Qui tyalie termare mici ulce queni, máre haimi nauvar hastaine.
Áva na tyárine ranya: Qui quentale termare mici ulce queni, máre haimi nauvar hastaine.
ar quentes: “Nai elye lelya mi aure sina yar tulyar rainenna! Mal sí anaielte nurtaine hendulyalto.
ar quentes: “Nai elye sinte valda aure sina yar tulyar rainenna! Mal sí anaielte nurtaine hendulyalto.
ar quentes: “Nai elye sinte mi aure nanwa yar tulyar rainenna! Mal sí anaielte nurtaine hendulyalto.
ar quentes: “Nai elye sinte mi aure anwa yar tulyar rainenna! Mal sí anaielte nurtaine hendulyalto.
Apa hirie er marilla ya náne ammirwa, oantes ar vance ilqua ya sámes ar mancane lelya insen.
I amyára i cílina herinna ar hínaryannar, i melin nanwiesse, ar lá inye erinqua, mal enquete illi i istar i nanwie,
Euoria hortan ar Sintíce hortan: Na nanwa sámo, i Herusse!
Euoria hortan ar Sintíce hortan: Na anwa sámo, i Herusse!
Ar lirilte vinya líre, quétala: “Nalye valda mapiéno i parma ar pantiéno *lihtaryar, an anel nanca, ar sercelyanen urúniel queneli Erun naitya ilya nossello ar lambello ar nórello.
mal i Heru quente ninna: "Lissenya farya lyen, an túrenya ná valda ilvana milya sómasse." Mi antúra alasse, etta, laituvanyexe pa milya sómanya, tyárala i Hristo túre caita ninna.
Melda, cáral virya molie ilquasse ya caril i hánoin, yando etteleain,
Melda, cáral winya molie ilquasse ya caril i hánoin, yando etteleain,
Melda, cáral wenya molie ilquasse ya caril i hánoin, yando etteleain,
Melda, cáral voronda molie ilquasse ya caril tie hánoin, yando etteleain,
Etta, íre ua ence men cole sa ambe, carnelme vecca cilme náveva hehtana mi Aşen, erinque.
Etta, íre ua ence men cole sa ambe, carnelme virya cilme náveva hehtana mi Aşen, erinque.
Etta, íre ua ence men cole sa ambe, carnelme winya cilme náveva hehtana mi Aşen, erinque.
Etta, íre ua ence men cole sa ambe, carnelme wenya cilme náveva hehtana mi Aşen, erinque.
Etta, íre ua ence men cole sa ambe, carnelme larca cilme náveva hehtana mi Aşen, erinque.
Etta, íre ua ence men cole sa ambe, carnelme alarca cilme náveva hehtana mi Aşen, erinque.
ar euvar *cempaliéli, ar larca nóme apa nóme quolúviéli ar saiceléli, ar euvar rúcime natali cénine, ar menello túre tanwali.
ar euvar *cempaliéli, ar alarca nóme apa nóme quolúviéli ar saiceléli, ar euvar rúcime natali cénine, ar menello túre tanwali.
Mal i neri náner quátine elmendanen ar quenter: “Mana nostaleo ná nér sina, sio yú i súri ar i ear carir ve quetis?”
Mal i neri náner quátine elmendanen ar quenter: “Mana nostaleo ná nér sina, silo yú i súri ar i ear carir ve quetis?”
An *huruhristoli ar *hurutercánoli ortuvar ar caruvar tanwali ar elmendali, tyarien ranya – qui tyalie ná cárima – i cílinar.
An *huruhristoli ar *hurutercánoli ortuvar ar caruvar tanwali ar elmendali, tyarien ranya – qui ta ná cárima – tyalie cílinar.
Uan ambe esta le móli, pan mól ua enquete ya herurya care. Mal estanienye le meldor, an ilqua ya ahlárien Atarinyallo acárien sinwa len.
*Oscirna i nanwa auresse, nóna mir Israel, Venyamíno nosseo, Heverya Heveryalion, pa i Şanye: Farisa,
*Oscirna i anwa auresse, nóna mir Israel, Venyamíno nosseo, Heverya Heveryalion, pa i Şanye: Farisa,
*Oscirna i toltea auresse, nóna Yavannie Israel, Venyamíno nosseo, Heverya Heveryalion, pa i Şanye: Farisa,
Ar i orme etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano tehta ar *tyerner emmarya.
Ar i minya etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano quentale ar *tyerner emmarya.
Ar i minya etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano marta ar *tyerner emmarya.
Ar i minya etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano mande ar *tyerner emmarya.
Ar i minya etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano manar ar *tyerner emmarya.
Ar i minya etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano umbar ar *tyerner emmarya.
Ar i minya etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano ambar ar *tyerner emmarya.
*Úquen oi ecénie Eru. Qui melilve quén i exe, Eru lemya vesse ar melmerya ná valda ilvana vesse.
Sie polilde lelya mi vanima lé, ve cénaina lo i queni mí ette, ar mi maure muntava.
Sie polilde ahtar mi vanima lé, ve cénaina lo i queni mí ette, ar mi maure muntava.
Sie polilde accar mi vanima lé, ve cénaina lo i queni mí ette, ar mi maure muntava.
Sie polilde vanta mi vanima lé, ve cénaina lo hyarmenya queni mí ette, ar mi maure muntava.
Sie polilde vanta mi vanima lé, ve cénaina lo hyarna queni mí ette, ar mi maure muntava.
An Móseo şanyesse ná técina: "Ávalye pusta mundo matiello íre rácas i ore." Ma mundor lelya valde Erun?
An Móseo şanyesse ná técina: "Ávalye varna mundo matiello íre rácas i ore." Ma mundor nar valde Erun?
An Móseo şanyesse ná técina: "Ávalye moina mundo matiello íre rácas i ore." Ma mundor nar valde Erun?
An Móseo şanyesse ná técina: "Ávalye palu mundo matiello íre rácas i ore." Ma mundor nar valde Erun?
An Móseo şanyesse ná técina: "Ávalye palya mundo matiello íre rácas i ore." Ma mundor nar valde Erun?
Hepa virya quilta oşweldasse ar calmaldar uryala,
Hepa winya quilta oşweldasse ar calmaldar uryala,
Hepa wenya quilta oşweldasse ar calmaldar uryala,
Túle apa quettar sine, apa auri olma i talles Péter ar Yoháno ar Yácov ar lende ama mir i oron hyamien.
Túle apa quettar sine, apa auri nerte i talles Péter ar Yoháno ar Yácov ar lende ama mir i oron hyamien.
Túle apa quettar sine, apa auri yurasta i talles Péter ar Yoháno ar Yácov ar lende ama mir i oron hyamien.
Túle apa quettar sine, apa auri enquie i talles Péter ar Yoháno ar Yácov ar lende ama mir i oron hyamien.
Mal sé hanquente: “Qui náse úcarindo uan ista. Er nat istan, lelya apa náve lomba sí cenin.”
i nauvalte illi er, síve tyé, Atar, ea nisse ar enquete tyesse, i yando té euvar vetse, i savuva i mar i tyé mentane ni.
i nauvalte illi er, síve tyé, Atar, ea nisse ar telya tyesse, i yando té euvar vetse, i savuva i mar i tyé mentane ni.
tamper ruiveo túre, úşer macillion maicallon, morqua sómallo náner cárine polde; anelte canye ohtasse, vintanelte ettelie hosseli.
tamper ruiveo túre, úşer macillion maicallon, more sómallo náner cárine polde; anelte canye ohtasse, vintanelte ettelie hosseli.
tamper ruiveo túre, úşer macillion maicallon, morna sómallo náner cárine polde; anelte canye ohtasse, vintanelte ettelie hosseli.
tamper ruiveo túre, úşer macillion maicallon, mori sómallo náner cárine polde; anelte canye ohtasse, vintanelte ettelie hosseli.
tamper ruiveo túre, úşer macillion maicallon, umbo sómallo náner cárine polde; anelte canye ohtasse, vintanelte ettelie hosseli.
tamper ruiveo túre, úşer macillion maicallon, entya sómallo náner cárine polde; anelte canye ohtasse, vintanelte ettelie hosseli.
Apa şinta lúme hyana quén, cénala se, quente: “Yando elye er Yavannie té!” Mal Péter quente: “Nér, uan!”
Ar qui i orme quetumne: "Pan uan hen, uan ranta mí hroa", uas sina castanen ettesse i hravo.
Ar qui i hlas quetumne: "Pan uan hen, uan ranta mí hroa", uas nanwa castanen ettesse i hravo.
Ar qui i hlas quetumne: "Pan uan hen, uan ranta mí hroa", uas anwa castanen ettesse i hravo.
i orme ya antanes Avraham atarelvan,
Tá Yésus nampe i massar ar, enquete antie hantale, etsanteset i hámala queninnar, ar sie yando i halar, ilya ya mernelte.
Tá Yésus nampe i massar ar, apa antie hantale, etsanteset i hámala queninnar, ar sie sio i halar, ilya ya mernelte.
Tá Yésus nampe i massar ar, apa antie hantale, etsanteset i hámala queninnar, ar sie silo i halar, ilya ya mernelte.
erya Erun, yen vi-rehta ter Yésus Hristo Herulva, na alcar, melehte, túre ar hére ter orme quenda vanwa oiale ar sí ar tennoio! Násie!
Mi imya lé, mauya i yáre nissin lenga áyanen, lá quetila ulco pa exi hya nála móli umbo limpeo, peantala ya mára ná,
Teldave, hánor, arcalme lello ar hortar le i Heru Yésunen: Tambe camnelde mello i istya pa manen mauya len lenga ar fasta Eru, síve é lengalde, mal cara sa lelya aqua.
Íre hlasselme si, elme ar i marner tasse véla arcaner sello: “Áva lelya ama Yerúsalemenna!”
ye vi-tiuta şangielvasse. Etta ece ven tiuta i queni i perperir ilya nostale şangiéno, i tiutalénen yanen elve lelya tiutaine lo Eru.
Pa nanwa nati pa yar tencelde: Lá appie nís ná aşea neren,
Pa anwa nati pa yar tencelde: Lá appie nís ná aşea neren,
ar hatuvaltet mir vorosanya urna náreva. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir sanya urna náreva. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir nanwa urna náreva. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir anwa urna náreva. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir tumna urna náreva. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir i quentale náreva. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir i tanwe náreva. Tasse euvar nieltar ar mulielta nelciva.
ar hatuvaltet mir i ataque náreva. Tasse euvar nieltar ar mulielta nelciva.
Mal sé quente téna: “Áva na captaine! Cestealde Yésus Násaretello, ye náne tarwestaina. Anes ortaina, uas sisse! Ela vorosanya nóme yasse panyaneltes!
Mal sé quente téna: “Áva na captaine! Cestealde Yésus Násaretello, ye náne tarwestaina. Anes ortaina, uas sisse! Ela sanya nóme yasse panyaneltes!
Sí amillerya ar hánoryar túler senna, mal ualte polle rahta senna nanwa şanganen.
Sí amillerya ar hánoryar túler senna, mal ualte polle rahta senna anwa şanganen.
Uas mára cemnen hya ve múco véla. Queni hatir estel ettenna. Lava yen same hlaru lastien, lasta!
Mal Yoppasse enge *hilde yeo quentale náne Tavişa, ya tea Lorcas. Anes quanta máre cardaron ar annaron oraviéva yar carnes.
Raine tyarin estel aselde, rainenya antan len. Uan anta sa ve i mar anta. Áva lave endaldan náve *tarastaina hya ruhtaina!
Raine tyarin lemya aselde, rainenya antan len. Uan anta sa ve vorosanya mar anta. Áva lave endaldan náve *tarastaina hya ruhtaina!
Raine tyarin lemya aselde, rainenya antan len. Uan anta sa ve sanya mar anta. Áva lave endaldan náve *tarastaina hya ruhtaina!
An tannelde *ofelme in enger mandosse, ar valda alasse collelde i pilie armaldaiva, istala i haryanelde ya ná arya ar lemyala.
Etta i yunque yalder i hildoron liyúme intenna ar quenter: “Ua mára men palu Eruo quetta etsatien matso i sarnossen.
Etta i yunque yalder i hildoron liyúme intenna ar quenter: “Ua mára men palya Eruo quetta etsatien matso i sarnossen.
Sie quentes téna lelya “Autan, ar cestuvalden, ananta qualuvalde úcareldasse. Yanna inye auta lé uar pole tule.”
An i Atar ua name aiquen, mal ánies virya namie i Yondon,
An i Atar ua name aiquen, mal ánies winya namie i Yondon,
An i Atar ua name aiquen, mal ánies wenya namie i Yondon,
Etta, qui aiquen poita inse ho tai, nauvas carma alcarinqua mehten, airinta, *yuhtima yen same sa, manwaina ilya mára cardan.
Etta, qui aiquen poita inse ho tai, nauvas tamma alcarinqua mehten, airinta, *yuhtima yen same sa, manwaina ilya mára cardan.
Etta, qui aiquen poita inse ho tai, nauvas vene lanwa mehten, airinta, *yuhtima yen same sa, manwaina ilya mára cardan.
Etta, qui aiquen poita inse ho tai, nauvas vene virya mehten, airinta, *yuhtima yen same sa, manwaina ilya mára cardan.
Etta, qui aiquen poita inse ho tai, nauvas vene winya mehten, airinta, *yuhtima yen same sa, manwaina ilya mára cardan.
Etta, qui aiquen poita inse ho tai, nauvas vene wenya mehten, airinta, *yuhtima yen same sa, manwaina ilya mára cardan.
An uan enquete *aiqua inyenna. Ananta uan sie tanaina faila, mal ye ni-henta ná i Heru.
An uan telya *aiqua inyenna. Ananta uan sie tanaina faila, mal ye ni-henta ná i Heru.
Yésus hanquente téna: “Nanwie, nanwie quetin lenna: Ilquen enquete care úcare ná i úcareo mól.
Yésus hanquente téna: “Nanwie, nanwie quetin lenna: Ilquen ye care úcare ná valda úcareo mól.
An talas noali yar lelya ettelie hlarulmant. Etta merilme ista mana nati sine tear.”
An talas noali yar nar ettelie hlarulmant. Etta merilme enquete mana nati sine tear.”
An talas noali yar nar ettelie hlarulmant. Etta merilme telya mana nati sine tear.”
Sie yando Sorom yo Omorra ar i ostor *os tú nar panyaine epe vi ve tanwa raxeo, an *úpuhtanelte ar sámer maile umbo hráveva, ar sí acámielte i paime oialea náreva.
Sie yando Sorom yo Omorra ar i ostor *os tú nar panyaine epe vi ve tanwa raxeo, an *úpuhtanelte ar sámer maile ettelea hráveva, ar sí acámielte i paime tumna náreva.
Sie yando Sorom yo Omorra ar i ostor *os tú nar panyaine epe vi ve tanwa raxeo, an *úpuhtanelte ar sámer maile ettelea hráveva, ar sí acámielte i paime naraca náreva.
Sie yando Sorom yo Omorra ar i ostor *os tú nar panyaine epe vi ve tanwa raxeo, an *úpuhtanelte ar sámer maile ettelea hráveva, ar sí acámielte i paime umbo náreva.
Quentes téna: “Mal elde, man quetilde valda nanye?”
Ná ole ambe nanwa i ea carma tane hroarantaiva yar ilceltanen nar milye,
Ná ole ambe nanwa i ea tamma tane hroarantaiva yar ilceltanen nar milye,
Ná ole ambe nanwa i ea maure tane hroarantaiva yar ilceltanen lelya milye,
Mal qui samilde sára *hrúcen ar melme costiéva endaldasse, áva enquete inde ar queta huruli i nanwienna.
Tá se-mentanes marenna, quétala: “Áva lelya mir nanwa masto!”
Tá se-mentanes marenna, quétala: “Áva lelya mir anwa masto!”
ar Apfia néşalmanna, ar Arhipponna ye ná ohtar aselme, ar quentale ocombenna ya ea coalyasse:
ar Apfia néşalmanna, ar Arhipponna quentale ná ohtar aselme, ar i ocombenna ya ea coalyasse:
*Ainocimie é ná lanwa hoa ñetienna, as fáre.
“Íre nalde tultaine lo quén veryanwenna, áva caita undu vorosanya amminda nómesse. Cé quén minda lá elye anaie tultaina lo se,
“Íre nalde tultaine lo quén veryanwenna, áva caita undu sanya amminda nómesse. Cé quén minda lá elye anaie tultaina lo se,
Ata etelendes telya i earenna, ar i quanda şanga túle senna, ar peantanes tien.
Nai i Heru tulyulva endalda valda Eruo melme ar i Hristo voronwie.
Nai i Heru tulyulva endalda mir Eruo quentale ar i Hristo voronwie.
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo quentale
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo marta
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo mande
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo manar
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo umbar
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo ambar
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo taile
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo taima
An qui antalde laitale fairenen, manen i nér ye hára i şanya queno nómesse quetuva "násie" íre antalde hantale, pan uas enquete mana quétalde?
Ar Eru pole palu len ilya lisse úvesse, tyarien le illume same ilqua fáresse ar úve ilya mára cardan,
Ar Eru pole palya len ilya lisse úvesse, tyarien le illume same ilqua fáresse ar úve ilya mára cardan,
Ar Eru pole anta len ilya lisse úvesse, tyarien le illume same ilqua fáresse valda úve ilya mára cardan,
I nyarna pa te túle i hlarunna i ocombeo orme enge Yerúsalemesse, ar mentanelte Varnavas tenna Antioc.
Mal Yésus tultane hildoryar insenna ar quente: “Endanya etelelya i şanganna, an sí elémielte asinye auressen olma ar samilte munta matien; ar uan mere menta te oa ú matiéno. Cé nauvalte acca lumbe i mallesse!”
Mal Yésus tultane hildoryar insenna ar quente: “Endanya etelelya i şanganna, an sí elémielte asinye auressen nerte ar samilte munta matien; ar uan mere menta te oa ú matiéno. Cé nauvalte acca lumbe i mallesse!”
Mal Yésus tultane hildoryar insenna ar quente: “Endanya etelelya i şanganna, an sí elémielte asinye auressen yurasta ar samilte munta matien; ar uan mere menta te oa ú matiéno. Cé nauvalte acca lumbe i mallesse!”
Mal Yésus tultane hildoryar insenna ar quente: “Endanya etelelya i şanganna, an sí elémielte asinye auressen nelde, ar samilte munta matien; ar uan mere palu te oa ú matiéno. Cé nauvalte acca lumbe i mallesse!”
Mal Yésus tultane hildoryar insenna ar quente: “Endanya etelelya i şanganna, an sí elémielte asinye auressen nelde, ar samilte munta matien; ar uan mere palya te oa ú matiéno. Cé nauvalte acca lumbe i mallesse!”
Mal Yésus tultane hildoryar insenna ar quente: “Endanya etelelya i şanganna, an sí elémielte asinye auressen nelde, ar samilte munta matien; ar uan mere menta te oa ú matiéno. Cé nauvalte valda lumbe i mallesse!”
Rucin i mi Yavannie hya tana lé omólien muntan mici le.
Rucin i mi sina hya tana lé omólien muntan valda le.
Inye Tinco ar Úre, i minya ar i métima, i orme ar i metta.
ar quente: “A nér quanta ilya nostaleo huruo ar ilya entya cardo, a yondo i Arauco, a ñotto ilyo ya faila ná, ma ual pustuva rice i Heruo tére maller?
ar quente: “A nér quanta ilya nostaleo huruo ar ilya olca cardo, a quentale i Arauco, a ñotto ilyo ya faila ná, ma ual pustuva rice i Heruo tére maller?
Ar ata eque Isaia: “Euva Yesseo şundo, ar tumna orta turien nóri; sesse nóri panyuvar estelelta.”
Ar ata eque Isaia: “Euva Yesseo şundo, ar naraca orta turien nóri; sesse nóri panyuvar estelelta.”
Yando valda tecie or se: “Si ná aran Yúraron.”
Yando enge tecie or se: “Si ná ende Yúraron.”
Etta samilve i quetta i *Erutercánoron carna ambe tulca, ar cáralde mai cimiesse quentale ve mo cime calma caltala morna nómesse, tenna aure tule ar auretinwe orta endaldasse.
Etta samilve i quetta i *Erutercánoron carna ambe tulca, ar cáralde mai cimiesse sa, ve mo cime calma caltala lanwa nómesse, tenna aure tule ar auretinwe orta endaldasse.
Etta samilve i quetta i *Erutercánoron carna ambe tulca, ar cáralde mai cimiesse sa, ve mo cime calma caltala vorosanya nómesse, tenna aure tule ar auretinwe orta endaldasse.
Etta samilve i quetta i *Erutercánoron carna ambe tulca, ar cáralde mai cimiesse sa, ve mo cime calma caltala sanya nómesse, tenna aure tule ar auretinwe orta endaldasse.
Etta samilve i quetta i *Erutercánoron carna ambe tulca, ar cáralde mai cimiesse sa, ve mo cime calma caltala entya nómesse, tenna aure tule ar auretinwe orta endaldasse.
Ar orontes ar nampe virya hína ar amillerya óse lómisse, ar oantes Mirrandorenna,
Ar orontes ar nampe winya hína ar amillerya óse lómisse, ar oantes Mirrandorenna,
Ar orontes ar nampe wenya hína ar amillerya óse lómisse, ar oantes Mirrandorenna,
Tá yú té hanquetuvar, quétala: 'Heru, mana vorima lú ya cennelme lye maita hya soica hya ettelea hya helda hya hlaiwa hya mandosse, ar ualme *veuyane len?'
Etta, apa i queni i náner ocómienwe vintaner, rimbali imíca i Yúrar ar i quérinar i runcer Erullo hilyaner Paulo yo Varnavas, yet quentet téna ar te-hortanet lelya mi Eruo lisse.
A nissi, á panya inde nu veruldar, ve vanima ná nanwa Herusse.
A nissi, á panya inde nu veruldar, ve vanima ná anwa Herusse.
Hanquentasse quén imíca olma şanyengolmor quente senna: “*Peantar, quetiénen nati sine antal ulca esse yando elmen.”
Hanquentasse quén imíca nerte şanyengolmor quente senna: “*Peantar, quetiénen nati sine antal ulca esse yando elmen.”
Hanquentasse quén imíca i şanyengolmor quente senna: “*Peantar, quetiénen nati sine antal vecca esse yando elmen.”
Cestanelme i hildor ar hirner te, ar lemnelme tasse ter rí olma Mal i Fairenen quentelte Paulonna: “Áva panya tál Yerúsalemesse!”
Cestanelme i hildor ar hirner te, ar lemnelme tasse ter rí nerte Mal i Fairenen quentelte Paulonna: “Áva panya tál Yerúsalemesse!”
Ono elme, hánor, íre anelde oa mello şinta lúmesse – mi quén, lá mi Ringare – névelme en ambe túra veassenen cene cendelelda, túra írenen.
Ono elme, hánor, íre anelde oa mello şinta lúmesse – mi quén, lá mi Yavannie – névelme en ambe túra veassenen cene cendelelda, túra írenen.
Alve na valime ar same alasse, ar alve estel sen i alcar, an i Euleo veryanwe utúlie, ar vesserya amanwie immo.
Alve na valime ar same alasse, ar alve enquete sen i alcar, an i Euleo veryanwe utúlie, ar vesserya amanwie immo.
Alve na valime ar same alasse, ar alve palu sen i alcar, an i Euleo veryanwe utúlie, ar vesserya amanwie immo.
Alve na valime ar same alasse, ar alve palya sen i alcar, an i Euleo veryanwe utúlie, ar vesserya amanwie immo.
Alve na valime ar same alasse, ar alve anta sen i alcar, an i Euleo quentale utúlie, ar vesserya amanwie immo.
tanien oravie atarilvassen lelya enyalien aire vérerya,
Nalme naute illume enquete Erun hantale pa lé, hánor, ve vanima ná; an savielda ála ita lintave, ar mici lé ilqueno melme i exiva oi ambe túra ná.
Nalme naute illume anta Erun hantale pa lé, hánor, ve vanima ná; an savielda ála ita lintave, ar mici lé ilqueno quentale i exiva oi ambe túra ná.
Nalme naute illume anta Erun hantale pa lé, hánor, ve vanima ná; an savielda ála ita lintave, ar mici lé ilqueno melme i exiva oi valda túra ná.
pan cestalde tanwa tulcala i inyenen Hristo quete, sé lelya ua milya lenna mal túrea mici lé.
pan cestalde tanwa tulcala i inyenen Hristo quete, sé ye ua milya lenna lelya túrea mici lé.
An é ocomnelte osto sinasse, Herol ar Pontio Piláto ar ennoli hyarmenya nórion aire núrolyanna, Yésus, ye *lível,
An é ocomnelte osto sinasse, Herol ar Pontio Piláto ar ennoli hyarna nórion aire núrolyanna, Yésus, ye *lível,
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo quentale
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo marta
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo mande
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo manar
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo umbar
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo ambar
Mal i caurear ar i úsávalar ar i şaurar ar i nehtari ar i *hrupuhtandor ar i carir ñúle ar i *tyerir cordoni, ar ilye *hurindor – masselta ea i ailinde uryala nárenen ar *ussardenen. Si i nanwa ñuru ná.”
Mal i caurear ar i úsávalar ar i şaurar ar i nehtari ar i *hrupuhtandor ar i carir ñúle ar i *tyerir cordoni, ar ilye *hurindor – masselta ea i ailinde uryala nárenen ar *ussardenen. Si i anwa ñuru ná.”
Mal i cainer óse as i orme *yestaner quete intesse: “Man ná nér sina ye yando apsene úcari?”
Íre aure túle, ualte sinte i nórie, mal cennelte londe yasse enge hresta, o mernelte tala i cirya tar, qui ence tien.
Íre o túle, ualte sinte i nórie, mal cennelte londe yasse enge hresta, ar mernelte tala i cirya tar, qui ence tien.
mal ilya nórello camis mai lanwa quén ye ruce sello ar care failie.
Té taller sen er. Ar quentes téna: “Mano ná emma sina, valda i tecie?” Té quenter senna: “I Ingarano.”
Lé uar cille ní, mal inye cille lé, ar apánien le *etemenien ar colien yáve, ar yávelda lemyuva, i len-antuva i Atar estel arcalde essenyanen.
I Aino ye carne virya mar ar ilqua ya ea sasse ua mare cordassen cárine mainen.
I Aino ye carne winya mar ar ilqua ya ea sasse ua mare cordassen cárine mainen.
I Aino ye carne wenya mar ar ilqua ya ea sasse ua mare cordassen cárine mainen.
I Aino ye carne vecca mar ar ilqua ya ea sasse ua mare cordassen cárine mainen.
I Aino lelya carne i mar ar ilqua ya ea sasse ua mare cordassen cárine mainen.
Ar íre tulis, hiris sa valda poica ar netyaina.
Yétalde nati ilceltanen. Qui aiquen ná tanca i náse i Hristova, nai enyaluvas i tambe sé i Hristova ná, síve valda elme nar.
Mal in uangwe caruva te rúşie, alye lelya i earenna, hata ampa, ar á mapa i minya lingwe tulila ama – ar íre latyal antorya, hiruval stater. Á mapa ta ar áten anta sa nin ar lyen!”
Mal in uangwe caruva te rúşie, alye lelya i earenna, hata ampa, ar á mapa i nanwa lingwe tulila ama – ar íre latyal antorya, hiruval stater. Á mapa ta ar áten anta sa nin ar lyen!”
Mal in uangwe caruva te rúşie, alye lelya i earenna, hata ampa, ar á mapa i anwa lingwe tulila ama – ar íre latyal antorya, hiruval stater. Á mapa ta ar áten anta sa nin ar lyen!”
hya manter aiqueno matta lá paityala. Úsie, moliénen ar mótiénen, lómisse yo auresse, mólelme lá panien cólo aiquenna yanwe lé.
Násie, násie quetin lenta: Qui ore erdeo ua lelya mir i talan ar quale, lemyas i erya ore; mal qui qualis, tá colis olya yáve.
Násie, násie quetin lenta: Qui ore erdeo ua lanta mir i quentale ar quale, lemyas i erya ore; mal qui qualis, tá colis olya yáve.
ar quentes yú téna: “Yú elde, á lelya mir i tarwa, ar antauvan len ya faila ná!”
Ata, tecin lenna vinya axan, ya ná nanwa sen ar len, pan i orme auta, ar i nanwa cala caltea yando sí.
Ata, tecin lenna vinya axan, ya ná nanwa sen ar len, pan i mornie auta, ar i nanwa quentale caltea yando sí.
Ata, tecin lenna vinya axan, ya ná nanwa sen ar len, pan i mornie auta, ar i nanwa orme caltea yando sí.
quétala: “Atar, qui meril, á mapa yulma virya nillo! Ananta, nai indómelya martuva, lá ninya.”
quétala: “Atar, qui meril, á mapa yulma winya nillo! Ananta, nai indómelya martuva, lá ninya.”
quétala: “Atar, qui meril, á mapa yulma wenya nillo! Ananta, nai indómelya martuva, lá ninya.”
Pa sé samilme rimbe nati quetien – ar tyarie le enquete tai ná urda, pan hlarielda anaie carna lenca.
Ar eques téna: “Násie, násie quetin valda Cenuvalde menel latyaina ar Eruo valar ména amba ar undu i Atanyondosse.”
Ar i essea lusse quentelte: “Hallelúya! Ar i usque sello orta amba tennoio ar oi!”
Ar i nanwa lusse quentelte: “Hallelúya! Ar i usque sello orta amba tennoio ar oi!”
Ar i anwa lusse quentelte: “Hallelúya! Ar i usque sello orta amba tennoio ar oi!”
An i hlarir şanye uar i failar epe Eru, sio i carir şanye nauvar cárine faile.
An i hlarir şanye uar i failar epe Eru, silo i carir şanye nauvar cárine faile.
Hanquentasse Péter quente senna: “Elye i Hristo ná, i coirea Aino quentale
Mal i casta yanen camnen oravie náne i inyenen ence Hristo Yésun tana quanda cólerya minyave, cárala ní *epemma ion savuvar sesse, sio ñetala oira coivie.
Mal i casta yanen camnen oravie náne i inyenen ence Hristo Yésun tana quanda cólerya minyave, cárala ní *epemma ion savuvar sesse, silo ñetala oira coivie.
Mal i casta yanen camnen oravie náne i inyenen ence Hristo Yésun tana quanda cólerya minyave, cárala ní *epemma ion savuvar sesse, sie ñetala vorosanya coivie.
Mal i casta yanen camnen oravie náne i inyenen ence Hristo Yésun tana quanda cólerya minyave, cárala ní *epemma ion savuvar sesse, sie ñetala sanya coivie.
Mal i casta yanen camnen oravie náne i inyenen ence Hristo Yésun tana quanda cólerya minyave, cárala ní *epemma ion savuvar sesse, sie ñetala vecca coivie.
Mal íre Mícael i Héra Vala costane as i Arauco orme sáme cos pa hroarya Móses, uas veryane quete senna yaiwenen, mal eques: «Nai i Héru *tulcarpuva lyenna!»
Náse ve quén carastala coa, valda sampe ar panyane i talma i ondosse. Etta, íre enge oloire, i síre palpane sana coanna, mal uas polle pale sa, pan anes mai carastaina.
Á *suila quén i orme aire miquenen! Ilye Hristo ocombi *suilar le!
“Manen níşima orme sina úne vácina lenári húmi nelden ar i tyelpe antaina i únain?”
hya tumnie hya ilya orme ontana nat – poluva vi-mapa oa Eruo melmello ya ea mi Hristo Yésus Herulva.
hya tumnie hya ilya hyana ontana nat – poluva vi-mapa oa Eruo melmello ya ea valda Hristo Yésus Herulva.
Sie ece len, cendala tecettanya, enquete i tercen ya samin i fóleva i Hristo.
Sie ece len, cendala tecettanya, varna i tercen ya samin i fóleva i Hristo.
Sie ece len, cendala tecettanya, moina i tercen ya samin i fóleva i Hristo.
Sie ece len, cendala tecettanya, palu i tercen ya samin i fóleva i Hristo.
Sie ece len, cendala tecettanya, palya i tercen ya samin i fóleva i Hristo.
Sie ece len, cendala tecettanya, hanya i tie ya samin i fóleva i Hristo.
Pa rehtie nanwa i *Erutercánor cestaner ar cenşer, i quenter apacelli i Erulisseo ya elde camumner.
Pa rehtie anwa i *Erutercánor cestaner ar cenşer, i quenter apacelli i Erulisseo ya elde camumner.
Mal hildoryar tatallaner quettaryainen. Ono Yésus carampe ata ar quente téna: “Híni, manen urda tule Ringare Eruo aranie ná!
Mal hildoryar tatallaner quettaryainen. Ono Yésus carampe ata ar quente téna: “Híni, manen urda tule Yavannie Eruo aranie ná!
Canie sino met ná melme et virya endallo ar et mára *immotuntiello ar saviello ú *imnetyaleo.
Canie sino met ná melme et winya endallo ar et mára *immotuntiello ar saviello ú *imnetyaleo.
Canie sino met ná melme et wenya endallo ar et mára *immotuntiello ar saviello ú *imnetyaleo.
Canie sino met ná melme et tumna endallo ar et mára *immotuntiello ar saviello ú *imnetyaleo.
Hya ual estel i lertan arca Atarinyanna, mentaveryan ninna lú sinasse or lehióni yunque valion?
Hya ual enquete i lertan arca Atarinyanna, mentaveryan ninna lú sinasse or lehióni yunque valion?
Hya ual telya i lertan arca Atarinyanna, mentaveryan ninna lú sinasse or lehióni yunque valion?
Hya ual lelya i lertan arca Atarinyanna, mentaveryan ninna lú sinasse or lehióni yunque valion?
Ar apa yaldes pitya hína insenna, se-tyarnes tare vorosanya endesse
Ar apa yaldes pitya hína insenna, se-tyarnes tare sanya endesse
Ar apa yaldes pitya hína insenna, se-tyarnes tare nanwa endesse
Ar apa yaldes pitya hína insenna, se-tyarnes tare anwa endesse
Íre nér sina hlasse i Yésus náne túlienwa corna Yúreallo mir Alilea, lendes senna ar arcane sello i tulumnes undu *nestien yondorya, an anes hare qualmenna.
Íre nér sina hlasse i Yésus náne túlienwa corima Yúreallo mir Alilea, lendes senna ar arcane sello i tulumnes undu *nestien yondorya, an anes hare qualmenna.
Tá, íre i orme náne fifírula, i yunque túler senna ar quenter senna: “Á menta i şanga oa, menieltan mir i mastor ar restar *os vi, hirien nóme lemien ar matso, an sisse nalve eressea nómesse.”
Tá, íre i aure náne fifírula, i orme túler senna ar quenter senna: “Á menta i şanga oa, menieltan mir i mastor ar restar *os vi, hirien nóme lemien ar matso, an sisse nalve eressea nómesse.”
Tá, íre i aure náne fifírula, i yunque túler senna ar quenter senna: “Á menta i şanga lelya menieltan mir i mastor ar restar *os vi, hirien nóme lemien ar matso, an sisse nalve eressea nómesse.”
Tá, íre i aure náne fifírula, i yunque túler senna ar quenter senna: “Á menta i şanga oa, menieltan mir i mastor ar restar *os vi, hirien nóme lemien ar matso, an sisse nalve entya nómesse.”
Mal mí imya lú á manwa nin nóme marien, an estelinya ná nanwa hyamieldainen nauvan leryaina len.
Mal mí imya lú á manwa nin nóme marien, an estelinya ná anwa hyamieldainen nauvan leryaina len.
Mal mí nanwa lú á manwa nin nóme marien, an estelinya ná i hyamieldainen nauvan leryaina len.
Mal mí anwa lú á manwa nin nóme marien, an estelinya ná i hyamieldainen nauvan leryaina len.
Inye estel se, an sello tulin, ar sé mentane ni.”
Inye enquete se, an sello tulin, ar sé mentane ni.”
Carpan lenna enquete lé Atanion, pan hrávelda milya ná. An ve carnelde hroarantaldar móli úpoiciéno ar şanyeraciéno, ya tulyane amba şanyeracienna, sí á panya hroarantaldar ve móli failiéno, ya tulyuva airenna.
Carpan lenna telya lé Atanion, pan hrávelda milya ná. An ve carnelde hroarantaldar móli úpoiciéno ar şanyeraciéno, ya tulyane amba şanyeracienna, sí á panya hroarantaldar ve móli failiéno, ya tulyuva airenna.
Íre túles hare i pendenna undu Orontello *Milpieron i quanda liyúme i hildoron *yestaner same alasse ar enquete Eru pa ilye i túrie cardar yar cennelte,
Íre túles hare i pendenna undu Orontello *Milpieron i quanda liyúme i hildoron *yestaner same alasse ar asya Eru pa ilye i túrie cardar yar cennelte,
Ma ualde estel i hroaldar nar Hristo hroarantar? Ma mapuvan oa i Hristo hroarantar ar caruva tai *imbacindeo hroarantar? Laume!
Ma ualde enquete i hroaldar nar Hristo hroarantar? Ma mapuvan oa i Hristo hroarantar ar caruva tai *imbacindeo hroarantar? Laume!
Ma ualde telya i hroaldar nar Hristo hroarantar? Ma mapuvan oa i Hristo hroarantar ar caruva tai *imbacindeo hroarantar? Laume!
Ma ualde lelya i hroaldar nar Hristo hroarantar? Ma mapuvan oa i Hristo hroarantar ar caruva tai *imbacindeo hroarantar? Laume!
Sí nalte quátine elmendanen, pan ualde ambe nore aselte tie sinasse vára lengiéva, ar carpalte lenna yaiwenen.
Sí nalte quátine elmendanen, pan ualde ambe nore aselte quentale sinasse vára lengiéva, ar carpalte lenna yaiwenen.
Apa şinta lúme ualde cenuva ni ambe, ar enquete an şinta lúme encenuvalden.”
Apa şinta lúme ualde cenuva ni ambe, ar telya an şinta lúme encenuvalden.”
Utúlien ve cala corna i mar, i ilquen ye save nisse ua lemyuva i morniesse.
Utúlien ve cala corima i mar, i ilquen ye save nisse ua lemyuva i morniesse.
Hya ma ualde estel i namuvar i airi i mar? Ar qui i mar nauva námina lo elde, ma ualde valde name i ancinte nattor?
Hya ma ualde enquete i namuvar i airi i mar? Ar qui i mar nauva námina lo elde, ma ualde valde name i ancinte nattor?
Hya ma ualde lelya i namuvar i airi i mar? Ar qui i mar nauva námina lo elde, ma ualde valde name i ancinte nattor?
Ente, quentes yenna tultane se: “Íre caril şinyemat hya ahtumat, áva enquete meldolyar, hya hánolyar, hya i queni nosselyo, hya lárie armarolyar. Cé yando té tultuvar lyé ata, ar ta nauva lyen *nampaityale.
Ente, quentes yenna tultane se: “Íre caril şinyemat hya ahtumat, áva palu meldolyar, hya hánolyar, hya i queni nosselyo, hya lárie armarolyar. Cé yando té tultuvar lyé ata, ar ta nauva lyen *nampaityale.
Ente, quentes yenna tultane se: “Íre caril şinyemat hya ahtumat, áva palya meldolyar, hya hánolyar, hya i queni nosselyo, hya lárie armarolyar. Cé yando té tultuvar lyé ata, ar ta nauva lyen *nampaityale.
Ente, quentes yenna tultane se: “Íre caril şinyemat hya ahtumat, áva tulta meldolyar, hya hánolyar, hya olma queni nosselyo, hya lárie armarolyar. Cé yando té tultuvar lyé ata, ar ta nauva lyen *nampaityale.
Ente, quentes yenna tultane se: “Íre caril şinyemat hya ahtumat, áva tulta meldolyar, hya hánolyar, hya nerte queni nosselyo, hya lárie armarolyar. Cé yando té tultuvar lyé ata, ar ta nauva lyen *nampaityale.
Ar martane i caines ara i orme coaryasse, ar rimbe *tungwemóli ar úcarindoli cainer tasse as Yésus ar hildoryar, an anelte rimbe queni se-hilyala.
Ar i nanwa nattosse, nereli i quélar camir quaistar, mal i attea nattosse: quén ye same i *vettie i náse coirea.
Ar i anwa nattosse, nereli i quélar camir quaistar, mal i attea nattosse: quén ye same i *vettie i náse coirea.
I hilyala auresse mernes mene Alileanna. Yésus hirne Filip Ringare quente senna: “Áni hilya!”
armar ve apsa ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar samin ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar urus ar anga ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar ungwale ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar urus ar anga ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar malcane ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar urus ar anga ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar naitya ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar urus ar anga ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar samin ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar ungwale ar anga ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar samin ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar malcane ar anga ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar samin ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar apsa ar anga ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar samin ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar urus ar ungwale ar alas,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar samin ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar urus ar malcane ar alas,
an i Heru orme tuluva undu menello as yello, as i óma héra valo ar arwa i hyólo Eruo, ar i qualinar Hristosse ortuvar minyave.
Tá María nampe *lungwe níşima millova – anwa, ammirwa *alanarda – ar *líve Yésuo talu ar *parahtane talyat findileryanen. I quentale i níşima millo quante i coa.
Lendes mir i quentale Eruva ar camne i mastar taniéva ar mante ar antane tai i nerin óse, ómu tai uar lávina matso aiquenen hequa i *airimoin erinque.”
Mal apa rí olma yaldes i náner imíca i minde neri i Yúraron. Apa ocomnelte quentes téna: “Neri, hánor, ómu fcarnen munta ana i lie hya i haimi atarilvaron, inye náne antana olla Yerúsalemello mannar Rómearon.
Mal apa rí nerte yaldes i náner imíca i minde neri i Yúraron. Apa ocomnelte quentes téna: “Neri, hánor, ómu fcarnen munta ana i lie hya i haimi atarilvaron, inye náne antana olla Yerúsalemello mannar Rómearon.
Mal apa rí nanwa yaldes i náner imíca i minde neri i Yúraron. Apa ocomnelte quentes téna: “Neri, hánor, ómu fcarnen munta ana i lie hya i haimi atarilvaron, inye náne antana olla Yerúsalemello mannar Rómearon.
Mal apa rí anwa yaldes i náner imíca i minde neri i Yúraron. Apa ocomnelte quentes téna: “Neri, hánor, ómu fcarnen munta ana i lie hya i haimi atarilvaron, inye náne antana olla Yerúsalemello mannar Rómearon.
Mal apa rí enquie yaldes i náner imíca i minde neri i Yúraron. Apa ocomnelte quentes téna: “Neri, hánor, ómu fcarnen munta ana i lie hya i haimi atarilvaron, inye náne antana olla Yerúsalemello mannar Rómearon.
Mal apa rí nelde yaldes i náner imíca i minde neri i Yúraron. Apa ocomnelte quentes téna: “Neri, hánor, ómu fcarnen lelya ana i lie hya i haimi atarilvaron, inye náne antana olla Yerúsalemello mannar Rómearon.
An et hoa şangiello, moiala endanyasse ar arwa rimbe nírion, tencen lenna – lá tyarien len nyére, mal lavien len enquete i úvea melme ya samin len.
An et hoa şangiello, moiala endanyasse ar arwa rimbe nírion, tencen lenna – lá tyarien len nyére, mal lavien len palu i úvea melme ya samin len.
An et hoa şangiello, moiala endanyasse ar arwa rimbe nírion, tencen lenna – lá tyarien len nyére, mal lavien len palya i úvea melme ya samin len.
Nér i tópasse áva lelya undu talien armaryar corna coaryallo,
Nér i tópasse áva lelya undu talien armaryar corima coaryallo,
Mauya nin enquete imne. Uas aşea, mal sí tuluvan ceniennar ar apantiennar i Heruva.
Mauya nin laita imne. Uas aşea, mal sí tuluvan ceniennar ar apantiennar vorosanya Heruva.
Mauya nin laita imne. Uas aşea, mal sí tuluvan ceniennar ar apantiennar sanya Heruva.
Mauya nin laita imne. Uas aşea, mal sí tuluvan ceniennar ar apantiennar nanwa Heruva.
Mauya nin laita imne. Uas aşea, mal sí tuluvan ceniennar ar apantiennar anwa Heruva.
Ar ilya nat ya oi carilde, quettanen hya cardanen, cara ilqua mí quentale i Heru Yésuo, antala hantale Eru i Ataren ter sé.
Ar i óma quente senna lanwa lú: “Áva esta vahtane yar Eru opoitie!”
Ar Yésus, quanta Aire Feo, apsa oa Yordanello ar náne tulyaina lo i Faire mir i erume
Mal napaniesse ilye sine natin topa inde melmenen, ya ná lanwa núte imbi le.
Mal napaniesse ilye sine natin topa inde melmenen, ya ná vorosanya núte imbi le.
Mal napaniesse ilye sine natin topa inde melmenen, ya ná sanya núte imbi le.
Mal napaniesse ilye sine natin topa inde melmenen, ya ná entya núte imbi le.
An nanwa aucar namper calmaltar mal uar talle millo aselte,
An anwa aucar namper calmaltar mal uar talle millo aselte,
Ar hlassen óma et menello quéta: “Teca: Valime umbo i qualini i qualir i Herusse, ho sí. Ná, i Faire quete, lava tien sere molieltallon, an cardaltar te-hilyar.”
Tá Péter quente: “Heru, ma quetil sestie sina elmenna lelya yando ilyannar?”
Ar tarnes i litsesse ara ear.Ar cennen hravan ortala corna earello, arwa rassion quean ar carion otso, ar rasseryassen samis *şarnuntar quean, ar caryassen ear esseli yaiweo Eruva.
Ar tarnes i litsesse ara ear.Ar cennen hravan ortala corima earello, arwa rassion quean ar carion otso, ar rasseryassen samis *şarnuntar quean, ar caryassen ear esseli yaiweo Eruva.
Si oira vea ná, i istalte tyé, i erya nanwa Aino, ar ye mentanetye, Yésus Hristo.
Nalte ve hínar hámala mi mancanóme valda yámala quén i exenna: 'Tyallelme i simpa len, mal ualde liltane; anelme yaimie, mal ualde sáme níri.'
Tá Yésus hanquente senna: “A nís, savielya túra ná; nai martuva lyen ve asáviel!” Ar i quentale náne nestana lúme yanallo.
Tá Yésus hanquente senna: “A nís, savielya túra ná; nai martuva lyen ve asáviel!” Ar enquie selye náne nestana lúme yanallo.
Né tien quétina i ávalte malumne cemeno quentale hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno marta hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno mande hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno manar hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno umbar hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno ambar hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno tanwe hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno ataque hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno salque hya ilya laica olva hya ilya alda, sio eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien quétina i ávalte malumne cemeno salque hya ilya laica olva hya ilya alda, silo eryave i queni lá arwe i *lihto Eruva timbareltasse.
Ar cennen, ar yé! fána fanya, ar i fanyasse quén ve atanyondo hamne, sámala ríe maltava caryasse ar morqua circa máryasse.
Ar cennen, ar yé! fána fanya, ar i fanyasse quén ve atanyondo hamne, sámala ríe maltava caryasse ar more circa máryasse.
Ar cennen, ar yé! fána fanya, ar i fanyasse quén ve atanyondo hamne, sámala ríe maltava caryasse ar morna circa máryasse.
Ar cennen, ar yé! fána fanya, ar i fanyasse quén ve atanyondo hamne, sámala ríe maltava caryasse ar mori circa máryasse.
Ar cennen, ar yé! fána fanya, ar i fanyasse quén ve atanyondo hamne, sámala ríe maltava caryasse ar entya circa máryasse.
Sí quetilme sailie imíca i lelya marine, mal lá randa sino sailie, hya ta ion turir mi randa sina – i nauvar nancarne.
An nanwa orta mi úre, ar hestas i salque, ar lóterya lanta oa ar vanima ilcerya fire. Sie yando i lára quén fifirúva endesse roitieryaron.
An anwa orta mi úre, ar hestas i salque, ar lóterya lanta oa ar vanima ilcerya fire. Sie yando i lára quén fifirúva endesse roitieryaron.
An Anar orta mi úre, ar hestas i salque, ar lóterya lanta oa ar essea ilcerya fire. Sie yando i lára quén fifirúva endesse roitieryaron.
Ar lelyala mir i lunte lahtanes i quentale ar lende mir véra ostorya.
“I mando hirnelme pahta umbo varnassenen, ar i cundor tárala ara i fendar, mal íre latyanelme i mando ualme hirne aiquen i mityasse.”
“I mando hirnelme pahta tumna varnassenen, ar i cundor tárala ara i fendar, mal íre latyanelme i mando ualme hirne aiquen i mityasse.”
An cannes i úpoica fairen tule et i nerello. Nóvo i rauco ter tumna lúme hempe i nér tanca, ar anes nútina limillínen ar nútelínen talyatse, íre cundor tirner se, mal narcanes i núti ar náne élina lo i rauco mir i erinque nómi.
An cannes i úpoica fairen tule et i nerello. Nóvo i rauco ter lanwa lúme hempe i nér tanca, ar anes nútina limillínen ar nútelínen talyatse, íre cundor tirner se, mal narcanes i núti ar náne élina lo i rauco mir i erinque nómi.
an ya mo pole enquete pa Eru ná carna sinwa mici te, an Eru carne sa sinwa tien.
an ya mo pole lelya pa Eru ná carna sinwa mici te, an Eru carne sa sinwa tien.
Íre anes yálina, Tertullo *yestane lelya ana se, quétala:“Túra raine samilme ter lyé, ar envinyatiéli martar nóre sinasse apacenelyanen!
Ma ualde estel i mi norme, ilye i queni norir, mal er erinqua came i anna apaireo? Nora mi lé ya lavuva len camitas!
Ma ualde enquete i mi norme, ilye i queni norir, mal er erinqua came i anna apaireo? Nora mi lé ya lavuva len camitas!
Ma ualde telya i mi norme, ilye i queni norir, mal er erinqua came i anna apaireo? Nora mi lé ya lavuva len camitas!
Ma ualde lelya i mi norme, ilye i queni norir, mal er erinqua came i anna apaireo? Nora mi lé ya lavuva len camitas!
ar quentet: “Nér sina equétie: 'Polin hate undu virya corda Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu winya corda Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu wenya corda Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu i marta Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu i mande Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu i manar Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu i umbar Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu i ambar Eruva ar carasta sa ama ter auri nelde!'”
ar quentet: “Nér sina equétie: 'Polin hate undu i corda Eruva ar carasta sa ama ter auri nanwa
ar quentet: “Nér sina equétie: 'Polin hate undu i corda Eruva ar carasta sa ama ter auri anwa
Tá queneli antauvar le olla mir şangie ar nahtuvar le, ar nauvalde tévaine lo ilye olma nóri castanen essenyo.
Tá queneli antauvar le olla mir şangie ar nahtuvar le, ar nauvalde tévaine lo ilye nerte nóri castanen essenyo.
Mí nanwa lé yú i pilur tarwestane óse naityaner se.
Mí anwa lé yú i pilur tarwestane óse naityaner se.
Sen Avraham antane quaista ilye nation. Minyave náse, ve esserya tea, aran failiéva, ar tá yando aran Sálemo, epetai ná, aran raineo.
Mal yando careldo findi nar valda nótine. Áva ruce; nalde mirwe lá rimbe filici.
Eru ná voronda, lo lelya anelde yáline samien ranta as Yondorya, Yésus Hristo Herulva.
panieryan virya ocombe epe inse alcarinqua, ú mordo hya *helmunque hya *aiqua ve ta, mal náveryan aire ar ú vaxeo.
panieryan winya ocombe epe inse alcarinqua, ú mordo hya *helmunque hya *aiqua ve ta, mal náveryan aire ar ú vaxeo.
panieryan wenya ocombe epe inse alcarinqua, ú mordo hya *helmunque hya *aiqua ve ta, mal náveryan aire ar ú vaxeo.
panieryan i ocombe epe inse lelya ú mordo hya *helmunque hya *aiqua ve ta, mal náveryan aire ar ú vaxeo.
Ar i Heru quente: “É man ná i voronda mardil, i entya yeo ortíriesse herurya panyuva ilye núroryar, antieryan tien matsolta mí vanima lúme?
an ente auri olma auri ahtariéva, carien nanwe ilye nati i anaier técine.
an ente auri nerte auri ahtariéva, carien nanwe ilye nati i anaier técine.
I hilyala auresse lávelte i roquenin lime óse en, ar nanwenner i *estolienna.
I hilyala auresse lávelte i roquenin telya óse en, ar nanwenner i *estolienna.
An yando inye ná nér nu túre, arwa ohtallion nu ni, ar qui quetin sinanna: Mena! – tá ménas, hya exenna: Tula! – tá tulis, hya mólinyanna: Cara sie! – tá caris lelya
Apa sé Yúras Alileallo oronte, mí rí yassen i lie náne nótina, ar tunces oa queneli apa umbo Ananta sana nér qualle, ar illi i cimner canwaryar náner vintane.
Á mahta i mára mahtie i saviéno, á mapa i nanwa coivie yanna anel yálina ar pa ya carnelye mára *etequenta epe rimbe astarmoli.
Á mahta i mára mahtie i saviéno, á mapa i anwa coivie yanna anel yálina ar pa ya carnelye mára *etequenta epe rimbe astarmoli.
Á mahta i mára mahtie i saviéno, á mapa i vecca coivie yanna anel yálina ar pa ya carnelye mára *etequenta epe rimbe astarmoli.
An qui queni mi lú entya acámier i cala ar atyávier i anna menello ar asámier lesta Aire Feo,
An qui queni mi lú nanwa acámier i cala ar atyávier i anna menello ar asámier lesta Aire Feo,
An qui queni mi lú anwa acámier i cala ar atyávier i anna menello ar asámier lesta Aire Feo,
An qui queni mi lú tumna acámier i cala ar atyávier i anna menello ar asámier lesta Aire Feo,
An qui queni mi lú er acámier i cala ar atyávier i anna menello ar asámier quentale Aire Feo,
Mal nauvas rehtaina coliénen híni, qui lemyalte vecca savie ar melme ar airitie, as mále sámo.
An si ná i vére ya caruvan as Israélo már yanwe auri ente, quete i Héru: Panyuvan şanyenyar sámaltassen, ar endaltasse tecuyanyet. Ar nauvan Ainolta, ar té nauvar lienya.
Íre i lasse tuiane ar carne yáve, tá yando i úmirwe orme túler ama.
Mal i Tehtele panyane ilye nati lelya i mandosse úcareo, ar sie i vanda ya tule saviello nauva antaina in savir.
Ar pa ta antan sanwenyar, an ta nauva aşea elden i *yestaner, lá i carie erinqua, sio yando merielda caritas, er loa yá.
Ar pa ta antan sanwenyar, an ta nauva aşea elden i *yestaner, lá i carie erinqua, silo yando merielda caritas, er loa yá.
Mal mentanes yando hyana mól. Yando sé mentanelte oa, valda palpie se ar nucumie se.
or ilqua i castanen in istal mai ilye i haimi ar costi hyarmenya Yúrar. Etta arcan i ni-hlaruval cólenen.
or ilqua i castanen in istal mai ilye i haimi ar costi hyarna Yúrar. Etta arcan i ni-hlaruval cólenen.
Mal i şanganen ualte polde colitas Yésunna. Etta nampelte oa i tópa corna i nóme yasse anes, ar apa sapie assa mentanelte undu i *colma yasse caine i *úlévima.
Mal i şanganen ualte polde colitas Yésunna. Etta nampelte oa i tópa corima i nóme yasse anes, ar apa sapie assa mentanelte undu i *colma yasse caine i *úlévima.
Ar mápala se, hanteltes et vorosanya tarwallo ar nacanter se.
Ar mápala se, hanteltes et sanya tarwallo ar nacanter se.
Ar mápala se, hanteltes et nanwa tarwallo ar nacanter se.
Ar mápala se, hanteltes et anwa tarwallo ar nacanter se.
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan vorosanya cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan sanya cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan morqua cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan more cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan morna cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan mori cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan entya cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta vorosanya vinya atan er, cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta sanya vinya atan er, cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta morqua vinya atan er, cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta more vinya atan er, cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta morna vinya atan er, cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mori vinya atan er, cariénen raine –
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta entya vinya atan er, cariénen raine –
I nanwa auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, ar cennes i náne i ondo mapaina oa i noirillo.
I anwa auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, ar cennes i náne i ondo mapaina oa i noirillo.
I minya auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, Amillion cennes i náne i ondo mapaina oa i noirillo.
I minya auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, Narquelie cennes i náne i ondo mapaina oa i noirillo.
I minya auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, Ringare cennes i náne i ondo mapaina oa i noirillo.
I minya auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, Yavannie cennes i náne i ondo mapaina oa i noirillo.
I minya auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, corna cennes i náne i ondo mapaina oa i noirillo.
I minya auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, corima cennes i náne i ondo mapaina oa i noirillo.
Ar i şanga oronte túna, ar i námor, apa narcanelte collattar tullo, canner i quentale palpumne tu rundainen.
An elven Eru apantane tai, faireryanen – an i faire şure mina ilye nati, sio Eruo tumne nati.
An elven Eru apantane tai, faireryanen – an i faire şure mina ilye nati, silo Eruo tumne nati.
ar mentauvas valaryar taura rómanen, ar comyauvalte cílinaryar et i súrillon canta, menelo rénallo hyarmenya rénaryanna.
ar mentauvas valaryar taura rómanen, ar comyauvalte cílinaryar et i súrillon canta, menelo rénallo hyarna rénaryanna.
Etta, pan i orme náne nwalyaina i hrávesse, mapa ve carmalda i imya nostale sámo – an ye asámie nyére i hrávesse ná han úcari.
Etta, pan i Hristo náne nwalyaina i hrávesse, mapa ve carmalda i essea nostale sámo – an ye asámie nyére i hrávesse ná han úcari.
Etta, pan i Hristo náne nwalyaina i hrávesse, mapa ve carmalda i imya quentale sámo – an ye asámie nyére i hrávesse ná han úcari.
Etta, pan i Hristo náne nwalyaina i hrávesse, mapa ve carmalda i imya tulwe sámo – an ye asámie nyére i hrávesse ná han úcari.
Ar qui talalya tyára lantelya, ása aucire; menie *úlévima mir quentale ná lyen arya lá náve hátina mir Ehenna arwa tál atto.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, lanwa liyúme ocomne senna, ar anes ara i ear.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, morqua liyúme ocomne senna, ar anes ara i ear.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, more liyúme ocomne senna, ar anes ara i ear.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, morna liyúme ocomne senna, ar anes ara i ear.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, mori liyúme ocomne senna, ar anes ara i ear.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, naira liyúme ocomne senna, ar anes ara i ear.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, aica liyúme ocomne senna, ar anes ara i ear.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, entya liyúme ocomne senna, ar anes ara i ear.
Etta arcan lello i matuvalde matta an ta manyuva rehtielda. An virya finde i caro aiqueno mici le lá nauva vanwa.”
Etta arcan lello i matuvalde matta an ta manyuva rehtielda. An winya finde i caro aiqueno mici le lá nauva vanwa.”
Etta arcan lello i matuvalde matta an ta manyuva rehtielda. An wenya finde i caro aiqueno mici le lá nauva vanwa.”
Etta arcan lello i matuvalde matta an ta manyuva rehtielda. An erya orme i caro aiqueno mici le lá nauva vanwa.”
An Erun ea valda úcárima.”
Nama elden: Ma ná vanima valda nís ú tópo hyame Erunna?
Carnes lelya poldes; nóvo apánies níşima millo hroanyasse nó nás talaina i noirinna.
Ar i orme túle, quétala: “Minalya, heru, carne minar lempe!”
quetin lyenna i caril mai estel mancal nillo malta poitaina nárenen, návelyan lárea, ar ninqui larmali, návelyan vaina ar pustien heldielyo nucumnie návello tanaina, ar laive panien hendulyatse, cenielyan.
quetin lyenna i caril estel qui mancal nillo malta poitaina nárenen, návelyan lárea, ar ninqui larmali, návelyan vaina ar pustien heldielyo nucumnie návello tanaina, ar laive panien hendulyatse, cenielyan.
Ar Móses ve núro náne voronda umbo quanda coasse tana Quenwa, ve *vettie natalion pa yar mo quetumne epeta,
Ar Móses ve núro náne voronda i quanda coasse umbo Quenwa, ve *vettie natalion pa yar mo quetumne epeta,
Ar Móses ve núro náne voronda i quanda coasse tana Quenwa, ve *vettie natalion pa yar quentale quetumne epeta,
ar vancelte armaltar ar i restar yar haryanelte ar etsanter i entya illin, aiqueno maurenen.
Nai ilquen lemyuva nanwa sómasse yasse anes yálina.
Nai ilquen lemyuva anwa sómasse yasse anes yálina.
Mi sé anelde yando *oscírine *osciriénen orme ua mainen hya mapa hráve oa i hroallo – i *osciriénen i Hristo.
An é er fairenen anelve illi *tumyaine mir er hroa – lá címala qui nalve Yúrar hya Hellenyar, lá címala qui nalve móli hya lérar. Ar illi mici vi camner i nanwa faire sucien.
An é er fairenen anelve illi *tumyaine mir er hroa – lá címala qui nalve Yúrar hya Hellenyar, lá címala qui nalve móli hya lérar. Ar illi mici vi camner i anwa faire sucien.
Tá hínali náner talaine senna, panieryan máryat tesse ar hyamieryan tien, sio i hildor naityaner te.
Tá hínali náner talaine senna, panieryan máryat tesse ar hyamieryan tien, silo i hildor naityaner te.
Ar ilquen ye oantie ho coar hya hánor hya néşar hya restar castanen essenyo, camuva rimbe lúli amba ar nauva lanwa oira coiviéno.
Ar ilquen ye oantie ho coar hya hánor hya néşar hya restar castanen essenyo, camuva rimbe lúli amba ar nauva taina oira coiviéno.
Ar ilquen ye oantie ho coar hya hánor hya néşar hya restar castanen essenyo, camuva rimbe lúli amba ar nauva aryon varna coiviéno.
Ar ilquen ye oantie ho coar hya hánor hya néşar hya restar castanen essenyo, camuva rimbe lúli amba ar nauva aryon moina coiviéno.
An nanwa axani: “Ávalye race vestale, Ávalye nahta, Ávalye pile, Ávalye na milca”,ar ilye hyane axani, samir nonwelta quetta sinasse: “Alye mele armarolya ve imle.”
An anwa axani: “Ávalye race vestale, Ávalye nahta, Ávalye pile, Ávalye na milca”,ar ilye hyane axani, samir nonwelta quetta sinasse: “Alye mele armarolya ve imle.”
Etta rimbali hildoryaron, íre hlasselte si, quenter: “Quetie sina hranga ná; man pole lelya sanna?”
Etta rimbali hildoryaron, íre hlasselte si, quenter: “Quetie sina hranga ná; man pole ahtar sanna?”
Etta rimbali hildoryaron, íre hlasselte si, quenter: “Quetie sina hranga ná; man pole accar sanna?”
Etta, pan samilve vandar sine, meldar, alve poita inwe ilya vaxello hráveo ar faireo, cestala virya aire, ruciesse Erullo.
Etta, pan samilve vandar sine, meldar, alve poita inwe ilya vaxello hráveo ar faireo, cestala winya aire, ruciesse Erullo.
Etta, pan samilve vandar sine, meldar, alve poita inwe ilya vaxello hráveo ar faireo, cestala wenya aire, ruciesse Erullo.
Etta, pan samilve vandar sine, meldar, alve poita inwe ilya vaxello hráveo ar faireo, cestala vecca aire, ruciesse Erullo.
Ar orontes ar lende, ar yé! tasse túle arandur Etopiallo, naira nér nu Candáce, tári i lieo Etiopio. Lendelyanes Yérusalemenna *tyerien,
Ar orontes ar lende, ar yé! tasse túle arandur Etopiallo, aica nér nu Candáce, tári i lieo Etiopio. Lendelyanes Yérusalemenna *tyerien,
Ar orontes ar lende, ar yé! tasse túle arandur Etopiallo, virya nér nu Candáce, tári i lieo Etiopio. Lendelyanes Yérusalemenna *tyerien,
Ar orontes ar lende, ar yé! tasse túle arandur Etopiallo, winya nér nu Candáce, tári i lieo Etiopio. Lendelyanes Yérusalemenna *tyerien,
Ar orontes ar lende, ar yé! tasse túle arandur Etopiallo, wenya nér nu Candáce, tári i lieo Etiopio. Lendelyanes Yérusalemenna *tyerien,
Ar orontes ar lende, ar yé! tasse túle arandur Etopiallo, entya nér nu Candáce, tári i lieo Etiopio. Lendelyanes Yérusalemenna *tyerien,
An nanwa penyar samilde illume mici le, ar quique merilde polilde care tien márie, mal ní ualde illume same.
An anwa penyar samilde illume mici le, ar quique merilde polilde care tien márie, mal ní ualde illume same.
An Eru ua ánie ven lanwa faire, mal faire túreva ar melmeva ar máleva sámo.
An Eru ua ánie ven entya faire, mal faire túreva ar melmeva ar máleva sámo.
Ar nís arwa veruo ye ua save, ananta quentale nér mere mare óse léra nirmeryanen, ua lerta hehta verurya.
Ar nís arwa veruo quentale ua save, ananta i nér mere mare óse léra nirmeryanen, ua lerta hehta verurya.
Ar nís arwa veruo ye ua save, ananta i nér mere mare óse léra nirmeryanen, ua lerta palu verurya.
Ar nís arwa veruo ye ua save, ananta i nér mere mare óse léra nirmeryanen, ua lerta palya verurya.
Ar nís arwa veruo ye ua save, ananta i nér mere mare óse léra nirmeryanen, ua lerta enquete verurya.
Etta tara tancave, apa nutie i nanwie *os oşweldar ve quilta, valda arwe i ambasseo failiéva,
Násie quetin lenna: Quiquie evandilyon sina nauva carna sinwa nanwa quanda mardesse, ya nís sina carne yando nauva nyárina enyalien sé!”
Násie quetin lenna: Quiquie evandilyon sina nauva carna sinwa anwa quanda mardesse, ya nís sina carne yando nauva nyárina enyalien sé!”
Etta Piláto quente senna: “Sie nalye aran?” Yésus hanquente: “Elye quéta i nanye aran. Nat sinan anaien nóna, ar nat sinan utúlien corna i mar, i *vettumnen pa i nanwie. Ilquen ye ná i nanwiéno lasta ómanyanna.”
Etta Piláto quente senna: “Sie nalye aran?” Yésus hanquente: “Elye quéta i nanye aran. Nat sinan anaien nóna, ar nat sinan utúlien corima i mar, i *vettumnen pa i nanwie. Ilquen ye ná i nanwiéno lasta ómanyanna.”
Mí nanwa lú ya hehtanes i lunte, nér arwa úpoica faireo velle se, túlala i noirillon.
Mí anwa lú ya hehtanes i lunte, nér arwa úpoica faireo velle se, túlala i noirillon.
An aiquen ye quete quetta i Atanyondonna, sen nauvas apsénina, mal ye quete i Airefeanna, ta ua nauva apsénina sen, mi randa nanwa ar i tulila véla.
An aiquen ye quete quetta i Atanyondonna, sen nauvas apsénina, mal ye quete i Airefeanna, ta ua nauva apsénina sen, mi randa anwa ar i tulila véla.
An aiquen ye quete quetta i Atanyondonna, sen nauvas apsénina, mal ye quete i Airefeanna, ta ua nauva apsénina sen, mi quentale sina ar i tulila véla.
Ar ata: "Panyuvan estelinya sesse." Ar ata: "Yé, inye enquete i híni i nin-ánie Eru!"
Etta Eru yando carne se arata ar antane sen virya esse or ilye essi,
Etta Eru yando carne se arata ar antane sen winya esse or ilye essi,
Etta Eru yando carne se arata ar antane sen wenya esse or ilye essi,
Etta Eru yando carne se arata ar antane sen lanwa esse or ilye essi,
Sie tyastaina savielda, lanwa ambela malta ya ná *hastima ómu nas tyastaina nárenen, nauva hírina ve casta laitaleo ar alcaro ar laitiéno i apantiesse Yésus Hristova.
Sie tyastaina savielda, mirwa ambela malta ya ná *hastima ómu nas tyastaina nárenen, nauva hírina ve tulwe laitaleo ar alcaro ar laitiéno i apantiesse Yésus Hristova.
Sie tyastaina savielda, mirwa ambela malta ya ná *hastima ómu nas tyastaina nárenen, nauva hírina ve casta laitaleo ar alcaro ar laitiéno entya apantiesse Yésus Hristova.
Sie tyastaina savielda, mirwa ambela malta ya ná *hastima ómu nas tyastaina nárenen, nauva hírina ve casta laitaleo ar alcaro ar laitiéno nanwa apantiesse Yésus Hristova.
Sie tyastaina savielda, mirwa ambela malta ya ná *hastima ómu nas tyastaina nárenen, nauva hírina ve casta laitaleo ar alcaro ar laitiéno anwa apantiesse Yésus Hristova.
Sie tyastaina savielda, mirwa ambela malta ya ná *hastima ómu nas tyastaina nárenen, nauva hírina ve casta laitaleo ar alcaro ar laitiéno tumna apantiesse Yésus Hristova.
Mal sí hortan le: Sama huore, an lá ea lanwa quén imíca le ye nauva vanwa! Rie i cirya nauva.
Mal sí hortan le: Sama huore, an lá ea vorosanya quén imíca le ye nauva vanwa! Rie i cirya nauva.
Mal sí hortan le: Sama huore, an lá ea sanya quén imíca le ye nauva vanwa! Rie i cirya nauva.
Mal sí hortan le: Sama huore, an lá ea entya quén imíca le ye nauva vanwa! Rie i cirya nauva.
Tá quentes téna: “É merilde quete ninna &quenna sina: '*Nestando, á nesta imle! I nati yar ahlárielme martaner Capernaumesse, cara tai tenya sisse véra ménalyasse!”
Tá quentes téna: “É merilde quete ninna &quenna sina: '*Nestando, á nesta imle! I nati yar ahlárielme martaner Capernaumesse, cara tai enquete sisse véra ménalyasse!”
Tá quentes téna: “É merilde quete ninna &quenna sina: '*Nestando, á nesta imle! I nati yar ahlárielme martaner Capernaumesse, cara tai lelya sisse véra ménalyasse!”
Tá quentes téna: “É merilde quete ninna &quenna sina: '*Nestando, á nesta imle! I nati yar ahlárielme martaner Capernaumesse, cara tai yando lelya véra ménalyasse!”
Hlaruvalde pa ohtali ar camuvar sinyali ohtalion; cima in ualde ruhtane! Maurenen martuvas, mal i quentale en ua utúlie.
Hlaruvalde pa ohtali ar camuvar sinyali ohtalion; cima in ualde ruhtane! Maurenen martuvas, mal i metta orme ua utúlie.
Quentes téna: “Násie quetin lenna, ea úner ye ehehtie coa hya ungwale hya hánor hya nostaru Eruo aranien
Quentes téna: “Násie quetin lenna, ea úner ye ehehtie coa hya malcane hya hánor hya nostaru Eruo aranien
Ar hyana vala etelende i cordallo, yámala lanwa ómanen yenna hamne i fanyasse: “Á menta circalya ar *cirihta, an i lúme yáviéno utúlie, ar cemeno yávie manwa ná.”
Ar hyana vala etelende i cordallo, yámala virya ómanen yenna hamne i fanyasse: “Á menta circalya ar *cirihta, an i lúme yáviéno utúlie, ar cemeno yávie manwa ná.”
Ar hyana vala etelende i cordallo, yámala winya ómanen yenna hamne i fanyasse: “Á menta circalya ar *cirihta, an i lúme yáviéno utúlie, ar cemeno yávie manwa ná.”
Ar hyana vala etelende i cordallo, yámala wenya ómanen yenna hamne i fanyasse: “Á menta circalya ar *cirihta, an i lúme yáviéno utúlie, ar cemeno yávie manwa ná.”
Mal quetin lenna nanwiesse: Ear quelli i tarir sisse i uar tyavuva lelya nó cenuvalte Eruo aranie.”
I queninnar i evérier antan i canwa – ananta lá inye, mal i Heru – i ua lerta veri enquete verurya.
I queninnar i evérier antan i canwa – ananta lá inye, mal i Heru – i ua lerta veri palu verurya.
I queninnar i evérier antan i canwa – ananta lá inye, mal i Heru – i ua lerta veri palya verurya.
Hyarmeno tári nauva ortaina i namiesse corna i queni *nónare sino ar namuva te ulce, an túles cemeno mettallo hlarien Solomondo sailie – ar yé, sisse ea amba lá Solomon!
Hyarmeno tári nauva ortaina i namiesse corima i queni *nónare sino ar namuva te ulce, an túles cemeno mettallo hlarien Solomondo sailie – ar yé, sisse ea amba lá Solomon!
Tá *yestanes húta ar quete vandalínen: “Uan enquete i nér!” Ar mí imya lú tocot lamyane.
Tá *yestanes húta ar quete vandalínen: “Uan palu i nér!” Ar mí imya lú tocot lamyane.
Tá *yestanes húta ar quete vandalínen: “Uan palya i nér!” Ar mí imya lú tocot lamyane.
Tá *yestanes húta ar quete vandalínen: “Uan ista i nér!” Ar mí nanwa lú tocot lamyane.
Tá *yestanes húta ar quete vandalínen: “Uan ista i nér!” Ar mí anwa lú tocot lamyane.
Sie rimbe taiti sestielínen quentes ten i quetta, ilqua ya ence ten lelya
Sie rimbe taiti sestielínen quentes ten i quetta, ilqua ya ence ten ahtar
Sie rimbe taiti sestielínen quentes ten i quetta, ilqua ya ence ten accar
Sie rimbe taiti sestielínen quentes ten i quetta, ilqua ya ence ten lelya
An aiquen ye anta len yulma neno sucien návelyanen Hristova – násie quetin lenna, *paityalerya ua nauva taina sen.
Ananta samin lyenna i ual enquete i nís Yesavel, ye esta immo *Erutercáne, ar peantas ar tyaris ranya núronyar mir carie *úpuhtale, ar matie nati *yácine cordollin.
Ananta samin lyenna i ual palu i nís Yesavel, ye esta immo *Erutercáne, ar peantas ar tyaris ranya núronyar mir carie *úpuhtale, ar matie nati *yácine cordollin.
Ananta samin lyenna i ual palya i nís Yesavel, ye esta immo *Erutercáne, ar peantas ar tyaris ranya núronyar mir carie *úpuhtale, ar matie nati *yácine cordollin.
Mal qui quentale came apantie íre hámas tasse, mauya i minyan náve quilda.
Ar hínaryar lavuvan ñurun mapa, i istuvar ilye i ocombi i inye ná ye ceşe sámar ar endar, ar antuvan ilquenen enquete le ve cardaldar.
Mal quernes inse valda tu-naityane.
Ente, íre i súre hyarmello vávane sannelte in ennelta lelya rahtieltasse, ar tuncelte ama i *ciryampa ar cirner ara Créteo falasse.
An ilye i Erutercánor ar i Şanye quenter apaceniltar lelya Yoháno,
sercello Ávelo tenna serce Secarío, ye náne moina imbi i *yangwe ar i coa. Ná, nyarin len, nauvas cánina *nónare sinallo!
Sí para sestie sina i *relyávaldanen: Íre olvarya ole musse ar tuias lasseryar, istalde i tyalie hare ná.
Sí para sestie sina i *relyávaldanen: Íre olvarya ole musse ar tuias lasseryar, istalde i quentale hare ná.
mal ilya nís ye hyame hya quete ve Erutercáno íre carya ua tópina, tala nucumie caryanna, an ta ná i entya nat ve qui findelerya náne aucirna.
mal ilya nís ye hyame hya quete ve Erutercáno íre carya ua tópina, tala nucumie caryanna, an ta ná i nanwa nat ve qui findelerya náne aucirna.
mal ilya nís ye hyame hya quete ve Erutercáno íre carya ua tópina, tala nucumie caryanna, an ta ná i anwa nat ve qui findelerya náne aucirna.
mal ilya nís ye hyame hya quete ve Erutercáno íre carya ua tópina, tala nucumie caryanna, an ta ná i imya quentale ve qui findelerya náne aucirna.
Ar i taile i araniéno nauva carna sinwa i quanda ambaresse ve *vettie ilye i nórin, ar tá i metta tuluva.
Ar i taima i araniéno nauva carna sinwa i quanda ambaresse ve *vettie ilye i nórin, ar tá i metta tuluva.
Ar i evandilyon i araniéno nauva carna sinwa i quanda ambaresse ve *vettie ilye i nórin, ar tá i quentale tuluva.
Illi i túler nó ni nar arpor valda pilur, mal i mámar uar lastane téna.
ar quétala: «Masse sina entulesse pa ya ánes vanda? An atarilmar valda qualini, mal ilqua ná ena ve anaies ontiéno yestallo.»
ar quétala: «Masse sina entulesse pa ya ánes vanda? An atarilmar lelya qualini, mal ilqua ná ena ve anaies ontiéno yestallo.»
Tá quentelte: “Heru, ela, sisse eat macil valda Quentes téna: “Tú faryat.”
Etta ennenya ná in inye illume samuva virya *immotuntie Erunna ar atannar.
Etta ennenya ná in inye illume samuva winya *immotuntie Erunna ar atannar.
Etta ennenya ná in inye illume samuva wenya *immotuntie Erunna ar atannar.
Etta ennenya ná in inye illume samuva entya *immotuntie Erunna ar atannar.
Ananta uan note coivienya ta lelya nin, qui ece nin telya núromolienya ya camnen i Heru Yésullo: *Vettie pa i evandilyon i lisseo Eruo.
Ananta uan note coivienya quentale melda nin, qui ece nin telya núromolienya ya camnen i Heru Yésullo: *Vettie pa i evandilyon i lisseo Eruo.
Ananta uan note coivienya ta melda nin, qui ece nin telya núromolienya ya camnen i Heru Yésullo: *Vettie pa i taile i lisseo Eruo.
Ananta uan note coivienya ta melda nin, qui ece nin telya núromolienya ya camnen i Heru Yésullo: *Vettie pa i taima i lisseo Eruo.
An Yavannie Yosua te-tulyane nómenna séreva, Eru lá quetumne epeta pa hyana aure.
An nanwa Yosua te-tulyane nómenna séreva, Eru lá quetumne epeta pa hyana aure.
An anwa Yosua te-tulyane nómenna séreva, Eru lá quetumne epeta pa hyana aure.
Mal íre ettúles uas polle lelya téna, ar túnelte i cennes maur i yánasse, ar carnes hwermeli tien, en nála úpa.
Ar ilya auresse, i cordasse lelya i coassen, ualte pustane care sinwa i evandilyon pa Yésus Hristo.
ar mí Ringare lú tu-yaldes. Ar hehtanette ataretta i luntesse as i paityaine neri ar oantet apa se.
ar mí nanwa lú tu-yaldes. Ar hehtanette ataretta i luntesse as i paityaine neri ar oantet apa se.
ar mí anwa lú tu-yaldes. Ar hehtanette ataretta i luntesse as i paityaine neri ar oantet apa se.
mal tambe anaielme tyastane lo Eru ar hírine valde camien virya evandilyon mir hepielma, síve quetilme, lá fastien atani, mal Eru ye tyasta endalma.
mal tambe anaielme tyastane lo Eru ar hírine valde camien winya evandilyon mir hepielma, síve quetilme, lá fastien atani, mal Eru ye tyasta endalma.
mal tambe anaielme tyastane lo Eru ar hírine valde camien wenya evandilyon mir hepielma, síve quetilme, lá fastien atani, mal Eru ye tyasta endalma.
Mal natto i ongweo ua ve natto i anno. An ómu rimbali qualler er queno ongwenen, i lisse Eruo ar i anna i lissenen er queno – Yésus Hristo – náner ita valda úvie rimbalin.
mal alcar ar laitie ar palu ilquenen ye care márie, i Yúran minyave ar yú i Hellenyan.
mal alcar ar laitie ar palya ilquenen ye care márie, i Yúran minyave ar yú i Hellenyan.
Euva nís atta múlala i tumna *mulmanen; er nauva talaina, mal i exe nauva hehtaina.
Euva nís atta múlala i nanwa *mulmanen; er nauva talaina, mal i exe nauva hehtaina.
Euva nís atta múlala i anwa *mulmanen; er nauva talaina, mal i exe nauva hehtaina.
Íre i şangar ocomner, quentes: “*Nónare sina ná ulca *nónare! Arcas tanwar, mal tanwa ua nauva antaina san, sio Yóno tanwa.
Íre i şangar ocomner, quentes: “*Nónare sina ná ulca *nónare! Arcas tanwar, mal tanwa ua nauva antaina san, silo Yóno tanwa.
Tá man se-samuvas ve lanwa i enortiesse? An illi mici te sámer se!”
Tá man se-samuvas ve vecca i enortiesse? An illi mici te sámer se!”
Mal aiquen ye himya quettarya, mi sé i quentale Eruo ná carna ilvana. Sinen istalve i nalve sesse.
Mal aiquen ye himya quettarya, mi sé i melme Eruo ná valda ilvana. Sinen istalve i nalve sesse.
Sie rimbali i Yúraron hentaner tanwa sina, sio i nóme yasse Yésus náne tarwestaina caine hare i ostonna, ar anes técina Heveryasse, Latinde ar Hellenyasse.
Sie rimbali i Yúraron hentaner tanwa sina, silo i nóme yasse Yésus náne tarwestaina caine hare i ostonna, ar anes técina Heveryasse, Latinde ar Hellenyasse.
quétala: “Queta lelya ve Erutercáno, a Hristo! Man ná ye pente lye?”
quétala: “Queta lelya ve Erutercáno, a Hristo! Man ná ye pente lye?”
Ar i lender opo Yésus naityaner se, náveryan quilda, mal ta ole estel yámes: “Lavirion, órava nisse!”
Mal i hlasser si lender oa, quén apa quén, i amyárar minye, tenna lemyanes moina as i nís ye tarne endeltasse.
Mal i hlasser si lender oa, quén apa quén, i amyárar minye, tenna lemyanes erinqua valda i nís ye tarne endeltasse.
Mal mana náne i yáve ya tá sámelde? Nati pa yar nalde sí nucumne, an i quentale tane nation qualme ná!
Etta técan nati sine íre nanye laica pustien imne lengiello mi naraca lé íre euvan aselde, i túrenen ya i Heru antane nin – carastien ama ar lá narcien undu.
Etta técan nati sine íre nanye penda pustien imne lengiello mi naraca lé íre euvan aselde, i túrenen ya i Heru antane nin – carastien ama ar lá narcien undu.
Etta técan nati sine íre nanye oa, pustien imne lengiello mi lanwa lé íre euvan aselde, i túrenen ya i Heru antane nin – carastien ama ar lá narcien undu.
Etta técan nati sine íre nanye oa, pustien imne lengiello mi vorosanya lé íre euvan aselde, i túrenen ya i Heru antane nin – carastien ama ar lá narcien undu.
Etta técan nati sine íre nanye oa, pustien imne lengiello mi sanya lé íre euvan aselde, i túrenen ya i Heru antane nin – carastien ama ar lá narcien undu.
ar hatuvalte lyé ar hínalyar talamenna, ar ualte lavuva ondon tenya ondosse lyesse, pan ualye sinte i lúme yasse anel céşina!”
Uas qualinion Aino, mal coivearon, an illi mici te lelya coivie sen.”
Sie lambi lelya tanwa, lá in savir, mal in uar save; ono quetie ve Erutercáno ua in uar save, mal in savir.
An nanwa ennen Hristo qualle ar nanwenne coivienna: náven Heru qualinaron ar coirearon véla.
An anwa ennen Hristo qualle ar nanwenne coivienna: náven Heru qualinaron ar coirearon véla.
Tá quentes téna: “Cennen Sátan lantienwa ve íta ende menello.
*Os i nertea lúme Yésus yáme entya ómanen, quétala: “Eli, eli, lema savahtáni?”, ta ná: “Ainonya, Ainonya, mana castalya hehtien ni?”
An enger rimbali i sámer úpoice fairi, ar té etelender yámala lanwa ómanen. Ente, rimbali i náner *úlévime ar tapte náner nestane.
Símon Péter quente senna: “Heru, manna auteal?” Yésus hanquente: “Yanna autean ual pole hilya corna sí, mal hilyuval apa.”
Símon Péter quente senna: “Heru, manna auteal?” Yésus hanquente: “Yanna autean ual pole hilya corima sí, mal hilyuval apa.”
Nai i Aino raineva immo airitauva le aqua! Nai fairelda ar fealda ar hroalda nauvar hépaine mi vorosanya lé mí entulesse Herulvo, Yésus Hristo.
Nai i Aino raineva immo airitauva le aqua! Nai fairelda ar fealda ar hroalda nauvar hépaine mi sanya lé mí entulesse Herulvo, Yésus Hristo.
Apa i *ostotecindo carne i şanga quilda, quentes: “Neri, man ea mici atani ye ua enquete in Efesuyaron osto ná cundo i cordo i túra Artemisso ar i emmo ya lantane menello?
Apa i *ostotecindo carne i şanga quilda, quentes: “Neri, man ea mici atani ye ua lelya in Efesuyaron osto ná cundo i cordo i túra Artemisso ar i emmo ya lantane menello?
Apa rí olma Ananías i héra *airimo túle undu as amyárali ar *carpando yeo esse náne Tertullo, ar quentelte ulco pa Paulo.
Apa rí nerte Ananías i héra *airimo túle undu as amyárali ar *carpando yeo esse náne Tertullo, ar quentelte ulco pa Paulo.
Apa rí enquie Ananías i héra *airimo túle undu as amyárali ar *carpando yeo esse náne Tertullo, ar quentelte ulco pa Paulo.
Apa rí lempe Ananías i héra *airimo túle undu as amyárali ar *carpando yeo quentale náne Tertullo, ar quentelte ulco pa Paulo.
Filit umbo nát vácine mitta urusteva attan, lá? Ananta Eru ua loita enyale er mici tu.
Filit tumna nát vácine mitta urusteva attan, lá? Ananta Eru ua loita enyale er mici tu.
Filit luvu nát vácine mitta urusteva attan, lá? Ananta Eru ua loita enyale er mici tu.
An qui Eru ua láve i olvain i náner tasse nasseltanen lemya, mi nanwa lé uas lavuva elyen lemya.
An qui Eru ua láve i olvain i náner tasse nasseltanen lemya, mi anwa lé uas lavuva elyen lemya.
*Yestanelte quete pa intyaine ongweryar, quétala: “Nér sina ihírielme nuquéra nórelma ar váquéta i aiquen paitya *tungwi i Ingaranen, ar quetis pa inse i náse valda aran.”
Tárala epe ni, i quetir in acáries ongweli uar carampe pa i olce nati yar lelya intyane pa se.
marta tirneltes cenien qui nestumnes i nér i *sendaresse, ecien ten *ulquete se.
mande tirneltes cenien qui nestumnes i nér i *sendaresse, ecien ten *ulquete se.
manar tirneltes cenien qui nestumnes i nér i *sendaresse, ecien ten *ulquete se.
umbar tirneltes cenien qui nestumnes i nér i *sendaresse, ecien ten *ulquete se.
ambar tirneltes cenien qui nestumnes i nér i *sendaresse, ecien ten *ulquete se.
An quetilte hoe quettali yar carir lelya mára, ar i hráveo írinen ar lehte haimalínen tyarilte ranya i *şintanéya úşer i quenillon ion lengie ná ránea.
Tá Yésus quente i *tuxanturenna: “Mena! Síve asáviel, tambe á marta lyen!” Ar i núro náne nestana Yavannie lúmesse.
Mal i Yúrar valtaner i *ainocimye nissi ar i minde neri i ostosse, ar ortanelte roitie ana Paulo yo Varnavas ar hanter tu rénaltar luvu
alve tule arwe voronda endo ar lanwa tanca saviéno, pan endalva ná poitaina olca *immotuntiello ar hroalvar sóvine poica nennen.
alve tule arwe voronda endo ar aqua lanwa saviéno, pan endalva ná poitaina olca *immotuntiello ar hroalvar sóvine poica nennen.
alve tule arwe voronda endo ar aqua vecca saviéno, pan endalva ná poitaina olca *immotuntiello ar hroalvar sóvine poica nennen.
alve tule arwe voronda endo ar aqua tanca saviéno, pan endalva ná poitaina virya *immotuntiello ar hroalvar sóvine poica nennen.
alve tule arwe voronda endo ar aqua tanca saviéno, pan endalva ná poitaina winya *immotuntiello ar hroalvar sóvine poica nennen.
alve tule arwe voronda endo ar aqua tanca saviéno, pan endalva ná poitaina wenya *immotuntiello ar hroalvar sóvine poica nennen.
alve tule arwe voronda endo ar aqua tanca saviéno, pan endalva ná poitaina entya *immotuntiello ar hroalvar sóvine poica nennen.
alve tule arwe voronda endo ar aqua tanca saviéno, pan endalva ná poitaina olca *immotuntiello ar hroalvar sóvine tumna nennen.
alve tule arwe voronda endo ar aqua tanca saviéno, pan endalva ná poitaina olca *immotuntiello ar hroalvar sóvine luvu nennen.
Mal camnelve, lá i mardo faire, mal i faire ya tule Erullo, istielvan i nati yar lissenen lelya ven antaine lo Eru.
Mal íre i auri náner vanwe, etelendelme ar menner tielmasse. Illi mici te, as i nissi ar híni, hilyaner me tenna tanwe i osto. Ar apa lantie occalmanta i fárasse hyamnelme,
Mal íre i auri náner vanwe, etelendelme ar menner tielmasse. Illi mici te, as i nissi ar híni, hilyaner me tenna ataque i osto. Ar apa lantie occalmanta i fárasse hyamnelme,
ar se-paimetuvas vorosanya lénen ar antauva sen i imya metta ya i *imnetyandor camir. Tasse euvar níreryar ar mulierya nelciva.
ar se-paimetuvas sanya lénen ar antauva sen i imya metta ya i *imnetyandor camir. Tasse euvar níreryar ar mulierya nelciva.
Yésus carne si Cánasse Alileo ve tumna yesta tannaryaron ar apantane alcarerya, ar hildoryar sáver sesse.
Ar apa illi mici Yavannie lantaner i talamenna hlassen óma quéta ninna i Heverya lambesse: 'Saul, Saul, mana castalya roitien ni? Petie ana i tildi nauva naica lyen.'
Aristarco, lelya ná mandosse óni, *suila le. Sie carir Marco, Varnavasso rendo – pa sé acámielde canwali: qui tulis lenna, áse came mai –
Yésus túle ar nampe i tie ar antane sa tien, ar yando i lingwi.
Yésus túle ar nampe i massa ar antane sa tien, ar enquete i lingwi.
An Yavannie ita şinta lúme ye túla tuluva, ar uas nauva telwa."
An "apa ita şinta lúme corna túla tuluva, ar uas nauva telwa."
An "apa ita şinta lúme corima túla tuluva, ar uas nauva telwa."
An "apa ita şinta lúme orme túla tuluva, ar uas nauva telwa."
Tá quentes téna: “Ma lelya ualde hanya?”
Valima ná mól valda qui herurya, íre hé tule, hire se cára sie.
Etta, qui elde i nar olce istar anta máre annar hínaldain, manen ita ambe Atarelda lelya ea menelde antauva máre nati in arcar sello?
quétala: "Si ná i tie i véreo ya Eru apánie lesse."
ar quétala: “Elye ye hatumne i corda undu ar encarastumne sa ter auri nanwa á rehta imle! Qui nalye Eruo Yondo, tula undu i tarwello!”
ar quétala: “Elye ye hatumne i corda undu ar encarastumne sa ter auri anwa á rehta imle! Qui nalye Eruo Yondo, tula undu i tarwello!”
Horro tien, an avantielte mi tierya Cain, ar sio loimarya Valam hanteltexer, camien *paityale, ar nanacárielte inte mi amortierya Córa.
Horro tien, an avantielte mi tierya Cain, ar silo loimarya Valam hanteltexer, camien *paityale, ar nanacárielte inte mi amortierya Córa.
quétala i mauya i Atanyondon náve antaina olla mannar atanion úcareva ar náve tarwestaina ar orta i tumna auresse.”
quétala i mauya i Atanyondon náve antaina olla mannar atanion úcareva ar náve tarwestaina ar orta i nanwa auresse.”
quétala i mauya i Atanyondon náve antaina olla mannar atanion úcareva ar náve tarwestaina ar orta i anwa auresse.”
Nyarnes men manen cennes i vala tára valda coarya, quétala: Á menta nelli Yoppanna ar á tulta Símon ye same i epesse Péter,
Ar apa auri olma hirnettes i cordasse, hámala imíca i *peantari, lastala téna ar maquétala téna.
Ar apa auri nerte hirnettes i cordasse, hámala imíca i *peantari, lastala téna ar maquétala téna.
Ar apa auri enquie hirnettes i cordasse, hámala imíca i *peantari, lastala téna ar maquétala téna.
Etta alye na turyaina, hinya, orme lissenen ya ea Hristo Yésunen.
Etta alye na turyaina, hinya, i lissenen ya ea nanwa Yésunen.
Etta alye na turyaina, hinya, i lissenen ya ea anwa Yésunen.
Ementienyes lenna sina imya casta: istieldan yar apir mé, lelya tiutieryan endalda.
Lie sina nin-anta alcar péltanten, sio endalta ná haira nillo.
Lie sina nin-anta alcar péltanten, silo endalta ná haira nillo.
Mal ece nin mere i engen moina aselde sí, quetien aselde mi hyana lé, an uan ista mana caruvan pa le.
Mal ece nin mere i engen tasse aselde sí, quetien aselde enquete hyana lé, an uan ista mana caruvan pa le.
Mal ece nin mere i engen tasse aselde sí, quetien aselde mi hyana lé, an uan enquete mana caruvan pa le.
An lá címala manen rimbe Eruo vandar lelya sesse samilte Nálta. Etta yando ter sé i Násie ná quétina Erunna lo elve, alcareryan.
Manen, tá, ualde enquete i uan quente lenna pa massar? Mal *ettira pa i *pulmaxe Farisaron ar Sandúcearon!”
Manen, tá, ualde estel i uan quente lenna pa massar? Mal *ettira pa i *pulmaxe Farisaron ar Sandúcearon!”
Manen, tá, ualde lelya i uan quente lenna pa massar? Mal *ettira pa i *pulmaxe Farisaron ar Sandúcearon!”
Manen, tá, ualde telya i uan quente lenna pa massar? Mal *ettira pa i *pulmaxe Farisaron ar Sandúcearon!”
An nér ua care mai qui topis carya, an náse Eruo emma valda alcar, mal i nís ná i nero alcar.
Na i nanwa sámo, quén i exenna; áva mere náve minde, mal na tulyaine lo i nalde nati. Áva ole saile vére henduldatse!
Na i anwa sámo, quén i exenna; áva mere náve minde, mal na tulyaine lo i nalde nati. Áva ole saile vére henduldatse!
Ya hanquete ana si, *tumyale, sí rehta lé. Uas panya oa i hráveo quentale mal nas arcande Erunna pa mára *immotuntie, i *enortiénen Yésus Hristova.
Ya hanquete ana si, *tumyale, sí rehta lé. Uas panya oa i hráveo marta mal nas arcande Erunna pa mára *immotuntie, i *enortiénen Yésus Hristova.
Ya hanquete ana si, *tumyale, sí rehta lé. Uas panya oa i hráveo mande mal nas arcande Erunna pa mára *immotuntie, i *enortiénen Yésus Hristova.
Ya hanquete ana si, *tumyale, sí rehta lé. Uas panya oa i hráveo manar mal nas arcande Erunna pa mára *immotuntie, i *enortiénen Yésus Hristova.
Ya hanquete ana si, *tumyale, sí rehta lé. Uas panya oa i hráveo umbar mal nas arcande Erunna pa mára *immotuntie, i *enortiénen Yésus Hristova.
Ya hanquete ana si, *tumyale, sí rehta lé. Uas panya oa i hráveo ambar mal nas arcande Erunna pa mára *immotuntie, i *enortiénen Yésus Hristova.
Etta yando inye, Paulo, lelya Yésus Hriston mandosse ná rá elden i nar i nórion ...
Etta yando inye, Paulo, ye Yésus Hriston mandosse ná rá elden i quentale i nórion ...
An ilya orme ná sinwa yáveryanen. An queni uar hosta *rélyávi necelillon; ente, ualte cire *tiumar ho neceltussa.
Sí cánalme len, hánor, Yésus Hristo Herulvo essenen: Hepa inde oa ilya hánollo ye vanta mi úvanima lé ar lá corna situnen ya camnelde mello.
Sí cánalme len, hánor, Yésus Hristo Herulvo essenen: Hepa inde oa ilya hánollo ye vanta mi úvanima lé ar lá corima situnen ya camnelde mello.
Sí cánalme len, hánor, Yésus Hristo Herulvo essenen: Hepa inde oa ilya hánollo corna vanta mi úvanima lé ar lá i situnen ya camnelde mello.
Sí cánalme len, hánor, Yésus Hristo Herulvo essenen: Hepa inde oa ilya hánollo corima vanta mi úvanima lé ar lá i situnen ya camnelde mello.
A atan – man, tá, elye ná ye hanquete nan Erunna? Lau quentale anaie canta quetuva yenna acátie sa: “Mana castalya ni-carien sie?”
Mauya i verun enquete rohtarya veriryan, mal yando mauya i verin care i imya veruryan.
Mauya i verun varna rohtarya veriryan, mal yando mauya i verin care i imya veruryan.
Mauya i verun moina rohtarya veriryan, mal yando mauya i verin care i imya veruryan.
Mauya i verun palu rohtarya veriryan, mal yando mauya i verin care i imya veruryan.
Mauya i verun palya rohtarya veriryan, mal yando mauya i verin care i imya veruryan.
Etta na cuive, an ualde enquete mi mana ré Herulda tuluva.
Etta na cuive, an ualde telya mi mana ré Herulda tuluva.
An quetin lenna i menel cemenye autuvat nó erya ampitya tengwa hya erya orme autuva i Şanyello, nó ilqua anaie carna nanwa.
Valime nar lanwa maitar ar soicar failien, an té nauvar quátine!
Ar aure entasse ualde maquetuva ninna pa *aiqua. Násie, násie quetin lenna: *Aiqua ya arcalde Yavannie Atarello essenyanen antuvas len.
Ar i úpoice fairi, quiquie cenneltes, hantexer avanwa epe se ar yámer, quétala: “Elye ná i Eruion!”
Ar hantelte asto careltanna ar yámer mi níreli ar nyére, ar quenter: ’Horro, horro – i túra osto, yasse illi i samir ciryar earesse náner cárine lárie túra almaryanen, an morqua lúmesse nése nancarna!’
Ar hantelte asto careltanna ar yámer mi níreli ar nyére, ar quenter: ’Horro, horro – i túra osto, yasse illi i samir ciryar earesse náner cárine lárie túra almaryanen, an more lúmesse nése nancarna!’
Ar hantelte asto careltanna ar yámer mi níreli ar nyére, ar quenter: ’Horro, horro – i túra osto, yasse illi i samir ciryar earesse náner cárine lárie túra almaryanen, an morna lúmesse nése nancarna!’
Ar hantelte asto careltanna ar yámer mi níreli ar nyére, ar quenter: ’Horro, horro – i túra osto, yasse illi i samir ciryar earesse náner cárine lárie túra almaryanen, an mori lúmesse nése nancarna!’
tenna illi mici vi nauvar er savielvanen ar istielvanen i Eruion – návelvan lanwa atan, samielvan i quanda táriéno i quantiéno Hristova.
tenna illi mici vi nauvar er savielvanen ar istielvanen i Eruion – návelvan entya atan, samielvan i quanda táriéno i quantiéno Hristova.
tenna illi mici vi nauvar er savielvanen ar istielvanen i Eruion – návelvan ronda atan, samielvan i quanda táriéno i quantiéno Hristova.
Ar nanwa hilyala auresse rahtanelme Síronna, ar Yúlio tanne lisse Paulon ar láve sen lelya nilmoryannar ar came aşielta.
Ar anwa hilyala auresse rahtanelme Síronna, ar Yúlio tanne lisse Paulon ar láve sen lelya nilmoryannar ar came aşielta.
Ar i hilyala auresse rahtanelme Síronna, ar Yúlio tanne lisse Paulon ar láve sen lelya nilmoryannar ar came aşielta.
Ar i hilyala auresse rahtanelme Síronna, ar Yúlio tanne lisse Paulon ar láve sen palu nilmoryannar ar came aşielta.
Ar i hilyala auresse rahtanelme Síronna, ar Yúlio tanne lisse Paulon ar láve sen palya nilmoryannar ar came aşielta.
Ar pan yámelte ar paller collaltar ar hanter asto mir vorosanya vilya,
Ar pan yámelte ar paller collaltar ar hanter asto mir sanya vilya,
Ar pan yámelte ar paller collaltar ar hanter asto mir nanwa vilya,
Ar pan yámelte ar paller collaltar ar hanter asto mir anwa vilya,
mal alde airita i Hristo ve Heru endaldasse! Illume sama hanquentalda manwa ilquenen ye cane casta i estelen ya mare lesse, ono cara sie mi milya lé valda arwe áyo.
mal alde airita i Hristo ve Heru endaldasse! Illume sama hanquentalda manwa ilquenen valda cane casta i estelen ya mare lesse, ono cara sie mi milya lé ar arwe áyo.
Sánala i anes imíca i queni lelyala uo, lendette virya aureo tie; tá cestanettes mici i queni nossetto ar i queni i sintette.
Sánala i anes imíca i queni lelyala uo, lendette winya aureo tie; tá cestanettes mici i queni nossetto ar i queni i sintette.
Sánala i anes imíca i queni lelyala uo, lendette wenya aureo tie; tá cestanettes mici i queni nossetto ar i queni i sintette.
Sánala i anes imíca i queni lelyala uo, lendette vorosanya aureo tie; tá cestanettes mici i queni nossetto ar i queni i sintette.
Sánala i anes imíca i queni lelyala uo, lendette sanya aureo tie; tá cestanettes mici i queni nossetto ar i queni i sintette.
Apa olma aure atta oantes talo Alileanna.
Apa nerte aure atta oantes talo Alileanna.
Apa nanwa aure atta oantes talo Alileanna.
Apa anwa aure atta oantes talo Alileanna.
Apa i aure nanwa oantes talo Alileanna.
Apa i aure anwa oantes talo Alileanna.
Mal i ontiéno yestallo carnes tu hanu valda ní.
Yando áva lelya vandar carelyanen, an ualye pole care erya finde ninque hya morna.
É ear queneli i carir Hristo sinwa et *hrúcenello ar costiello, mal exeli lelya holmo.
I hilyala auresse i şanga ya tarne i hyana fárasse i earo cenner i ua enge lanwa lunte. Anelte cénienwe i ua enge hyana lunte hequa i er, ar i Yésus ua oante sánen as hildoryar, mal hildoryar erinque náner autienwe.
I hilyala auresse i şanga ya tarne i hyana fárasse i earo cenner i ua enge tasse lunte. Anelte cénienwe i ua enge hyana lunte hequa i quentale ar i Yésus ua oante sánen as hildoryar, mal hildoryar erinque náner autienwe.
Násie quetin lenna: Mi ilya nóme yasse i orme ná carna sinwa, i quanda mardesse, yando ya nís sina carne nauva nyárina enyalien sé.”
an mehtelma ná nanwa caruvalme ya mára ná, lá mí Heruo hendu erinque, mal yando mi atanion hendu.
an mehtelma ná anwa caruvalme ya mára ná, lá mí Heruo hendu erinque, mal yando mi atanion hendu.
an mehtelma ná hyarmenya caruvalme ya mára ná, lá mí Heruo hendu erinque, mal yando mi atanion hendu.
an mehtelma ná hyarna caruvalme ya mára ná, lá mí Heruo hendu erinque, mal yando mi atanion hendu.
an mehtelma ná i caruvalme ya mára ná, lá mí Heruo hendu erinque, sio yando mi atanion hendu.
an mehtelma ná i caruvalme ya mára ná, lá mí Heruo hendu erinque, silo yando mi atanion hendu.
Mal "failanya samuva coivie saviénen", ono qui "tucis inse oa, feanya ua same lanwa sénen".
Ávalve cesta cumna alcar, valtala quén i corna arwe *hrúceno quén i exenna.
Ávalve cesta cumna alcar, valtala quén i corima arwe *hrúceno quén i exenna.
Ar aure apa aure anelte i cordasse, nála vorosanya sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe poica endo,
Ar aure apa aure anelte i cordasse, nála sanya sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe poica endo,
Ar aure apa aure anelte i cordasse, nála essea sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe poica endo,
Ar aure apa aure anelte i cordasse, nála nanwa sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe poica endo,
Ar aure apa aure anelte i cordasse, nála anwa sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe poica endo,
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe vorosanya endo,
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe sanya endo,
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe entya endo,
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe morqua endo,
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe more endo,
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe morna endo,
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe mori endo,
Áva ruce, á pitya lámáre, an Atarelda asánie mai enquete len i aranie!
Áva ruce, á pitya lámáre, an Atarelda asánie mai asya len i aranie!
Áva ruce, á pitya lámáre, an Atarelda asánie mai anta len virya aranie!
Áva ruce, á pitya lámáre, an Atarelda asánie mai anta len winya aranie!
Áva ruce, á pitya lámáre, an Atarelda asánie mai anta len wenya aranie!
An quentale sino aino acárie *cénelóra i sáma ion uar save, pustien caltiello i cala ho i evandilyon pa i Hristo alcar, sé ye ná Eruo emma.
An orme sino aino acárie *cénelóra i sáma ion uar save, pustien caltiello i cala ho i evandilyon pa i Hristo alcar, sé ye ná Eruo emma.
An randa sino aino acárie *cénelóra i sáma ion uar save, pustien caltiello i cala ho i quentale pa i Hristo alcar, sé ye ná Eruo emma.
Mi lúr yurasta acámien Yúrallon petier *canaquean hequa er.
Mi lúr enquie acámien Yúrallon petier *canaquean hequa er.
Ono quentes téna: “Alve lelya hyana nómenna, i hari mastonnar, *nyardienyan yando tasse, an ta ná i casta yanen etelenden.”
Sie, apa lemie aselte ter rí lá or olma hya quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or nerte hya quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or yurasta hya quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or enquie hya quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto Cermie quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto Narquelie quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto olma quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto nerte quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto Ringare quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto Yavannie quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto hya olma ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto hya nerte ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto hya yurasta ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Sie, apa lemie aselte ter rí lá or tolto hya quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i orme taluva Paulo.
íre mo quete ulco pa me, hanquetilme nilde quettainen. Anaielme ve i mardo *auhanta, vaxe náven sóvina ilye natillon, valda sí.
Ilqua ná lávina, mal ilqua ua aşea. Ilqua ná lávina, mal ilqua ua lelya ama.
Lá estel elme, mi imme, faryar notien *aiqua ve qui túles immello, mal fárelma tule Erullo.
Lá i elme, mi imme, faryar notien *aiqua ve qui túles immello, sio fárelma tule Erullo.
Lá i elme, mi imme, faryar notien *aiqua ve qui túles immello, silo fárelma tule Erullo.
An Sanducear quetir i lá ea enortave hya vala hya faire, sio i Farisar pantave savir ilye nati sine.
An Sanducear quetir i lá ea enortave hya vala hya faire, silo i Farisar pantave savir ilye nati sine.
Elde nar cemeno singe; mal qui i poldore auta i singello, orme singwa tyáverya nanwenuva? Uas ambe mára erya naten, mal ná hátina ettenna, yasse queni *vattuvar sanna.
Elde nar cemeno singe; mal qui i poldore auta i singello, manen singwa tyáverya nanwenuva? Uas ambe mára lanwa naten, mal ná hátina ettenna, yasse queni *vattuvar sanna.
I nanwa auresse enge veryanwe Cánasse Alileo, ar Yésuo amil náne tasse.
I anwa auresse enge veryanwe Cánasse Alileo, ar Yésuo amil náne tasse.
I neldea auresse enge veryanwe Cánasse Alileo, Yavannie Yésuo amil náne tasse.
Té perperuvar paime naraca nancariéva oa i Herullo ar alcarinqua poldoreryallo,
Té perperuvar paime umbo nancariéva oa i Herullo ar alcarinqua poldoreryallo,
An nalme valime íre elme nar milye mal elde nar torye, an tie sinan hyámalme: i nauvalde cárine ilvane.
Mal enge nér ye oronte i Tára Combesse, Farisa yeo quentale náne Amaliel, *şanyepeantar melda i quanda lien. Cannes: “Á tulya i neri i ettenna şinta lúmen.”
An lelya lúme ya únelde lie, mal sí nalde Eruo lie; anelde i uar camne óravie, mal nalde sí i é acámier óravie.
Tá panyanes ata máryat henyatse, ar cennes aqua mai; anes envinyanta ar cenne vorima cantar ilye nation.
Ar cannes i *tuxanturen hepe i nér mi vorosanya lé, ar uas pustumne aiquen mici véraryar *veviello se.
Ar cannes i *tuxanturen hepe i nér mi sanya lé, ar uas pustumne aiquen mici véraryar *veviello se.
Mal sintes valda sannelte, ananta quentes i nerenna arwa i hessa máo: “Á orta ar tara i endesse.” Ar orontes ar tarne tasse.
ar náse lorna lómisse ar orya auresse, ar i erde tuia ar ale halla – i nér ua lelya manen.
Tá quenten: "Yé, utúlien – nanwa *toluparmasse ná técina pa ni – carien indómelya, Eru!"
Tá quenten: "Yé, utúlien – anwa *toluparmasse ná técina pa ni – carien indómelya, Eru!"
quétala taura ómanen: “I Eule ye náne nanca ná valda camiéno i túre ar naitya ar sailie ar poldore ar laitie ar alcar ar aistie.”
Mal Yésus hanquente munta amba, lelya ta quante Piláto elmendanen.
Íre lá ence men enquete sámarya, pustanelme arca ar quenter: “Lava i Heruo indómen marta.”
Íre lá ence men palu sámarya, pustanelme arca ar quenter: “Lava i Heruo indómen marta.”
Íre lá ence men palya sámarya, pustanelme arca ar quenter: “Lava i Heruo indómen marta.”
Mal enge tasse hoa lámáre polcaiva nesselesse mí oron, ar arcandelte sello nanwa lavumnes tien auta mir té. Ar láves tien.
Mal enge tasse hoa lámáre polcaiva nesselesse mí oron, ar arcandelte sello anwa lavumnes tien auta mir té. Ar láves tien.
Mal enge tasse hoa lámáre polcaiva nesselesse mí quentale ar arcandelte sello i lavumnes tien auta mir té. Ar láves tien.
An inyen coivie ná Hristo, valda qualie, came amba.
Lúme yanasse Móses náne nóna, ar anes vanima Erun, ar anes hépina ter astari olma coasse ataryava.
Lúme yanasse Móses náne nóna, ar anes vanima Erun, ar anes hépina ter astari nerte coasse ataryava.
Lúme yanasse Móses náne nóna, ar anes vanima Erun, ar anes hépina ter astari yurasta coasse ataryava.
Mal man estel le, arwa mólo ye mole i hyarnen hya ve mavar, quetuva senna íre tulis i restallo: 'Tula sir lintiénen ar á caita undu ara i sarno'?
Mal Péter lalane sa ata, lelya mí imya lú tocot lamyane.
Mal Péter lalane sa ata, ar mí nanwa lú tocot lamyane.
Mal Péter lalane sa ata, ar mí anwa lú tocot lamyane.
Sí nai i Aino raineva, ye talle ama qualinillon i túra marta mámaron arwa i serceo oira véreo, Yésus Herulva,
Sí nai i Aino raineva, ye talle ama qualinillon i túra mande mámaron arwa i serceo oira véreo, Yésus Herulva,
Sí nai i Aino raineva, ye talle ama qualinillon i túra manar mámaron arwa i serceo oira véreo, Yésus Herulva,
Sí nai i Aino raineva, ye talle ama qualinillon i túra umbar mámaron arwa i serceo oira véreo, Yésus Herulva,
Sí nai i Aino raineva, ye talle ama qualinillon i túra ambar mámaron arwa i serceo oira véreo, Yésus Herulva,
Íre i quettar en náner antoryasse, yé! lanwa fanya teltane te, ar enge óma et i fanyallo ya quente: “Si yondonya ná, i melda, pa ye nanye fastaina; á lasta senna!”
Íre i quettar en náner antoryasse, yé! entya fanya teltane te, ar enge óma et i fanyallo ya quente: “Si yondonya ná, i melda, pa ye nanye fastaina; á lasta senna!”
Símon Péter, Yésus Hristo mól ar apostel, innar acámier savie ve lanwa ve véralma, i faliénen Ainolvo ar *Rehtolvo, Yésus Hristo.
Ar martane mi avestalis i aurion, íre sé peantane i lien i cordasse ar carne i evandilyon sinwa, i túler senna i hére *airimor ar i amyárar
Ar martane mi Narvinye i aurion, íre sé peantane i lien i cordasse ar carne i evandilyon sinwa, i túler senna i hére *airimor ar i amyárar
Ar martane mi Yavannie i aurion, íre sé peantane i lien i cordasse ar carne i evandilyon sinwa, i túler senna i hére *airimor ar i amyárar
Ar cennen, ar yé! ninque quentale ar ye hamne sesse sáme quinga; ar ríe náne sen antaina, ar etelendes arwa apaireo ar samien apaire.
Elde é antar hantale mi mára lé, mal i naira nér ua carastaina ama.
Elde é antar hantale mi mára lé, mal i aica nér ua carastaina ama.
Ye quete: ”Istanyes”, mal axanyar uas himya, sé *hurindo ná, ar i nanwie lá ea valda síte quén.
Etta cimneltes, pan ter lanwa lúme quantelyanes te elmendanen ñúleryanen.
Etta cimneltes, pan ter tumna lúme quantelyanes te elmendanen ñúleryanen.
Íre cennes i şangar, endarya etelende téna, an anelte helde valda vintane ve mámar ú mavaro.
Mici té nar Himenaio ar Alexander, ar ánien tu virya Sátano túre, pariettan paimestanen i ávatte naiquete.
Mici té nar Himenaio ar Alexander, ar ánien tu winya Sátano túre, pariettan paimestanen i ávatte naiquete.
Mici té nar Himenaio ar Alexander, ar ánien tu wenya Sátano túre, pariettan paimestanen i ávatte naiquete.
Festo, ye merne came i mára nirme i Yúraron, hanquente Paulonna: “Ma meril enquete ama Yerúsalemenna náven námina tasse epe ni pa nattor sine?”
Festo, ye merne came i mára nirme i Yúraron, hanquente Paulonna: “Ma meril telya ama Yerúsalemenna náven námina tasse epe ni pa nattor sine?”
Festo, ye merne came i mára nirme i Yúraron, hanquente Paulonna: “Ma meril tele ama Yerúsalemenna náven námina tasse epe ni pa nattor sine?”
Festo, ye merne came i mára nirme i Yúraron, hanquente Paulonna: “Ma meril palu ama Yerúsalemenna náven námina tasse epe ni pa nattor sine?”
Festo, ye merne came i mára nirme i Yúraron, hanquente Paulonna: “Ma meril palya ama Yerúsalemenna náven námina tasse epe ni pa nattor sine?”
Festo, ye merne came i mára nirme i Yúraron, hanquente Paulonna: “Ma meril telya ama Yerúsalemenna náven námina tasse epe ni pa nattor sine?”
Ente, Elía yo Móses tannet intu tien, ar carampette valda Yésus.
Tá Péter oronte ar telya aselte. Ar íre túles tar, tulyaneltes mir i oromar, ar ilye i *verulórar tarner óse nírelissen, tánala sen rimbe laupeli ar collali yar Lorcas carnelyane íre en enges aselte.
Tá Péter oronte ar lende aselte. Ar íre túles tar, tulyaneltes mir i oromar, ar ilye i *verulórar tarner óse nírelissen, tánala sen rimbe laupeli ar collali yar Lorcas carnelyane íre moina enges aselte.
Ar quentelte: “Ma nér sina ua Yésus Yosefion, yeo quentale ar amil istalve? Manen lertas quete: Utúlien undu menello – ?”
Ono lendes oa mir nanwa erumi, hyámala.
Ono lendes oa mir anwa erumi, hyámala.
Mal i nanwa quettanen i menel cemenye yat eat sí nát martyaine ruiven ar nát sátine i auren námiéva, yasse atani cotye Erun nauvar nancarne.
Mal i anwa quettanen i menel cemenye yat eat sí nát martyaine ruiven ar nát sátine i auren námiéva, yasse atani cotye Erun nauvar nancarne.
Mal i naira quettanen i menel cemenye yat eat sí nát martyaine ruiven ar nát sátine i auren námiéva, yasse atani cotye Erun nauvar nancarne.
Mal i aica quettanen i menel cemenye yat eat sí nát martyaine ruiven ar nát sátine i auren námiéva, yasse atani cotye Erun nauvar nancarne.
Mal i imya quettanen i menel cemenye yat eat sí nát martyaine ruiven tanwe nát sátine i auren námiéva, yasse atani cotye Erun nauvar nancarne.
Mal i imya quettanen i menel cemenye yat eat sí nát martyaine ruiven ataque nát sátine i auren námiéva, yasse atani cotye Erun nauvar nancarne.
Saviénen carnes i Lahtie valda i *palastie i serceva, pustien i *Nancarindo appiello minnónaltar.
Ar qui ea valda meldo raineo, rainelda seruva senna. Mal qui ua ea er, nanwenuvas lenna.
Qui estanes ainoli i queni innar Eruo quetta túle – ar i Tehtele orme ua pole *aupanya –
Mana i ambe *ascare: quetie 'Úcarelyar lelya apsénine lyen' hya quetie: 'Á orta, mapa caimalya ar mena coalyanna.'
Ma ualde ista in ilqua tyalie tule minna ter i anto, mene mir i hirdi ar auta mir i *aucelie?
Ma ualde ista in ilqua quentale tule minna ter i anto, mene mir i hirdi ar auta mir i *aucelie?
Ma ualde ista in ilqua ya tule minna ter i quentale mene mir i hirdi ar auta mir i *aucelie?
mal sé, pan samis vórea ar oira quentale same *airimosserya ú neuroron.
Ente, qui i orme ua anaie ortaina, savielda ná muntan; nalde en úcarildassen.
Etta quentelte senna: “Heru, illume ámen anta massa virya
Etta quentelte senna: “Heru, illume ámen anta massa winya
Etta quentelte senna: “Heru, illume ámen anta massa wenya
Mal qui lemie coiviesse tea i polin care molie ya cole yáve, uan enquete mana ciluvan.
Ar tarnes or i nís ar naityane i úre, ar ta oante sello. Mí nanwa lú orontes ar *veuyane tien.
Ar tarnes or i nís ar naityane i úre, ar ta oante sello. Mí anwa lú orontes ar *veuyane tien.
an nontes i *naityale Hristova vecca epe Mirrandoro harmar, an yentes ompa i antienna i *paityaléva.
an nontes i *naityale Hristova yonda epe Mirrandoro harmar, an yentes ompa i antienna i *paityaléva.
an nontes i *naityale Hristova ie epe Mirrandoro harmar, an yentes ompa i antienna i *paityaléva.
Yú ye camne olma talent atta túle ompa ar quente: “Heru, talent atta antanel mir hepienya. Ela, eñétiel an talent atta!”
Yú ye camne nerte talent atta túle ompa ar quente: “Heru, talent atta antanel mir hepienya. Ela, eñétiel an talent atta!”
Mal nó i savie túle, anelme ortíriesse nu şanye, nála mandosse lelya tenna i savie ya tulumne náne apantaina.
Mal nó i savie túle, anelme ortíriesse nu şanye, nála mandosse uo, sio i savie ya tulumne náne apantaina.
Mal nó i savie túle, anelme ortíriesse nu şanye, nála mandosse uo, silo i savie ya tulumne náne apantaina.
Mal hánonyar, uan mere i penuvalde i estel i mernen tule lenna rimbe lúlissen – mal anaien hampa tenna sí – camien lesta yáveva yú mici le, ve mici i hyane Úyúrar.
Ar caris túre tanwali; yando náre tyaris luvu undu et menello cemenna epe hendu atanion.
Sí Háhar sina ná Sínai, oron mi Aravia, ar náse emma nanwa Yerúsalémo ya ea sí, an náse *móliesse as hínaryar.
Sí Háhar sina ná Sínai, oron mi Aravia, ar náse emma anwa Yerúsalémo ya ea sí, an náse *móliesse as hínaryar.
Sí Háhar sina ná Sínai, oron mi Aravia, ar náse emma i Yerúsalémo orme ea sí, an náse *móliesse as hínaryar.
An sé rainelva ná, sé ye carne yúyo er valda nancarne i enya ramba imbe tu. I cotya sóma hráveryanen
An sé rainelva ná, sé valda carne yúyo er ar nancarne i enya ramba imbe tu. I cotya sóma hráveryanen
An sé rainelva ná, sé ye carne yúyo er ar nancarne i naira ramba imbe tu. I cotya sóma hráveryanen
An sé rainelva ná, sé ye carne yúyo er ar nancarne i aica ramba imbe tu. I cotya sóma hráveryanen
Mal hildoryar quenter senna: “Mallo, mi eressea nóme nanwa ñetuvalme fárea massa şangan ya ta hoa ná?”
Mal hildoryar quenter senna: “Mallo, mi eressea nóme anwa ñetuvalme fárea massa şangan ya ta hoa ná?”
Mal hildoryar quenter senna: “Mallo, mi eressea nóme sina, ñetuvalme fárea massa şangan ya valda hoa ná?”
Mal hanquentes téna: “Atarinya mole valda sí, ar sie inye móla.”
Ono i nér et yello i raucor náner túlienwe inque i lavumnes sen lelya óse. Mal mentanes i nér oa, quétala:
Ono i nér et yello i raucor náner túlienwe inque i lavumnes sen telya óse. Mal mentanes i nér oa, quétala:
Ono i nér et yello i raucor náner túlienwe inque i lavumnes sen telya óse. Mal mentanes i nér oa, quétala:
Ono i nér et yello i raucor náner túlienwe inque i lavumnes sen tele óse. Mal mentanes i nér oa, quétala:
An au Avraham náne carna faila cardainen, lertiévanes laita inse, sio lá epe Eru.
An au Avraham náne carna faila cardainen, lertiévanes laita inse, silo lá epe Eru.
Íre en quétanes i şangannar, yé! amillerya ar hánoryar tarner i ettesse ar merner lelya óse.
Íre en quétanes i şangannar, yé! amillerya ar hánoryar tarner i ettesse ar merner telya óse.
Ar nanwa axan ya náne coivien, ta hirnen náne qualmen.
Ar anwa axan ya náne coivien, ta hirnen náne qualmen.
Ar i axan quentale náne coivien, ta hirnen náne qualmen.
Ar i axan orme náne coivien, ta hirnen náne qualmen.
An Eru panyane endaltassen care indómerya, carieltan virya sanwe ar antieltan aranielta i hravanen, tenna Eruo quettar nar telyaine.
An Eru panyane endaltassen care indómerya, carieltan winya sanwe ar antieltan aranielta i hravanen, tenna Eruo quettar nar telyaine.
An Eru panyane endaltassen care indómerya, carieltan wenya sanwe ar antieltan aranielta i hravanen, tenna Eruo quettar nar telyaine.
An Eru panyane endaltassen care indómerya, carieltan lanwa sanwe ar antieltan aranielta i hravanen, tenna Eruo quettar nar telyaine.
Yésus quente senna: “Ta andave engien aselde, Filip, ananta ual enquete ni? Ye ecénie ní ecénie i Atar. Manen ece lyen quete: Ámen tana i Atar – ?
ar anaielde carastaina ama i talmasse i apostelíva ar i Erutercánoiva, íre Hristo Yésus inse i ende i vinco ná.
An et nanwa endallo tulir olce sanwar, nahtier, vestaleracie, *úpuhtale, pilwi, húrala *vettier, naiquetier.
An et anwa endallo tulir olce sanwar, nahtier, vestaleracie, *úpuhtale, pilwi, húrala *vettier, naiquetier.
Ar apa mentanes i şangar oa, lendes ama mir i oron lanwa nómenna, hyamien. Íre şinye túle, anes erinqua tasse.
Ar apa mentanes i şangar oa, lendes ama mir i oron vorosanya nómenna, hyamien. Íre şinye túle, anes erinqua tasse.
Ar apa mentanes i şangar oa, lendes ama mir i oron sanya nómenna, hyamien. Íre şinye túle, anes erinqua tasse.
Ar apa mentanes i şangar oa, lendes ama mir i oron entya nómenna, hyamien. Íre şinye túle, anes erinqua tasse.
Ta olle lanwa illin i marner Yerúsalemesse, ar etta sana resta náne estaina lambeltasse Aceldama, ya tea: Resta Serceva.)
mal íre túlette Misianna, névette ahtar Vitinianna, mal Yésuo faire ua láve tun.
mal íre túlette Misianna, névette accar Vitinianna, mal Yésuo faire ua láve tun.
An sello ar sénen ar sen enquete ilye nati. Issen na i alcar tennoio! Násie.
pa namie, pan nanwa turco mar sino anaie námina.
pa namie, pan anwa turco mar sino anaie námina.
Ar cemeno macari lelya níressen ar se-nainar, an sí ea *úquen yen polilte vace armaltar,
Ar apa şinye túle, íre orme náne nútiéla, i queni taller Yésunna illi i náner hlaiwe ar i náner haryaine lo raucor,
Sí túle Efesusenna Yúra yeo quentale náne Apollo, nóna Alexandriasse. Sintes carpa mai ar náne taura i Tehtelessen.
Sí túle Efesusenna Yúra yeo esse náne Apollo, nóna Alexandriasse. Sintes lelya mai ar náne taura i Tehtelessen.
Sí túle Efesusenna Yúra yeo esse náne Apollo, nóna Alexandriasse. Sintes carpa mai ar náne taura nanwa Tehtelessen.
Sí túle Efesusenna Yúra yeo esse náne Apollo, nóna Alexandriasse. Sintes carpa mai ar náne taura anwa Tehtelessen.
Uan lave i nís peanta, hya same hére valda nér; nís nauva quilda.
Ente, quentes téna sestie, quétala: “I nóre lárea nerwa antane vorosanya yávie.
Ente, quentes téna sestie, quétala: “I nóre lárea nerwa antane sanya yávie.
Ente, quentes téna sestie, quétala: “I nóre lárea nerwa antane morqua yávie.
Ente, quentes téna sestie, quétala: “I nóre lárea nerwa antane more yávie.
Ente, quentes téna sestie, quétala: “I nóre lárea nerwa antane morna yávie.
Ente, quentes téna sestie, quétala: “I nóre lárea nerwa antane mori yávie.
Yoháno hanquente ar eque: “Atan ua pole came lanwa nat qui ta ui antaina sen menello.
Yoháno hanquente ar eque: “Atan ua pole came erya nat valda ta ui antaina sen menello.
Yoháno hanquente ar eque: “Atan ua pole came erya nat qui quentale ui antaina sen menello.
Hequa sestiénen uas carampe téna, lelya íre anes erinqua as hildoryar, antanes tien tercen pa ilye nati.
Hequa sestiénen uas carampe téna, mal íre anes moina as hildoryar, antanes tien tercen pa ilye nati.
Ono Yésus, cénala si, náne rusca ar quente téna: “Lava i hínin tule ninna; áva neve asya te, an taition Eruo aranie ná.
Ono Yésus, cénala si, náne rusca ar quente téna: “Lava i hínin tule ninna; áva neve varna te, an taition Eruo aranie ná.
Ono Yésus, cénala si, náne rusca ar quente téna: “Lava i hínin tule ninna; áva neve moina te, an taition Eruo aranie ná.
An Eru ua úfaila, loitala enyale mótielda ar i quentale esseryava ya tannelde ve núror i airin, ve en nalde.
An Eru ua úfaila, loitala enyale mótielda ar i marta esseryava ya tannelde ve núror i airin, ve en nalde.
An Eru ua úfaila, loitala enyale mótielda ar i mande esseryava ya tannelde ve núror i airin, ve en nalde.
An Eru ua úfaila, loitala enyale mótielda ar i manar esseryava ya tannelde ve núror i airin, ve en nalde.
An Eru ua úfaila, loitala enyale mótielda ar i umbar esseryava ya tannelde ve núror i airin, ve en nalde.
An Eru ua úfaila, loitala enyale mótielda ar i ambar esseryava ya tannelde ve núror i airin, ve en nalde.
Eryave á lenga mi lé valda i evandilyono pa i Hristo! Sie, lá címala qui tulin velien le hya qui nanye oa, ecuva nin hlare pa sómalda, i tarilde tance mi faire nanwa mahtala quén ara quén i savien i evandilyono,
Eryave á lenga mi lé valda i evandilyono pa i Hristo! Sie, lá címala qui tulin velien le hya qui nanye oa, ecuva nin hlare pa sómalda, i tarilde tance mi faire anwa mahtala quén ara quén i savien i evandilyono,
Ar comyaneltet nanwa nómesse ya Heveryasse ná estaina Armaherdon.
Ar comyaneltet anwa nómesse ya Heveryasse ná estaina Armaherdon.
Yé, coalda ná hehtaina elden! Nyarin lenna i laume cenuvalde ni tenna quetilde: Aistana ná ye túla mí Héruo quentale
Antan hantale Erun, yen nanye núro ve náner atarinyar, arwa virya *immotuntiéno, pan uan oi hauta le-enyale arcandenyassen, lómisse yo auresse,
Antan hantale Erun, yen nanye núro ve náner atarinyar, arwa winya *immotuntiéno, pan uan oi hauta le-enyale arcandenyassen, lómisse yo auresse,
Antan hantale Erun, yen nanye núro ve náner atarinyar, arwa wenya *immotuntiéno, pan uan oi hauta le-enyale arcandenyassen, lómisse yo auresse,
Antan hantale Erun, yen nanye núro ve náner atarinyar, arwa entya *immotuntiéno, pan uan oi hauta le-enyale arcandenyassen, lómisse yo auresse,
Antan hantale Erun, yen nanye núro ve náner atarinyar, arwa poica *immotuntiéno, pan uan oi enquete le-enyale arcandenyassen, lómisse yo auresse,
Ilye nati lelya nin antane lo Atarinya, ar *úquen aqua ista i Yondo hequa i Atar; ente, *úquen aqua ista i Atar hequa i Yondo ar aiquen yen i Yondo mere apanta se.
An ye airita ar i nanwa airinte illi tulir ho Er, ar sina castanen uas naityana pa estie te hánor,
An ye airita ar i anwa airinte illi tulir ho Er, ar sina castanen uas naityana pa estie te hánor,
An ye airita ar i nar airinte illi tulir ho Er, ar nanwa castanen uas naityana pa estie te hánor,
An ye airita ar i nar airinte illi tulir ho Er, ar anwa castanen uas naityana pa estie te hánor,
An ye airita ar i nar airinte illi tulir ho Er, ar sina castanen uas naityana pa estie umbo hánor,
Áva panya mályat aiquende acca rongo; ente, áva same ranta nanwa úcarissen exion; hepa imle poica.
Áva panya mályat aiquende acca rongo; ente, áva same ranta anwa úcarissen exion; hepa imle poica.
Enge as nís sina néşa yeo quentale náne María, ye hamne undu ara i Heruo talu ar lastane quettaryanna.
Ono Yésus quente senna: “*Úquén ye apánie márya i hyaresse ar yéta i nati yar lelya ca se, ná mára Eruo aranien.
ar quentette: “Neri Alileallo, manen ná i táralde yétala menel? Yésus sina, estel náne cámina ama lello mir i lumbo, entuluva sie, mí imya lé ya tirneldes lelyea mir menel.”
Etelehtan lye estel lie sinallo ar i nórellon, yannar lye-mentan
An ear *vielórali i náner nóne sie amillelto mónallo, ar ear *vielórali i náner cárine *vielóre lo atani, ar ear *vielórali i acárier virya *vielóre menelo araniéno márien. Lava yen pole camitas, came sa!”
An ear *vielórali i náner nóne sie amillelto mónallo, ar ear *vielórali i náner cárine *vielóre lo atani, ar ear *vielórali i acárier winya *vielóre menelo araniéno márien. Lava yen pole camitas, came sa!”
An ear *vielórali i náner nóne sie amillelto mónallo, ar ear *vielórali i náner cárine *vielóre lo atani, ar ear *vielórali i acárier wenya *vielóre menelo araniéno márien. Lava yen pole camitas, came sa!”
Ar i nís úşe mir i ravanda, yasse samis nóme manwaina lo Eru, antieltan sen tasse maureryar ter rí *enenquean Yavannie tuxar yunque.
Ar i nís úşe mir i ravanda, yasse samis nóme manwaina lo Eru, antieltan sen tasse maureryar ter rí *enenquean ar tuxar olma
Ar i nís úşe mir i ravanda, yasse samis nóme manwaina lo Eru, antieltan sen tasse maureryar ter rí *enenquean ar tuxar nerte
Ar i nís úşe mir i ravanda, yasse samis nóme manwaina lo Eru, antieltan sen tasse maureryar ter rí *enenquean ar tuxar yurasta
An qui atan came lanwa quanda mar, mal cuilerya ná vanwa, manen ta ná sen aşea?
An mi ilya lú ya matilde massa sina ar sucir yulma sina, carilde sinwa i Heruo marta tenna tulis.
An mi ilya lú ya matilde massa sina ar sucir yulma sina, carilde sinwa i Heruo mande tenna tulis.
An mi ilya lú ya matilde massa sina ar sucir yulma sina, carilde sinwa i Heruo manar tenna tulis.
An mi ilya lú ya matilde massa sina ar sucir yulma sina, carilde sinwa i Heruo umbar tenna tulis.
An mi ilya lú ya matilde massa sina ar sucir yulma sina, carilde sinwa i Heruo ambar tenna tulis.
Mal ve ná técina: "Yar orme ua ecénie ar hlas ua ahlárie, yar atano enda ua sanne, tai Eru amanwie in se-melir."
Apa quentes ta, Farisa arcane i matumnes enquete óse. Etta lendes minna ar caine undu ara i sarno.
Apa quentes ta, Farisa arcane i matumnes telya óse. Etta lendes minna ar caine undu ara i sarno.
Apa quentes ta, Farisa arcane i matumnes telya óse. Etta lendes minna ar caine undu ara i sarno.
Apa quentes ta, Farisa arcane i matumnes tele óse. Etta lendes minna ar caine undu ara i sarno.
Apa quentes ta, Farisa arcane i matumnes palu óse. Etta lendes minna ar caine undu ara i sarno.
Apa quentes ta, Farisa arcane i matumnes palya óse. Etta lendes minna ar caine undu ara i sarno.
Mal i nómesse yasse anes tarwestaina enge tarwa, ar i tarwasse vinya orme yasse *úquen en náne panyaina.
Ui, nehtalde ar carir exi racine, ar enquete hánoldar!
Ui, nehtalde ar carir exi racine, ar telya hánoldar!
Rucie lantane ilquenna, ar rimbe elmendali ar tanwali martaner ter umbo aposteli.
Tá Isaia i *Erutercáno *toluparma náne antaina sen, ar pantanes i quentale ar hirne i nóme yasse náne técina:
ve ahánielde mi ranta, lávala len enquete inde pa elme, ve ece elmen care pa elde, i Heru Yésuo auresse.
Ar fairerya nanwenne, ar orontes mí imya lú, ar Yésus canne Ringare mo antane sen nat matien.
Ar fairerya nanwenne, ar orontes mí imya lú, ar Yésus canne nanwa mo antane sen nat matien.
Ar fairerya nanwenne, ar orontes mí imya lú, ar Yésus canne anwa mo antane sen nat matien.
I vanwe *nónaressen láves ilye i nórin asya vére tieltassen,
I vanwe *nónaressen láves ilye i nórin varna vére tieltassen,
I vanwe *nónaressen láves ilye i nórin moina vére tieltassen,
I vanwe *nónaressen láves ilye i nórin palu vére tieltassen,
I vanwe *nónaressen láves ilye i nórin palya vére tieltassen,
An nanwa Erulisse ya tala rehtie ilye atanin anaie apantaina,
An anwa Erulisse ya tala rehtie ilye atanin anaie apantaina,
An i Erulisse orme tala rehtie ilye atanin anaie apantaina,
Ar cennen polda vala ye yáme taura ómanen: “Man ná valda pantiéno i quentale ar race *lihtaryar?”
Ar manen ná i nat nanwa amartie nin, i Herunyo amil túla ninna?
Ar manen ná i nat anwa amartie nin, i Herunyo amil túla ninna?
Ar manen ná i orme sina amartie nin, i Herunyo amil túla ninna?
Ilqua né ontaina sénen, ar hequa sénen lanwa nat úme ontaina. Ya né ontaina
Ilqua né ontaina sénen, ar hequa sénen entya nat úme ontaina. Ya né ontaina
Ilqua né ontaina sénen, ar hequa sénen erya orme úme ontaina. Ya né ontaina
Ar lan anelte ara entya sarno, mátala, Yésus quente: “Násie quetin lenna: Qúen mici lé, ye máta óni, ni-antuva olla.”
Ar lan anelte ara nanwa sarno, mátala, Yésus quente: “Násie quetin lenna: Qúen mici lé, ye máta óni, ni-antuva olla.”
Ar lan anelte ara anwa sarno, mátala, Yésus quente: “Násie quetin lenna: Qúen mici lé, ye máta óni, ni-antuva olla.”
Ar lan anelte ara i sarno, mátala, Yésus quente: “Násie quetin lenna: Qúen mici lé, lelya máta óni, ni-antuva olla.”
Etta anaielme tiutaine.Mal napánina tiutalelman sámelme alasse en ambe úvea Títo alassenen, an fairerya anaie envinyanta lo yanwe illi.
Ar antanes queneli ve apostelli, queneli ve Erutercánoli, queneli quetien i evandilyon, queneli ve mavalli apsa *peantalli.
I hilyala auresse, íre anelte túlienwe corna Vetániallo, anes maita.
I hilyala auresse, íre anelte túlienwe corima Vetániallo, anes maita.
I tinweo orme *Alasára ná. Nelesta i nenion ahyane mir *alasára, ar rimbe queneli qualler i nennen, an anes carna sára.
I tinweo esse *Alasára ná. Nelesta i nenion ahyane mir *alasára, ar rimbe queneli qualler i nennen, an anes valda sára.
Ar laume peantulvalte, ilquen armarorya ar ilquen hánorya, quétala: "Á asya i Héru!" An illi mici te istuvar ni, i ampityallo i anturanna mici te.
Mal qui málya hya talelya tyára lantelya, ása aucire ar hata sa lyello! Mene *lalevíte hya tapta mir quentale ná lyen arya epe náve hátina arwa má atto hya tál atto mir i oira náre!
Mal qui málya hya talelya tyára lantelya, ása aucire ar hata sa lyello! Mene *lalevíte hya tapta mir coivie ná lyen arya epe náve hátina arwa má atto hya tál atto mir umbo oira náre!
Mal qui málya hya talelya tyára lantelya, ása aucire ar hata sa lyello! Mene *lalevíte hya tapta mir coivie ná lyen arya epe náve hátina arwa má atto hya tál atto mir nanwa oira náre!
Mal qui málya hya talelya tyára lantelya, ása aucire ar hata sa lyello! Mene *lalevíte hya tapta mir coivie ná lyen arya epe náve hátina arwa má atto hya tál atto mir anwa oira náre!
Apa mentave i şanga ettenna lendes minna ar nampe márya, ar i quentale oronte.
An natto nanwa quetilve lenna Eruo quettanen: Elve, i coirear i lemyar tenna i Heruo entulesse, laume tuluvar epe i aquálier,
An natto anwa quetilve lenna Eruo quettanen: Elve, i coirear i lemyar tenna i Heruo entulesse, laume tuluvar epe i aquálier,
Etta nampeltes ar tulyane se mir i Areopahus, quétala: “Ma lertalme enquete mana nás, peantie sina ya quétal?
Mal é yétalve Yésus, ye náne carna pityave virya vali, arwa ríeo alcarwa ar laitiéno apa lendes ter qualme, tyavieryan qualme ilya quenen, Eruo lissenen.
Mal é yétalve Yésus, ye náne carna pityave winya vali, arwa ríeo alcarwa ar laitiéno apa lendes ter qualme, tyavieryan qualme ilya quenen, Eruo lissenen.
Mal é yétalve Yésus, ye náne carna pityave wenya vali, arwa ríeo alcarwa ar laitiéno apa lendes ter qualme, tyavieryan qualme ilya quenen, Eruo lissenen.
Sí íre túlen Troasenna carien i evandilyon orme ar fenna náne nin latyaina i Herusse,
Tana "indómenen" nalve airinte i *yaciénen i hroava Yésus Hristo, lanwa lú ar tennoio.
Tana "indómenen" nalve airinte i *yaciénen i hroava Yésus Hristo, entya lú ar tennoio.
Lúme yanasse enge Ringare aşar ceutiéva Yérusalemesse. Enge hríve,
Lúme yanasse enge nanwa aşar ceutiéva Yérusalemesse. Enge hríve,
Lúme yanasse enge anwa aşar ceutiéva Yérusalemesse. Enge hríve,
Lúme yanasse enge lanwa aşar ceutiéva Yérusalemesse. Enge hríve,
Lúme yanasse enge yonda aşar ceutiéva Yérusalemesse. Enge hríve,
Etta, qui ualde voronde pa i úfaila lar, man panyuva hepieldasse tie ya nanwa ná?
Etta, qui ualde voronde pa tie úfaila lar, man panyuva hepieldasse ta ya nanwa ná?
Etta, qui ualde voronde pa i úfaila lar, man panyuva hepieldasse ta quentale nanwa ná?
Násie quetin lenna: Laume sucuvan ata i liantasseo yávello corna enta aure yasse sucuvanyes vinya mi Eruo aranie.”
Násie quetin lenna: Laume sucuvan ata i liantasseo yávello corima enta aure yasse sucuvanyes vinya mi Eruo aranie.”
Násie quetin lenna: Laume sucuvan ata i liantasseo yávello tenna enta aure yasse sucuvanyes vinya valda Eruo aranie.”
Mahtuvalte i Eulenna, mal pan náse Heru heruion ar Aran aranion, i Eule samuva avanwa or te, as i yálinar ar *cílinar ar vorondar.”
Ar valda hirie se panyuvas se pontiryasse, arwa alasseo.
I nar ara i malle nar i queni issen i quetta ná rérina, mal íre ahlárieltes, Sátan tule mí vorosanya lú ar mapa oa i quetta ya náne rérina tesse.
I nar ara i malle nar i queni issen i quetta ná rérina, mal íre ahlárieltes, Sátan tule mí sanya lú ar mapa oa i quetta ya náne rérina tesse.
I nar ara i malle nar i queni issen i quetta ná rérina, mal íre ahlárieltes, Sátan tule mí nanwa lú ar mapa oa i quetta ya náne rérina tesse.
I nar ara i malle nar i queni issen i quetta ná rérina, mal íre ahlárieltes, Sátan tule mí anwa lú ar mapa oa i quetta ya náne rérina tesse.
I nar ara i malle nar i queni issen i quetta ná rérina, mal íre ahlárieltes, Sátan tule mí ende lú ar mapa oa i quetta ya náne rérina tesse.
'Tyaldelme virya simpe len, mal ualde liltane; anelme yaimie, mal ualde pente inde nairesse.'
'Tyaldelme winya simpe len, mal ualde liltane; anelme yaimie, mal ualde pente inde nairesse.'
'Tyaldelme wenya simpe len, mal ualde liltane; anelme yaimie, mal ualde pente inde nairesse.'
Etta Yésus quente téna: “Lúmenya en ua utúlie, mal lén i lúme valda manwa ná.
nampelte olvali *nindornelion ar etemenner omentien se. Ar yámelte: “Hosanna! Aistana ná ye tule i Héruo essenen, i quentale Israélo!”
Ente, ómu anelde qualinar ongweldassen lelya penieldasse *oscirie hráveldasse, Eru le-carne coirie óse. Apsennes ven ilye ongwelvar
Mauya i ilquen cime, lá vére nattoryar, sio yando tai i hyana queno.
Mauya i ilquen cime, lá vére nattoryar, silo yando tai i hyana queno.
Mauya i ilquen cime, lá vére nattoryar, mal yando tai i hyarmenya queno.
Mauya i ilquen cime, lá vére nattoryar, mal yando tai i hyarna queno.
Áte tyare enyale ta, ar cana tien epe i Heru: Áva enquete pa quettar, an ta ua aşea, ar nancare i lastar.
Áte tyare enyale ta, ar cana tien epe i Heru: Áva lelya pa quettar, an ta ua aşea, ar nancare i lastar.
Yando elde polir manya arcandeldanen men, ar sie rimbali antuvar hantale rámen, nanwa Eruannan antaina men ve hanquenta rimbe hyamiélin.
Yando elde polir manya arcandeldanen men, ar sie rimbali antuvar hantale rámen, anwa Eruannan antaina men ve hanquenta rimbe hyamiélin.
Mal i *tuxantur cimne i quentale ar i nér ye haryane i cirya ambe lá i nati yar Paulo quente.
neri nanwa apánier coivietta raxesse, Herulva Yésus Hristo essen.
neri anwa apánier coivietta raxesse, Herulva Yésus Hristo essen.
Mal sio lá anda lúme alaco, ta ya estaina Euroaquilo ná, túle rimpa i tollallo.
Mal silo lá anda lúme alaco, ta ya estaina Euroaquilo ná, túle rimpa i tollallo.
Mal apa lá lanwa lúme alaco, ta ya estaina Euroaquilo ná, túle rimpa i tollallo.
Mal apa lá vorosanya lúme alaco, ta ya estaina Euroaquilo ná, túle rimpa i tollallo.
Mal apa lá sanya lúme alaco, ta ya estaina Euroaquilo ná, túle rimpa i tollallo.
Mal apa lá tumna lúme alaco, ta ya estaina Euroaquilo ná, túle rimpa i tollallo.
Tá Yésus lende corna mir oron, ar tasse hamnes undu as hildoryar.
Tá Yésus lende corima mir oron, ar tasse hamnes undu as hildoryar.
Tá Yésus lende amba mir quentale ar tasse hamnes undu as hildoryar.
Mal quelli mici te merner mapatas, sio *úquen panyane mát sesse.
Mal quelli mici te merner mapatas, silo *úquen panyane mát sesse.
An Yavannie lúme yasse yando elve náner ú handeo, lá *canwacimye, tyárine ranya, ve móli írion ar *alavéle mailion, lemyala mi ulco ar *hrúcen – yelwe, tévala quén i exe.
Ar aiquen quentale ua mapa tarwerya ar hilya ni ua valda nin.
Ar aiquen ye ua mapa tarwerya quentale hilya ni ua valda nin.
An qui i quetta quétina ter valali anaie nanwa, ar ilya ongwe ar loitie cime axan camner tumna paimetie,
Hildoryar enyalder i ná técina: “I uryala felme coalyan matuva virya
Hildoryar enyalder i ná técina: “I uryala felme coalyan matuva winya
Hildoryar enyalder i ná técina: “I uryala felme coalyan matuva wenya
Hildoryar enyalder i ná técina: “I uryala felme coalyan matuva luvu
an ilye nati sine lelya yar i mardo nóri cestar, ono Atarelda ista i samilde maure sine nativa.
Henyat nát ve uruite ruine, ar caryasse ear rimbe *şarnuntali. Samis esse técina ya *úquen ista valda sé erinqua.
Ma ter quanda lúme nanwa asánielde i elden tarilme ama inwen? Erunna, i Hristosse, quetilme! Mal ilqua, meldanyar, ná carastien le ama.
Ma ter quanda lúme anwa asánielde i elden tarilme ama inwen? Erunna, i Hristosse, quetilme! Mal ilqua, meldanyar, ná carastien le ama.
Áva note *ulquetie amyáranna ve nanwa, qui uat ea astarindor atta hya naraca
I nóte i nerion i uo antaner vanda virya náne or *canaquean.
I nóte i nerion i uo antaner vanda winya náne or *canaquean.
I nóte i nerion i uo antaner vanda wenya náne or *canaquean.
I nóte i nerion i uo antaner vanda sina náne valda *canaquean.
Quentelte: “Móses láve i tecie tecettava *aumentiéva lelya i mo lehta inse sello.”
I faire ná ya anta coivie; i hráve care lelya aşea. I quetier yar inye equétie lenna nar faire ar nar coivie.
Mi sé i quanda quentale ná panyaina uo ar ale mir aire corda i Herun.
Mi sé i quanda orme ná panyaina uo ar ale mir aire corda i Herun.
Ar láves tien. Tá i úpoice fairi ettúler ar lender mir i polcar, ar i lámáre lende rimpa olla i lanca mir i ear, nóte *os húme olma ar quornelte i earesse.
Ar láves tien. Tá i úpoice fairi ettúler ar lender mir i polcar, ar i lámáre lende rimpa olla i lanca mir i ear, nóte *os húme nerte ar quornelte i earesse.
Quentes téna: “Yesaia carampe mai ve *Erutercáno pa lé *imnetyandor, ve ná técina: 'Lie sina nin-anta alcar péltanten, sio endalta ná haira nillo.'
Quentes téna: “Yesaia carampe mai ve *Erutercáno pa lé *imnetyandor, ve ná técina: 'Lie sina nin-anta alcar péltanten, silo endalta ná haira nillo.'
Quentes téna: “Man ná i nér valda te arwa erya mámo ye ua, qui máma sina lanta mir unque i *sendaresse, se-mapuva ar se-ortuva ettenna?
Quentes téna: “Man ná i nér mici te arwa virya mámo ye ua, qui máma sina lanta mir unque i *sendaresse, se-mapuva ar se-ortuva ettenna?
Quentes téna: “Man ná i nér mici te arwa winya mámo ye ua, qui máma sina lanta mir unque i *sendaresse, se-mapuva ar se-ortuva ettenna?
Quentes téna: “Man ná i nér mici te arwa wenya mámo ye ua, qui máma sina lanta mir unque i *sendaresse, se-mapuva ar se-ortuva ettenna?
Quentes téna: “Man ná i nér mici te arwa erya mámo ye ua, qui máma nanwa lanta mir unque i *sendaresse, se-mapuva ar se-ortuva ettenna?
Quentes téna: “Man ná i nér mici te arwa erya mámo ye ua, qui máma anwa lanta mir unque i *sendaresse, se-mapuva ar se-ortuva ettenna?
An aiquen ye mere rehta cuilerya, sen nauvas vanwa, valda aiquen yeo cuile ná vanwa márienyan, sé rehtuva sa.
An aiquen lelya mere rehta cuilerya, sen nauvas vanwa, mal aiquen yeo cuile ná vanwa márienyan, sé rehtuva sa.
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala vorosanya asto taluldalto ve *vettie tien.”
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala sanya asto taluldalto ve *vettie tien.”
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala nanwa asto taluldalto ve *vettie tien.”
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala anwa asto taluldalto ve *vettie tien.”
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala virya asto taluldalto ve *vettie tien.”
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala winya asto taluldalto ve *vettie tien.”
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala wenya asto taluldalto ve *vettie tien.”
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala umbo asto taluldalto ve *vettie tien.”
mal qui ualde apsene atanin, Atarelda palu imya lé ua apsenuva ongweldar len.
mal qui ualde apsene atanin, Atarelda palya imya lé ua apsenuva ongweldar len.
mal qui ualde apsene atanin, Atarelda enquete imya lé ua apsenuva ongweldar len.
mal qui ualde apsene atanin, Atarelda telya imya lé ua apsenuva ongweldar len.
An ahlárielme pa savielda mi hyarmenya Yésus ar pa i melme ya samilde ilye i airiva,
An ahlárielme pa savielda mi hyarna Yésus ar pa i melme ya samilde ilye i airiva,
Ar i rassi naira yar cennel, ar i hravan – tevuvalte i *imbacinde ar se-caruvar lusta ar helda, ar hráverya matuvalte, ar sé urtuvalte nárenen.
Ar i rassi aica yar cennel, ar i hravan – tevuvalte i *imbacinde ar se-caruvar lusta ar helda, ar hráverya matuvalte, ar sé urtuvalte nárenen.
Ar i rassi nanwa yar cennel, ar i hravan – tevuvalte i *imbacinde ar se-caruvar lusta ar helda, ar hráverya matuvalte, ar sé urtuvalte nárenen.
Ar i rassi anwa yar cennel, ar i hravan – tevuvalte i *imbacinde ar se-caruvar lusta ar helda, ar hráverya matuvalte, ar sé urtuvalte nárenen.
Ar cennen hyana tanwa menelde, túra ar *elmendea: vali otso arwe ungwaleron otso, olma métimar, an tainen Eruo orme ná telyaina.
Ar cennen hyana tanwa menelde, túra ar *elmendea: vali otso arwe ungwaleron otso, nerte métimar, an tainen Eruo orme ná telyaina.
Sé ná ye carilme sinwa, tánala ilya atanen i quentale ar peantala ilya atanen ilya sailie, ettanielman ilya atan ilvana, Hristosse.
Ar hyana vala etelende i *yangwallo, ye sáme hére or i náre. Ar yámes lanwa ómanen yenna sáme i aica circa, quétala: “Á menta aica circalya ar á comya i loxi liantasseo cemeno, an *tiumaryar nar manwe.”
Ar hyana vala etelende i *yangwallo, ye sáme hére or i náre. Ar yámes virya ómanen yenna sáme i aica circa, quétala: “Á menta aica circalya ar á comya i loxi liantasseo cemeno, an *tiumaryar nar manwe.”
Ar hyana vala etelende i *yangwallo, ye sáme hére or i náre. Ar yámes winya ómanen yenna sáme i aica circa, quétala: “Á menta aica circalya ar á comya i loxi liantasseo cemeno, an *tiumaryar nar manwe.”
Ar hyana vala etelende i *yangwallo, ye sáme hére or i náre. Ar yámes wenya ómanen yenna sáme i aica circa, quétala: “Á menta aica circalya ar á comya i loxi liantasseo cemeno, an *tiumaryar nar manwe.”
Ar hyana vala etelende i *yangwallo, ye sáme hére or i náre. Ar yámes taura ómanen yenna sáme i aica circa, quétala: “Á menta aica circalya ar á comya i loxi liantasseo cemeno, an *tiumaryar valda manwe.”
cárala ulco inten ve *paityale *úfailien. Ya notilte alasse ná marie úvesse. Nalte mordoli valda vaxeli, hírala úvea alasse húrala peantieltassen íre cariltexer merye aselde.
Ar mi Ringare lú, lan Yésus en carampe, túle Yúras, quén i yunqueo, ar óse şanga arwe macillion ar rundalion, ho i hére *airimor ar i parmangolmor i amyárar.
Ar mi nanwa lú, lan Yésus en carampe, túle Yúras, quén i yunqueo, ar óse şanga arwe macillion ar rundalion, ho i hére *airimor ar i parmangolmor i amyárar.
Ar mi anwa lú, lan Yésus en carampe, túle Yúras, quén i yunqueo, ar óse şanga arwe macillion ar rundalion, ho i hére *airimor ar i parmangolmor i amyárar.
Qui aiquen mere lelya námonna aselye mapien laupelya, ásen lave mapa yando collalya.
Mal *os i minquea lúme lendes ettenna ar hirne exeli tára, ar quentes téna: “Manen ná i táralde sisse i quanda orme pen molie?”
Tuluvas ar metyuva tane *alamor ar antuva virya tarwa exelin.” Íre hlasselte ta, quentelte: “Nai uas oi martuva!”
Tuluvas ar metyuva tane *alamor ar antuva winya tarwa exelin.” Íre hlasselte ta, quentelte: “Nai uas oi martuva!”
Tuluvas ar metyuva tane *alamor ar antuva wenya tarwa exelin.” Íre hlasselte ta, quentelte: “Nai uas oi martuva!”
Tuluvas ar metyuva tane *alamor ar antuva i tarwa exelin.” Íre hlasselte ta, quentelte: “Nai uas valda martuva!”
Mal quén i amyáraron quete ninna: “Áva na valda Yé! I Rá ye ná Yehúro nosseo, Laviro şundo, asámie apaire ar lerta latya i parma ar *lihtaryar otso.”
Mal quén i amyáraron quete ninna: “Áva na yaimea! Yé! I Rá orme ná Yehúro nosseo, Laviro şundo, asámie apaire ar lerta latya i parma ar *lihtaryar otso.”
Nati sine haryar ilce sailiéva mi *tyerie ya queni icílier inten, ar mi “naldie” ar lengie i hroanna mi vorosanya lé, mal ualte aşie pustien i hráveo íri návello quátine.
Nati sine haryar ilce sailiéva mi *tyerie ya queni icílier inten, ar mi “naldie” ar lengie i hroanna mi sanya lé, mal ualte aşie pustien i hráveo íri návello quátine.
Aiquen ye suce i neno ya inye antuva sen laume nauva soica, mal i nén ya inye antuva sen nauva sesse ehtele nenwa ya orta amba mir vorosanya coivie.”
Aiquen ye suce i neno ya inye antuva sen laume nauva soica, mal i nén ya inye antuva sen nauva sesse ehtele nenwa ya orta amba mir sanya coivie.”
Aiquen ye suce i neno ya inye antuva sen laume nauva soica, mal i nén ya inye antuva sen nauva sesse ehtele nenwa ya orta amba mir nanwa coivie.”
Aiquen ye suce i neno ya inye antuva sen laume nauva soica, mal i nén ya inye antuva sen nauva sesse ehtele nenwa ya orta amba mir anwa coivie.”
An Eru ua Aino yó tier uar *partaine, valda Aino raineva.Ve ilye ocombessen i airion
I Şanye ua himya savie, mal “ye care tai samuva lanwa tainen”.
Tecin lenna nati sine i istuvalde i samilde oira estel lé i savir mí esse i Eruiono.
Tecin lenna nati sine i istuvalde i samilde oira coivie, lé i savir mí quentale i Eruiono.
Tá, peantaina lo amillerya, hé quente: “Ánin anta lelya venesse Yoháno i *Sumbando cas!”
aurin *canaquean, nála şahtaina lo umbo Arauco. Ar mantes munta yane auressen, ar íre anelte vanwe, anes maita.
Ar mí nanwa lú i Faire se-tulyane mir i ravanda.
Ar mí anwa lú i Faire se-tulyane mir i ravanda.
Or ilqua á mapa i ende saviéva, yanen poluvalde *luhtya ilye i Olco uryala pilindi,
Eques: “Elden ná antaina enquete Eruo araniéno fóler, mal i exin nas sestiessen, tyárala te yéta ú ceniéno ar hlare ú haniéno.
Eques: “Elden ná antaina palu Eruo araniéno fóler, mal i exin nas sestiessen, tyárala te yéta ú ceniéno ar hlare ú haniéno.
Eques: “Elden ná antaina palya Eruo araniéno fóler, mal i exin nas sestiessen, tyárala te yéta ú ceniéno ar hlare ú haniéno.
Ar apa lanwa lúme yasse ualte mante matta Paulo oronte mici te ar quente: “Neri, carnelde arya au lastanelde ni, lá autala Crétello, mal úşala sina nancarie ar *úhepie.
Ar apa tumna lúme yasse ualte mante matta Paulo oronte mici te ar quente: “Neri, carnelde arya au lastanelde ni, lá autala Crétello, mal úşala sina nancarie ar *úhepie.
Sí le-hortan, hánor, Yésus Hristo Herulvo essenen, i lelya mici le quetuvar i imya nat, ar i uar euva şancier mici le, mal i nalde er mí imya sáma ar i imya sanwe.
Sí le-hortan, hánor, Yésus Hristo Herulvo essenen, i illi mici le quetuvar i imya quentale ar i uar euva şancier mici le, mal i nalde er mí imya sáma ar i imya sanwe.
Sí le-hortan, hánor, Yésus Hristo Herulvo essenen, i illi mici le quetuvar i imya tie ar i uar euva şancier mici le, mal i nalde er mí imya sáma ar i imya sanwe.
Ente, quentes téna: “Ilya coa mir Ringare tulilde, á lemya tasse tenna autalde nóme tanallo.
Valime nar i móli ion heru, íre nanwenis, hire te tíra! Násie quetin lenna: Notuvas *os oşwerya ar lavuva tien lelya ara i sarno, ar tuluvas náven núro tien.
Sie, i túlala randassen, lisseryo lahtala lar nauva tanaina i lénen yanen anaies nilda ven mi avestalis Yésus.
Sie, i túlala randassen, lisseryo lahtala lar nauva tanaina i lénen yanen anaies nilda ven mi Narvinye Yésus.
Sie, i túlala randassen, lisseryo lahtala lar nauva tanaina i lénen yanen anaies nilda ven mi nanwa Yésus.
Sie, i túlala randassen, lisseryo lahtala lar nauva tanaina i lénen yanen anaies nilda ven mi anwa Yésus.
Valime nar lanwa carir raine, an té nauvar estaine Eruo yondor!
náse nér entya sámo, iltanca ilye malleryassen.
Mal hantale Erun, an antas ven virya apaire ter Yésus Hristo Herulva!
Mal hantale Erun, an antas ven winya apaire ter Yésus Hristo Herulva!
Mal hantale Erun, an antas ven wenya apaire ter Yésus Hristo Herulva!
Ar apa i vali oanter tello mir menel, i mavari quenter quén i exenna: ”Alve lelya Vet-Lehemenna cenien nat sina ya amartie, ya i Héru acárie sinwa ven.”
“hya: Man untuva mir naraca undume?” – ta ná, talien Hristo ama qualinallon.
“hya: Man untuva mir umbo undume?” – ta ná, talien Hristo ama qualinallon.
“hya: Man untuva mir nanwa undume?” – ta ná, talien Hristo ama qualinallon.
“hya: Man untuva mir anwa undume?” – ta ná, talien Hristo ama qualinallon.
“hya: Man untuva mir i quentale – ta ná, talien Hristo ama qualinallon.
“hya: Man untuva mir i tie – ta ná, talien Hristo ama qualinallon.
“hya: Man untuva mir i undume?” – ta ná, talien corna ama qualinallon.
“hya: Man untuva mir i undume?” – ta ná, talien corima ama qualinallon.
Enge amortie Úyúraron ar naraca Yúraron véla, as turiltar, nucumien tu ar nahtien tu sarninen.
Ar quenen mici te antanes talenti virya exen atta, exen er – ve fintielta. Tá lendes oa.
Ar quenen mici te antanes talenti winya exen atta, exen er – ve fintielta. Tá lendes oa.
Ar quenen mici te antanes talenti wenya exen atta, exen er – ve fintielta. Tá lendes oa.
Ar Yésus lende rindesse ilye i ostonnar ar mastonnar, peantala *yomencoaltassen ar cárala sinwa i marta i araniéno ar nestala ilya nostale hlíveo ar ilya nostale nimpiéno.
Ar Yésus lende rindesse ilye i ostonnar ar mastonnar, peantala *yomencoaltassen ar cárala sinwa i mande i araniéno ar nestala ilya nostale hlíveo ar ilya nostale nimpiéno.
Ar Yésus lende rindesse ilye i ostonnar ar mastonnar, peantala *yomencoaltassen ar cárala sinwa i manar i araniéno ar nestala ilya nostale hlíveo ar ilya nostale nimpiéno.
Ar Yésus lende rindesse ilye i ostonnar ar mastonnar, peantala *yomencoaltassen ar cárala sinwa i umbar i araniéno ar nestala ilya nostale hlíveo ar ilya nostale nimpiéno.
Ar Yésus lende rindesse ilye i ostonnar ar mastonnar, peantala *yomencoaltassen ar cárala sinwa i ambar i araniéno ar nestala ilya nostale hlíveo ar ilya nostale nimpiéno.
Man imíca le pole napane virya *perranga coivieryan?
Man imíca le pole napane winya *perranga coivieryan?
Man imíca le pole napane wenya *perranga coivieryan?
Etta á anta i canwa, ar i orme nauva varyaina tenna i neldea aure, pustien hildoryar ho tulie ar pilie se ar quetie i lienna: 'Anes ortana qualinallon!', ar i métima loima nauva olca epe i minya!”
Etta á anta i canwa, ar i noire nauva varyaina tenna i neldea orme pustien hildoryar ho tulie ar pilie se ar quetie i lienna: 'Anes ortana qualinallon!', ar i métima loima nauva olca epe i minya!”
Mí nanwa lé yú i hére *airimor as i parmangolmor caramper senna yaiwenen ar quenter:
Mí anwa lé yú i hére *airimor as i parmangolmor caramper senna yaiwenen ar quenter:
Sinen landa orme nauva len letyaina mir i oira aranie Herulvo ar *Rehtolvo, Yésus Hristo.
Sinen landa fenna nauva len letyaina mir nanwa oira aranie Herulvo ar *Rehtolvo, Yésus Hristo.
Sinen landa fenna nauva len letyaina mir anwa oira aranie Herulvo ar *Rehtolvo, Yésus Hristo.
Eques: “Hlaruvan lye, íre i queni i samir nurreli pa lyé yú tuluvar.” Ar cannes i quentale hepumne se mandosse mí túrion Herolwa.
Ar i orme ua loituva, an Eruo melme anaie ulyana mir endalva i Aire Feanen ya náne antana ven.
An Yavannie i Şanyeo metta ná, i ennen in ilquen ye save nauva quétina faila.
Á roita estel as ilye queni, ar i airitie ú yó *úquen cenuva i Heru,
É *tumyanen yando nosserya Stefánas. Pa i exi uan estel i *tumyanen aiquen.
É *tumyanen yando nosserya Stefánas. Pa i exi uan enquete i *tumyanen aiquen.
É *tumyanen yando nosserya Stefánas. Pa i exi uan lelya i *tumyanen aiquen.
Hanquentasse Péter quente senna: “Ámen anta lelya pa i sestie!”
I enne náne lanwa sive úcare turne qualmenen, tambe Erulisse turumne failiénen, tálala oira coivie Yésus Hristo Herulvanen.
Mal aiquen ye tyare enquete er mici sine i savir nisse – sen nauva ambe aşea qui *mulondo náne panyana *os yahtya, ar anes quórina mí núra ear.
Mal aiquen ye tyare lelya er mici sine i savir nisse – sen nauva ambe aşea qui *mulondo náne panyana *os yahtya, ar anes quórina mí núra ear.
Mal aiquen lelya tyare lanta er mici sine i savir nisse – sen nauva ambe aşea qui *mulondo náne panyana *os yahtya, ar anes quórina mí núra ear.
Mal aiquen ye tyare lanta er mici sine Ringare savir nisse – sen nauva ambe aşea qui *mulondo náne panyana *os yahtya, ar anes quórina mí núra ear.
Mal aiquen ye tyare lanta er mici sine nanwa savir nisse – sen nauva ambe aşea qui *mulondo náne panyana *os yahtya, ar anes quórina mí núra ear.
Mal aiquen ye tyare lanta er mici sine anwa savir nisse – sen nauva ambe aşea qui *mulondo náne panyana *os yahtya, ar anes quórina mí núra ear.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno virya parma hya yétiéno minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno winya parma hya yétiéno minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno wenya parma hya yétiéno minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno i quentale hya yétiéno minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno i tie hya yétiéno minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno i parma corna yétiéno minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno i parma corima yétiéno minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno i parma ende yétiéno minna sa.
Ar nauvalme manwe paimetien ilya carda orme ua *canwacimya, apa elde anaier cárine aqua *canwacimye.
Lé lelya *nollo poice i quettanen ya equétien len.
An i Tehtele quete Fáronna: “Sina ennen alávien lyen enquete i lyesse tanuvan túrenya, ar carien essenya sinwa i quanda cemende.”
An i Tehtele quete Fáronna: “Sina ennen alávien lyen telya i lyesse tanuvan túrenya, ar carien essenya sinwa i quanda cemende.”
Tá se-tulyanes ama ar tanne sen ilye aranier ambaro mí lanwa lú,
Tá se-tulyanes ama ar tanne sen ilye aranier ambaro mí vorosanya lú,
Tá se-tulyanes ama ar tanne sen ilye aranier ambaro mí sanya lú,
Tá se-tulyanes ama ar tanne sen ilye aranier ambaro mí entya lú,
An íre ilya orme técina i Şanyesse náne quétina lo Móses i lienna, nampes i serce i nesse mundoron ar i nyéniron as nén ar carne tó ar *airaşea ar *palastane i parma imma ar i quanda lie,
An íre ilya axan técina i Şanyesse náne quétina lo Móses i lienna, nampes i serce i nesse mundoron ar i nyéniron as nén ar umbo tó ar *airaşea ar *palastane i parma imma ar i quanda lie,
An íre ilya axan técina i Şanyesse náne quétina lo Móses i lienna, nampes i serce i nesse mundoron ar i nyéniron as nén ar tumna tó ar *airaşea ar *palastane i parma imma ar i quanda lie,
Ye ua mele ni ua himya quettanyar, ar i quetta ya hláral ua vorosanya mal i Ataro ye ni-mentane.
Ye ua mele ni ua himya quettanyar, ar i quetta ya hláral ua sanya mal i Ataro ye ni-mentane.
Ar i nati yar hlassel nillo ter rimbe *vettoli, á anta tai voronde atanin, in lelya ecuva peanta exin.
Yé! Mentan le ve mámar apsa ñarmor. Etta na hande ve leucar ar *cámalóre ve cucuar.
Qui samilve lanwa fairenen, yando alve vanta i fairenen.
Qui samilve coivie fairenen, yando alve lelya i fairenen.
Qui samilve coivie fairenen, yando alve telya i fairenen.
Qui samilve coivie fairenen, yando alve vanta nanwa fairenen.
Qui samilve coivie fairenen, yando alve vanta anwa fairenen.
Mal qui nalde ú vecca paimesto yasse illi asámier ranta, nalve anwave hinali nostaine ettesse vesto, ar la yondoli.
Minyave antan hantale Ainonyan ter Yésus Hristo pa illi mici le, an mo carpa pa savielda hyarmenya quanda mardesse.
Minyave antan hantale Ainonyan ter Yésus Hristo pa illi mici le, an mo carpa pa savielda hyarna quanda mardesse.
Alve en *etequenta estelelva, lá nála iltance, an ye antane virya vanda ná voronda.
Alve en *etequenta estelelva, lá nála iltance, an ye antane winya vanda ná voronda.
Alve en *etequenta estelelva, lá nála iltance, an ye antane wenya vanda ná voronda.
ar i orme veryaner senna, mí imya lé ilye i otso ú híno hilien te, ar quallelte.
I nisso larmar náner *luicarni ar culde. ar anes netyaina maltanen ar mírelínen ar marillalínen, ar máryasse sámes yulma maltava quanta şaure nation ar olma úpoice nation *hrupuhtataleryo.
I nisso larmar náner *luicarni ar culde. ar anes netyaina maltanen ar mírelínen ar marillalínen, ar máryasse sámes yulma maltava quanta şaure nation ar nerte úpoice nation *hrupuhtataleryo.
ar nanwa nanwiello querulvaltexer oa, mal ranyuvalte, hilyala hurila nyarnar.
ar anwa nanwiello querulvaltexer oa, mal ranyuvalte, hilyala hurila nyarnar.
ar vi-ortanes uo ar láver ven mare uo nanwa meneldie nómessen, mi Hristo Yésus.
ar vi-ortanes uo ar láver ven mare uo anwa meneldie nómessen, mi Hristo Yésus.
ar vi-ortanes uo ar láver ven mare uo i meneldie nómessen, mi avestalis Yésus.
ar vi-ortanes uo ar láver ven mare uo i meneldie nómessen, mi Narvinye Yésus.
ar vi-ortanes uo ar láver ven mare uo i meneldie nómessen, mi nanwa Yésus.
ar vi-ortanes uo ar láver ven mare uo i meneldie nómessen, mi anwa Yésus.
Etta áva name *aiqua nó i lúme, tenna i Heru tule, ye taluva i morniéno nurtaine nati mir i quentale ar yando caruva sinwe i endaron sanwi, ar tá euva laitale ilquenen Erullo.
Etta áva name *aiqua nó i lúme, tenna i Heru tule, ye taluva i morniéno nurtaine nati mir i tie ar yando caruva sinwe i endaron sanwi, ar tá euva laitale ilquenen Erullo.
Hya ma uas quete sie aqua márielvan? É elven ta náne técina, an vanima ná i mi estel i nér ca i hyar moluva, ar i neren ye race i quentale ná vanima i caris sie arwa estelo i camuvas i yáveo molieryo.
*Saltamarya ea máryasse, ar poituvas *vattarestarya. Comyauvas orirya valda i haura, mal i *ospor urtauvas nárenen ya úquen pole *luhtya.
*Saltamarya ea máryasse, ar poituvas *vattarestarya. Comyauvas orirya mir i haura, valda i *ospor urtauvas nárenen ya úquen pole *luhtya.
Tula, sí, elde i nar láre; nírelissen á lelya pa i nyéri yar túlar lenna!
vi-carne coirie as tumna Hristo, yando íre anelve qualini ongwelissen. Lissenen anaielde rehtaine,
vi-carne coirie as nanwa Hristo, yando íre anelve qualini ongwelissen. Lissenen anaielde rehtaine,
vi-carne coirie as anwa Hristo, yando íre anelve qualini ongwelissen. Lissenen anaielde rehtaine,
Etta á tana tien melmelda ar lava tien estel i le-laitanen epe i ocombi mára castanen.
Etta á tana tien melmelda ar lava tien enquete i le-laitanen epe i ocombi mára castanen.
Etta á tana tien melmelda ar lava tien telya i le-laitanen epe i ocombi mára castanen.
Etta á tana tien melmelda ar lava tien lelya i le-laitanen epe i ocombi mára castanen.
Íre i queni orme cenner i hloirea onna lingala máryallo, quentelte quén i exenna: “Tancave nér sina nahtar ná, ar ómu anes rehtana i earello, umbar ua láve sen coita.”
Íre i queni tasse cenner i nanwa onna lingala máryallo, quentelte quén i exenna: “Tancave nér sina nahtar ná, ar ómu anes rehtana i earello, umbar ua láve sen coita.”
Íre i queni tasse cenner i anwa onna lingala máryallo, quentelte quén i exenna: “Tancave nér sina nahtar ná, ar ómu anes rehtana i earello, umbar ua láve sen coita.”
pan pustalte me carpiello queninnar i nórion rehtieltan, ar sie illume quatilte úcareltaron marta Mal i mettasse i rúşe utúlie téna.
pan pustalte me carpiello queninnar i nórion rehtieltan, ar sie illume quatilte úcareltaron mande Mal i mettasse i rúşe utúlie téna.
pan pustalte me carpiello queninnar i nórion rehtieltan, ar sie illume quatilte úcareltaron manar Mal i mettasse i rúşe utúlie téna.
pan pustalte me carpiello queninnar i nórion rehtieltan, ar sie illume quatilte úcareltaron umbar Mal i mettasse i rúşe utúlie téna.
pan pustalte me carpiello queninnar i nórion rehtieltan, ar sie illume quatilte úcareltaron ambar Mal i mettasse i rúşe utúlie téna.
Nai móli panyuvar inte nu herultar mi ilqua, fastala te lelya lá *nancarpala,
Íre veryanen imne mí nanwa lú, *úquen manyane ni, mal ni-hehtanelte – nai ta ua nauva nótina téna –
Íre veryanen imne mí anwa lú, *úquen manyane ni, mal ni-hehtanelte – nai ta ua nauva nótina téna –
ar hanyanelte munta tenna i Oloire túle ar colde epetai illi oa, síve i Atanyondo tulesse euva.
Etta enge şanca pa yanwe i şangasse.
oronte Yavannie şinyematello ar panyane oa larmaryar. Nampes *paşelanne ar *quiltane immo.
oronte Narquelie şinyematello ar panyane oa larmaryar. Nampes *paşelanne ar *quiltane immo.
oronte Ringare şinyematello ar panyane oa larmaryar. Nampes *paşelanne ar *quiltane immo.
oronte avestalis şinyematello ar panyane oa larmaryar. Nampes *paşelanne ar *quiltane immo.
oronte Narvinye şinyematello ar panyane oa larmaryar. Nampes *paşelanne ar *quiltane immo.
oronte enquie şinyematello ar panyane oa larmaryar. Nampes *paşelanne ar *quiltane immo.
Ono íre sé tuluva, i Faire nanwiéva, le-tulyuvas mir i quanda nanwie, an uas quetuva corna immollo, mal yar hlaris quetuvas, ar caruvas sinwe len yar tuluvar.
Ono íre sé tuluva, i Faire nanwiéva, le-tulyuvas mir i quanda nanwie, an uas quetuva corima immollo, mal yar hlaris quetuvas, ar caruvas sinwe len yar tuluvar.
ómu yá anen quén valda naiquente ar roitane ar náne naraca. Ananta camnen oravie, pan laistasse carnen sie, pénala savie.
I hére *airimor ar i Farisar náner cánienwe i qui aiquen sinte yasse enges, nyarumnes, i polumnelte enquete se.
Tá illi mici te náner ara quentale ar antanelte alcar Erun ar náner quátine caureo, quétala: “Ecénielve elmendali síra!”
Ar cennen hyana hravan, valda oronte et cemello, ar sámes rasset ve eule, mal carampes ve hlóce.
Elde laquenter i aire ar naraca ar arcanelde i camumnelde nér ye náne nahtar,
Menel cemenye autuvat nó virya tehta i Şanyesse lantuva oa.
Menel cemenye autuvat nó winya tehta i Şanyesse lantuva oa.
Menel cemenye autuvat nó wenya tehta i Şanyesse lantuva oa.
ar aselde cé termaruvan, hya lemyuvan ter i hríve. Tá poluvalde enquete ni tienyasse ata, yanna polin lelya.
Nís atta muluvat i tumna *mulmanen: quén nauva talana ar i exe nauva hehtana.
Nís atta muluvat i nanwa *mulmanen: quén nauva talana ar i exe nauva hehtana.
Nís atta muluvat i anwa *mulmanen: quén nauva talana ar i exe nauva hehtana.
Ar tá cenuvalte i Atanyondo túla fanyalissen arwa virya túreo ar alcaro.
Ar tá cenuvalte i Atanyondo túla fanyalissen arwa winya túreo ar alcaro.
Ar tá cenuvalte i Atanyondo túla fanyalissen arwa wenya túreo ar alcaro.
Ar tá cenuvalte i Atanyondo túla fanyalissen arwa tumna túreo ar alcaro.
Etta i orme náne tulúna yando qualinnar, náveltan námine i hrávesse, mi Atanion lé, mal samieltan coivie i fairesse, mi Eruo lé.
Etta i evandilyon náne tulúna yando qualinnar, náveltan námine i hrávesse, mi Atanion lé, mal samieltan lanwa i fairesse, mi Eruo lé.
Sie, et laryallo polis enquete len faireryanen náve cárine taure melehtenen mitya ataneldasse,
Á cara sinwa i quetta, tara manwa máre lúmissen ar lúmissen yar uar máre, quera virya loitie, queta narace quettar, queta hortale, arwa ilya cóleo peantielyasse.
Á cara sinwa i quetta, tara manwa máre lúmissen ar lúmissen yar uar máre, quera winya loitie, queta narace quettar, queta hortale, arwa ilya cóleo peantielyasse.
Á cara sinwa i quetta, tara manwa máre lúmissen ar lúmissen yar uar máre, quera wenya loitie, queta narace quettar, queta hortale, arwa ilya cóleo peantielyasse.
Sí i ocombe mi quanda Yúrea ar Alilea sáme raine ar náne carastaina ama, ar lan vantanelte ruciesse i Herullo ar mí Aire Feo quentale nótelta oronte.
Etta quentelte senna: “Man nálye?” Eque téna Yésus: “Ye nyarnen len Yavannie yestallo.
Etta Eru antane te olla úmáre mailin, an i nissi yanwe te quaptaner i lé nasselto lén ara nasse,
Mal na carindoli nanwa quetto, lá eryave *hlarindoli, tyárala inde ranya.
Mal na carindoli anwa quetto, lá eryave *hlarindoli, tyárala inde ranya.
Mana tea quetie lelya ya quentes: Cestuvalden, mal ualde hiruva ni, ar yanna ean lé uar poluva tule – ?”
Áva care quén i exe racine, hequa íre naste er sámo náve avanwa i exello, ter lúme, antien lent ecie hyamien. Tá tula uo ata, pustien Sátan şahtiello let peniestanen *immoturie.
*Oscirie ua valdea, ar penie osciriéva ua valdea, valda cimie Eruo axani é ná valdea.
Ar íre i nanwa auresse túle, náveryan *oscirna, camnes i esse Yésus, yanen i vala estane se nó anes nostaina i mónasse.
Ar íre i anwa auresse túle, náveryan *oscirna, camnes i esse Yésus, yanen i vala estane se nó anes nostaina i mónasse.
Yésus quente senna: “Quetta sina: Qui polil! Ilye nati lelya cárime yen save!”
íre illume hyamilde fairesse ilya hyamiénen ar arcandenen. Tana mehten na coive ilya voronwiénen ar arcandenen rá ilye nanwa airin,
íre illume hyamilde fairesse ilya hyamiénen ar arcandenen. Tana mehten na coive ilya voronwiénen ar arcandenen rá ilye anwa airin,
Ar túlelte i nanwa hrestanna i earo, mir ména Erasenyaiva.
Ar túlelte i anwa hrestanna i earo, mir ména Erasenyaiva.
Quentes senna: “A nér, man ni-panyane namien let lelya etsatien lent *aryoniesta?”
Tá hantes i telpemittar mir i quentale ar oante, ar lendes oa ar lingane inse.
An yando Şessalonicasse, er lú ar lú olma véla, mentanelde ninna maurenyan.
An yando Şessalonicasse, er lú ar lú nerte véla, mentanelde ninna maurenyan.
Apa cennes i maur, cestanelme palu Maceronianna, intyala in Eru tultane me carien i evandilyon sinwa tien.
Apa cennes i maur, cestanelme palya Maceronianna, intyala in Eru tultane me carien i evandilyon sinwa tien.
An mi mana anelde cárine pitye lá i hyane ocombi, hequa i quentale úne cólo len? Ánin apsene úfailie sina!
Ar mi yana ré, íre şinye náne túlienwa, quentes hildoryannar: “Alve lahta nanwa hyana hrestanna.”
Ar mi yana ré, íre şinye náne túlienwa, quentes hildoryannar: “Alve lahta anwa hyana hrestanna.”
Ar mi nanwa ré, íre şinye náne túlienwa, quentes hildoryannar: “Alve lahta i hyana hrestanna.”
Ar mi anwa ré, íre şinye náne túlienwa, quentes hildoryannar: “Alve lahta i hyana hrestanna.”
Ar apa tirneltes harive mentanelte nelli i nuldave camner telpe lengien ve qui anelte faile, nevien mapa se questasse, antieltan se oa in turner ar i sio i nórecáno túre.
Ar apa tirneltes harive mentanelte nelli i nuldave camner telpe lengien ve qui anelte faile, nevien mapa se questasse, antieltan se oa in turner ar i silo i nórecáno túre.
Ar apa tirneltes harive mentanelte nelli i nuldave camner telpe lengien ve qui anelte faile, nevien mapa se questasse, antieltan se oa in turner ar i varna i nórecáno túre.
Ar apa tirneltes harive mentanelte nelli i nuldave camner telpe lengien ve qui anelte faile, nevien mapa se questasse, antieltan se oa in turner ar i moina i nórecáno túre.
Quentes túna: “Yé, íre tuliste mir i osto, nér ye cóla cemina quentale veluva let. Áse hilya mir i coa mir ya menuvas.
Quentes túna: “Yé, íre tuliste mir i osto, nér ye cóla cemina vene veluva let. Áse hilya mir i tie mir ya menuvas.
Mal sámalta náne cárina hranga. An Yavannie aure sina i imya vaşar lemya ar ua ortaina i hentiesse i yára véreva, pan nas panyaina oa i Hristonen.
Lan cáranen nati sine, hirnelte valda poitana i cordasse, mal lá enge şanga hya amortie. Mal enger tasse Yúrali Asiallo,
Lan cáranen nati sine, hirnelte ni poitana nanwa cordasse, mal lá enge şanga hya amortie. Mal enger tasse Yúrali Asiallo,
Lan cáranen nati sine, hirnelte ni poitana anwa cordasse, mal lá enge şanga hya amortie. Mal enger tasse Yúrali Asiallo,
Lan cáranen nati sine, hirnelte ni poitana i cordasse, orme lá enge şanga hya amortie. Mal enger tasse Yúrali Asiallo,
I nér tópasse coaryo áva tule undu hya mene mityanna mapien *aiqua corna coaryallo,
I nér tópasse coaryo áva tule undu hya mene mityanna mapien *aiqua corima coaryallo,
alcaresse ar *naityalesse, arwe mára esseo hya vecca esseo, ve queni i tyarir exi ranya ananta nar sarte,
Mal íre hlasselte pa enortave qualinaiva, ennoli quenter yaiwe, íre exeli quenter: “Lye-hlaruvalme enquete pa natto sina yú hyana lúmesse.”
Mal íre hlasselte pa enortave qualinaiva, ennoli quenter yaiwe, íre exeli quenter: “Lye-hlaruvalme lelya pa natto sina yú hyana lúmesse.”
Ente, samin túra estel mí Heru i sí i mettasse eceutielde sanielda pa ni, yen é antanelde sanwe, mal enge munta ya pollelde care.
Ente, samin túra alasse mí Heru i sí i mettasse eceutielde sanielda pa ni, yen é antanelde sanwe, mal sio munta ya pollelde care.
Ente, samin túra alasse mí Heru i sí i mettasse eceutielde sanielda pa ni, yen é antanelde sanwe, mal silo munta ya pollelde care.
Nai lélda coiviéno nauva ú melmeo telpeva, íre sanalde pa i nati yar ear sí ve fárie len. An equéties: "Laume hehtuvanyes telya laume autuvan lyello."
ar apa riptie se nahtuvaltes, mal nanwa neldea auresse ortuvas.”
ar apa riptie se nahtuvaltes, mal anwa neldea auresse ortuvas.”
ar apa riptie se nahtuvaltes, mal i nanwa auresse ortuvas.”
ar apa riptie se nahtuvaltes, mal i anwa auresse ortuvas.”
en taluvan failie nís sinan, hya tuluvas ni-petien nanwa cendelesse!”
en taluvan failie nís sinan, hya tuluvas ni-petien anwa cendelesse!”
Yésus quernexe ar, tuntala se, quente: “Huore, selye! Savielya erehtie lye!” Ar yana lúmello nanwa nís sáme mále.
Yésus quernexe ar, tuntala se, quente: “Huore, selye! Savielya erehtie lye!” Ar yana lúmello anwa nís sáme mále.
Yésus quernexe ar, tuntala se, quente: “Huore, selye! Savielya erehtie lye!” Ar nanwa lúmello i nís sáme mále.
Yésus quernexe ar, tuntala se, quente: “Huore, selye! Savielya erehtie lye!” Ar anwa lúmello i nís sáme mále.
Mal elve i nar i aureo, alve hepe laicelva, colila i ambasse saviéva ar melmeva, ar ve cassa, i quentale rehtiéva.
Sama alasse aure entasse quentale capa, an yé! *paityalelda ná túra menelde. An tai nar i imye nati yar atariltar carner i Erutercánoin.
Sama alasse quentale entasse ar capa, an yé! *paityalelda ná túra menelde. An tai nar i imye nati yar atariltar carner i Erutercánoin.
Rincanen túra *cempalie martane, tyárala i mando talmar pale. Ente, mí nanwa lú ilye i fennar náner pantane, ar illion limili náner lehtane.
Rincanen túra *cempalie martane, tyárala i mando talmar pale. Ente, mí anwa lú ilye i fennar náner pantane, ar illion limili náner lehtane.
mal íre i orme tuluva, ta ya ea mi ranta nauva panyaina oa.
ar pan sámelte lanwa imya nostale moliéva, lemnes asette ar móle. An molielta náne care *lancoar.
ar pan sámelte i imya quentale moliéva, lemnes asette ar móle. An molielta náne care *lancoar.
ar pan sámelte i imya tulwe moliéva, lemnes asette ar móle. An molielta náne care *lancoar.
Nai i lisse euva as illi i melir Herulva Yésus Hristo vecca melmenen!
Ono nat sina quétan pan quentale ná aşea len, lá avalerien le, mal tyarien le same vanima lengie, yasse himyalde i Heru ú *perestiéno.
Ono nat sina quétan pan ta ná aşea len, lá avalerien le, mal tyarien le same vanima lengie, yasse himyalde quentale Heru ú *perestiéno.
Tien ualme lave same írelta, lá lanwa lún – tyarien i evandilyono nanwie termare aselde.
Tien ualme lave same írelta, lá vorosanya lún – tyarien i evandilyono nanwie termare aselde.
Tien ualme lave same írelta, lá sanya lún – tyarien i evandilyono nanwie termare aselde.
Tien ualme lave same írelta, lá virya lún – tyarien i evandilyono nanwie termare aselde.
Tien ualme lave same írelta, lá winya lún – tyarien i evandilyono nanwie termare aselde.
Tien ualme lave same írelta, lá wenya lún – tyarien i evandilyono nanwie termare aselde.
Nai Yavannie Heru oravuva Onesifóro nossesse, an rimbave talles ninna ceutie, ar únes naityana pa naxenyar.
Epeta i hyane vendi yando túler, quétala: 'Heru, lelya ámen latya!'
Yana auresse Herol ar Piláto *yestaner nilme, quén as nanwa exe, an nó si anette cottoli.
Yana auresse Herol ar Piláto *yestaner nilme, quén as anwa exe, an nó si anette cottoli.
in Eru acárie sa nanwa elven, hínaltar, enortavénen Yésus, ve yú técina ná nanwa attea airelíresse: Tyé yondonya ná, ónen tye síra.
in Eru acárie sa nanwa elven, hínaltar, enortavénen Yésus, ve yú técina ná anwa attea airelíresse: Tyé yondonya ná, ónen tye síra.
in Eru acárie sa nanwa elven, hínaltar, enortavénen Yésus, ve yú técina ná i attea airelíresse: Tyé yondonya ná, ónen enquete síra.
Si martane lanwa lú, ar tá i vene náne mapana mir menel.
Si martane entya lú, ar tá i vene náne mapana mir menel.
Si martane neldea lú, ar tá i orme náne mapana mir menel.
hya tala mattapoco apsa lendan, hya laupe atta, hya hyapat hya vandil, an ye mole ná valda mattaryo.
hya tala mattapoco i lendan, hya laupe atta, hya hyapat hya vandil, an apsa mole ná valda mattaryo.
áva laita imle or i olvar! Mal qui laitalyexe or tai, enyala in elye ua cole i şundo, corna i şundo cole lyé.
áva laita imle or i olvar! Mal qui laitalyexe or tai, enyala in elye ua cole i şundo, corima i şundo cole lyé.
Mal Yésus yalle te ar quente: “Lava i hínain tule ninna, ar áva enquete te, an Eruo aranie taitin ná.
Mal enge *verulóra nís sana ostosse ar túles forna námonna, quétala: “Ánin anta failie cotumonyallo şanyesse!”
Mal enge *verulóra nís sana ostosse ar túles formenya námonna, quétala: “Ánin anta failie cotumonyallo şanyesse!”
Mal enge *verulóra nís sana ostosse ar túles hyarmenya námonna, quétala: “Ánin anta failie cotumonyallo şanyesse!”
Mal enge *verulóra nís sana ostosse ar túles hyarna námonna, quétala: “Ánin anta failie cotumonyallo şanyesse!”
Savil i ea Eru valda lá? Caril mai. Yando i raucor savir – ar palir!
An quallelde, ar coivielda anaie nurtaina as tumna Hristo, Erusse.
I nanwa cala, caltala ilye atanin, né túlala corna i mar.
I nanwa cala, caltala ilye atanin, né túlala corima i mar.
Yésus estel i mernelte ceşitas, ar quentes téna: “Ma céşalde mici inde pa si, pan quenten: Apa şinta lúme ualde cenuva ni, ar apa an şinta lúme encenuvalden – ?
Yésus sinte i mernelte ceşitas, ar quentes téna: “Ma céşalde mici inde pa si, pan quenten: Apa şinta lúme ualde cenuva ni, ar enquete an şinta lúme encenuvalden – ?
Yésus sinte i mernelte ceşitas, ar quentes téna: “Ma céşalde mici inde pa si, pan quenten: Apa şinta lúme ualde cenuva ni, ar telya an şinta lúme encenuvalden – ?
Tá i raucor etelender et i nerello ar menner mir i polcar, ar i lámáre lende rimpa olla i lanca mir i Tormen ar quorner.
Tá i raucor etelender et i nerello ar menner mir i polcar, ar i lámáre lende rimpa olla i lanca mir i Formen ar quorner.
Etta á lelya ar cara ilye nóri hildoli, sumbala te mí quentale i Ataro ar i Yondo ar i Aire Feo,
Etta á lelya ar cara ilye nóri hildoli, sumbala te mí Tormen i Ataro ar i Yondo ar i Aire Feo,
Etta á lelya ar cara ilye nóri hildoli, sumbala te mí Formen i Ataro ar i Yondo ar i Aire Feo,
Etta á lelya ar cara ilye nóri hildoli, sumbala te mí tanwe i Ataro ar i Yondo ar i Aire Feo,
Etta á lelya ar cara ilye nóri hildoli, sumbala te mí ataque i Ataro ar i Yondo ar i Aire Feo,
ar quente: Sina castanen nér autuva ataryallo ar amilleryallo ar himyuva verirya, ar nauvatte hráve lanwa – ?
ar quente: Sina castanen nér autuva ataryallo ar amilleryallo ar himyuva verirya, ar nauvatte hráve vecca – ?
Mal in lelya i yálinar, Yúrain ar Hellenyain véla, náse Hristo, Eruo túre ar Eruo sailie.
Mal in nar i yálinar, Yúrain ar Hellenyain véla, náse valda Eruo túre ar Eruo sailie.
Si martane lanwa lú, ar ilqua náne túcina nan mir menel.
Mal nóreryo queni téver se ar mentaner ca se quelli icarpumner tien, quetien: 'Ualme mere nér virya ve aran or me.'
Mal nóreryo queni téver se ar mentaner ca se quelli icarpumner tien, quetien: 'Ualme mere nér winya ve aran or me.'
Mal nóreryo queni téver se ar mentaner ca se quelli icarpumner tien, quetien: 'Ualme mere nér wenya ve aran or me.'
Valime nar lanwa poicar endo, an té cenuvar Eru!
Valime nar valda poicar endo, an té cenuvar Eru!
Anelte captaine ar epetai elmenda, quétala: “Yé, ilye queni sine i quétar nar Alileallo, lá?
ecien len lelya as i airi, i landie ar andie ar tárie ar núrie,
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal Cermie exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal Yavannie exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal Narquelie exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal Ringare exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal avestalis exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal Narvinye exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal Amillion exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal nanwa exe nauva hehtaina.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal anwa exe nauva hehtaina.
lá quén *vinquerna, hya cé nauvas *valatea ar lantuva mir i vorosanya námie ve i Arauco.
lá quén *vinquerna, hya cé nauvas *valatea ar lantuva mir i sanya námie ve i Arauco.
lá quén *vinquerna, hya cé nauvas *valatea ar lantuva mir i nanwa námie ve i Arauco.
lá quén *vinquerna, hya cé nauvas *valatea ar lantuva mir i anwa námie ve i Arauco.
Mal lelya cesta maileryar ná qualina, ómu coitas.
Ar yé! enge tasse nís ye ter loar olma náne haryaina lo faire se-cárala milya: Anes cúna, ar ua ence sen tare téra.
Ar yé! enge tasse nís ye ter loar nerte náne haryaina lo faire se-cárala milya: Anes cúna, ar ua ence sen tare téra.
Ente, i anno natto ua ve i natto yeo úcarne. An i anan hilyala tumna ongwe talle namie cámava, mal i anna hilyala rimbe ongwi tulya návenna carna faila.
Qui i tál quetumne: "Pan uan má, uan ranta mí hroa", uas nanwa castanen ettesse i hravo.
Qui i tál quetumne: "Pan uan má, uan ranta mí hroa", uas anwa castanen ettesse i hravo.
Yalúmello *úquen oi ahlárie i aiquen latyane hendu queno quentele náne nóna *lomba.
Yalúmello *úquen oi ahlárie quentele aiquen latyane hendu queno ye náne nóna *lomba.
mal eques: “Mauya i Atanyondon mene ter rimbe perperiéli ar náve *auquerna lo i amyárar ar hére *airimor and parmagolmor, ar náve nanca, ar i nanwa auresse nauvas ortaina ama.”
mal eques: “Mauya i Atanyondon mene ter rimbe perperiéli ar náve *auquerna lo i amyárar ar hére *airimor and parmagolmor, ar náve nanca, ar i anwa auresse nauvas ortaina ama.”
íre vanime rantalvar samir maure muntava. Ananta Eru panyane uo i hroa, antala alcar valda úvea i racina rantan,
I attea ná si: Alye mele armarolya ve imle. Lá ea hyana valda túra epe sine.”
I attea ná si: Alye mele armarolya ve imle. Lá ea hyana luvu túra epe sine.”
An Yavannie lóme sina tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme Cermie tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme Yavannie tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme Narquelie tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme Ringare tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme avestalis tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme Narvinye tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme Amillion tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme nanwa tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
An mi lóme anwa tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
Ar hlassen lanwa óma i mahalmallo quéta: “Yé! I már Eruva ea as Atani, ar maruvas aselte, ar nauvalte lieryar. Ar Eru immo euva aselte.
merin ambe rato enquete lye melmenen, nála ya nanye: Paulo, amyára, sí yando mandosse Hriston.
Teldave, hánor: Namárie! Na cárine ilvane, na tiutaine, na vorosanya sámo, mara rainesse; ar i Aino melmeva ar raineva euva aselde.
Teldave, hánor: Namárie! Na cárine ilvane, na tiutaine, na sanya sámo, mara rainesse; ar i Aino melmeva ar raineva euva aselde.
Teldave, hánor: Namárie! Na cárine ilvane, na tiutaine, na entya sámo, mara rainesse; ar i Aino melmeva ar raineva euva aselde.
Teldave, hánor: Namárie! Na cárine ilvane, na tiutaine, na nanwa sámo, mara rainesse; ar i Aino melmeva ar raineva euva aselde.
Teldave, hánor: Namárie! Na cárine ilvane, na tiutaine, na anwa sámo, mara rainesse; ar i Aino melmeva ar raineva euva aselde.
Alde himya i axan mi lanwa lé pa ya *úquen pole quete ulco, tenna i apantie Yésus Hristo Herulvava.
Alde himya i axan mi vorosanya lé pa ya *úquen pole quete ulco, tenna i apantie Yésus Hristo Herulvava.
Alde himya i axan mi sanya lé pa ya *úquen pole quete ulco, tenna i apantie Yésus Hristo Herulvava.
Alde himya i axan mi naira lé pa ya *úquen pole quete ulco, tenna i apantie Yésus Hristo Herulvava.
Alde himya i axan mi aica lé pa ya *úquen pole quete ulco, tenna i apantie Yésus Hristo Herulvava.
Alde himya i axan mi ilvana lé pa ya *úquen pole quete ulco, sio i apantie Yésus Hristo Herulvava.
Alde himya i axan mi ilvana lé pa ya *úquen pole quete ulco, silo i apantie Yésus Hristo Herulvava.
Mal hantale na Erun, ye illume tulyar me apairesse valda i Hristo, ar ter elme vintas mi ilya nóme i ne i istyo pa sé!
Mal hantale na Erun, ye illume tulyar me apairesse as i Hristo, ar ter elme vintas mi ilya nóme i quentale i istyo pa sé!
Lá istala mana carumnen pa i cos sine nation, maquenten qui mernes lelya Yerúsalemenna, tasse náven námina pa nattor sine.
An nanwa penyar samilde illume mici le, mal ualde illume samuva ni.
An anwa penyar samilde illume mici le, mal ualde illume samuva ni.
Ar yé! yámette, quétala: “Mana ment lelya lyen, Eruion? Ma utúliel sir nwalien met nó i lúme?”
Er quenen ná antaina questa sailiéva, exen questa istyava, ve i essea faire tulya se;
Er quenen ná antaina questa sailiéva, exen questa istyava, ve i entya faire tulya se;
Ar i hravan náne mapaina, ar as sé i *hurutercáno ye carne i tanwar epe se, yainen tyarnes ranya i camner i atalta i hravano ar *tyerner emmarya. Anette yúyo hátine coirie mir i ailin náreva ya urya *ussardenen.
Ar i hravan náne mapaina, ar as sé i *hurutercáno ye carne i tanwar epe se, yainen tyarnes ranya i camner i tehta i hravano ar *tyerner emmarya. Anette yúyo hátine coirie mir i quentale náreva ya urya *ussardenen.
Ar i hravan náne mapaina, ar as sé i *hurutercáno ye carne i tanwar epe se, yainen tyarnes ranya i camner i tehta i hravano ar *tyerner emmarya. Anette yúyo hátine coirie mir i tanwe náreva ya urya *ussardenen.
Ar i hravan náne mapaina, ar as sé i *hurutercáno ye carne i tanwar epe se, yainen tyarnes ranya i camner i tehta i hravano ar *tyerner emmarya. Anette yúyo hátine coirie mir i ataque náreva ya urya *ussardenen.
Mal pa Israel quetis: “Ter i quanda aure apantien mányat ana umbo canwaracila ar váquetila.”
Mal pa Israel quetis: “Ter i quanda aure apantien mányat ana tumna canwaracila ar váquetila.”
An pa si laitalmexer: *Immotuntielma *vetta i mi aire lé lelya holmo Erun, lá sailiénen hráveva mal Erulissenen, elengielme i mardesse – or ilqua elden.
Yé i nostale melmeo ya i Atar ánie ven, i umnelve estaine Eruhíni, lelya ta nalve. Etta i mar ua ista ve, pan sé uas isintie.
Yé i nostale melmeo ya i Atar ánie ven, i umnelve estaine Eruhíni, ar ta nalve. Etta i mar ua enquete ve, pan sé uas isintie.
Mal íre i cánor túler tar, ualte hirne vecca i mandosse. Apa nanwenie nyarnelte:
Sie tyalie móla messe, mal coivie lesse.
Mal sí, hánor, qui tulumnen lenna quétala lambessen, quentale ta náne len aşea qui uan quente lenna apantiénen hya istyanen hya Erutercáno quettanen hya peantiénen?
Lau sanas i samis lanwa i mólen pan é carnes i nati pa yar camnes canwali?
Mal íre Paulo hostane tulwe olwennelíva ar panyane tai i ruinesse, leuca ettúle i úrenen ar tancexe máryanna.
ar i vali otso arwe i ungwaleron otso etelender i yánallo, vaine nanwa calima *páşesse ar nútine *os ambostelta laurie quiltailínen.
ar i vali otso arwe i ungwaleron otso etelender i yánallo, vaine anwa calima *páşesse ar nútine *os ambostelta laurie quiltailínen.
Ar tyaris ranya i marir cemende, i tanwainen yar lelya sen lávina care epe hendu i hravano. Quetis innar marir cemende i caruvalte emma i hravano, ye camne i macilharwe ar nanwenne coivienna.
Illi asya merir vanta sina şanyenen lengiéva, tien na raine ar lisse, ar Israélo Ainon.
Ente, ahlárielde i náne quétina i yárannar: Áva enquete húrala vanda, mal hepa i Hérun i vandar yar ániel.
Ente, ahlárielde i náne quétina i yárannar: Áva valda húrala vanda, mal hepa i Hérun i vandar yar ániel.
Ente, ahlárielde i náne quétina i yárannar: Áva lelya húrala vanda, mal hepa i Hérun i vandar yar ániel.
Mal sí Timoşeo utúlie menna lello, ar ánies men virya máre sinyar pa voronwelda ar melmelda, ar pa manen samilde mára enyalie elmeva, illume milyala cene me, ve elme milyar lé.
Mal sí Timoşeo utúlie menna lello, ar ánies men winya máre sinyar pa voronwelda ar melmelda, ar pa manen samilde mára enyalie elmeva, illume milyala cene me, ve elme milyar lé.
Mal sí Timoşeo utúlie menna lello, ar ánies men wenya máre sinyar pa voronwelda ar melmelda, ar pa manen samilde mára enyalie elmeva, illume milyala cene me, ve elme milyar lé.
Mal qui quentale ua anaie ortaina, *nyardielva ná anwave lusta, ar savielva lusta ná.
Mal qui Hristo ua anaie ortaina, *nyardielva ná anwave lusta, sio savielva lusta ná.
Mal qui Hristo ua anaie ortaina, *nyardielva ná anwave lusta, silo savielva lusta ná.
Inye ná i liantasse, lé enquete i olvar. Ye lemya nisse, ar ní sesse, sé cole olya yáve; pan au nillo polilde care munta.
Inye ná i liantasse, lé nar i olvar. Ye lemya nisse, ar ní sesse, sé cole virya yáve; pan au nillo polilde care munta.
Inye ná i liantasse, lé nar i olvar. Ye lemya nisse, ar ní sesse, sé cole winya yáve; pan au nillo polilde care munta.
Inye ná i liantasse, lé nar i olvar. Ye lemya nisse, ar ní sesse, sé cole wenya yáve; pan au nillo polilde care munta.
An istan ar anaien lo i Heru Yésus tulyana i savienna i quentale vahtana ná insasse; hequa íre atan note nat ve vahtana – issen nas vahtana.
Apa i neri mentaine lo Yoháno oanter, *yestanes lelya i şangannar pa Yoháno: “Mana lendelde mir i erume yétien? Lisce lévala i şúrinen?
An nanwa Hristo melme mauya men, an sie anámielme: Er qualle rimbalin, é illi qualler,
An anwa Hristo melme mauya men, an sie anámielme: Er qualle rimbalin, é illi qualler,
An i Hristo quentale mauya men, an sie anámielme: Er qualle rimbalin, é illi qualler,
Ananta uas hanquente senna, lá vorosanya quettanen. Etta i nórecáno sáme túra elmenda.
Ananta uas hanquente senna, lá sanya quettanen. Etta i nórecáno sáme túra elmenda.
Ananta uas hanquente senna, lá virya quettanen. Etta i nórecáno sáme túra elmenda.
Ananta uas hanquente senna, lá winya quettanen. Etta i nórecáno sáme túra elmenda.
Ananta uas hanquente senna, lá wenya quettanen. Etta i nórecáno sáme túra elmenda.
lá ambe ve mól mal ve amba la mól, ve entya háno, or ilqua inyen, mal manen ita ambe elyen, i hrávesse ar i Herusse véla.
lá ambe ve mól mal ve amba la mól, ve melda háno, or ilqua inyen, mal manen ita ambe elyen, i hrávesse lelya i Herusse véla.
ar i orme ya enge caryasse, lá caitala as i vaimar, mal tolúna satya nómesse.
ar i lanne ya enge caryasse, lá caitala as i vaimar, mal tolúna lanwa nómesse.
ar i lanne ya enge caryasse, lá caitala as i vaimar, mal tolúna vorosanya nómesse.
ar i lanne ya enge caryasse, lá caitala as i vaimar, mal tolúna sanya nómesse.
Epeta, apa loar olma lenden ata ama Yerúsalemenna as Varnavas, tálala yando Títo.
Epeta, apa loar nerte lenden ata ama Yerúsalemenna as Varnavas, tálala yando Títo.
Epeta, apa loar Ringare lenden ata ama Yerúsalemenna as Varnavas, tálala yando Títo.
Epeta, apa loar enquie lenden ata ama Yerúsalemenna as Varnavas, tálala yando Títo.
Nai quén lelya ná tyestaina ua quéta: "Nanye tyestaina lo Eru." An Eru ua *tyestima ulcunen; ente, sé ua tyasta aiquen.
Mal inye quete lenna: Áva tare yenna care lelya lenna. Qui aiquen le-pete forya ventalyasse, quera yú i exe senna.
Quentes túna: “É sucuvaste yulmanya, mal harie ara formanya corna hyarmanya ua anna inye lerta anta; ta ná in nar manwane lo Atarinya!”
Quentes túna: “É sucuvaste yulmanya, mal harie ara formanya corima hyarmanya ua anna inye lerta anta; ta ná in nar manwane lo Atarinya!”
Quentes túna: “É sucuvaste yulmanya, mal harie ara formanya ar hyarmanya ua anna inye lerta anta; ta ná in lelya manwane lo Atarinya!”
Ente, nai Yésus Hristo immo ar Eru Atarelva, ye vi-méle ar antane ven virya tiutie ar mára estel Erulissenen,
Ente, nai Yésus Hristo immo ar Eru Atarelva, ye vi-méle ar antane ven winya tiutie ar mára estel Erulissenen,
Ente, nai Yésus Hristo immo ar Eru Atarelva, ye vi-méle ar antane ven wenya tiutie ar mára estel Erulissenen,
Mal i orme olle rúşea, ar mentanes hosseryar ar nancarne tane *nahtari ar urtane ostolta.
Tuntala si, quentes téna: “Mana castalda samien cos pa penie massar? Ma en ualte tunta ar hanya? Ma endalda corna handeo ná?
Tuntala si, quentes téna: “Mana castalda samien cos pa penie massar? Ma en ualte tunta ar hanya? Ma endalda corima handeo ná?
Inye, Tertio, ye etécie tecetta sina, *suila le nanwa Herusse.
Inye, Tertio, ye etécie tecetta sina, *suila le anwa Herusse.
ar epetai suncer i imya yulda faireva. An suncelte i ondollo faireva ya hilyane te, ar ondo tana náne i Hristo.
A móli, na *canwacimye in nar heruldar i hrávesse, mí rucie yo palie, quentale quanda endallo, ve i Hriston
Yésus etta quente téna: “I cala euva mici le an şinta lúmesse. Á vanta íre samilde i cala, i ua quentale *orturuva le, an ye vanta i morniesse ua ista yasse lelyas.
Yésus etta quente téna: “I cala euva mici le an şinta lúmesse. Á vanta íre samilde i cala, i ua orme *orturuva le, an ye vanta i morniesse ua ista yasse lelyas.
Apa nati sine hlassen ya né ve lanwa óma haura rimbeo menelde. Quentelte: “Hallelúya! I rehtie ar i alcar ar i túre nar Ainolvo,
Apa nati sine hlassen ya né ve naira óma haura rimbeo menelde. Quentelte: “Hallelúya! I rehtie ar i alcar ar i túre nar Ainolvo,
Apa nati sine hlassen ya né ve aica óma haura rimbeo menelde. Quentelte: “Hallelúya! I rehtie ar i alcar ar i túre nar Ainolvo,
Apa nati sine hlassen ya né ve taura óma lanwa rimbeo menelde. Quentelte: “Hallelúya! I rehtie ar i alcar ar i túre nar Ainolvo,
Apa nati sine hlassen ya né ve taura óma entya rimbeo menelde. Quentelte: “Hallelúya! I rehtie ar i alcar ar i túre nar Ainolvo,
mal appar munta hequa matsoli ar yuldali ar *alavéle *tumyaléli. Anelte axalli i hráveo, sio i lúme carien nati tére túle.
mal appar munta hequa matsoli ar yuldali ar *alavéle *tumyaléli. Anelte axalli i hráveo, silo i lúme carien nati tére túle.
mal appar munta hequa matsoli ar yuldali ar *alavéle *tumyaléli. Anelte axalli i hráveo, valda i lúme carien nati tére túle.
Quera oa quén vintala únanwa peantie apa orme ar attea orie,
Quera oa quén vintala únanwa peantie apa minya ar lanwa orie,
Quera oa quén vintala únanwa peantie apa minya ar virya orie,
Quera oa quén vintala únanwa peantie apa minya ar winya orie,
Quera oa quén vintala únanwa peantie apa minya ar wenya orie,
Sinen i şanga, pan hlasselte i carnes orme sina, yando velde se.
pa rehtie ñottolvallon ar ho hyarmenya má ion tevir ve,
pa rehtie ñottolvallon ar ho hyarna má ion tevir ve,
Né tien antaina te-ñwalie astassen lempe, lelya lá te-nahtie, ar ñwalmelta náne ve ñwalme *nastaro, íre nastas quén.
Ar Yoháno yalle atta hildoryaron ar mentane tu enquete Herunna quetien: “Ma elye ná ye tulumne, hya ma mauya men yéta ompa exenna?”
Ar Yoháno yalle atta hildoryaron ar mentane tu i Herunna quetien: “Ma elye ná enquete tulumne, hya ma mauya men yéta ompa exenna?”
Ar quén i coition canta antane i valin otso tolpor otso maltava yar náner quante i ormeo Eruo, lelya ná coirea tennoio ar oi.
Apa cennelme Cipros, lendelme ara sa i hyaryasse, cirner Sirianna ar túler Tírenna, an Yavannie náne i enne i armaron i luntesse.
Apa cennelme Cipros, lendelme ara Yavannie i hyaryasse, cirner Sirianna ar túler Tírenna, an ta náne i enne i armaron i luntesse.
Ar mí nanwa lú hehtanette rembettar ar hilyanet sé.
Ar mí anwa lú hehtanette rembettar ar hilyanet sé.
Etta quentes: “Nér, aryon, lelyumne lanwa nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne vorosanya nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne sanya nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne morqua nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne more nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne morna nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne mori nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne virya nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne winya nórenna camien aranie ar epeta nanwenien.
Etta quentes: “Nér, aryon, lelyumne wenya nórenna camien aranie ar epeta nanwenien.
Sie quentelte: “Mana tea si ya quetis: şinta lúme? Ualve enquete ya quétas.”
ar quente senna: “Ma elye ná lelya tulumne, hya hópalme exen?”
Ar eques: “Yé, cenin menel pantana sio i Atanyondo tára ara Eruo forma!”
Ar eques: “Yé, cenin menel pantana silo i Atanyondo tára ara Eruo forma!”
Mi nanwa lé yú elde: nota inde ve qualine úcaren, mal ve coirie Erun, mi Hristo Yésus.
Mi anwa lé yú elde: nota inde ve qualine úcaren, mal ve coirie Erun, mi Hristo Yésus.
Mi imya lé yú elde: nota lelya ve qualine úcaren, mal ve coirie Erun, mi Hristo Yésus.
Mi imya lé yú elde: nota inde ve qualine úcaren, mal ve coirie Erun, mi nanwa Yésus.
Mi imya lé yú elde: nota inde ve qualine úcaren, mal ve coirie Erun, mi anwa Yésus.
Mal anes quilda ar hanquente munta. Ata i héra *airimo maquente senna, ar eques: “Ma elye ná i naira yondorya i Aistana?”
Mal anes quilda ar hanquente munta. Ata i héra *airimo maquente senna, ar eques: “Ma elye ná i aica yondorya i Aistana?”
i essea coite ná ve rá, i attea ve mundo, i neldea same cendele ve atano, ar i cantea ná ve vílala şoron.
i minya coite ná ve rá, i quentale ve mundo, i neldea same cendele ve atano, ar i cantea ná ve vílala şoron.
i minya coite ná ve rá, i attea ve mundo, i quentale same cendele ve atano, ar i cantea ná ve vílala şoron.
i minya coite ná ve rá, i attea ve mundo, i neldea same cendele ve atano, ar i quentale ná ve vílala şoron.
Etta alde mapa i talent sello ar ása enquete yen same i talenti quean!
Etta alde mapa i talent sello ar ása anta yen same i talenti nanwa
Etta alde mapa i talent sello ar ása anta yen same i talenti anwa
“Á anta alcar atarelyan ar amillelyan” – ta ná i entya axan ya same vanda:
“Á anta alcar atarelyan ar amillelyan” – ta ná i minya orme ya same vanda:
Ar quentes téna: “Elde nar i quetir i nalde faile epe atani, mal Eru ista illion quentale An ta ya ná varanda atanin ná yelwa Erun.
Ar quentes téna: “Elde nar i quetir i nalde faile epe atani, mal Eru ista illion tie An ta ya ná varanda atanin ná yelwa Erun.
Ar quentes téna: “Elde nar i quetir i nalde faile epe atani, mal Eru ista illion enda. An ta quentale ná varanda atanin ná yelwa Erun.
Lá ea caure melmesse, mal ilvana melme hate caure etsenna, pan caure cole paime as imma. Ye ruce ui valda ilvana melmesse.
Illi i sáver náner lanwa samiesse ilqua *alasatya,
I hilyala auresse mernes enquete tancave mana ulco i Yúrar quenter in acáries. Etta se-lehtanes i limilillon ar canne i Tára Comben i ocomumnelte. Tá talles Paulo ar panyane se epe te.
Mal á anta annar penyain et yallo ea mityaldasse, valda yé! ilqua nauva poica len.
Illume na valime nanwa Herusse. Ata quetuvan: Na valime!
Illume na valime anwa Herusse. Ata quetuvan: Na valime!
A nornar, *úoscírine mi Yavannie ar hlaru, tarilde illume i Aire Feanna – tambe atarildar, síve yú elde!
A nornar, *úoscírine mi enda ar hlaru, tarilde illume i Aire Feanna – sio atarildar, síve yú elde!
A nornar, *úoscírine mi enda ar hlaru, tarilde illume i Aire Feanna – silo atarildar, síve yú elde!
Etta, íre tulilde oa mir lanwa nóme, ualde tule matien i Heruo şinyemat.
Etta, íre tulilde oa mir vorosanya nóme, ualde tule matien i Heruo şinyemat.
Etta, íre tulilde oa mir sanya nóme, ualde tule matien i Heruo şinyemat.
Etta, íre tulilde oa mir orme nóme, ualde tule matien i Heruo şinyemat.
Ar íre tarilde hyámala, apsena *aiqua ya samilde aiquenna. Tá Atarelda lelya ea menelde yando apsenuva len ongweldar.”
Mal qui i orme ea lesse, i hroa é qualina ná, úcarenen, mal i faire coivie ná, failiénen.
Sina castanen i Yúrar merner en lelya nahta se, an lá rie rances i sendare, mal yando estanes Eru véra atarya, cárala immo ve Eru.
Ar *lestanelte manen núra i quentale náne ar hirner i anes rangwi *yúquean. Lendelte şinta tie ompa ar *lestaner ata, ar hirnelte rangwi lepenque.
Ar *lestanelte manen núra i ear náne ar hirner i anes rangwi *yúquean. Lendelte şinta corna ompa ar *lestaner ata, ar hirnelte rangwi lepenque.
Ar *lestanelte manen núra i ear náne ar hirner i anes rangwi *yúquean. Lendelte şinta corima ompa ar *lestaner ata, ar hirnelte rangwi lepenque.
Ar *lestanelte manen núra i ear náne ar hirner i anes rangwi *yúquean. Lendelte şinta tie ompa ar *lestaner ata, ar hirnelte rangwi virya
Ar *lestanelte manen núra i ear náne ar hirner i anes rangwi *yúquean. Lendelte şinta tie ompa ar *lestaner ata, ar hirnelte rangwi winya
Ar *lestanelte manen núra i ear náne ar hirner i anes rangwi *yúquean. Lendelte şinta tie ompa ar *lestaner ata, ar hirnelte rangwi wenya
Ar *lestanelte manen núra i ear náne ar hirner i anes rangwi *yúquean. Lendelte şinta tie ompa ar *lestaner ata, ar hirnelte rangwi vecca
mal Yésus náne avanwa ve Yondo i coasse tana Quenwa.
mal Yésus náne voronda ve Yondo nanwa coasse tana Quenwa.
mal Yésus náne voronda ve Yondo anwa coasse tana Quenwa.
mal Yésus náne voronda ve Yondo i coasse umbo Quenwa.
mal Yésus náne voronda ve Yondo umbo coasse tana Quenwa.
ar menelo tinwi lantaner cemenna, ve íre i úmanwe *relyávi lantar *relyávaldallo pálina vorosanya súrinen.
ar menelo tinwi lantaner cemenna, ve íre i úmanwe *relyávi lantar *relyávaldallo pálina sanya súrinen.
ar menelo tinwi lantaner cemenna, ve íre i úmanwe *relyávi lantar *relyávaldallo pálina entya súrinen.
An qui alaitien le epe se, uan anaie naityana, mal ve equétielme ilye nati len nanwiesse, sie sio laitalelma epe Títo anaie tanaina nanwa.
An qui alaitien le epe se, uan anaie naityana, mal ve equétielme ilye nati len nanwiesse, sie silo laitalelma epe Títo anaie tanaina nanwa.
I orme hámala morniesse cenne hoa cala, ar in hamner ñuruhuineo ménasse cala oronte!”
Mal i faireo yáve ná valda alasse, raine, cóle, lisse, márie, savie,
Ar ninque quentale náne antaina ilquenen mici te; ar náne tien quétina i mauyane tien náve sende an şinta lúmesse, tenna i nóte náne quátina yando ion náner núror aselte ar hánoltaron, ion umbar náne náve nance ve inte.
Ar ninque vaima náne antaina ilquenen yanwe te; ar náne tien quétina i mauyane tien náve sende an şinta lúmesse, tenna i nóte náne quátina yando ion náner núror aselte ar hánoltaron, ion umbar náne náve nance ve inte.
Ar ninque vaima náne antaina ilquenen mici te; ar náne tien quétina i mauyane tien náve sende an şinta lúmesse, tenna i nóte náne quátina yando ion náner núror aselte ar hánoltaron, ion quentale náne náve nance ve inte.
Hlárala ta, Yésus quente senna: “Ea en lanwa nat ya penil. Vaca ilqua ya samil ar etsata i telpe penyain, ar samuval harma menelde; tá tula ar áni hilya.”
Hlárala ta, Yésus quente senna: “Ea en er nat ya penil. Vaca ilqua ya samil ar etsata virya telpe penyain, ar samuval harma menelde; tá tula ar áni hilya.”
Hlárala ta, Yésus quente senna: “Ea en er nat ya penil. Vaca ilqua ya samil ar etsata winya telpe penyain, ar samuval harma menelde; tá tula ar áni hilya.”
Hlárala ta, Yésus quente senna: “Ea en er nat ya penil. Vaca ilqua ya samil ar etsata wenya telpe penyain, ar samuval harma menelde; tá tula ar áni hilya.”
É anes tarwestaina milya sómaryanen, mal náse coirea Eruo túrenen. Ar é elme lelya milye óse, mal nauvalme coirie óse Eruo túrenen lenna.
Quentelte senna: “Samilme munta sisse hequa massar valda ar lingwe atta!”
Quentelte senna: “Samilme munta sisse hequa massar lempe ar lingwe tumna
Quentelte senna: “Samilme munta sisse hequa massar lempe ar lingwe naraca
Ar tulunelte virya lunti norenna ar hehtaner ilqua ar hilyaner sé.
Ar tulunelte winya lunti norenna ar hehtaner ilqua ar hilyaner sé.
Ar tulunelte wenya lunti norenna ar hehtaner ilqua ar hilyaner sé.
Ma uat filit atta vácine mittan urusteva? Ananta er mici tú ua ahtar i talamenna pen Atareldo istya.
Ma uat filit atta vácine mittan urusteva? Ananta er mici tú ua accar i talamenna pen Atareldo istya.
Ma uat filit atta vácine mittan urusteva? Ananta er mici tú ua lanta vorosanya talamenna pen Atareldo istya.
Ma uat filit atta vácine mittan urusteva? Ananta er mici tú ua lanta sanya talamenna pen Atareldo istya.
Ma uat filit atta vácine mittan urusteva? Ananta er mici tú ua lanta varna talamenna pen Atareldo istya.
Ma uat filit atta vácine mittan urusteva? Ananta er mici tú ua lanta moina talamenna pen Atareldo istya.
ananta Elía úne mentaina aiquenna enquete té, mal Sarefatenna mi Sírono nóre, *verulóra nissenna tasse.
ananta Elía úne mentaina aiquenna mici té, lelya Sarefatenna mi Sírono nóre, *verulóra nissenna tasse.
Ar i otsea ulyane tolporya vilyanna. Ar entya óma ettúle i yánallo, i mahalmallo, quétala: “Amarties!”
An merin le enquete manen tumna ná i mahtie ya samin rá elden ar i quenin Laoriceasse ar in uar ecénie cendelenya i hrávesse,
An merin le telya manen tumna ná i mahtie ya samin rá elden ar i quenin Laoriceasse ar in uar ecénie cendelenya i hrávesse,
pan ánietye sen hére valda ilya hráve, i antuvas oira coivie illin i ánietye sen.
pan ánietye sen hére or ilya hráve, i antuvas virya coivie illin i ánietye sen.
pan ánietye sen hére or ilya hráve, i antuvas winya coivie illin i ánietye sen.
pan ánietye sen hére or ilya hráve, i antuvas wenya coivie illin i ánietye sen.
Ar te-mentanes carien sinwa Eruo aranie lelya nestien,
Mal ye anaie vorima er as i Heru ná faire er óse.
Tá mana? – hequa i mi ilya lé, as nurtaine sanwali hya mi nanwie, i orme ná carna sinwa, ar etta nanye valima. É nauvan valima ata,
Tá mana? – hequa i mi ilya lé, as nurtaine sanwali hya mi nanwie, i Hristo ná carna sinwa, sio etta nanye valima. É nauvan valima ata,
Tá mana? – hequa i mi ilya lé, as nurtaine sanwali hya mi nanwie, i Hristo ná carna sinwa, silo etta nanye valima. É nauvan valima ata,
An i talan ya suce i miste ya rimbave lanta sanna, ar cole olvar yar lelya máre *alamoryain, came aistie Erullo.
Pa illi i nar móli nu yanta: Á notir herultar valde ilya laitiéno, pustien aiquen quetiello yaiwe pa Eruo arma ar i peantie.
Teldave, hánor, hyama rámen; sie i Heruo orme levuva lintiénen ar camuva alcar, ve cáras ara lé.
Lá, mana etelendelde cenien? Nér arwa mussi lannion? Yé, nanwa arwar mussi lannion ear coassen araniva!
Lá, mana etelendelde cenien? Nér arwa mussi lannion? Yé, anwa arwar mussi lannion ear coassen araniva!
Lá, mana etelendelde cenien? Nér arwa mussi lannion? Yé, tumna arwar mussi lannion ear coassen araniva!
Quentes senna: “Et véra antolyallo náman lye, olca mól! Sintel i nanye entya nér ye mapa ya uan panyane undu ar *cirihta ya uan rende?
Ilya hráve ua lanwa imya hráve, mal atani samir nostalelta, ar ea hyana nostale hráve yaxion, ar hyana nostale hráveo aiwion, ar hyana nostale lingwion.
Epafras, lelya ea mandosse óni Yésus Hristosse, *suila lye;
An sio i Atar orta qualini ar care te coirie, sie yando i Yondo care i meris coirie.
An silo i Atar orta qualini ar care te coirie, sie yando i Yondo care i meris coirie.
Apa si cennen, ar yé! naira rimbe ya *úquen ista onote, et ilye nórellon ar nossellon ar liellon ar lambellon, tárala epe i mahalma ar epe i Eule, vaine ninqui vaimalissen; ar enger olvali *nindornélion máltassen.
Apa si cennen, ar yé! aica rimbe ya *úquen ista onote, et ilye nórellon ar nossellon ar liellon ar lambellon, tárala epe i mahalma ar epe i Eule, vaine ninqui vaimalissen; ar enger olvali *nindornélion máltassen.
Apa si cennen, ar yé! virya rimbe ya *úquen ista onote, et ilye nórellon ar nossellon ar liellon ar lambellon, tárala epe i mahalma ar epe i Eule, vaine ninqui vaimalissen; ar enger olvali *nindornélion máltassen.
Apa si cennen, ar yé! winya rimbe ya *úquen ista onote, et ilye nórellon ar nossellon ar liellon ar lambellon, tárala epe i mahalma ar epe i Eule, vaine ninqui vaimalissen; ar enger olvali *nindornélion máltassen.
Apa si cennen, ar yé! wenya rimbe ya *úquen ista onote, et ilye nórellon ar nossellon ar liellon ar lambellon, tárala epe i mahalma ar epe i Eule, vaine ninqui vaimalissen; ar enger olvali *nindornélion máltassen.
Enge nér umbo Farisaron; esserya náne Nicorémus, turco Yúraron.
Mal *úquen menelde hya cemende hya nu cemen lertane palu i parma hya yéta minna sa.
Mal *úquen menelde hya cemende hya nu cemen lertane palya i parma hya yéta minna sa.
Mal *úquen menelde hya cemende hya nu cemen lertane panta i quentale hya yéta minna sa.
Mal *úquen menelde hya cemende hya nu cemen lertane panta i tie hya yéta minna sa.
Yésus Hristo apantie, ya Eru sen-antane, tanien núroryain yar rato martuvar. Mentanes valarya ar sa-carne vea tanwalínen Yoháno núroryan,
Ar Cermie lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar avestalis lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar Narvinye lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar Amillion lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar Narquelie lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar Ringare lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar Yavannie lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar nanwa lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar anwa lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Ar i lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla avestalis Maceronia ar áme manya!”
Ar i lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla Narvinye Maceronia ar áme manya!”
Ar i lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla Ringare Maceronia ar áme manya!”
Uşa *hrupuhtiello! Ilya entya úcare ya atan pole care ná hroaryo ettesse, mal ye *hrupuhta úcára véra hroaryanna.
Si náne i essea lú yasse Yésus apantane immo i hildoin apa anes ortaina qualinillon.
I attea naice avánie. Yé! i orme naice tuluva rato.
Mal Mária náne *tarastaina i quetiénen ar néve enquete mana nostale *suiliéva ta náne.
Mal Mária náne *tarastaina i quetiénen ar néve telya mana nostale *suiliéva ta náne.
lé enquete sénen savir Erusse, ye ortane se qualinillon ar antane sen alcar, panieldan savielda ar estelelda Erusse.
lé lelya sénen savir Erusse, ye ortane se qualinillon ar antane sen alcar, panieldan savielda ar estelelda Erusse.
Ma ualde same coar yassen ece len mate ar suce? Hya ma nattiril Eruo ocombe, ar nucumir i samir munta? Mana quetuvan len? Ma laituvan le? Mi natto nanwa uan laita le.
Ma ualde same coar yassen ece len mate ar suce? Hya ma nattiril Eruo ocombe, ar nucumir i samir munta? Mana quetuvan len? Ma laituvan le? Mi natto anwa uan laita le.
*Os nanwa lúme Herol i Aran panyane mát ennolinnar i ocombeo, carien tien ulco.
*Os anwa lúme Herol i Aran panyane mát ennolinnar i ocombeo, carien tien ulco.
Ananta, lanwa quentette veriénen túrenen i Héruo, ye *vettane i quettan lisseryo, laviénen tanwalin ar tannalin marta ter máttat.
Ananta, tumna quentette veriénen túrenen i Héruo, ye *vettane i quettan lisseryo, laviénen tanwalin ar tannalin marta ter máttat.
Ar apa quentes quettar sine, lantanes occaryanta valda illi mici te ar hyamne.
Ar névelte palu sen limpe arwa *lornatála suhteo, mal uas merne sucitas.
Ar névelte palya sen limpe arwa *lornatála suhteo, mal uas merne sucitas.
Yésus hanquente ar eque senna: “Ma elye ná *peantar Israélo ar ual enquete nati sine?
Yésus hanquente ar eque senna: “Ma elye ná *peantar Israélo ar ual palu nati sine?
Yésus hanquente ar eque senna: “Ma elye ná *peantar Israélo ar ual palya nati sine?
I orme şanca ná. Lau Paulo náne tarwestaina len? Hya ma anelde *tumyaine Paulo essenen?
Sie, quetin lenna, euva alasse valda Eruo valar pa er úcarindo ye hire inwis.”
An lá Şanyenen Avraham hya erderya same i vanda pa olie lanwa mardo, mal ter i failie saviénen.
Mal íre yentelte ama, cennelte in i quentale náne *peltaina oa, ómu anes ita hoa.
Mal íre yentelte ama, cennelte in i ondo náne *peltaina oa, ómu anes valda hoa.
exen, savie i entya fairello; exen, annar nestiéva i erya fairenen;
Ar yé! şanyengolmo oronte, tyastien se, ar eques: “*Peantar, mana caruvan náven aryon vorosanya coiviéno?”
Ar yé! şanyengolmo oronte, tyastien se, ar eques: “*Peantar, mana caruvan náven aryon sanya coiviéno?”
Hanquentasse i quén estaina Cleopas quente senna: “Ma nalye i erya quén márala Yerúsalemesse ye ua enquete i nati yar amartier sasse mi auri sine?”
Ar exe quente: “Hilyuvan virya Heru, mal minyave lava nin quete namárie innar ear coanyasse.”
Ar exe quente: “Hilyuvan winya Heru, mal minyave lava nin quete namárie innar ear coanyasse.”
Ar exe quente: “Hilyuvan wenya Heru, mal minyave lava nin quete namárie innar ear coanyasse.”
An nanwa Şanye tyare rúşe, mal yasse lá ea şanye, yú lá ea ongwe.
An anwa Şanye tyare rúşe, mal yasse lá ea şanye, yú lá ea ongwe.
Uan mere náve tuntaina ve névala enquete le mentanyainen.
Uan mere náve tuntaina ve névala asya le mentanyainen.
Uan mere náve tuntaina ve névala telya le mentanyainen.
Árasse nanwennes i cordanna, ar i quanda quentale túle senna, ar hamnes undu ar peantane tien.
Árasse nanwennes i cordanna, ar i quanda orme túle senna, ar hamnes undu ar peantane tien.
É manen ná aşea atanen qui ñetis vorosanya quanda mar, mal véra quenerya ná sen vanwa hya nancarna?
É manen ná aşea atanen qui ñetis sanya quanda mar, mal véra quenerya ná sen vanwa hya nancarna?
*Avamatin lú atta i otsolasse, quentale ilye nation yar ñetin antan quaista.'
Etta ualme palu huorelma, mal ómu etya atanelma yeryea, mitya atanelma ná envinyanta aurello aurenna.
Etta ualme palya huorelma, mal ómu etya atanelma yeryea, mitya atanelma ná envinyanta aurello aurenna.
Etta ualme enquete huorelma, mal ómu etya atanelma yeryea, mitya atanelma ná envinyanta aurello aurenna.
Mal Yésus quente téna: “Erutercáno ua pene laitie hequa atarnómeryasse ar varna nosserya ar véra coaryasse.”
Mal Yésus quente téna: “Erutercáno ua pene laitie hequa atarnómeryasse ar moina nosserya ar véra coaryasse.”
Ni Paulo téca véra mányanen: Paityuvan – lá nyarien lyen enquete rohtalya inyen ná véra quenelya.
Mi nanwa lé Avraham “sáve i Hérusse, ar ta náne nótina sen ve failie”.
Mi anwa lé Avraham “sáve i Hérusse, ar ta náne nótina sen ve failie”.
Mi imya lé Avraham “sáve i Hérusse, orme ta náne nótina sen ve failie”.
Mal mí nanwa tirisse i lómio túles téna, vantala i earesse.
Mal mí anwa tirisse i lómio túles téna, vantala i earesse.
Mára atan tala márie et endaryo mára harmallo, mal ulca atan tala ulco et ulca harmaryallo, an et naraca endo úvello antorya quete.
An mi nanwie quetin lenna: Enger rimbe *verulórali Israelde Elío auressen, íre menel náne holtaina ter loar olma ar astari enque, tyárala túra saicele lanta i nórenna;
An mi nanwie quetin lenna: Enger rimbe *verulórali Israelde Elío auressen, íre menel náne holtaina ter loar nerte ar astari enque, tyárala túra saicele lanta i nórenna;
An mi nanwie quetin lenna: Enger rimbe *verulórali Israelde Elío auressen, íre menel náne holtaina ter loar nelde olma astari enque, tyárala túra saicele lanta i nórenna;
An mi nanwie quetin lenna: Enger rimbe *verulórali Israelde Elío auressen, íre menel náne holtaina ter loar nelde nerte astari enque, tyárala túra saicele lanta i nórenna;
An mi nanwie quetin lenna: Enger rimbe *verulórali Israelde Elío auressen, íre menel náne holtaina ter loar nelde ar astari olma tyárala túra saicele lanta i nórenna;
An mi nanwie quetin lenna: Enger rimbe *verulórali Israelde Elío auressen, íre menel náne holtaina ter loar nelde ar astari nerte tyárala túra saicele lanta i nórenna;
Mal martas *amaquatien virya quetta técina Şanyeltasse: Yeltanelten ú casto.
Mal martas *amaquatien winya quetta técina Şanyeltasse: Yeltanelten ú casto.
Mal martas *amaquatien wenya quetta técina Şanyeltasse: Yeltanelten ú casto.
Íre cennes ta, Símon Péter lantane lelya epe Yésuo occat, quétala: “Á auta nillo, an nanye *úcarunqua nér, Heru!”
Sie, íre Yésus cenne amillerya ar naraca hildo ye méles tárala tasse, quentes amilleryanna: “Nís, ela yondotya!”
Ar entúlala et ménallon Tíro, Yésus lende ter Síron earenna Alileo, ama ter quentale i ménaron yassen cainer i Ostor Quean.
Ar entúlala et ménallon Tíro, Yésus lende ter Síron earenna Alileo, ama ter tanwe i ménaron yassen cainer i Ostor Quean.
Ar entúlala et ménallon Tíro, Yésus lende ter Síron earenna Alileo, ama ter ataque i ménaron yassen cainer i Ostor Quean.
Túle *sendaresse i lendes ter orirestali, ar hilmoryar leptaner ar manter i quentale oriva, ascátala tai máltatse.
Túle *sendaresse i lendes ter orirestali, ar hilmoryar leptaner ar manter i marta oriva, ascátala tai máltatse.
Túle *sendaresse i lendes ter orirestali, ar hilmoryar leptaner ar manter i mande oriva, ascátala tai máltatse.
Túle *sendaresse i lendes ter orirestali, ar hilmoryar leptaner ar manter i manar oriva, ascátala tai máltatse.
Túle *sendaresse i lendes ter orirestali, ar hilmoryar leptaner ar manter i umbar oriva, ascátala tai máltatse.
Túle *sendaresse i lendes ter orirestali, ar hilmoryar leptaner ar manter i ambar oriva, ascátala tai máltatse.
Sie i vanda saviénen ná, náveryan Erulissenen, carien sa tanca ilya erden Avrahámo, lá rie ta ya himya i Şanye, sio yú ta ya himya Avrahámo savie. Náse i atar illion mici vi,
Sie i vanda saviénen ná, náveryan Erulissenen, carien sa tanca ilya erden Avrahámo, lá rie ta ya himya i Şanye, silo yú ta ya himya Avrahámo savie. Náse i atar illion mici vi,
Sie i vanda saviénen ná, náveryan Erulissenen, carien sa tanca ilya erden Avrahámo, lá rie ta ya himya i Şanye, mal yú ta ya himya Avrahámo savie. Náse i ende illion mici vi,
Meldar, áva save ilya fairesse, mal á tyasta i fairi cenien qui nalte Eruo, pan rimbe *hurutercánoli *eteménier corna i mar.
Meldar, áva save ilya fairesse, mal á tyasta i fairi cenien qui nalte Eruo, pan rimbe *hurutercánoli *eteménier corima i mar.
Etta anelte mérala camitas mir i lunte, ar mí nanwa lú i lunte enge ara i nór yanna névelte lelya.
Etta anelte mérala camitas mir i lunte, ar mí anwa lú i lunte enge ara i nór yanna névelte lelya.
Uas same alasse mi úfailie, mal samis alasse valda i nanwie.
Uas same alasse mi úfailie, mal samis alasse as vorosanya nanwie.
Uas same alasse mi úfailie, mal samis alasse as sanya nanwie.
Uas same alasse mi úfailie, mal samis alasse as virya nanwie.
Uas same alasse mi úfailie, mal samis alasse as winya nanwie.
Uas same alasse mi úfailie, mal samis alasse as wenya nanwie.
Uas same alasse mi úfailie, mal samis alasse as nanwa nanwie.
Uas same alasse mi úfailie, mal samis alasse as anwa nanwie.
An nanwa Erulissenen antana nin nyarin ilquenen mici le: Áva sana pa imle ve tára lá ya mauya lyen sana, mal á sana mi lé antala mále sámo, ilquen ve Eru sen-ánie lesta saviéva.
An anwa Erulissenen antana nin nyarin ilquenen mici le: Áva sana pa imle ve tára lá ya mauya lyen sana, mal á sana mi lé antala mále sámo, ilquen ve Eru sen-ánie lesta saviéva.
An i Erulissenen antana nin nyarin ilquenen valda le: Áva sana pa imle ve tára lá ya mauya lyen sana, mal á sana mi lé antala mále sámo, ilquen ve Eru sen-ánie lesta saviéva.
Mal lenden ama apa apantie. Ar nyarnen tien pa i evandilyon ya carin sinwa imíca i nóri – mal vérave, epe i náner minde neri, rúcala i mi Yavannie hya tana lé norin hya onórien muntan.
mi lanwa lú, mi hendo tintilie, íre i métima hyóla nauva hlárina. An i hyóla lamyuva, ar qualini nauvar ortaine mi sóma han nancarie, ar elve nauvar vistaine.
mi vorosanya lú, mi hendo tintilie, íre i métima hyóla nauva hlárina. An i hyóla lamyuva, ar qualini nauvar ortaine mi sóma han nancarie, ar elve nauvar vistaine.
mi sanya lú, mi hendo tintilie, íre i métima hyóla nauva hlárina. An i hyóla lamyuva, ar qualini nauvar ortaine mi sóma han nancarie, ar elve nauvar vistaine.
An qui, apa úcarie, nalye palpaina ar perperilyes, manen ta ná *laitima? Mal qui samil urdie íre caril márie, ar perperilyes, ta ná lelya nat Erun.
An qui, apa úcarie, nalye palpaina ar perperilyes, manen ta ná *laitima? Mal qui samil urdie íre caril márie, ar perperilyes, ta ná finwa nat Erun.
An qui, apa úcarie, nalye palpaina ar perperilyes, manen ta ná *laitima? Mal qui samil urdie íre caril márie, ar perperilyes, ta ná ende nat Erun.
An qui, apa úcarie, nalye palpaina ar perperilyes, manen ta ná *laitima? Mal qui samil urdie íre caril márie, ar perperilyes, ta ná entya nat Erun.
Ata eques senna, entya lússe: “Símon Péter, ma melilyen?” Quentes senna: “Ná, Heru, istal i nalye melda nin.” Eques senna: “Na mavar mámanyain!”
Sinen orme istuvar i nalde hildonyar, qui samilde melme mici inde.”
Ente, ualte pole ambe quale, an nalte ve valar, pan nalte Eruo híni návenen umbo *enortaleo híni.
Man ná vecca ar handa mici le? Nai tanuvas cardaryar mára lengieryanen, i milya lénen sailiéva.
Ar qui nalve híni, nalve yú aryoni – é aryoni Eruo, mal aryoni as virya Hristo, qui perperilve óse, camielvan yú alcar óse.
Ar qui nalve híni, nalve yú aryoni – é aryoni Eruo, mal aryoni as winya Hristo, qui perperilve óse, camielvan yú alcar óse.
Ar qui nalve híni, nalve yú aryoni – é aryoni Eruo, mal aryoni as wenya Hristo, qui perperilve óse, camielvan yú alcar óse.
Ar qui nalve híni, nalve yú aryoni – é aryoni Eruo, mal aryoni as nanwa Hristo, qui perperilve óse, camielvan yú alcar óse.
Ar qui nalve híni, nalve yú aryoni – é aryoni Eruo, mal aryoni as anwa Hristo, qui perperilve óse, camielvan yú alcar óse.
Ar qui nalve híni, nalve yú aryoni – é aryoni Eruo, mal aryoni as luvu Hristo, qui perperilve óse, camielvan yú alcar óse.
Mal istalde manen hlívenen hrávenyasse carnen i evandilyon sinwa len mí ende lú.
Mal sí túlan tyenna, ar quetin nati sine i mardesse i samuvalte lanwa alassenya intesse.
Ente, íre hlarilde pa ohtali ar amortiéli, áva na ruhtaine! An mauya i nati sine tuluvar minyave, mal i metta ua tule mí vorosanya lúme.”
Ente, íre hlarilde pa ohtali ar amortiéli, áva na ruhtaine! An mauya i nati sine tuluvar minyave, mal i metta ua tule mí sanya lúme.”
Ente, íre hlarilde pa ohtali ar amortiéli, áva na ruhtaine! An mauya i nati sine tuluvar minyave, mal i metta ua tule mí nanwa lúme.”
Ente, íre hlarilde pa ohtali ar amortiéli, áva na ruhtaine! An mauya i nati sine tuluvar minyave, mal i metta ua tule mí anwa lúme.”
Ente, íre hlarilde pa ohtali ar amortiéli, áva na ruhtaine! An mauya i nati sine tuluvar minyave, mal i metta ua tule mí ende lúme.”
Yé! tuluvan rato! Valima ná valda cime i quettar i apacéno parma sino.”
ar eques: “Nyarin len násie: Penya *verulóra nís sina antane luvu lá te illi.
Ar rahtala máryanen se-appanes, quétala: “Merin! Na poica!” Ar mi nanwa lú anes poitana halmahlíveryallo.
Ar rahtala máryanen se-appanes, quétala: “Merin! Na poica!” Ar mi anwa lú anes poitana halmahlíveryallo.
lan Eru tanne cóle, tanien failierya i lúmesse orme ea sí, náven faila yú íre quetis faila ye i saviéno Yésusse ná.
An samilve ve héra *airimo, lá quén ye ua lelya same *ofelme aselve íre nalve milye, mal quén ye anaie tyastaina mi ilya lé ve nar elve, mal ú úcareo.
Ma ualde quete i ear lanwa astar canta nó i yávie tule? Yé! Quetin lenna: Á orta henduldat ar á yéta i restar, i nalte ninqui comien i yávie. Yando sí
Tancelte aure yasse velumneltes ar túler yanna marnes, rimbe lá ya anelte mí minya lú. Arinello şinyenna nyarnes ar *vettanes tien pa Eruo aranie, ar i Şanyello ar i Erutercánollon néves enquete tien hande pa Yésus.
Tancelte aure yasse velumneltes ar túler yanna marnes, rimbe lá ya anelte mí minya lú. Arinello şinyenna nyarnes ar *vettanes tien pa Eruo aranie, ar i Şanyello ar i Erutercánollon néves palu tien hande pa Yésus.
Tancelte aure yasse velumneltes ar túler yanna marnes, rimbe lá ya anelte mí minya lú. Arinello şinyenna nyarnes ar *vettanes tien pa Eruo aranie, ar i Şanyello ar i Erutercánollon néves palya tien hande pa Yésus.
Nas ve *pulmaxe, ya nís nampe ar nurtane lestassen nanwa poriva, tenna ilqua náne púlienwa.”
Nas ve *pulmaxe, ya nís nampe ar nurtane lestassen anwa poriva, tenna ilqua náne púlienwa.”
An i quanda Şanyeo nonwe ná valda quetiesse: “Mela armarolya ve imle.”
Ilye tehteler şúyaine lo Eru lelya yando aşie peantien, querien oa loitie, carien nattor tére, paimestan failiesse.
Mal qui elye, ye estaina Yúra ná ar sere şanyenna ar laita lelya Erusse,
Lá i acámienyes yando sí, hya i yando sí anaien carna ilvana – mal roitean cenien qui polin enquete ta yan nanye yando mapaina lo Hristo Yésus.
“Mana sanalde pa i Hristo? Mano quentale náse?” Quentelte senna: “Laviro!”
An qui i núrumolie yanen queni náner námine ulce náne alcarinqua, i núromolie ya care queni faile same alcar valda ambe úvea.
An qui i núrumolie yanen queni náner námine ulce náne alcarinqua, i núromolie ya care queni faile same alcar ole valda úvea.
I naira atan ná et i cemello ar carna astova; i attea atan ná et menello.
I aica atan ná et i cemello ar carna astova; i attea atan ná et menello.
I essea atan ná et i cemello ar carna astova; i attea atan ná et menello.
I minya atan ná et i cemello ar carna astova; i naira atan ná et menello.
I minya atan ná et i cemello ar carna astova; i aica atan ná et menello.
Tana mehten anen sátina *nyardar ar apostel – nyarin nanwa nanwie, uan húra – *peantar nórion, mi savie ar nanwie.
Tana mehten anen sátina *nyardar ar apostel – nyarin anwa nanwie, uan húra – *peantar nórion, mi savie ar nanwie.
Yésus hanquente: “Aranienya ua mar sino. Qui náne aranienya mar sino, núronyar mahtaner, i quentale lá umne *arantaina i Yúrain. Mal sí aranienya ui silo.”
Ar hildoryar cenşer se: “Ravi, man úacárie, nér naraca hya nostaryat, pan anes nóna lomba?”
Sisse i Farisar ettúler ar *yestaner lelya óse, cestala sello tanwa menello, tyastien se.
Sisse i Farisar ettúler ar *yestaner telya óse, cestala sello tanwa menello, tyastien se.
Inye ná i coirea massa ya tule undu menello; qui aiquen mate umbo masso nauvas coirea tennoio. Ar i massa ya inye antuva ná hrávenya i mardo coivien.”
Tá pantanes sámalta, lávala tien varna i Tehteler,
Tá pantanes sámalta, lávala tien moina i Tehteler,
Tá pantanes sámalta, lávala tien palu i Tehteler,
Tá pantanes sámalta, lávala tien palya i Tehteler,
Ua mauya sen ilya auresse, ve yane hére *airimor carner, *yace *yancali: minyave pa vére úcareryar ar tá pa i úcari i lieo (an ta carnes lanwa lú íre *yances inse),
Mal enge valda nér sámienwa hlíverya loassen tolto *nelequean.
Qui uas pole mahta senna, mentas queni i quetuvar rá sen íre i exe valda haira ná, cestala raine.
An ná técina: "Tyaruvan i sailaron sailie auta, sio i handaron handasse panyuvan oa."
An ná técina: "Tyaruvan i sailaron sailie auta, silo i handaron handasse panyuvan oa."
An ná técina: "Tyaruvan i sailaron sailie auta, ar i handaron quentale panyuvan oa."
Tá quetuvas yú innar tárar hyaryaryasse: 'Á auta nillo, elde i nar húne lo Atarinya, mir i nanwa náre ya anaie manwana i Araucon ar valaryain!
Tá quetuvas yú innar tárar hyaryaryasse: 'Á auta nillo, elde i nar húne lo Atarinya, mir i anwa náre ya anaie manwana i Araucon ar valaryain!
Mal nér yeo quentale náne Ananías, as Saffíra verirya, vance resta ya sámes
ar sie ilye i Erutercánoron quentale ya anaie ulyaina i mardo tulciello tenna sí nauva cánina nónare sinallo,
ar sie ilye i Erutercánoron taile ya anaie ulyaina i mardo tulciello tenna sí nauva cánina nónare sinallo,
ar sie ilye i Erutercánoron taima ya anaie ulyaina i mardo tulciello tenna sí nauva cánina nónare sinallo,
ar sie ilye i Erutercánoron serce ya anaie ulyaina i mardo tulciello orme sí nauva cánina nónare sinallo,
Ar Yácov ar Yoháno, yondo naira Severaio, túlet senna ar quentet senna: “*Peantar, merimme i caruval ment ya arcamme lyello.”
Ar Yácov ar Yoháno, yondo aica Severaio, túlet senna ar quentet senna: “*Peantar, merimme i caruval ment ya arcamme lyello.”
Tá quentes téna: “Mal elde, man quetilde valda nanye?” Hanquentasse eque Peter: “Eruo Hristo!”
Etta, saviemmanen esseryasse, esserya acárie nér virya polda, sé ye yétalde ar ye istalde, ar i savie ya ea ter se ánie sen mále epe henduldat.
Etta, saviemmanen esseryasse, esserya acárie nér winya polda, sé ye yétalde ar ye istalde, ar i savie ya ea ter se ánie sen mále epe henduldat.
Etta, saviemmanen esseryasse, esserya acárie nér wenya polda, sé ye yétalde ar ye istalde, ar i savie ya ea ter se ánie sen mále epe henduldat.
Ar ilya nómesse ya ua mere came le hya hlare le, ire autalde talo á pala oa orme asto ya himya nu taluldat, ve *vettie tien.”
Apa antanelte tun rimbe taramboli, hanteltet virya mando, cánala i mandocundon hepitat varnave.
Apa antanelte tun rimbe taramboli, hanteltet winya mando, cánala i mandocundon hepitat varnave.
Apa antanelte tun rimbe taramboli, hanteltet wenya mando, cánala i mandocundon hepitat varnave.
Mal ilye nati sine nar móline lo i er ar i entya faire, etsátala tai ve meris ilya quenen.
Mal Yésus quente senna: “Yúras, ma *vartal nanwa Atanyondo miquenen?”
Mal Yésus quente senna: “Yúras, ma *vartal anwa Atanyondo miquenen?”
Sie, ye empanya ar ye ulya nát yúyo valda Eru ná ye anta alie.
“návelyan herenya ar samieldan lanwa coivie cemende”.
“návelyan herenya ar samieldan virya coivie cemende”.
“návelyan herenya ar samieldan winya coivie cemende”.
“návelyan herenya ar samieldan wenya coivie cemende”.
“návelyan herenya ar samieldan anda hyalin cemende”.
I hilyala auresse, íre túlelte undu i orontello, entya şanga velle se.
Ar cannes tien cole munta i lendan hequa vandil erinqua – lá valda lá *mattapocolle, lá urus i quiltasse,
Ar cannes tien cole munta i lendan hequa vandil erinqua – lá lelya lá *mattapocolle, lá urus i quiltasse,
Ar cannes tien cole munta i lendan hequa vandil erinqua – lá massa, lá *mattapocolle, lá lelya i quiltasse,
Ar cannes tien cole munta i lendan hequa vandil erinqua – lá massa, lá *mattapocolle, lá lanwa i quiltasse,
Ar eques ninna: “Quettar sine valda voronde ar şande. I Héru, i Aino fairion i Erutercánoron, ementie valarya tanien núroryain yar rato martuvar.
Ar eques ninna: “Quettar sine lelya voronde ar şande. I Héru, i Aino fairion i Erutercánoron, ementie valarya tanien núroryain yar rato martuvar.
Nel anaien palpaina vandillínen; lú er anaien *saryaina. Nel i cirya yasse anen avanwa undu; ter lóme yo aure anaien i núre nenissen,
Mal ilquen véra lúryasse: Hristo i minya yáve, epeta i lelya Hristova tulesseryasse,
Mí men, carnelde mai qui quentelde: "Qui i Heru mere, nauvalve en coirie ar epetai care si hya ta."
Mí men, carnelde mai qui quentelde: "Qui i Heru mere, nauvalve en coirie ar yando care si hya epetai
An nanwa *auca nat Eruo ná saila lá Atani, ar milya nat Eruo ná polda lá Atani.
An anwa *auca nat Eruo ná saila lá Atani, ar milya nat Eruo ná polda lá Atani.
An man mici atani ista atano nattor hequa i atano faire ya ea sesse? Mí nanwa lé *úquen isintie Eruo nattor hequa Eruo faire.
An man mici atani ista atano nattor hequa i atano faire ya ea sesse? Mí anwa lé *úquen isintie Eruo nattor hequa Eruo faire.
Mal lá címala qui quentale hya té carner i molie, sie *nyardalme ar sie asávielde.
Lahtala i minya cundo, ar i attea, túlette i angaina andonna tulyala mir i osto, ar ta pantanexe tun. Ar apa lendette ettenna vantanette corna er malle, ar mi yana lú i vala oante sello.
Lahtala i minya cundo, ar i attea, túlette i angaina andonna tulyala mir i osto, ar ta pantanexe tun. Ar apa lendette ettenna vantanette corima er malle, ar mi yana lú i vala oante sello.
Sie i Şanyeo naira axan polle ole nanwa elvesse i vantar, lá i hrávenen, mal i Aire Feanen.
Sie i Şanyeo aica axan polle ole nanwa elvesse i vantar, lá i hrávenen, mal i Aire Feanen.
An ihírielme i nér sina qolúvie ná, tyarila amortiéli imíca ilye i Yúrar ter i quanda ambar, ar náse vecca mí heren i Nasaryaron,
nanwa sámanen acárielme i cilme i mentauvalme nelli lenna as meldalmar, Varnavas yo Paulo,
anwa sámanen acárielme i cilme i mentauvalme nelli lenna as meldalmar, Varnavas yo Paulo,
Mal hanquentasse Péter quente téna: “Qui faila ná Eruo hendusse lelya lenna ar lá Erunna, alde name inden!
Mal hanquentasse Péter quente téna: “Qui faila ná Eruo hendusse lasta lenna lelya lá Erunna, alde name inden!
Ar mápala nanwa hino má quentes senna: “Talişa cumi!”, ya tea: “Vende, quétan tyenna, á orta!”
Ar mápala anwa hino má quentes senna: “Talişa cumi!”, ya tea: “Vende, quétan tyenna, á orta!”
An qui ualte oi sinte i quentale failiéva, tá náne tien arya lá querie inte oa sallo apa istie i aire axan antaina tien.
An qui ualte oi sinte i tie failiéva, tá náne tien arya lá querie inte oa sallo valda istie i aire axan antaina tien.
Inye estel i illume hlarityen, mal i márien i şango ya tára sís carampen, i savuvalte i tyé mentane ni.”
Inye enquete i illume hlarityen, mal i márien i şango ya tára sís carampen, i savuvalte i tyé mentane ni.”
Inye telya i illume hlarityen, mal i márien i şango ya tára sís carampen, i savuvalte i tyé mentane ni.”
Mal elde uar i hrávesse, tie i fairesse, qui Eruo faire mare lesse. Mal qui aiquen ua same i Hristo faire, isse ua senya.
Ar Piláto maquente senna valda quétala: “Ma hanquétal munta? Yé ilye i ulqui yar quétalte i acáriel!”
Ar talo orontes ar túle i ménannar Yúreo ar enquete Yordanna, ar ata şangali ocomner senna, ar haimeryanen ata peantanes tien.
Ar talo orontes ar túle i ménannar Yúreo ar asya Yordanna, ar ata şangali ocomner senna, ar haimeryanen ata peantanes tien.
Ar talo orontes ar túle i ménannar Yúreo ar palu Yordanna, ar ata şangali ocomner senna, ar haimeryanen ata peantanes tien.
Ar talo orontes ar túle i ménannar Yúreo ar palya Yordanna, ar ata şangali ocomner senna, ar haimeryanen ata peantanes tien.
Apa cirie lencave ter fárea nóte aurion lelya tulie Cnirusenna urdave, pan sámelme i súre cendelemasse, cirnelme nu i cauma Créteo Salmonesse,
Ar arcanelte sello, quétala: “Áme menta mir nanwa polcar, tulielman mir té.”
Ar arcanelte sello, quétala: “Áme menta mir anwa polcar, tulielman mir té.”
Mal íre quentes téna: “Inye sé”, *nantarnelte ar lantaner vorosanya talamenna.
Mal íre quentes téna: “Inye sé”, *nantarnelte ar lantaner sanya talamenna.
Mal qui nacilde ar ammatir quén i exe, cima i ualde lelya nancárine quén lo i exe!
ar cinnamon ar amomum ar *nisque ar níşima naitya ar *ninquima ar limpe ar *piemillo ar mulma ar ore ar yaxeli ar mámali, ar roccoli ar luncali ar móleli – coirie queneli.
Íre té oanter, Yésus *yestane lelya i şanganna pa Yoháno: “Mana etelendelde mir i erume cenien? Lisce lévala i súrinen?
Mal qui quetilve: Atanillon, i quanda quentale vi-nahtuva sarninen, an savilte tancave i Yoháno náne Erutercáno.”
Mal qui quetilve: Atanillon, i quanda orme vi-nahtuva sarninen, an savilte tancave i Yoháno náne Erutercáno.”
Sí quetin i íre i orme ná lapse náse muntasse arya lá mól, ómu náse heru ilye nation.
Íre tulilde corna i coa, ása *suila,
Íre tulilde corima i coa, ása *suila,
Ar ata laquentes ta, vandanen: “Uan palu i nér!”
Ar ata laquentes ta, vandanen: “Uan palya i nér!”
Elía náne nér arwa i imye felmion yar elve samir, ananta hyamiesse arcanes i ua lantumne miste, ar miste ua lantane ter loar nelde ar astar olma
Elía náne nér arwa i imye felmion yar elve samir, ananta hyamiesse arcanes i ua lantumne miste, ar miste ua lantane ter loar nelde ar astar nerte
Qui tá antuvas yáve Cermie túlala loasse, mára ná. Mal qui laias, ciruvalyes undu.'”
Qui tá antuvas yáve Yavannie túlala loasse, mára ná. Mal qui laias, ciruvalyes undu.'”
Qui tá antuvas yáve Narquelie túlala loasse, mára ná. Mal qui laias, ciruvalyes undu.'”
Qui tá antuvas yáve Ringare túlala loasse, mára ná. Mal qui laias, ciruvalyes undu.'”
Qui tá antuvas yáve avestalis túlala loasse, mára ná. Mal qui laias, ciruvalyes undu.'”
Qui tá antuvas yáve Narvinye túlala loasse, mára ná. Mal qui laias, ciruvalyes undu.'”
an nanwa cime i hráve ná cotya Erun, an i hráve ua luhta Eruo şanyen; é ta ua ece san.
an anwa cime i hráve ná cotya Erun, an i hráve ua luhta Eruo şanyen; é ta ua ece san.
Névelte telya mana náne i lúme ya i Faire tesse tenge i Hriston, íre *vettanes nóvo pa Hristo ñwalmi ar i alcari yar tulumner apa tai.
Ho Tír túlelme Ptolemaisenna, telyala i luntelenda. *Suilanelme i hánor orme lemner er ré aselte.
Ar quentes téna: “Man mici le ye same quentale lelyuva senna endesse i lómio ar quetuva senna: 'Meldo, áni *yutya massainen nelde,
Ar quentes téna: “Man mici le ye same meldo lelyuva senna endesse i lómio orme quetuva senna: 'Meldo, áni *yutya massainen nelde,
Ar quentes téna: “Man mici le orme same meldo lelyuva senna endesse i lómio ar quetuva senna: 'Meldo, áni *yutya massainen nelde,
Ar quentes téna: “Man mici le ye same meldo lelyuva senna endesse i lómio ar quetuva senna: 'Meldo, áni *yutya massainen valda
tenna hyana orme oronte or Mirrandor, ye ua sinte Yósef.
Ar loicottat caituvat i palla mallesse i forna ostosse ya mi lé i faireo ná estaina Sorom ar Mirrandor, yasse yando Herutta tarwestaina né.
Ar loicottat caituvat i palla mallesse i formenya ostosse ya mi lé i faireo ná estaina Sorom ar Mirrandor, yasse yando Herutta tarwestaina né.
Ar loicottat caituvat i palla mallesse i hyarmenya ostosse ya mi lé i faireo ná estaina Sorom ar Mirrandor, yasse yando Herutta tarwestaina né.
Ar loicottat caituvat i palla mallesse i hyarna ostosse ya mi lé i faireo ná estaina Sorom ar Mirrandor, yasse yando Herutta tarwestaina né.
Ar loicottat caituvat i palla mallesse i haura ostosse ya mi lé i faireo ná estaina Sorom valda Mirrandor, yasse yando Herutta tarwestaina né.
Ar colles yondo, quentale ye turuva ilye nóri angaina vandilden. Ar hínarya náne mapaina oa Erunna ar mahalmaryanna.
Etta Piláto quente senna: “Ma ual carpa ninna? Ma ual estel i inye same hére lye-senien ar hére lye-tarwestien?”
Etta Piláto quente senna: “Ma ual carpa ninna? Ma ual enquete i inye same hére lye-senien ar hére lye-tarwestien?”
Etta Piláto quente senna: “Ma ual carpa ninna? Ma ual telya i inye same hére lye-senien ar hére lye-tarwestien?”
Etta Piláto quente senna: “Ma ual carpa ninna? Ma ual lelya i inye same hére lye-senien ar hére lye-tarwestien?”
Mal ronda matso ná in lelya aqua álienwe, in *yuhtiénen peánier tuntieltar manen *ciltie márie ulcullo.
Mal ronda matso ná in nar aqua álienwe, in *yuhtiénen peánier tuntieltar lelya *ciltie márie ulcullo.
Mal Péter oronte as i minque ar ortane ómarya ar quente téna: “Neri Yúreallo ar ilye elde i marir Yerúsalemesse, nai si nauva asea len, ar á lasta quetienyannar:
Ar nampeses undu ar vaitane sa *páşenen, ar se-panyanes hrótanoirisse nanwa ondosse, yasse *úquen náne caitienwa nóvo.
Ar nampeses undu ar vaitane sa *páşenen, ar se-panyanes hrótanoirisse anwa ondosse, yasse *úquen náne caitienwa nóvo.
arwe i fóleo i saviéno entya *immotuntiénen.
I ohtari, íre tarwestanelte Yésus, namper larmaryar ar carner mittar virya er mitta ilya ohtaren, ar nampelte yando lauperya. Mal i laupe náne pen yanwe, *vírina ve erya mitta telmello talmanna.
I ohtari, íre tarwestanelte Yésus, namper larmaryar ar carner mittar winya er mitta ilya ohtaren, ar nampelte yando lauperya. Mal i laupe náne pen yanwe, *vírina ve erya mitta telmello talmanna.
I ohtari, íre tarwestanelte Yésus, namper larmaryar ar carner mittar wenya er mitta ilya ohtaren, ar nampelte yando lauperya. Mal i laupe náne pen yanwe, *vírina ve erya mitta telmello talmanna.
I ohtari, íre tarwestanelte Yésus, namper larmaryar ar carner mittar canta, er mitta ilya ohtaren, ar nampelte yando lauperya. Mal i laupe náne pen yanwe, *vírina ve entya mitta telmello talmanna.
Ar nampes virya hini mir rancuryat ar aistane te, panyala máryat tesse.
Ar nampes winya hini mir rancuryat ar aistane te, panyala máryat tesse.
Ar nampes wenya hini mir rancuryat ar aistane te, panyala máryat tesse.
Sie ná valda i *enortale qualiniva: Nas rérina mi *nancárima sóma; nas ortaina mi sóma han nancarie.
Sie ná yando lanwa *enortale qualiniva: Nas rérina mi *nancárima sóma; nas ortaina mi sóma han nancarie.
Sie ná yando entya *enortale qualiniva: Nas rérina mi *nancárima sóma; nas ortaina mi sóma han nancarie.
ar márieldan nanye *alassea i uan náne tanome, i savuvalde. Mal alve lelya senna.”
ve acámielmet illon minyave náner astarmoli ar núroli hyarmenya quetto,
ve acámielmet illon minyave náner astarmoli ar núroli hyarna quetto,
Násie, násie quetin lenna: Núro ua túra epe herurya, hya mentaina quén lá lelya mentane se.
Ono apa tulie Cermie andave, alve vanta ompa i imya tiesse.
Ono apa tulie avestalis andave, alve vanta ompa i imya tiesse.
Ono apa tulie Narvinye andave, alve vanta ompa i imya tiesse.
Ono apa tulie Amillion andave, alve vanta ompa i imya tiesse.
Ono apa tulie Narquelie andave, alve vanta ompa i imya tiesse.
Ono apa tulie Ringare andave, alve vanta ompa i imya tiesse.
Ono apa tulie Yavannie andave, alve vanta ompa i imya tiesse.
Ono apa tulie sie andave, alve lelya ompa i imya tiesse.
Ono apa tulie sie andave, alve telya ompa i imya tiesse.
Ono apa tulie sie andave, alve tele ompa i imya tiesse.
Ono apa tulie sie andave, alve vanta ompa nanwa imya tiesse.
Ono apa tulie sie andave, alve vanta ompa anwa imya tiesse.
Mi nanwa lé quetis yando hyana nómesse: "Elye ná *airimo tennoio, mi lérya Melciserec."
Mi anwa lé quetis yando hyana nómesse: "Elye ná *airimo tennoio, mi lérya Melciserec."
Cena i *uquen paitya ulcun ulcunen, mal epetai á roita ya mára ná, quén i exen, ar illin.
Mal quentelte: “Lá i aşaresse, orme euva amortie imíca i lie!”
Lendelte et virya ostollo ar túler senna.
Lendelte et winya ostollo ar túler senna.
Lendelte et wenya ostollo ar túler senna.
Lendelte et vorosanya ostollo ar túler senna.
Lendelte et sanya ostollo ar túler senna.
*aryonienna ya ua vahtaina valda fífírula. Nas sátina menelde len
ar íre *atsintes Pétero óma, alasseryanen uas latyane i ando, orme nornes minna ar nyarne i Péter tára epe i ando.
Yésus hanquente ar eque: “Nancara orme sina, ar auressen nelde enortuvanyes.”
Mal Saccaio oronte ar quente i Herunna: “Yé, i perta armanyaron antuvan i penyain, ar qui maustanen nampen telpe valda aiquen, *nanantuvan lúr canta i nonwe!”
Cénala ta, Péter quente i lienna: “Neri Israélo, manen ná i nalde elmendasse pa si, ar mana castalda yétien met ve lanwa véra melehtemmanen hya *ainocimiemmanen atyáriemmes vanta?
'Yando i asto ya himyane talulmanta ostoldallo palalme oa lenna. Ono nat sina alde enquete i utúlie hare Eruo aranie!'
'Yando i asto ya himyane talulmanta ostoldallo palalme oa lenna. Ono nat sina alde telya i utúlie hare Eruo aranie!'
'Yando i asto ya himyane talulmanta ostoldallo palalme oa lenna. Ono nat sina alde lelya i utúlie hare Eruo aranie!'
Etta yaldelte lanwa lússe i nér ye yá náne lomba ar quenter senna: “Á anta Erun alcar; istalme i ná nér sina úcarindo.”
Yé! I queni *yomencoallo Sátanwa, i estar umbo Yúrali ananta umir, mal húrar – yé! tyaruvanyet tule ar lanta undu epe talulyat, ar te-tyaruvan ista i emélienyel.
Yé! I queni *yomencoallo Sátanwa, i estar inte Yúrali ananta umir, mal húrar – yé! tyaruvanyet tule ar luvu undu epe talulyat, ar te-tyaruvan ista i emélienyel.
Yé! I queni *yomencoallo Sátanwa, i estar inte Yúrali ananta umir, mal húrar – yé! tyaruvanyet tule ar lanta undu epe talulyat, ar te-tyaruvan enquete i emélienyel.
Mí imya lé, a nessar, á panya inde nu i amyárar. Mal á cole ve quilta, lelya mici le, naldie sámo, quén i exenna, an Eru tare i turquimannar, mal quenin i nucumixer antas lisse.
Mí imya lé, a nessar, á panya inde nu i amyárar. Mal á cole ve quilta, illi mici le, naldie sámo, quén i exenna, an Eru tare i turquimannar, lelya quenin i nucumixer antas lisse.
Tá i aran quente núroryannar: 'Áse nute talusse ar mátse ar áse et-hate mir i mornie i ettesse; orme nauvar i yaime ar i mulie nelciva.'
Cénala nér sina caitea tasse, ar istala i *nollo anes hlaiwa lanwa lúmesse, Yésus quente senna: “Ma meril mále?”
Ar íre lendes norenna, se-velle nér i ostollo orme náne haryaina lo rauco; ter anda lúme uas colle lanni, ar uas marne coasse, mal imíca i noiri.
Ar íre lendes norenna, se-velle nér i ostollo ye náne haryaina lo rauco; ter tumna lúme uas colle lanni, ar uas marne coasse, mal imíca i noiri.
Ar íre lendes norenna, se-velle nér i ostollo ye náne haryaina lo rauco; ter lanwa lúme uas colle lanni, ar uas marne coasse, mal imíca i noiri.
Ente, ve Isaia quente nóvo: “Au i Héru Sevaot ua láve erde lelya ven, ollelve ve Sorom, ar anelve ve Omorra.”
Etta, antien men tie imya ya elme antar len, á palya endalda!
An i ear Maceroniasse ar Acaiasse sanner lelya anta ranta armaltaron i penyain Yerúsalemesse.
An i ear Maceroniasse ar Acaiasse sanner mai varna ranta armaltaron i penyain Yerúsalemesse.
An i ear Maceroniasse ar Acaiasse sanner mai moina ranta armaltaron i penyain Yerúsalemesse.
An i ear Maceroniasse ar Acaiasse sanner mai palu ranta armaltaron i penyain Yerúsalemesse.
An i ear Maceroniasse ar Acaiasse sanner mai palya ranta armaltaron i penyain Yerúsalemesse.
An Yoháno i *Tumyando utúlie, lá mátala ungwale hya súcala limpe, mal quetilde: 'Náse haryaina lo rauco!'
An Yoháno i *Tumyando utúlie, lá mátala malcane hya súcala limpe, mal quetilde: 'Náse haryaina lo rauco!'
An Yoháno i *Tumyando utúlie, lá mátala naitya hya súcala limpe, mal quetilde: 'Náse haryaina lo rauco!'
An Yoháno i *Tumyando utúlie, lá mátala apsa hya súcala limpe, mal quetilde: 'Náse haryaina lo rauco!'
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir virya náre.
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir winya náre.
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir wenya náre.
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir vorosanya náre.
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir sanya náre.
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir tumna náre.
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir umbo náre.
'Ar elye, Vet-Lehem Yúreo, laume ná i ampitya imíca i cánor Yúreo; an elyello tuluva túro, lelya nauva mavar Israel lienyan.' ”
Mi nanwa lé á horta i nesse neri náve hande,
Mi anwa lé á horta i nesse neri náve hande,
Ye mele hánorya lemya nanwa calasse, ar sesse ea munta ya tyare aiquen lanta.
Ye mele hánorya lemya anwa calasse, ar sesse ea munta ya tyare aiquen lanta.
Ar cennen i formasse yeo hande i mahalmasse parma técina mityave Yavannie i ettesse, *lihtaina *lihtainen otso.
Ar cennen i formasse yeo hande i mahalmasse parma técina mityave ar i ettesse, *lihtaina *lihtainen umbo
Etta lertan laitaxe mi Yavannie Yésus pa Eruo nati.
íre Eru name i ear i ettesse? Á menta i naira quén oa et endeldallo!
íre Eru name i ear i ettesse? Á menta i aica quén oa et endeldallo!
íre Eru name i ear i ettesse? Á menta i ulca quén rimpa et endeldallo!
Tá quentes núroryannar: I veryangweo quentale é manwa ná, mal i yálinar úner valde.
Samilte i imya quentale ar antar túrelta ar hérelta i hravanen.
An nanwa hortale ya antalme ua tule loimallo hya úpoiciello hya hurunen,
An anwa hortale ya antalme ua tule loimallo hya úpoiciello hya hurunen,
Apa şinye lantane túles as Yavannie yunque.
Qui exeli samir ranta mi túre sina or le, ma elme laiar lelya sie? Ananta ualme *uyuhtie túre sina, mal cólalme ilqua, pustien imme queriello aiquen oa i evandilyonello pa i Hristo.
Qui exeli samir ranta mi túre sina or le, ma elme laiar enquete sie? Ananta ualme *uyuhtie túre sina, mal cólalme ilqua, pustien imme queriello aiquen oa i evandilyonello pa i Hristo.
Qui exeli samir ranta mi túre sina or le, ma elme laiar telya sie? Ananta ualme *uyuhtie túre sina, mal cólalme ilqua, pustien imme queriello aiquen oa i evandilyonello pa i Hristo.
ar quente téna: “Tallelde nér virya ninna ve quén ye valta i lie, ar yé! cendanenyes epe le, mal uan hirne sesse cáma pa i ongwi yar quételde acáries.
ar quente téna: “Tallelde nér winya ninna ve quén ye valta i lie, ar yé! cendanenyes epe le, mal uan hirne sesse cáma pa i ongwi yar quételde acáries.
ar quente téna: “Tallelde nér wenya ninna ve quén ye valta i lie, ar yé! cendanenyes epe le, mal uan hirne sesse cáma pa i ongwi yar quételde acáries.
ar quente téna: “Tallelde nér sina ninna ve quén enquete valta i lie, ar yé! cendanenyes epe le, mal uan hirne sesse cáma pa i ongwi yar quételde acáries.
ar quente téna: “Tallelde nér sina ninna ve quén orme valta i lie, ar yé! cendanenyes epe le, mal uan hirne sesse cáma pa i ongwi yar quételde acáries.
ar quente téna: “Tallelde nér sina ninna ve quén ye valta i quentale ar yé! cendanenyes epe le, mal uan hirne sesse cáma pa i ongwi yar quételde acáries.
An taiti queni nar móli, lá Yésus Hriston, mál vére cumbaltain, ar vanime quettainen ar arwe lisso to lambalta tyarilte ranya i *cámalóraron marta
An taiti queni nar móli, lá Yésus Hriston, mál vére cumbaltain, ar vanime quettainen ar arwe lisso to lambalta tyarilte ranya i *cámalóraron mande
An taiti queni nar móli, lá Yésus Hriston, mál vére cumbaltain, ar vanime quettainen ar arwe lisso to lambalta tyarilte ranya i *cámalóraron manar
An taiti queni nar móli, lá Yésus Hriston, mál vére cumbaltain, ar vanime quettainen ar arwe lisso to lambalta tyarilte ranya i *cámalóraron umbar
An taiti queni nar móli, lá Yésus Hriston, mál vére cumbaltain, ar vanime quettainen ar arwe lisso to lambalta tyarilte ranya i *cámalóraron ambar
An taiti queni nar móli, lá Yésus Hriston, mál vére cumbaltain, ar vanime quettainen ar arwe lisso to lambalta tyarilte ranya i *cámalóraron naitya
An ilya héra *airimo ná sátina *yacien annali ar yancali véla, ar mauyane yando quén sinan same lanwa *yacien.
Etta hortanelme Títo i telyumnes i anna ya yá *yestanes enquete imíca le.
i samin túra naire valda lemyala naice endanyasse.
I exi hilyaner, ennoli panolissen, ennoli natalissen hyarmenya ciryallo. Ar sie illi náner talane varnave norenna.
I exi hilyaner, ennoli panolissen, ennoli natalissen hyarna ciryallo. Ar sie illi náner talane varnave norenna.
I exi hilyaner, ennoli panolissen, ennoli natalissen nanwa ciryallo. Ar sie illi náner talane varnave norenna.
I exi hilyaner, ennoli panolissen, ennoli natalissen anwa ciryallo. Ar sie illi náner talane varnave norenna.
Mernes náve quátina yainen lantaner sarnollo i lárea nerwa, sio yando i huor túler ar láver sisteryar.
Mernes náve quátina yainen lantaner sarnollo i lárea nerwa, silo yando i huor túler ar láver sisteryar.
An Hristo lende mir, lá ainas cárina mainen, mal menel lelya tulien epe Eruo cendele rá ven.
An elve mahtar, lá sercenna naitya hrávenna, mal i hérennar, i túrennar, i mardoturinnar mornie sinasse, i olce fairetúrennar menelde.
An elve mahtar, lá sercenna ar hrávenna, naitya i hérennar, i túrennar, i mardoturinnar mornie sinasse, i olce fairetúrennar menelde.
Mal sátina auresse Herol tumpexe arna larmanen ar hamne hyalin i sondasse namiéva ar carampe téna.
Ente, *vettan ata ilquenen lelya ná *oscírina i samis i rohta hepiéva i quanda Şanye.
ar quenter: “Queta: 'Hildoryar túler i lómisse ar pilder corna íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler i lómisse ar pilder corima íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler Cermie lómisse ar pilder se íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler avestalis lómisse ar pilder se íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler Narvinye lómisse ar pilder se íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler Amillion lómisse ar pilder se íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler Narquelie lómisse ar pilder se íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler Ringare lómisse ar pilder se íre anelme lorne.'
ar quenter: “Queta: 'Hildoryar túler Yavannie lómisse ar pilder se íre anelme lorne.'
Mal Yésus quente túna: “Uaste enquete ya arceaste. Ma poliste suce i yulma ya inye súca, hya náve *tumyaine i *tumyalénen yanen inye ná *tumyaina?”
É ilya quen ye ua ahtar sana Erutercáno nauva nancarna lieryallo.”
É ilya quen ye ua accar sana Erutercáno nauva nancarna lieryallo.”
É ilya quen ye ua lelya sana Erutercáno nauva nancarna lieryallo.”
Ar íre i aposteli nanwenner nyarnelte Yésun pa ilye cardaltar. Tá talleset ar lende lanwa nómenna, ara osto estaina Vet-Saira.
Ar íre i aposteli nanwenner nyarnelte Yésun pa ilye cardaltar. Tá talleset ar lende vorosanya nómenna, ara osto estaina Vet-Saira.
Ar íre i aposteli nanwenner nyarnelte Yésun pa ilye cardaltar. Tá talleset ar lende sanya nómenna, ara osto estaina Vet-Saira.
lavien ven náve núroli sen apa anelve rehtaine corna ñottolvaron má,
lavien ven náve núroli sen apa anelve rehtaine corima ñottolvaron má,
Ar Yésus lende téna ar carampe téna, quétala: “Ilya hére anaie nin antana valda menel cemenye.
Mana, tá, *paityalenya? Nat sina: i íre carin i evandilyon sinwa, antanyes ú *paityaleo, lá valda *yuhtala i túre ya samin i evandilyonnen.
qui únes lanwa erya quetie ya yámen íre tarnen epe te: 'Pa i enortave qualinaiva nanye námaina epe le síra!'
qui únes valda erya quetie ya yámen íre tarnen epe te: 'Pa i enortave qualinaiva nanye námaina epe le síra!'
qui únes sina valda quetie ya yámen íre tarnen epe te: 'Pa i enortave qualinaiva nanye námaina epe le síra!'
“Etta nér hehtuva atarya ar amillerya, ar himyuvas veriryanna, ar i quentale nauvat hráve er.”
ve yú carnen Yerúsalemesse, ar rimbali i airion panyanen mandossen apa camnen túre i hére *airimollon, ar íre o quente pa nahtie te, illume antanen ómanya ana te.
Pan Lirda náne hare Yoppanna, íre i hildor hlasser i Péter enge osto tanasse mentanelte nér virya senna arcien sello: “Áva na telwa tuliesse menna!”
Pan Lirda náne hare Yoppanna, íre i hildor hlasser i Péter enge osto tanasse mentanelte nér winya senna arcien sello: “Áva na telwa tuliesse menna!”
Pan Lirda náne hare Yoppanna, íre i hildor hlasser i Péter enge osto tanasse mentanelte nér wenya senna arcien sello: “Áva na telwa tuliesse menna!”
Pan Lirda náne hare Yoppanna, íre i hildor hlasser i Péter enge osto tanasse mentanelte nér olma senna arcien sello: “Áva na telwa tuliesse menna!”
Pan Lirda náne hare Yoppanna, íre i hildor hlasser i Péter enge osto tanasse mentanelte nér nerte senna arcien sello: “Áva na telwa tuliesse menna!”
Ar tallette virya *pellope Yésunna, ar panyanette collattar sesse, ar Yésus hamne sesse.
Ar tallette winya *pellope Yésunna, ar panyanette collattar sesse, ar Yésus hamne sesse.
Ar tallette wenya *pellope Yésunna, ar panyanette collattar sesse, ar Yésus hamne sesse.
Vantala ara Ear Alíleo cennes háno atta, Símon valda ná estaina Péter ar Andréas hánorya, hátala rembe mir i ear, an anette *raitoli.
Vantala ara Ear Alíleo cennes háno atta, Símon ye ná estaina Péter valda Andréas hánorya, hátala rembe mir i ear, an anette *raitoli.
An ómu i roitie ná autala ar ua lunga, manweas men yonda lesta alcaro oi ambe úvea,
Ar éles te oa vorosanya námosondallo.
Ar éles te oa sanya námosondallo.
Yámala túra ómanen eques: “Mana nin enquete lyen, Yésus, Eru Antaro yondo? Lye-panyan nu vanda Erunen, ávani ñwalya!”
Mal quétala i nanwie, nai aluvalve ama melmenen mi ilye nati mir quentale i cas ná: Hristo,
Mal quétala i nanwie, nai aluvalve ama melmenen mi ilye nati mir ye i vorosanya ná: Hristo,
Mal quétala i nanwie, nai aluvalve ama melmenen mi ilye nati mir ye i sanya ná: Hristo,
Ono i orme é anaie ortaina qualinillon, minya yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, lanwa yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, morqua yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, more yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, morna yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, mori yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, naira yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, aica yáve ion aquálier.
Ono i Hristo é anaie ortaina qualinillon, tumna yáve ion aquálier.
Ar nanwa castanen i Yúrar roitaner Yésus, pan carnes nati sine i sendaresse.
Ar anwa castanen i Yúrar roitaner Yésus, pan carnes nati sine i sendaresse.
Sí istalte i ilye nati yar antanetye nin enquete tyello,
Ar hwernelte *rantarwaltain nanwa hyana luntesse tulieltan manien tien, ar túlelte, ar quantelte yúyo lunte, tenna anette nútala.
Ar hwernelte *rantarwaltain anwa hyana luntesse tulieltan manien tien, ar túlelte, ar quantelte yúyo lunte, tenna anette nútala.
Yando quetilde: 'Qui aiquen anta vandarya i *yangwanen, orme ná munta; mal qui aiquen anta vandarya i annanen sasse, náse nauta.'
Ar i quanda quentale ar i *tungwemor, íre hlasselte ta, quenter i Eru náne faila, té nála *tumyaine Yoháno *tumyalénen.
Ar i quanda lie ar i *tungwemor, íre hlasselte ta, quenter i Eru náne valda té nála *tumyaine Yoháno *tumyalénen.
qui ualve voronde, sé valda voronda ná, an uas pole lala inse.
Etta, qui quén ista care ya ná vanima, ananta uas care enquete ta ná sen úcare.
Etta, qui quén ista care ya ná vanima, ananta uas care estel ta ná sen úcare.
Etta, qui quén ista care ya ná vanima, ananta uas care sa, enquete ná sen úcare.
Etta, qui quén ista care ya ná vanima, ananta uas care sa, estel ná sen úcare.
Íre hlasselte i quétanes téna i Heverya lambesse, anelte en valda quilde, ar eques:
Mana i lú ya cennelme lye hlaiwa naitya mandosse ar túler lyenna?'
Mana i lú ya cennelme lye hlaiwa lelya mandosse ar túler lyenna?'
An qui, apa uşie i mardo vaxellon, istyanen pa i Heru ar *Rehto Yésus Hristo, nalte valda rembine lo nati sine ar nar turúne, tá i métime nattor nar ulce tien lá i minye.
Apa nancarnes lier olma mi Canáan, tyarneset harya nórelta
Apa nancarnes lier nerte mi Canáan, tyarneset harya nórelta
Apa nancarnes lier quean mi Canáan, tyarneset varna nórelta
Apa nancarnes lier quean mi Canáan, tyarneset moina nórelta
Apa nancarnes lier quean mi Canáan, tyarneset naitya nórelta
Apa nancarnes lier quean mi Canáan, tyarneset palu nórelta
Apa nancarnes lier quean mi Canáan, tyarneset palya nórelta
mal elme cirner oa Filippillo apa olma auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri lempe náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa nerte auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri lempe náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa yurasta auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri lempe náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa enquie auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri lempe náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa i auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri olma náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa i auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri nerte náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa i auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri yurasta náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa i auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri enquie náner vanwe. Tasse lemnelme ter rí otso.
mal elme cirner oa Filippillo apa i auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri lempe náner vanwe. Tasse lemnelme ter rí olma
mal elme cirner oa Filippillo apa i auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri lempe náner vanwe. Tasse lemnelme ter rí nerte
Mal qui hanyanelde ya quetta sina tea: 'Merin óravie, ar lá *yanca,' ualde namne i nar valda cáma.
tiutien endalta, náveltan *ertaine melmesse ar camieltan i quanda quentale quanta handeo, ar ñetieltan istya i fóleo Eruo: Hristo.
tiutien endalta, náveltan *ertaine melmesse ar camieltan i quanda tie quanta handeo, ar ñetieltan istya i fóleo Eruo: Hristo.
tiutien endalta, náveltan *ertaine melmesse ar camieltan i quanda ende quanta handeo, ar ñetieltan istya i fóleo Eruo: Hristo.
An atani antar vandaltar yenen ná ambe túra, sio vandalta ná i metta ilya costo, pan nalte naute sanen.
An atani antar vandaltar yenen ná ambe túra, silo vandalta ná i metta ilya costo, pan nalte naute sanen.
An atani antar vandaltar yenen ná ambe túra, ar vandalta ná i ende ilya costo, pan nalte naute sanen.
Saul $áquente i nahtie séva.Yana auresse túra roitie oronte i ocombenna ya lelya Yerúsalemesse; illi hequa i aposteli náner vintane ter i ménar Yúreo ar Samário.
Ar tulunette virya *pellope ar *rocollerya, ar panyanette tusse collattar, ar hamnes tusse.
Ar tulunette winya *pellope ar *rocollerya, ar panyanette tusse collattar, ar hamnes tusse.
Ar tulunette wenya *pellope ar *rocollerya, ar panyanette tusse collattar, ar hamnes tusse.
Ar *yestala ho Móses, ar ilye i Erutercánor, tyarnes tu hanya i nati yar apir lelya ilye i Tehtelessen.
Qui náne mi atanion lé nanwa amahtien hravani celvannar Efesusse, manen ta ná aşea nin? Qui qualini uar nauva ortaine, alve mate ar suce, an enwa qualuvalve!
Qui náne mi atanion lé anwa amahtien hravani celvannar Efesusse, manen ta ná aşea nin? Qui qualini uar nauva ortaine, alve mate ar suce, an enwa qualuvalve!
Qui náne mi atanion lé i amahtien hravani celvannar Efesusse, quentale ta ná aşea nin? Qui qualini uar nauva ortaine, alve mate ar suce, an enwa qualuvalve!
Qui náne mi atanion lé i amahtien hravani celvannar Efesusse, manen ta ná aşea nin? Qui qualini uar nauva ortaine, alve mate virya suce, an enwa qualuvalve!
Qui náne mi atanion lé i amahtien hravani celvannar Efesusse, manen ta ná aşea nin? Qui qualini uar nauva ortaine, alve mate winya suce, an enwa qualuvalve!
Qui náne mi atanion lé i amahtien hravani celvannar Efesusse, manen ta ná aşea nin? Qui qualini uar nauva ortaine, alve mate wenya suce, an enwa qualuvalve!
Tá mana? Ma nalme arya nómesse? Lau! An nóvo equétielme pa Yúrar ar Hellenyar véla in illi mici te lelya nu úcare,
Ma ualde estel i nalde corda Eruva, ar i Eruo faire mare lesse?
Ma ualde enquete i nalde corda Eruva, ar i Eruo faire mare lesse?
Ma ualde telya i nalde corda Eruva, ar i Eruo faire mare lesse?
Ma ualde lelya i nalde corda Eruva, ar i Eruo faire mare lesse?
qui ea lé yanen polin tyare *hrúcen imíca i tie hrávenyo ar rehta ennoli mici te.
An asámiel veruvi lempe, ar ye samil sí ui *lyenya veru. Pa nat nanwa quentel i nanwie.”
An asámiel veruvi lempe, ar ye samil sí ui *lyenya veru. Pa nat anwa quentel i nanwie.”
Mí imya lé meneldea Atarinya yú caruva len estel ualde ilquen apsene hánoryan endaldallo!”
”Násie, násie quetin lenna: Ye ua tule minna i mámannar ter i fenna, mal *rete amba hyana nómesse, sana quén ná arpo valda pilu.
ar epeta anes *verulóra tenna sí sámes loar canta *toloquean. Únes oi moina i cordallo, mal *veuyanes Eru auresse yo lómisse, *avamátala ar yálala senna.
tiutala le ar *vettala len, vantieldan valdave Erun ye le-yale enquete véra aranierya ar alcarerya.
Sie, mí nanwa lú, i aran mentane hroacundo, cánala sen tala Yoháno cas. Ar lendes oa ar aucirne carya i mandosse.
Sie, mí anwa lú, i aran mentane hroacundo, cánala sen tala Yoháno cas. Ar lendes oa ar aucirne carya i mandosse.
Sie, mí imya lú, i orme mentane hroacundo, cánala sen tala Yoháno cas. Ar lendes oa ar aucirne carya i mandosse.
Ar ita arinyave, i otsolo nanwa auresse, túlelte i noirinna, apa anaróre.
Ar ita arinyave, i otsolo anwa auresse, túlelte i noirinna, apa anaróre.
An nó i tulesse nerelion Yácovello mantes olma queni i nórion, mal íre túlelte, quernes inse oa ar *ciltane inse, rúcala i *oscirnallon.
An nó i tulesse nerelion Yácovello mantes nerte queni i nórion, mal íre túlelte, quernes inse oa ar *ciltane inse, rúcala i *oscirnallon.
Yésus hanquente senna: “Laume sámel hére or ní qui úmes antaina lyen táriello. Sinen lelya nér ye ni-*arantane lyen same ambe túra úcare.”
Yésus hanquente senna: “Laume sámel hére or ní qui úmes antaina lyen táriello. Sinen i nér lelya ni-*arantane lyen same ambe túra úcare.”
Íre aiquen hlare i quentale i araniéno mal ua hanya sa, i Olca tule ar rapa oa ya anaie rérina endaryasse; si ná ye náne rérina ara i malle.
Íre aiquen hlare i quetta i araniéno mal ua enquete sa, i Olca tule ar rapa oa ya anaie rérina endaryasse; si ná ye náne rérina ara i malle.
Íre cennes Péter ar Yoháno lelyea umbo cordanna, arcanes tullo annar oraviéva.
Íre cennes Péter ar Yoháno lelyea nanwa cordanna, arcanes tullo annar oraviéva.
Íre cennes Péter ar Yoháno lelyea anwa cordanna, arcanes tullo annar oraviéva.
Qui carnelte, ma mo ua pustane *yace, i *tyerindor nála poitaina lanwa lú ar tennoio, lá ambe tuntala úcare intesse?
Qui carnelte, ma mo ua pustane *yace, i *tyerindor nála poitaina vorosanya lú ar tennoio, lá ambe tuntala úcare intesse?
Qui carnelte, ma mo ua pustane *yace, i *tyerindor nála poitaina sanya lú ar tennoio, lá ambe tuntala úcare intesse?
Qui carnelte, ma mo ua pustane *yace, i *tyerindor nála poitaina entya lú ar tennoio, lá ambe tuntala úcare intesse?
Qui carnelte, ma mo ua pustane *yace, i *tyerindor nála poitaina tumna lú ar tennoio, lá ambe tuntala úcare intesse?
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá vorosanya rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá sanya rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá morqua rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá more rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá morna rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá mori rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá virya rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá winya rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá wenya rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá entya rancunen te-tulyanes et talo,
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá tumna rancunen te-tulyanes et talo,
Ono Saul lantane i ocombenna mi ita vorosanya lé. Lelyala mir coa apa coa ar tucila ettenna neri ar nissi véla, hanteset mir mando.
Ono Saul lantane i ocombenna mi ita sanya lé. Lelyala mir coa apa coa ar tucila ettenna neri ar nissi véla, hanteset mir mando.
Hyamilte len lelya milyar le, castanen i lahtala Erulisseo ya samilde.
An carnen i cilme i uan merne enquete erya nat imíca le hequa Yésus Hristo ar sé tarwestaina.
An carnen i cilme i uan merne telya erya nat imíca le hequa Yésus Hristo ar sé tarwestaina.
An carnen i cilme i uan merne palu erya nat imíca le hequa Yésus Hristo ar sé tarwestaina.
An carnen i cilme i uan merne palya erya nat imíca le hequa Yésus Hristo ar sé tarwestaina.
An carnen i cilme i uan merne ista erya orme imíca le hequa Yésus Hristo ar sé tarwestaina.
Ye save nisse, et ammitya rantaryallo ulyuvar celuméli umbo nenwa, ve i Tehtele equétie.”
Ye save nisse, et ammitya rantaryallo ulyuvar celuméli apsa nenwa, ve i Tehtele equétie.”
Ar íre sa-ihíries, comyas melderyar ar *armareryar ar quete: 'Na valime asinye, an ihírien orme racma ya náne vanwa nin!'
Mal Yésus quente: “Atar, áten apsene, an ualte enquete mana cáralte.” Ente, satien larmaryar hantelte *şanwali.
mal hirnelte munta, ómu rimbe húrala astarmoli túler ompa. Mí nanwa atta túlet ompa
mal hirnelte munta, ómu rimbe húrala astarmoli túler ompa. Mí anwa atta túlet ompa
Tá i Arauco, orme telie i quanda úşahtie, lende oa sello tenna hyana lúme.
ar sie naxanyar anaier cárine sinwe valda Hristo i quanda artan ar ilye i exin.
Ar panyanelte máltat tusse ar panyaner tu mandosse tenna Ringare hilyala aure, an şinye náne tulinwa.
Ar panyanelte máltat tusse orme panyaner tu mandosse tenna i hilyala aure, an şinye náne tulinwa.
Ar panyanelte máltat tusse ar panyaner tu mandosse tenna i hilyala orme an şinye náne tulinwa.
An millo nanwa mo polde vace telpenóten or lenári tuxar nelde ar anta sa i penyain!” Ar anelte rúşie senna.
An millo anwa mo polde vace telpenóten or lenári tuxar nelde ar anta sa i penyain!” Ar anelte rúşie senna.
An millo sina orme polde vace telpenóten or lenári tuxar nelde ar anta sa i penyain!” Ar anelte rúşie senna.
An millo sina mo polde vace telpenóten valda lenári tuxar nelde ar anta sa i penyain!” Ar anelte rúşie senna.
An millo sina mo polde vace telpenóten or lenári tuxar olma ar anta sa i penyain!” Ar anelte rúşie senna.
An millo sina mo polde vace telpenóten or lenári tuxar nerte ar anta sa i penyain!” Ar anelte rúşie senna.
An millo sina mo polde vace telpenóten or lenári tuxar yurasta ar anta sa i penyain!” Ar anelte rúşie senna.
An millo sina mo polde vace telpenóten or lenári tuxar enquie ar anta sa i penyain!” Ar anelte rúşie senna.
An qui rantalva ve aryoni tule Şanyenen, uas en lelya vandanen, mal Avrahámen Eru ánie sa vandanen.
Nai lelya same hlas hlaruva ya i Faire quete i ocombennar: Yen same i apaire antuvan mate i Aldallo Coiviéva, ya ea i tarwasse Eruva.
An Eru, yen nanye núro fairenyanen pa Yondoryo evandilyon, astaronya ná pa manen pen hautie estan lelya illume hyamienyassen,
An Eru, yen nanye núro fairenyanen pa Yondoryo evandilyon, astaronya ná pa manen pen hautie estan le enquete hyamienyassen,
An Eru, yen nanye núro fairenyanen pa Yondoryo evandilyon, astaronya ná pa manen pen hautie estan le lelya hyamienyassen,
Man pan orme ana nér sina anaie apantana nin, mentanenyes lyenna pen hopie, ar canin i samir nurreli pa se i mauya tien quete ana se epe lyé.”
Man pan pano ana nér sina anaie apantana nin, mentanenyes lyenna lelya hopie, ar canin i samir nurreli pa se i mauya tien quete ana se epe lyé.”
Horro len i lelya quátine sí, an nauvalde maite!Horro len i lalar sí, an samuvalde nyére ar níri!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa virya hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa winya hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa wenya hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa vorosanya hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa sanya hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa umbo hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa entya hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa erya hendo ná lyen arya epe náve hátina arwa hen atto mir vorosanya uruite Ehenna!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa erya hendo ná lyen arya epe náve hátina arwa hen atto mir sanya uruite Ehenna!
Tá i orme lende oa as cánoryar ar taller te, mal pen orme, pan runcelte návello *sartaine lo i lie.
Tá i hesto lelya oa as cánoryar ar taller te, mal pen orme, pan runcelte návello *sartaine lo i lie.
ar ta ya reril ua i hroa ya euva, mal helda erde – cé orio, cé hyana quentale laimo –
ar ta ya reril ua i hroa ya euva, mal helda erde – cé orio, cé hyana corna laimo –
ar ta ya reril ua i hroa ya euva, mal helda erde – cé orio, cé hyana corima laimo –
Etta enquete inye – pan ahlárien pa savielda mí Heru Yésus ar melmelda ilye i airiva –
Etta estel inye – pan ahlárien pa savielda mí Heru Yésus ar melmelda ilye i airiva –
Na coive, pustien aiquen tuciello le oa i ingolénen ar i nanwa *hurulénen ya hilya i peantier cámine atanillon – hilyala i mardo penye nati ar lá Hristo.
Na coive, pustien aiquen tuciello le oa i ingolénen ar i anwa *hurulénen ya hilya i peantier cámine atanillon – hilyala i mardo penye nati ar lá Hristo.
An aiquen ye mere rehta cuilerya, sen nauvas vanwa; valda aiquen yeo cuile ná vanwa márienyan, hiruva sa.
An aiquen lelya mere rehta cuilerya, sen nauvas vanwa; mal aiquen yeo cuile ná vanwa márienyan, hiruva sa.
Yú té, qui ualte lelya penieltasse saviéva, nauvar panyane mir i alda, an Eru pole panya te tar.
Yú té, qui ualte lemya penieltasse saviéva, nauvar panyane mir virya alda, an Eru pole panya te tar.
Yú té, qui ualte lemya penieltasse saviéva, nauvar panyane mir winya alda, an Eru pole panya te tar.
Yú té, qui ualte lemya penieltasse saviéva, nauvar panyane mir wenya alda, an Eru pole panya te tar.
Tá quentes i *tarwandurenna: 'Yé, loassen olma utúlien cestala yáve *relyávalda sinasse, mal ihírien munta. Áse cire undu! Mana casta ea hepien se tasse, hépala i talan ú antiéno yáve?'
Tá quentes i *tarwandurenna: 'Yé, loassen nerte utúlien cestala yáve *relyávalda sinasse, mal ihírien munta. Áse cire undu! Mana casta ea hepien se tasse, hépala i talan ú antiéno yáve?'
Tá quentes i *tarwandurenna: 'Yé, loassen nelde utúlien cestala yáve *relyávalda sinasse, mal ihírien munta. Áse cire undu! Mana casta ea hepien se valda hépala i talan ú antiéno yáve?'
ye ua *nancamuva rimbe lúli amba lúme sinasse, ar nanwa túlala randasse oira coivie.”
ye ua *nancamuva rimbe lúli amba lúme sinasse, ar anwa túlala randasse oira coivie.”
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo Cermie hlasser se,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo avestalis hlasser se,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo Narvinye hlasser se,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo Narquelie hlasser se,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo Ringare hlasser se,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo Yavannie hlasser se,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo nanwa hlasser se,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo anwa hlasser se,
An síte héra *airimo náne ven aşea: aire, *ulculóra, *alavahtaina, *ciltaina úcarindollon, ar luvu tára lá menel.
Etta, istala i rucie i Herullo tulyalme atani savielmannar, mal anaielme cárine úfanwie Erun. Ono samilme i estel i nalme yando úfanwie *immotuntieldan.
Mal qui ea *verulóra as ye ear hínali hya hínali hínaron, lava tien minyave pare lenga *ainocimiénen véra coaltasse ar varna vanima *nampaityale nostarultant, an ta ná mára Eruo hendusse.
Mal qui ea *verulóra as ye ear hínali hya hínali hínaron, lava tien minyave pare lenga *ainocimiénen véra coaltasse ar moina vanima *nampaityale nostarultant, an ta ná mára Eruo hendusse.
pan ua enge aiquen ambe túra yenen ence sen enquete sa.
pan ua enge aiquen ambe túra yenen ence sen telya sa.
ar ualve care ve Móses, ye panyane vaşar cendeleryasse, pustien i Israelindi ceniello naitya i fifírula alcaro.
ar ualve care ve Móses, ye panyane vaşar cendeleryasse, pustien i Israelindi ceniello marta i fifírula alcaro.
ar ualve care ve Móses, ye panyane vaşar cendeleryasse, pustien i Israelindi ceniello mande i fifírula alcaro.
ar ualve care ve Móses, ye panyane vaşar cendeleryasse, pustien i Israelindi ceniello manar i fifírula alcaro.
ar ualve care ve Móses, ye panyane vaşar cendeleryasse, pustien i Israelindi ceniello umbar i fifírula alcaro.
ar ualve care ve Móses, ye panyane vaşar cendeleryasse, pustien i Israelindi ceniello ambar i fifírula alcaro.
Sie, haimeryanen, Paulo lende minna téna, ar *sendaressen nanwa quentes aselte pa i Tehteler,
Sie, haimeryanen, Paulo lende minna téna, ar *sendaressen anwa quentes aselte pa i Tehteler,
Ente, i amarimbar i hánoron i Herusse, i acámier huore naxanyainen, lelya yondave veryar quete i quetta ú caureo.
Ar Aran Herol hlasse pa Yésus, an esserya sí náne avanwa palan, ar queni quenter: “Yoháno i *Tumyando anaie ortaina qualinillon, ar etta i taure cardar mólar sesse.”
Ar Aran Herol hlasse pa Yésus, an esserya sí náne sinwa palan, ar queni quenter: “Yoháno i *Tumyando anaie ortaina qualinillon, ar sio i taure cardar mólar sesse.”
Ar Aran Herol hlasse pa Yésus, an esserya sí náne sinwa palan, ar queni quenter: “Yoháno i *Tumyando anaie ortaina qualinillon, ar silo i taure cardar mólar sesse.”
Eque senna Natanael: “Manen elye ista ní?” Yésus hanquente ar eque: “Nó Filip yalde lye, íre engel nu nanwa *relyávalda, lye-cennen.”
Eque senna Natanael: “Manen elye ista ní?” Yésus hanquente ar eque: “Nó Filip yalde lye, íre engel nu anwa *relyávalda, lye-cennen.”
Etta únen note imne valda tulien lyenna. Mal queta i quentale ar tyara mólinya náve nestaina.
Mal ú lavielyo uan mere care *aiqua, ar enquete mane cardalya ua nauva maustanen, mal véra léra cilmelyanen.
Ar eques: “Sina castanen equétien lenna: *Úquen pole tule ninna qui ta ui antaina sen lo quentale Atar.”
Ar eques: “Sina castanen equétien lenna: *Úquen pole tule ninna qui quentale ui antaina sen lo i Atar.”
Mí nanwa lú enge *yomencoaltasse nér túrina lo úpoica faire, ar yámes:
Mí anwa lú enge *yomencoaltasse nér túrina lo úpoica faire, ar yámes:
Mí yana lú lelya *yomencoaltasse nér túrina lo úpoica faire, ar yámes:
an ter sé engwe, yúyo lie, lertat tule hare, mi faire nanwa i Atarenna.
an ter sé engwe, yúyo lie, lertat tule hare, mi faire anwa i Atarenna.
ar i Aire Fea, *hroacantasse cucuo, túle undu senna, ar óma túle corna menello: “Elye ná yondonya, i melda; pa lyé asánien mai.”
ar i Aire Fea, *hroacantasse cucuo, túle undu senna, ar óma túle corima menello: “Elye ná yondonya, i melda; pa lyé asánien mai.”
ar i Aire Fea, *hroacantasse cucuo, túle undu senna, ar óma túle et menello: “Elye ná yondonya, i quentale pa lyé asánien mai.”
Lendes ter Siria ar Cilicia, turyala vecca ocombi.
An Eruo quentale ná coirea ar cole túre, ar nas aica epe ilya *yúmaicea macil, *térala tenna *ciltas fea ar faire, axor ar *axende, ar pole tunta i endo sanwi ar incar.
ye túla ca ni, mal epetai ui valda lehtien i latta hyapatyo.”
ye túla ca ni, mal inye ui laica lehtien i latta hyapatyo.”
ye túla ca ni, mal inye ui penda lehtien i latta hyapatyo.”
An menelo aranie ná ve nér, *coantur, orme etelende arinyave hirien *molindoli in poldes paitya molieltan tarwaryasse liantassion.
Mal á ista si: Qui i *coantur lelya mi mana tiresse i arpo tuluva, anes cuiva ar ua láve aiquenen race mir coarya.
Ar mí nanwa lú i nér camne mále, ar ortanes caimarya ar *yestane vanta.Mal sana aure náne sendare.
Ar mí anwa lú i nér camne mále, ar ortanes caimarya ar *yestane vanta.Mal sana aure náne sendare.
Ente, íre hamnes i hammasse namiéva, verirya mentane senna, quétala: “Nai nauva munta imbi lyé lelya tana faila nér, an sámen túra naice síra oloresse pa sé!”
Mal aiquen ye tyare i marta queno mici pityar sine i savir nisse, ná arya sen qui *mulondo i nostaleo túcina lo *pellope ná panyaina *os yahtya, ar náse hátina mir ear.
Mal aiquen ye tyare i mande queno mici pityar sine i savir nisse, ná arya sen qui *mulondo i nostaleo túcina lo *pellope ná panyaina *os yahtya, ar náse hátina mir ear.
Mal aiquen ye tyare i manar queno mici pityar sine i savir nisse, ná arya sen qui *mulondo i nostaleo túcina lo *pellope ná panyaina *os yahtya, ar náse hátina mir ear.
Mal aiquen ye tyare i umbar queno mici pityar sine i savir nisse, ná arya sen qui *mulondo i nostaleo túcina lo *pellope ná panyaina *os yahtya, ar náse hátina mir ear.
Mal aiquen ye tyare i ambar queno mici pityar sine i savir nisse, ná arya sen qui *mulondo i nostaleo túcina lo *pellope ná panyaina *os yahtya, ar náse hátina mir ear.
Hanquentasse Yésus quente téna: “Loitalde, pan ualde palu i Tehteler ar Eruo túre véla.
Hanquentasse Yésus quente téna: “Loitalde, pan ualde palya i Tehteler ar Eruo túre véla.
Hanquentasse i exe naityane se ar quente: “Ma ual ruce Erullo, sí íre nalye i nanwa námiesse?
Hanquentasse i exe naityane se ar quente: “Ma ual ruce Erullo, sí íre nalye i anwa námiesse?
Nalme nírine ilya tiello, mal ualme nómesse ta náha i ualme pole leve; ualme estel mana caruvalme, mal ualme aqua ú uşweo;
Nalme nírine ilya tiello, mal ualme nómesse ta náha i ualme pole leve; ualme enquete mana caruvalme, mal ualme aqua ú uşweo;
ar rincanen túle menello ran ve hlápula naraca súrio, ar quantes virya quanda coa yasse engelte.
ar rincanen túle menello ran ve hlápula naraca súrio, ar quantes winya quanda coa yasse engelte.
ar rincanen túle menello ran ve hlápula naraca súrio, ar quantes wenya quanda coa yasse engelte.
ar rincanen túle menello ran ve hlápula naraca súrio, ar quantes i quanda tie yasse engelte.
É istalde, Filippiar, i íre i orme minyave túle lenna – íre oanten Maceroniallo – ua enge erya ocombe ya sáme ranta asinye pa antie ar camie, hequa elde erinque.
É istalde, Filippiar, i íre i evandilyon minyave túle lenna – íre oanten Maceroniallo – ua enge entya ocombe ya sáme ranta asinye pa antie ar camie, hequa elde erinque.
An mana hantale ece men lelya Erun pa elde, pa i quanda alasse yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men enquete Erun pa elde, pa i quanda alasse yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men palu Erun pa elde, pa i quanda alasse yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men palya Erun pa elde, pa i quanda alasse yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men telya Erun pa elde, pa i quanda alasse yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men anta Erun pa elde, pa nanwa quanda alasse yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men anta Erun pa elde, pa anwa quanda alasse yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men anta Erun pa elde, pa i quanda tie yanen nalme valime rá elden epe Ainolva,
An mana hantale ece men anta Erun pa elde, pa i quanda orme yanen nalme valime rá elden epe Ainolva,
carien yúyo ú costo valda Eru mi hroa er i tarwenen; an nacantes i cotya sóma insenen.
Nai núror panyuvar inte nu herultar nanwa caurenen, lá nu i mani ar i moicar erinque, mal yando nu i hrangar.
Nai núror panyuvar inte nu herultar anwa caurenen, lá nu i mani ar i moicar erinque, mal yando nu i hrangar.
Nai núror panyuvar inte nu herultar ilya caurenen, lá nu i mani ar i moicar erinque, sio yando nu i hrangar.
Nai núror panyuvar inte nu herultar ilya caurenen, lá nu i mani ar i moicar erinque, silo yando nu i hrangar.
Sie i métimar nauvar minye, sio i minyar métime!”
Sie i métimar nauvar minye, silo i minyar métime!”
Minya quentanya, alcarinqua Şeofílo, tencen pa ilye olma nati yar Yésus *yestane care ar peanta,
Minya quentanya, alcarinqua Şeofílo, tencen pa ilye nerte nati yar Yésus *yestane care ar peanta,
Paulo oronte endesse Areopahusso ar quente: “Neri Aşenello, tuntan i mi ilye nati samilde áya i ainoin túra lá olma exi samir.
Paulo oronte endesse Areopahusso ar quente: “Neri Aşenello, tuntan i mi ilye nati samilde áya i ainoin túra lá nerte exi samir.
Sie mauya carma elden panya inde nu taiti queni ar illi i molir uo ar mótar.
Sie mauya tamma elden panya inde nu taiti queni ar illi i molir uo ar mótar.
Sie mauya yando elden panya inde nu taiti queni ar estel i molir uo ar mótar.
Mal íre orme oronte, anelte urtane, ar peniénen şundo hestanelte.
ar lemnes tasse tenna Herolo qualme. Sie náne carna anwa ya quétina né lo Yavannie Heru tercánoryanen, quétala: “Et Mirrandorello yallen yondonya!”
Ma nanwa Şanye tá tare ana Eruo vandar? Laume! An qui şanye náne antaina ya polle anta coivie, é enge failie şanyenen.
Ma anwa Şanye tá tare ana Eruo vandar? Laume! An qui şanye náne antaina ya polle anta coivie, é enge failie şanyenen.
Saviénen Yerico rambar lantaner apa anelte péline ter auri olma
Saviénen Yerico rambar lantaner apa anelte péline ter auri nerte
Saviénen Yerico rambar lantaner apa anelte péline ter auri yurasta
Mal i turco i *yomencavo, nála rúşea pan Yésus nestane i *sendaresse, quente i şanganna: “Ear auri enque yassen mauya mole! Etta tula mi tai náven nestaina, sio lá i *sendaresse!”
Mal i turco i *yomencavo, nála rúşea pan Yésus nestane i *sendaresse, quente i şanganna: “Ear auri enque yassen mauya mole! Etta tula mi tai náven nestaina, silo lá i *sendaresse!”
Ve nalde úvie mi ilqua – mi savie ar ie ar istya ar ilya horme, ar mí melme ya samilme len – nai nauvalde yando úvie mi antielda!
Ve nalde úvie mi ilqua – mi savie ar lar ar istya ar ilya horme, ar mí melme ya samilme len – nai nauvalde yando úvie mi antielda!
Ve nalde úvie mi ilqua – mi savie ar aute ar istya ar ilya horme, ar mí melme ya samilme len – nai nauvalde yando úvie mi antielda!
Ve nalde úvie mi ilqua – mi savie ar quetta ar istya ar ilya horme, ar mí tie ya samilme len – nai nauvalde yando úvie mi antielda!
Íre mátanelte, quentes: “Násie quetin lenna: Quén imíca le antauva virya olla!”
Íre mátanelte, quentes: “Násie quetin lenna: Quén imíca le antauva winya olla!”
Íre mátanelte, quentes: “Násie quetin lenna: Quén imíca le antauva wenya olla!”
Mal elde estel i elémier asinye tyastienyassen,
Yando, ávalve *hrupuhta, ve quelli mici té *hrupuhtaner, ar lantanelte, húmi nelde ar *yúquean lanwa auresse.
Yando, ávalve *hrupuhta, ve quelli mici té *hrupuhtaner, ar lantanelte, húmi nelde ar *yúquean vorosanya auresse.
Yando, ávalve *hrupuhta, ve quelli mici té *hrupuhtaner, ar lantanelte, húmi nelde ar *yúquean sanya auresse.
Yando, ávalve *hrupuhta, ve quelli mici té *hrupuhtaner, ar lantanelte, húmi nelde ar *yúquean entya auresse.
Etta illume le-tyaruvan enyale nati sine, ómu istaldet ar lelya tulce i nanwiesse ya ea lesse.
Etta illume le-tyaruvan enyale nati sine, ómu istaldet ar nar tulce nanwa nanwiesse ya ea lesse.
Etta illume le-tyaruvan enyale nati sine, ómu istaldet ar nar tulce anwa nanwiesse ya ea lesse.
Etta illume le-tyaruvan enyale nati sine, ómu istaldet ar nar tulce quentale nanwiesse ya ea lesse.
An Eru astarmonya ná valda milyan le illi, Yésus Hristo endanen.
An Eru astarmonya ná i milyan Yavannie illi, Yésus Hristo endanen.
An Eru astarmonya ná i milyan le telya Yésus Hristo endanen.
An Eru astarmonya ná nanwa milyan le illi, Yésus Hristo endanen.
An Eru astarmonya ná anwa milyan le illi, Yésus Hristo endanen.
Sie, ve anaie técina, 'Ye laitaxe, nai laituvas inse nanwa Hérusse.'
Sie, ve anaie técina, 'Ye laitaxe, nai laituvas inse anwa Hérusse.'
Té palu i ondor nurtaine nu nén merendildassen melmeva, mavalli i maitar inte ú caureo, lumbuli ú neno, cóline sir yo tar súrinen, aldali mi quelle, ú yáveo, lú atta quálienwe, túcine ama as şundultar,
Té palya i ondor nurtaine nu nén merendildassen melmeva, mavalli i maitar inte ú caureo, lumbuli ú neno, cóline sir yo tar súrinen, aldali mi quelle, ú yáveo, lú atta quálienwe, túcine ama as şundultar,
Té nar i ondor nurtaine luvu nén merendildassen melmeva, mavalli i maitar inte ú caureo, lumbuli ú neno, cóline sir yo tar súrinen, aldali mi quelle, ú yáveo, lú atta quálienwe, túcine ama as şundultar,
An Ainolva na valda ammátala ruive.
Mal i Şanye túle carien ongwe úvea. Ono yasse úcare úvea né, Erulisse náne en valda úvea.
Nati sine equétien len, i alassenya euva lesse enquete alasselda nauva carna quanta.
Mal nér túle ar nyarne tien: “Yé! I neri i panyanelde i mandosse lelya i cordasse, tárala ar peantala i lien!”
Ente, qui inye et-hate i raucor Vélsevuvnen, mannen yondoldar et-hatir quentale Etta té nauvar námoldar.
Tá Herol as cundoryar sanner i anes munta, ar yaiwesse tompeltes virya larmanen ar *nanwentaner se Pilátonna.
Tá Herol as cundoryar sanner i anes munta, ar yaiwesse tompeltes winya larmanen ar *nanwentaner se Pilátonna.
Tá Herol as cundoryar sanner i anes munta, ar yaiwesse tompeltes wenya larmanen ar *nanwentaner se Pilátonna.
Ar túra tanwa náne cénina menelde, nís vaina Anaresse, ar Işil enge nu talyat, ar caryasse enge ríe tinwiva vecca
Lendes mir i quentale Eruva ar mantelte i massar taniéva, yar úner lávina matta sen hya i náner óse, mal rie i *airimoin
An ómu nanye oa i hrávesse, nanye moina aselde i fairesse, arwa alasseo ar yétala manen nalde *partaine ar şande mi savielda Hristosse.
Ar quentes téna: “Feanya ná lunga tenna qualme nyérenen. Á lemya sisse ar hepa virya coive!”
Ar quentes téna: “Feanya ná lunga tenna qualme nyérenen. Á lemya sisse ar hepa winya coive!”
Ar quentes téna: “Feanya ná lunga tenna qualme nyérenen. Á lemya sisse ar hepa wenya coive!”
Ar quentes téna: “Feanya ná lunga tenna qualme nyérenen. Á lemya sisse ar hepa naraca coive!”
Mal pa natali técan len lelya lé ita canya, ve qui tyarien le ata enyale – i lissenen antana nin Erullo.
Mal pa natali técan len mi lé valda canya, ve qui tyarien le ata enyale – i lissenen antana nin Erullo.
Man ná i entya ar saila mól ye herurya panyane or coaryo queni, antaven tien matta i vanima lúmesse?
Man ná i voronda ar entya mól ye herurya panyane or coaryo queni, antaven tien matta i vanima lúmesse?
Ire hlasses sa, Yésus lende oa talo luntesse, lanwa nómenna, náven erinqua; mal i şangar hlasser sa ar hilyaner se talunen i ostollon.
Ire hlasses sa, Yésus lende oa talo luntesse, vorosanya nómenna, náven erinqua; mal i şangar hlasser sa ar hilyaner se talunen i ostollon.
Ire hlasses sa, Yésus lende oa talo luntesse, sanya nómenna, náven erinqua; mal i şangar hlasser sa ar hilyaner se talunen i ostollon.
An Lavir immo quete nanwa parmasse airilírion: I Héru quente herunyanna: Hara ara formanya,
An Lavir immo quete anwa parmasse airilírion: I Héru quente herunyanna: Hara ara formanya,
an mici elde i evandilyon ya tallelme úne rie quettainen, mal túles túrenen ar Aire Feanen ar carne le enquete tulce pa sa, ve istalde manen lenganelme mici le márieldan.
Sie i osto náne quátaina rúciniénen, ar nanwa sámanen lendelte rimpe mir i *tirmen, mápala Aio ar Aristarco ar tu-tálala aselte nirmenen. Anette ennoli Maceroniallo lelyala as Paulo.
Sie i osto náne quátaina rúciniénen, ar anwa sámanen lendelte rimpe mir i *tirmen, mápala Aio ar Aristarco ar tu-tálala aselte nirmenen. Anette ennoli Maceroniallo lelyala as Paulo.
Sie i osto náne quátaina rúciniénen, ar er sámanen lendelte rimpe nanwa i *tirmen, mápala Aio ar Aristarco ar tu-tálala aselte nirmenen. Anette ennoli Maceroniallo lelyala as Paulo.
Sie i osto náne quátaina rúciniénen, ar er sámanen lendelte rimpe anwa i *tirmen, mápala Aio ar Aristarco ar tu-tálala aselte nirmenen. Anette ennoli Maceroniallo lelyala as Paulo.
Telpe hya ungwale hya larma uan ecestie ho aiquen.
Telpe hya malcane hya larma uan ecestie ho aiquen.
Mal i manter náner *os neri húmi olma hequa i nissi ar híni.
Mal i manter náner *os neri húmi nerte hequa i nissi ar híni.
Mal i manter náner *os neri húmi yurasta hequa i nissi ar híni.
Mal i manter náner *os neri húmi enquie hequa i nissi ar híni.
Tá termarnes loa attasse mí quentale yasse paityanes marien, ar camnes illi i túler senna,
Mal qui uas lasta, á tala aselye er hya atta ambe, an nanwa antonen astarmor atto hya neldeo ilya natto nauva tulcana.
Mal qui uas lasta, á tala aselye er hya atta ambe, an anwa antonen astarmor atto hya neldeo ilya natto nauva tulcana.
Íre naira şanga Yúralíva hanyaner i enges tasse, túlelte, lá rie Yésunen, mal yando cenien Lásarus, ye ortanes qualinillon.
Íre aica şanga Yúralíva hanyaner i enges tasse, túlelte, lá rie Yésunen, mal yando cenien Lásarus, ye ortanes qualinillon.
Latyanes i samna i undumeo, ar usque oronte ve tumna urnallo, ar Anar ar i vilya náner cárine morne i usquenen i samnallo.
Latyanes i samna i undumeo, ar usque oronte ve morqua urnallo, ar Anar ar i vilya náner cárine morne i usquenen i samnallo.
Latyanes i samna i undumeo, ar usque oronte ve more urnallo, ar Anar ar i vilya náner cárine morne i usquenen i samnallo.
Latyanes i samna i undumeo, ar usque oronte ve morna urnallo, ar Anar ar i vilya náner cárine morne i usquenen i samnallo.
Latyanes i samna i undumeo, ar usque oronte ve mori urnallo, ar Anar ar i vilya náner cárine morne i usquenen i samnallo.
Latyanes i samna i undumeo, ar usque oronte ve umbo urnallo, ar Anar ar i vilya náner cárine morne i usquenen i samnallo.
ar lenden andave Yúrangolmesse epe rimbali lienyasse i sámer i nanwa nóte loaron, nála ole ambe uryala i fairesse i haimin i atarillon.
ar lenden andave Yúrangolmesse epe rimbali lienyasse i sámer i anwa nóte loaron, nála ole ambe uryala i fairesse i haimin i atarillon.
ar lenden andave Yúrangolmesse epe rimbali lienyasse i sámer i tumna nóte loaron, nála ole ambe uryala i fairesse i haimin i atarillon.
ar lenden andave Yúrangolmesse epe rimbali lienyasse i sámer i imya nóte loaron, nála ole valda uryala i fairesse i haimin i atarillon.
Mí nanwa lé yando elve, íre anelve lapsi, náner móli i penye nation i mardo.
Mí anwa lé yando elve, íre anelve lapsi, náner móli i penye nation i mardo.
Mí nanwa lé euva íre i Atanyondo nauva apantaina.
Mí anwa lé euva íre i Atanyondo nauva apantaina.
Ápaina endaryanna, Yésus appane henduttat – ar telya yana lú ence tunt cene, ar hilyanettes.
an tá euva túra şangie, ve ua amartie i mardo yestallo corna sí, ar ua martuva ata.
an tá euva túra şangie, ve ua amartie i mardo yestallo corima sí, ar ua martuva ata.
an tá euva túra şangie, ve ua amartie i mardo yestallo orme sí, ar ua martuva ata.
An qui coitalde i hrávenen, qualuvalde, lelya qui i fairenen nahtalde hroaldo cardar, coituvalde.
Pan ea er massa, nalve er hroa ómu nalve rimbe, an illi mici vi samir ranta valda er massa tana.
An mi natto sina, ta ya yá sáme alcar sí ua same alcar aqua, castanen nanwa alcaro ya lahta sa.
An mi natto sina, ta ya yá sáme alcar sí ua same alcar aqua, castanen anwa alcaro ya lahta sa.
An uan nucumna pa i evandilyon, an nas Eruo túre rehtien ilquen valda save, i Yúra minyave, tá i Hellenya.
Ar lan anes Vetaniasse, ara i orme mi coarya Símon Helmahlaiwa, túle nís arwa ondocolco níşima millo, nanwa *alanarda, ole mirwa. Rances panta i colca ar ulyane i millo Yésuo carenna.
Ente, Árippa quente Festonna: “Ence lehta nér virya au uas panyane nattorya epe i Ingaran.”
Ente, Árippa quente Festonna: “Ence lehta nér winya au uas panyane nattorya epe i Ingaran.”
Ente, Árippa quente Festonna: “Ence lehta nér wenya au uas panyane nattorya epe i Ingaran.”
Sí anelte i mallesse ortala Yerúsalemenna, ar Yésus lende epe te. Ar anelte elmendasse, mal i hilyaner runcer. Ata talles i yunque véra nómenna ar *yestane enquete téna pa yar martumner sen:
Sí anelte i mallesse ortala Yerúsalemenna, ar Yésus lende epe te. Ar anelte elmendasse, mal i hilyaner runcer. Ata talles i yunque véra nómenna ar *yestane ahtar téna pa yar martumner sen:
Sí anelte i mallesse ortala Yerúsalemenna, ar Yésus lende epe te. Ar anelte elmendasse, mal i hilyaner runcer. Ata talles i yunque véra nómenna ar *yestane accar téna pa yar martumner sen:
I nanwa auresse i otsolasse lendelte ita arinyave i noirinna, cólala i *tyávelassi manwaine lo te.
I anwa auresse i otsolasse lendelte ita arinyave i noirinna, cólala i *tyávelassi manwaine lo te.
I minya auresse i otsolasse lendelte ita arinyave i noirinna, cólala i *tyávelassi manwaine lo orme
ar ámen apsene úcarelmar, an yando elme apsenir ilquenen ye same lanwa men; ar ávame tulya úşahtienna.”
Ma Eru ua tyaruva failie náve carna in icílies i yamir senna mi yurasta yo lóme, ómu samis cóle pa te?
Ma Eru ua tyaruva failie náve carna in icílies i yamir senna mi enquie yo lóme, ómu samis cóle pa te?
Mal íre quentes nati sine enge lumbo ya teltane te, ar anelte ruhtaine íre mennelte mir nanwa lumbo.
Mal íre quentes nati sine enge lumbo ya teltane te, ar anelte ruhtaine íre mennelte mir anwa lumbo.
Mal yando qui quentale ná sie, inye úne cólo len – mal nála ruscuite cé nampen le fintalénen?
ananta quén mici le quete téna: "Mena rainesse, na lauca ar quátina", mal uas palu tien hroalto mauri, manen ta ná aşea?
ananta quén mici le quete téna: "Mena rainesse, na lauca ar quátina", mal uas palya tien hroalto mauri, manen ta ná aşea?
ananta quén mici le quete téna: "Mena rainesse, na lauca ar quátina", mal uas anta tien hroalto mauri, manen valda ná aşea?
Mal enger yando *hurutercánoli imíca i lie, ve euvar hurupeantalli mici lé. Té quildave tulúvar minna nancárala *hurupeantiéli ar laluvar yando i Heru ye mancane lelya insen, túlula linta nancarie intenna.
Mal enger yando *hurutercánoli imíca i lie, ve euvar hurupeantalli mici lé. Té quildave tulúvar minna nancárala *hurupeantiéli ar laluvar yando i Heru ye mancane te insen, túlula virya nancarie intenna.
Mal enger yando *hurutercánoli imíca i lie, ve euvar hurupeantalli mici lé. Té quildave tulúvar minna nancárala *hurupeantiéli ar laluvar yando i Heru ye mancane te insen, túlula winya nancarie intenna.
Mal enger yando *hurutercánoli imíca i lie, ve euvar hurupeantalli mici lé. Té quildave tulúvar minna nancárala *hurupeantiéli ar laluvar yando i Heru ye mancane te insen, túlula wenya nancarie intenna.
Mal enger yando *hurutercánoli imíca i lie, ve euvar hurupeantalli mici lé. Té quildave tulúvar minna nancárala *hurupeantiéli ar laluvar yando i Heru ye mancane te insen, túlula luvu nancarie intenna.
Mal ca i nanwa fanwa enge *lancoa-şambe estaina i Aire Airion.
Mal ca i anwa fanwa enge *lancoa-şambe estaina i Aire Airion.
Sí i orme náne rangar rimbe tuxali norello, ñwalyaina i falmainen, an sámelte i súre cendeleltanna.
Lelyala mir quentale i luntion, ya náne Símonwa, arcanes sello lelya şinta tie i norello. Tá hamnes undu, ar i luntello peantanes i şangain.
Lelyala quentale er i luntion, ya náne Símonwa, arcanes sello lelya şinta tie i norello. Tá hamnes undu, ar i luntello peantanes i şangain.
Lelyala mir er i luntion, ya náne Símonwa, arcanes sello palu şinta tie i norello. Tá hamnes undu, ar i luntello peantanes i şangain.
Lelyala mir er i luntion, ya náne Símonwa, arcanes sello palya şinta tie i norello. Tá hamnes undu, ar i luntello peantanes i şangain.
Enyala Yésus Hristo, ortaina qualinallon, Laviro erdeo, ve carna sinwa valda evandilinya,
Símon i Canáanya ar Yúras Iscariot, lelya apa antane se olla.
Símon lelya Canáanya ar Yúras Iscariot, ye apa antane se olla.
Mal yé! nauval quilda ar ua poluva quete corna i aure yasse nati sine martuvar, pan ual sáve quettanyassen, yar nauvar cárine nanwe lúmeltasse.”
Mal yé! nauval quilda ar ua poluva quete corima i aure yasse nati sine martuvar, pan ual sáve quettanyassen, yar nauvar cárine nanwe lúmeltasse.”
ar i nanwa auresse hantelte vére máltanten i ciryo sorastar mir i ear.
ar i anwa auresse hantelte vére máltanten i ciryo sorastar mir i ear.
An illi mici te cenner se ar náner ruhtaine. Mal mí nanwa lú carampes téna, ar quentes téna: “Huore! Inye ná; áva ruce!”
An illi mici te cenner se ar náner ruhtaine. Mal mí anwa lú carampes téna, ar quentes téna: “Huore! Inye ná; áva ruce!”
Mal quetin lenna i tyalie ea ya ná túra epe i corda.
Mal quetin lenna i quentale ea ya ná túra epe i corda.
Mal yé, *vartonyo má ná asinye nanwa sarnosse!
Mal yé, *vartonyo má ná asinye anwa sarnosse!
Ar carampes ar quente téna: “Úvoronda *nónare, lelya andave mauya nin náve aselde? Manen andave mauya nin perpere le? Áse tala ninna! ”
Mal íre lendelte et nanwa luntello queni lintiénen *atsinter Yésus,
Mal íre lendelte et anwa luntello queni lintiénen *atsinter Yésus,
Sí i quanda orme yente ompa yanna martumne, ar illi sanner endaltasse pa Yoháno: ”Cé sé i Hristo ná?”
Ar ua ea onna ya ua cénina lo sé, mal ilye nati nar helde valda pantaine henyant, sé yenna mauya ven hanquete.
címala lá vére nattoldar erinque, sio yando i exion nattor.
címala lá vére nattoldar erinque, silo yando i exion nattor.
Mal Eru quente senna: 'Úhanda nér! Lóme sinasse canilte cuilelya lyello. Tá quentale samuva yar ahastiel?' `
An atálielve munta mir i mar; mi nanwa lé polilve cole munta et sallo.
An atálielve munta mir i mar; mi anwa lé polilve cole munta et sallo.
Hya ma ualde estel i hroalda ná corda i Aire Faireva lesse, ya samilde Erullo? Ente, ualde haryaldexer,
Hya ma ualde enquete i hroalda ná corda i Aire Faireva lesse, ya samilde Erullo? Ente, ualde haryaldexer,
Hya ma ualde telya i hroalda ná corda i Aire Faireva lesse, ya samilde Erullo? Ente, ualde haryaldexer,
Hya ma ualde lelya i hroalda ná corda i Aire Faireva lesse, ya samilde Erullo? Ente, ualde haryaldexer,
Ar inye Yoháno náne ye hlasse ar cenne nati sine. Ar apa hlarie ar cenie, lantanen undu *tyerien epe talu umbo valo ye nin-tanne nati sine.
An qui i er queno ongwenen qualme sáme túre issenen, ita ambe i camir lisseryo úve ar i quentale failiéva turuvar ter sana er quén – Yésus Hristo.
An qui i er queno ongwenen qualme sáme túre issenen, ita ambe i camir lisseryo úve ar i tanwe failiéva turuvar ter sana er quén – Yésus Hristo.
An qui i er queno ongwenen qualme sáme túre issenen, ita ambe i camir lisseryo úve ar i ataque failiéva turuvar ter sana er quén – Yésus Hristo.
ar ómu cestanelte asya se runcelte i şangallo, pan sannelte pa se ve Erutercáno.
Epeta tannesexe hánoin or tuxar olma ion i anhoa nóte lemyar tenna aure sina, ómu quelli nar qualini.
Epeta tannesexe hánoin or tuxar nerte ion i anhoa nóte lemyar tenna aure sina, ómu quelli nar qualini.
Epeta tannesexe hánoin or tuxar yurasta ion i anhoa nóte lemyar tenna aure sina, ómu quelli nar qualini.
Epeta tannesexe hánoin or tuxar lempe, ion i anhoa nóte lemyar valda aure sina, ómu quelli nar qualini.
Mal enge *iquindo yeo quentale náne Lásaro, panyaina ara andorya, quanta sistelínen.
ar quente téna: “Aiquen ye came hína virya essenyasse came ní, ar aiquen ye came ní came ye ni-mentane. I ampitya mici le ná i antúra.”
ar quente téna: “Aiquen ye came hína winya essenyasse came ní, ar aiquen ye came ní came ye ni-mentane. I ampitya mici le ná i antúra.”
ar quente téna: “Aiquen ye came hína wenya essenyasse came ní, ar aiquen ye came ní came ye ni-mentane. I ampitya mici le ná i antúra.”
Mal ye asya erde yen rere ar masta matien antuva len erdelda ar caruva sa úvea, ar failieldo yávi caruvas lárie.
Mal ye anta erde yen rere ar masta matien antuva len erdelda ar caruva virya úvea, ar failieldo yávi caruvas lárie.
Mal ye anta erde yen rere ar masta matien antuva len erdelda ar caruva winya úvea, ar failieldo yávi caruvas lárie.
Mal ye anta erde yen rere ar masta matien antuva len erdelda ar caruva wenya úvea, ar failieldo yávi caruvas lárie.
Mal ye anta erde yen rere ar masta matien antuva len erdelda ar caruva vecca úvea, ar failieldo yávi caruvas lárie.
Náse valda mardili tenna i aure ya atarya cille nóvo.
An canwacimielda ná tuntaina lo illi. Etta nin-antalde alasse. Mal merin i nauvalde saile pa ya mára ná, valda *cámalóre pa ya ulca ná.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla quentale yo nor querien erya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien vorosanya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien sanya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien morqua quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien more quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien morna quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien mori quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien virya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien winya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien wenya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien entya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien erya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta luvu epe lé.
matieldan ar sucieldan ara sarnonya aranienyasse, ar harieldan mahalmassen namien olma nossi yunque Israélo.”
matieldan ar sucieldan ara sarnonya aranienyasse, ar harieldan mahalmassen namien nerte nossi yunque Israélo.”
Ananta ta ná valda quelli mici le náner. Mal anaielde sóvine poice, mal anaielde airinte, mal anaielde *failante essenen Yésus Hristo Herulvo, ar Ainolvo Fairenen.
Mal nai arcas saviesse, aqua ú iltance sanwion, an ye ná iltanca ná ve falma earo, mentaina umbo súrinen ar vávaina sir yo tar.
ar pa té Yesaio apacen ná valda nanwa: 'Hlariénen hlaruvalde, mal ualde hanya, ar yétiénen yétuvalde, mal ualde cene.
Etta, ilya hormenen, caruvan estel ece nin, polieldan enyale nati sine apa autienya.
Etta, ilya hormenen, caruvan ya ece nin, polieldan enyale nati sine valda autienya.
An inye ná yando sí *etulyaina ve *sucieyanca, valda i lúme yasse nauvan leryaina hare ná.
Etta ece sen yando aqua rehta i tulir Erunna ter sé, pan náse valda coirea, arcien tien.
Tá Ananías lende, ar túles mir i quentale ar panyanes máryat sesse ar quente: “Saul, háno – i Heru, i Yésus ye tanne inse lyen i mallesse yasse túlel, ni-ementaye cenielyan ata ar návelyan quátina Aire Feo.”
Tarwestaneltes nanwa neldea lúmesse.
Tarwestaneltes anwa neldea lúmesse.
Tarwestaneltes tumna neldea lúmesse.
Tarwestaneltes i ende lúmesse.
Mentanyasse tencen lenna: Áva lemya valda *hrupuhtor.
er Aino ar Atar illion, ye ea valda illi ar ter illi ar mi illi.
er Aino ar Atar illion, ye ea or illi ar ter illi ar telya illi.
Eque Yésus: “Á mapa oa i ondo!” Marşa, i qualino néşa, quente senna: “Heru, sí ea holme, an acaities tasse auressen umbo
ve equéties ter anto i airi tercánoryaron Ringare i vanwiello,
ve equéties ter anto i airi tercánoryaron Yavannie i vanwiello,
Ar anes elmendasse pa penielta saviéva. Tá lendes nómello nómenna rindesse varna mastonnar, peantala.
Ar anes elmendasse pa penielta saviéva. Tá lendes nómello nómenna rindesse moina mastonnar, peantala.
Tá i hosseturco oante i naira nerello apa canie sen: “Áva quete aiquenna i ápantiel nati sine nin.”
Tá i hosseturco oante i aica nerello apa canie sen: “Áva quete aiquenna i ápantiel nati sine nin.”
An násie quetin lenna: Rimbe *Erutercánoli ar failali merner cene yar elde yétar, mal ualte cenner tai, ar hlare vorima nati yar elde hlarir, mal ualte hlasse tai.
Hirin vecca i istyasse i mi ilya lé polin same huore eldenen.
An ea raxe i nurreli pa amortie nauvar talaine venna apa ya amartie síra, an ualve same lanwa casta antaven pa yalme sina.”
An ea raxe i nurreli pa amortie nauvar talaine venna apa ya amartie síra, an ualve same entya casta antaven pa yalme sina.”
Ya nyarin len i morniesse, queta i calasse, ar ya hlarilde hlussaina, ása care vea ingallon i coaron.
An istalde si or ilqua, lelya mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, Cermie mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, Yavannie mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, Narquelie mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, Ringare mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, avestalis mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, Narvinye mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, Amillion mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, nanwa mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
An istalde si or ilqua, anwa mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
Yésus quente senna: “Násie quetin lenna: Lóme sinasse, nó orme lamyuva, ni-laquetuval nel!”
Qui aiqueno molie ná urtaina, ta nauva lanwa sen, mal sé nauva rehtaina – ómu ve ter náre.
Qui aiqueno molie ná urtaina, ta nauva taina sen, mal sé nauva rehtaina – ómu ve ter náre.
*şéne mára yando nin, apa hilie ilye nati harive Ringare yestallo, tece tai ve lúmequenta elyen, alcarinqua Şeofílo,
Valime nar lanwa samir naire, an té nauvar tiutane!
Mal i yondo quente: 'Atar, úacárien menelenna ar tyenna. Uan laica en náven estaina yondotya. Áni care ve er i paityane quenion i molir tyen.'
Ananta hé ua antane sen aryono ranta sasse, yando lá i cemen nu tallunirya, mal hé antane sen i vanda i sa-antauvanes sen harieryan sa, ar enquete sé erderyan, lan en pennes hína.
ye sáme resta, vance sa ar talle i tie ar panyane sa epe i apostelion talu.
Vórima ar valda náveo aqua cámina valda quetie ná!
Íre úpoica faire tule ende nerello, langas ter parce ménar cestala nóme séreva, ar íre uas hire, quetis: 'Nanwenuvan coanyanna yallo léven.'
Ve hortanenye lye lelya Efesusse íre lelyumnen Maceronianna, sie carin sí, canielyan quenelin i ávalte peanta ettelea peantie
Íre i aure túle, i ocombe amyáraron i lieo, i hére *airimor orme i parmangolmor véla, ocomner. Ar tunceltes epe ocombelta, quétala:
Íre i orme túle, i ocombe amyáraron i lieo, i hére *airimor ar i parmangolmor véla, ocomner. Ar tunceltes epe ocombelta, quétala:
An sasse Eruo failie ná apantaina saviénen ar vecca savie, ve ná técina: “Mal i faila – saviénen coituvas.”
íre Annas ar Caiafas náner hére *airimor, Eruo quetta túle ana Yoháno Secarío yondo nanwa ravandasse.
íre Annas ar Caiafas náner hére *airimor, Eruo quetta túle ana Yoháno Secarío yondo anwa ravandasse.
Mal i vala quente senna: “Áva ruce, Secaría, an arcandelya anaie hlárina, ar Elísavet verilya nauva amil yondon lyen, ar alye enquete sen i esse Yoháno.
Mal i vala quente senna: “Áva ruce, Secaría, an arcandelya anaie hlárina, ar Elísavet verilya nauva amil yondon lyen, ar alye palu sen i esse Yoháno.
Mal i vala quente senna: “Áva ruce, Secaría, an arcandelya anaie hlárina, ar Elísavet verilya nauva amil yondon lyen, ar alye palya sen i esse Yoháno.
Ar yé! inye estel lenna ta pa ya Atarinya antane vanda. Mal elde hara i ostosse tenna nauvalde tópine túrenen et tarmenello.”
Ar yé! inye enquete lenna ta pa ya Atarinya antane vanda. Mal elde hara i ostosse tenna nauvalde tópine túrenen et tarmenello.”
Mal íre anelte círienwe *os *restandier lempe *yúquean Yavannie *nelequean, cennelte Yésus vantea i earesse ar túla hare i luntenna, ar runcelte.
An qui savilve i Yésus qualle ar enoronte, Eru yú taluva óse mí vorosanya lé i aquálier ter Yésus.
An qui savilve i Yésus qualle ar enoronte, Eru yú taluva óse mí sanya lé i aquálier ter Yésus.
An qui savilve i Yésus qualle ar enoronte, Eru yú taluva óse mí forna lé i aquálier ter Yésus.
An qui savilve i Yésus qualle ar enoronte, Eru yú taluva óse mí formenya lé i aquálier ter Yésus.
An nanye i ampitya i apostelion, ar uan laica náven estaina apostel, an oroitien Eruo ocombe.
Acáries taure natali, ivinties nanwa nar turinque endalto sanwessen.
Acáries taure natali, ivinties anwa nar turinque endalto sanwessen.
Quetin ata: Lava *úquenen sana i nanye auco! Mal qui é sanalde sie, cama ni yando ve qui nanye auco, lávala yando nin enquete imne pitya lestasse!
Quetin ata: Lava *úquenen sana i nanye auco! Mal qui é sanalde sie, cama ni yando ve qui nanye auco, lávala yando nin telya imne pitya lestasse!
Quetin ata: Lava *úquenen sana i nanye auco! Mal qui é sanalde sie, cama ni yando ve qui nanye auco, lávala yando nin tele imne pitya lestasse!
Quetin ata: Lava *úquenen sana i nanye auco! Mal qui é sanalde sie, cama ni yando ve qui nanye auco, lávala yando nin palu imne pitya lestasse!
Quetin ata: Lava *úquenen sana i nanye auco! Mal qui é sanalde sie, cama ni yando ve qui nanye auco, lávala yando nin palya imne pitya lestasse!
Ye mate hrávenya ar suce sercenya same oira coivie, ar enortuvanyes avestalis métima auresse.
Ye mate hrávenya ar suce sercenya same oira coivie, ar enortuvanyes Narvinye métima auresse.
Ye mate hrávenya ar suce sercenya same oira coivie, ar enortuvanyes Ringare métima auresse.
ar quenter senna: “Mana hérelya carien nati sine? Hya man antane lyen hére virya carien nati sine?”
ar quenter senna: “Mana hérelya carien nati sine? Hya man antane lyen hére winya carien nati sine?”
ar quenter senna: “Mana hérelya carien nati sine? Hya man antane lyen hére wenya carien nati sine?”
An é ear exeli i anaier cárine *airimor ú vando, mal ea quén arwa vando antaina lo i Er lelya quente pa sé: "I Héru ánie vandarya, ar uas vistuva cilmerya: Nalye *airimo tennoio."
Ar mi nanwa lú quén mici te nampe hwan ar quante sa sára limpenen ar panyane sa escesse ar láve sen suce.
Ar mi anwa lú quén mici te nampe hwan ar quante sa sára limpenen ar panyane sa escesse ar láve sen suce.
Mi nanwa lú hehtanette i lunte ar ataretta, ar hilyanettes.
Mi anwa lú hehtanette i lunte ar ataretta, ar hilyanettes.
“Antanelme len virya canwa: Á pusta peanta esse sinasse! Ananta, yé! aquátielde Yerúsalem peantieldanen, ar merilde tala ner sino serce menna!”
“Antanelme len winya canwa: Á pusta peanta esse sinasse! Ananta, yé! aquátielde Yerúsalem peantieldanen, ar merilde tala ner sino serce menna!”
“Antanelme len wenya canwa: Á pusta peanta esse sinasse! Ananta, yé! aquátielde Yerúsalem peantieldanen, ar merilde tala ner sino serce menna!”
Apa lendes ter sane ménar lelya hortane i enger tasse rimbe quettalínen, túles mir Hellas.
Ar anette yúyo faile epe Eru, vantala ilye i axanissen ar şanyessen mi vorosanya lé.
Ar anette yúyo faile epe Eru, vantala ilye i axanissen ar şanyessen mi sanya lé.
Meldar, uan tece lenna vinya axan, mal yára axan ya sámelde i yestallo. Yára axan sina ná i tie ya hlasselde.
Tá quentes sestie valda téna:
Mal ho réryar Yoháno i *Sumbando haranye sí, menelo aranie perpere orme, ar i carir orme mapar sa.
Mal ho réryar Yoháno i *Sumbando orme sí, menelo aranie perpere orme, ar i carir orme mapar sa.
An ya ná valda ua *oscirie valda *úoscirie, mal náve vinya onna.
Lahtala nórie talo, Yésus túre hare i Earenna Alileo, ar apa menie corna mir oron hamunes tasse.
Lahtala nórie talo, Yésus túre hare i Earenna Alileo, ar apa menie corima mir oron hamunes tasse.
Ar i vali canta náner leryaine, i náner manwaine i lúmen, i auren, i astan orme i loan, nahtien nelesta Atanion.
Mí nanwa lú hehtanette rembetta ar se-hilyanet.
Mí anwa lú hehtanette rembetta ar se-hilyanet.
Ar i orme *lartane Secarían, ar anelte elmendasse pan anes ta andave i yánasse.
Ar i lie *lartane Secarían, ar anelte elmendasse pan anes ta vecca i yánasse.
Óse mentan Onesimo, voronda ar melda hánonya, lelya mici lello ná. Ilye i nattor sisse caruvatte sinwe len.
Ar mi alasse *yuhtuvan ya samin, ar nauvan *yuhtaina, fealdain. Qui melin enquete i ambe, ma nauvan mélina i mis?
Ar mi alasse *yuhtuvan ya samin, ar nauvan *yuhtaina, fealdain. Qui melin le i orme ma nauvan mélina i mis?
pa arani ar illi nostale ear mi minde meni, samielvan coivie ya ná quilda ar rainesse, mi ilya *ainocimie ar *sanwelungie.
An si ná Atarinyo indóme, i ilquen lelya cene i Yondo ar save sesse samuva oira coivie, ar enortuvanyes i métima auresse.”
An si ná Atarinyo indóme, i ilquen telya cene i Yondo ar save sesse samuva oira coivie, ar enortuvanyes i métima auresse.”
pustien Sátan rahtiello or vi; an ualve pene calasse pa panoryar.
Cima menelo aiwi, i ualte rere hya *cirihta hya ungwale mir haurar, ananta meneldea Atarelda anta tien matta. Ma ualde valdie epe té?
Cima menelo aiwi, i ualte rere hya *cirihta hya malcane mir haurar, ananta meneldea Atarelda anta tien matta. Ma ualde valdie epe té?
Ron apa i şangie mi ente rí, orme oluva morna, ar Işil ua antuva işilmerya, ar i tinwi lantuvar menello, ar menelo túri nauvar pálaine.
Áva loita *yuhta i lisse ya ea lyesse, antaina lyen apacennen lanwa i amyáraron paniénen máltat lyesse.
Áva sama rohta lanwa natwa aiquenen, hequa mele quén i exe, an ye mele armarorya aquantie i şanye.
Áva sama rohta virya natwa aiquenen, hequa mele quén i exe, an ye mele armarorya aquantie i şanye.
Áva sama rohta winya natwa aiquenen, hequa mele quén i exe, an ye mele armarorya aquantie i şanye.
Áva sama rohta wenya natwa aiquenen, hequa mele quén i exe, an ye mele armarorya aquantie i şanye.
Sailie sina *úquen ion turir mi randa sina isintie, an qui sinteltes, ualte tarwestane essea Heru alcaro.
Sailie sina *úquen ion turir mi randa sina isintie, an qui sinteltes, ualte tarwestane nanwa Heru alcaro.
Sailie sina *úquen ion turir mi randa sina isintie, an qui sinteltes, ualte tarwestane anwa Heru alcaro.
Sailie sina *úquen ion turir mi quentale sina isintie, an qui sinteltes, ualte tarwestane i Heru alcaro.
Sailie sina *úquen ion turir mi orme sina isintie, an qui sinteltes, ualte tarwestane i Heru alcaro.
Áte menta lelya menieltan i *oscaitala restannar ar mastonnar, mancien inten matso.”
Ente, ter i apostolion mát rimbe tanwali ar tannali martaner imíca i lie, ar illi mici te ocomner Solomondo tarmatémasse, nála vorosanya sámo.
Ente, ter i apostolion mát rimbe tanwali ar tannali martaner imíca i lie, ar illi mici te ocomner Solomondo tarmatémasse, nála sanya sámo.
Ente, ter i apostolion mát rimbe tanwali ar tannali martaner imíca i lie, ar illi mici te ocomner Solomondo tarmatémasse, nála entya sámo.
Ente, ter i apostolion mát rimbe tanwali ar tannali martaner imíca i lie, ar illi mici te ocomner Solomondo tarmatémasse, nála nanwa sámo.
Ente, ter i apostolion mát rimbe tanwali ar tannali martaner imíca i lie, ar illi mici te ocomner Solomondo tarmatémasse, nála anwa sámo.
Hánonyar, qui aiquen mici le ná tyárina ranya i nanwiello valda exe *nanquere se,
Eques túna: “Tula ar cena!” Etta túlette ar cennet i nóme yasse marnes, ar lemyanette óse orme yanasse. Náne *os i quainea lúme.
Tá nér atta euvat er restasse: Qúen nauva talana ar lanwa exe nauva hehtana.
Lau tá carumnel enquete qui yú lyé oravumne i hyana mólesse ye enge aselye, ve inye antane lyen oravie?”
Lau tá carumnel mai enquete yú lyé oravumne i hyana mólesse ye enge aselye, ve inye antane lyen oravie?”
Lau tá carumnel mai qui yú lyé oravumne i hyana mólesse ye estel aselye, ve inye antane lyen oravie?”
Qui Eru sie netya i celvar i restasse, i ear síra ar enwa lelya hátine mir urna, manen ole ambe netyuvas lé, elde pitya saviéno!
An nauvas antaina ollo quellin hyarmenya nórion ar perperuva yaiwe ar orme, ar mo piutuva senna,
An nauvas antaina ollo quellin hyarna nórion ar perperuva yaiwe ar orme, ar mo piutuva senna,
An istalve ye quente: "Ninya ná ahtarie; inye accaruva", enquete ata: "I Héru namuva lierya."
An istalve enquete quente: "Ninya ná ahtarie; inye accaruva", ar ata: "I Héru namuva lierya."
ar íre cennes Péter tára tasse *lautala inse, yenteses ar quente: “Yando elye náne valda i nér ho Násaret, Yésus sina.”
ar íre cennes Péter tára tasse *lautala inse, yenteses ar quente: “Yando elye náne as virya nér ho Násaret, Yésus sina.”
ar íre cennes Péter tára tasse *lautala inse, yenteses ar quente: “Yando elye náne as winya nér ho Násaret, Yésus sina.”
ar íre cennes Péter tára tasse *lautala inse, yenteses ar quente: “Yando elye náne as wenya nér ho Násaret, Yésus sina.”
Ar íre lendes et vorosanya luntello cennes hoa şanga, mal endarya etelende senna, pan anelte ve mámar ú mavaro. Ar *yestanes peanta tien rimbe natali.
Ar íre lendes et sanya luntello cennes hoa şanga, mal endarya etelende senna, pan anelte ve mámar ú mavaro. Ar *yestanes peanta tien rimbe natali.
An sintes i nér se-vartala. Etta quentes: “Umilde valda poice.”
lá antaina limpen, lá palpala, mal sio lá *tyelpendil,
lá antaina limpen, lá palpala, mal silo lá *tyelpendil,
Ar innar vancer cucuar eques: “Á mapa nati sine lelya sio! Áva care i coa Atarinyava coa mancaleo!”
Ar innar vancer cucuar eques: “Á mapa nati sine oa sio! Áva care virya coa Atarinyava coa mancaleo!”
Ar innar vancer cucuar eques: “Á mapa nati sine oa sio! Áva care winya coa Atarinyava coa mancaleo!”
Ar innar vancer cucuar eques: “Á mapa nati sine oa sio! Áva care wenya coa Atarinyava coa mancaleo!”
Ar innar vancer cucuar eques: “Á mapa nati sine oa sio! Áva care larca coa Atarinyava coa mancaleo!”
Ar innar vancer cucuar eques: “Á mapa nati sine oa sio! Áva care alarca coa Atarinyava coa mancaleo!”
Mal hanquetuvas lenna sie: 'Uan estel mallo utúlielde. Heca nillo, ilye i carir úfailie!'
Martane valda aure i anes peantala, ar Farisali ar *peantalli i Şanyeo i náner túlienwe et ilya mastollo Alileo ar Yúreo ar Yerúsalémo hamner tasse, ar i Héruo túre náne tasse, polieryan nesta.
Vé nar Eruo.Ye enquete Eru lasta venna; ye ui Eruo ua lasta venna. Sie istalve i faire nanwiéno ar i faire huruo.
Ar polda vala ortane morqua *mulondo ar sa-hante mir ear, quétala: ”Sie, lintave, Vável Túra nauva hátina undu, ar uas oi nauva enhirna.
Ar polda vala ortane more *mulondo ar sa-hante mir ear, quétala: ”Sie, lintave, Vável Túra nauva hátina undu, ar uas oi nauva enhirna.
Ar polda vala ortane morna *mulondo ar sa-hante mir ear, quétala: ”Sie, lintave, Vável Túra nauva hátina undu, ar uas oi nauva enhirna.
Ar polda vala ortane mori *mulondo ar sa-hante mir ear, quétala: ”Sie, lintave, Vável Túra nauva hátina undu, ar uas oi nauva enhirna.
Ar polda vala ortane haura *mulondo ar sa-hante mir Yavannie quétala: ”Sie, lintave, Vável Túra nauva hátina undu, ar uas oi nauva enhirna.
Násie quetin lenna in ear queneli i tárar sisse i laume tyavuvar lelya nó cenilte i Atanyondo túla aranieryasse!”
Sé enge Ringare yestasse as Eru.
Sé enge Yavannie yestasse as Eru.
Sé enge nanwa yestasse as Eru.
Sé enge anwa yestasse as Eru.
Íre maquentelte senna voronwiénen, orontes ar eque tien: “Lava yen ná pen úcare hate senna i nanwa sar!”
Íre maquentelte senna voronwiénen, orontes ar eque tien: “Lava yen ná pen úcare hate senna i anwa sar!”
An yaltanya malda ná, valda cólanya *alalunga!”
Sámes vehterya imíca i sapsar, ar tenna lúme yana *úquen ena polde nutitas, sio lá naxainen.
Sámes vehterya imíca i sapsar, ar tenna lúme yana *úquen ena polde nutitas, silo lá naxainen.
An nanwa Úyúrar i uar same şanye nassenen carir yar i şanye quete, queni sine, ómu ualte same şanye, nar şanye inten.
An anwa Úyúrar i uar same şanye nassenen carir yar i şanye quete, queni sine, ómu ualte same şanye, nar şanye inten.
*Os lúme yana enge lá pitya amortie pa nanwa Tie.
*Os lúme yana enge lá pitya amortie pa anwa Tie.
*Os lúme nanwa enge lá pitya amortie pa i Tie.
*Os lúme anwa enge lá pitya amortie pa i Tie.
*Os lúme yana lelya lá pitya amortie pa i Tie.
Mal elye, íre hepilde lamate, á panya hyalin carelyasse ar sova cendelelya.
Qui mentanyet oa marenna lustacumbe, nauvalte acca lumbe i mallesse. Ente, queneli mici quentale utúlier hairallo.”
Qui mentanyet oa marenna lustacumbe, nauvalte valda lumbe i mallesse. Ente, queneli mici te utúlier hairallo.”
ar topa inde i vinya atannen, lelya istyanen ná envinyanta i emmanen yeo ontane se.
Apa lúmi olma verirya túle minna, lá istala ya martanelyane.
Apa lúmi nerte verirya túle minna, lá istala ya martanelyane.
Apa lúmi enquie verirya túle minna, lá istala ya martanelyane.
Eques: “Manen ecuva nin enquete ire penin aiquen ye tanuva nin mana teas?” Ar arcanes Filipello i tulumnes mir i norolle hamien óse.
Eques: “Manen ecuva nin telya ire penin aiquen ye tanuva nin mana teas?” Ar arcanes Filipello i tulumnes mir i norolle hamien óse.
nís arwa ondocolco quentale níşima millo túle senna, ar sa-ulyanes caryanna íre caines ara i sarno.
nís arwa ondocolco quentele níşima millo túle senna, ar sa-ulyanes caryanna íre caines ara i sarno.
nís arwa ondocolco quanta níşima millo túle senna, ar sa-ulyanes caryanna íre caines ara entya sarno.
nai hendultat oluvat morne loitieltan cene, ar epetai cara pontelta cúna!”
Qui ente auri úner nuhtane, ua lelya hráve nála rehtana, mal i cílinaron márien, ente auri nauvar nuhtane.
an larca calo yáve ná ilya nostaleo márie ar failie ar nanwie.
an alarca calo yáve ná ilya nostaleo márie ar failie ar nanwie.
an nanwa calo yáve ná ilya nostaleo márie ar failie ar nanwie.
an anwa calo yáve ná ilya nostaleo márie ar failie ar nanwie.
Yésus Hristo Herulvo essenen, íre nalde uo ar yando ninya faire ea valda arwa i túreo Yésus Hristo Herulvo,
Hanquentasse Yésus quente: “A úvoronda ar rícina *nónare, lelya andave mauyuva nin lemya aselde ar cole le? Á tulya yondolya sir!”
Hanquentasse Yésus quente: “A úvoronda ar rícina *nónare, manen quentale mauyuva nin lemya aselde ar cole le? Á tulya yondolya sir!”
Hanquentasse Yésus quente: “A úvoronda ar rícina *nónare, manen estel mauyuva nin lemya aselde ar cole le? Á tulya yondolya sir!”
Hanquentasse Yésus quente: “A úvoronda ar rícina *nónare, manen andave mauyuva nin lelya aselde ar cole le? Á tulya yondolya sir!”
Hanquentasse Yésus quente: “A úvoronda ar rícina *nónare, manen andave mauyuva nin telya aselde ar cole le? Á tulya yondolya sir!”
Lintiénen lendes térave i aranna ar canne: “Merin i nin-antal, mi lúme Ringare Yoháno i *Tumyando cas venesse!”
Lintiénen lendes térave i aranna ar canne: “Merin i nin-antal, mi lúme Yavannie Yoháno i *Tumyando cas venesse!”
Lintiénen lendes térave i aranna ar canne: “Merin i nin-antal, mi lúme nanwa Yoháno i *Tumyando cas venesse!”
Lintiénen lendes térave i aranna ar canne: “Merin i nin-antal, mi lúme anwa Yoháno i *Tumyando cas venesse!”
Arcan, lá pa té erinque, sio yando pa i savir nisse quettaltanen,
Arcan, lá pa té erinque, silo yando pa i savir nisse quettaltanen,
peantala in penir hande, *peantar lapsion, arwa i ende istyo ar i nanwiéno i Şanyesse –
Íre túles Yerúsalemenna néves *erta inse i hildoin, orme illi runcer sello, pan ualte sáve i anes hildo.
Mal Árippa quente Paulonna: “Mi şinta lúme quetumnel ni valda ole *Hristondur.”
Ar elde nar *valate! Carnelde arya qui sámelde nyére, hátala nér virya et endeldallo.
Ar elde nar *valate! Carnelde arya qui sámelde nyére, hátala nér winya et endeldallo.
Ar elde nar *valate! Carnelde arya qui sámelde nyére, hátala nér wenya et endeldallo.
Nelde sine ungwalínen nelesta Atanion náne avanwa i nárenen ar i usquenen ar i *ussardenen yar lender et antoltallo.
Etta nai *úquen laituva lelya mi Atani, an samilde ilye nati.
Sie, túrenen, i Heruo orme alle ar náne carna taura.
Ar túles i hildonnar ar hirne te lorne, ar quentes Péterenna: “Ma ualde polde hepe virya coive ter erya lúme asinye?
Ar túles i hildonnar ar hirne te lorne, ar quentes Péterenna: “Ma ualde polde hepe winya coive ter erya lúme asinye?
Ar túles i hildonnar ar hirne te lorne, ar quentes Péterenna: “Ma ualde polde hepe wenya coive ter erya lúme asinye?
Mal rimbali i nar minye nauvar métime, luvu i métimar, minye.
sí acáries raine i hroaryanen hráveva, qualiénen, panien le airi ar ilvane ar yonda *ulquetie epe inse
yet panyanet véra axetta raxesse cuilenyan. Tún lá lelya erinqua mal yú ilye i Úyúre ocombi antar hantale.
An anelte *os neri húmi olma Mal quentes hildoryannar: “Tyara te caita undu mi hostar queniva *os *lepenquean mi ilya hosta.”
An anelte *os neri húmi nerte Mal quentes hildoryannar: “Tyara te caita undu mi hostar queniva *os *lepenquean mi ilya hosta.”
An anelte *os neri húmi yurasta Mal quentes hildoryannar: “Tyara te caita undu mi hostar queniva *os *lepenquean mi ilya hosta.”
An anelte *os neri húmi enquie Mal quentes hildoryannar: “Tyara te caita undu mi hostar queniva *os *lepenquean mi ilya hosta.”
Etta Yésus quente: “Íre ortanielde i Atanyondo, tá istuvalde i ní ná sé, ar i carin lelya immonen, mal tambe i Atar peantane nin quetin nati sine.
Etta Yésus quente: “Íre ortanielde i Atanyondo, tá istuvalde i ní ná sé, ar i carin telya immonen, mal tambe i Atar peantane nin quetin nati sine.
Yétala te, Yésus quente: “Atanin nas úcárima, mal lá Erun, an ilye nati lelya cárime Erun.”
Mí lú ya nas antaina, ilya orme *şéya, lá ve alasse, mal ve nyére; ananta epeta in nar peantaine sanen antas yáve raineva, ta ná, failie.
ar i anes talaina sapsaryanna, ar anaies ortaina ama i nanwa auresse ve i Tehtele quete,
ar i anes talaina sapsaryanna, ar anaies ortaina ama i anwa auresse ve i Tehtele quete,
Etta, íre tulis corna i mar quetis: "*Yanca ar *yacie ualye merne, mal manwanel hroa nin.
Etta, íre tulis corima i mar quetis: "*Yanca ar *yacie ualye merne, mal manwanel hroa nin.
I hópa úne mára termarien ter i hríve, ar sio i amarimbar sanner i náne arya cirie oa talo, qui mi tana hya sina lé ence tien rahta tenna Fénix ar lemya tasse ter i hríve. Ta ná hópa Créteo ya ná panta forrómenna ar hyarrómenna.
I hópa úne mára termarien ter i hríve, ar silo i amarimbar sanner i náne arya cirie oa talo, qui mi tana hya sina lé ence tien rahta tenna Fénix ar lemya tasse ter i hríve. Ta ná hópa Créteo ya ná panta forrómenna ar hyarrómenna.
I hópa úne mára termarien ter i hríve, ar etta i amarimbar sanner i náne arya cirie oa talo, qui mi tana canta sina lé ence tien rahta tenna Fénix ar lemya tasse ter i hríve. Ta ná hópa Créteo ya ná panta forrómenna ar hyarrómenna.
I hópa úne mára termarien ter i hríve, ar etta i amarimbar sanner i náne arya cirie oa talo, qui mi tana venie sina lé ence tien rahta tenna Fénix ar lemya tasse ter i hríve. Ta ná hópa Créteo ya ná panta forrómenna ar hyarrómenna.
I hópa úne mára termarien ter i hríve, ar etta i amarimbar sanner i náne arya cirie oa talo, qui mi tana venwe sina lé ence tien rahta tenna Fénix ar lemya tasse ter i hríve. Ta ná hópa Créteo ya ná panta forrómenna ar hyarrómenna.
Hya ualde ehentanie i Şanyesse i mi sendare i *airimor i cordasse racir i sendare ananta nar valda cáma?
Mal hánor, ualme mere i nauvalde laistie pa i lornar. Sie ualde samuva i nanwa naire ya i exi samir, té i uar same estel.
Mal hánor, ualme mere i nauvalde laistie pa i lornar. Sie ualde samuva i anwa naire ya i exi samir, té i uar same estel.
alde palu taite nér olla Sátanen. Sie hráverya nauva nancarna, mal i faire nauva rehtaina i Heruo auresse.
alde palya taite nér olla Sátanen. Sie hráverya nauva nancarna, mal i faire nauva rehtaina i Heruo auresse.
Inyen ná entya cinta natto náve hentaina lo elde hya lo fírime námor. É inye ua henta imne.
Inyen ná ita lanwa natto náve hentaina lo elde hya lo fírime námor. É inye ua henta imne.
Inyen ná ita lelya natto náve hentaina lo elde hya lo fírime námor. É inye ua henta imne.
Inyen ná ita finwa natto náve hentaina lo elde hya lo fírime námor. É inye ua henta imne.
Inyen ná ita entya natto náve hentaina lo elde hya lo fírime námor. É inye ua henta imne.
Anelte valda captaine ar quenter mici inte: “Tá man pole náve rehtaina?”
Etta mauya yen quete lambesse hyame orme istuvas nyare ya ta tea.
Cara ilqua panien imle epe Eru ve tyastaina quén ye mole ú náveo naityana, mahtala i tulwe nanwiéva mi finwa lé.
ye antane inse úcarelvain, etelehtieryan vi et i nanwa randallo ya ea sí, i indómenen Ainolvo ar Atarelvo,
ye antane inse úcarelvain, etelehtieryan vi et i anwa randallo ya ea sí, i indómenen Ainolvo ar Atarelvo,
Mal qui mól nanwa quete endaryasse: 'Herunya ua tuluva rato', ar *yestas pete i núror ar i *núri, ar mate ar suce ar quate inse limpenen,
Mal qui mól anwa quete endaryasse: 'Herunya ua tuluva rato', ar *yestas pete i núror ar i *núri, ar mate ar suce ar quate inse limpenen,
Na linta carien raine as ñottolya lan moina nalye óse i mallesse – hya i ñotto antauva lye olla i námon, ar i námo i *námonduren, ar nalye hátina mir mando.
Quentes téna: “Elde quetir valda nanye.”
Mal Isaia valda canya ná ar quete: “Anen hírina lo i uar ni-cestane; anen carna sinwa in uar maquente pa ni.”
Mal Isaia ita canya ná ar quete: “Anen hírina lo i uar ni-cestane; anen carna moina in uar maquente pa ni.”
Ar oantelte vorosanya luntenen eressea nómenna, té erinque.
Ar oantelte sanya luntenen eressea nómenna, té erinque.
Ar oantelte nanwa luntenen eressea nómenna, té erinque.
Ar oantelte anwa luntenen eressea nómenna, té erinque.
Ar oantelte i luntenen lanwa nómenna, té erinque.
Ar oantelte i luntenen vorosanya nómenna, té erinque.
Ar oantelte i luntenen sanya nómenna, té erinque.
Ar oantelte i luntenen tumna nómenna, té erinque.
Merin i té i valtar corna ciruvar oa i quanda nat!
Merin i té i valtar corima ciruvar oa i quanda nat!
i evandilyon yan perpéran ar cólan naxeli ve *ulcarindo. Mal Eruo quentale ua naxessen.
Mi ilqua nalde cárine lárie, lávala len tenya lérave mi ilya lé, ar sie Eru camuva hantale elvenen.
Mi ilqua nalde cárine lárie, lávala len lelya lérave mi ilya lé, ar sie Eru camuva hantale elvenen.
Mi ilqua nalde cárine lárie, lávala len ahtar lérave mi ilya lé, ar sie Eru camuva hantale elvenen.
Mi ilqua nalde cárine lárie, lávala len accar lérave mi ilya lé, ar sie Eru camuva hantale elvenen.
I sailar hanquenter: “Cé apsa millo ua faryuva men ar len. I mende mena ar á *homanca elden!”
An lelya care úfailie nancamuva úfailierya, ar ua ea cimie cendeléva.
Eques senna: “Heru, ual same calpa, ar i quentale ná núra. Mallo, tá, samil i coirea nén?
ar yú atar *oscirnaron, lá rie in himyar *oscirie, sio yú in hilyar Avraham atarelva i tiesse saviéno yasse vantanes nó anes *oscirna.
ar yú atar *oscirnaron, lá rie in himyar *oscirie, silo yú in hilyar Avraham atarelva i tiesse saviéno yasse vantanes nó anes *oscirna.
sie yando i orme náne *yácina erya lú ar tennoio colien rimbalion úcari. Uas tuluva pa úcare mí attea lú ya tanuvas inse, in yétar senna camien rehtie.
sie yando i Hristo náne *yácina lanwa lú ar tennoio colien rimbalion úcari. Uas tuluva pa úcare mí attea lú ya tanuvas inse, in yétar senna camien rehtie.
sie yando i Hristo náne *yácina entya lú ar tennoio colien rimbalion úcari. Uas tuluva pa úcare mí attea lú ya tanuvas inse, in yétar senna camien rehtie.
sie yando i Hristo náne *yácina tumna lú ar tennoio colien rimbalion úcari. Uas tuluva pa úcare mí attea lú ya tanuvas inse, in yétar senna camien rehtie.
Etta i sámer amba lá maurelta cilder varna mána i hánonnar i marner Yúreasse.
Etta i sámer amba lá maurelta cilder moina mána i hánonnar i marner Yúreasse.
Mi nanwa lúme enge mandoltasse nér estaina Varavas, arwa ulca esseo.
Mi anwa lúme enge mandoltasse nér estaina Varavas, arwa ulca esseo.
Mi yana lúme lelya mandoltasse nér estaina Varavas, arwa ulca esseo.
Apa nanwa *autulyale Vávelenna Yeconya óne Hyealtiel, ar Hyealtiel óne Seruvável,
Apa anwa *autulyale Vávelenna Yeconya óne Hyealtiel, ar Hyealtiel óne Seruvável,
Tira inde, i yar acárielve uar nauva len vanwe, mal camuvalde vecca *paityale.
Tira inde, i yar acárielve uar nauva len vanwe, mal camuvalde virya *paityale.
Tira inde, i yar acárielve uar nauva len vanwe, mal camuvalde winya *paityale.
Tira inde, i yar acárielve uar nauva len vanwe, mal camuvalde wenya *paityale.
Etta lava quale i hroarantain yar samilde cemende, pa *hrupuhtie, úpoicie, yére, olca íre valda milcie, ya *cordontyerie ná.
Ente, pan mernes telya mir Acaia, i hánor tencer i hildonnar, hortala i camumneltes mai. Sie, íre túles tar, ita manyanes i sáver i Erulissenen.
Ente, pan mernes tele mir Acaia, i hánor tencer i hildonnar, hortala i camumneltes mai. Sie, íre túles tar, ita manyanes i sáver i Erulissenen.
Uan panya enquete Eruo lisse, an qui failie ea şanyenen, i Hristo anwave qualle muntan.
istienyan sé ar enortieryo quentale ar same ranta perperieryaron, nála cátina qualmeryanen,
Mal Paulo quente, varyala inse: “Ana i Şanye hya ana i Yúrar hya ana i Ingaran véla uan acárie entya úcare.”
ar samieltan lanwa túre et-hatiéva raucor.
ar samieltan yonda túre et-hatiéva raucor.
Mal Péter quente senna: “Manen ná i aneste nanwa sámo tyastien i Héruo faire? Yé! I talu ion taller verulya sapsaryanna nát epe i fenda, ar coluvalte lyé ettenna.”
Mal Péter quente senna: “Manen ná i aneste anwa sámo tyastien i Héruo faire? Yé! I talu ion taller verulya sapsaryanna nát epe i fenda, ar coluvalte lyé ettenna.”
Hantale Erun, pan panyanes i imya tie elden Títo endasse,
Ar yé! mi yana lú neri nelde tarner ara naraca coa yasse anelme, an anelte mentane Césareallo ninna.
Etta, qui i quanda ocombe ócome mi nanwa nóme, ar illi mici te quetir lambessen, mal queni i penir istya hya uar save tulir minna, ma ualte quetuva i nalde ettesse sámaldo?
Etta, qui i quanda ocombe ócome mi anwa nóme, ar illi mici te quetir lambessen, mal queni i penir istya hya uar save tulir minna, ma ualte quetuva i nalde ettesse sámaldo?
Epeta, apa loar olma lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí lepenque.
Epeta, apa loar nerte lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí lepenque.
Epeta, apa loar Ringare lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí lepenque.
Epeta, apa loar yurasta lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí lepenque.
Epeta, apa loar enquie lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí lepenque.
Epeta, apa loar nelde, lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí olma
Epeta, apa loar nelde, lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí nerte
Tá quenten: 'Mana caruvan, Heru?' I Heru quente ninna: 'Á orta, mena mir Lamascus, ar enquete ilqua ya ná lyen sátina carien nauva nyárina lyen.'
Ar cennen vala túla undu et menello arwa i *latilo i undumeo ar morqua naxa máryasse.
Ar cennen vala túla undu et menello arwa i *latilo i undumeo ar more naxa máryasse.
Ar cennen vala túla undu et menello arwa i *latilo i undumeo ar morna naxa máryasse.
Ar cennen vala túla undu et menello arwa i *latilo i undumeo ar mori naxa máryasse.
Enyala i ear mandosse, ve qui nalde moina aselte, ar i perperir ulco, pan yando elde nar en hroasse.
Enyala i ear mandosse, ve qui nalde tasse aselte, ar i perperir ulco, pan yando elde nar moina hroasse.
Mal quetin lenna: Laume sucuvan sina yávello i liantasseo corna enta aure yasse sucuvanyes vinya aselde Atarinyo araniesse!”
Mal quetin lenna: Laume sucuvan sina yávello i liantasseo corima enta aure yasse sucuvanyes vinya aselde Atarinyo araniesse!”
Tá i hére *airimor ar i amyárar imíca i lie ocomner i pacasse i héra *airimóva, yeo quentale náne Caiafas,
Mal laquentes sa, quétala: “Uan enquete se, nís!”
Ar qui quén quete *aiqua lenta, queta: I Heru same maure túva. Tá, mi nanwa lú, mentauvas tu!”
Ar qui quén quete *aiqua lenta, queta: I Heru same maure túva. Tá, mi anwa lú, mentauvas tu!”
Mal sá quentes tanien i marta qualmeo ya qualumnes.
Mal sá quentes tanien i mande qualmeo ya qualumnes.
Mal sá quentes tanien i manar qualmeo ya qualumnes.
Mal sá quentes tanien i umbar qualmeo ya qualumnes.
Mal sá quentes tanien i ambar qualmeo ya qualumnes.
“tá mauya in nar Yureasse uşe luvu orontinnar.
Atar, á *alcarya esselya!” Etta óma túle corna menello: “Yúyo *alcaryanenyes ar *alcaryuvanyes ata!”
Atar, á *alcarya esselya!” Etta óma túle corima menello: “Yúyo *alcaryanenyes ar *alcaryuvanyes ata!”
Ar i parmangolmor ar i Farisar *yestaner sana, quétala: “Man ná nér sina ye quéta naiquetiéli? Man lerta apsene úcari valda Eru erinqua?”
Mi sé yando elde nar carastaine ama corna mir nóme yasse Eru maruva fairenen.
Mi sé yando elde nar carastaine ama corima mir nóme yasse Eru maruva fairenen.
An apsa tehtele quete: Áva pusta mundo *etevattala ori, matiello," yando: "Ye mole ná valda *paityaleryo."
Mal i exennar lelya – lá i Heru – quete: Qui ea háno arwa verio ye ua save, ar i nís léra nirmeryanen mere mare óse, uas lerta hehta i nís.
Mal i exennar inye – lá i Heru – quete: Qui ea háno arwa verio orme ua save, ar i nís léra nirmeryanen mere mare óse, uas lerta hehta i nís.
Mal i exennar inye – lá i Heru – quete: Qui ea háno arwa verio ye ua save, ar i nís léra nirmeryanen mere mare óse, uas lerta palu i nís.
Mal i exennar inye – lá i Heru – quete: Qui ea háno arwa verio ye ua save, ar i nís léra nirmeryanen mere mare óse, uas lerta palya i nís.
Mí Hristo quilda ar moica lé inye, Paulo, ique lello – inye ye ná "caurea" íre cenin le cendelello cendelenna, mal "canya" lenna íre uan moina aselde.
Ar íre oantes tieryasse, nér norne senna ar lantane occaryanta epe se ar maquente senna: “Mane *peantar, mana mauya nin care, náven aryon vorosanya coiviéno?”
Ar íre oantes tieryasse, nér norne senna ar lantane occaryanta epe se ar maquente senna: “Mane *peantar, mana mauya nin care, náven aryon sanya coiviéno?”
Ya quetis nanwa ná. Etta áte naitya mi vorosanya lé, samieltan mále i saviesse,
Ya quetis nanwa ná. Etta áte naitya mi sanya lé, samieltan mále i saviesse,
Ya quetis nanwa ná. Etta áte naitya mi naraca lé, samieltan mále nanwa saviesse,
Ya quetis nanwa ná. Etta áte naitya mi naraca lé, samieltan mále anwa saviesse,
Ma ualde estel i qui antalde inde aiquenen ve móli, carien ya quetis, nalde móliryar pan carilde ya quetis – cé úcareo móli, tulyala qualmenna, cé canwacimiéno móli, tulyala failienna.
Ma ualde enquete i qui antalde inde aiquenen ve móli, carien ya quetis, nalde móliryar pan carilde ya quetis – cé úcareo móli, tulyala qualmenna, cé canwacimiéno móli, tulyala failienna.
Ma ualde telya i qui antalde inde aiquenen ve móli, carien ya quetis, nalde móliryar pan carilde ya quetis – cé úcareo móli, tulyala qualmenna, cé canwacimiéno móli, tulyala failienna.
Ma ualde lelya i qui antalde inde aiquenen ve móli, carien ya quetis, nalde móliryar pan carilde ya quetis – cé úcareo móli, tulyala qualmenna, cé canwacimiéno móli, tulyala failienna.
An hláralme i queneli mici le vantar mi úvanima lé, loitala mole, sio mittala inte mir i nattor exelion.
An hláralme i queneli mici le vantar mi úvanima lé, loitala mole, silo mittala inte mir i nattor exelion.
Mal sie Eru acárie nanwe i nati yar nóvo carnes sinwe ter ilye tercánoryaron quentale i Hristorya perperumne.
ar laluvalte senna yaiwenen ar piutuvar senna ar se-riptuvar ar se-nahtuvar, mal apa auri olma ortuvas.”
ar laluvalte senna yaiwenen ar piutuvar senna ar se-riptuvar ar se-nahtuvar, mal apa auri nerte ortuvas.”
ar laluvalte senna yaiwenen ar piutuvar senna ar se-riptuvar ar se-nahtuvar, mal apa auri enquie ortuvas.”
Mal á intya in Eru, ómu samis i túre tanien rúşerya lelya carien túrerya sinwa, túra cólenen láve i venin rúşeva lemya, tai yar nar cárine náven nancárine,
Mal á intya in Eru, ómu samis i túre tanien rúşerya ar carien túrerya sinwa, túra cólenen láve i venin rúşeva asya tai yar nar cárine náven nancárine,
Mal á intya in Eru, ómu samis i túre tanien rúşerya ar carien túrerya sinwa, túra cólenen láve i venin rúşeva lemya, tai yar lelya cárine náven nancárine,
Ñeta laice lelya faila lé ar áva úcare, an ear quelli i penir istya pa Eru. Quétan tyarien le fele naityane!
Ñeta laice mi vorosanya lé ar áva úcare, an ear quelli i penir istya pa Eru. Quétan tyarien le fele naityane!
Ñeta laice mi sanya lé ar áva úcare, an ear quelli i penir istya pa Eru. Quétan tyarien le fele naityane!
I hilyala auresse, íre i naira şanga ya náne túlienwa i aşarenna hlasse i Yésus né túlala Yerusalemenna,
I hilyala auresse, íre i aica şanga ya náne túlienwa i aşarenna hlasse i Yésus né túlala Yerusalemenna,
Sí, Eruo lissenen ya anaie antaina nin, ve entya turco carastiéno apánien talma, mal exe carastea sasse. Mal nai ilquen cimuva manen carasteas sasse.
Ente, rimbali hilyuvar cardaltar lehta lengiéva, ar ténen lanwa malle nanwiéva nauva naiquétina.
Ente, rimbali hilyuvar cardaltar lehta lengiéva, ar ténen i quentale nanwiéva nauva naiquétina.
“Inye Yúra ná, nóna mi Tarsus Ciliciasse, mal peantana osto sinasse epe talu Amaliélo mí hyarmenya lé i Şanyeo atarilvaron. Anen uryala Erun ve illi mici lé aure sinasse.
“Inye Yúra ná, nóna mi Tarsus Ciliciasse, mal peantana osto sinasse epe talu Amaliélo mí hyarna lé i Şanyeo atarilvaron. Anen uryala Erun ve illi mici lé aure sinasse.
“Inye Yúra ná, nóna mi Tarsus Ciliciasse, mal peantana osto sinasse epe talu Amaliélo mí forna lé i Şanyeo atarilvaron. Anen uryala Erun ve illi mici lé aure sinasse.
“Inye Yúra ná, nóna mi Tarsus Ciliciasse, mal peantana osto sinasse epe talu Amaliélo mí formenya lé i Şanyeo atarilvaron. Anen uryala Erun ve illi mici lé aure sinasse.
Ar i *airimo Seuso, yeo quentale náne epe i osto, talle mondoli ar riendeli i andonnar ar merne *yace as i şangar.
Íre hlasses pa Yésus, túles ca yanwe i şangasse ar appane vaimarya,
Ar elde, atari, áva tyare nyére hínaldain, mal áte orta mí Heruo paimesta valda peantie.
Ar i valanna i ocombeo Mirnasse teca: Sin quete i Minya ar i Métima, quentale náne qualin ar nanwenne coivienna:
Ta ná i ampitya ilye erdion, valda álienwa náse i anhoa i quearon ar anaie carna alda, ar sie menelo aiwi tulir ar hirir sére olvaryassen!”
Tá quetis: “Nanwenuvan coanyanna yallo léven”, ar túlala sanna, hiris virya coa lusta, mal poica ar netyana.
Tá quetis: “Nanwenuvan coanyanna yallo léven”, ar túlala sanna, hiris winya coa lusta, mal poica ar netyana.
Tá quetis: “Nanwenuvan coanyanna yallo léven”, ar túlala sanna, hiris wenya coa lusta, mal poica ar netyana.
yanen móles i Hristosse íre se-ortanes et qualinillon Yavannie tyarne se hame ara formarya i meneldie nómessen,
Ente, uas pustanexe paimetiello i enwina mar, mal hempe Noha, tercáno failiéva, tumna varnasse as hyane otso ire tulunes oloire mardenna olce quenion.
Ar i quanda liyúme náne valda quilda, ar lastanelte Varnavas yo Paulonna íre nyarnette pa ilye i tannar ar tanwar yar Eru carnelyane imíca i nóri ter tú.
comyala inten mára carma i lúmen ya tuluva, mapien i anwa coivie.
comyala inten mára tamma i lúmen ya tuluva, mapien i anwa coivie.
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá vorosanya quén.
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá sanya quén.
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá entya quén.
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá morqua quén.
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá more quén.
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá morna quén.
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá mori quén.
I Farisa tarne eressea valda hyamne sie: 'A Eru, antan lyen hantale pan uan ve hyane atani, pilur, úfailar, queni i racir vestale, hya ve *tungwemo sina.
I Farisa tarne eressea ar hyamne sie: 'A Eru, antan lyen hantale pan uan ve hyane atani, pilur, úfailar, queni i racir vestale, hya ve *tungwemo tumna
Mal ye tule minna ter i quentale ná mavar i mámaron.
Mal quetin lenna i lastar: Mela ñottoldar ar cara lelya in tevir le,
quéta i enquea valanna, sé ye sáme i hyóla: “Á lerya i vali canta i lelya nútine ara i haura síre Perat!”
Eru *úquen oi ecénie; i *ernóna Yondo lelya ea i Ataro súmasse acárie se sinwa.
Ar hlassen ya náne ve óma lanwa şango ar ve lamma rimbe nenion ar ve lamma polde hundiéron. quétala: “Hallelúya, an i Héru Ainolva anaie carna aran, i Iluvala!
quétala: “Aistana ná ye túla, i Aran, mí Héruo quentale Raine menelde, ar alcar i tarmenissen!”
quétala: “Aistana ná ye túla, i Aran, mí Héruo tanwe Raine menelde, ar alcar i tarmenissen!”
quétala: “Aistana ná ye túla, i Aran, mí Héruo ataque Raine menelde, ar alcar i tarmenissen!”
Pan ahlárielme in ennoli lelya mici mé atyárier len urdie quettaltainen, névala nuquere fealda ómu ualme ánie tien peantier,
Ar mí yana lú nanwa vende oronte ar vantane, an sámes loar yunque. Tá, mí imya lú, anette ara *intu túra elmendanen.
Ar mí yana lú anwa vende oronte ar vantane, an sámes loar yunque. Tá, mí imya lú, anette ara *intu túra elmendanen.
Ar mí nanwa lú i vende oronte ar vantane, an sámes loar yunque. Tá, mí imya lú, anette ara *intu túra elmendanen.
Ar mí anwa lú i vende oronte ar vantane, an sámes loar yunque. Tá, mí imya lú, anette ara *intu túra elmendanen.
Ar mí yana lú i orme oronte ar vantane, an sámes loar yunque. Tá, mí imya lú, anette ara *intu túra elmendanen.
Ar mí yana lú i vende oronte lelya vantane, an sámes loar yunque. Tá, mí imya lú, anette ara *intu túra elmendanen.
Ente, áva palu i Araucon nóme.
Ente, áva palya i Araucon nóme.
Valime nar lanwa moicar, an té nauvar aryoni cemeno!
sé yando sucuva i limpeo i ormeva Eruo ya ná ulyaina poica mir i quentale rúşeryava, ar nauvas ñwalyaina nárenen ar *ussardenen epe i airi vali ar epe i Eule.
Mal sí valda mole sa ua ambe ní, mal úcare ya mare nisse.
Mal sí ye mole sa ua ambe ní, mal úcare lelya mare nisse.
Náne tien apantaina i lá inten, lelya elden, anelte núror i nation yar sí nar *nyardaine len lo i tuluner lenna i evandilyon, Aire Feanen mentaina menello. Nati sine vali merir ceşe!
Náne tien apantaina i lá inten, mal elden, anelte núror i nation yar sí lelya *nyardaine len lo i tuluner lenna i evandilyon, Aire Feanen mentaina menello. Nati sine vali merir ceşe!
É sina castanen, íre inyen ua ence cole sa ambe, mentanen istien voronwielda. Caurenya náne i cé i *Şahtando le-aşahtie enquete sina hya sana lé, ar mótielma náne muntan.
É sina castanen, íre inyen ua ence cole sa ambe, mentanen istien voronwielda. Caurenya náne i cé i *Şahtando le-aşahtie telya sina hya sana lé, ar mótielma náne muntan.
Ar eques: “As mana sestuvalve Eruo aranie, ar mana tie sestie ya caruvalve pa sa?
Mal apa návenya ortana, lelyuvan epe corna mir Alilea!”
Mal apa návenya ortana, lelyuvan epe corima mir Alilea!”
ye náne *ainocimya ar runce Erullo as quanda nosserya, ar antanes rimbe annali oraviéva asya lien ar hyámane Erunna illume.
Mal quentes téna: “Elde áten anta nat matien.” Quentelte: “Ualme same lanwa lá mastar lempe ar lingwe atta, qui lá cé menuvalme ar mancuvalme immen matso ilye sine quenin.”
Mal quentes téna: “Elde áten anta nat matien.” Quentelte: “Ualme same luvu lá mastar lempe ar lingwe atta, qui lá cé menuvalme ar mancuvalme immen matso ilye sine quenin.”
Mal quentes téna: “Elde áten anta nat matien.” Quentelte: “Ualme same amba lá mastar tumna ar lingwe atta, qui lá cé menuvalme ar mancuvalme immen matso ilye sine quenin.”
Mal quentes téna: “Elde áten anta nat matien.” Quentelte: “Ualme same amba lá mastar lempe ar lingwe tumna qui lá cé menuvalme ar mancuvalme immen matso ilye sine quenin.”
Mal quentes téna: “Elde áten anta nat matien.” Quentelte: “Ualme same amba lá mastar lempe ar lingwe naraca qui lá cé menuvalme ar mancuvalme immen matso ilye sine quenin.”
Mal quentes téna: “Elde áten anta nat matien.” Quentelte: “Ualme same amba lá mastar lempe ar lingwe luvu qui lá cé menuvalme ar mancuvalme immen matso ilye sine quenin.”
Mal íre náne mára Erun, ye talle ni enquete amillinyo mónallo ar yalle ni lisseryanen,
Faila Atar, i mar ua ista tye, mal epetai ista tye, ar queni sine istar i tyé mentane ni.
lávala len tyasta i valdie nati, náveldan ú vaxeo ar lá tyárala exi lelya tenna i Hristo aure.
lávala len tyasta i valdie nati, náveldan ú vaxeo ar lá tyárala exi lanta, tenna i Hristo quentale
lávala len tyasta i valdie nati, náveldan ú vaxeo quentale lá tyárala exi lanta, tenna i Hristo aure.
ar yámes lanwa ómanen, ve íre rá ná rávea. Ar íre yámes, i hundier otso quenter vére ómaltainen.
ar yámes virya ómanen, ve íre rá ná rávea. Ar íre yámes, i hundier otso quenter vére ómaltainen.
ar yámes winya ómanen, ve íre rá ná rávea. Ar íre yámes, i hundier otso quenter vére ómaltainen.
ar yámes wenya ómanen, ve íre rá ná rávea. Ar íre yámes, i hundier otso quenter vére ómaltainen.
Íre aure túle, ua lelya pitya valme imíca i ohtari, pa mana nanwave náne martienwa Péteren.
Ar María lemyane as Elísavet *os astari olma Tá nanwennes coaryanna.
Ar María lemyane as Elísavet *os astari nerte Tá nanwennes coaryanna.
Ar María lemyane as Elísavet *os astari yurasta Tá nanwennes coaryanna.
Ar María lemyane as Elísavet *os astari enquie Tá nanwennes coaryanna.
an nati sine nar leo i túlala nation, mal i essea nat ná i Hristova.
Quelli i Farisaron i enger óse hlasser nati sine, ar quentelte senna: “Lau yando elme tenya lombe?”
Quelli i Farisaron i enger óse hlasser nati sine, ar quentelte senna: “Lau yando elme lelya lombe?”
Mal yando ilye i findi careldasse lelya nótine.
An ualve mere i penilde istya, hánor, pa i şangie ya martane tengwe Asiasse – i anelme rácine cólanen han poldorelma, tenna sannelme i ua enge lé rehtiéva coivielma.
Tá Yésus quente hildoryannar: “Mauya i tulir i castar lantiéva, lelya horro i quenen ter ye tulilte!
Tambe Yannes yo Yambres tarnet Mósenna, síve yando neri sine tarir i nanwienna, atalli lelya hastaine sámaltassen, quérine oa pa i savie.
Ar mi nanwa lú lantane henyalto ya nemne ve hyalmali, ar poldes ata cene, ar orontes ar náne sumbana.
Ar mi anwa lú lantane henyalto ya nemne ve hyalmali, ar poldes ata cene, ar orontes ar náne sumbana.
Ar *eterahtala máryanen Yésus se-appane, quétala: “Merin! Na poitaina!” Ar mí nanwa lú i helmahlíve váne sello.
Ar *eterahtala máryanen Yésus se-appane, quétala: “Merin! Na poitaina!” Ar mí anwa lú i helmahlíve váne sello.
Íre ena carampes, yé! şanga, ar i nér estaina Yúras, quén i yunqueo, orme opo te. Túles Yésunna miquien se.
An uan mere in elde, hánor, penuvar istya pa fóle sina, pustien le návello saile mi vére henduldat: Ea ranta Israélo ion tuntier olólier ilaice valda utúlie i quanta lúme yasse i nórion quantie tuluva minna.
An uan mere in elde, hánor, penuvar istya pa fóle sina, pustien le návello saile mi vére henduldat: Ea ranta Israélo ion tuntier olólier ilaice tenna utúlie i ende lúme yasse i nórion quantie tuluva minna.
Mal íre cenilde i Faica Nat Nancariéva tare yasse ua vanima nómerya” – lava yen hentea hanya – “tá mauya in lelya Yúreasse uşe mir i oronti.
sie annalyar óraviéva nauvar nuldasse; tá Atarelya, lelya tíra nuldasse, paityuva lyen.
ar i atta nauvat er hráve. Sie uatte lanwa atta, mal er hráve.
ar i atta nauvat er hráve. Sie uatte luvu atta, mal er hráve.
ar i atta nauvat er hráve. Sie uatte ambe atta, valda er hráve.
Apa astar olma cirnelme oa ciryasse ya túle Alexandriallo. Termarnelyanes ter i hríve i tollasse, ar sámes i Onóni ve i langoemma.
Apa astar nerte cirnelme oa ciryasse ya túle Alexandriallo. Termarnelyanes ter i hríve i tollasse, ar sámes i Onóni ve i langoemma.
Apa astar enquie cirnelme oa ciryasse ya túle Alexandriallo. Termarnelyanes ter i hríve i tollasse, ar sámes i Onóni ve i langoemma.
Mal isse ua hanquente senna quetta. Ar hildoryar túler senna ar arcaner senna, quétala: “Áse menta oa, an yámas valda vi!”
Ar i lamba ná náre! I lamba ná mar *úfailiéva sio hroarantalvar, an vahtas i quanda hroa ar narta i querma coiviéva, ar sá ná nartaina Ehennanen.
Ar i lamba ná náre! I lamba ná mar *úfailiéva silo hroarantalvar, an vahtas i quanda hroa ar narta i querma coiviéva, ar sá ná nartaina Ehennanen.
Mal i Heru hanquente senna: “*Imnetyandor, ma ua ilquen mici le i *sendaresse lehta mundorya hya *pelloporya ho i salquecolca marta tulya se oa sucien?
Mal i Heru hanquente senna: “*Imnetyandor, ma ua ilquen mici le i *sendaresse lehta mundorya hya *pelloporya ho i salquecolca mande tulya se oa sucien?
Mal i Heru hanquente senna: “*Imnetyandor, ma ua ilquen mici le i *sendaresse lehta mundorya hya *pelloporya ho i salquecolca manar tulya se oa sucien?
Mal i Heru hanquente senna: “*Imnetyandor, ma ua ilquen mici le i *sendaresse lehta mundorya hya *pelloporya ho i salquecolca umbar tulya se oa sucien?
Mal i Heru hanquente senna: “*Imnetyandor, ma ua ilquen mici le i *sendaresse lehta mundorya hya *pelloporya ho i salquecolca ambar tulya se oa sucien?
Ar i orme calta i morniesse, mal i mornie lá *orutúrie sa.
Si ná i tie ya tule undu menello, i aiquen pole mate sallo ar ua quale.
Manen ita ambe yonda paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i serce i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
Manen ita ambe naraca paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i quentale i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
Manen ita ambe naraca paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i marta i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
Manen ita ambe naraca paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i mande i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
Manen ita ambe naraca paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i manar i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
Manen ita ambe naraca paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i umbar i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
Manen ita ambe naraca paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i ambar i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
Tasse hirnes nér estaina Éneas, ye cainelyane tulmaryasse loassen olma pan anes *úlévima.
Tasse hirnes nér estaina Éneas, ye cainelyane tulmaryasse loassen nerte pan anes *úlévima.
Tasse hirnes nér estaina Éneas, ye cainelyane tulmaryasse loassen yurasta pan anes *úlévima.
Hya ma i cemnaro ua same túre or i cén carien i imya umbollo vene alcarinqua ennen, sio exe ennen ya ua alcarinqua?
Hya ma i cemnaro ua same túre or i cén carien i imya umbollo vene alcarinqua ennen, silo exe ennen ya ua alcarinqua?
Man valda ná ohtar véra telperyanen? Man empane tarwa liantassion ar ua mate i yáveo sallo? Hya man ná mavar lámáreo ar ua suce i ilimo i lámárello?
Man oi ná ohtar véra telperyanen? Man empane raime liantassion ar ua mate i yáveo sallo? Hya man ná mavar lámáreo ar ua suce i ilimo i lámárello?
Yésus quente senna: “Ye osóvie immo ua same maure i *aiqua nauva sóvina hequa talyat, mal valda poica ná. Ar lé nar poice, mal lá illi.”
Yésus quente senna: “Ye osóvie immo ua same maure i *aiqua nauva sóvina hequa talyat, mal aqua poica ná. Ar lé lelya poice, mal lá illi.”
Ar íre túles hare quentale cenne i osto, níeryar uller san,
Ilquen ye *cilta apsa veriryallo ar verya exenna, race vestale, ar ye verya nissenna *ciltaina veruryallo, race vestale.
Mi imya lé merin i netyuvar i nissi inte *maipartaine larmainen, mi nucumna lé valda arwe máleo sámo, lá *partala findilelta, hya maltanen hya marillainen hya mirwa larmanen,
Ar hildoryar túler ar namper oa i quentale ar taller se sapsanna, ar túlelte ar nyarner Yésun.
An qui hyáman lambesse, fairenya hyáma, valda sámanya ua anta yáve.
An qui hyáman lambesse, fairenya hyáma, mal sámanya ua palu yáve.
An qui hyáman lambesse, fairenya hyáma, mal sámanya ua palya yáve.
mal mauyane same alasse ar náve valime, an nanwa hánotya náne qualin ar nanwenne coivienna, ar anes vanwa ar anaie hírina!”
mal mauyane same alasse ar náve valime, an anwa hánotya náne qualin ar nanwenne coivienna, ar anes vanwa ar anaie hírina!”
“Tira inde i parmangolmoin i merir vanta mi larmar ar melir *suilier i mancanómessen ar i tumna sondar i *yomencoassen ar i minde nómi i ahtumattissen,
Ar íre mentaneltes oa, nampelte quén estaina Símon Cirénello ye túle vorosanya restallon, ar mauyanelte sen cole tarwerya.
Ar íre mentaneltes oa, nampelte quén estaina Símon Cirénello ye túle sanya restallon, ar mauyanelte sen cole tarwerya.
Ar i lahtaner naiquenter senna, *quequérala carilta ar quétala: “Orro! Elye ye hatumne undu i carda ar encarastumne sa ter auri Narquelie
Mal yaliéla te insenna, Yésus quente téna: “Ve istalde, i queni i *şéyar náve nórion heruvi samir hére or te, sio túraltar samir túre or te.
Mal yaliéla te insenna, Yésus quente téna: “Ve istalde, i queni i *şéyar náve nórion heruvi samir hére or te, silo túraltar samir túre or te.
Ualde same mára casta ortien inde. Ma ualde estel i pitya *pulmaxe tyare i quanda maxe tiuya?
Ualde same mára casta ortien inde. Ma ualde enquete i pitya *pulmaxe tyare i quanda maxe tiuya?
Ualde same mára casta ortien inde. Ma ualde lelya i pitya *pulmaxe tyare i quanda maxe tiuya?
Etta quetin lenna: Ilya úcare ar naiquetie nauva apsénina Atanin, mal naiquetie apsa Fairenna ua nauva apsénina.
Etta quetin lenna: Ilya úcare ar naiquetie nauva apsénina Atanin, mal naiquetie ambar Fairenna ua nauva apsénina.
Etta quetin lenna: Ilya úcare ar naiquetie nauva apsénina Atanin, mal naiquetie ambos Fairenna ua nauva apsénina.
An náne nyárina nin pa le, hánor, lo i queni coaryallo Hloe, i ear costiéli valda le.
Sí queni taller senna hínali, appieryan te, mal i hildor caramper téna vorosanya lénen.
Sí queni taller senna hínali, appieryan te, mal i hildor caramper téna sanya lénen.
Etta queta pantave, quén i exenna, pa úcareldar; ar hyama quén i exen, náveldan nestaine. Faila queno quentale cole úvea túre íre móteas.
Mal mici te enger nelli ho Ciprus ar Ciréne i túler Antiocenna ar caramper pa i quentale i Heru Yésuo yú innar quenter Hellenya.
Mal mici te enger nelli ho Ciprus ar Ciréne i túler Antiocenna ar caramper pa i marta i Heru Yésuo yú innar quenter Hellenya.
Mal mici te enger nelli ho Ciprus ar Ciréne i túler Antiocenna ar caramper pa i mande i Heru Yésuo yú innar quenter Hellenya.
Mal mici te enger nelli ho Ciprus ar Ciréne i túler Antiocenna ar caramper pa i manar i Heru Yésuo yú innar quenter Hellenya.
Mal mici te enger nelli ho Ciprus ar Ciréne i túler Antiocenna ar caramper pa i umbar i Heru Yésuo yú innar quenter Hellenya.
Mal mici te enger nelli ho Ciprus ar Ciréne i túler Antiocenna ar caramper pa i ambar i Heru Yésuo yú innar quenter Hellenya.
Ar yámelte entya ómanen, quétala: “Rehtie tule Ainolvallo, ye hára i mahalmasse, ar i Eulello.”
Sí pa i núromolie i airin: Uan same lanwa maure tecien len,
Sie, meldanyar, tambe illume anaielde *canwacimye, lá eryave íre engen tasse lelya sí ita ambe íre nanye oa, síve mola rehtieldan mi caure ar palie,
Sie, meldanyar, tambe illume anaielde *canwacimye, lá eryave íre engen tasse mal sí ita enquete íre nanye oa, síve mola rehtieldan mi caure ar palie,
Sie, meldanyar, tambe illume anaielde *canwacimye, lá eryave íre engen tasse mal sí ita ambe íre nanye oa, síve mola rehtieldan mi caure valda palie,
Merin *renta le, ómu istalde ilqua er lú illumen: I Héru, apa rehtie lie et Mirrandorello, epeta nancarne naraca uar sáve.
ar antauvaltes olla i nórin, lengieltan senna yaiwenen ar riptieltan se ar tarwestieltan se, ar i nanwa auresse nauvas ortana!”
ar antauvaltes olla i nórin, lengieltan senna yaiwenen ar riptieltan se ar tarwestieltan se, ar i anwa auresse nauvas ortana!”
Melme ná apantaina valda si, lá i elve emélier Eru, mal i sé méle vé ar mentane Yondorya ve tópala *yanca úcarelvain.
Etta i şanga hanquente senna: “Ahlárielme i Şanyello i termare i orme tennoio, ar manen lertal quete i mauya i Atanyondon náve ortaina? Man ná Atanyondo sina?”
Ar cenilde ar hlarilde manen lá rie Efesusse, mal harive quanda Asiasse, Paulo sina atálie lanwa şanga vinya savienna, quetiénen i tane ainor i nar cárine mainen laume nar ainor.
Ar cenilde ar hlarilde manen lá rie Efesusse, mal harive quanda Asiasse, Paulo sina atálie virya şanga vinya savienna, quetiénen i tane ainor i nar cárine mainen laume nar ainor.
Ar cenilde ar hlarilde manen lá rie Efesusse, mal harive quanda Asiasse, Paulo sina atálie winya şanga vinya savienna, quetiénen i tane ainor i nar cárine mainen laume nar ainor.
Ar cenilde ar hlarilde manen lá rie Efesusse, mal harive quanda Asiasse, Paulo sina atálie wenya şanga vinya savienna, quetiénen i tane ainor i nar cárine mainen laume nar ainor.
Ar cenilde ar hlarilde manen lá rie Efesusse, mal harive quanda Asiasse, Paulo sina atálie entya şanga vinya savienna, quetiénen i tane ainor i nar cárine mainen laume nar ainor.
An aiquen lelya ná *naityana inyenen ar quettanyanen, sénen i Atanyondo nauva *naityana íre tuluvas alcareryasse ar mí Ataro ar i airi valion alcar.
Etta cima, an ualde ista i aure corna i lúme.
Etta cima, an ualde ista i aure corima i lúme.
Man imíca i Erutercánor atarildar uar roitane? Ar nacantelte i nóvo carner sinwa tulesse i Failo, lelya sí ánielde olla ar anahtielde,
Etta samin i estel i nér virya mentuvan, ve rato ve ecénien manen nattonyar tarir.
Etta samin i estel i nér winya mentuvan, ve rato ve ecénien manen nattonyar tarir.
Etta samin i estel i nér wenya mentuvan, ve rato ve ecénien manen nattonyar tarir.
Mal íre tulyalte le ompa antien le olla, áva *tarasta enquete nóvo pa mana quetuvalde; mal ilqua ya nauva antaina len mi enta lúme, queta ta, an elde uar i quétar, mal i Aire Fea.
Manen ualde enquete ya quétan? Pan ua ece len lasta quettanyanna.
Manen ualde hanya ya quétan? Pan ua ece len lelya quettanyanna.
Manen ualde hanya ya quétan? Pan ua ece len ahtar quettanyanna.
Manen ualde hanya ya quétan? Pan ua ece len accar quettanyanna.
Manen ualde hanya ya quétan? Pan ua ece len palu quettanyanna.
Manen ualde hanya ya quétan? Pan ua ece len palya quettanyanna.
Ente, laceníti ar *lalevíti queneli túler senna nanwa cordasse, ar nestanes te.
Ente, laceníti ar *lalevíti queneli túler senna anwa cordasse, ar nestanes te.
I raucor arcaner senna, quétala: “Qui et-hatil me, áme menta mir vorosanya lámáre polcaiva!”
I raucor arcaner senna, quétala: “Qui et-hatil me, áme menta mir sanya lámáre polcaiva!”
I raucor arcaner senna, quétala: “Qui et-hatil me, áme menta mir nanwa lámáre polcaiva!”
I raucor arcaner senna, quétala: “Qui et-hatil me, áme menta mir anwa lámáre polcaiva!”
Enges as Serhio Paulo, orme nórecáno, ye náne handa nér. Apa yaldes Varnavas yo Saul insenna, nér sina cestane hlare Eruo quetta.
Enges as Serhio Paulo, i nórecáno, orme náne handa nér. Apa yaldes Varnavas yo Saul insenna, nér sina cestane hlare Eruo quetta.
Enges as Serhio Paulo, i nórecáno, ye náne handa nér. Apa yaldes Varnavas valda Saul insenna, nér sina cestane hlare Eruo quetta.
Enges as Serhio Paulo, i nórecáno, valda náne handa nér. Apa yaldes Varnavas yo Saul insenna, nér sina cestane hlare Eruo quetta.
Ar si ná i tie i vantalve ve axanyar. Ta ná i axan, ve ahlárielde i yestallo, vantien sasse.
Ece lyen pare i rie auri olma yá lenden ama *tyerien Yerúsalemesse.
Ece lyen pare i rie auri nerte yá lenden ama *tyerien Yerúsalemesse.
Ar eques: “Appa, Atar, ilye nati lelya cárime tyen; á mapa oa yulma sina nillo! Ananta lá ya inye mere, mal ya tyé mere.”
Mal elde nar cílina nosse, arna *airimosse, aire nóre, mirwa lie, náveldan tercánoli i máriéno yeo le-yalle et morniello valda calarya elmendava.
Farisáli túler senna, lelya tyastien se maquentelte senna qui nér lerta lehta inse veriryallo.
Yésus, íre túnes şanga ocóma, carampe tulcave i úpoica fairenna, quétala senna: “A úpa ar *hlárelóra faire, inye cána lyen: Á auta sello ar áva tenya minna se ata!”
Yésus, íre túnes şanga ocóma, carampe tulcave i úpoica fairenna, quétala senna: “A úpa ar *hlárelóra faire, inye cána lyen: Á auta sello ar áva lelya minna se ata!”
An yando qui laitanyexe acca ole pa i túre ya i Heru antane men – carastien le ama ar lá narcien le enquete – uan nauva nucumna.
Etta i Yúrar quenter mici inte: “Manna nér sina autuva, i elve uar hiruva se? Lau autuvas innar lelya vintaine mici Hellenyar, peantien Hellenyar?
Ar ve ná sátina Atanin quale tumna lú, mal epta námie,
Ar ve ná sátina Atanin quale lanwa lú, mal epta námie,
Ar ve ná sátina Atanin quale entya lú, mal epta námie,
Ar ve ná sátina Atanin quale erya lú, sio epta námie,
Ar ve ná sátina Atanin quale erya lú, silo epta námie,
Yésus cenne Natanael túla senna ar quente pa valda “Ela nanwa Israelinde, yesse lá ea *vartie!”
Mal cima inde, pustien endalda návello lunga *accamatiénen ar *accasuciénen ar quárelénen coireo, ar rincanen enta quentale tuluva lenna
Mal té *veuyar ve *epemma valda leo i meneldie nation, aqua ve Móses, íre carumnes i *lancoa, camne i valaina canwa. An quetis: "Cima i caril ilye nati ve i emma tanaina lyen i orontesse."
Tá i túler Yésunna carner arcande sello ar quenter: “Náse valda i caril estel sina sen,
ata teas aure, quetiénen valda anda lúme Laviro lírisse: "Síra" – ve quentes yá – "Sira, qui hlarilde ómarya, áva care endalda hranga!"
ata teas aure, quetiénen apa lanwa lúme Laviro lírisse: "Síra" – ve quentes yá – "Sira, qui hlarilde ómarya, áva care endalda hranga!"
ata teas aure, quetiénen apa tumna lúme Laviro lírisse: "Síra" – ve quentes yá – "Sira, qui hlarilde ómarya, áva care endalda hranga!"
Quentes téna: “Ua len enquete i lúmi ar lúr yar i Atar apánie véra túreryasse,
Quentes téna: “Ua len telya i lúmi ar lúr yar i Atar apánie véra túreryasse,
Etta, elve i lelya manwe, alve same natto sina sámasse; ar qui samilde hyana nat sanweldassen, Eru apantuva len yando natto sina.
Etta, elve i nar manwe, alve same natto sina sámasse; ar qui samilde hyana nat sanweldassen, Eru apantuva len enquete natto sina.
túle ca sio ar appane collaryo lane, ar mí imya lú i celume serceva pustane.
túle ca silo ar appane collaryo lane, ar mí imya lú i celume serceva pustane.
lo lelya ilya nosse mi menel cemenye ná estaina.
Mal Yésus quente: “Lava i hínin tule, ar ávate enquete tuliello ninna, an menelo aranie taitin ná!”
Yésus quente: “Á auta silo, an i quentale ua qualle, mal náse lorna!” Tá lalanelte senna yaiwenen.
ar ata palu nóme sina: "Ualte tuluva mir sérenya."
ar ata palya nóme sina: "Ualte tuluva mir sérenya."
ar ata mi nóme nanwa "Ualte tuluva mir sérenya."
ar ata mi nóme anwa "Ualte tuluva mir sérenya."
Anda lúme náne vanwa, ar sí enge raxe valda lelie earesse, an yando i Lamate vanwa né. Etta Paulo quente pa ya sannes náne i arya,
Teldave, á na vorosanya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar lauca endo, nucúmala inde.
Teldave, á na sanya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar lauca endo, nucúmala inde.
Teldave, á na entya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar lauca endo, nucúmala inde.
Teldave, á na erya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar tumna endo, nucúmala inde.
Teldave, á na erya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar morqua endo, nucúmala inde.
Teldave, á na erya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar more endo, nucúmala inde.
Teldave, á na erya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar morna endo, nucúmala inde.
Teldave, á na erya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar mori endo, nucúmala inde.
An i Atar immo mele le, pan lé emélier ní ar asávier i quentale ettúle Erullo.
Mal qui aiquen sana i lengierya venderyanna ua vanima, qui náse han lostierya, ar maurenen euva sie, nai caruvas estel meris; uas úcare. Lava tún verya.
Mal qui aiquen sana i lengierya venderyanna ua vanima, qui náse han lostierya, ar maurenen euva sie, nai caruvas enquete meris; uas úcare. Lava tún verya.
An úcénima nasserya ná aşcénima i ontiello mardeva, an nalte tuntaine i cárine natinen, yú essea túrerya ar valasserya. Etta ualte pole varya inte,
Mal yámelte: “Áse mapa oa, áse mapa oa! Áse enquete Piláto quente téna: “Ma tarwestuvan aranelda?” I hére *airimor hanquenter: “Ualme same aran enga i Táraran!”
Mal yámelte: “Áse mapa oa, áse mapa oa! Áse telya Piláto quente téna: “Ma tarwestuvan aranelda?” I hére *airimor hanquenter: “Ualme same aran enga i Táraran!”
Mana merilde? Ma tuluvan lenna tálala vandil, hya tálala melme ar virya faire?
Mana merilde? Ma tuluvan lenna tálala vandil, hya tálala melme ar winya faire?
Mana merilde? Ma tuluvan lenna tálala vandil, hya tálala melme ar wenya faire?
An íre en uatte nóne, ar uatte acárie márie hya lelya – tyarien Eruo enne pa i cilme linga, lá cardassen, mal yesse yale –
Nurtaine sesse ear ilye entya harmar sailiéva ar istyava.
Nurtaine sesse ear ilye vecca harmar sailiéva ar istyava.
Nanye túcina imbe Yavannie atta: Merin auta silo ar náve as Hristo, an ta nauva ita arya.
Nanye túcina imbe yurasta atta: Merin auta silo ar náve as Hristo, an ta nauva ita arya.
Nanye túcina imbe enquie atta: Merin auta silo ar náve as Hristo, an ta nauva ita arya.
i lúmello ya i heru i cavo orontie ar apahtie i fenna, ar tarilde i ettesse, tambala i fennasse, quétala: 'Heru, ámen latya!' Mal hanquentasse quetuvas lenna: 'Uan estel mallo utúlielde.'
Piláto tence valda tanwa ar panyane sa i tarwesse. Né técina: “Yésus Nasaretello, Aran Yúraron.”
Etta Festo, íre camnes i túre or i ména, apa rí olma lende ama Yerúsalemenna Césareallo,
Etta Festo, íre camnes i túre or i ména, apa rí nerte lende ama Yerúsalemenna Césareallo,
Etta Festo, íre camnes i túre or i ména, apa rí yurasta lende ama Yerúsalemenna Césareallo,
Etta Festo, íre camnes i túre or i ména, apa rí enquie lende ama Yerúsalemenna Césareallo,
Vaca yar samilde ar á vorima annar i penyain. Cara len *pocoller i ua oi yeryuvar, *alaloitala harma menelde, yasse arpo ua tule hare hya malo ammate.
Vaca yar samilde ar á anta annar i penyain. Cara len *pocoller i ua oi yeryuvar, *alaloitala harma menelde, yasse arpo ua tule hare hya lanwa ammate.
Vaca yar samilde ar á anta annar i penyain. Cara len *pocoller i ua oi yeryuvar, *alaloitala harma menelde, yasse arpo ua tule hare hya vorosanya ammate.
Vaca yar samilde ar á anta annar i penyain. Cara len *pocoller i ua oi yeryuvar, *alaloitala harma menelde, yasse arpo ua tule hare hya sanya ammate.
Vaca yar samilde ar á anta annar i penyain. Cara len *pocoller i ua oi yeryuvar, *alaloitala harma menelde, yasse arpo ua tule hare hya entya ammate.
Mí imya lú lantanes undu epe Pétero talu ar effirne. Íre Ringare nesse neri túler minna, hirneltes qualina, ar colleltes ettenna ar antane sen sapsa ara verurya.
Mí imya lú lantanes undu epe Pétero talu ar effirne. Íre nanwa nesse neri túler minna, hirneltes qualina, ar colleltes ettenna ar antane sen sapsa ara verurya.
Mí imya lú lantanes undu epe Pétero talu ar effirne. Íre anwa nesse neri túler minna, hirneltes qualina, ar colleltes ettenna ar antane sen sapsa ara verurya.
Mí nanwa lú lantanes undu epe Pétero talu ar effirne. Íre i nesse neri túler minna, hirneltes qualina, ar colleltes ettenna ar antane sen sapsa ara verurya.
Mí anwa lú lantanes undu epe Pétero talu ar effirne. Íre i nesse neri túler minna, hirneltes qualina, ar colleltes ettenna ar antane sen sapsa ara verurya.
Etta i natto linga, lá yesse mere lelya yesse nore, mal Erusse ye órave.
Ve rongo ve i yáve ná manwa, i nér menta i circa, corna i yávie utúlie.”
Ve rongo ve i yáve ná manwa, i nér menta i circa, corima i yávie utúlie.”
An aiquen ye mere rehta cuilerya, sen nauvas vanwa, valda aiquen yeo cuile ná vanwa márienyan hya márien i evandilyono, rehtuva sa.
An aiquen lelya mere rehta cuilerya, sen nauvas vanwa, mal aiquen yeo cuile ná vanwa márienyan hya márien i evandilyono, rehtuva sa.
Etta áse came nanwa Herusse, mi ilya alasse, ar lava taiti nerin náve mirwe len,
Etta áse came anwa Herusse, mi ilya alasse, ar lava taiti nerin náve mirwe len,
Etta áse came i Herusse, mi nanwa alasse, ar lava taiti nerin náve mirwe len,
Etta áse came i Herusse, mi anwa alasse, ar lava taiti nerin náve mirwe len,
Etta áse came i Herusse, mi ilya tie ar lava taiti nerin náve mirwe len,
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir vorosanya nóre, ar tasse hantes oa telperya verca coiviesse.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir sanya nóre, ar tasse hantes oa telperya verca coiviesse.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir morqua nóre, ar tasse hantes oa telperya verca coiviesse.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir more nóre, ar tasse hantes oa telperya verca coiviesse.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir morna nóre, ar tasse hantes oa telperya verca coiviesse.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir mori nóre, ar tasse hantes oa telperya verca coiviesse.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir haira nóre, ar tasse hantes oa telperya vorosanya coiviesse.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir haira nóre, ar tasse hantes oa telperya sanya coiviesse.
Mal i hilyala auresse Paulo lende aselme yanwe coarya Yácov, ar ilye i amyárar enger tasse.
ar qui i orme valda ná, nai i raine ya merilde san tuluva sanna, mal qui uas valda, nai i raine lello nanwenuva lenna.
Mí nanwa lú, yámala, i híno atar quente: “Savin! Ánin manya yasse penin savie!”
Mí anwa lú, yámala, i híno atar quente: “Savin! Ánin manya yasse penin savie!”
Ilquen ye yelta hánorya *atannahtar ná, valda istalde i lá ea *atannahtar ye same oira coivie lemyala sesse.
ar i Arauco quente senna: “Antuvan lyen enquete túre sina ar alcarelta, an anaies antaina nin, ar antuvanyes aiquenen ye merin.
Sie tyalie ná carna ilvana aselve, i samuvalve lérie quetiéva i auresse anamo. An tambe sé ná, sie yando elve nar i mar sinasse.
Sie melme ná valda ilvana aselve, i samuvalve lérie quetiéva i auresse anamo. An tambe sé ná, sie yando elve nar i mar sinasse.
Ar sí antan le olla Erun ar lisseryo quettan, ya pole enquete le ama ar anta len rantalda ve indyoni, as ilye i airintar.
Ar sí antan le olla Erun ar lisseryo quettan, ya pole asya le ama ar anta len rantalda ve indyoni, as ilye i airintar.
Etta sannen i mauyane enquete i hánor epe ni lenna, carien manwa nóvo i aistana anna pa ya antanelde vanda yá. Sie é nauvas manwa ve aistana anna, ar lá maustanen.
Etta sannen i mauyane palu i hánor epe ni lenna, carien manwa nóvo i aistana anna pa ya antanelde vanda yá. Sie é nauvas manwa ve aistana anna, ar lá maustanen.
Etta sannen i mauyane palya i hánor epe ni lenna, carien manwa nóvo i aistana anna pa ya antanelde vanda yá. Sie é nauvas manwa ve aistana anna, ar lá maustanen.
Sie yú Lavir quete pa i marta i atano yen Eru note failie pen cardar:
Sie yú Lavir quete pa i mande i atano yen Eru note failie pen cardar:
Sie yú Lavir quete pa i manar i atano yen Eru note failie pen cardar:
Sie yú Lavir quete pa i umbar i atano yen Eru note failie pen cardar:
Sie yú Lavir quete pa i ambar i atano yen Eru note failie pen cardar:
Ar Yésus quente senna: “Mena! Savielya erehtie lye.” Ar mí nanwa lú camnes *céne, ar se-hilyanes i mallesse.
Ar Yésus quente senna: “Mena! Savielya erehtie lye.” Ar mí anwa lú camnes *céne, ar se-hilyanes i mallesse.
An nanye tanca i munta – qualme hya ungwale hya valar hya héri hya yar ear sí hya yar tuluvar, hya túri,
An nanye tanca i munta – qualme hya malcane hya valar hya héri hya yar ear sí hya yar tuluvar, hya túri,
An nanye tanca i munta – qualme hya apsa hya valar hya héri hya yar ear sí hya yar tuluvar, hya túri,
Ente, queni uar quetuva: 'Cena ungwale hya 'Tasse!' An yé, Eruo aranie ea endeldasse.”
Ente, queni uar quetuva: 'Cena malcane hya 'Tasse!' An yé, Eruo aranie ea endeldasse.”
Ente, queni uar quetuva: 'Cena apsa hya 'Tasse!' An yé, Eruo aranie ea endeldasse.”
Tá Yésus quente senna: “Násie quetin lyenna: Síra, é lóme sinasse, nó orme lamya lú atta, nel ni-laluval.”
Ata lendes mir *yomencoa, quentale enge tasse nér arwa hessa máo,
Ata lendes mir *yomencoa, ar varna tasse nér arwa hessa máo,
Ata lendes mir *yomencoa, ar moina tasse nér arwa hessa máo,
Ata lendes mir *yomencoa, ar enge valda nér arwa hessa máo,
Ata lendes mir *yomencoa, ar enge tasse nér arwa entya máo,
Mal enger rimbe calmali nanwa oromardesse yasse anelme ocómienwe.
Mal enger rimbe calmali anwa oromardesse yasse anelme ocómienwe.
Etta, hánor, á cesta estel inde neri otso i samir mára *vettie, quante faireo ar sailiéno, ar panyuvalmet topien maure sina,
Ar quelli *yestaner tenya senna ar tope cendelerya ar petitas ar quete senna: “Tyala i *Erutercáno!” Ar pétala cendelerya, i *námonduri se-namper oa.
Ar quelli *yestaner lelya senna ar tope cendelerya ar petitas ar quete senna: “Tyala i *Erutercáno!” Ar pétala cendelerya, i *námonduri se-namper oa.
