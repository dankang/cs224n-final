quétala taura ómanen: “Ruca Erullo ar ásen anta alcar, an i lúme námieryo utúlie, ar *tyera ye carne menel cemenye ar ear ar ehteler nenwa.”
Epeta quentes exenna: 'Ar elye, mana i nonwe rohtalyo?' Quentes: 'Cor-lestar tuxa oriva.' Quentes senna: 'Á mapa hyalinelyar ar teca *toloquean!'
Ar Yésus immo, molieryo yestasse, sáme *os loar *nelequean. Anes, mo intyane, yondorya Yósef, yondorya Heli,
lá ve i vére ya carnen as atariltar i auresse ya nampen málta ar te-tulyane et Mirrandorello. An té uar lemyane vérenyasse, ar ualte valdie nin ambe, quete i Héru.
Ar rimbali túler senna, ar quentelte: “Yoháno ua carne erya tanna, mal ilqua ya Yoháno quente pa nér sina nanwa né.”
Qui erérielme nati i faireva elden, ma ná hoa nat qui elme *cirihtuvar nati i hráveva lello?
Ananta, qui hurunyanen Eruo nanwie olólie ambe minda, alcareryan, manen ná in inye en ná námaina ve úcarindo?
Íre hé quente, “I etteleallon,” Yésus quente senna: “Tá i yondor é nar lére.
Ar túle íre nanwennes, apa camie i aranie, i cannes i yalumnelte senna móli sine in antanes i telpe, istieryan mana ñentelte mancalénen.
Mal qui colis neceli ar *nelmasirpi, nas nótina ve munta ar hare návenna hunta, ar i mettasse nas urtaina.
Íre helpilde lamate, áva na ve *imnatyandor, i carir cendelelta úvanima carien lamatelta cénima atanin. Násie quetin lenna: Cámalte quanda *paityalelta.
Lelyala talo cennes yú exe atta yet nánet hánor, Yácov Severaion ar Yohannes hánorya, i luntesse as Severai ataretta, envinyatála rembettar – ar yaldeset.
Ar nampeses formaryasse ar ortane se. Mí imya lú talluniryat ar i oxor imbi tál ar telco oller tulce,
i nar Israelyar, i náner cíline ve híni ar samir i alcar ar i véri ar i antave i Şanyeva ar i núromolie ar i vandar,
Ómu ear rimbe nostaler lambeleron i mardesse, ua ea erya nostale ya tea munta.
ar ilya hráve cenuva Eruo rehtie.”
Lau carnen úcare íre carnenyexe nalda ortien lé, pan úpaityana carnen Eruo evandilyon sinwa?
Tá i namper Yésus tulyane se oa Caiafas i héra *airimonna, íre i parmangolmor ar i amyárar ocomner.
Sí íre yentes cennes i lárear panyea annaltar i harwessen.
Quetin lenna: Nér sina lende marda nála faila lá i exe, an ilquen ye orta inse nauva nucumna, mal sé ye nucume inse nauva ortaina.”
Ar i hína alle ar náne carna taura fairesse, ar anes i erumessen tenna i aure yasse tanumnes inse Israélen.
I mára mahtie amahtien, onórien i norie i mettanna, ehépien i savie.
náne quétina senna: “I amyára nauva mól i amnessan.”
Ortírie sina panyan lyesse, Timoşeo hínanya, i apaceninen yar tulyaner lyenna; sie polil tainen mahta i mára mahtiesse,
apa tirie poica lengielda ar i áya ya tanalde.
An yá anelde mornie, mal sí nalde cala, i Herusse. Á vanta ve híni calo,
Hepilde auri ar astar ar lúmi ar loar.
Ente, qui aquálielve as Hristo, savilve i yú coituvalve óse.
“Ánin tana lenár. Mano emma ar tecie samis?” Quentelte: “I ingarano.”
I Héru ui lenca *amaquatiesse vandarya, ve quelli notir lencie, mal tanas cóle len, pan uas mere i aiquen nauva nancarna, mal i illi hiruvar inwis.
ar Ása óne Yóhyafat, ar Yóhyafat óne Yóram, ar Yóram óne Ussia,
an anelde mancaine nonwen. Etta alde anta alcar Erun hroaldanen!
ar nelesta earo ahyane mir serce, nelesta i onnaron earesse qualle, i sámer coivie; ar nelesta i ciryaron náne nancarna.
Merilme pusta aiquen quetiello ulco pa me pa i anna ya samilme hepielmasse,
hínanyar, in ata samin naiceli tenna Hristo nauva cátina lesse.
I Atanyondo é auta, ve ná técina pa sé, mal horro sana atanen lo ye i Atanyondo ná antaina olla! Arya náne sana atanen qui únes nóna.”
Ar i usque ñwalmelto orta ama tennoio ar oi, ar auresse yo lómisse ualte same sére, i *tyerir i hravan ar emmarya, ar aiquen ye came i tehta esseryo.
Ar óma náne hlárina et menello: “Tyé Yondonya, i melda; pa tyé asánien mai.”
An Ontie náne panyana nu úaşea sóma, lá véra şelmaryanen mal lo ye panyane sa nu ta, ómu arwa estelo:
Áva cole *pocolle, hya poco matson, hya hyapat; ar áva *suila aiquen i mallesse.
I Heruo márien á panya inde nu ilya ontie atanion – cé nu i aran, ve inga quén,
An mana i Tehtele quete? “Avraham sáve i Hérusse, ar ta náne nótina sen ve failie.”
Yé, si ná i neldea lú yasse nanye manwa tulien lenna. Ar uan nauva cólo, an uan cesta ya samilde, mal cestan lé. An híni uar same rohta care haura ontarultant, mal ontaru hínaltain.
Ar qui *yutyal queni illon samil estel camiéva, manen ta nauva aşea len? Yando úcarindor *yutyar úcarindor *nancamien i imya nonwa.
Á mapa ya *lyenya ná ar mena! Merin anta métima sinan i imya ya antan lyen.
Valima nalde íre atani tevir le, ar íre queriltexer lello ar naityar le ar et-hatir esselda ve ulca, i Atanyondo márien.
an ilquen ye suce ilimo ua maite pa i quetta failiéva, an náse hína.
ar lendes oa ar quente as i hére *airimor ar i cordahestor pa manen antumneses olla tien.
An ve i nís ná et i nerello, sie yando i nér ea i nissenen, mal ilye nati nar et Erullo.
Ar nampes yulma, ar apa antave hantale sa-antanes tien, quétala: “Suca et sallo, illi mici le!
Quén mici te, yeo esse náne Ahavo, oronte ar carne sinwa i Fairenen i tulumne túra saicele i quanda ambarenna. Ta é túle mi réryar Claurio.
Íre Yésus ortane henyat ar cenne haura şanga túla senna, quentes Filipenna: “Masse *hómancuvalve massali i sine lertuvar mate?”
Ar i vende lende ettenna ar quente amilleryanna: “Mana canuvan?” Eques: “Yoháno i *Tumyando cas!”
A neri, mela verildar ar áva na sárave rúşie téna.
Náse vaina collasse *tumyaina sercesse, ar i esse yanen náse estaina ná Eruo Quetta.
Ananta sí, castanen i vando antana atarilmain, táran náven námina.
Ar hamunelte, téma ara téma, mi tuxali ar *lepenquealli.
An únar illume samuvalde mici le, mal ní ualde illume same.”
Cé telil nahta ni, ve nacantel i Mirra noa?'
tenna panyuvan ñottotyar ve tulco talutyant.'
Yé! Coalda ná len hehtana!
An úcare ua le-turuva, an ualde nu şanye, mal nu Erulisse.
Man imíca i atta carne i ataro nirme?” Quentelte: “I attea!” Yésus quente téna: “Násie quetin lenna: I *tungwemor ar i *imbacindi lelyar epe lé mir Eruo aranie.
Etta hanquentasse quentes téna: “Ma intyalde i queni sine Alileallo náner úcarindoli ambe túre lá ilye exi Alileasse, pan perpérelte ta?
Á *suila Filolóho ar Yúlia, Nereo ar néşarya, ar Olimpas, ar ilye i airi aselte.
Íre lendelme mir Róma náne lávina Paulon mare vérave, as i ohtar tirila se.
Ar qui aiquen quete lenta: Mana castasta carien si? – tá queta: I Heru same maure séva, ar rongo se-*nanwentuvas.”
Ar martane, íre lahtanes i orirestar i sendaresse, i hildoryar *yestaner lepta i cari oriva.
Inye *tumya le nennen, mal sé le-*tumyuva Aire Feanen.”
Apa matie i matta ya mernelte, carnelte i cirya mis lunga hatiénen i ore mir i ear.
Ono qui vantalve i calasse tambe sé ea i calasse, samilve ranta quén as i exe, ar i serce Yésus Yondoryo vi-sove ilya úcarello.
Ar roitanen Tie sina i qualmenna, nútala ar antala olla mir mando neri ar nissi véla,
Etta Yésus quente i yunquenna: “Ma ualde mere auta, yando lé?”
Hyane erdeli lantaner i ondonna, yasse ualte sáme núra cemen, ar orontelte lintiénen, peniénen núra cemen.
Íre túlelte Capernaumenna, i neri comyala i racma atta túler Péterenna ar quente: “Ma *peantarelda ua paitya i *racma atta?”
quétala: “Áva mala cemen hya ear hya i aldar, tenna apa *ilihtielme i núror Ainolvo timbareltassen.”
Íre hamnes Orontesse *Milloaldaron, i hildor túler senna íre anelte erinque, quétala: “Nyara men: Mana i lúme yasse nati sine martuvar, ar mana nauva i tanwa entulesselyo ar i tyeldo i rando?”
lá hehtala yomenielvar, ve quenelion haime, mal hortala quén i exe, ar i ambe sie íre cenilde i aure túla hare.
Íre lahtanes, cennes nér lomba i mónallo.
Ar enge tasse nér yeo esse náne Saccaio, ar anes héra *tungwemo ar lárea.
Mal ualte *renne mapa massar aselte, ar hequa massa er sámelte munta aselte i luntesse.
Ar yé! martane hoa *cempalie, an i Héruo vala túle undu menello ar panyane oa i ondo, ar hamnes sasse.
Antalme illume hantale Erun íre enyalilme le hyamielmassen, oi
Nalde şance Hristollo, elde i nevir náve cárine faile i Şanyenen; et i Erulissello alantielde.
Mal quera imle oa ho *auce centar ar ontaleparmar ar costier ar costi pa i Şanye, an taiti nati nar ú yáveo ar cumne.
Hanquentasse Yésus quente: “Úvoronda ar rícina *nónare, manen andave mauya nin lemya aselde? Manen andave mauya nin cole le? Áse tala ninna!”
Apa hautie aselde mernen lelya Maceronianna, ar nanwénala Maceroniallo, náve manyaina lo elde i yestasse lendanyo Yúreanna.
Ar peantanes tien rimbe natali sestielínen ar quente téna peantieryasse:
Pallette i asto taluttalto ar lendet Iconiumenna.
Ente, véra Şanyeldasse ná técina: Atan atto *vettie nanwa ná.
Etta quentelte senna: “Mana, tá, cáral ve tanna, i cenuvalmes ar savuvalme lyesse? Mana cáral?
An ear rimbali – quenten pa te rimbave, mal sí quétan pa té arwa nyéreo – i vantar ve ñottoli i tarweo i Hristo.
Qui enges sí cemende, únes *airimo, an ear i *yacir annar i Şanyenen.
mal istan pa lé i ualde same melme Eruva lesse.
Sí Iconiumesse lendette uo mir i *yomencoa, ar quentette mi lé tyarila hoa liyúme save, Yúrali ar Hellenyali véla.
Yando Nicorémus, i nér ye túle senna lómisse i yestasse, túle túlula ostime níşima suhteva ar *şortoava, *os *lungwi tuxa.
Mi imya lé, meneldea Atarinyo indóme ua in erya mici pityar sine nauva vanwa.
An i limpeo ormeva *hrupuhtaleryo usúcier ilye nóri, ar cemeno arani *hrupuhtaner óse, ar cemeno macari náner cárine lárie i túrenen úveryo ú landaron!”
Ar nyarna sina ettúle quanda sana ménanna.
An aiquen ye himya i quanda Şanye, mal loita pa erya nat, arácie ilye i axani.
Ta martane ter loa atta, ar sie illi i marner Asiasse hlasser Eruo quetta, Yúrar ar Hellenyar véla.
carien sinwa úvea alcarerya i venin oraviéva, yar nóvo manwanes alcaren
Péter quente senna: “Qui mauya nin quale aselye, uan oi laquetuva lye!” Ilye i hyane hildor quenter i imya nat.
Nér sina lende Pilátonna ar arcane Yésuo hroa. Tá Piláto antane i canwa, ar anes antana sen.
Hanquentasse Péter ar i hyane aposteli quenter: “Mauya men hilya Eruo canwar or tai atanion.
Queta nati sine ar á horta ar naitya, quanta túrelyanen. Áva lave aiquenen nattire lye!
Epeta tannesexe Yácoven, tá ilye i apostelin,
An quetin lenna: Ho sí uan sucuva ata i yávello i liantasseo nó Eruo aranie tuluva.”
Áva na turúna ulcunen, mal turua ulco mariénen.
Hé quente: “Ná, Heru, mal i huor matir i mier yar lantar sarnollo herultaiva!”
Mal sí Eru apánie i rantar i hroasse, ilya erya mici tai, aqua ve mernes.
Paulo quente i *tuxanturenna ar i ohtarinnar: “Qui neri sine uar lemya i ciryasse, ua ece len náve rehtane!”
Tá arcanes tello tule minna, ar camneset ve *naşali.I hilyala auresse orontes ar lende oa aselte, ar ennoli i hánoron Yoppasse lender óse.
Mal íre lendelte oa carampelte quén as i exe, quétala: “Nér sina care munta valda qualmeo hya limilion.”
Etta mentanyes ta ambe rato, velieldan se ar samieldan alasse, ar sie inye samuva mis nyére.
Mal íre i hére airimor ar i cánor cenner se, yámelte quétala: “Áse tarwesta! Áse tarwesta!” Piláto quente téna: “Lé áse mapa ar áse tarwesta, an inye ua hire cáma sesse.”
Cenuvalte cendelerya, ar esserya euva timbareltasse.
Hanquentasse Yésus quente: “Ma únelte i quean nestaine? Masse, tá, nar i hyane nerte?
an ná técina: “Ve coitan, quete i Héru, ninna ilya occa cuvuva, ar ilya lamba laituva Eru.”
Ar lahtala nér ye túle i restallo mauyanelte cole tarwerya – Símon Círenello, Alexander ar Rúfuso atar.
sénen coivie né, ar i coivie náne Atanion cala.
Sí şinye náne túlienwa, ar i lunte náne i earo endesse, mal sé náne erinqua i noresse.
Mi imya lé, mauya i nauva *ocombenduri *sanwelunge, lá quetila lamba attanen, lá antala inte olya limpen, lá milcave cestala úfaila ñetie,
mi ilye nati tánala imle ve *epemma máre cardaron. Peantielyasse á tana i nalye alahasta, *sanwelunga,
Ar i neldea ulyane tolporya mir i síri ar i ehteler nenwa, ar ahyanelte mir serce.
Yé! Nyáran len fóle: Illi mici vi uar qualuva, mal illi mici vi nauvar vistaine,
Quentes senna: “Alye mele i Héru Ainolya quanda endalyanen ar quanda fealyanen ar quanda sámalyanen.
Ar i óma ya hlassen menello quente ninna ata, quétala: “Á lelya, á mapa i parma ya ná pantaina ho i má i valo ye tára earesse ar noresse.”
elye ye peanta exin, ma ual peanta imlen? Elye ye peanta: “Áva pile,” ma pilil?
Mal hanquentasse quentes: “Ná técina: Atan ua coita rie massanen, mal ilya quettanen ya tule Eruo antollo!”
ar sa-panyanes vinya noiriryasse, ya nóvo carnes ve hróta i ondosse. Apa *peltanes sar epe i noirio fennanna oantes.
ar quétala: “Heru, órava yondonyasse, an náse *ránahlaiwa ar nimpa, an lantas rimbave mir i náre ar rimbave mir i nén,
alye anta alcar atarelyan ar amillelyan, ar mela armarolya ve imle!”
Mal i hére *airimor ar i amyárar *mirquenter i şangar arcienna Varavas, mal i nancarumnelte Yésus.
Ar íre caines asette ara i sarno nampes i massa, aistane sa, rance sa ar antane sa tun.
ar quentes senna: 'Á auta nórelyallo ar nosselyallo ar tula i nómenna ya tanuvan lyen.'
I cirya náne mapaina ar ua polle hepe langorya i súrinna, ar pustanelme neve turitas ar náner cóline.
Epeta ulyanes nén mir salpe, ar *yestanes sove i hildoron talu ar *parahta tú i *paşelannenen yanen anes *quiltaina.
Ono Sílas sanne i náne mára sen lemya tasse.
Yésus quente senna: “Manen ná i estal ní mane? *Úquen ná mane hequa er, Eru.
Ar i *Sahtando túle senna ar quente: “Qui elye Eruo yondo ná, queta sine sarninnar: Ola massar!”
An qui antolyanen teal savielya i Yésus Heru ná, ar savil endalyasse in Eru ortane se et qualinallon, nauval rehtana.
ar Yoháno Severaion ar Yoháno Yácovo háno – yent antanes i epesse Voaneryes, ya tea Yondor Hundiéva –
Ar óma túle et i lumbullo, quétala: “Si ná yondonya, ye anaie cílina. Á lasta senna!”
Tá, íre Yúras cenne i hé náne námaina, sámes inwis ar talle nan i telpemittar *nelequean i hére *airimoin ar i amyárain.
Mal qui ua ea aiquen antien i *teale, mauya sen náve quilda i ocombesse ar quete insenna ar Erunna.
Quernen immo cenien mano óma quente ninna, ar apa querie cennen calmatarmar otso maltava,
Mal ente auressen, apa enta şangie, Anar oluva morna, ar Işil ua antuva calarya,
Apa menie şinta vanta ompa, lantanes cemenna ar arcane i autumne sello i lúme, qui ta náne cárima.
Té nar i tyarir şancier, neri ve celvar, pénala faire.
“Neri, hánor ar atari, hlara ni quéta inyen!”
Mal alde cime; len-anyárien ilqua nóvo.
Hánor, arcan lello: Na ve inye, an anen yando ve lé! Carnelde munta raica nin.
'Apa nati sine nanwenuvan ar encarastuvan Laviro lantienwa cauma, ar encarastuvan atalantie rantaryar ar ortauvan sa ata,
endaryo fóli nar apantaine, ar sie, lantala cendeleryanna, *tyeruvas Eru ar *etequentuva: "Eru é ea mici elde!"
Carnes i héri ar i túri helde ar pantave ettanne tai yaiwen, apa sámes apairerya or tai i tarwesse.
Ar i liyúme i ostollon pelila Yerúsalem ocomne, cólala hlaiwali ar i náner nwalyaine lo úpoice fairi, ar ilquen mici te náne nestana.
Mal samin alasse pan Stefánas ar Fortunáto ar Acaico nar sisse, an ómu elde uar asinye, té aquátier maurenya.
Etta áva ruce tello. An ea munta tópina ya ua nauva apantana, ar munta nulda ya ua nauva sinwa.
Apa quentes nati sine, piutanes i talamenna ar carne luxo i piutanen, ar panyanes i luxo i nero hendunna
Ar i sinyar pa sé náner palyaine i quanda Alilea-ménasse.
Tá elmenda lantane illinnar, ar carampelte quén as i exe, quétala: “Mana nostale quetiéno ná si? An túrenen ar melehtenen canis i úpoice fairi, ar ettulilte!”
“An anaie técina i Parmasse Airelírion: 'Nai mardarya nauva lusta, ar nai *úquen maruva tasse', ar: 'Nómerya ortíriéno lava exen mapa.'
Taris venna ar ortaxe or ilquen estaina aino hya yenna mo hyame; sie hamúvas Eruo cordasse, tánala inse ve aino.
Ar peantieryasse quentes: “Tira inde pa i parmangolmor i merir vanta ande collassen ar merir náve *suilaine i *mancanómessen
Etta, apa autie i minye natillon i peantiéno Hristo, alve cesta i quanta hande, lá ata tulcala talma pa inwis qualini cardallon ar savie Erusse,
Ar cennen, ar yé! malwa rocco, ar ye hamne sesse sáme i esse Ñuru, ar Mandos se-hilyane. Ar hére náne tien antaina or i canasta cemeno, nahtien andamacilden ar maitiénen ar qualmequámenen ar i hravaninen cemeno.
pustien aiquen návello túcina oa lo şangier sine. An elde istar i nat sina ná ven martyana.
Mal i nissi i túler óse et Alileallo hilyaner ar cenner i noire ar i nóme yasse hroarya náne panyaina.
i yestallo íre Yoháno sumbeáne ar tenna i aure yasse anes cámina ama vello. Quenen mici té mauya náve astarmo aselve pa enortierya.”
An yétalde yalierya eldeva, hánor: i lá rimbali i náner saile mí hráveo lé náner yáline, lá rimbe taurali, lá rimbali nóne ve arator.
Nyarin lenna, antuvas ten failie rato! Ananta, íre i Atanyondo tuluva, ma hiruvas i savie cemende?”
Tá Annas se-mentane nútina Caiafas i héra *airimonna.
Yando mauya i ear *rantier mici le, tanien man mici le nar sarte.
Antalme illume hantale Erun, Yésus Hristo Herulvo Atar, lan hyámalme pa le.
Mal qui aiquen vanta i lómisse, taltas, pan i cala lá ea sesse.”
Íre i exi quean hlasser pa si, anelte rúşie i háno attanna.
*Immotuntie, quetin, lá véralya, mal ta i hyana queno. An manen ná i lérienya ná *námima hyana queno *immotuntiénen?
Ilya *şanyelórie úcare ná, ananta ea úcare ya ua tulya qualmenna.
An illi mi Aşen ar ilye i ettelear tasse satir ilya lerina lú carien munta hequa nyare hya hlare sinyar.
Tumna nyére nampe i aran, ananta uas merne váquete ya i vende canne, pan antanes vandaryar epe i queni ara i sarno.
Mal eques: “Quetin lyenna, Péter: Tocot ua lamyuva síra nó equétiel nel i ual ista ni.”
An yando i Atanyondo túle, lá samien núror, mal náven núro ar antien cuilerya ve *nanwere quaptalesse rimbalin.”
Lá i nalme heruvi savieldo, mal molilme aselde alassedan, an i saviénen tarilde.
Ente, ea raxe i lá rie sina molielva nauva nattíraina, mal yú i corda i túra Artemisso nauva nótaina ve munta, metyala yú túrierya, epe ya quanda Asia ar ambar luhtar.”
I Farisar etta hanquenter: “Lau yando lé nar *útulyaine?
ar sé ná cas hroaryo: i ocombe. Náse i yesta, i minnóna qualinallon, náveryan mi ilye nati i minya.
et nossello Simeondo húmi yunque, et nossello Levio húmi yunque, et nossello Issacáro húmi yunque,
Nai samuvalde lisse ar raine ho Eru Atarelva ar i Heru Yésus Hristo,
Istan masse marilye, yasse ea i mahalma Sátanwa; ananta hépal essenya. Ual lalane savielya nisse yando mi réryar Antipas voronda *vettonya, ye náne nanca ara lye, yasse mare Sátan.
Tá Paulo talle i neri óse, ar i hilyala auresse poines inse aselte ar lende mir i corda, carien sinwa i ré yasse sovallelta nauvane telyana ar i *yanca nauvane talaina ilquenen mici te.
Etta endanya náne valima ar lambanya quente pa túra alasse. Ente, yú hrávenya maruva estelde,
*Saltamarya ea máryasse poitien *vattarestarya, ar comyuvas orirya mir i haura, mal i *ospor urtuvas nárenen ya *úquen pole *luhtya.”
Etta ualde ambe ettelear ar queni i marir nóresse lá véralda, mal nalde asambaror as i airi ar marir coasse Eruva,
Etta enyala i yá anelde queni i nórion pa i hráve. Anelde estaine *Úoscirie lo ta ya ná estaina *Oscirie, carna i hrávesse mainen.
Sie, qui quanda hroalya ná calyaina, lá arwa ranto ya ná morna, ilqua nauva calyaina ve íre i calma calya lye alcaryanen!”
i *imnatyalénen quenion i quetir huruvi, arwe tehto véra *immotuntieltasse ve qui carna lauca anganen.
Tá quetuvalde: 'Mantelme ar suncelme epe lye, ar peantanel mallelmassen!'
Ar quetin lyenna: Elye Péter, ar ondo sinasse carastuvan ocombenya, ar Mandosto andor uar turúva sa.
mal paimetan hroanya ar tulya sa ve mól, i inye ye peánie exin ua nauva quérina oa.
É euva en *sendare-sére Eruo lien.
An ná técina: “Na valima, a nís ye ua colle hína; sama alasse ar yama, elye ye ua sáme naice nóniéva! An i *yávelóra nisso híni nar rimbe lá i híni i nisso ye sáme i veru.”
Yúrain carnenyexe ve Yúra, ñetienyan Yúrar; in náner nu şanye carnenyexe ve quén nu şanye, ómu inye ua nu şanye, ñetienyan i nar nu şanye.
an hendunyat ecéniet rehtielya,
Ar i ulo ulle undu, ar i oloiri túler, ar i súri vávaner ar penter coa tananna, ar talantes, ar lantarya náne hoa!”
Mal íre caril merende, á tulta penyar, *hroaloicali, *úlévimar, *cénelórar,
Ar qui melilde i melir lé, manen ta nauva aşea len? An yando i úcarindor melir i melir té.
Váquetilte i mo verya ar canir i mo quere oa mastimar yar Eru ontane náven cámine hantalénen lo i samir savie ar istya i nanwiéno.
Qui, tá, quén ye ua *oscirna hepe i Şanyeo faile axani, ma penierya *osciriéva ua nauva nótina ve *oscirie?
An té inte nyárar manen minyave túlelme lenna, ar manen quernelde inde Erunna ho i cordoni, náven núror coirea ar nanwa Ainon,
Cestalte le uryala írenen, lá márien, mal merilte cire le oa nillo, cestieldan té uryala írenen.
Mal i exe túle, quétala: 'Heru, sisse ea minalya, ya ehépien caitala mi lanne.
Ve mára ohtar Hristo Yésuo perpera ulco as i exi.
Qui sintelden, sintelde yando Atarinya. Lú sinallo istaldes ar ecénieldes.”
Mal Paulo ua sanne i náne mára tala i nér ye tu-hehtane Pamfiliasse ar ua lende asette i molienna.
Istan cardalyar ar molielya ar voronwielya, ar i ual pole cole ulce queni, ar i atyastiel i quetir i nalte aposteli, mal uar, ar hirnelyet *hurindoli.
An i natto ua er i Yúran ar exe i Hellenyan; mal ea i imya Heru or illi, ye lára ná illin i yalir senna.
ar íre anen Yerúsalemesse i hére *airimor ar i amyárar i Yúraron nyarner ni pa se, arcala i namuvan se ve arwa cámo.
Ar panyanes máryat sesse, ar mí imya lú orontes tarien téra, ar antanes alcar Erun.
Na, tá, ilvane, tambe Atarelda menelde ilvana ná.
Ente, i nar Hristo Yésuo atarwestie i hráve as maileryar ar íreryar.
Íre i Hristo, coivielva, nauva apantaina, tá yando elde nauvar apantaine óse alcaresse.
Ar íre latyanes i neldea *lihta, hlassen i neldea coite quéta: “Tula!” Ar cennen, ar yé! more rocco; ar ye hamne sesse sáme tolpot máryatse.
Tá lemnette lá şinta lúmesse as i hildor.
Aranielya na tuluva! Na care indómelya cemende tambe menelde!
Mal i ranta i tehteleo ya hentanes náne si: “Ve máma anes talaina i nahtienna, ar ve eule ye ómalóra ná epe ye hocire tórya, uas pantane antorya.
I núror ar i cánor náner cárienwe *hyulmaruine, an ringa né, ar tarnelte *lautala inte. Yando Péter tarne aselte *lautala immo.
Tira inde pa i huor, tira inde pa i molir ulco, tira inde pa i cirir i hráve!
Tá i Heru quente: “Hlara ya úfaila námo sina quéta!
auri túlar yassen queni quetuvar: 'Valime nar i nissi ú yáveo, ar i súmar yar uar nostane ar i tyetsi yar uar *tyente!'
Ar Yésus quente téna: “Lau i endero meldor polir same lamate lan i ender ea aselte? Mí quanda lúme yasse samilte i ender endaltasse ualte pole same lamate.
An mana i ambe ancárima nat quetien: Úcaretyar nar apsénine, hya: Á orta ar vanta – ?
Íre arcanelte senna lemya aselte an anda lúmesse, uas merne,
i hosseturco canne i mo talumne se mir i *estolie, náven céşina riptiénen, istieryan i casta yanen sie yámelte senna.
Ar talleltes senna. Mal íre i faire cenne se, lintiénen tyarnes i seldo rihta, ar apa lantie undu anes pélala i talamesse, falastala.
Etta cestanelte Yésus ar quentelte, quén i exenna: “Mana sanalde? I laume tuluvas i aşarenna?”
Inye ná i massa coiviéva.
Mal elye, íre antal annar óraviéva, áva lave hyarmalyan ista ya formalya cára;
Ar túlala ompa, ye camne i talenti lempe talle an talenti lempe, quétala: “Heru, antanel talenti lempe mir hepienya. Ela, eñétien an talenti lempe!”
Si náne i aure Manwiéva, ar i *sendareo yesta náne hare.
Mal erya natwa ea maure. María icílie i mára ranta, ar sá ua nauva mapaina sello.”
Quetielya nauva ná, ná – lá, lá! Ya ná han si, ulcuo ná.
Ananta i ocombesse merin quete quettar lempe sámanyanen or quettar húmi quean lambesse.
Tá quente i heru ye sáme i tarwa lantassion: 'Mana caruvan? Mentuvan melda yondonya; sen samuvalte áya.'
Ámen anta síra ilaurea massalma,
Ennoli mici te lastaner quettaryannar, mal exeli loitaner save.
An nís ye evérie nerenna ná nútina veruryanna lan i nér coirea ná, mal qui verurya quale, i nís ná lehtana i veruo şanyello.
I rundacolindor nyarner quettar sine i námoin. Anelte ruhtane íre hlasselte i anette Rómear.
Íre pustanes quete, quentes Símonna: “Á lelya yanna núra ná, ar alde panya undu rembeldar mapien.”
Ar illi mici te, i ampityallo i antúranna, cimner se ar quenter: “Nér sina Eruo túre ná, ya ná estaina Túra.”
Hya manen ece lyen quete hánolyanna: 'Lava nin tuce i lisce et hendetyallo', íre yé! ea pano véra hendelyasse?
Sé ye ua sinte úcare carnes úcare elven, návelvan Eruo failie ter sé.
Istalme i Eru acarpie Mósenna, mal pa nér sina ualme ista mallo náse.”
Mal carampes i *yomencoasse ilya *sendaresse ar tyarne Yúrar ar Hellenyar save.
Sie hortan le i tanuvalde melmelda séva.
Ar lenden i valanna ar quente senna i nin-antumnes i pitya parma; ar quentes ninna: “Ása mapa ar mata; nauvas sára cumbalyan, mal lisse ve lís antolyasse.”
Ar apa quetie si lendes oa ar yalde María néşarya, quétala nuldave: “I *Peantar ea sís, ar yálas tye.”
Hya ma Varnavas ar inye erinque uat lerta náve lére moliello?
Hanquentasse Yésus quente: “Uaste ista ya arcaste! Ma lét polit suce i yulma ya inye sucuva?” Quentette senna: “Polimme!”
Etta, alve care ilqua ya polilve tulien mir sére tana, pustien aiquen lantiello mi imya lé: loitiénen cime i canwar.
A veruvi, mela verildar, ve i Hristo yando méle i ocombe ar antane inse san,
Sí lan i Farisar aner comyane Yésus maquente téna:
An sí nalme coirie, qui tarilde tulce i Herusse.
á aista i hútar le ar hyama in quetir ulco pa le.
Ar cennen vala tárala Anaresse, ar yámes taura ómanen ar quente ilye aiwennar i vilir menelo endesse: “Tula, ócoma Eruo túra merendenna,
Nér sina mare coasse quenwa yeo esse Símon ná, *alumo, ye same coa ara i ear.”
Queneli i lieron ar nossion ar lambion ar nórion yétuvar loicottat ter rí nelde ar perta, ar ualte lave i loicottat nát panyaine noirisse.
Sie mauya ilquenen mici le harya véra venerya, mi airitáve ar mi alcar,
Ente, i alcar ya ánietye nin ánien tien, i nauvalte er, síve vet nát er,
Mal palles i hloirea onna mir i ruine ar perpére munta olca.
Mal mauyanette sen, quétala: “Á lemya asemme, an i şinye hare ná, ar i aure rato nauva vanwa.”
Ar yé! enge tasse nér epe se ye náne púlienwa, arwa acca olya neno hroaryasse.
Ar i rassi quean yar cennel nar arani quean, i uar en acámie aranie, mal é camilte hére ve aralli erya lúmesse as i hravan.
Tulyaneltet i námonnar ar quenter: “Neri sine *tarastar ostolva, nála Yúrar,
Etta cara nat sina ya nyarilme len: Ear aselme neri canta i samir vanda.
Ar i vorondar i náner tulinwe as Péter, té i náner i *osciriéva, náner quátine elmendanen pan i anna Aire Feava náne ulyaina yando Úyúrannar.
Íre Elísavet hlasse Marío *suilie, i lapse mónaryasse campe, ar Elísavet náne quátina Aire Feanen,
Ar yé! i quanda osto etelende velien Yésus, ar apa cenie se, hortaneltes auta ménaltallon.
Ar hyana vala etelende i cordallo ya ea menelde, yando sé arwa aica circo.
Áva panya malta hya telpe hya urus quiltaldasse
Mal i lómisse i Héruo vala pantane i mando andor, talle te ettenna ar quente:
Mal elmen mauya anta Erun hantale pa lé, hánor mélaine lo i Heru, an Eru le-cilde i yestallo náveldan rehtaine, airitiénen faireva ar saviénen i nanwiesse
Sí istalme i istal ilye nati ar ua same maure i aiquen ceşuva lye. Sinen istalme i ettúlel Erullo.”
Mal aiqua úaire ar aiquen ye care şaura nat ar huru ua tuluva minna sa, mal eryave i nar técine i parmasse coiviéva i Euleva.
An lisseryanen anaielde rehtaine, saviénen, ar ta ua martane eldenen; i anna ná Eruo,
Mal Péter quente ata ar ata: “Yando qui mauya nin quale aselye, laume lye-laluvan!” Sin quenter yando ilye i exi.
sinwe et yalúmello.
Ar quentes téna: “Aiquen ye lehtaxe veriryallo ar verya exenna care vestaracie senna,
Ar íre túles hare i osto andonna, qualin nér náne cólina ettenna, amilleryo *ernóna yondo. Ente, verurya náne qualin. Hoa şanga i ostollo enge yando as i nís.
Natto sina túle illion istyanna, Yúraron ar Hellenyaron véla, té i marner Efesusse. Ar caure lantane ilyannar mici te, ar i Heru Yésuo esse náne laitaina.
Mal qui aiqueno enda ná tulca, lá arwa maureo, mal samis túre or véra nirmerya ar acárie i cilme véra endaryasse i se-hepuvas vende, caruvas mai.
Mal íre Yésus sa-hlasse quentes: “Hlíve sina ua qualmen, mal Eruo alcaren, i nauva i Atanyondo *alcaryaina sanen.”
Ar quentes téna: “Lau calma ná talaina náven panyaina nu colca hya nu caima? Ma uas talaina náven panyaina i calmatarmasse?
Sí merin i istalde, hánor, i sómanya atyárie i evandilyon lelya ompa ambe lá pusta sa,
Etta, pan anaielve quétine faile saviénen, nai samuvalve raine as Eru, Yésus Hristo Herulvanen.
Ar lendes oa tello ar hyamne mi neldea lú, quétala i imya quetta ata.
Á quetir ulco pa *úquen, la merila cos, nála maxe sámo, lengala moicave ilye atanin.
Etta i hildor quenter, quén i exenna: “Ma aiquen tulune senna *aiqua matien?”
Mal hildoryar quenter senna: “Cénal i şanga níra lyenna, ar tá quétal: Man ni-appane?”
An i nér pa ye nati sine nar quétine náne nóna mir hyana nosse, yallo *úquen *evévier ara i *yangwa.
lá i quanda lien, mal astarmolin cíline lo Eru – elmen, i manter ar suncer óse apa ortaverya et qualinallon.
An «ye mere mele coivie ar cene máre aureli, nai hepuvas lambarya ulcullo ar péryat quetiello huru,
Ar cennen túra ninque mahalma ar ye hande sasse. Cendeleryallo cemen yo menel úşet oa, ar nóme úme hírina tún.
Ve mauya, i mólala *cemendur ná i minya ye came ranta i yávion.
Etta, qui samilve matta ar larmar, lava tain farya ven.
Nai samuvalde Erulisse ar raine úvesse, i istyanen pa Eru ar Yésus Hristo Herulva,
Ar sie María, íre túles i nómenna yasse enge Yésus ar cenne se, lantane epe talyat, quétala senna: “Heru, qui anel sís hánonya úme qualin!”
An ilya nostale hravani celvaron ar aiwion ar hlócion ar onnaron earesse anaier cárine núror Atanion.
Melme same cóle ar moica ná. Melme ua same *hrucen, uas laitaxe, uas turquima,
– yando rá inyen, camienyan quettar íre pantan antonya, lávala nin veriénen care sinwa i evandilyono fóle,
Ar nóri vantuvar i calanen i ostollo, ar cemeno arani mentuvar alcarelta minna sa.
Ar cennen hyana vala ortala anarórello, sámala i coirea Eruo *lihta, ar yámes taura ómanen i valannar canta in náne lávina mala cemen earye,
Mal istalde manen tannes voronwerya – i ve hína as atar móles óni i evandilyonen.
Mal tambe ye náne nóna mí lé hráveo roitane ye náne nóna mí lé faireo, síve yando sí.
Etta antanes yando rimbe hyane hortiéli ar *nyardane i evandilyon i lien.
Íre i aran túle minna yétien i caitalar ara i sarno, cennes nér ye úne netyana lanninen veryangweo.
I neri i enger asinye cenner i cala, mal ualte hlasse i óma yeo quétane ninna.
Á lenga mai imíca i nóri! Tá i sí quetir ulco pa le, ve pa ulcarindor, nauvar astarmor máre cardaldaron, ar antuvar alcar Erun i auresse ya ceşis te.
Eruo failie saviénen Yésus Hristosse, illin i savir. An *úquen arya ná lá exe,
Mal íre sávelte i quetier Filipo, ye carne sinwe Eruo aranie ar Yésus Hristo esse, anelte sumbane, neri ar nissi véla.
Ar hlassen taura óma et i yánallo quéta i valannar otso: “Á lelya ar ulya i tolpor otso ormeva Eruo mir cemen!”
Ar talleset óse yana lúmesse ar sóve rimpeltar, ar isse ar illi i enger óse náner sumbane pen hopie.
Mana tá? Ya Israel cestea, ta uas eñétie, mal i cílinar eñétier sa. I exion tuntie olleilaica,
Tanalte i samilte i şanyeo nattor técine endaltasse, íre *immotuntielta *vetta aselte ar, vére sanweltassen, ore tien i colilte cáma hya cé penir cáma
Íre túlelme Yerúsalemenna, i hánor me-camner mi alasse.
Yésus maquente senna: “Mana esselya?” Eques: “Lehion,” an rimbe raucoli náner ménienwe mir se.
Tá Yósef, apa cuivierya húmeryallo, carne ve i Heruo vala cannelyane sen, ar nampes verirya coaryanna.
Pan anelte şance oantelte, mal Paulo quente téna erya quetie: “Nanwave i Aire Fea carampe atarildannar ter Yesaia i Erutercáno,
Ente, áva name, ar laume nauvalde námine; áva quete i exi nar valde paimeo, ar ua nauva quétina pa lé i nalde valde paimeo. Á lehta, ar nauvalde lehtaine.
Ma ualde ista i camir i núror i airi nation laulestalta i Cordallo, ar i núror ara i *yangwa samir masselta i matsosse *yácina i *yangwasse?
Laitan le pan mi ilye nati ni-hepilde sámasse, ar himyalde i haimi aqua ve antanenyet len.
Ar esseryasse i nóri panyuvar estelelta!”
nai nauva sinwa illin mici le ar i quanda lien Israélo i sá martane i essenen Yésus Hristo Nasaretello, ye elde tarwestaner mal Eru ortane et qualinaillon. Issenen nér sina tára sisse epe le málesse.
Ar quentette i Heruo quetta senna ar ilyannar i enger coaryasse.
haira or ilya turie ar túre ar hére ar ilya esse estaina, lá sina randasse erinqua, mal yando mí túlala.
Apa hlasselte ta, lendelte árasse mir i corda ar peantaner.Íre i héra *airimo ar i enger óse túler, comyanelte i Tára Combe ar i quanda ocombe amyáraron i yondoron Israélo, ar mentaneltet i mandonna talien te.
Mal i Heruo quetta alle ar vintane.
Mal ná milya, ar inye ua milya? Man ná tyarna lanta ar inye ua urya?
Atalante ar angayasse nar tieltassen,
Mal mena undu ar áte hilya, ar áva na iltanca pa *aiqua, an inye ementaye te.”
Tá i hyana hildo, ye túle minya i noirinna, yando lende minna; ar cennes ar sáves.
É *oscirie aşea ná qui himyal i Şanye; mal qui racil i Şanye, *oscirielya olólie penie *osciriéva.
Mal íre ilye nati nauvar panyaine nu túrerya, tá i Yondo immo panyuvaxe nu túre yeo panyane ilye nati nu se, carien Eru ilqua illin.
Ar nostaryat nánet ara *intu; mal peantanes tún i ávatte nyare aiquenen pa ya náne martienwa.
Pa sé ilye i hánor mi Listra ar Iconium caramper mai.
Apa quetie nati sine, Yésus náne *tarastaina fairesse, ar *vettanes ar quente: “Násie, násie quetin lenna: Quén imíca le *vartuva ni.”
Sie é carin ennenya in uan caruva sinwa i evandilyon yasse Hristo esse anaie quétina nóvo, pustien imne carastiello hyana queno talmasse.
Ar apa tulie marda tultas meldoryar ar armaroryar ar quete téna: 'Na valime asinye, an ihírien mámanya, ye náne vanwa.'
An i aran hanya nati sine, isse yenna quétan veriénen, an nanye tanca i munta sino uşe tuntierya, an nat sina ua anaie carna vincasse.
Tá i vende ye tirne i ando quente: “Ma ua yando elye i hildoron atan tano?” Sé hanquente: “Uan.”
Sina castanen Móses antane len i *oscirie – lá i nas Mósello, mal i atarillon – ar *oscirilde atan sendaresse.
Ente, nalve yando hírine ve húrala *vettoli Erunna, an *evettielve Erunna i ortanes i Hristo, ye uas ortane qui qualini uar ortaine.
Mal quentes téna: “Lé nar i tumnallon; inye i tarmenillon ná. Lé nar mar sinallo; inye ui mar sinallo.
Mal íre nati sine *yestar marta, tara halle ar á orta carelda, an etelehtielda túla hare.
ye *vettane pa i quetta Eruo ar i *vettie pa Yésus Hristo, ilqua ya cennes.
ar larmaryar mirilyaner ninquissenen ambela ya aiquen sóvala lanni cemende pole ninquita.
Singe mára ná, mal qui i singeo túre avánie, mananen antuvalde tyáve i singe imman? Sama singe eldesse, ar hepa raine imbe quén ar quén.”
Etta, címala i eala maure, sanan i si i arya ná: i lemya nér ve náse.
Áva comya len harmar cemende, yasse malo ar ñwarie ammatir, ar yasse arpor racir minna ar pilir.
ar nampeses ar mante sa epe hendultat.
Sé ná i hildo ye *vetta pa si ar ye etécie nati sine, ar istalme i *vettierya nanwa ná.
Pen hopie talles ohtalli ar *tuxantulli ar norne undu téna. Cénala i turco ar i ohtari pustanelte palpa Paulo.
Nála tanca i caruval ve méran técan lyenna, istala i caruval yando amba, han ya quetin.
Istala símaltar quentes téna: “Ilya aranie şanca insanna ná nancarna, ar már şanca insanna lanta.
Etta elme laitar imme pa lé mici Eruo ocombi, castanen voronwieldo ar savieldo mi ilye roitieldar ar i şangier yar cólalde,
É nanye tanca i Herusse i yando inye rato tuluva.
Hanquentasse quentes téna: “Mauya i neren ye same laupe atta, anta er i quenen ye ua same, ar mauya yen same natali matien care i imya.”
Sine nát i axan atta yanten i quanda Şanye linga, ar i Erutercánor!”
Mal ye ceşe i ilvana şanye lériéva, ar perpere sasse – pan acáries inse, lá *hlarindo ye enyale munta, mal carindo moliéno – nauva valima molieryasse.
Mal ilquen ná tyestaina návenen túcina ar şahtaina véra maileryanen.
Mal, indómenen i Aireo ye le-yalle, na yando elde airi ilye carieldassen,
Hata ilya caurelda senna, an nalde valde sen.
Yesaia quente nati sine pan cennes alcarerya ar carampe pa se.
Ar cennes lunte atta caitala ara i ailin, mal i *lingwimor náner ménienwe et tullo ar náner sóvala rembeltar.
*Imnatyandoli, Yesaia quente mai ve Erutercáno pa le, íre quentes:
Valime nar i roitainar failiénen, an tien menelo aranie ná!
Íre queni náner lorne, ñottorya túle ar rende úmirwe olvali imíca i ore, are oante.
Pa i fóle i tinwion otso yar cennel formanyasse, ar i calmatarmaron otso: i tinwi otso nar valali i ocombion otso, ar i calmatarmar otso nar i ocombi otso.”
Ar sie náse enelmo vinya véreo. Etta i nar yáline polir came i vanda oira *aryoniéno, pan qualme amartie lehtien te *nanwerenen ongweltallon nu i noa vére.
Mal samin i estel i hanyuvalde i elme uar oloitie i tyastiesse.
Ar illi mici te tatallaner, ar ilquen maquente i exi: “Mana si? Vinya peantie! Hérenen canis yando i úpoice fairi, ar carilte ve quetis.”
Mal ye ua hanyane sa, ar sie carne nati valde palpiéno, nauva palpaina mance tarambolínen. An ho ilquen yen olya anaie antaina, olya nauva cánina, ar ho quén yeo ortíriesse apánielte olya, canuvalte amba lá senwa.
Tá névelte mapa se, mal *úquen panyane márya sesse, an lúmerya en úme túlienwa.
Mi imya lú Yésus rahtane máryanen ar nampe se, ar quentes senna: “Nér pitya saviéno, manen ná i savielya náne iltanca?”
Mal íre nosserya hlasse sa, lendelte mapien se, an quentelte: “Avánies et sámaryallo!”
Ar et antoryallo mene aica andamacil, yanen petuvas nóri, ar tai-turuvas angaina vandilden. *Vattas i *limpevorma i ormeva i rúşeva Eru Iluvalo.
ar quenter: “Métimar sine omólier erya lúme, ananta antal tien i imya ya antal men i ocólier i aureo cólo ar i úre!”
Mal si quentes tyastien se, an sé sinte ya carumnes.
Ar mí ilya lú ya i coiti antar alcar ar laitale ar hantale yen hára i mahalmasse, ye coirea ná tennoio ar oi,
Mal na coive, illume hyámala i poluvalde uşe ilye nati sine yar tuluvar, ar tare epe i Atanyondo.”
Mal Piláto maquente ata ar quente téna: “Tá mana merilde i caruvan yen estalde aran Yúraron?”
Sinen istalde Eruo faire: Ilya faire ye *etequenta Yésus Hristo túlienwa i hrávesse ná Eruo,
tie ya pantanes ven ve vinya ar coirea tie ter i fanwa, ta ná, i hráve,
Ar i quanda şanga cestaner appa se, an túre lende et sello ar nestane illi mici te.
Tá vala menello tannexe sen ar talle sen antoryame.
Péter hanquente senna: “Heru, qui ta elye ná, ánin cane i tuluvan lyenna olla i neni!”
An lá nu vali apánies i ambar ya tuluva, pa ya quétalve.
