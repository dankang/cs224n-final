Ar mí imya lé, queni sine nar i rérinar i ondosse: Mí lú ya ahlárielte i quetta, camiltes arwe alasseo.
Etta áva na úhande, mal á tunta mana i Heruo indóme ná.
An ya úcare paitya qualme ná, mal Eruo anna oira coivie ná, mi Hristo Yésus Herulva.
Ar qui málya tyára lantelya, ása aucire; menie mir coivie ú hroaranto ná lyen arya lá autie arwa má atto mir Ehenna, mir i náre ya urya tennoio.
Mal quera oa ambe nesse *verúlórar, an íre yérelta tuce te oa Hristollo, merilte verya
ananta nanye valima sí, lá pan sámelde nyére, mal pan i nyére le-tulyane inwistenna. An i nyére ya sámelde náne Eruo, i munta nauva len vanwa elmenen.
pen hande, racila quettalta, úméle, pen oravie.
An ea munta nurtaina ya ua nauva apantaina, hya *aiqua halyaina ya ua oi nauva sinwa hya aşcénima.
Sé ua quente ná panoltanna hya cardaltanna. Túles Arimaşeallo, osto i Yúraron, ye yente ompa Eruo aranienna.
Mal lan lemnelme tasse ter nóte aurion, Erutercáno yeo esse náne Ahavo túle undu Yúreallo, ar túles menna ar nampe i quilta Paulova.
Ar cirnelte nórienna Erasenyaiva, ya caita i hyana hrestasse Alileallo.
Saviénen oantes Mirrandorello, mal ú ruciéno i aranello, and an anes voronda ve qui cennes i *Alacénima.
Sí i lúme náne *os i enquea, ar mornie lantane i quanda cemenna tenna i nertea lúme,
Ualme anta immen mára *vettie epe le, mal antalme len ecie laitien inde elmenen, samieldan hanquenta in laitar inte etya ilcen, mal lá i endan.
Ea *úquen ye, apa nartie calma, tupe sa venenen hya panya sa nu caima, mal sa-panyas i calmatarmasse, lávala in tulir minna yéta i cala.
Ar Isaia yame pa Israel: “Ómu Israelindion nóte ná ve earo litse, rie pitya lemyala ranta nauva rehtana.
Lúme yanasse, íre i şanga náne ocómienwa mi *quaihúmeli, ta rimbe i quén *vattane i exe undu, *yestanes quete hildoryannar minyave: “Hepa inde oa i *pulmaxello i Fariryaron, yá *imnetie ná.
Ar quentes téna: ”Nati sine nar quettanyar yar quenten lenna íre en anen aselde, i ilye i nati técine Móseo Şanyesse ar mí Erutercánor ar mi Airelíri nauvar carne nanwe.”
Ar túlelte mir Yerico. Mal lan sé ar hildoryar ar hoa şanga náner tieltasse et Yericollo, Vartiméo yondorya Timéo, lomba *iquindo, hamne ara i malle.
ar ter *nónaréli ar nónaréli oravierya ea issen melir se.
Ar ye same apaire ar cime cardanyar tenna i metta – antuvan sen hére or nóri.
Ar qui lúr otso mi erya aure úcaris lyenna ar nanwenis lyenna lúr otso, quétala, 'Samin inwis!', ásen apsene!
I nessa nér quente senna: “Ihímien ilye axani sine; mana en penin?”
Etta i *coanturo móli túler ar quenter senna: “Heru, ma ualye rende mára erde restalyasse? Mallo samis úmirwe olvali?”
Mal hanquentasse i olca faire quente téna: “Yésus istan, ar ahlárien pa Paulo, mal man nar elde?”
Apa nati sine náner telyane, Paulo carne ennerya in apa lendes ter Maceronia ar Acaia lelyumnes Yerúsalemenna, quétala: “Apa tuluvan tar mauya nin yú cene Róma.”
Rimbe lúli se-aháties mir náre ar mir nén véla, nancarien se. Mal qui polil, áment manya, orávala metse!”
Mal ta uas quente immonen, mal pan anes i héra *airimo, quentes ve Erutercáno i náne Yésus martyaina qualien i nóren,
Ar tulyanes tu ettenna ar quente: “Heruvi, mana mauya nin care ñetien rehtie?”
Etta ilquen ye *etequeta ni epe atani, yú inye *etequetuva epe Atarinya ye ea menelde;
Elde i racir vestale, ma ualde ista i qui mo ná nilda i marden, mo ná cotya Erun? Etta, aiquen ye mere náve meldo i mardo care inse ñotto Eruo.
Ye ua mele ua ista Eru, pan Eru ná melme.
Mal Piláton ua ence save i qualles ta rongo, ar yaldes i *tuxantur ar maquente senna: “Ma náse sí qualin?”
ar quenter senna: “Ma hláral ya té quétar?” Yésus quente téna: “Ná! Ma ualde oi hentane si: Et antollo lapsion ar vinimoron amanwiel laitale – ?”
Etta Tíren ar Síronen i namie nauva *cólima lá lent!
An enyalin i savie ya ea lyesse ú *imnetyaleo, ar ya marne minyave Lois &harunilyasse ar Eunice amillelyasse, mal nanye tanca i eas yando lyesse.
Quentette: “Sava i Heru Yésusse, ar nauval rehtana, elye ar nosselya.”
Mí imya lé ye camne i atta ñente an atta.
Mal Eru, nála alya mi oravie, túra melmeryanen yanen méles vi,
I lantaner imíca i neceli, té nar i ahlárier, mal nála cóline oa coive sino querelínen ar larnen ar alassínen, nalte quórine ar telyar munta.
Áva na lintiénen páline handeldallo hya valtaine, faireo apantiénen hya quettanen hya tecettanen quétina tule mello, quétala in i Heruo aure utúlie.
Ar autala talo Yésus cenne nér hára i *tungwemende, estaina Mattéo, ar quentes senna: “Áni hilya!” Ar orontes ar hilyane se.
Ar mí imya lú i celume serceva pustane, ar túnes hroaryasse i anes nestaina naicelea hlíveryallo.
ar i tie raineva ualte isintie.
Mal enge yando costie imbi te pa man mici te *şéne náve i antúra.
An sintes i hrúcen náne ya tyarne i hére *airimor anta se olla.
An i evandilyon náne tulúna venna ar téna véla, mal i quetta ya hlasselte carne munta aşea tien, an ualte náne *ertaine saviénen as i lastaner.
An íre Eru carne vandarya Avrahámen, antanes vandarya insenen,
Mal Yésus quente senna: “Yelde, savielya erehtie lye; á auta rainesse!”
íre lómisse yo auresse, i antúra veassenen, cáralme arcandeli cenien cendelelda ar quatien yar savielda pene?
Mí imya lé yando i máre cardar nar aşceni, ar yar uar sie, nar en úfantime.
Mal etelehtanes faila Lót, ye náne ita *tarastaina i lehta lengiénen ion tarner şanyenna.
Sina castanen lantan occanyanta epe i Atar,
Áva na panyaina nu *alavéla yanta as queni i uar save. An mana ná ya failie ar *şanyelórie samir uo? Hya mana i *uome imbi cala ar mornie?
Núromolie sina tana voronwelda íre nalde tyastaine. Etta antalte alcar Erun, pan panyaldexer nu i Hristo evandilyon, ve pantave quetilde i carilde, ar antalde lérave tien and illin.
Mal quelli mici te quenter: “Ma nér sina ye latyane i lombo hendu ua polde pusta quén sina qualiello?”
ar Yosía óne Yeconya ar hánoryar, i lúmesse ya i lie náne tulyana oa ve etyar Vávelenna.
Ar i yána náne quátina usqueo, i alcarnen Eruo ar túreryanen, ar *úquen polle tule mir i yána tenna i ungwaler otso i valion otso náner telyaine.
Íre i nessa nér hlasse ta, oantes nairesse, an sámes rimbe armali.
Ar et i mahalmallo túlar ítali ar ómali ar hundiéli, ar ear calmar otso uryala epe i mahalma, yar nar i fairi otso Eruo.
An ómu sí carnelde arya qui anelde *peantalli, elden mauya i quén peanta len ata i minye nati Eruo quetto. Samilde maure ilinwa, lá ronda matsova,
Mal enger yú vantala Yúrali imíca i et-hanter raucor i néver quete Yésuo esse innar náner haryaine lo raucor: “Canin le i Yésunen ye Paulo care sinwa.”
pa ya inye anaie sátina ve tercáno ar apostel ar *peantar.
Hya manen aiquen pole lelya mir i coa polda nerwa ar mapa armaryar, qui uas minyave nute i polda nér? Ar tá piluvas i nati coaryasse.
Yen pole hepe le tulce i evandilyonnen ya tallen ar cariénen sinwa Yésus Hristo, i apantiénen i fóleva ya anaie hépina quildesse ter oireli,
Etta i camner quettarya náner sumbane, ar yana auresse *os fear húmi nelde náner napánine.
An quetin len i mauya i nat sina técina pa ni nauva carna nanwa: 'Ar anes nótina mici şanyelórali.' An ya ape ni nauva telyaina.”
Cima i quácor, i ualte rere hya *cirihta, ar ualte same haurar hya ataqui hepien nati, ananta Eru anta tien masto. Manen mirwe lá aiwi elde nar!
Ar áva sana i lertalde quete indenna: 'Atarelva Avraham ná' – an inye quete lenna in Eru pole orta ama híni Avrahámen sine sarnillon.
Nat sinanna é anelde yáline, an yando Hristo perpére ñwalmeli elden, hilieldan ca se tieryasse.
cílala perpere ulco as Eruo lie or samie alasse mi úcare ter lúme,
An i hroa é ua er ranta, mal rimbe.
Apa réli Felix túle as Rusilla verirya, ye náne Yúra, ar talles Paulo ar lastane senna pa i savie mi Hristo Yésus.
Ananta i Antara ua mare coassen cárine mainen, ve i Erutercáno quete:
An illi i nar tulyaine lo Eruo faire, té nar Eruo yondor.
Tá cestanelte hanquenta i atarello hwermelínen, mana esse mernes i hínan.
Mal ente auressen olles hlaiwa ar qualle. Sóveltes ar panyaner se i oromardesse.
Ente, *yestanes peanta tien i mauyane i Atanyondon perpere rimbe ñwalmeli ar náve *auquerna lo i amyárar ar i hére *airimor ar i parmangolmor, ar náve nanca, ar orta apa auri nelde.
Mal sa-lalanes, quétala: “Uan ista se ar uan hanya ya quétal!” Tá lendes ettenna i *andoşambenna.
Mal Yoháno, hlárala pa se mandosse, mentane vére hildoryainen
Sama cóle, tá, tenna i Heru tule! *Cemendur yéta ompa cemeno mirwa yávenna; mauya sen same cóle tenna i miste alantie mi quelle ar tuile véla.
Elde uar mi náha nóme aselme, mal véra endalda náha ná.
Ar enge olya *nurrule pa sé imíca i şangar. Quelli quenter: “Náse mane nér.” Exeli quenter: “Uas, mal tyaris i şanga ranya!”
Ar panyanes máryat tesse ar oante talo.
Ar sí, yé, nútina i fairesse lelyan Yerúsalemenna, ómu uan ista yar martuvar nin sasse,
Qui aiquen cene hánorya cára úcare ya ua tulya qualmenna, arcuvas, ar Eru antuva sen coivie – qui uas mici queni ion úcare tulya qualmenna. Eä úcare ya tulya qualmenna; uan quete i mo arcuva pa ta.
Hé náne antana olla ongwelvainen ar náne ortana návelvan quétine faile.
Sí yaldes i şanga insenna as hildoryar ar quente téna: “Qui aiquen mere tule apa ni, mauya sen váquete insen ar orta tarwerya ar ni-hilya.
Elve uar i nostale ya tucixe oa mir nancarie, mal i nostale ya same savie, sie cámala coivie.
ye Eru panyane ve *cámayanca ter savie serceryasse. Sie tanumnes failierya, apseniénen i úcari yar martaner i vanwiesse
Ar eques: “Masse apánieldes?” Quentelte senna: “Heru, tula ar cena.”
Laume! Elve i qualler pa úcare, manen en coituvalve sasse?
Mal queni te-cenner íre oantelte, ar rimbali sinter, ar ilye i ostollon nornelte tar talanen ar túler nó te.
Etta anelte rúşie senna. Mal Yésus quente téna: “Erutercáno ua pene laitie hequa vehteryasse ar coaryasse!”
Ar quentes téna: “Manen ná i nalde caurie? Ma en ualde same savie?”
an ómu sintelte Eru, ualte antane sen i alcar hya hantale yain náse valda ve Aino, mal sanweltar oller luste ar mornie quante úhanda endalta.
Nai i Aino ye anta raine euva as illi mici le! Násie!
Eques: “I úcárimar atanin nar cárime Erun.”
Horro i *lapsarwe nissin ar in *tyetir vinimor ente auressen!
Yé! Yando ciryar, ómu nalte ta hoe ar taure súri tyarir tai leve, nar túrine ampitya *tullanen, yanna i ciryaher mere.
Yando i ohtari lenganer senna yaiwenen. Túlelte hare ar antaner se sára limpenen
Ar mí imya lú, túlala amba et i nenello, cennes menel şanca ar i Faire ve cucua túla undu senna.
Mal sí, apa sintelde Eru – hya ambe mai quétina, sí íre anaielde sinwe lo Eru – manen ná i nanquerilde inde i milye ar penye natinnar ar merir náve móli tain ata?
Sie quenten vanda endanyasse: Ualte tuluva mir sérenya!"
Etta nanye valima íre nanye milya, íre perperin orme, íre samin maure, íre nanye roitaina hya urdiessen, Hriston. An íre nanye milya, tá nanye torya.
Nanwave quetin lenna i or ilye armaryar se-panyuvas.
ar quétala: “Heru, núronya caita *lalevíte i coasse, arwa rúcime ñwalmaron!”
I talmar i ostorambo náner netyaine ilya nostalénen mírion: I minya *talmondo *nambíre né, i attea lúle, i neldea *ostimmir, i cantea *laimaril,
Sí hortan le, hánor: Istalde i nosserya Stefánas ná i minya yáve Acaiasse, ar i panyaneltexer náve núror i airin.
Ar i nér yente ama ar quente: “Cénan i queni, an yétan ya *şéya ve vantala aldali!”
Ar inye, íre nauvan ortaina et cemello, tucuva ilye atani ninna.”
Yésus quente senna: “Inye i tie ar i nanwie ar i coivie. *Úquen tule i Ataranna hequa ter ní.
Mal qui Eruo fairenen inye et-hate i raucor, Eruo aranie é utúlie lenna.
Ananta i minya ua ta ya ná faireva, mal ta ya ná ermava; epeta tule ta ya ná faireva.
Etta rimbali i Yúraron i náner túlienwe Maríanna ar cenner ya carnes sáver sesse.
I attea vala lamyane hyólarya, ar nat ve haura oron, uryala nárenen, náne hátina mir ear;
Ata yámelte: “Áse tarwesta!”
Mal Saul, en şúyala *lurwe ar nahtie i Heruo hildonnar, lende i héra *airimonna ar arcane sello mentar i *yomencoain Lamascusse,
Lan cáranen nati sine – mi lenda Lamascusenna, apa camie túre ar lavie i hére *airimollon –
Ettelea quén laume hilyalte, mal uşuvalte sello, pan ualte ista i óma ettelearon.”
Ye mele coivierya nancare sa, mal ye yelta coivierya mar sinasse varyuva sa oira coivien.
Mal ye yelta hánorya ea i morniesse ar vanta i morniesse, ar uas ista yasse ménas, pan i mornie acárie henyat *cénelóre.
Mana i lú ya cennelme lye ve ettelea quén ar camner lye, hya helda, ar tumper lye?
Enger lúli yassen anelde pantave antaine yaiwelin ar şangiélin véla, ar lúlissen sámelde ranta as exeli i lender ter síti nati.
ar apa quentette i quetta Peryasse, lendette undu Attalianna.
Etta, qui Eru antane i imya anna tien ve elven i asávier mí Heru Yésus Hristo, man inye ná, i poluvan pusta Eru?”
Ente, nai i samir sávala heruvi uar nattiruva te pan nalte hánor. Úsie, mauya tien náve arye núror, pan i nar manyaine mára molieltanen nar queni i savir ar nar melde.Á peanta nati sine ar á horta sie!
An ve i Hristo perperier nar úvie vesse, sie i tiutale ya camilve ná yando úvea i Hristonen.
Yen pole tire le lantiello ar panya le alahaste epe alcarerya arwe túra alasseo,
i illi laituvar i Yondo ve laitalte i Atar. Ye ua laita i Yondo ua laita i Atar ye mentane se.
Á yéta, elde i quetir yaiwe, ar na quátine elmendanen, an carin carie réldassen – carie ya laume savuvalde yando qui aiquen nyare len pa sa!”
Sie i Aire Fea lave ven hanya i ua náne i tie mir i aire nóme carna sinwa íre i minya *lancoa tarne.
ar qui merilde came sa, náse Elía ye tulumne.
Taiti quenin antalme sina canwa ar hortale i Heru Yésus Hristonen: Á matir véra mastalta moliénen quildesse.
Ar i hildor túler senna ar quenter: “Mana castalya carpien téna sestiessen?”
Quentes téna: “Pitya savieldanen. An násie quetin lenna: Qui samilde savie ve erde *sinapio, quetuvalde oron sinanna: Leva silo tar, ar levuvas, ar munta nauva úcárima len.
Illume hantan Ainonya ire quetin pa lye hyamienyassen,
“Nyara men, mana i lúme yasse nati sine martuvar, ar mana i tanwa i ilye nati sine nauvar hari náven telyaine?”
Apa tarwestie se etsantelte larmaryar hatiénen *şanwali,
Elde nar i mardo cala. Osto orontesse ua cúvima.
Á cesta mana mára ná i Herun,
Ar yé! Canáanya nís ente nóriellon ettúle ar yáme, quétala: “Órava nisse, Lavirion! Selyenya ná ulcave haryana lo rauco!”
Mal ear mici le quelli i uar save.” An i yestallo Yésus sinte i uar sáve ar man né i quén ye *vartumne se.
Paulo inse merne lelya minna i lienna, mal i hildor úner mérala lave sen.
íre yando Eru *vettane tanwalínen ar elmendalínen ar taure carielínen ar etsatielínen aire feava, ve indómerya náne.
Óma queno ye yáma i ravandasse:Alde manwa i Héruo malle!Cara tieryar tére!
Mal oronter nelli i *yomencoallo estaina Leryainaiva, ar ennoli i túler Cireneallo ar Alexandriallo, costien as Stefáno.
ar Ásor óne Sároc, ar Sároc óne Ácim, ar Ácim óne Eliur,
I lómisse nó Herol se-tulyumne epe i lienna, Péter náne lorna, nútina naxenen atta imbe ohtar atta, ar cundoli epe i fenda hemper i mando.
ómu vali, i nar polde ar taure lá té, uar quete narace quettar namiéva ana te epe i Héru.
Sí lan Péter náne undu i pacasse, quén imíca i *núri túle,
Mal Yésus carampe tulcave senna, quétala: “Na quilda, ar ettula sello!”
An é Hristo, íre en anelve milye, qualle *ainolóre atanin íre i lúme túle.
An yú inye ná nér panyana nu túre, arwa ohtarion nu ni, ar qui quetin quén sinanna: Mena!, tá ménas, ar exenna: Tula!, tá túlas, ar núronyanna: Cara si!, tá carises!”
Ar i Erutercánoron quettar quetir i imya nat, ve ná técina:
Ar 'i nóre yan nauvalte móli inye namuva,' quente Eru, 'ar epeta etelelyuvalte ar caruvar núromolie nin nóme sinasse.'
Anaro alcar ná véra nostaleryo, ar Işilo alcar ná exe, ar tinwion alcar ná exe; é tinwe ua ve hyana tinwe mi alcar.
Etta yámes ar quente: 'Atar Avraham, órava nisse ar á menta Lásaro, panieryan leperyo tille nenesse, carien lambanya ringa! An nanye ñwalyaina náre sinasse!”
*Úquen ye usúcie yára limpe mere vinya, an quetis: 'I yára ná mára.'”
Ar mi yana lú henduttat nánet aqua pantaine, ar sintettes, mal vánes tullo.
Yoháno *vettane pa se, é etyámes: “Nér sina ná i quén pa ye quenten: Ye túle ca ni ná sí opo ni, pan enges nó ni.”
An lenna náne antaina, rá Hriston, lá eryave save sesse, mal yando perpere rá sen.
Úsie, íre túles Rómanna, ni-cestanes ú séreo ar hirne ni.
Qui Paulo hya Apollo hya Céfas hya i mar hya cuile hya qualme hya i nati yar ear sí hya i nati yar tuluvar – ilqua elde samir, íre elde nar i Hristova, mal Hristo ná Eruva.
I Atanyondo utúlie, mátala ar súcala, mal quetilde: 'Ela! Nér *accamátala ar antaina sucien limpe, meldo *tungwemoron ar úcarindoron!'
Mal matso ua vi-caruva mani Eruo hendusse; qui ualve mate, ualve loita, ar qui matilve, ualve arye.
Sie yú elde tyara calalda calta atannar, polieltan cene máre cardaldar ar antaveltan alcar Atareldan, ye ea menelde.
Enger tasse rimbe nisseli tírala hairallo, i hilyaner Yésus Alileallo *vevien sen.
Etta se-tultanes ar quente senna: 'Mana nat sina ya hlarin pa lye? Á anta onótie pa molierya ve mardil, an ualye mahtuva i coa ambe.'
Sé vi-rehtane ar vi-yalde aire yaliénen, lá castanen cardanyaron, mal castanen véra &mehtyo ar lisseryo, antaina ven Yésus Hristonen nó oialie lúmeli,
Mal exeli, yaiwesse téna, quenter: “Nalte quante lisse limpeo!”
Yésus quente téna: “Maquetuvan lello maquetie er. Alde hanquete nin, ar yando inye nyaruva len pa i hére yanen carin nati sine.
I híni Eruo ar i híni i Arauco nar apantaine sinen: Ilquen ye ua care failie ui Eruo, yando lá ye ua mele hánorya.
É man manwaxe mahtien qui i hyóla anta nulla lamma?
mal ennoli himyaner se ar sáver. Mici té náne Lionisio ye náne námo mí Areopahus, ar nís estaina Lamaris, ar exeli ara tu.
Mici té náner María Mahtaléne, yú María amil Yácovo ar Yósefo, ar i amil yondoron Severaio.
Etta tira harive i manen vantalde ua ve alasailar, mal ve sailar,
Úsie, mela ñottoldar ar cara márie ar á *yutya exeli, ú estelo *nancamiéva, ar *paityalelda nauva túra, ar nauvalde i Antaro yondor, an sé ná aşea in uar anta hantale ar i ulcain.
Yésus quente téna: “Násie, násie quetin lenna, nó Avraham enge, inye ea.”
Ente, i ear amoronte, pan polda súre náne hlápula.
Hairallo cennes *relyávalda arwa lasselion, ar lendes cenien qui cé hirumnes *relyáveli sesse. Mal íre túles senna, hirnes munta hequa lasseli, an ta úne i lúme *relyávion.
Ar lendes ettenna ar hilyane hé, mal uas sinte i ya martane i valanen náne nanwa. Intyanes i cennes maur.
Savie ná i varnasse nation pa yar mo same estel, i tanie nanwaron yar mo en ua cene.
Ente, qui i minya massa airinta ná, tá i quanda maxe aire ná. Ar qui i şundo aire ná, sie nar i olvar.
cárala munta meriénen cos hya cumna alcar, mal nalda sámanen nótala quén i exe ambe túra lá elde,
oira coivie in cestar alcar ar laitie ar ilfirin sóma, voronwiénen mára moliesse,
Ar yé, halmahlaiwa quén túle senna ar lantane undu epe se, quétala: “Heru, qui meril, polil poita ni!”
Ar i náner mentaine, íre nanwennelte i coanna, hirner i mól málesse.
Á anta, ar mo antuva len. Mára lesta – nírina undu, pálina ar úvea – panyuvalte súmaldasse. An i lestanen yanen *etelestalde, *etelestuvalte len.
Ortuvan ar lelyuvan atarinyanna, ar quetuvan senna: Atar, úacárien menelenna ar tyenna.
Qui sa lisse ná, uas ambe cardainen; qui lá, i lisse uas ambe lisse.
Ar Yésus, istala sanweltar, quente: “Manen ná i sánealde olce natali endaldasse?
Mal quérala inte sine natillon quelli anaier quérine cumna quetienna,
ar quenter: “Qui nalye Yúraron aran, á rehta imle!”
Ono ye enge i *núrenen náne nóna mí lé i hráveo, mal i exe lo i léra nís, vandanen.
Tá nyarnes sestie i nerin i náner tultaine, íre túnes manen cillelte i amminde nómi inten, quétala téna:
Aiquen ye care i indóme Atarinyo ye ea menelde, isse ná hánonya ar néşanya ar amillinya!”
Ar íre tecetta sina anaie hentaina mici le, tyara i nauvas hentaina yando i ocomben Laoriceasse, ar ñeta i tecetta Laoriceallo, lávala yando elden henta sa.
Sís ea maure sailiéva: Lava i handan note i hravano nóte, an nás atano nóte, ar nóterya ná enque ar *enenquean ar tuxar enque.
Mal savin i ea maure i mentan lenna Epafrolíto, hánonya ye mole ar mahta asinye, mal *lenya mentaina nér ar núro maurenyan.
Ar ve rongo ve ára túle, i héra *airimo as i amyárar ar i parmangolmor ar i quanda Tára Combe carner úvie, ar nuntelte Yésus ar tulyaner se oa ar antaner se olla Pilátonna.
Hanquentes senna: “Atarelma Avraham ná!” Eque téna Yésus: “Qui nalde Avrahámo híni, cara Avrahámo cardar!
Hya, “Man minyave antane sen ar camuva *paityale nan?”
Ar náse tópala *yanca úcarelvain, ananta lá *venyain erinque, mal i quanda mardo.
A heruvi, á anta mólildain ya faila ar vanima ná, istala i yando elde samir Heru menelde.
Ear er hroa ar er faire, ve anelde yáline i er estelde yalieldo,
Ar ata luhtala undu tences i talamesse.
Pa i vendi uan same canwa i Herullo, mal antan sanwenyar ve quén ye acámie i oravie i Herullo i nanye voronda.
Ar rimbali ion sáver túler ar quenter pantave pa cardaltar.
Illi i ear asinye *suilar lye. Á *suila i melir me i saviénen.Nai i Erulisse euva as illi mici le!
Etta yando elme, i aurello yasse hlasselme sa, uar upustie hyame ar arca len i nauvalde quátine istyanen indómeryo mi ilya sailie ar hande i faireo,
ar ye Aire Feanen quente ter i anto Lavir atarelmo, núrolya: 'Manen ná i nóreli amortar ar lieli sánear pa luste natali?
Etta ho sí ualme ista aiquen i hrávenen. Yando qui isintielme i Hristo i hrávenen, ualme ista se sie ambe.
Á aista i roitar – á aista ar áva húta!
Ar hautiéla Yésus quente: “Áse yale!” Ar yaldelte i lomba nér, quétala senna: “Huore! Á orta, lye-yálas!”
mal mir i attea şambe i héra airimo erinqua lelya, erya lúmesse mi loa er, lá ú serceo, ya *yacis insen ar i lieo úcarin yar nar cárine ú istyo.
Lá i cestean i anna, mal é cestean i yáve ya nauva nótina elden.
Ar mauya i Erutercánoin ture faireltar.
Epeta, íre caines ara i sarno i coasse, yé! rimbe *tungwemóli ar úcarindoli túler ar cainer tasse as Yésus ar hildoryar.
lendes erya lú ar tennoio mir i aire nóme, lá arwa serceo nyéniron ar nesse mundoron, mal arwa véra serceryo, *ñetala oira etelehtie ven.
ar ilya úfaila úşahtiénen i nancaruvainaiva, pan ualte camne i melme nanwiéva, náveltan rehtaine.
Valima ná i nér ye perpere tyastiesse, an aqua tyestaina camuvas i ríe coiviéva, pa ya i Heru ánie vanda in melir se.
Násie quetin lenna, ilqua ya nutilde cemende nauva nútina menelde, ar ilqua ya lehtalde cemende nauva lehtana menelde.
Mal hortalme le, hánor: Á tana i téra tie i ráneain, queta tiutalénen innar penir huore, á manya i nar milye, sama cóle illin.
Úsie, sine *yancainen ea enyalie úcariva loallo loanna,
Or ilqua, hánonyar, áva anta vandar – menelden hya cemennen hya ilya hyana vandanen. Nai quetilde "ná, ná" – "vá, vá", hya lantuvalde nu námie.
An apánien len emma, i ve inye acárie, yando lé caruvar.
An ilye i exi cestar vére nattoltar, lá tai Yésus Hristova.
An asáties ré yasse namuvas cemen failiesse nernen ye icílies, ar ánies ilye atanin varnasse ortavénen se et qualinallon.”
Ar sámelte apaire i Euleo sercenen ar *vettielto quettanen, ar coivieltar úner tien ta melde i avanelte vele qualme.
ar i quanda lie túle arinyave senna i cordasse hlarien se.
Ar hlassen taura óma menello quéta: “Sí utúlier i rehtie ar i túre ar i aranie Ainolvo ar i hére Hristoryo, an i *ulquéto hánolvaron hátina undu ná, ye te-*ulquente mi aure yo lóme epe Ainolva!
– ar elde uar ista manen coivielda nauva enwa! An nalde híşie, cénina mi şinta lú ar tá autala.
An ilya onna Eruo mára ná, ar ua ea maure quere oa *aiqua qui nas cámina hantalénen,
Neri, hánor, nin ná lávina quete lérave lenna pa i *atartur Lavir, i effirnes ar náne talana sapsaryanna, ar noirirya ea mici vi tenna aure sina.
Mal hrangielyanen ar endanen pen inwis comyal lyen rúşe i auresse rúseva, íre Eruo faile namier nauvar apantane.
Ar timbareryasse náne técina esse, fóle: “Vável Túra, amil *imbacindion ar i şaure nation cemeno.”
An quelli fincave utúlier minna, i andanéya náner sátine námien lo i Tehtele – olcali, i querir i Erulisse mir ecie lehta lengiéno, ar lalar erya Cánolva ar Herulva, Yésus Hristo.
Mal mo nyarne sen: “Amillelya ar hánoldar tárar i ettesse, mérala vele lye.”
Inye ná i mane mavar, ar istan mámanyar ar mámanyar istar ní,
Nucuma inde mí Héruo hendu, ar le-ortuvas ama.
An Eru ua cime cendeler.
An ta yú ná i casta yanen paityalde *tungwi, an nalte Eruo núror, oi cimila sina enne.
Quetilde ninna, *Peantar ar Heru, ar quetilde mai, an ta nanye.
An or ilqua istalde i ua ea apacen i Tehtelesse ya tule aiqueno véra tercenello.
– ve yú i Atanyondo ua túle náven *veuyaina, mal *vevien ar antaven cuilerya ve *nanwere quaptalesse rimbaiva!”
Ente, queni tuluvar Rómello ar Númello ar Formello ar Hyarmello, ar caituvalte ara i sarno mi Eruo aranie.
Mal qui ualde save tecieryar, manen savuvalde ninye quetier?”
Mal elye, ire hyamil, mena mir véra şambelya, ar apa holtie fennalya, hyama Atarelyanna ye ea nuldasse; tá Atarelya ye nuldave tíra, paityuva lyen.
Nanwave, íre samilve ecie, nai caruvalve márie ilquenen, ar or ilqua in nar nosselva i saviesse.
Ar carampes ar quente senna: “Nai *úquen oi matuva yáve lyello ata!” Ar hildoryar hlasser sa.
Ar lava Erutercánoin atta hya nelde anta quettaltar, ar lava i exin name ya quétalte.
Ar tá cenuvalte i Atanyondo túla fanyasse arwa túreo ar túra alcaro.
Hánor, hyama yú elmen!
ar nahtuvaltes, ar i nelya auresse nauvas ortana!” Etta sámelte lunga naire.
Ar i hyamie saviéva tulúva mále i caimeassean, ar i Héru ortuva se. Ente, qui acáries úcareli, ta nauva sen apsénina.
Eque senna Yésus: “Ma uan quente lyenna i qui savil, cenuval Eruo alcar?”
Yésus quente senna: “Elye equétie sa. Mal quetin lenna: Ho sí cenuvalde i Eruion hára ara i Túreo forma ar túla menelo fanyassen!”
Etta na coive, an ualde ista i lú yasse i *coantur tuluva, telwa hya endesse i lómio hya íre i tocot lamya hya arinya
Á nesta hlaiwar, á orta qualinar, á poita halmahlaiwar, et-hata raucor! Acámielde muntan, á anta muntan!
Íre ualme cenne Anar hya tinwi rimbe rélissen, ar lá pitya raumo caine menna, firne ilya estel rehtielmo.
Sí hildoryar nanwenner, ar engelte elmendasse pan carampes as nís; ananta *úquen quente: “Mana cesteal?” hya “Mana i casta yanen quétal óse?”
Estel sina samilve ve *ciryampa i fean, tulca ar tanca, ar rahtas han i fanwa,
Mal i Héruo vala quente Filipenna, quétala: “Á orta ar mena hyarmenna i mallenna ya mene undu Yerúsalemello mir Ása.” Ta i ravandasse ná.
Laume carilme *aiqua ya queruva queni oa, lá mérala i núromolielma nauva vahtaina.
Mal Paulo quente téna: “Rípelte met epe i lie, ú námiéno, neri i nar Rómear, ar hantelte met mir mando – ar sí et-hátalte met nuldave? Laume! Lava tén tule ar tulya met ettenna!”
Apa láves ta, Paulo, tárala i tyellessen, léve máryanen i lien. Íre tumna quilde lantane, carampes téna i Heverya lambesse, quétala:
Ye se-antumne ollo antane tien nóvo tanwa, quétala: “I quén ye miquin, ta ná sé; áse mapa ar á tulya se oa varnave.”
Ar íre túlelte i şanganna, nér túle senna, lantala occaryanta epe se
Voronweldanen rehtuvalde coivieldar.
Etta, pan camuvalve *alapálima aranie, nai en samuvalve Erulisse, yanen lertalme *veuya Erun mi *ainocimie ar áya.
ar uas láve aiquenen cole tamma ter i corda.
Ar yé! quén túle senna ar quente: “*Peantar, mana i márie ya mauya nin care, camien oira coivie?”
Yerúsalem, Yerúsalem, ye nahta i *Erutercánor ar *sarya i nar mentaine senna – manen rimbave mernen comya hínalyar, ve poroce comya nessaryar nu rámaryat! Mal ualde merne.
Áva ranya: Mo ua lerta quete yaiwe Erunna. An ya nér rere, ta yando *cirihtuvas,
Etta maquetin: Ma talantelte lantien aqua? Laume! Mal taltieltanen ea rehtie i nórin, tyarien *hrúcen mici te.
Qui háno hya néşa ná helda ar pene matso i auren,
Mentanes i quetta Israelindinnar carien sinwa tien i evandilyon raineva ter Yésus Hristo; isse Heru illion ná.
Ar ye lanta ondo sinanna nauva rácina; mal aiquen yenna lantas – aqua ascatuvas se.
Mal antala canwar sine uan laita le, an ualde tule uo márien, mal ulcun.
Sie Péter ununte ar quente i nerinnar: “Yé, inye ná ye cestealde. Mana castalda tulien sir?”
An ilquen nauva carna singwa singenen.
Ar i vala quente senna: “Áva ruce, María, an ihíriel lisse as Eru,
Ar yé! enge tasse nér arwa hessa máo.
Tá rimbali caramper tulcave senna náveryan quilda, mal i ambe yámes: “Lavirion, órava nisse!”
Íre i aure aşaro Otsolaron Otso náne tulinwa, anelte illi uo mi er nóme,
Ar hanquentasse i lie quente: “Nai sercerya tuluva menna ar hínalmannar!”
An istalme i qui coalma mariéva cemende nauva nancarna, camuvalme ataque Erullo, coa lá cárina mainen, oialea mi menel.
Lendelte mir i coa ar cenner i hína as María amillerya, ar lantala undu luhtanelte senna. Pantanelte harmaltar ar antaner sen annali: malta ar *ninquima ar níşima suhte.
Yésus hanquente téna: “Ma ua técina Şanyeldasse: Quenten: Nalde ainoli – ?
Hlárala ta, quén mici i cainer ara i sarno quente: “Valima ná ye mate massa mi Eruo aranie!”
Ar sé i mityallo quete hanquentasse: 'Ávani *tarasta! I fenna ná pahta, ar hínanyar nar asinye i caimasse; ua ece nin orta antien lyen.'
Ar ya cáran yando caruvan, lá antien ecie in merir ecie, náveltan hírine vávie aselme mi ta pa ya laitaltexer.
Etta ennoli mici te sáver ar himyaner Paulo ar Sílas, as hoa liyúme i Hellenyaron i *tyerner Eru, ar lá mancali i amminde nission.
An ulyanelte serce airion ar Erutercánoron, ar ániel tien serce sucien. Nalte valde!”
an ecénies *núreryo naldie. Ar yé! ho sí ilye *nónari estuvar ni valima,
I Yúrar hanquenter senna: “Samilme şanye, ar sana şanyenen náse valda qualmeo, pan acárien immo Eruo yondo.”
Istalve in ilqua ya i Şanye quete, quetis innar nar nu i Şanye, pustien ilya anto, ar i quanda mar nauva *námima Erun, paimen.
I mende mena i vanwe mámannar Israélo coallo!
Lúme yanallo Yésus *yestane tana hildoryain i mauyane sen lelya Yerúsalemenna ar perpere rimbe natali ho i *amyárar ar parmangolmor, ar náve nahtana, ar i nelya auresse náve ortana.
Ar sie técalme nati sine, i alasselma nauva quanta.
I Yúrar quenter senna: “Sí é istalme i nalye haryaina lo rauco. Avraham qualle, yando i Erutercánor, mal elye quete: Qui aiquen himya quettanya, laume oi cenuvas qualme.
Mana, tá, arya ná i Yúran, hya mana aşea pa i *oscirie?
Áva ruce i ñwalmallon yar sí tuluvar lyenna. Yé! I Arauco sí hatuva queneli mici le mir mando, i nauvalde aqua tyastaine, ar i samuvalde şangie ter rí quean. Na voronda tenna ñuru, ar lyen-antuvan i ríe coiviéva.
Ar íre i hlóce cenne i anes hátina undu cemenna, roitanes i nís ye colle i seldo.
Ar i Quetta ahyane mir hráve ar marne imíca me, ar cennelme alcarerya, taite alcar ya *ernóna yondo same ataryallo; ar anes quanta Erulisseo ar nanwiéno.
Qui ondo muliéva náne panyaina *os axerya, ar anes hátina mir i ear, ta náne sen arya lá tyarie i lante ero mici pityar sine!
quelli quenter: “Náse sé.” Exeli quenter: “Ui, mal náse ve sé.” I nér quente: “Inye ná sé!”
Sie yando istuvalde, íre cenuvalde nati sine martea, i Eruo aranie hare ná.
Ar carnes úvie sámaryasse, quétala: 'Mana caruvan, sí ire penin nóme yasse polin hosta yávienya?'
Talo orontes ar lende mir i ménar Tíro ar Sírono. Ar lendes mir coa ar ua merne i aiquen istumne, mal uas polde lemya muina.
Istalve i aiquen nóna Eruo ua úcare, mal i Quén ye náne nóna Eruo himya se, ar i Olca ua appa se.
Mal qui aiquen atyárie nyére, uas atyárie sa inyen, mal mi lesta – lá carpien mi acca naraca lé – illin mici le.
Rie Lúcas lemya óni. Á tala Marco ar áse care tule aselye, an náse aşea nin mi núromolienya.
Ar i şangar yar lender epe se ar i hilyaner yámer: “Hosanna yondon Laviro! Aistana ná ye túla i Heruo essenen! Hosanna i tarmenissen!”
Ono sannelte i pulumnes, hya i lantumnes rincanen qualina. Apa hopie andave ar cenie munta olca martala sen, vistanelte sámalta ar quenter i náse aino.
ar quenter senna: “Queta men mana túrenen cáral nati sine, ar man ánie lyen túre sina!”
Lá peniénen túre, mal carien imme *epemma len, carnelme sie – carieldan ve elme carner.
Mal yando Yúras, ye *vartane se, sinte i nóme, pan Yésus rimbe lúlissen velde as hildoryar tasse.
Áva anta aiquenen ulco ulcun! Á tala ompa mánar epe ilye atani!
Nai i Heru euva as fairelya! Nai lisserya euva aselde!
Tá Yésus pustane ar canne i tulyumnelte i ner senna. Apa túles hare, Yésus quente:
Sí aure atta lemnet nó i Lahtie ar i Aşar *Alapulúne Massaron. Ar i hére *airimor ar i parmangolmor cestaner manen mapumnelte Yésus curunen ar se-nahtumner.
laitieldan er antonen Yésus Hristo Herulvo Aino ar Atar.
Cima manen i indili alir; ualte móta hya lanya, mal nyarin len: Yando Solomon ilya alcareryasse úne netyaina ve er mici té!
Ar apa túles undu i orontello hoe şangar hilyaner se.
Ear *alavéle nostaléli núrumoliéva, mal i Heru ná i imya.
Tá túles Lervenna ar yú Listranna. Ar yé! enge tasse hildo yeo esse náne Timoşeo, i yondo savila Yúra nisso, mal Hellenya ataro.
Apa quetie nati sine, Yésus *etemenne as hildoryar olla i nelle estaina Cirron, nómenna yasse enge tarwa, ar sé ar hildoryar lender minna sa.
Ar mentanes núrorya i ahtumatto lúmesse, quetien innar náner tultaine: “Tula, an sí ilqua manwa ná!”
Mal sanalme i vanima ná hlare lyello mana sanwelyar nar, an é istalme pa heren sina i mi ilya nóme mo quete ana sa.”
Ar íre cestenelte nahta se, quetta túle i turconna i hosseo pa quanda Yerúsalem nála rúcina.
Ye lasta lenna lasta ninna. Ar ye loita cime lé, loita cime ní. Ente, ye loita cime ní, loita cime ye ni-mentane.”
ve yando inye care illi valime mi ilye nati – lá cestala ya ná aşea nin, mal ya nauva aşea i ambe rimbain, náveltan rehtaine.
Mal qui aiquen ua anta véraryain – ar or illi tien i nar coaryasse – maureltar, alálies i savie ar ná faica lá quén ye ua save.
Ar nampen i pitya parma ho i valo má ar sa-mante; anes lisse ve lís antonyasse, mal apa mantenyes cumbanya náne carna sára.
Qui mi coivie sina erinqua apánielve estelelva Hristosse, nalve mici ilye atani i anvalde *ofelmeo.
Saul, ye ná yú Paulo, náne quátina Aire Feanen, yente se
Istan şangielya ar únielya – mal nalye lárea – ar i yaiwe ion estar inte Yúrar, ananta ualte, mal nar Sátano *yomencoa:
Ananta, qui é namin, námienya nanwa ná, pan uan erinqua, mal i Atar ye ni-mentane ea óni.
Apa anda lúme i heru sane mólion túle ar sáme onótie aselte.
An istalve i sí, íre i Hristo anaie ortana qualinallon, uas quale ata; qualme ua ambe same túre or se.
Yésus hanquente ar eque: “Pan anyárien lyen i cennen lye nu i *relyávalda savil? Cenuval natali túre lá ta.”
Nó telyanes quete, quelli túler i coallo i *yomencoanturwa ar quenter: “Selyelya ná qualin. Manen ná i en *tarasteal i *peantar?”
ar á mapa i carma rehtiéva ar i macil faireva, ya Eruo quetta ná,
Tana mehten é hyámalme len illume, arcala i Ainolva notuva le valde yalieryo ar telyuva ilqua ya meris – márie ar cardar saviéno – túresse,
Ar ye hande i mahalmasse quente: “Yé! Envinyatan ilye nati!” Ar eques: “Teca, an quettar sine nar voronde ar şande!”
Té carir i Hristo sinwa et melmello, an istalte i anaien panyaina sisse varien i evandilyon.
Ono manen yaluvalte senna yesse ualte save? Ar manen savuvalte sesse pa ye ualte ahlárie? Ar manen hlaruvalte qui penilte aiquen ye care sinwa i evandilyon?
Íre túles coanna i turwa ar cenne i simpetari ar i şanga cárala yalme,
I savie ya samil, hepa sa imbe lyé ar Eru. Valima ná i quén ye ua tala namie insenna yanen quetis mára.
Lau elye ná túra lá Yácov aterelma, ye antane men i tampo ar ye immo sunce sallo as yondoryar ar celvaryar?”
Etta quentelte, quén i exenna: “Ávalve narca sa, mal alve hate *şanwali pa man samuva sa” – *amaquatien i tehtele ya quete: “*Ciltanelte larmanyar mici inte, ar pa laupenya hantelte *şanwali.” Ar i ohtari é carner sie.
Hanquenten: 'Man nalye, Heru?' Ar quentes ninna: 'Nanye Yésus Nasaretello, ye elye roita.'
Uan mere i penilde istya sina, hánor, i atarilvar náner illi nu i lumbo ar illi lahtaner ter i ear
Mal tanuvan len manello mauya len ruce: Ruca yello apa nehtie same i túre hatiéva mir Ehenna. Ná, quetin lenna, ruca sello!”
An i Atanyondo ná Heru i *sendareo!”
Mentalma elde ná, técina eldalmasse ar sinwa ar cendaina lo ilye atani.
Tá Yésus quente téna: “Maquetin lello: Ma ná lávina i *sendaresse care márie hya care ulco, rehta quén hya nancare se?”
Eques: “Etta nai i taurar mici le tuluvar undu quetien pa ongweryar, qui ea *aiqua úşanya pa se.”
Ente, íre hyamilde, áva na ve i *imnatyandor, an melilte hyame íre táralte i malleron vincassen, náven cénime atanin. Násie quetin lenna: Cámalte quanda *paityalelta.
Á orta, alve mene! Ela! Ye anta ni olla utúlie hare!”
Mal i hildor maquenter: “Tá manen ná in i parmangolmor quetir in Elía maurenen tuluva minyave?”
Ar i ulo ulle undu, ar i oloiri túler, ar i súri vávaner ar penter coa tananna, mal uas lantane, an anes tulcana i ondosse.
An quetin lenna: Ho sí ualde ni-cenuva tenna quetuvalde: Aistana ná ye túla i Héruo essenen!”
An Eruo annar ar yalie uar nati pa yar samuvas tatye sanwi.
Mi tai rimbe caine: hlaiwali, lombali, *úlévimáli ar hessali, *lartala coiren i nenwa.
ar ilye natinyar nar *tyenye ar *tyenyar nar ninye, ar anaien *alcaryaina mici te.
Eque senna Tomas: “Heru, ualme ista yanna auteal. Manen istalme i tie?”
Tá landelte senna yaiwenen. Mal sé, apa mentie te illi ettenna, talle i híno atar ar amil ar i náner óse, ar lendes i şambenna yasse náne i hína.
Rimbali quetuvar nin enta auresse: 'Heru, Heru, ma ualme quente ve Erutercánor esselyanen, ar et-hanter raucor esselyanen, ar carner rimbe taurie cardali esselyanen?'
Yétala mina sa tirnen ar cenne cemeno *cantalyar ar i hravani celvar ar i hlicilar ar menelo aiwi.
An apacen úne oi tulúna atano nirmenen, mal atalli caramper Erullo, nála cóline Aire Feanen.
á rehta imle tuliénen undu i tarwello!”
Sie i hildor lender ar carner ve Yésus canne tun.
Qui quetilve i ualve same úcare, *útulyalve inwe ar i nanwie lá ea vesse.
ar sie i şanga náne quanta elmendanen íre cennelte i úpar quéta ar i *úlévimar vantea ar i lalevíti céna, ar antanelte alcar Israélo Ainon.
Ar rimbe hyane naiquetiéli quentelte senna.
cárala sinwa ven indómeryo fóle, mára nirmeryo cilmenen:
“Man ná i nér mici le arwa mámaron tuxa ye ua, qui er mici te ná vanwa, hehtuva i nerte *neterquean i erumasse ar cestuva i vanwa máma tenna hirises?
Mára alda ua pole cole úmirwa yáve; mi imya lé, quelexima alda ua pole cole mára yáve.
An elde istar mai in i Héruo ré tuluva ve arpo i lómisse.
ar eque senna: “Mena, sova immo i ailinde estaina Siloam” – ya tea Mentaina. Ar sie oantes ar sóve immo, ar nanwennes cénala.
Áva fele elmenda, hánor, qui i mar yelta le.
Mal Piláto quente téna: “Tá mana i ulco ya acáries?” Ono té yámer rúcima yalmenen: “Áse tarwesta!”
Etta, i amyárain mici le antan hortale sina – an yando inye ná amyára nér aselde, ar astaro i ñwalmion i Hristo, quén ye same ranta i alcaresse ya nauva apantaina:
Hildo ua or ye peanta se, hya mól or herurya.
Lúme yanasse i hildor túler hare Yésunna ar quenter: “Man nanwave ná i antúra menelo araniesse?”
An Eru ná ye canne: "Cala á calta et i morniello", ar sé acaltie endalmanna, calyala i istya pa Eruo alcar mi Yésus Hristo cendele.
Ar nís, ye ter loar yunque perpére celuménen serceva, yen ua ence ñete nestie ho aiquen,
Ar se-tulyanes oa i şangallo eressea nómenna ar panyane leperyar mir i nero hlaru ar, apa piutie, appanes lambarya.
Ar rimbali ocomner, tenna lá enge amba nóme, yando lá epe i fenna, ar quentes téna i quetta.
Inye Avrahámo Aino ar Ísaco Aino ar Yácovo Aino – ? Uas qualinaron Aino, mal coirearon!”
Ar tulyanettes Yésunna, ar hantette collattar i *pellopenna ar panyanet Yésus sesse.
Mal quentelte senna: 'Heru, samis minar quean!'
Quetin lenna i mí imya lé, íre er úcarindo hire inwis, ea menelde alasse túra lá ya ea pa úcarindor nerte *neterquean i uar same maure inwisteva.
