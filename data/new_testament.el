Paulo, Yésus Hristo apostel Eruo indómenen, i airinnar i ear Efesusse, voronde mi Hristo Yésus:
Nai samuvalde lisse ar raine ho Eru Atarelva ar i Heru Yésus Hristo.
Aistana na Eru, Yésus Hristo Herulvo Atar, ye vi-aistie ilya aistiénen i faireo mí meneldie nómi Hristosse,
ve vi-cilles sesse nó i mar náne tulcaina, návelvan airi ar ú mordo epe sé melmesse.
An nóvo vi-santes náven cámine ló Yésus Hristo ve yondoryar, ve sannes mai indómeryanen,
mir laitale alcarinqua lisseryava ya antanes ven Meldaryanen.
Sénen samilve i *nanwere serceryanen: i apsenie úcarilvaiva, lisseryo larnen.
Vi-tyarnes same úve ilya sailiéva ar handeva,
cárala sinwa ven indómeryo fóle, mára nirmeryo cilmenen:
tulcie pano ya carumnes anwa íre i lú náne manwa, hostien uo ilye nati i Hristosse – i nati menelde ar i nati cemende – sénen.
Sesse anelve yando tulcaine ve aryoni, návelvanen nóvo sátine i mehtenen yeo mole ilqua indómeryo cilmenen,
návelvan laitale alcareryava, elve i nóvo sámer estel i Hristosse.
Mal yando elde sámer estel sesse apa hlasselde i quetta nanwiéva, i evandilyon rehtieldo. Sénen, apa sávelde, anelde yando *lihtaine vandaryo Aire Feanen,
ya ná tanwa rantalvo ve aryoni, ve *nanwere véra lieryan, alcarinqua laitaleryan.
Etta yando inye – pan ahlárien pa savielda mí Heru Yésus ar melmelda ilye i airiva –
ua pusta anta hantale pa le íre enyalin le hyamienyassen,
arcala i Yésus Hristo Herulvo Aino antuva len faire sailiéva ar apantiéva samieldan istya pa se,
endaldo hendu nála calyaine, istieldan mana i estel ná yan yalles le – mana i alcarinqua lar ná ya asáties i airin i nauvar aryoni san,
ar mana túreryo úvea túrie ná, elven i savir – i moliénen taura túreryo,
yanen móles i Hristosse íre se-ortanes et qualinillon ar tyarne se hame ara formarya i meneldie nómessen,
haira or ilya turie ar túre ar hére ar ilya esse estaina, lá sina randasse erinqua, mal yando mí túlala.
Ar ilqua panyanes nu talyat, ar carneses i cas ilye nation i ocomben,
ya ná hroarya, i quantie yeo quate ilqua ilye natissen.
Ar elde Eru carne coirie ómu anelde qualini mi ongweldar ar úcareldar,
yassen vantanelde yá, randa sino lénen, i lé i turo ye same túre or i vilya, i faire ya sí mole i yondossen *canwaraciéno.
Mici té yando elve yá lenganer hrávelvo milminen, cárala i hráveo íri ar sanwi, ar nassenen anelve rúşeo híni, ve náner yando i exi.
Mal Eru, nála alya mi oravie, túra melmeryanen yanen méles vi,
vi-carne coirie as i Hristo, yando íre anelve qualini ongwelissen. Lissenen anaielde rehtaine,
ar vi-ortanes uo ar láver ven mare uo i meneldie nómessen, mi Hristo Yésus.
Sie, i túlala randassen, lisseryo lahtala lar nauva tanaina i lénen yanen anaies nilda ven mi Hristo Yésus.
An lisseryanen anaielde rehtaine, saviénen, ar ta ua martane eldenen; i anna ná Eruo,
uas cardainen, pustien aiquen laitiello inse.
An nalve *senya tamna ar náner ontaine mi Hristo Yésus máre cardain, yar Eru manwane nóvo, vantielvan mi tai.
Etta enyala i yá anelde queni i nórion pa i hráve. Anelde estaine *Úoscirie lo ta ya ná estaina *Oscirie, carna i hrávesse mainen.
An anelde mi yana lúme ú Hristo, ettelie nossen Israélo ar ettelear i vérin i vandava, ar ualde sáme estel ar náner ú Eruo i mardesse.
Mal sí, mi Yésus Hristo, elde i yá náner haire nar sí talaine hari, i Hristo sercenen.
An sé rainelva ná, sé ye carne yúyo er ar nancarne i enya ramba imbe tu. I cotya sóma hráveryanen
nampes oa, i Şanye axanion mi canwali, carien insesse i lie atta mir vinya atan er, cariénen raine –
carien yúyo ú costo as Eru mi hroa er i tarwenen; an nacantes i cotya sóma insenen.
Ar túles ar carne i evandilyon raineva sinwa len i náner haire, ar raine in nar hari,
an ter sé engwe, yúyo lie, lertat tule hare, mi faire er, i Atarenna.
Etta ualde ambe ettelear ar queni i marir nóresse lá véralda, mal nalde asambaror as i airi ar marir coasse Eruva,
ar anaielde carastaina ama i talmasse i apostelíva ar i Erutercánoiva, íre Hristo Yésus inse i ondo i vinco ná.
Mi sé i quanda ataque ná panyaina uo ar ale mir aire corda i Herun.
Mi sé yando elde nar carastaine ama uo mir nóme yasse Eru maruva fairenen.
Etta yando inye, Paulo, ye Yésus Hriston mandosse ná rá elden i nar i nórion ...
– qui é ahlárielde pa i ortírie i Erulisseva ya náne nin antaina márieldan,
manen apantiénen i fóle náne nyárina nin, ve tencen şintave nóvo.
Sie ece len, cendala tecettanya, hanya i tercen ya samin i fóleva i Hristo.
Yane *nónaressen fóle sina úne carna sinwa i atanyondoin ve sí fairenen anaies carna sinwa airi apostelyain ar tercánoryain:
manen queni i nórion nauvar aryoni ar hroarantar ar samuvar ranta aselme i vando, mi Yésus Hristo, i evandilyonnen.
Anaien carna núro evandilyon sinan i lissenen Eruo, ya anaie antaina nin túreryanen mólala nisse.
Inyen, nér pitya lá i ampitya ilye i airion, lisse sina náne antaina, carienyan sinwa i nórin i evandilyon pa i lar han lesta i Hristova,
ar tanien manen Eru care anwa i fóle ya oiollo anaie nurtaina Erusse ye ontane ilye nati,
sí carien sinwa, ter i ocombe, in túrar ar samir hére meneldie nómessen Eruo *lincantea sailie,
i oira mehtenen ya carnes mí Hristo, Yésus Herulva.
Sénen samilve verie ar lertar tule hare ú ruciéno, savielvanen sesse.
Etta arcan: Áva quere inde mehteldallo sine şangienyainen rá elden, yar nar alcar len.
Sina castanen lantan occanyanta epe i Atar,
lo ye ilya nosse mi menel cemenye ná estaina.
Sie, et laryallo polis anta len faireryanen náve cárine taure melehtenen mitya ataneldasse,
tyarien i Hristo mare indoldasse savieldanen, melmesse arwe şunduo ar talmo,
ecien len hanya, as i airi, i landie ar andie ar tárie ar núrie,
ar istieldan i Hristo melme ya lahta istya, náveldan quátine Eruo quantiénen.
Yen pole care ambela ilqua ya arcalve hya sanalve, túreryanen ya mole vesse,
sen na i alcar i ocombenen ar Hristo Yésunen mi ilye *nónari tennoio ar oi! Násie.
Etta inye, ye ea mandosse castanen i Heruo, arca lello: Á vanta mi lé valda i yaliéno yanen anelde yáline,
arwe quanda naldiéno sámo ar milya léo, perpérala quén i exe melmenen,
holmo návala hepe i *erie i faireo nútenen raineva.
Ear er hroa ar er faire, ve anelde yáline i er estelde yalieldo,
er Heru, er savie, er *tumyale,
er Aino ar Atar illion, ye ea or illi ar ter illi ar mi illi.
Mal ilya erya quenen mici vi i lisse anaie antaina, i lestanen yanen i Hristo anna etsátina ná.
Etta quetis: “Íre lendes ama tárienna tulyanes oa queneli mandosse; antanes annali atanin.”
Mana i quettar “lendes ama” tear, qui lá i yando lendes undu i ambe núre ménannar, ta ná, cemenna?
Ye lende undu ná yando ye lende ama or ilye menelinnar, antieryan quantie ilye natin.
Ar antanes queneli ve apostelli, queneli ve Erutercánoli, queneli quetien i evandilyon, queneli ve mavalli ar *peantalli.
Sie manwanes i airi molieltan ve núror, carastien ama i Hristo hroa,
tenna illi mici vi nauvar er savielvanen ar istielvanen i Eruion – návelvan ilvana atan, samielvan i quanda táriéno i quantiéno Hristova.
Sie ualve nauva lapsi ambe, hátine ama ar undu falmalínen ar cóline sir ar tar ilya súrinen peantiéva, atanion fintalínen, i curunen autiesse loimava.
Mal quétala i nanwie, nai aluvalve ama melmenen mi ilye nati mir ye i cas ná: Hristo,
et yello i quanda hroa ná panyaina uo ar carna er, ilya lequet napánala molierya i lestanen ilya erya ranto. Sie i hroa tyarixe ale, carastala insa melmesse.
Nat sina etta quetin ar *vettan len i Herusse: Áva vanta ambe ve i nóri yando vantar luste sámaltainen,
handelta nála morniesse, ettelear i coivien Eruo, i úhandenen ya ea tesse ilaica endaltanen.
Apa tuntierya máriéva ar ulcuva firne, antaneltexer lehta lengien, molien ilya nostaleo úpoicie milciesse.
Mal sie elde uar apárie pa i Hristo,
qui é hlasseldes ar náner peantaine lo sé, ve nanwa ná Yésusse:
Á panya oa ya hilya vanwa lengieldo, i yára atan hastaina şahtala írelínen,
náveldan envinyante i fairesse sámaldo
ar panieldan indesse i vinya atan ye náne ontaina Eruo indómenen, i nanwiéno failiesse ar airesse.
Etta, sí íre apánielde oa huru, queta nanwie, ilquen mici le as armarorya, an nalve ilquen i exeo hroarantar.
Na rúşie, ananta áva úcare; áva lave Anaren núta rúşeldanna.
Ente, áva anta i Araucon nóme.
Mauya i pilun pusta pile, mal nai moluvas máryanten cárala ya mára ná, lávala sen same nat etsatien in nar mauresse.
Áva lave şaura quettan tule et antoldallo, mal queta ilya quetie ya mára ná carastien ama, ve i maure ná, antien lisse in lastar.
Ente, áva tyare nyére Eruo aire fean, yanen anaielde *lihtaine i auren *nanwereva.
Nai ilya sárie ar rúşe ar orme ar yamie ar naiquetie nauvar mapaine oa lello, as ilya ulco.
Mal na méle quén i exen, arwe *ofelmeo, apsénala quén i exen ve Eru mi Hristo apsenne len.
Etta, na ve Eru, ve méline híni,
ar á vanta melmesse, ve yando i Hristo méle le ar antane inse rá len, ve anna ar *yanca Erun, níşima olmen.
Nai ilya nostaleo *hrupuhtie ar úpoicie, hya milcie, ua nauva estaina mici le, ve mára airin ná.
Sie yando pa núra lengie ar *auca quetie hya váre *tuncinyarnar, yar uar mára haime; arya ná antie hantale.
An nat sina istalde: Hanyalde i *hrupuhtala hya úpoica hya milca quén – ya tea i *tyeris cordoni – ua nauva aryon i Hristo ar Eruo araniéno.
Áva lave aiquenen tyare le ranya luste quettainen, an sine natainen Eruo rúşe túla innar uar *canwacimye.
Etta áva same ranta aselte.
An yá anelde mornie, mal sí nalde cala, i Herusse. Á vanta ve híni calo,
an i calo yáve ná ilya nostaleo márie ar failie ar nanwie.
Á cesta mana mára ná i Herun,
ar áva same ranta i *yávelóre cardassen morniéno; carilde arya naityala tai.
An yar martar tainen ná *naityale yando nyare.
Mal i nati naityaine nar apantaine i calanen,
an ilqua ya ná apantaina, cala ná. Etta quetis: “Na coiva, elye ye húma, ar á orta qualinillon, ar i Hristo caluva lyenna.”
Etta tira harive i manen vantalde ua ve alasailar, mal ve sailar,
mancala inden i mára lú, an i auri nar ulce.
Etta áva na úhande, mal á tunta mana i Heruo indóme ná.
Ente, áva quate inde limpenen, ya tyare *immoturie auta, mal na quátine fairenen,
quétala indenna airelírínen ar laitalínen Erun ar fairelírínen, lindala ar cárala líri endaldasse i Herun,
Herulva Yésus Hristo essenen hantala illume pa ilye nati, Eru i Ataren.
Á panya inde nu quén i exe, ruciesse Yésus Hristollo.
Mauya i verin panya inte nu verultar, ve panyaltexer nu i Heru,
an veru ná veriryo cas ve yando i Hristo ná cas i ocombeo, sé nála ye rehta hroarya.
Mal tambe i ocombe panya insa nu i Hristo, síve nai i nissi caruvar verultain ilquasse.
A veruvi, mela verildar, ve i Hristo yando méle i ocombe ar antane inse san,
airitieryan sa, poitala sa i sovallenen nenwa i quettanen,
panieryan i ocombe epe inse alcarinqua, ú mordo hya *helmunque hya *aiqua ve ta, mal náveryan aire ar ú vaxeo.
Sie yando i veruion rohta ná melie i nissi ve vére hroaltar. Ye mele verirya mele inse,
an *úquen oi téve véra hráverya, mal antas san matso ar ná mára san, ve i Hristo yando care i ocomben,
an nalve rantali hroaryo.
“Etta nér hehtuva atarya ar amillerya, ar himyuvas veriryanna, ar i atta nauvat hráve er.”
Fóle sina túra ná – inye quéta pa Hristo yo i ocombe.
Mal mauya yando ilquenen mici le mele verirya ve melis inse, ar mauya i verin same áya veruryan.
Híni, na *canwacimye nostaruldant i Herusse, an ta faila ná.
“Á anta alcar atarelyan ar amillelyan” – ta ná i minya axan ya same vanda:
“návelyan herenya ar samieldan anda coivie cemende”.
Ar elde, atari, áva tyare nyére hínaldain, mal áte orta mí Heruo paimesta ar peantie.
A móli, na *canwacimye in nar heruldar i hrávesse, mí rucie yo palie, et quanda endallo, ve i Hriston
– lá ve hendunúror, ve qui fastien atani, mal ve Hristo móli, cárala Eruo indóme holmo.
Na móli i molir mérala sámanen, ve i Herun ar lá atanin,
an istalde i ilquen, qui caris *aiqua mára, nancamuva carda sina i Herullo, lá címala qui náse mól hya léra.
Ar a heruvi, cara i imye nati tien. Á pusta quete *lurwi, an istalde i elde ar té véla samir Heru menelde, ar sé ua cime cendeler.
Teldave, na taure i Herusse ar i melehtesse poldoreryo.
Á panya indesse i varyala sorosta Eruo, tarieltan i Arauco fintelennar.
An elve mahtar, lá sercenna ar hrávenna, mal i hérennar, i túrennar, i mardoturinnar mornie sinasse, i olce fairetúrennar menelde.
Etta á orta i varyala sorosta Eruo, polieldan tare i ñottonna i ulca auresse. Tá, téliéla ilqua, taruvalde.
Etta tara tancave, apa nutie i nanwie *os oşweldar ve quilta, ar arwe i ambasseo failiéva,
ar nótine *os taluldat i manwaina sómanen ya i evandilyon raineva anta.
Or ilqua á mapa i turma saviéva, yanen poluvalde *luhtya ilye i Olco uryala pilindi,
ar á mapa i carma rehtiéva ar i macil faireva, ya Eruo quetta ná,
íre illume hyamilde fairesse ilya hyamiénen ar arcandenen. Tana mehten na coive ilya voronwiénen ar arcandenen rá ilye i airin,
– yando rá inyen, camienyan quettar íre pantan antonya, lávala nin veriénen care sinwa i evandilyono fóle,
yan nanye *ráquen mi naxali – quetienyan sa veryave, ve mauya nin quete.
Lavien yando elden ista ninye nattor, manen cáran, Ticico – melda háno ar voronda núro i Herusse – caruva ilqua sinwa len.
Mentanenyes lenna sina mehten, istieldan nattolmar ar tiutieryan endalda.
Nai i hánor samuvar raine ar melme ho Eru i Atar ar i Heru Yésus Hristo!
Nai i lisse euva as illi i melir Herulva Yésus Hristo ilfirin melmenen!
Paulo ar Timoşeo, Yésus Hristo móli, ilye i airin Yésus Hristosse i ear Filippisse, ar ortirindoin ar ocombenúroin:
Nai samuvalde lisse ar raine ho Eru Atarelva ar i Heru Yésus Hristo.
Antan hantale Ainonyan quiquie enyalin le.
Illume, mi ilya arcandenya illin mici le, carin arcande mi alasse
i rantanen ya samilde mi carie i evandinyon sinwa, i minya aurello tenna lú sina.
An nanye tanca pa nat sina, i sé ye *yestane mára molie lesse yando telyuva sa, tenna Yésus Hristo aure.
Carin mai saniénen sie pa le illi, pan samin le endanyasse, elde i illi samir ranta mí lisse asinye – naxanyainen, ar véla i variénen ar tulciénen i evandilyonwa.
An Eru astarmonya ná i milyan le illi, Yésus Hristo endanen.
Ar nat sinan hyáman: i melmelda en nauva ambe ar ambe úvea, istyanen ar laicenen,
lávala len tyasta i valdie nati, náveldan ú vaxeo ar lá tyárala exi lanta, tenna i Hristo aure.
Nauvalde quátine faila yávenen, ya ea Yésus Hristonen, i alcaren ar laitalen Eruva.
Sí merin i istalde, hánor, i sómanya atyárie i evandilyon lelya ompa ambe lá pusta sa,
ar sie naxanyar anaier cárine sinwe mi Hristo i quanda artan ar ilye i exin.
Ente, i amarimbar i hánoron i Herusse, i acámier huore naxanyainen, ambe yondave veryar quete i quetta ú caureo.
É ear queneli i carir Hristo sinwa et *hrúcenello ar costiello, mal exeli yando holmo.
Té carir i Hristo sinwa et melmello, an istalte i anaien panyaina sisse varien i evandilyon.
Mal i exi carir sa pan merilte i cos, lá poice castainen, an intyalte i tyaruvalte şangie nin naxanyassen.
Tá mana? – hequa i mi ilya lé, as nurtaine sanwali hya mi nanwie, i Hristo ná carna sinwa, ar etta nanye valima. É nauvan valima ata,
an istan i sie ñetuvan rehtie, arcandeldanen ar camiénen i faire Yésus Hristo,
ve maivoinenya ar estelinya i nauvan nucumna muntasse, mal ilya veriesse i Hristo nauva laitana hroanyanen – tambe illume yá, síve yando sí, coiviesse hya qualmesse.
An inyen coivie ná Hristo, ar qualie, came amba.
Mal qui lemie coiviesse tea i polin care molie ya cole yáve, uan ista mana ciluvan.
Nanye túcina imbe i atta: Merin auta silo ar náve as Hristo, an ta nauva ita arya.
Ono ea ambe túra maure i lemyuvan i hrávesse, márieldan.
Ar nála tanca pa si, istan i lemyuvan ar termaruvan as le illi, menieldan ompa ar i samieldan i alasse ya i savie anta.
Sie ecuva len laita inde Hristo Yésusse mi úvea lé – pa ní, návenyanen aselde ata.
Eryave á lenga mi lé valda i evandilyono pa i Hristo! Sie, lá címala qui tulin velien le hya qui nanye oa, ecuva nin hlare pa sómalda, i tarilde tance mi faire er, mahtala quén ara quén i savien i evandilyono,
muntasse ruhtaine lo i tarir lenna. Ta ná tien tanwa nancariéno, mal len, rehtiéno. Yando tanwa sina tule Erullo.
An lenna náne antaina, rá Hriston, lá eryave save sesse, mal yando perpere rá sen.
An samilde i imya ohta ya cennelde i inye sáme, ar ve sí hláralde i en samin.
Etta, qui ea hortie Hristosse, qui ea tiutale melmeo, qui ea etsatie faireva, qui ear oravie ar *ofelme,
cara alassenya quanta návenen er sámo, arwe i imya melmeo, nála er mi fea, arwe i imye sanwi,
cárala munta meriénen cos hya cumna alcar, mal nalda sámanen nótala quén i exe ambe túra lá elde,
címala lá vére nattoldar erinque, mal yando i exion nattor.
Sama i imya sáma ya yando enge mi Hristo Yésus!
Sé, nála Eruo cantasse, ua nonte náve ve Eru nat ya mapumnes.
Úsie, carnes inse lusta ar nampe mólo canta ar náne carna ve atani.
Ar íre hirnes inse atano cantasse, nucumnes inse ar náne *canwacimya tenna qualme, é qualme i tarwesse.
Etta Eru yando carne se arata ar antane sen i esse or ilye essi,
tyarien cúna ilya occa, ion ear menelde ar ion ear cemende ar ion ear nu talan,
ar tyarien ilya lamba quete pantave i Yésus Hristo ná Heru, Eru i Ataro alcaren.
Sie, meldanyar, tambe illume anaielde *canwacimye, lá eryave íre engen tasse mal sí ita ambe íre nanye oa, síve mola rehtieldan mi caure ar palie,
an Eru ná ye mole lesse ve sanas mai, tyárala le mere ar mole véla.
Cara ilqua ú nurrion ar costiéron,
náveldan ú mordo ar poice, Eruo híni imíca i quarce ar rícine queni lúmeldo. Mici té caltalde ve calacolindor i mardesse,
hépala i quetta coiviéva. Etta laituvanyexe Hristo auresse, i uan onórie muntan hya omótien muntan.
Mal yando qui nanye etulyaina i *yancanna ar savieldo núromolienna, nanye valima ar samin alasse as illi mici le.
Mí imya lé, na valime ar sama alasse óni, yando elde!
An samin i estel i Heru Yésunen i rato mentuvan Timoşeo lenna; sie yando inye samuva i alasse istiéva i nati yar apir le.
An uan same exe arwa i imya óreo, quén ye holmo tiruva nattoldar.
An ilye i exi cestar vére nattoltar, lá tai Yésus Hristova.
Mal istalde manen tannes voronwerya – i ve hína as atar móles óni i evandilyonen.
Etta samin i estel i nér sina mentuvan, ve rato ve ecénien manen nattonyar tarir.
É nanye tanca i Herusse i yando inye rato tuluva.
Mal savin i ea maure i mentan lenna Epafrolíto, hánonya ye mole ar mahta asinye, mal *lenya mentaina nér ar núro maurenyan.
An milyas vele le illi, ar asámies nyére pan hlasselde i anes hlaiwa.
An é anes hlaiwa, hare qualmenna, mal Eru oráve sesse, é lá sesse erinqua, mal yando inyesse, lá mérala i samumnen nyére nyéresse.
Etta mentanyes ta ambe rato, velieldan se ar samieldan alasse, ar sie inye samuva mis nyére.
Etta áse came i Herusse, mi ilya alasse, ar lava taiti nerin náve mirwe len,
an castanen i Heruo moliéno túles hare qualmenna, panyala cuilerya raxesse carien i núromolie ya elde náner hampe antiello nin.
Teldave, hánonyar: Sama alasse mí Heru! Tece len i imye nati ua urda nin, mal len nás varnasse.
Tira inde pa i huor, tira inde pa i molir ulco, tira inde pa i cirir i hráve!
An elve i nanwa *oscirie nar, i Eruo Fairenen nar núror ar laitar inwe Yésus Hristonen, lá sávala i samilve varnasselva i hrávenen,
ómu inye é same castali savien i samin varnasse yando i hrávenen.Qui aiquen save i samis varnasse mí hráve, inye same amba:
*Oscirna i toltea auresse, nóna mir Israel, Venyamíno nosseo, Heverya Heveryalion, pa i Şanye: Farisa,
pa uryala savie: roitala i ocombe, pa i failie ya ná Şanyenen: quén ye ilvana né.
Mal i nati yar náner nin ñetie, tai onótien ve *uñetie, castanen i Hristo.
Ono é notin ilye nati ve *úñetie, i arata mirwiénen i istyo pa Yésus Hristo Herunya. Castaryanen yando alávien ilye natin náve vanwe nin, ar sanan pa tai ve *auhantar, ñetienyan Hristo
ar náven hírina sesse, lá véra failienyanen, ta ya tule i Şanyenen, mal yanen ea saviénen Hristosse, i failie ya tule Erullo i saviénen,
istienyan sé ar enortieryo melehte ar same ranta perperieryaron, nála cátina qualmeryanen,
– cenien qui ea lé yanen ece nin rahta i enortienna qualinillon.
Lá i acámienyes yando sí, hya i yando sí anaien carna ilvana – mal roitean cenien qui polin mapa ta yan nanye yando mapaina lo Hristo Yésus.
Hánor, uan save pa imne i sí amápienyes, mal er nat carin: Panyala et sámanyallo yar caitar ca ni ar rahtala yannar nar epe ni
roitean i met, cestala i *paityale – Eruo yalie tarmenna Hristo Yésunen.
Etta, elve i nar manwe, alve same natto sina sámasse; ar qui samilde hyana nat sanweldassen, Eru apantuva len yando natto sina.
Ono apa tulie sie andave, alve vanta ompa i imya tiesse.
Áni hilya tienyasse, hánor, ar cima i vantar mí lé hilyala i *epemma ya samilde messe.
An ear rimbali – quenten pa te rimbave, mal sí quétan pa té arwa nyéreo – i vantar ve ñottoli i tarweo i Hristo.
Mettalta nauva nancarie ar ainolta cumbalta ná, ar alcarelta ná véra nucumnielta, an samilte sámalta i natissen cemende.
Pa elve, nórelva ea menelde, yallo yando *lertalve *Rehton, i Heru Yésus Hristo,
ye encaruva nalda hroalva náven ve alcarinqua hroarya, túreryanen yanen yando polis panya ilqua nu inse.
Etta, hánonyar melde ar milyaine, alassenya ar ríenya – sie tara tance i Herunen, meldar!
Euoria hortan ar Sintíce hortan: Na er sámo, i Herusse!
É arcan yando lyello, sarto ye mole asinye nu i yanwe, á manya tú yet omótiet asinye i evandilyonen, as Hlement ar yando i exi i molir asinye, ion essi ear Parmasse Coiviéva.
Illume na valime i Herusse. Ata quetuvan: Na valime!
Lava moicieldan náve sinwa ilye atanin. I Heru hare ná.
Lava muntan *tarasta le, mal ilquasse, hyamiénen ar arciénen as hantale, cara arcandeldar sinwe Erun,
ar Eruo raine, ya lahta ilya sanwe, tiruva endalda ar sámalda Yésus Hristonen.
Teldave, hánor: Ilye nati yar nanwe nar, ilye yar lunge nar, ilye yar faile nar, ilye yar poice nar, ilye yar írime nar, ilye pa yar mo quete mai, ilya márie ar ilqua nála valda laitaleo – á sana pa tai!
Yar parnelde ar camnelde ar hlasselde ar cennelde inyenen, tai cara, ar i Aino raineva euva aselde.
Ente, samin túra alasse mí Heru i sí i mettasse eceutielde sanielda pa ni, yen é antanelde sanwe, mal enge munta ya pollelde care.
Lá i quétan pa penie, an apárien lave yain samin farya nin.
É istan manen perpere penie, é istan manen same úve. Mi ilqua ar ilye sómar apárien i fóle – manen náve quanta ar manen náve maita véla, manen same úve ar manen perpere penie.
Ilye natin samin poldore, yenen anta nin antoryame.
Ananta carnelde mai samiénen ranta asinye şangienyasse.
É istalde, Filippiar, i íre i evandilyon minyave túle lenna – íre oanten Maceroniallo – ua enge erya ocombe ya sáme ranta asinye pa antie ar camie, hequa elde erinque.
An yando Şessalonicasse, er lú ar lú atta véla, mentanelde ninna maurenyan.
Lá i cestean i anna, mal é cestean i yáve ya nauva nótina elden.
Mal samin ilqua ar same úve. Anaien quátina apa camnen ho Epafrolíto i nati lello, níşima olme, írima *yanca, mára Erun.
Ar Ainonya quatuva ilya maurelda laryanen alcaresse, Yésus Hristonen.
Ainolvan ar Atarelvan na i alcar tennoio ar oi! Násie.
Á *suila ilya aire mi Yésus Hristo. I hánor i ear asinye *suilar le.
Ilye i airi, mal or illi i ear i coasse i Ingaranwa, *suilar le!
Nai i Heru Yésus Hristo lisse euva as fairelda!
Paulo, Yésus Hristo apostel Eruo indómenen, ar Timoşeo i háno,
i airi ar voronde hánonnar Hristosse i ear mi Colosse: Lisse len, ar raine Eru Atarelvallo.
Antalme illume hantale Erun, Yésus Hristo Herulvo Atar, lan hyámalme pa le.
An ahlárielme pa savielda mi Hristo Yésus ar pa i melme ya samilde ilye i airiva,
i estelden ya anaie sátina len menelde. Pa ta ahlárielde nóvo, i nanwa quettanen i evandilyono
ya ea mici le. Tambe colis yáve ar ale i quenda mardesse, síve yando mici lé, i aurello ya lastanelde ar sinter Eruo lisse nanwiesse.
Ta ná ya apárielde ho Epafras, melda mól aselme, ye rá len voronda núro Hriston ná.
Yando tannes men melmelda, fairenen.
Etta yando elme, i aurello yasse hlasselme sa, uar upustie hyame ar arca len i nauvalde quátine istyanen indómeryo mi ilya sailie ar hande i faireo,
vantien valdave epe i Heru, antala sen ilvana alasse íre colilde yáve ar alir mí istya Eruo,
ilya túrenen nála cárine taure, alcareryo melehtenen. Sie ñetuvalde ilya voronwe ar cóle, as alasse,
antala hantale i Ataren ye carne le valde camien masselda i calo ya i airi haryuvar.
Etelehtanes vi et i túrello i morniéva ar talle vi olla mir i aranie melda Yondoryo,
yenen samilve *nanwerelva, i apsenie úcarelvaiva.
Náse i úcénima Aino emma, i minnóna ilya ontiéno,
an sénen ilye nati náner ontaine menelde ar cemende, i cénimar ar i úcénimar, lá címala qui nalte mahalmali hya héreli hya turiéli hya túreli. Ilqua ter sé ar sén anaie ontaina,
ar sé ea nó ilye nati, ar ilye nati sénen náner tyárine ea,
ar sé ná cas hroaryo: i ocombe. Náse i yesta, i minnóna qualinallon, náveryan mi ilye nati i minya.
An Eru sanne mai pa tyarie i quanda quantie mare sesse,
ar ter sé *erta ilye nati as inse, apa carie raine i sercenen tarweryo, i nati cemende ar i nati menelde véla.
Yando as elde, i náner mi lú ettelear ar cotya sámo olce cardaldainen,
sí acáries raine i hroaryanen hráveva, qualiénen, panien le airi ar ilvane ar han *ulquetie epe inse
– qui é termarilde i saviesse, tulcaine i talmasse ar voronde, ar lá nála tyárine hehta i estel i evandilyono ya hlasselde, ta ya anaie carna sinwa i quanda ontiesse nu menel. Evandilyon sinan inye, Paulo, náne carna núro.
Sí hirin alasse perperielmassen pa elde, ar inye quáta ya pénina ná i perperiéron i Hristo hrávenyanen, rá hroaryan, ya i ocombe ná.
San inye náne carna núro i hérenen Erullo ya náne nin antaina márieldan, carien nanwa Eruo quetta,
i fóle ya náne nurtaina ho i vanwe randar ar *nónari. Mal sí anaies apantaina airiryain,
inen Eru emérie care sinwa imíca i nóri mana i alcarinque lari fóle sino nar. Ta ná Hristo lesse, i estel alcaro.
Sé ná ye carilme sinwa, tánala ilya atanen i tie ar peantala ilya atanen ilya sailie, ettanielman ilya atan ilvana, Hristosse.
Tan yando mótan, mahtala molieryanen ya mole nisse túrenen.
An merin le ista manen tumna ná i mahtie ya samin rá elden ar i quenin Laoriceasse ar in uar ecénie cendelenya i hrávesse,
tiutien endalta, náveltan *ertaine melmesse ar camieltan i quanda lar quanta handeo, ar ñetieltan istya i fóleo Eruo: Hristo.
Nurtaine sesse ear ilye i harmar sailiéva ar istyava.
Nat sina quetin pustien aiquen tyariello le ranya sahtala quetiénen.
An ómu nanye oa i hrávesse, nanye en aselde i fairesse, arwa alasseo ar yétala manen nalde *partaine ar şande mi savielda Hristosse.
Etta, pan acámielde Hristo Yésus i Heru, á vanta sesse,
arwe şunduldar mi sé ar nála carastaine ama sesse, tulcaine i saviesse – ve náne peantaina len, antala hantale úvesse.
Na coive, pustien aiquen tuciello le oa i ingolénen ar i lusta *hurulénen ya hilya i peantier cámine atanillon – hilyala i mardo penye nati ar lá Hristo.
An sesse i quanda valaina quantie mare hroasse.
Sie nalde quátine sénen ye ná i cas ilya héreo ar túreo.
Mi sé anelde yando *oscírine *osciriénen ya ua mainen hya mapa hráve oa i hroallo – i *osciriénen i Hristo.
An anelde panyaine óse sapsasse i *tumyalénen, ar sénen anelde yando ortaine ama óse, castanen savieldo ar i moliénen Eruo, ye se-ortane qualinallon.
Ente, ómu anelde qualinar ongweldassen ar penieldasse *oscirie hráveldasse, Eru le-carne coirie óse. Apsennes ven ilye ongwelvar
ar *aupsarne i tecie rohtava ya náne ana ve, ar amápies sa oa taciénen sa i tarwenna taxelínen.
Carnes i héri ar i túri helde ar pantave ettanne tai yaiwen, apa sámes apairerya or tai i tarwesse.
Etta nai *úquen mici le namuva le pa matie ar sucie ar pa aşar hya ceuran hya *sendare,
an nati sine nar leo i túlala nation, mal i anwa nat ná i Hristova.
Áva lave aiquenen mapa oa ríelda apaireva ye same alasserya mi “naldie” ar *tyerme valaiva. Sanweryar nar tácine i natissen yar ecénies mi maur, ar laitas inse ú casto, sámaryanen hráveva.
Uas himya i cas, sé yello i quanda hroa – hépina uo ar arwa tulcuryo oxaryainen ar núteryainen – ale i aliénen ya Eru anta.
Qui quallelde as Hristo i mardo penye natillon, manen ná i elde, ve qui en marilde i mardesse, lengar i canwainen:
“Áva appa, áva tyave, áva pelta” –
pa nati yar nar martyaine nancarien návenen *yuhtaine, i canwainen ar peantiainen atanion?
Nati sine haryar ilce sailiéva mi *tyerie ya queni icílier inten, ar mi “naldie” ar lengie i hroanna mi naraca lé, mal ualte aşie pustien i hráveo íri návello quátine.
Mal qui anelde ortaine as i Hristo, á cesta i artar, yassen i Hristo hára ara Eruo forma.
Cima i artar, lá i nati cemende.
An quallelde, ar coivielda anaie nurtaina as i Hristo, Erusse.
Íre i Hristo, coivielva, nauva apantaina, tá yando elde nauvar apantaine óse alcaresse.
Etta lava quale i hroarantain yar samilde cemende, pa *hrupuhtie, úpoicie, yére, olca íre ar milcie, ya *cordontyerie ná.
Sine natinen Eruo rúşe tule i queninnar i racir canwaryar.
Mi nati sine yando elde vantaner yá, íre tai quanter coivielda.
Mal sí yando elde á panya oa ilqua: rúşe, aha, olcie, naiquetie, ar vára carpie et antoldallo.
Áva hure quén i exen. Á panya oa i yára atan as carieryar,
ar topa inde i vinya atannen, ye istyanen ná envinyanta i emmanen yeo ontane se.
Sís ua ea Hellenya hya Yúra, *oscirie hya *úoscirie, ettelea quén, Scişa, mól, léra quén, mal ilqua ar mi illi Hristo ná.
Etta, ve Eruo cílinar, airi ar melde, topa inde i felmínen *ofelmeva, máriéva, nalda sámava, moiciéva, cóleva.
Cola i exi ar apsena quén i exin, qui aiquen same nur exenna. Tambe i Heru apsenne elden, síve alde apsene, yando elde.
Mal napaniesse ilye sine natin topa inde melmenen, ya ná ilvana núte imbi le.
Ente, lava i Hristo rainen ture endaldasse, yanna anelde yáline mi hroa er, ar hantale alde anta!
Lava i Hristo quettan mare lesse alyave. Ilya sailiesse á peanta quén i exen ar á tana quen i exen i tie airelindínen, lírínen laitaléva, fairelírínen mi lisse, lindala endaldasse Erun.
Ar ilya nat ya oi carilde, quettanen hya cardanen, cara ilqua mí esse i Heru Yésuo, antala hantale Eru i Ataren ter sé.
A nissi, á panya inde nu veruldar, ve vanima ná i Herusse.
A neri, mela verildar ar áva na sárave rúşie téna.
A híni, cima nostaruldato canwar mi ilqua, an ta mára ná i Herun.
A atari, áva tyare hínaldain nyére, hya mapuvalde oa huorelta.
A móli, cima mi ilqua i nirme ion nar heruldar i hrávenen, lá ve hendunúror, ve qui fastien atani, mal holmo, rúcala i Herullo.
*Aiqua ya carilde, mola quanda fealdanen, ve i Herun ar lá atanin.
An istalde i camuvalde i Herullo *paityalelda, i ranta ya haryuvalde ve aryoni. Na móli i Heru Yésun.
An ye care úfailie nancamuva úfailierya, ar ua ea cimie cendeléva.
A heruvi, á anta mólildain ya faila ar vanima ná, istala i yando elde samir Heru menelde.
Na voronde hyamiesse, coive sasse hantalénen,
mí imya lú hyámala yando elmen, arcala i Eru pantuva men fenna i Quetto, quetien i fóle pa Hristo, pa ya nanye yando naxalissen,
apantienyan sa, ve mauya nin quete.
Á lenga sailiénen in ear i ettesse, mápala i ecie.
Lava quettalda illume same lisse, tyáverya antoryaina singenen, istieldan manen hanquetuvalde ilquenna.
Ilye nattonyar nauvar cárine sinwe len lo Tíhico, melda hánonya ar voronda núro ar mól asinye i Herusse.
Ementienyes lenna sina imya casta: istieldan yar apir mé, ar tiutieryan endalda.
Óse mentan Onesimo, voronda ar melda hánonya, ye mici lello ná. Ilye i nattor sisse caruvatte sinwe len.
Aristarco, ye ná mandosse óni, *suila le. Sie carir Marco, Varnavasso rendo – pa sé acámielde canwali: qui tulis lenna, áse came mai –
ar Yésus estaina Yusto, i nar i *oscirnaron.Té erinque molir asinye Eruo aranien, ar anaielte nin tiutale.
Epafras *suila le, sé ye mici lello ná, mól Hristo Yésuo. Illume mahtas rá elden hyamieryassen, arcala i taruvalde ilvane ar tulce mí quanda indóme Eruo.
É *vettan sen i samis túra mótie elden ar in ear mi Laoricea ar Hierapolis.
Lúcas, i melda *nestando, *suila le, ve care Lémas.
Á *suila i hánor Laoriceasse ar Nimfa ar i ocombe coaryasse.
Ar íre tecetta sina anaie hentaina mici le, tyara i nauvas hentaina yando i ocomben Laoriceasse, ar ñeta i tecetta Laoriceallo, lávala yando elden henta sa.
Ente, queta Arcipponna: “Hepa i núromolie ya camnel i Herusse, telien sa!”
Sisse técan *suilienya véra mányanen. Enyala naxanyar! Nai i Erulisse euva aselde.
Paulo ar Silváno ar Timoşeo i ocombenna Şessalonicasse, mi Eru i Atar ar i Heru Yésus Hristo: Nai samuvalde lisse yo raine!
Antalme illume hantale Erun íre enyalilme le hyamielmassen, oi
enyalila voronda molielda ar melila mótielda ar voronwielda mí estel ya samilde mi Herulva, Yésus Hristo, epe Ainolva ar Atarelva.
An istalme, hánor méline lo Eru, manen nalde cíline,
an mici elde i evandilyon ya tallelme úne rie quettainen, mal túles túrenen ar Aire Feanen ar carne le aqua tulce pa sa, ve istalde manen lenganelme mici le márieldan.
Ar hilyanelde tielmasse ar i Heruo tiesse, camila i quetta mi naraca şangie, arwe alasseo Aire Feanen.
Sie anelde cárine *epemma illin i savir Maceroniasse ar Acaiasse.
An lello i Heruo quetta anaie carna sinwa, lá rie Maceroniasse ar Acaiasse, mal ilya nómenna savielda Erusse eteménie, ar sie ualme same maure quetiéva erya nat.
An té inte nyárar manen minyave túlelme lenna, ar manen quernelde inde Erunna ho i cordoni, náven núror coirea ar nanwa Ainon,
ar yétien ompa i tulienna menello Yondoryo, ye ortanes qualinallon: Yésus, ye vi-etelehta i tuluvaila rúşello.
An elde istar, hánor, manen tulielma lenna úne muntan.
Mal apa perperie orme Filippisse, ve istalde, camnelme verie Erullo quetien lenna Eruo evandilyon, ómu urda mahtiénen.
An i hortale ya antalme ua tule loimallo hya úpoiciello hya hurunen,
mal tambe anaielme tyastane lo Eru ar hírine valde camien i evandilyon mir hepielma, síve quetilme, lá fastien atani, mal Eru ye tyasta endalma.
An ve istalde, ualme oi equétie passe quettainen, hya nurtana milca írenen: Eru astarindo ná!
Ente, ualme ecestie alcar atanillon, lá eldello hya exellon,
ómu éciévane men náve lunga cólo ve Hristo aposteli.Úsie, anelme moice imíca le, ve íre *tyetila amil lenga mélave vére hínaryain.
Etta, pan nalde melde men, sannelme mai pa antave len ranta, lá rie mi Eruo evandilyon, mal yú mi véra fealma, pan olólielde melde men.
An enyalilde, hánor, molielma ar mótielma. Molila lómisse yo auresse, lá náven cólo aiquenna mici le, carnelme sinwa len Eruo evandilyon.
Nalde astarmor, ve Eru ná, manen airi ar faile ar ilvane anelme mici elde i savir.
Sie istalde mai manen hortanelme ilquen mici le, ve atar hortala hínaryar,
tiutala le ar *vettala len, vantieldan valdave Erun ye le-yale mir véra aranierya ar alcarerya.
Ar tanen yú elme antar Erun hantale voro, an íre ñentelde Eruo quetta ya hlasselde mello, camneldes lá ve atanion quetta, mal ve ta ya nanwave nas: ve Eruo quetta, ya yú mole lesse i savir.
An ihílielde, hánor, i tiesse i ocombion Eruo yar ear Yúreasse mi Hristo Yésus, an yú elde eperpérier quenillon véra nóreldo i imye nati yar yú té perperir i Yúrallon,
i nacanter i Heru Yésus ar i Erutercánor véla, ar roitaner mé. Ente, loitalte fasta Erun, mal tarilte i márienna ilye atanion,
pan pustalte me carpiello queninnar i nórion rehtieltan, ar sie illume quatilte úcareltaron lesta. Mal i mettasse i rúşe utúlie téna.
Ono elme, hánor, íre anelde oa mello şinta lúmesse – mi quén, lá mi enda – névelme en ambe túra veassenen cene cendelelda, túra írenen.
Sina castanen mernelme tule lenna – é inye, Paulo – mi lú er ar atta véla, mal Sátan pustane me.
An mana estelelma hya alasselma hya i ríe yanen laitalmexer? Ma ta ua elde, epe Herulva, Yésus, mi entulesserya?
An elde nar alcarelma ar alasselma!
Etta, íre ua ence men cole sa ambe, carnelme i cilme náveva hehtana mi Aşen, erinque.
Ar mentanelme Timoşeo, ye hánolma ná ar ye aselme Eruo núro ná mi Hristo evandilyon, carieryan le tulce ar hortaven le pa savielda,
pustien aiquen návello túcina oa lo şangier sine. An elde istar i nat sina ná ven martyana.
É yú íre anelme aselde nyarnelme len nóvo i perperumnelve şangie, ve yú amartie ar istalde.
É sina castanen, íre inyen ua ence cole sa ambe, mentanen istien voronwielda. Caurenya náne i cé i *Şahtando le-aşahtie mi sina hya sana lé, ar mótielma náne muntan.
Mal sí Timoşeo utúlie menna lello, ar ánies men i máre sinyar pa voronwelda ar melmelda, ar pa manen samilde mára enyalie elmeva, illume milyala cene me, ve elme milyar lé.
Etta, hánor, anaielme tiutana pa le voronweldanen, mi ilya maurelma ar şangielma.
An sí nalme coirie, qui tarilde tulce i Herusse.
An mana hantale ece men anta Erun pa elde, pa i quanda alasse yanen nalme valime rá elden epe Ainolva,
íre lómisse yo auresse, i antúra veassenen, cáralme arcandeli cenien cendelelda ar quatien yar savielda pene?
Sí nai Ainolya ar Atarelva inse antauva men téra tie lenna!
Ente, nai i Heru tyaruva le ale ar same úvea melme quén i exeva ar illiva, ve yú elme samir melme eldeva,
carien endalda tulca, pen motto airesse epe Ainolva ar Atarelva mí entulesse Herulvo, Yésus, as ilye airiryar. Násie!
Teldave, hánor, arcalme lello ar hortar le i Heru Yésunen: Tambe camnelde mello i istya pa manen mauya len lenga ar fasta Eru, síve é lengalde, mal cara sa ambe aqua.
An istalde i hortaler yar antanelme len ter i Heru Yésus.
An sie Eruo indóme ná: I nauvalde airinte – i hepuvalde inde oa *hrupuhtiello.
Sie mauya ilquenen mici le harya véra venerya, mi airitáve ar mi alcar,
lá mi milca maile, ve samir yú i nóri yar uar ista Eru.
Mauya in *úquen lahta i tea ar care úfailie hánoryan mi natto sina, an i Heru ahtaruva ilye taiti nati, ve anyárielme len nóvo ar *evettielme len.
An Eru ua vi-yalde úpoicienna, mal airitávesse.
Etta, ye ua cime ua loita cime atan, mal Eru, ye yú panya Aire Fearya lesse.
Mal pa hánomelme ualde same maure i tecuvalme lenna, an elde nar peantane lo Eru i meluvalde quén i exe,
ar é carilde sie, melila i hánor quanda Maceroniasse. Mal hortalme le, hánor, i caruvalde sie ambe aqua,
ar i caruvalde ennelda i samuvalde quilda coivie, cimila vére nattoldar ar molila vére máldanten, ve cannelme len.
Sie polilde vanta mi vanima lé, ve cénaina lo i queni mí ette, ar mi maure muntava.
Mal hánor, ualme mere i nauvalde laistie pa i lornar. Sie ualde samuva i imya naire ya i exi samir, té i uar same estel.
An qui savilve i Yésus qualle ar enoronte, Eru yú taluva óse mí imya lé i aquálier ter Yésus.
An natto sina quetilve lenna Eruo quettanen: Elve, i coirear i lemyar tenna i Heruo entulesse, laume tuluvar epe i aquálier,
an i Heru inse tuluva undu menello as yello, as i óma héra valo ar arwa i hyólo Eruo, ar i qualinar Hristosse ortuvar minyave.
Epeta elve, i coirear i lemyar, nauvar rapane oa aselte mir i fanyar velien i Heru i vilyasse, ar sie nauvalve illume as i Heru.
Etta á tiuta quén i exe sine quettainen.
Mal pa i lúmi ar i lúr, hánor, ualde same maure in aiquen tece lenna.
An elde istar mai in i Héruo ré tuluva ve arpo i lómisse.
Íre quetilte: “Raine yo varnasse!”, tá nancarie rincenen tuluva téna, aqua ve i aice mapa lapselunga nís nó colis hínarya, ar laume uşuvalte.
Mal elde, hánor, uar i morniesse, ar sie i ré ua lantuva lenna ve arpo,
an illi mici le nar yondor calo ar yondor aureo. Ualve i lómio hya morniéno.
Etta, ávalve na lorne ve i exi, mal alve na coivie ar hepe laicelva.
An i lornar nar lorne lómisse, ar i quatir inte limpenen sucir limpe lómisse.
Mal elve i nar i aureo, alve hepe laicelva, colila i ambasse saviéva ar melmeva, ar ve cassa, i estel rehtiéva.
An Eru ua me-sante rúşen, mal camielvan rehtie ter Herulva, Yésus Hristo
ye qualle ven. Sie, coirear hya qualinar, coituvalve óse.
Etta á tiuta quén i exe sine quettainen, ar á carasta ama quén i exe, ve é cáralde.
Mal arcalme lello, hánor, cima i mótar mici le ar tulyar le ar tanar len i tie.
Sama túra áya tien, melmesse, molieltanen. Hepa raine mici inde.
Mal hortalme le, hánor: Á tana i téra tie i ráneain, queta tiutalénen innar penir huore, á manya i nar milye, sama cóle illin.
Cena i *uquen paitya ulcun ulcunen, mal illume á roita ya mára ná, quén i exen, ar illin.
Illume sama alasse.
Hyama oiale.
Pa ilqua á anta hantale, an ta ná Eruo indóme Yésus Hristosse elden.
Áva *luhtya i Faire.
Áva nattire quetie ve Erutercáno.
Á tyasta ilye nati; hepa yar máre nar.
Hepa inde oa ilya nostalello ulcuo.
Nai i Aino raineva immo airitauva le aqua! Nai fairelda ar fealda ar hroalda nauvar hépaine mi ilvana lé mí entulesse Herulvo, Yésus Hristo.
Isse ye le-yale voronda ná – ye yú caruvas sa.
Hánor, hyama yú elmen!
Á *suila ilye i hánor aire miquenen!
Canin len vandanen, mí Heru: Á et-henta tecetta sina ilye i hánoin.
Nai Yésus Hristo Herulvo lisse euva aselde.
Paulo ar Silváno ar Timoşeo i ocombenna Şessalonicasse, mi Eru Atarelva ar i Heru Yésus Hristo.
Nai samuvalde lisse ar raine ho Eru i Atar ar i Heru Yésus Hristo!
Nalme naute illume anta Erun hantale pa lé, hánor, ve vanima ná; an savielda ála ita lintave, ar mici lé ilqueno melme i exiva oi ambe túra ná.
Etta elme laitar imme pa lé mici Eruo ocombi, castanen voronwieldo ar savieldo mi ilye roitieldar ar i şangier yar cólalde,
ettánala Eruo faila namie, náveldan nótine valde Eruo araniéno, yan é perpéralde.
Sie ná faila Eruo hendusse *nampaitya şangie in tyarir şangie len,
mal elden i perperir şangie, sére aselme íre i Heru Yésus ná apantaina menello as túrie valiryar
mi uryala náre, íre talas nancarie innar uar ista Eru ar uar hilya Yésus Herulvo evandilyon.
Té perperuvar paime oira nancariéva oa i Herullo ar alcarinqua poldoreryallo,
i auresse ya tuluvas camien alcar mici airiryar ar náven tíraina elmendanen lo illi i sáver – an i *vettie ya antanelme náne sávina lo elde.
Tana mehten é hyámalme len illume, arcala i Ainolva notuva le valde yalieryo ar telyuva ilqua ya meris – márie ar cardar saviéno – túresse,
talien alcar Yésus Herulvo essen mici le.
Mal, hánor, pa Yésus Hristo Herulvo entulesse ar návelva comyaine senna, arcalme lello:
Áva na lintiénen páline handeldallo hya valtaine, faireo apantiénen hya quettanen hya tecettanen quétina tule mello, quétala in i Heruo aure utúlie.
Lava *úquenen sie şahta le, pan uas tuluva nó i *oatarie utúlie minyave, ar i nér *şanyelóriéva nauva apantaina, yondo nancariéno.
Taris venna ar ortaxe or ilquen estaina aino hya yenna mo hyame; sie hamúvas Eruo cordasse, tánala inse ve aino.
Ma ualde enyale i lan engen aselde, nyarnen lenna nati sine?
Ar sí istalde ya hépa se hampa, náveryan apantaina véra lúmeryasse.
I fóle *şányelóriéva móla yando sí, mal eryave tenna ye sí tára sanna nauva mapaina oa.
Ar tá i *şányelóra quén nauva apantaina, ye i Heru Yésus nancaruva antoryo şúlenen, ar ye taluvas muntanna íre tulierya anaie carna sinwa –
sé yeo tulie ná tyárina Sátano moliénen, ilya taura cardanen ar tanwanen ar húrala tengwenen
ar ilya úfaila úşahtiénen i nancaruvainaiva, pan ualte camne i melme nanwiéva, náveltan rehtaine.
Etta Eru antane te olla túrea ranyan, savieltan i huru.
Sie illi mici te nauvar námine, pan ualte sáve i nanwie, mal sámer alasse úfailiesse.
Mal elmen mauya anta Erun hantale pa lé, hánor mélaine lo i Heru, an Eru le-cilde i yestallo náveldan rehtaine, airitiénen faireva ar saviénen i nanwiesse
yanna yando le-yaldes i evandilyonnen ya talalme, samieldan i Heru Yésus Hristo alcar.
Etta, hánor, tara tance ar á himya i cámine peantier pa yar anelde peantaine – qui quettalmanen hya qui tecettalmanen.
Ente, nai Yésus Hristo immo ar Eru Atarelva, ye vi-méle ar antane ven oira tiutie ar mára estel Erulissenen,
tiuituva endalda ar caruva le tulce mi ilya mára carda ar quetta.
Teldave, hánor, hyama rámen; sie i Heruo quetta levuva lintiénen ar camuva alcar, ve cáras ara lé.
Yando hyama i nauvalme etelehtaine ulce ar olce atanillon, an illi uar same savie.
Mal i Heru voronwa ná, ar le-caruvas tance ar le-varyuva i Ulcallo.
Ente, nalme tance i Herusse pa le, i cáralde ar en caruvar yar canilme.
Nai i Heru tulyulva endalda mir Eruo melme ar i Hristo voronwie.
Sí cánalme len, hánor, Yésus Hristo Herulvo essenen: Hepa inde oa ilya hánollo ye vanta mi úvanima lé ar lá i situnen ya camnelde mello.
An elde istar manen mauya len hilya tielmasse, pan ualme lengane imíca te mi úvanima lé,
hya manter aiqueno matta lá paityala. Úsie, moliénen ar mótiénen, lómisse yo auresse, mólelme lá panien cólo aiquenna mici lé.
Lá peniénen túre, mal carien imme *epemma len, carnelme sie – carieldan ve elme carner.
An íre anelme aselde, antanelme len canwa sina: "Qui aiquen ua mere mole, yando uas matuva."
An hláralme i queneli mici le vantar mi úvanima lé, loitala mole, mal mittala inte mir i nattor exelion.
Taiti quenin antalme sina canwa ar hortale i Heru Yésus Hristonen: Á matir véra mastalta moliénen quildesse.
Mal elde, hánor, áva ole lumbe, pustala care ya vanima ná!
Mal qui aiquen ua lasta quettalmanna ter tecetta sina, *rena man náse. Á pusta náve óse, náveryan nucumna.
Ono áva note se ve ñotto, mal ora sen ve háno.
Nai i Heru raineva immo antuva len raine illume, mi ilya lé. Nai i Heru euva aselde!
Si ná *suilienya véra mányanen, ya ná tanwa mi ilya tecetta: Sie tecin.
Nai Herulva Yésus Hristo lisse euva as illi mici le!
Paulo – Yesus Hristo apostel, ve cánina lo Eru *Rehtolva ar Hristo Yésus estelelva,
Timoşeonna, nanwa hína i saviesse: Nai euvar lisse, oravie, raine ho Eru i Atar ar Hristo Yésus Herulva.
Ve hortanenye lye lemya Efesusse íre lelyumnen Maceronianna, sie carin sí, canielyan quenelin i ávalte peanta ettelea peantie
ar i ávalte cime nyarier ar ontaleparmar ú metto, yar tulyar minaşúriennar nó tulyalte i etsatienna *aiquava lo Eru i saviesse.
Canie sino met ná melme et poica endallo ar et mára *immotuntiello ar saviello ú *imnetyaleo.
Mal quérala inte sine natillon quelli anaier quérine cumna quetienna,
mérala náve *peantari şanyeo, mal loitalte hanya yúyo i nati pa yar quétalte ar tai pa yar carpalte verye quettalínen.
Istalve in i Şanye mára ná, qui mo *yuhta sa mi şanya lé,
arwa istya sino: Uas tare faila quenenna, mal queninnar *şanyelóre ar amortala, *ainolóre ar úcarindor, ú melmeo ar úairi, i nahtar atari ar amilli, atannahtari,
*hrupuhtari, neri i caitar as neri, *aumapandor, quenir i hurir ar racir vandaltar, ar ilya hyana nat ya tare i *málea peantienna
ve i alcarinqua evandilyon i valima Aino, ya náne panyaina hepienyasse.
Hantan Hristo Yésun, ye antane nin antoryame, pan nontesen voronwa ar ni-sante núromolien,
ómu yá anen quén ye naiquente ar roitane ar náne naraca. Ananta camnen oravie, pan laistasse carnen sie, pénala savie.
Mal Herulvo lisse náne ita úvea, as savie ar melme mi Hristo Yésus.
Quetie sina vórima ná, ar valda náveo aqua cámina: Hristo Yésus túle mir i mar rehtien úcarindor. Mici té inye i amminda ná.
Mal i casta yanen camnen oravie náne i inyenen ence Hristo Yésun tana quanda cólerya minyave, cárala ní *epemma ion savuvar sesse, sie ñetala oira coivie.
Aranen oialeo, *alahastima, *alacénima, i erya Ainon, na laitie ar alcar tennoio ar oi! Násie.
Ortírie sina panyan lyesse, Timoşeo hínanya, i apaceninen yar tulyaner lyenna; sie polil tainen mahta i mára mahtiesse,
arwa saviéno ar mára *immotuntieno, ya queneli ahátier oa, ar savielto cirya anaie nancarna.
Mici té nar Himenaio ar Alexander, ar ánien tu mir Sátano túre, pariettan paimestanen i ávatte naiquete.
Etta, minyave, hortan i carilde arcandi, hyamier, *mánacestier, hantaler, pa ilye atani,
pa arani ar illi i ear mi minde meni, samielvan coivie ya ná quilda ar rainesse, mi ilya *ainocimie ar *sanwelungie.
Ta ná mára ar *maicámima Eru *Rehtolvan,
ye mere i ilye atani nauvar rehtaine ar tuluvar istyanna i nanwiéno.
An ea Eru er, ar er enelmo imbi Eru ar atani, Hristo Yésus i atan,
ye antanexe ve *nanwere illin: Pa ta mauya *vetta mi lúmi yar samilve.
Tana mehten anen sátina *nyardar ar apostel – nyarin i nanwie, uan húra – *peantar nórion, mi savie ar nanwie.
Etta merin i mi ilya nóme atani hyamuvar, ortala sarte mát, quérala inte rúşello ar costiellon.
Mi imya lé merin i netyuvar i nissi inte *maipartaine larmainen, mi nucumna lé ar arwe máleo sámo, lá *partala findilelta, hya maltanen hya marillainen hya mirwa larmanen,
mal mi lé valda nission i quetir pa inte i nalte *ainocimye – ta ná, máre cardainen.
Nís paruva quildesse, aqua panyala inse nu hére.
Uan lave i nís peanta, hya same hére or nér; nís nauva quilda.
An Atan náne ontaina minyave, tá Éve.
Ente, Atan úne úşahtaina, mal i nís náne úşahtaina ar lantane mir şanyeracie.
Mal nauvas rehtaina coliénen híni, qui lemyalte mi savie ar melme ar airitie, as mále sámo.
Vórima i quetta: Qui aiquen mere ortírie, meris mára molie.
Etta mauya i nauva ortirindo napsa pella, erya verio veru, nalda haimassen, arwa máleo sámo, *partaina, nilda etteleain, istala peanta,
lá antaina limpen, lá palpala, mal moica, lá *tyelpendil,
nér tulyala véra nosserya, arwa *canwacimye hínion ilya *sanwelungiesse
(qui quén ua ista tulya véra nosserya, manen ortiruvas Eruo ocombe?),
lá quén *vinquerna, hya cé nauvas *valatea ar lantuva mir i imya námie ve i Arauco.
Ente, mauya i samis mára *vettie quenillon i ettesse, pustien se lantiello mir *tulcarpie ar i Arauco remma.
Mi imya lé, mauya i nauva *ocombenduri *sanwelunge, lá quetila lamba attanen, lá antala inte olya limpen, lá milcave cestala úfaila ñetie,
arwe i fóleo i saviéno poica *immotuntiénen.
Nai té nauvar tyastaine minyave; epeta lava tien náve núror, nála napsa pella.
Nai nissi mi imya lé nauvar *sanwelunge, lá quétala ulco pa exeli, nalda haimassen, voronwe ilye natissen.
Nai *ocombenduri nauvar veruvi erya nisso, tulyala vére nosseltar mai.
An neri i molir mai ve *ocombenduri ñetir mára tarie ar túra lérie quetiéva pa Hristo Yésus.
Tecin lyen nati sine, ómu samin estel i tuluvan lyenna rato.
Mal qui nauvan hampa, istuval manen mauya lyen lenga Eruo coasse, ya ná i coirea Eruo ocombe, i nanwiéno tarma ar tulco.
É i fóle *ainocimiéva ná hoa: Anes apantaina i hrávesse, quétina faila i fairesse, tannexe valalin, náne *nyardaina i nóressen, sávina i mardesse, cámina ama alcaresse.
Mal i Faire quete tére quettalínen i mí métime lúmi queneli lantuvar oa i saviello, címala faireli i tyarir exi ranya, ar i peantier raucoron,
i *imnatyalénen quenion i quetir huruvi, arwe tehto véra *immotuntieltasse ve qui carna lauca anganen.
Váquetilte i mo verya ar canir i mo quere oa mastimar yar Eru ontane náven cámine hantalénen lo i samir savie ar istya i nanwiéno.
An ilya onna Eruo mára ná, ar ua ea maure quere oa *aiqua qui nas cámina hantalénen,
an nás airinta Eruo quettanen ar hyamiénen.
Antala canwar sine i hánoin nauval Hristo Yésuo mára núro, ye ale i quettainen i saviéno ar i mára peantiéno ya ihíliel.
Mal quera oa i úairi nyarier yáre nission. Ono á cesta náve finwa *ainocimiesse!
An carie i hroa finwa ná aşea nótime natalin, mal *ainocimie ná aşea ilye natin, pan samis vanda coivie sinan ar i túlala coivien véla.
Vórima ar valda náveo aqua cámina tana quetie ná!
An met sinan molilve ar mótalve, pan apánielve estelelva coirea Ainosse, ye ná *Rehto ilye atanion, i vorondaron or illi.
Cana tien sie, ar á peanta!
Áva lave aiquenen nattire néşelya. Úsie, na *epemma i vorondain quetiesse, lengiesse, melmesse, saviesse, poiciesse.
Tenna tuluvan, cima i hentie, i tiutie, i peantie.
Áva loita *yuhta i lisse ya ea lyesse, antaina lyen apacennen ar i amyáraron paniénen máltat lyesse.
Á sana pa nati sine, quata sámalya tainen; sie nauva aşcene illin manen lelyal ompa.
Hepa sámalya imlesse ar peantielyasse! Á lemya sine natissen, an sie cárala rehtal imle ar i lastar lyenna véla.
Amyáranna áva quete narace quettainen.
Úsie, cara arcande sen ve ataren, nesse nerin ve hánoin, yáre nissin ve amillin, nesse nissin ve néşain, ilya poiciesse.
Á anta alcar *verulórain i nanwave nar *verulórar.
Mal qui ea *verulóra as ye ear hínali hya hínali hínaron, lava tien minyave pare lenga *ainocimiénen véra coaltasse ar anta vanima *nampaityale nostarultant, an ta ná mára Eruo hendusse.
Ono ye é *verulóra ná ar olólie úna, apánie estelya Erusse ar lemya arcandessen ar hyamiessen lómisse yo auresse.
Mal ye cesta maileryar ná qualina, ómu coitas.
Etta á anta canwar sine, pustien aiquen quetiello ulco pa te.
Mal qui aiquen ua anta véraryain – ar or illi tien i nar coaryasse – maureltar, alálies i savie ar ná faica lá quén ye ua save.
Á panya *verulóra nís i témasse qui samis loar lá nu *enequean, veri erya veruo,
nís pa ye mo nyare pa máre cardaryar, qui ortanes híni, qui camnes ettelear, qui sóves i airion talu, qui manyanes roitainar, qui hilyanes ilya mára carda.
Mal quera oa ambe nesse *verúlórar, an íre yérelta tuce te oa Hristollo, merilte verya
ar lantar nu námie pan hehtanelte minya voronwielta.
Mí imya lú yando parilte náve ú moliéno, vantala coallo coanna, lá eryave loitala mole, mal yando nyátala ar mittala inte mir exelion nattor, carpala pa nati pa yar ua ea maure carpa.
Etta merin i sine ambe nesse *verulórar veryuvar, ar i colilte híni ar mahtar mar; sie ye tare venna ua samuva casta quetien yaiwe.
Yando sí queneli anaier quérine hilien Sátan.
Qui ea *verulórali as sávala nís, mauya sen manya te, lá panyala cólo i ocombenna. Tá i ocombe pole manya i é nar *verulórar.
Lava i amyárain i tarir epe i ocombe mi mára lé náve nótaine valde atwa laitiéno, or illi i mótar carpiesse ar peantiesse.
An i tehtele quete: Áva pusta mundo *etevattala ori, matiello," yando: "Ye mole ná valda *paityaleryo."
Áva note *ulquetie amyáranna ve nanwa, qui uat ea astarindor atta hya nelde.
Á naitya úcarindor íre illi tírar, tyarien yando i exi ruce.
*Vettan epe Eru ar Hristo Yésus ar i cíline vali: Á himya nati sine ú namiéno nóvo, cárala munta címala cendeler.
Áva panya mályat aiquende acca rongo; ente, áva same ranta i úcarissen exion; hepa imle poica.
Áva ambe suce nén, mal á *yuhta pitya lesta limpeva, cumbalyan ar i hlívin yar rimbave perperil.
Atallion úcari nar aşceni, ar menir epe te námienna. Exelion úcari yando hilyar.
Mí imya lé yando i máre cardar nar aşceni, ar yar uar sie, nar en úfantime.
Pa illi i nar móli nu yanta: Á notir herultar valde ilya laitiéno, pustien aiquen quetiello yaiwe pa Eruo esse ar i peantie.
Ente, nai i samir sávala heruvi uar nattiruva te pan nalte hánor. Úsie, mauya tien náve arye núror, pan i nar manyaine mára molieltanen nar queni i savir ar nar melde.Á peanta nati sine ar á horta sie!
Qui aiquen peanta hyana peantie ar ua came i *málie quettar Yésus Hristo Herulvo hya i *ainocimya peantie,
samis palúna sáma, hanyala munta, mal nála hlaiwa pa maquetier ar costier pa quettar. Sine natillon tulir *hruceni, costi, yaiwi, olce napsar,
vórie mahtier mici neri hastaine sámaltassen, illon i nanwie anaie &pílina, pan sanalte pa *ainocimie ve tie ñetienna.
*Ainocimie é ná tie hoa ñetienna, as fáre.
An atálielve munta mir i mar; mi imya lé polilve cole munta et sallo.
Etta, qui samilve matta ar larmar, lava tain farya ven.
Mal i merir náve láre lantar mir úşahtie ar remma ar rimbe úhande ar harnala íreli, yar tucir atani undu mir nancarie ar atalantie.
An i melme telpeva ná şundo ilye ulce nation, ar cestala melme sina queneli anaier tyárine ranya i saviello ar *etérier inte rimbe naicelínen.
Mal elye, Eruo atan, uşa ho nati sine! Mal á roita failie, *ainocimie, savie, melme, voronwie, moicie!
Á mahta i mára mahtie i saviéno, á mapa i oira coivie yanna anel yálina ar pa ya carnelye mára *etequenta epe rimbe astarmoli.
Epe Eru, ye anta coivie illin, ar epe Yésus Hristo, ye *vettane Pontio Piláton i mára *vettie, canin len:
Alde himya i axan mi ilvana lé pa ya *úquen pole quete ulco, tenna i apantie Yésus Hristo Herulvava.
Apantie sina i valima ar erya Meletya tanuva véra lúmeryasse, i Aran ion turir ve arani ar i Heru ion turir ve heruvi,
i erya Quén arwa ilfirin coiviéno, ye mare calasse yanna *úquen pole tule, ye *úquen mici atani ecénie hya pole cene. Sen na i laitie ar túre tennoio! Násie.
Cana in nar láre randa sinasse: Áva na turquime ar áva panya estel mi lar, ya ua tanca, mal mi Eru, ye anta ven lára lesta ilye nativa alasselvan.
Á molir márien, náveltan láre máre cardassen, valime antiesse, manwe satien ya samilte,
comyala inten mára talma i lúmen ya tuluva, mapien i anwa coivie.
A Timoşeo, tira ya anaie antaina mir mandolya, ar quera imle oa i úairi ar cumne nyatiellon, ar i cotye quetiellon i "istyo" estaina sie hurunen.
An quétala i samilte taite "istya", queneli equérier inte oa i saviello.Nai i Erulisse euva aselde!
Paulo – Yesus Hristo apostel Eruo indómenen, i vandanen pa i coivie ya ea Hristo Yésusse –
Timoşeonna, melda hína: Lisse, oravie ar raine ho Eru i Atar ar Hristo Yésus Herulva.
Antan hantale Erun, yen nanye núro ve náner atarinyar, arwa poica *immotuntiéno, pan uan oi hauta le-enyale arcandenyassen, lómisse yo auresse,
milyala cene lye, íre enyalin nírelyar, návenyan quátina alassenen.
An enyalin i savie ya ea lyesse ú *imnetyaleo, ar ya marne minyave Lois &harunilyasse ar Eunice amillelyasse, mal nanye tanca i eas yando lyesse.
Sina castanen *rentan lye entinta ve náre Eruo anna ya ea lyesse, pan panyanen mányat to lye.
An Eru ua ánie ven caurea faire, mal faire túreva ar melmeva ar máleva sámo.
Etta áva na naityana pa i *vettie Herulvo, hya pa ni ve nútinarya, mal sama masselya perperiéva i evandilyonen, Eruo túrenen.
Sé vi-rehtane ar vi-yalde aire yaliénen, lá castanen cardanyaron, mal castanen véra &mehtyo ar lisseryo, antaina ven Yésus Hristonen nó oialie lúmeli,
mal sí anaies carna sinwa íre *Rehtolva, Yésus Hristo, náne apantaina. Sé emétie qualme ar acaltie cala coivienna ar alahastienna i evandilyonnen,
pa ya inye anaie sátina ve tercáno ar apostel ar *peantar.
Sina castanen perperin yando nati sine, mal uan naityana. An istan i quén yesse asávien, ar nanye tanca i polis tire ya ná panyaina mandonyasse tenna enta Aure.
Na tulyaina lo i quettar máleva ya ahláriel nillo, i saviénen ar melmenen Hristo Yésusse.
I mána panyaina mandolyasse tira i aire fairenen ya mare vesse.
Ve istal, ilye i queni Asiallo ni-ehehtier. Mici té nar Fihello ar Hermohenes.
Nai i Heru oravuva Onesifóro nossesse, an rimbave talles ninna ceutie, ar únes naityana pa naxenyar.
Úsie, íre túles Rómanna, ni-cestanes ú séreo ar hirne ni.
Nai i Heru lavuva sen hire oravie as i Héru enta auresse. Ar ilye i núrocardar yar carnes Efesosse istal mai.
Etta alye na turyaina, hinya, i lissenen ya ea Hristo Yésunen.
Ar i nati yar hlassel nillo ter rimbe *vettoli, á anta tai voronde atanin, in yando ecuva peanta exin.
Ve mára ohtar Hristo Yésuo perpera ulco as i exi.
*Úquen ye ná ohtar lave inse náve rembina i mancalenen coiviéva. Sie ece sen fasta ye carne se ohtar.
Ente, qui aiquen nore noriesse, uas came ríe qui uas nore ve i şanyer.
Ve mauya, i mólala *cemendur ná i minya ye came ranta i yávion.
Quata sámalya quetienyainen, an i Heru antuva lyen tercen pa ilye nati.
Enyala Yésus Hristo, ortaina qualinallon, Laviro erdeo, ve carna sinwa mi evandilinya,
i evandilyon yan perpéran ar cólan naxeli ve *ulcarindo. Mal Eruo quetta ua naxessen.
Etta nanye voronda ilquasse márien i cílinaron, mérala i yando té camuvar i rehtie ya ea Hristo Yésusse, as oira alcar.
Voronda i quetta: Qui quallelve óse, yando coituvalve óse;
qui nalve voronde, yando turuvalve óse: qui lalalves, sé yando laluva vé,
qui ualve voronde, sé en voronda ná, an uas pole lala inse.
Áte tyare enyale ta, ar cana tien epe i Heru: Áva costa pa quettar, an ta ua aşea, ar nancare i lastar.
Cara ilqua panien imle epe Eru ve tyastaina quén ye mole ú náveo naityana, mahtala i macil nanwiéva mi finwa lé.
Mal quera imle oa cumne ar úairi quetiellon, an tulyuvalte mir oi ambe tumna *ainolórie,
ar quettalta vintuva ve quelexie hrávesse. Himenaio ar Fileto nát mici te.
Queni sine equérier inte i nanwiello, quétala in i enortie amartie yando sí, ar carilte quenelion savie iltanca.
Ananta Eruo tulca talma en tare, arwa *lihta sino: “I Héru ista véraryar”, ar: “Mauya ilquenen ye esta i Héruo esse hehta úfailie!”
Hoa coasse ear veneli lá rie maltava ar telpeva, mal yando toava ar cemne, ar veneli alcarinqua mehten, mal exeli mehten ú alcaro.
Etta, qui aiquen poita inse ho tai, nauvas vene alcarinqua mehten, airinta, *yuhtima yen same sa, manwaina ilya mára cardan.
Etta, uşa i nessime írellon, mal á roita failie, savie, melme, raine, as i yalir i Herunna et poica endallo.
Mal quera oa *auce ar *istyalóre minaşurier, istala i tulyalte costiennar.
Ono mauya in i Heruo mól ua costa, mal i náse moica illin, istala peanta, avaleryala inse íre perperis ulco,
moicave peantala i cotyain, an cé Eru antauva tien inwis, tulyala istyanna i nanwiéva,
ar entuluvalte laiceltanna et i Arauco remmallo, té i náner mapaine lo sé carien nírarya.
Mal á ista si: I métime auressen hrange lúmeli tuluvar.
An atani meluvar inte ar meluvar telpe, laitala inte – turquime, naiquetila, lá cimila ontaru, ú hantaleo, úsarte,
ú melmeo nosseltava, lemyala mi cos, quetila ulco pa exi, lá polila ture inte, verce, ú melmeo máriéva,
*vartandoli, *nirmunque, *valatie, melila maileltar or Eru,
arwe canto *ainocimiéva mal váquetila túrerya; ar taiti quenillon quera imle oa.
An ho té ortar i fincave tulir mir coar ar rembar nípe nissi i colir cólo úcariva, tulyaine lo *alavéle íreli.
Illume páralte, mal ualte oi pole tule istyanna i nanwiéno.
Tambe Yannes yo Yambres tarnet Mósenna, síve yando neri sine tarir i nanwienna, atalli aqua hastaine sámaltassen, quérine oa pa i savie.
Mal ualte tuluva ambe andave, an *sámalórielta nauva tanaina illin, ve yando náne *sámalórietta.
Ono elye ihílie peantienya, tienya coiviesse, mehtinya, savienya, cólenya, melmenya, voronwenya,
roitienyar, perperienyar – ve tai yar martaner nin mi Antiocía, mi Iconium, mi Listra – ar et ilyallon i Heru etelehtie ni.
É illi i merir same *ainocimya coivie mi Hristo Yésus nauvar roitaine.
Mal olce atani ar *hurumor menuvar mir oi ambe tumna ulco, tyarila exi ranya ar tyáraine ranya.
Mal elye, á lemya mí nati yar parnel ar yassen tancave savil, istala i queni illon parnel tai,
ar i ho anel hína sintel i airi tehteler, yar polir care lye saila rehtielyan, i saviénen ya Hristo Yésunen ná.
Ilye tehteler şúyaine lo Eru nar yando aşie peantien, querien oa loitie, carien nattor tére, paimestan failiesse.
Sie ece Eruo atanen náve aqua polila, nála manwaina ilya mára cardan.
Panyan lyesse i canwa epe Eru ar Hristo Yésus, ye namuva i coirear ar i qualinar, ar ilceryanen ar aranieryanen:
Á cara sinwa i quetta, tara manwa máre lúmissen ar lúmissen yar uar máre, quera oa loitie, queta narace quettar, queta hortale, arwa ilya cóleo peantielyasse.
An tuluva lúme yasse ualte lavuva i aşea peantie, mal vére íreltainen comyuvalte *peantari i cityar hlarultat,
ar i nanwiello querulvaltexer oa, mal ranyuvalte, hilyala hurila nyarnar.
Mal elye, hepa laicelda ilye natissen, perpera ulco, mola ve quén carila sinwa i evandilyon, á telya núromolielya.
An inye ná yando sí *etulyaina ve *sucieyanca, ar i lúme yasse nauvan leryaina hare ná.
I mára mahtie amahtien, onórien i norie i mettanna, ehépien i savie.
Ho sí i ríe failiéva ná nin sátina, ta ya i Heru antauva nin enta auresse, mal lá inyen erinqua, mal yando illin i emélier i apantie seva.
Cara ilqua ya polil tulien ninna rongo!
An Lémas ehehtie ni, pan méles Arda sina, ar oantes Şessalonicanna, Hrescens Alatianna, Títo Lalmatianna.
Rie Lúcas lemya óni. Á tala Marco ar áse care tule aselye, an náse aşea nin mi núromolienya.
Mal Tihico ementien Efesonna.
Íre tulil, á tala i colla ya hehtanen Troasse as Carpo, ar i parmar, or ilqua i helmaparmar.
Alexander i *urustan carne rimbe ulculi nin – i Heru *nampaityuva se ve cardaryar nar –
ar yando elye tira imle pa sé, an tarnes quettalmannar mi ita cotya lé.
Íre veryanen imne mí minya lú, *úquen manyane ni, mal ni-hehtanelte – nai ta ua nauva nótina téna –
ono i Heru tarne óni ar turyane ni, mérala in inyenen i *nyardie nauva telyaina ar in ilye i nóri hlaruvar sa; ar anen etelehtaina i rauro antollo.
I Heru ni-etelehtuva ilya olca cardallo ar rehtuva ni meneldea aranieryan. Sen na i alcar tennoio ar oi. Násie!
Á *suila Prisca ar Aquila ar Onesifóro nosse!
Erasto lemne Corintesse, mal Trofimo hehtanenye hlaiwa Miletosse.
Cara ilqua tulien nó i hríve!Euvolo *suila lye, ar sie cárar Purens ar Lino ar Hlauria ar ilye i hánor.
Nai i Heru euva as fairelya! Nai lisserya euva aselde!
Paulo – Eruo mól ar Yésus Hristo apostel, Eruo cílinaron saviénen ar i istyanen i nanwiéva ya hilya áyallo Eruva,
i estelden i oira coiviéva pa ya Eru, ye ua pole hure, antane vanda nó oialie lúmeli,
ómu vére lúmeryassen apantanes quettarya cariénen sa sinwa, i molie yan inye náne sátina i canwanen Eru *Rehtolvallo –
Títonna, anwa hína i saviesse ya uo samingwe: Lisse yo raine ho Eru i Atar ar Hristo Yésus *Rehtolva.
Sina castanen lye-tyarnen lemya Hrétasse, carielyan tére i penye nattor ar satielyan amyárar ilya ostosse, ve cannen lyen:
qui ea aiquen napsa pella, erya verio veru, arwa savila hínalion i uar nu napsa verca coiviéno hya amortala.
An mauya in *úquen pole quete ulco pa *ortirmo ve Eruo mardil. Mauya in uas quén ye fasta inse, lá linta rúşenna hya antaina limpen hya quén ye palpa hya ná milca úfaila ñétien,
mal nilda etteleain, meldo máriéno, málesse sámo, faila, aire, istala ture inse,
tancave hepila i voronda quetta peantieryasse, ecien sen horta i aşea peantiénen ar yando naitya i quetir ana se.
An ear rimbe *útúrime queni, i quetir cumne nati ar şahtar i sáma, or ilqua i himyar i *oscirie.
Mauya holta antolta, an queni sine nuquerir quande nossi peantiénen pa ya ua vanima, íresse úfaila ñetiéva.
Quén mici te, véra Erutercánolta, quente: "I Hrétear illume hurir, nála ulce hravani, úmolila cumbar."
Ya quetis nanwa ná. Etta áte naitya mi naraca lé, samieltan mále i saviesse,
lá címala Yúre nyarier ar axani atanion i querir inte oa i nanwiello.
Ilye nati nar poice poicain, mal quenin i nar vahtaine ar ú sáviéno munta poica ná, mal sámalta ar *immotuntielta véla nar vahtaine.
Quetilte in istalte Eru, mal cardaltainen lalalte se, pan nalte yelwe ar amortala, ar pa ilya mára carie nalte quérine oa.
Mal elye, queta yar nar aşie *málea peantien!
Mauya yáre nerin náve nalde haimeltassen, *sanwelunge, málesse sámo ar saviéno ar melmeo ar voronwiéno.
Mi imya lé, mauya i yáre nissin lenga áyanen, lá quetila ulco pa exi hya nála móli olya limpeo, peantala ya mára ná,
sie *rentala i nesse nissi i mauya tien mele verultar, mele hínaltar,
same mále sámo, náve poice, molila i coasse, nála mani, panyala inte nu verultar – pustien aiquen naiquetiello Eruo quetta.
Mi imya lé á horta i nesse neri náve hande,
mi ilye nati tánala imle ve *epemma máre cardaron. Peantielyasse á tana i nalye alahasta, *sanwelunga,
quetila aşea questa ya *úquen pole nattire; sie cotya quén nauva naityaina, pénala şaura nat ya polis quete pa vi.
Nai móli panyuvar inte nu herultar mi ilqua, fastala te mai, lá *nancarpala,
lá pilila, mal tánala mára savie, sie netyala i peantie *Rehtolvo, Eru, mi ilye nati.
An i Erulisse ya tala rehtie ilye atanin anaie apantaina,
peantala ven hehta i Erucotie ar i mardo íri ar mare mi mále sámo ar failie ar Eruáya mi randa sina,
yétala ompa i valima estelenna ar i alquarinqua apantie i túra Eruva ar *Rehtolvava, Hristo Yésus,
ye antanexe vi-etelehtien ilya *şányelóriello ar poitien insen lie aqua véra – lie uryala carien máre cardar.
Queta nati sine ar á horta ar naitya, quanta túrelyanen. Áva lave aiquenen nattire lye!
Alye *renta te i panyuvaltexer nu turier ar héri, náven manwe ilya mára cardan.
Á quetir ulco pa *úquen, la merila cos, nála maxe sámo, lengala moicave ilye atanin.
An enge lúme yasse yando elve náner ú handeo, lá *canwacimye, tyárine ranya, ve móli írion ar *alavéle mailion, lemyala mi ulco ar *hrúcen – yelwe, tévala quén i exe.
Mal íre *Rehtolvo ar Ainolvo nildie ar melme ataniva náner apantaine,
– lá faile cardainen yar elve carner, mal oravieryasse – vi-rehtanes i sovallenen ya vi-care nóne ata ar envinyante, Aire Feanen.
Ulyanes úvea lesta fea sino venna, ter Yésus Hristo, *Rehtolva.
Sie, apa náve *failante tana Queno lissenen, polilve náve aryoni estelo oira coiviéva.
Voronda ná i quetta, ar nati sine merin i tancave tulcal; sie i asávier Erusse polir tace sámaltar i cariesse máre cardaiva. Nati sine nar máre ar aşie atanin.
Mal quera imle oa ho *auce centar ar ontaleparmar ar costier ar costi pa i Şanye, an taiti nati nar ú yáveo ar cumne.
Quera oa quén vintala únanwa peantie apa minya ar attea orie,
istala i taite quén equériexe i tiello ar úcára, nála námina insenen.
Íre mentan Artemas hya Ticico lyenna, cara ilqua ya polil tulien ninna Nicopolisse, an tasse *aşantien termare i hrívesse.
Á anta Sénas i *Şanyengolmon, ar Apollon, ilye maurettar lendattan, peniettan munta.
Mal nai véralvar yando paruvar care máre cardar ar quate i nírala mauri, uien pene yáve.
Illi i ear asinye *suilar lye. Á *suila i melir me i saviénen.Nai i Erulisse euva as illi mici le!
Paulo, mandosse Yésus Hriston, ar Timoşeo hánolma, Fílemon meldalmanna, ye mole aselme,
ar Apfia néşalmanna, ar Arhipponna ye ná ohtar aselme, ar i ocombenna ya ea coalyasse:
Nai samuvalde oravie ar raine ho Eru Atarelva ar i Heru Yésus Hristo.
Illume hantan Ainonya ire quetin pa lye hyamienyassen,
pan hláran pa melmelya ar savielya ya samil Yésus Hristova ar ilye i airiva,
antielyan exelin ranta savielyo, i istyanen ilya mánava ya samilve Hristosse.
An camnen túra alasse ar tiutie melmelyanen, an i airion lauce felmi nar envinyante elyenen, háno.
Sina castanen, ómu samin túra huore Hristosse antien lyen canwa pa ya ná vanima,
merin ambe rato horta lye melmenen, nála ya nanye: Paulo, amyára, sí yando mandosse Hriston.
Hortan lye pa hínanya ye ónen naxenyassen: Onésimo,
yá ú *yuhtiéno elyen, mal sí nála *yuhtiéno lyen ar nin véla.
Sé tyarin nanwene lyenna, ta ná, véra endanya.
Mernen hepitas asinye, náveryan núro nin nómelyasse, mí naxi yar colin i evandilyonen.
Mal ú lavielyo uan mere care *aiqua, ar sie mane cardalya ua nauva maustanen, mal véra léra cilmelyanen.
Cé sina castanen anes mapaina lyello şinta lúmen, *nancamielyan se tennoio,
lá ambe ve mól mal ve amba la mól, ve melda háno, or ilqua inyen, mal manen ita ambe elyen, i hrávesse ar i Herusse véla.
Etta, qui sanal pa ní ve arwa ranto aselye, cama se ve qui camumnel ni.
Ente, qui acáries *aiqua raica hya same rohta lyen, nota ta inyenna.
Ni Paulo téca véra mányanen: Paityuvan – lá nyarien lyen i rohtalya inyen ná véra quenelya.
Ná, háno: nai inye samuva ñetie lyello i Herusse! Alye envinyata lauce felmenyar Hristosse.
Nála tanca i caruval ve méran técan lyenna, istala i caruval yando amba, han ya quetin.
Mal mí imya lú á manwa nin nóme marien, an estelinya ná i hyamieldainen nauvan leryaina len.
Epafras, ye ea mandosse óni Yésus Hristosse, *suila lye;
yando Marco, Aristarco, Lémas, Lúcas – i molir asinye.
Nai i Heru Yésus Hristo lisse nauva as fairelda!
Eru, ye andanéya mi rimbe lúli ar mi rimbe léli quente atarilvannar i Erutercánoinen,
i mettasse sine aurion equétie venna Yondonen, ye asáties aryon ilye nation, ar ter ye acáries Ea.
Náse alca alcareryo ar emma nasseryo, cólala ilye nati quettaryo túrenen, ar apa carie poitie úcarelvaiva hamnes undu i Meletyo foryasse tarmenissen.
Sie anaies carna túra epe vali, nála aryon esseo alcarinqua epe esselta.
An manenna mici vali oi equéties: "Elye ná yondonya; ónen tye síra" – ? Ar ata: "Inye nauva atarya, ar sé nauva yondonya."
Mal íre ata mentas *Ernónarya mir ambar, quetis: "Ar nai ilye Eruo vali *tyeruvar se!"
Ar pa i vali quetis: "Caris valaryar faireli, ar i molir sen, ruine náreva."
Mal pa i Yondo: "Eru ná mahalmalya tennoio ar oi, ar aranielyo vandil ná vandil tériéva.
Eméliel failie, ar etéviel *şanyelórie. Etta Eru, Ainolya, *ilívie lye millonen, ambe lá sartolyar."
Ar, "I yestasse, a Heru, elye tulcane cemen, ar menel ná taman mályato.
Tú autuvat, mal elye termare; ar yúyo yeryuvatte ve colla,
ar ve larma tu-tolúval, ve colla, ar nauvatte vistaine. Mal elye i imya ná, ar loalyar uar pustuva."
Mal pa man mici vali oi equéties: "Hama ara formanya tenna panyan ñottolyar ve tulco talulyant."
Ma uar illi mici te núrofaireli, mentaine *vevien in nauvar aryoni rehtiéno?
Sina castanen mauya tumnave cime yar ahlárielve, vi-pustien oi luttiello oa.
An qui i quetta quétina ter valali anaie nanwa, ar ilya ongwe ar loitie cime axan camner faila paimetie,
manen uşuvalve qui oloitielve cime rehtie ta túra? I yestasse anes quétina lo Herulva, ar anes tulcaina elven lo i hlasser se,
íre yando Eru *vettane tanwalínen ar elmendalínen ar taure carielínen ar etsatielínen aire feava, ve indómerya náne.
An lá nu vali apánies i ambar ya tuluva, pa ya quétalve.
Mal mi nóme quén *evettie, quétala: "Mana atan, enyalielyan se, hya atanyondo, cimielyan se?
Apánielyes pityave nu vali; antanel sen ríe alcarwa ar laitiéva; ar panyanelyes or mályato tamani.
Ilye nati apániel nu talyat." An íre Eru panyane ilye nati nu sé, ea munta yan láves náve han túrerya. Sí en ualve cene ilye nati panyaine nu sé.
Mal é yétalve Yésus, ye náne carna pityave nu vali, arwa ríeo alcarwa ar laitiéno apa lendes ter qualme, tyavieryan qualme ilya quenen, Eruo lissenen.
Náne mára sen yen ilye nati ear ar ter ye ilqua ea, íre tulyumnes rimbe yondoli alcarenna, care i Cáno rehtielto ilvana ter perperiéli.
An ye airita ar i nar airinte illi tulir ho Er, ar sina castanen uas naityana pa estie te hánor,
ve quetis: "*Nyarduvan esselya hánonyain; i ocombeo endesse lye-laituvan lírinen."
Ar ata: "Panyuvan estelinya sesse." Ar ata: "Yé, inye ar i híni i nin-ánie Eru!"
Pan i híni samir ranta mi hráve yo serce, yando sé mi imya lé camne ranta mi tú, qualmenen nancárala ye sáme i túre tyarien qualme, ta ná, i Arauco,
ar etelehtien illi i caurenen qualmeva náner móli quanda coivieltasse.
An uas manya vali, mal Avrahámo nossen manyas.
Etta mauyane sen náve ve hánoryar ilye natisssen, náveryan órávala ar voronda héra *airimo, *yacien *cámayancali pa i lieo úcari.
An pan sé asámie perperiéli íre anes tyastaina, polis manya i nar tyastaine.
Etta, airi hánor i samir ranta i yaliénó menelenna, cima i apostel ar héra *airimo ye *etequentalve – Yésus.
Anes voronda yen se-carne sie, ve náne yando Móses quanda coasse tana Quenwa.
An sé ná nótina valda alcaro túra lá ta Móseo, pan ye acarastie coa same laitie túra lá ya i coa same.
An ilya coa ná carastaina lo quén, mal ye acarastie ilye nati ná Eru.
Ar Móses ve núro náne voronda i quanda coasse tana Quenwa, ve *vettie natalion pa yar mo quetumne epeta,
mal Yésus náne voronda ve Yondo i coasse tana Quenwa.
Etta, ve i Aire Fea quete: "Síra qui lastalde ómaryanna,
áva care endalda hranga, ve íre queni tyarner sára rúşe i auresse tyastiéva i erumesse,
íre atarildar ni-şahtaner tyastiénen, apa cennelte cardanyar ter loar *canaquean.
Etta feuyanen *nónare sina ar quente: Illume ranyalte endaltasse, ar té uar isintier tienyar.
Sie quenten vanda endanyasse: Ualte tuluva mir sérenya!"
Tira inde, hánonyar, pustien aiquen mici le oi samiello olca enda ú saviéno, tyárala le hehta i coirea Eru.
Mal á horta quén i exe, ilya auresse íre en ea ya mo esta "síra", pustien aiquen mici le návello carna sarda i úşahtiénen úcareo.
An asámielve ranta as i Hristo. Erya maurelva ná i hepilve tenna i tyel i talma ya sámelve i yestasse.
Íre ná quétina: "Síra, qui lastalde ómaryanna, áva care endalda hranga, ve íre queni tyarner sára rúşe"
– man náner i queni i lastaner, ananta tyarner sára rúşe? Ma i carner sie uar náne illi i lender et Mirrandorello, tulyaine lo Móses?
Ente, man Eru feuyane ter loar *canaquean? Ma ualte náne i úcarner, i queni ion loicor lantaner i erumesse?
I queni in vandanen váquentes tule mir sérerya, man anelte, qui lá i loitaner cime canwaryar?
Sie cenilve i ualte lertane tule minna peniénen saviéva.
Etta, pan vanda lemya pa tulie mir sérerya, alve ruce, an *şéya i míci le ear queneli i oloitier sa.
An i evandilyon náne tulúna venna ar téna véla, mal i quetta ya hlasselte carne munta aşea tien, an ualte náne *ertaine saviénen as i lastaner.
An elve i asávier é tulir mir i sére, aqua ve equéties: "Vanda sina antanen rúşenyasse: Ualte tuluva mir sérenya" – ómu cardaryar náner telyaine tulciello i mardeva.
An mi nóme equéties i pa i otsea aure: "Ar Eru sende i otsea auresse ilye cardaryallon",
ar ata mi nóme sina: "Ualte tuluva mir sérenya."
Etta, pan ear en queneli i tuluvar mir sa, ar i camner i evandilyon i yestasse uar túle mir sa loitiénen cime canwaryar,
ata teas aure, quetiénen apa anda lúme Laviro lírisse: "Síra" – ve quentes yá – "Sira, qui hlarilde ómarya, áva care endalda hranga!"
An qui Yosua te-tulyane nómenna séreva, Eru lá quetumne epeta pa hyana aure.
É euva en *sendare-sére Eruo lien.
An ye utúlie mir Eruo sére, yando esérie cardaryallon, ve Eru carne véraryallon.
Etta, alve care ilqua ya polilve tulien mir sére tana, pustien aiquen lantiello mi imya lé: loitiénen cime i canwar.
An Eruo quetta ná coirea ar cole túre, ar nas aica epe ilya *yúmaicea macil, *térala tenna *ciltas fea ar faire, axor ar *axende, ar pole tunta i endo sanwi ar incar.
Ar ua ea onna ya ua cénina lo sé, mal ilye nati nar helde ar pantaine henyant, sé yenna mauya ven hanquete.
Etta, cénala i samilve túra héra *airimo ye elendie ter i meneli, Yésus i Eruion, nai hépalve *etequentielva.
An samilve ve héra *airimo, lá quén ye ua ista same *ofelme aselve íre nalve milye, mal quén ye anaie tyastaina mi ilya lé ve nar elve, mal ú úcareo.
Etta nai tuluvalve i mahalmanna Erulisseva arwa huoreo, camien oravie ar hirien lisse vi-manyala lúmelvasse maureva.
An ilya héra *airimo mapaina ho mici atani ná panyaina rá atanin or nati Eruva, *yacien annar ar *yancar pa úcari.
Istas lenga milyave in penir istya ar nar ránie, pan yando sé illume tunta véra milya sómarya,
ar síve caris *yancar rá i lien, tambe mauya sen care *yancar yando rá insen – pa úcare.
Ente, quén ua came alcar sina insenen, mal eryave íre náse yálina lo Eru, ve náne yando Áron.
Sie yando i Hristo ua *alcaryane inse, ve qui carnesexe héra *airimo. Ye se-carne sie náne ye quente pa se: "Tyé ná yondonya; inye síra óne tye."
Mi imya lé quetis yando hyana nómesse: "Elye ná *airimo tennoio, mi lérya Melciserec."
Hráveryo auressen Yésus mentane hyamiéli ar arcandeli yenna polle se-rehta et qualmello, polde rambelínen ar nírelínen, ar anes hlárina áyaryanen Eruva.
Ómu anes Yondo, parnes náve *canwacimya i natainen yar perpéres.
Ente, apa anes carna ilvana, anes ye sate oira rehtie illin i cimir canwaryar.
An anes tultaina lo Eru, náven héra *airimo, mi lérya Melciserec.
Pa sé samilme rimbe nati quetien – ar tyarie le hanya tai ná urda, pan hlarielda anaie carna lenca.
An ómu sí carnelde arya qui anelde *peantalli, elden mauya i quén peanta len ata i minye nati Eruo quetto. Samilde maure ilinwa, lá ronda matsova,
an ilquen ye suce ilimo ua maite pa i quetta failiéva, an náse hína.
Mal ronda matso ná in nar aqua álienwe, in *yuhtiénen peánier tuntieltar manen *ciltie márie ulcullo.
Etta, apa autie i minye natillon i peantiéno Hristo, alve cesta i quanta hande, lá ata tulcala talma pa inwis qualini cardallon ar savie Erusse,
i peantie pa *tumyaler ar i panie maiva, i *enortie qualiniva ar oialea namie.
Ar ta caruvalve, qui Eru lave.
An qui queni mi lú er acámier i cala ar atyávier i anna menello ar asámier lesta Aire Feo,
ar atyávier Eruo mára quetta ar i túlala Ardo túri,
mal alantier oa, tá mo ua pole envinyata inwisselta, an tarwestalte ata i Eruion inten ar naityar se.
An i talan ya suce i miste ya rimbave lanta sanna, ar cole olvar yar nar máre *alamoryain, came aistie Erullo.
Mal qui colis neceli ar *nelmasirpi, nas nótina ve munta ar hare návenna hunta, ar i mettasse nas urtaina.
Ómu quetilme lenna sie, meldar, pa lé nalme tance i arye nation yar tulyar rehtienna.
An Eru ua úfaila, loitala enyale mótielda ar i melme esseryava ya tannelde ve núror i airin, ve en nalde.
Ar merilme i ilquen mici le tanuva uryala şúle samien i quanta tancie tenna i tyel.
Sie ualde nauva lence, mal caruvar ve té carir i saviénen ar cólenen nar aryoni i vandaron.
An íre Eru carne vandarya Avrahámen, antanes vandarya insenen,
pan ua enge aiquen ambe túra yenen ence sen anta sa.
Ar sie, apa Avraham tanne cóle, camnes vanda sina.
An atani antar vandaltar yenen ná ambe túra, ar vandalta ná i metta ilya costo, pan nalte naute sanen.
Lé sinasse Eru, íre mernes tana i vando aryonin manen *alavistima *şantierya náne, carnexe nauta tien.
Sie, nat attanen yat uat pole ahya ar pa yat Eru ua pole hure, elve i uşúşier i caumanna polir same túra horme hepien i estel panyaina epe ve.
Estel sina samilve ve *ciryampa i fean, tulca ar tanca, ar rahtas han i fanwa,
i nómenna yanna Yésus lende ar pantane i malle ven, sé ye anaie carna héra *airimo tennoio, Melcisiréco lénen.
Melciserec sina, aran Sálemo, *airimo i Antura Eruo, velle Avraham íre nanwennes apa i apaire or i arani ar aistane se.
Sen Avraham antane quaista ilye nation. Minyave náse, ve esserya tea, aran failiéva, ar tá yando aran Sálemo, ta ná, aran raineo.
Náse ú ataro hya amillo ar pene ontaleparma, ar uas same yesta aurion hya metta coiviéno. Sie náse emma i Eruiono, lemyala ve *airimo tennoio.
Cima mai manen túra náse, nér sina yen Avraham, i *nossentur, antane quaista i arye armaron mapaine i ohtasse.
Nanwa, i neri i ontalesse Lévillo i camir i *airimosse samir axan i Şanyesse pa mapie quaistar i liello, ta ná, vére hánoltallon, ómu yando té nar Avrahámo nosseo.
Mal nér sina, yeo ontale ua tule ho té, namper quaistali Avrahamello ar aistane ye sáme i vandar.
Ilya cos pella, i ambe pitya ná aistana lo i ambe túra.
Ar i minya nattosse, nereli i quélar camir quaistar, mal i attea nattosse: quén ye same i *vettie i náse coirea.
Ar, qui lertan sie carpa, ter Avraham yando Lévi ye came quaistar apátie quaistali,
an enges en ataryo oşwesse íre Melciserec velle se.
Qui mo é camne ilvana sóma ter i *airimosse Léviron – an ta náne ranta íre i Şanye náne antaina i lien – manen enge en maure i ortumne hyana *airimo, Melciseréco lénen ar lá quétina náve Árono lénen?
An i nér pa ye nati sine nar quétine náne nóna mir hyana nosse, yallo *úquen *evévier ara i *yangwa.
An ve ilquen ista, Herulva túle ho nosserya Yehúra, ar Móses quente munta pa *airimor ho nosse tana.
Ar ná en ambe *aşcénima i orta hyana *airimo, vávea Melciserecenna,
ye ná carna sie, lá i şanyenen axano ya caita hrávenna, mal i túrenen *alanancárima coiviéno.
An *vettiesse ná quétina: "Nalye *airimo tennoio, mi lérya Melciserec."
Sie i ambe arinya axan ná panyaina oa, an nas milya ar ua aşea.
An i Şanye carne munta ilvana, mal i talie arya estelwa carne sie: i estel yanen túlalve hare Erunna.
Ta ua martane ú vando.
An é ear exeli i anaier cárine *airimor ú vando, mal ea quén arwa vando antaina lo i Er ye quente pa sé: "I Héru ánie vandarya, ar uas vistuva cilmerya: Nalye *airimo tennoio."
Etta véren ta arya Yésus anaie antaina ve varnasse.
Ente, mauyane rimbalin náve *airimóli, pan qualme pustane te lemiello,
mal sé, pan samis vórea ar oira coivie, same *airimosserya ú neuroron.
Etta ece sen yando aqua rehta i tulir Erunna ter sé, pan náse illume coirea, arcien tien.
An síte héra *airimo náne ven aşea: aire, *ulculóra, *alavahtaina, *ciltaina úcarindollon, ar carna tára lá menel.
Ua mauya sen ilya auresse, ve yane hére *airimor carner, *yace *yancali: minyave pa vére úcareryar ar tá pa i úcari i lieo (an ta carnes erya lú íre *yances inse),
an i Şanye sate nereli ve *hére airimor ómu nalte milye, mal i quetta i vando ya túle apa i Şanye sate Yondo, ye ná carna ilvana tennoio.
I onotie i quétine nation ná si: Samilve síte héra *airimo, ar ahámies undu i foryasse i mahalmo i meneldea Meletyava,
ve núro i aire nómeo ar i nanwa *lancoasse, ya i Héru ortane, ar lá atan.
An ilya héra *airimo ná sátina *yacien annali ar yancali véla, ar mauyane yando quén sinan same nat *yacien.
Qui enges sí cemende, únes *airimo, an ear i *yacir annar i Şanyenen.
Mal té *veuyar ve *epemma ar leo i meneldie nation, aqua ve Móses, íre carumnes i *lancoa, camne i valaina canwa. An quetis: "Cima i caril ilye nati ve i emma tanaina lyen i orontesse."
Mal sí Yésus acámie ambe maira *núronóme, ar sie náse enelmo véreo ya mi vávea lé ná arya, tulcaina arye vandalinnar.
An qui i minya vére náne ilvana, *úquen cestumne nóme attean,
an é tuntas cáma íre quetis: "Yé! I auri tuluvar, quete i Héru, íre caruvan vinya vére as i már Israélo ar i már Yehúro,
lá ve i vére ya carnen as atariltar i auresse ya nampen málta ar te-tulyane et Mirrandorello. An té uar lemyane vérenyasse, ar ualte valdie nin ambe, quete i Héru.
An si ná i vére ya caruvan as Israélo már apa auri ente, quete i Héru: Panyuvan şanyenyar sámaltassen, ar endaltasse tecuyanyet. Ar nauvan Ainolta, ar té nauvar lienya.
Ar laume peantulvalte, ilquen armarorya ar ilquen hánorya, quétala: "Á ista i Héru!" An illi mici te istuvar ni, i ampityallo i anturanna mici te.
An nauvan órávala pa úfaile cardaltar, ar laume enyaluvan úcareltar ambe."
Íre quetis "vinya vére", ehehties i noa vére. Mal ta ya ná hehtaina ar yeryaina rato autuva.
Yando i noa vére é sáme şanyeli pa *vevie ar yána ya náne mar sino.
An mo carne minya *lancoa-şambe yasse enger i *calmatarma ar i tanie i massaiva, ar nas estaina i Aire.
Mal ca i attea fanwa enge *lancoa-şambe estaina i Aire Airion.
Tasse enger i *yangwa *nisqueo ar i Vérecolca, aqua tópina maltanen, yasse enge i *camma maltava arwa i máno ar Árono vandil ya tuiane ar i *palmat i véreo;
mal or sa enget i alcarinque ceruvu, yat teltanet i túpo. Mal sí ua i lúme quetien pa ilya erya mici nati sine.
Apa nati sine náner cárine sie, i *airimor lender mir i minya *lancoa-şambe ilye lúmessen carien *núromolier,
mal mir i attea şambe i héra airimo erinqua lelya, erya lúmesse mi loa er, lá ú serceo, ya *yacis insen ar i lieo úcarin yar nar cárine ú istyo.
Sie i Aire Fea lave ven hanya i ua náne i tie mir i aire nóme carna sinwa íre i minya *lancoa tarne.
*Lancoa sina náne emma pa i lúme ya sí utúlie, ar ve i emma náne, annali ar *yancali nar sí yácine. Mal tai uar pole care i nér ye care *núromolie, ilvana pa *immotuntierya,
mal appar munta hequa matsoli ar yuldali ar *alavéle *tumyaléli. Anelte axalli i hráveo, tenna i lúme carien nati tére túle.
Mal íre Hristo túle ve héra *airimo i mánaron yar utúlier, ter i ambe hoa ar ambe ilvana *lancoa lá carna mainen, ta ná, lá ontie sino,
lendes erya lú ar tennoio mir i aire nóme, lá arwa serceo nyéniron ar nesse mundoron, mal arwa véra serceryo, *ñetala oira etelehtie ven.
An qui i serce nyéniron ar mundoron ar *litte yaxio, *palastaina innar anaier vahtaine, pole airita tenna carie i hráve poica,
manen olya ambe serce i Hristo, ye oira fairenen *yancexe ú vaxeo Erun, poituva *immotuntielvar qualini cardallon, molielvan i coirea Ainon?
Ar sie náse enelmo vinya véreo. Etta i nar yáline polir came i vanda oira *aryoniéno, pan qualme amartie lehtien te *nanwerenen ongweltallon nu i noa vére.
An yasse ea métima nirme, mauya náve tulcaina i ye tence sa, ná qualin.
Lá nó náse qualin nas túresse, pan uas same túre mí lúme ya i quén ye carne sa, ná coirea.
An íre ilya axan técina i Şanyesse náne quétina lo Móses i lienna, nampes i serce i nesse mundoron ar i nyéniron as nén ar carne tó ar *airaşea ar *palastane i parma imma ar i quanda lie,
quétala: "Si ná i serce i véreo ya Eru apánie lesse."
Ar *palastanes i *lancoa ar ilye i veni i *veviéno mí imya lé, i sercenen.
I Şanyenen *harive ilye nati nar poitaine sercenen, ar qui serce ua ulyaina, ua ea apsenie.
Etta, maurenen, i emmar i meneldie nation náner tainen poitaine, mal i meneldie nati inte anaier poitaine *yancalínen yar nar arye lá taiti *yancar.
An Hristo lende mir, lá ainas cárina mainen, mal menel imma, tulien epe Eruo cendele rá ven.
Ente, uas túle *yacien inse rimbave, ve i héra *airimo mene mir i ainas loallo loanna, túlula serce ya ua vérarya.
Tá mauyane sen perpere rimbave tulciello i mardeva. Mal sí acáries inse sinwa mi erya lú ar tennoio i rando tyeldesse, panien oa úcare *yaciénen inse.
Ar ve ná sátina Atanin quale erya lú, mal epta námie,
sie yando i Hristo náne *yácina erya lú ar tennoio colien rimbalion úcari. Uas tuluva pa úcare mí attea lú ya tanuvas inse, in yétar senna camien rehtie.
An pan i Şanye same leo i túlala mánaron, mal lá i quanta emma i nation, i *yancar *yácine loallo loanna uar pole care ilvane i talar tai.
Qui carnelte, ma mo ua pustane *yace, i *tyerindor nála poitaina erya lú ar tennoio, lá ambe tuntala úcare intesse?
Úsie, sine *yancainen ea enyalie úcariva loallo loanna,
an i serce mundoron ar nyéniron ua pole mapa úcare oa.
Etta, íre tulis mir i mar quetis: "*Yanca ar *yacie ualye merne, mal manwanel hroa nin.
Ual same alasse urtaine *yancassen, hya *yancassen pa úcari."
Tá quenten: "Yé, utúlien – i *toluparmasse ná técina pa ni – carien indómelya, Eru!"
Apa minyave quentes: "Ual merne hya sáme alasse mi *yancar ar *yacier ar urtaine *yancar ar úcare-yancar" – yar nar *yácine i Şanyenen –
tá quetis: "Yé, utúlien carien indómelya." Mapas oa i minya tulcien i attea.
Tana "indómenen" nalve airinte i *yaciénen i hroava Yésus Hristo, erya lú ar tennoio.
Ente, ilya héra *airimo tare aurello aurenna *vevien ar *yacien i imye *yancar rimbave, pan tai uar oi pole aqua mapa oa úcari.
Mal nér sina yance er *yanca pa úcari oiale ar hamne undu ara Eruo forma,
ho tá *lartala tenna ñottoryar nauvar panyaine ve tulco talyant.
An erya *yaciénen acáries i nar airinte ilvane tennoio.
Ente, i Aire Fea yando *vetta ven, an apa quetie:
"Si ná i vére ya caruvan aselte ente auressen, quéta i Héru: Panyuvan şanyenyar endaltasse, ar sámaltasse tecuvanyet",
yando quetis: "Ar úcareltar ar *şanyelóre cardaltar laume enyaluvan ambe."
Yasse ea apsenie taiva, ua ea en *yacie pa úcare.
Etta, hánor, pan samilve huore pa i tie mir i ainas Yésus Hristo sercenen,
tie ya pantanes ven ve vinya ar coirea tie ter i fanwa, ta ná, i hráve,
ar pan samilve túra *airimo or i coa Eruva,
alve tule arwe voronda endo ar aqua tanca saviéno, pan endalva ná poitaina olca *immotuntiello ar hroalvar sóvine poica nennen.
Alve en *etequenta estelelva, lá nála iltance, an ye antane i vanda ná voronda.
Ar alve cime quén i exe, hortien melme ar máre cardar,
lá hehtala yomenielvar, ve quenelion haime, mal hortala quén i exe, ar i ambe sie íre cenilde i aure túla hare.
An qui nirmenen úcarilve apa camie i istya i nanwiéva, ua ambe lemya *yanca pa úcari.
Mí men mo cauresse yéta ompa namienna ar uruiva urtala şúlenna ya ammatuva i tarir sanna.
Aiquen ye arácie Móseo Şanye quale ú oraviéno, íre ear atta hya nelde i náner astarmor.
Manen ita ambe naraca paimeo, sanalye, sé nauva valda ye *avattie i Eruionna ar ye oloitie note ve aire i serce i véreo yanen anes airinta, ar ye acárie i Erulisseo Faire rúşea nattiriénen?
An istalve ye quente: "Ninya ná ahtarie; inye accaruva", ar ata: "I Héru namuva lierya."
Ná rúcima lantie mir i coirea Eruo mát!
Mal enyala i noe auri, yassen, apa camnelde i cala, perpérelde túra mahtie, arwe urdiéli.
Enger lúli yassen anelde pantave antaine yaiwelin ar şangiélin véla, ar lúlissen sámelde ranta as exeli i lender ter síti nati.
An tannelde *ofelme in enger mandosse, ar mi alasse collelde i pilie armaldaiva, istala i haryanelde ya ná arya ar lemyala.
Etta, áva hate oa indello huorelda, ya cole as insa túra *paityale.
An samilde maure voronwiéva, camieldan ta pa ya Eru ánie vandarya, apa carie indómerya.
An "apa ita şinta lúme ye túla tuluva, ar uas nauva telwa."
Mal "failanya samuva coivie saviénen", ono qui "tucis inse oa, feanya ua same alasse sénen".
Elve uar i nostale ya tucixe oa mir nancarie, mal i nostale ya same savie, sie cámala coivie.
Savie ná i varnasse nation pa yar mo same estel, i tanie nanwaron yar mo en ua cene.
An síte saviénen i yárar camner *vettie.
Saviénen tuntalve i náne Ea *partaina Eruo quettanen, ar sie ya ná cénima utúlie yallo ua apantaina.
Saviénen Ável *yance Erun *yanca ambe mirwa epe ta Caino, ar tana saviénen camnes *vettie i anes faila, Eru *vettala pa annaryar, ar ter ta en carpas, ómu qualles.
Saviénen Énoc náne mapaina oa, se-pustien ceniello qualme, ar únes hírina, pan Eru nampe se oa. Nó anes mapaina camnes i *vettie i anes mára Eruo hendusse.
Ente, ú saviéno ua cárima náve mára sen, an mauya yen tule hare Erunna save i eas, ar i paityas aiquen ye cesta se.
Saviénen Nóa, apa sámes orie pa natali yar en uar náne cénine, tannexe Erunilda ar carastane marcirya i rehtien nosseryava, ar savieryanen namnes i mar, ar anes carna aryon i rehtiéno ya tule saviello.
Saviénen Avraham, íre anes yálina, carne ve i canwa ar *etelende mir nóme ya anes martyaina came ve aryono ranta, ar *etelendes lá istala yanna lelyumnes.
Saviénen marnes ve ettelea quén i nóresse vandava ve ettelea nóresse, ar marnes *lancoalissen as Ísac ar Yácov, i aner aryolli óse i imya vando.
An yentes ompa i ostonna arwa nanwe talmalion, yó şamno ar tano ná Eru.
Saviénen yando Sara camne túre nostien erde, ómu anes acca yára, pan nontes sé voronda ye antane i vanda.
Sie erya nerello, ye yando náne ve qualin, náner nóne hínali rimbe ve menelo tinwi ar ve i litsi earo fárasse, únótime.
Saviesse ilye queni sine qualler, ómu ualde cenne i vandar cárine nanwe, mal cenneltet hairello ar *suilaner tai, ar *etequentaner i anelte etteleáli ar etyali cemende.
An i quetir síti natali tanar i cestalte nóme ya nauva véralta.
Ananta, qui é hempelte sámasse i nóme yallo *etelendelte, ence tien nanwene.
Mal sí cestanelte arya nóme, ta ná, meneldea nóme. Etta Eru ua naityana pa te íre náse estaina Ainolta, an amanwies osto tien.
Saviénen Avraham, íre anes tyastaina, *yance Ísac, ar i nér ye acámie i vandar náne yácala *ernóna yondorya,
ómu náne quétina senna: "Ya naiva estaina 'erdelya' tuluva ter Ísac."
Mal intyanes i polle Eru orta se yando qualinillon, ar emmanen é camneses talo.
Ente, saviénen Ísac aistane Yácov ar Ésau pa i nati yar tulumner.
Saviénen Yácov, íre anes hare qualmenna, aistane yúyo yondoron Yósefo ar *tyerne nírala vandilyo inganna.
Saviénen Yósef, hare mettaryanna, carampe pa manen Israelindi lelyumner ettenna, ar antanes canwa pa axoryar.
Saviénen Móses náne nurtaina ter astor nelde lo ontaryat apa náve nóna, pan cennelte i lapseo vanie ar uar runcer i arano canwallo.
Saviénen Móses, apa túles vienna, váquente náve estaina Fárao selyeo yondo,
cílala perpere ulco as Eruo lie or samie alasse mi úcare ter lúme,
an nontes i *naityale Hristova mirwa epe Mirrandoro harmar, an yentes ompa i antienna i *paityaléva.
Saviénen oantes Mirrandorello, mal ú ruciéno i aranello, and an anes voronda ve qui cennes i *Alacénima.
Saviénen carnes i Lahtie ar i *palastie i serceva, pustien i *Nancarindo appiello minnónaltar.
Saviénen Yerico rambar lantaner apa anelte péline ter auri otso.
Saviénen Ráhav i *imbacinde ua qualle as i uar *canwacimye, pan camnes i larmor rainesse.
Ar mana amba quetuvan? An lúme ni-loituva qui nyarin pa Ireon, Varac, Samson, Yefta, Lavir ar yando Samuel ar i Erutercánor,
ye saviénen turuner araniéli, carner failie, camner vandali, pustaner raurolion antor,
tamper ruiveo túre, úşer macillion maicallon, milya sómallo náner cárine polde; anelte canye ohtasse, vintanelte ettelie hosseli.
Nisseli *nancamner qualiniltar *enortiénen, mal exeli náner ñwalyaine pan ualte merne lehtie *nanwerenen, an mernelte came arya *enortie.
Exeli camner yaiweli ar ripiéli, é amba la tai, nútelínen ar mandolínen.
Anelte nance sarnelínen, anelte tyastaine, anelte saraine mir rantali, quallelte nála nance macillínen, tompeltexer helmalínen mámaron ar nyéniron, íre anelte mauresse, şangiesse, perpérala ulca lengie.
Anelte acca mani mar sinan. Vantanelte mi erumeli ar rottoli ar eccali cemeno.
Ananta ilye queni sine, ómu camnelte *vettie pa savielta, uar ñente i vando *amaquatie.
An Eru apacenne arya nat elven; sie únelte cárine ilvane oa vello.
Etta, arwe ta hoa fanyo *vettolíva os ve, nai yando elve panyuva oa ilya lunga nat ar i úcare ya lintiénen vi-remba, ar alve nore voronwiénen i norme panyaina epe vi,
íre yétalve yenna ná savielvo Cáno ar ye care sa ilvana, Yésus. I alassen ya náne panyaina epe se perpéres i tarwe, nattírala *naityale, ar ahámie undu i foryasse i mahalmo Eruva.
Cima mai ye ocólie taite cotya carpie ho i úcarir intenna; tá ualde nauva lumbe ar taltuva fealdasse.
Mahtala úcarenna, ualde en atárie tenna serce,
mal alávielde i hortie ya quete lenna ve yondor: "Yonya, áva nattire paimesta i Héruo, ar áva talta íre camitye tulce quettaryar,
an yen i Héru mele, antas paimesta, an ripis ilquen ye camis ve yondo."
Paimestanen nalde voronde. Eru lenga len ve yondoin. An man ná i yondo yen i atar ua anta paimesta?
Mal qui nalde ú i paimesto yasse illi asámier ranta, nalve anwave hinali nostaine ettesse vesto, ar la yondoli.
Ente, sámelve hrávelvo atari antien ven paimesta, ar sámelve áya tien. Lau ita ambe carilve mai qui panyalvexe nu i Atar fairion, samien coivie?
An té ter şinta lúme antaner ven paimesta, ve náne mára mi hendulta, mal sé care ta pan nas aşea ven, camielvan ranta aireryasse.
Mí lú ya nas antaina, ilya paimesta *şéya, lá ve alasse, mal ve nyére; ananta epeta in nar peantaine sanen antas yáve raineva, ta ná, failie.
Etta á antorya i mát yat lingat undu ar i milye occat,
ar cara tére maller taluldant. Sie ya ná *úlévima ua nauva panyaina et lequettello, mal ambe rato nauvas nestaina.
Á roita raine as ilye queni, ar i airitie ú yó *úquen cenuva i Heru,
íre cimilde i *úquen nauva nehtaina lisseo Eruo ar uar lave hloirea şundun tuia ama tyarien urdie ar vahtien rimbali.
Sie ua euva aiquen ye *úpuhta hya aiquen úaire, ve Ésau, ye quaptalesse erya matten antane oa minnónierya.
An yando lé istar i epeta, ire mernes harya i aistie, anes quérina oa. An ómu nírelissen cestanes inwis, uas hirne nóme san.
An sí ualde utúlie hare yanna mo pole appa ar ya ná nartaina ruivenen, ana lumbo ar huive ar raumo
ar hyólo lamma ar i óma ya carampe; íre hlasselte óma tana, i lie arcane i munta amba náve quétina tien.
An ualte polle cole i axan: "Ar qui celva appa i oron, mauya i náse nanca sarnelínen!"
Ente, ya cennelte náne ta rúcima i tyarnes Móses quete: "Rúcan ar pélan!"
Mal utúlielde hare Oron Síonna ar i coirea Eruo ostonna, meneldea Yerúsalem, ar valannar *quaihúmi
ócómienwe, ar i ocombenna i minnónaron *minatécine menelde, ar Erunna ye na illion námo, ar fairennar i failaron i nar cárine ilvane,
ar Yésunna, i enelmo vinya véreo, ar i serce *palastiéva, ya carpa mi lé arya epe Ávelo serce.
Cena i ualde quere oa ye quéta! An qui té uar úşe i querner se oa íre camnelde orie cemende, ita ambe elve lauvar, qui querilvexer yello quete menello.
Lúme yanasse ómarya palle i cemen, mal sí ánies vanda, quétala: "Mi attea lú paluvan, lá cemen erinqua, mal yando menel."
I quettar "mi attea lú" tear i mapie oa i nativa yar anaier cárine, ar sie i nati yar uar páline lemyuvar.
Etta, pan camuvalve *alapálima aranie, nai en samuvalve Erulisse, yanen lertalme *veuya Erun mi *ainocimie ar áya.
An Ainolva na yando ammátala ruive.
Nai hánonilmelda lemyuva!
Áva loita anta nilme etteleain, an ter sa queneli, ú istiéno, acámier valali ve *aşaliltar.
Enyala i ear mandosse, ve qui nalde tasse aselte, ar i perperir ulco, pan yando elde nar en hroasse.
Nai vesta nauva *alcarvalda mici illi, ar i vestacaima ú vaxeo, an Eru namuva i *úpuhtar ar i racir vesta.
Nai lélda coiviéno nauva ú melmeo telpeva, íre sanalde pa i nati yar ear sí ve fárie len. An equéties: "Laume hehtuvanyes ar laume autuvan lyello."
Etta lertalve same huore ar quete: "I Héru ni-manya; uan rucuva. Mana pole atan care nin?"
Cima i tulyar le, i equétier Eruo quetta lenna, ar íre sanalde pa lengielto yáve, á hilya savielta.
Yésus Hristo ná i imya noa ar síra ar tennoio.
Áva na cóline oa lo *alavéle ar ettelie peantier, an ná aşea i endan náve carna tanca Erulissenen, lá matsonen, ya ua anaie aşea in cimir ta.
Samilve *yangwa yallo i *veuyar ara i *lancoa uar lerta mate.
An i hroar i celvaron ion serce ná talaina mir i aire nóme lo i héra *airimo, nar urtaine ettesse i *estoliéno.
Sie yando Yésus, airitieryan i lie véra serceryanen, perpére ettesse i ando.
Etta alve lelya senna ettesse i *estoliéno, cólala i naityala carpie ya sé colle,
an ualve same sisse osto ya lemya, mal cestalve ta ya tuluva.
Ter sé alve illume *yace Erun *yanca laitaléva, ta ná, yáve ho péli yar *etequentar esserya.
Ente, áva lave i carien márieva hya i antien exin talta sámaldallo, an tai nar *yancali yar nar írime Erun.
Á himya i canwar ion tulyar le ar á panya inde nu te, an tirilte fealdar ve queneli i antuvar onótie; sie ecuva tien care si arwe alasseo ar lá arwe siquiléo, an ta ua aşea len.
Hyama elmen, an nalme tance i samilme mára *immotuntie, mérala lenga mai mi ilye nattor.
Mal hortan i carilde si or ilqua, entulienyan lenna ta ambe rato.
Sí nai i Aino raineva, ye talle ama qualinillon i túra mavar mámaron arwa i serceo oira véreo, Yésus Herulva,
antuva len ilya mána ve carmar carien indómerya, cárala vesse Yésus Hristonen ya ná mára henyatse. Nai euva sen i alcar tennoio ar oi! Násie.
Sí hortan le, hánor, i colilde quetta sina hortaléva, an mentan len tenna mance quettalíva.
Á ista i Timoşéo, hánolva, anaie etelehtaina. Qui tulis lintiénen, le-cenuvan óse.
Á anta *suilienya illin i tulyar le, ar ilye i airin. I ear Italiasse *suilar le.
Nai i Erulisse euva as illi mici le!
Ontaleparma Yésus Hristo, Laviro yondo, Avrahámo yondo:
Avraham óne Ísac, ar Ísac óne Yácov, ar Yácov óne Yehúra ar hánoryar,
Yehúra óne Peres ar Sera as Tamar, ar Peres óne Hesron, ar Hesron óne Ram,
ar Ram óne Amminarav, ar Amminarav óne Naxon, ar Naxon óne Salmon,
ar Salmon óne Voas as Ráhav, ar Voas óne Óver as Rút, ar Óver óne Yesse,
ar Yesse óne Lavir i aran. Ar Lavir óne Solomon i verinen Úrio,
ar Solomon óne Rehóvoam, ar Rehóvoam óne Ávia, ar Ávia óne Ása,
ar Ása óne Yóhyafat, ar Yóhyafat óne Yóram, ar Yóram óne Ussia,
ar Ussia óne Yótam, ar Yótam óne Áħas, ar Áħas óne Ħesecia,
ar Ħesecia óne Manasse, ar Manasse óne Ámon, ar Ámon óne Yosía,
ar Yosía óne Yeconya ar hánoryar, i lúmesse ya i lie náne tulyana oa ve etyar Vávelenna.
Apa i *autulyale Vávelenna Yeconya óne Hyealtiel, ar Hyealtiel óne Seruvável,
ar Seruvável óne Áviur, ar Áviur óne Elyacim, ar Elyacim óne Ásor,
ar Ásor óne Sároc, ar Sároc óne Ácim, ar Ácim óne Eliur,
ar Eliur óne Eleásar, ar Eleásar óne Mattan, ar Mattan óne Yácov,
ar Yácov óne Yósef verurya María, lo ye Yésus, ye ná estaina Hristo, náne nóna.
Sie ilye i *nónari Avrahamello Lavirenna náner canaque, ar Lavirello i *autulyalenna Vávelenna ear *nónari canaque, ar i *autulyalello Vávelenna i Hristonna *nónari canaque.
Mal Yésus Hristo náne nóna mi lé sina: Íre amillerya María náne nauta vestien Yósef, anes hírina as hína i Aire Feanen, nó anette *ertane.
Mal Yósef verurya, ye náne faila ar ua merne nucume se, merne lerya se nuldave.
Mal apa sannes sin, yé! i Heruo vala túle senna oloresse, quétala: “Yósef Lavirion, áva ruce mapiello María verilya coalyanna, an ya ná nostana sesse i Aire Feanen ná.
Coluvas yondo, ar estuvalyes Yésus, an etelehtuvas lierya úcariltallon!”
Ilye sine nati martaner carien nanwa ya i Heru carampe tercánoryanen, quétala:
“Yé! i vende nauva as hína ar coluva yondo, ar antauvalte sen i esse Immanuel” – ya tea “Aselve Eru”.
Tá Yósef, apa cuivierya húmeryallo, carne ve i Heruo vala cannelyane sen, ar nampes verirya coaryanna.
Mal uas sinte se nó coldes yondo, ar antanes sen i esse Yésus.
Apa Yésus náne nóna Vet-Lehemesse Yúreo, mi Herol i arano rí, en! elentirmor rómenye ménallon túler Yerúsalemenna,
quétala: “Masse ná ye anaie nóna aran Yúraron? An Rómesse cennelme tinwerya, ar utúlielme luhtien epe se!”
Íre hlasses si, Aran Herol náne valtaina, ar quanda Yerúsalem óse,
ar comyala ilye i hére *airimor ar i parmangolmor imíca i lie, maquentes téna pa masse i Hristo nauvane nóna.
Quentelte senna: “Vet-Lehemesse Yúreo, an sie ná técina lo i *Erutercáno:
'Ar elye, Vet-Lehem Yúreo, laume ná i ampitya imíca i cánor Yúreo; an elyello tuluva túro, ye nauva mavar Israel lienyan.' ”
Tá Herol nuldave tultane i elentirmor ar maquente te pa i minya lú ya cennelte i tinwe.
Te-mentanes Vet-Lehemenna quétala: “Mena, á cesta i hína, ar íre ihírieldes, nyara nin, polienyan lelya luhtien senna, yú inye!”
Íre hlasselte i aran lendelte oa, ar yé! i tinwe ya cennelyanelte Rómesse lende epe te, tenna pustanes or i nóme yasse enge i hína.
Cenie i tinwe antane tien túra alasse.
Lendelte mir i coa ar cenner i hína as María amillerya, ar lantala undu luhtanelte senna. Pantanelte harmaltar ar antaner sen annali: malta ar *ninquima ar níşima suhte.
Mal íre náne tien nyárina oloresse pa i raxe nanweniéno Herolenna, lendelte nóreltanna hyana mallenen.
Apa oantelte, yé! Héru vala tannexe Yósefen oloresse, quétala: “Á orta, á mapa i hína ar amillerya ar uşa Mirrandorenna, ar á lemya tasse tenna quetuvan lyenna; an Herol cestuva i hína nehtien se!”
Ar orontes ar nampe i hína ar amillerya óse lómisse, ar oantes Mirrandorenna,
ar lemnes tasse tenna Herolo qualme. Sie náne carna anwa ya quétina né lo i Heru tercánoryanen, quétala: “Et Mirrandorello yallen yondonya!”
Tá Herol, cénala i anes carna auco lo i elentirmor, náne quátina túra ahanen, ar mentanes ohtalli nehtien ilye seldor Vet-Lehemesse ar ilye i ménassen pelila sa, ho loa atta ar nu ta, ve i lúme pa ya camnelyanes istya i elentirmollon.
Tá náne carna anwa ya quétina né ter Yeremía i Erutercáno, quétala:
“Óma náne hlárina Ramasse, yaime ar túra nainie – Ráħel yaimea hínaryain, ar uas merne tiutale, an nalte vanwe!”
Apa Herolo qualme, en! Héru vala tannexe Yósefen mi olos Mirrandoresse,
ar eques: “Á orta, á mapa i hína ar mena mir Israélo nóre, an i merner mapa i híno cuile nar qualini!”
Orontes ar nampe i hína ar amillerya ar lende mir Israélo nóre.
Mal íre hlasses i Arcelaüs turne Yúreasse nómesse Herol ataryo, runces meniello tar. Oloresse náne sen nyárina pa raxerya, ar oantes mir i ména Alíleo.
Túles ar marne ostosse estaina Násaret, carien nanwa ya náne quétina ter i Erutercánor: “Nauvas estaina Násarya!”
Yane auressen túle Yoháno i *Sumbando, carala sinwa mi ravanda Yúreo:
“Hira inwis, an menelo aranie utúlie hare!”
An nér sina náne pa ye carampe Yesaia i Erutercáno íre quentes: “I óma queno yámala i ravandasse: Alde manwa i Heruo malle, cara tieryar tére!”
Yoháno náne vaina collasse findíva ulumpion ar sáme *aluquilta *os osweryar, ar matsorya náne *salquecápor ar verca lis.
Tá Yerúsalem ar quanda Yúrea ar i quanda nóre pelila Yordan etelender senna,
ar anelte sumbane lo sé Yordan·síresse, *etequétala úcareltar.
Íre cennes rimbe Farisali ar Sandúceali túla i sumbienna, quentes téna: “Nónar laucion, man peantane len uşe i túlala rúşello?
Tá cola yáve valda i inwisso!
Ar áva sana i lertalde quete indenna: 'Atarelva Avraham ná' – an inye quete lenna in Eru pole orta ama híni Avrahámen sine sarnillon.
Yando sí i pelecco cáya ara şundor i aldaron: Sie ilya alda ya ua cole mára yáve nauva círina undu ar hátina mir i náre.
Inye sumba le nennen, mir inwis. Mal ye tuluva apa ni ná taura epe ni – inye ua valda mapa i hyapatu talyalto. Sé le-sumbuva Aire Feanen ar nárenen.
*Saltamarya ea máryasse, ar poituvas *vattarestarya. Comyauvas orirya mir i haura, mal i *ospor urtauvas nárenen ya úquen pole *luhtya.
Tá Yésus lende Alíleallo Yordanna ar túle Yohánonna náven sumbana lo se.
Mal Yoháno váquente senna, ar eques: “Maurenya ná i inye nauva sumbana lo lyé, ar elye túla ninna!”
Mal hanquentasse Yésus quente: “Ásan lave marta, an sie ná vent vanima care ilqua ya faila ná!” Tá hé láve sen.
Apa Yésus náne sumbana túles térave et i nenello, ar yé! menel náne sen pantana, ar cennes Eruo Faire túla undu senna ve cucua.
Yé! yú enge óma menello ya quente: “Si ná yondonya, i melda, yenen nanye fastana!”
Tá Yésus náne tulyana lo i Faire ama mir i erume, náven sahtana lo i Arauco.
Uas mante auressen *canaquean ar lómissen *canaquean, ar apa lúme sina anes maita.
Ar i *Sahtando túle senna ar quente: “Qui elye Eruo yondo ná, queta sine sarninnar: Ola massar!”
Mal hanquentasse quentes: “Ná técina: Atan ua coita rie massanen, mal ilya quettanen ya tule Eruo antollo!”
Tá i Arauco se-talle i aire ostonna ar se-panyane i cordo telmasse.
Quentes senna: “Qui elye Eruo yondo ná, hata inse undu, an ná técina: Canuvas valaryain pa lye, ar coluvaltel máltatse, pustien talelya petiello sarnenna!”
Eque Yésus: “Ata ná técina: Áva tyasta i Heru Ainolya!”
Ata i Arauco se-talle ita tára orontenna, ar tanne sen ilye i aranier cemeno ar alcarelta.
Ar quentes senna: “Ilye nati sine antauvan elyen qui lantal undu ar *tyeril ni!”
Tá Yésus quente senna: “Heca, Sátan! An ná técina: I Heru Ainolya alye *tyera, ar sé erinqua alye *veuya!”
Tá i Arauco oante sello, ar yé! valali túler ar *veuyaner se.
Apa hlarie i Yoháno náne panyana mandosse, lendes oa mir Alílea.
Oantes Násaretello ar túle ar marne Capernaumesse ara ear, Sevulondo ar Naftalio ménassen,
carien anwa ya náne quétina ter Yesaia i Erutercáno íre quentes:
“A nóre Sevulondo ar Naftalio, i earmalle Yordan pella, Alílea i nórion!
I lie hámala morniesse cenne hoa cala, ar in hamner ñuruhuineo ménasse cala oronte!”
Lúme yanallo carnes mentarya sinwa ar quente: “Hira inwis, an menelo aranie utúlie hare!”
Vantala ara Ear Alíleo cennes háno atta, Símon ye ná estaina Péter ar Andréas hánorya, hátala rembe mir i ear, an anette *raitoli.
Ar quentes túna: “Tula, áni hilya, ar caruvan let *raitoli atanion!”
Mí imya lú hehtanette rembetta ar se-hilyanet.
Lelyala talo cennes yú exe atta yet nánet hánor, Yácov Severaion ar Yohannes hánorya, i luntesse as Severai ataretta, envinyatála rembettar – ar yaldeset.
Mi imya lú hehtanette i lunte ar ataretta, ar hilyanettes.
Ar lendes ter quanda Alílea, peantala *yomencoaltassen ar carila sinwa i evandilyon pa i aranie ar nestala ilya hlíve ar ilya quáme imíca i lie.
Ar i nyarie pa se lende mir quanda Síria, ar tulunelte senna ilye engwar, nwalyaine hlívelínen ar nwalmelínen rimbe nostaleron: raucoharyainali ar ránahlaiwali ar taptali – ar nestanes te.
Ar hoe şangar se-hilyaner, ho Alílea ar i Ostor Quean ar Yerúsalem ar Yúrea, ar i nóriello Yordan pella.
Cénala i şangar lendes ama mir i oron, ar íre hamunes hildoryar túler senna.
Ar pantanes antorya ar peantane tien, quétala:
“Valime nar i penyar fairesse, an té haryuvar menelo aranie!
Valime nar i samir naire, an té nauvar tiutane!
Valime nar i moicar, an té nauvar aryoni cemeno!
Valime nar i maitar ar soicar failien, an té nauvar quátine!
Valime nar i antar oravie, an té camuvar oravie!
Valime nar i poicar endo, an té cenuvar Eru!
Valime nar i carir raine, an té nauvar estaine Eruo yondor!
Valime nar i roitainar failiénen, an tien menelo aranie ná!
Valime nalde íre queni le-naityar ar le-roitar ar húrala quetir ilya ulco pa le, castanyanen.
Alde na valime ar capa alassenen, an hoa ná *paityalelda menelde.
Elde nar cemeno singe; mal qui i poldore auta i singello, manen singwa tyáverya nanwenuva? Uas ambe mára erya naten, mal ná hátina ettenna, yasse queni *vattuvar sanna.
Elde nar i mardo cala. Osto orontesse ua cúvima.
Mo ua narta calma ar sa-panya nu calpa, mal i calmatarmasse, ar caltas ilquenna i coasse.
Sie yú elde tyara calalda calta atannar, polieltan cene máre cardaldar ar antaveltan alcar Atareldan, ye ea menelde.
Áva sana in inye utúlie mapien oa i Şanye hya i Erutercánor. Uan utúlie mapien oa, mal carien nanwa!
An quetin lenna i menel cemenye autuvat nó erya ampitya tengwa hya erya tehta autuva i Şanyello, nó ilqua anaie carna nanwa.
Sie, ilquen ye race er mici i ampitye axani ar peanta atanin sie, isse nauva estaina i ampitya, menelo araniesse. Mal ilquen ye hilya sa ar peanta exi pa sa, isse nauva estaina túra menelo araniesse.
An quetin lenna i qui failielda ua túra epe i failie i parmangolmoron ar i Farisaron, laume tuluvalde mir menelo aranie.
Hlasselde i náne quétina i yárannar: Áva nahta; mal aiquen ye é nahta tuluva nu namie!
Mal inye quete lenna in aiquen ye ná rúşea hánoryanna tuluva nu namie, ar ilquen ye quete hánoryanna: 'Ráca!' nauva námina lo i Tára Ocombe, mal aiquen ye quete 'Auco!' nauva martyana Ehenno náren!
Qui, tá, túlal i *yangwanna tálala annalya, ar tasse enyalil i hánolya same costie aselye,
alye hehta annalya tasse epe i *yangwa, ar mena oa: Minyave cara rainelya as hánolya, ar tá, íre enutúliel, lertal tala annalya.
Na linta carien raine as ñottolya lan en nalye óse i mallesse – hya i ñotto antauva lye olla i námon, ar i námo i *námonduren, ar nalye hátina mir mando.
Násie quetin lyenna: Laume tulil et talo nó apaitielye i métima pitya mitta tyelpeva!
Ahlárielde i anaie quétina: 'Áva race vestale!'
Mal inye quete lenna in ilquen ye yéta nís milyala se, yando tá arácie vestale óse endaryasse.
Qui forya hendelya le-sahta, ása narca ettenna ar ása hate oa lyello! An qui er mici hroamittalyar vanwa ná, ta ná len arya epe i quanda hroalya ná hátina mir Ehenna.
Ar qui formalya lye-sahta, ása aucire ar ása hate lyello. An qui er mici hroamittalyar vanwa ná, ta ná len arya epe i quanda hroalya tule mir Ehenna.
Ná quétaina: Mauya yen lehtaxe veriryallo sen-anta tecie *aumentavéva.
Mal inye quete lenna: Ilquen ye lehtaxe veriryallo hequa castanen úpuhtiéva, tyare se race vestale. Ar ye verya nissenna ye anaie mentana oa, race vestale.
Ente, ahlárielde i náne quétina i yárannar: Áva anta húrala vanda, mal hepa i Hérun i vandar yar ániel.
Mal inye quete lenna: Aqua áva anta vandar, lá menelden, an nas i mahalma Eruva,
hya cemennen, an nas i tulco talyant, hya Yerúsalemnen, an nas osto i taura aranwa.
Yando áva anta vandar carelyanen, an ualye pole care erya finde ninque hya morna.
Quetielya nauva ná, ná – lá, lá! Ya ná han si, ulcuo ná.
Ahlárielde i anaie quétina: Hen henden, ar nelet nelcen!
Mal inye quete lenna: Áva tare yenna care ulco lenna. Qui aiquen le-pete forya ventalyasse, quera yú i exe senna.
Qui aiquen mere lelya námonna aselye mapien laupelya, ásen lave mapa yando collalya.
Ar qui aiquen mauya lye hilya se *larnelta er, á lelya óse *larnelta atta.
Á anta yen arca lyello, ar áva quere imle yello mere *yuhta aiqua ya samil.
Ahlárielde i ná quétaina: Mela armarolya ar teva ñottolya!
Mal inye quete lenna: Mela ñottolyar, á aista i hútar le, cara mai in le-tevir, ar hyama in le-roitar,
náveldan híni Atareldo menelde. An isse tyare Anarya orta or ulcar ar mani, ar lave ulo lanta failannar ar úfailannar.
Qui melilde i melir le, mana *paityalelya? Ma i *tungwemor uar care i imya nat?
Ar qui *suilal rie hánolyar, manen si ná túra nat? Ma queni i nórion uar care i imya?
Na, tá, ilvane, tambe Atarelda menelde ilvana ná.
Cima i ualde care failielda epe atani náven tíraine lo té, hya ualde samuva *paityale Atareldallo menelde.
Etta, íre antal annar óraviéva, áva lamya róma epe lye, ve i *imnetyandor carir i *yomencoassen ar i mallessen, camien alcar ho té. Násie quetin lenna: Cámalte quanda *paityalelta.
Mal elye, íre antal annar óraviéva, áva lave hyarmalyan ista ya formalya cára;
sie annalyar óraviéva nauvar nuldasse; tá Atarelya, ye tíra nuldasse, paityuva lyen.
Ente, íre hyamilde, áva na ve i *imnatyandor, an melilte hyame íre táralte i malleron vincassen, náven cénime atanin. Násie quetin lenna: Cámalte quanda *paityalelta.
Mal elye, ire hyamil, mena mir véra şambelya, ar apa holtie fennalya, hyama Atarelyanna ye ea nuldasse; tá Atarelya ye nuldave tíra, paityuva lyen.
Mal íre hyámal, áva quete i imye nati mi vorongandale, ve queni i nórion carir, an sanalte i nauvalte hláraine *yuhtiénen rimbe quettali.
Etta, áva na ve té, an Eru Atarelda ista maureldar nó arcalde senna.
Hyama, tá, sie: Átarelma menelde, na aire esselya!
Aranielya na tuluva! Na care indómelya cemende tambe menelde!
Ámen anta síra ilaurea massalma,
ar ámen apsene úcarelvar síve elme apsenir tai tien i úcarir elmen.
Álame tulya úsahtienna mal áme etelehta ulcullo! Násie!
Qui apsenilde atanin ongweltar, yú apsenuva len Atarelda menelde;
mal qui ualde apsene atanin, Atarelda mi imya lé ua apsenuva ongweldar len.
Íre helpilde lamate, áva na ve *imnatyandor, i carir cendelelta úvanima carien lamatelta cénima atanin. Násie quetin lenna: Cámalte quanda *paityalelta.
Mal elye, íre hepilde lamate, á panya millo carelyasse ar sova cendelelya.
Sie lamatelta ua nauva cénima atanin, mal Atarelyan ye ea nuldasse; ar Atarelya, ye nuldave tíra, paityuva len.
Áva comya len harmar cemende, yasse malo ar ñwarie ammatir, ar yasse arpor racir minna ar pilir.
Mal á comya len harmar menelde, yasse malo ar ñwarie uar ammate, ar yasse arpor uar race minna ar pilir.
An yasse harmalya ea, tasse yú endalya euva.
Hroalyo calma ná i hen. Etta, qui hendelya ná málesse, quanda hroalya nauva calima,
mal qui hendelya ná olca, quanda hroalya nauva morna. Qui i cala ya ea lyesse ná mornie, manen hoa i mornie ná!
*Úquen pole náve mól heru attan; tevuvas i mine ar meluva i exe, hya himyuvas i mine ar yeltuva i exe. Ualde pole náve móli Erun ar Mammonden véla.
Etta quetin lenna: Áva na *tarastaine pa inde, pa mana matuvalde hya mana sucuvalde, hya pa hroalya, pa i lanni yar coluvalde. Ma i cuile ua amba epe matta ar i hroa epe i larmar?
Cima menelo aiwi, i ualte rere hya *cirihta hya comya mir haurar, ananta meneldea Atarelda anta tien matta. Ma ualde valdie epe té?
Man imíca le pole napane erya *perranga coivieryan?
Ente, pa lanni, mana castalda náven *tarastaine? Para i resto indilinen, manen ualte mole ar ualte lia,
mal nyarin len i Solomon alcareryasse úne tópina ve erya mici té.
Qui Eru sie netya i olvar i restasse, i ear sisse síra ar nauvar vanwe enwa, manen ita ambe lárave topuvas lé, elde pitya saviéno?
Etta áva na *tarastaine ar quete: “Mana matuvalve?” hya “Mananen topuvalve inwe?”
An ilye nati sine i nóri roitar. Mal meneldea Atarelda ista i samilde maure ilye sine nativa.
Á cesta minyave i aranie ar failierya, ar ilye sine nati nauvar napánine len.
Etta, áva na *tarastaine pa i hilyala aure, an i hilyala aure samuva vére *tarastieryar. Ilya réo ulco farya san.
Áva name, lá náven námine,
an i námiénen yanen námal nauval námina, ar i lestanen yanen *etelestal nauva *etelestana lyen.
Tá manen ná i yétal i lisce hanolyo hendesse, mal ua cime i pano véra hendelyasse?
Hya manen ece lyen quete hánolyanna: 'Lava nin tuce i lisce et hendetyallo', íre yé! ea pano véra hendelyasse?
*Imnetyando! Minyave tuca i pano et véra hendelyallo, ar tá polil cene manen tuce i lisce et hánolyo hendello.
Áva anta ya aire ná i huoin; mi imya lé, áva hate marillaldar epe polcar, hya *vattuvaltet nu talultar ar queruvar inte ar narcuvar le.
Á arca, ar nauva len antana; á cesta, ar hiruvalde, á tamba, ar nauva len latyana.
An ilquen arcala came, ar ilquen cestala hire, ar ilquenen tambala nauva latyana.
Man imíca le, qui yondorya arca massa, antauva sen sar?
Hya qui arcas lingwe – lau antauvas sen leuca?
Etta, qui elde i nar olce istar anta máre annar hínaldain, manen ita ambe Atarelda ye ea menelde antauva máre nati in arcar sello?
Etta, ilye nati yar merilde cárine len lo atani, cara tien mí imya lé. Ta ná ya tear i Şanye ar i Erutercánor.
Mena minna ter i náha ando, an palla ar yanda ná i malle ya tulya oa nancarienna, ar rimbe nar i menir minna ter sa,
mal náha ná i ando ar lenwa i malle tulyala mir coivie, ar mance nar i hirir sa.
Tira inde pa i *hurutercánor i tulir lenna mámaron lannessen, mal mityasse nalte ammátala ñarmor.
Yáveltainen istuvaldet. Queni uar comya *tiumar necelillon hya *relyávi *nastalaimallon, lá?
Sie ilya mára alda cole mára yáve, mal ilya quelexima alda care şaura yáve.
Mára alda ua pole cole úmirwa yáve; mi imya lé, quelexima alda ua pole cole mára yáve.
Ilya alda ya ua cole mára yáve ná círina undu ar hátina mir i náre.
É yáveltainen istuvaldet!
Lá ilquen ye quete ninna: Heru, Heru, tuluva mir menelo aranie, mal ye care i indóme Atarinyo ye ea menelde.
Rimbali quetuvar nin enta auresse: 'Heru, Heru, ma ualme quente ve Erutercánor esselyanen, ar et-hanter raucor esselyanen, ar carner rimbe taurie cardali esselyanen?'
Mal tá nyaruvan tien: 'Uan oi sinte le! Oa nillo, elde i racir i şánye!”
Etta ilquen ye hlare sine quettanyar nauva ve handa nér, ye carastane coarya i ondosse.
Ar i ulo ulle undu, ar i oloiri túler, ar i súri vávaner ar penter coa tananna, mal uas lantane, an anes tulcana i ondosse.
Ente, ilquen ye hlare sine quettanyar ar uar care ve tai ná ve úhanda nér, ye carastane coarya i litsesse.
Ar i ulo ulle undu, ar i oloiri túler, ar i súri vávaner ar penter coa tananna, ar talantes, ar lantarya náne hoa!”
Sí íre Yésus telyane ilye quettar sine, i şángar náner elmendasse peantieryanen,
an peantanes ve quén arwa túreo, ar lá ve parmangolmoltar.
Ar apa túles undu i orontello hoe şangar hilyaner se.
Ar yé, halmahlaiwa quén túle senna ar lantane undu epe se, quétala: “Heru, qui meril, polil poita ni!”
Ar rahtala máryanen se-appanes, quétala: “Merin! Na poica!” Ar mi imya lú anes poitana halmahlíveryallo.
Ar Yésus quente senna: “Cena i quetil munta aiquenna, mal mena, á tana imle i *airimon, ar *yaca i anna ya Móses canne, ve *vettie tien!”
Ar íre lendes mir Capernaum, *tuxantur túle senna, arcala senna
ar quétala: “Heru, núronya caita *lalevíte i coasse, arwa rúcime ñwalmaron!”
Ar quentes senna: “Íre tulin, nestuvanyes!”
Mal i *tuxantur hanquente sie: “Heru, uan valda i tulil nu tópanya, mal qui rie quetil i quetta, núronya nauva nestana.
An yú inye ná nér panyana nu túre, arwa ohtarion nu ni, ar qui quetin quén sinanna: Mena!, tá ménas, ar exenna: Tula!, tá túlas, ar núronyanna: Cara si!, tá carises!”
Hlárala ta, Yésus náne quátina elmendanen ar quente innar hilyaner se: “Násie quetin lenna, taite savie uan ihírie aiquende Israelde.
Mal quetin lenna i tuluvar rimbali Rómello ar Númello ar caituvar ara i sarno as Avraham ar Ísac ar Yácov menelo araniesse,
lan i araniéno yondor nauvar hátine mir i mornie i ettesse. Tasse euvar yaiwelta ar mulielta nelciva!”
Tá Yésus quente i *tuxanturenna: “Mena! Síve asáviel, tambe á marta lyen!” Ar i núro náne nestana yana lúmesse.
Ar Yésus, íre túles mir coarya Péter, cenne veriryo amil caimassea ar hlaiwa úrenen.
Ar Yésus appane márya, ar i úre váne hello. Ar hé oronte ar *veuyane sen.
Mal apa şinye túle, queni taller senna rimbali i náner haryane lo raucor, ar et-hantes i raucor quettanen, ar nestanes illi i perpéraner,
carien nanwa ya náne quétina ter Yesaia i Erutercáno, quétala: “Sé nampe hlívelvar ar colde quámelvar!”
Íre Yésus cenne şanga ya pelde se, cannes i lelyumnelte i hyana fáranna.
Ar parmangolmo túle senna ar quente senna: “*Peantar, hilyuvan lye ilya nómenna yanna lelyal!”
Mal Yésus quente senna: “Rusqui samir eccar ar menelo aiwi samir *haustar, mal i Atanyondo ua same nóme yasse lertas panya carya!”
Tá exe mici hildoryar quente senna: “Heru, lava nin minyave auta ar tala atarinya sapsaryanna!”
Yésus quente senna: “Áni hilya, ar lava i qualinain tala qualinaltar sapsaltannar!”
Ar íre lendes mir lunte, hildoryar hilyaner se.
Ar yé, enge túra amortale i earesse, túpala i lunte i falmainen, mal isse náne lorna.
Ar túlelte ar eccoitaner se, quétala: “Heru, áme rehta, quálalme!”
Mal quentes téna: “Manen ná i nalde caurie, queni pitya saviéno?” Tá, ortala, naityanes i súri ar i ear, ar enge núra quilde.
Mal i neri náner quátine elmendanen ar quenter: “Mana nostaleo ná nér sina, pan yú i súri ar i ear carir ve quetis?”
Íre túles i hyana fáranna, mir i nóre i Araryaron, nér atta haryaine lo raucoli vellet se, túlala et i noirillon, ita verce. Etta *úquen sáme i huore cilien tana malle.
Ar yé! yámette, quétala: “Mana ment ar lyen, Eruion? Ma utúliel sir nwalien met nó i lúme?”
Mal haira tello enge lámáre rimbe polcalíva nesselesse.
I raucor arcaner senna, quétala: “Qui et-hatil me, áme menta mir i lámáre polcaiva!”
Ar quentes téna: “Mena!” Ar apa ettulie lendelte mir i poicar, ar yé! i quanda lámáre lende rimpa olla i lanca mir i ear, ar quallelte i nenissen.
Mal i mavari úşer, ar lelyala mir i osto nyarnelte ilqua, yú pa i raucoharyanar.
Ar yé! i quanda osto etelende velien Yésus, ar apa cenie se, hortaneltes auta ménaltallon.
Ar lelyala mir i lunte lahtanes i ear ar lende mir véra ostorya.
Ar yé, tallelte senna *lalevíte nér caitala caimasse. Cénala savielta Yésus quente i *lalevítenna: “Huore, hína! Úcaretyar nar apsénine!”
Ar yé, queneli mici i parmangolmor quenter intenna: “Nér sina naiquéta!”
Ar Yésus, istala sanweltar, quente: “Manen ná i sánealde olce natali endaldasse?
An mana i ambe ancárima nat quetien: Úcaretyar nar apsénine, hya: Á orta ar vanta – ?
Mal istieldan in i Atanyondo same túre cemende apsenien úcari –” tá quentes i *lalevítenna: “Á orta, á mapa caimalya ar mena marelyanna!”
Ar orontes ar lende maryanna.
Cénala ta, i şangar náner ruhtaine ar antaner alcar Erun, ye antane taite túre atanin.
Ar autala talo Yésus cenne nér hára i *tungwemende, estaina Mattéo, ar quentes senna: “Áni hilya!” Ar orontes ar hilyane se.
Epeta, íre caines ara i sarno i coasse, yé! rimbe *tungwemóli ar úcarindoli túler ar cainer tasse as Yésus ar hildoryar.
Mal cénala ta, i Farisar quétaner hildoryannar: “Manen ná i *peantarelda mate as *tungwemóli ar úcarindoli?”
Hlárala sa quentes: “Queni málesse uar same maure aşarova, mal i nimpar.
Mena ar para ya si tea: Merin óravie ar lá *yacie. An túlen yalien, lá failar, mal úcarindor!”
Tá Yoháno i *Sumbando hildor túler senna ar maquenter: “Manen ná i elme ar i Farisar hepir lamati, mal hildolyar uar hepe lamati?”
Ar Yésus quente téna: “I endero meldor lau polir same naire íre i ender ea aselte? Mal rí tuluvar íre i ender nauva mapana oa tello, ar tá hepuvalte lamati.
*Úquen tace *lilma *alapihta lanneo yára collasse, an quanta túrerya tucuva sallo, ar euva ambe faica narcie.
Ente, *úquen panya vinya limpe yáre *helmolpessen, mal qui mo é care sie, i *helmolpi nar *rúvine ar i limpe ulya ettenna ar i *helmolpi nar nancarne. Mal queni panyar vinya limpe vinye *helmolpessen, ar yúyo lemyat!”
Íre nyarnes téna nati sine, yé! tur túle hare ar luhtane senna, quétala: “Selyenya *şintanéya qualle, mal tula ar á panya málya sesse, ar nanwenuvas coivienna!”
Tá Yésus oronte ar se-hilyane, as hildoryar.
Ar yé! nís arwa celumeo serceva ter loar yunque túle ca se ar appane collaryo lane,
an quentes insenna: “Qui eryave appan collarya nauvan rehtana!”
Yésus quernexe ar, tuntala se, quente: “Huore, selye! Savielya erehtie lye!” Ar yana lúmello i nís sáme mále.
Íre túles coanna i turwa ar cenne i simpetari ar i şanga cárala yalme,
Yésus quente: “Á auta silo, an i vende ua qualle, mal náse lorna!” Tá lalanelte senna yaiwenen.
Apa mentave i şanga ettenna lendes minna ar nampe márya, ar i vende oronte.
Ar nyarna sina ettúle quanda sana ménanna.
Íre Yésus lahtane talo, laceníte nér atta hilyanet se, yámala ar quétala: “Órava metse, Lavirion!”
Apa menie mir i coa, i laceníti túler senna, ar Yésus quente senna. “Ma saviste i polin care si?” Hanquentette senna: “Ná, Heru!”
Tá appanes henduttat, quétala: “Nai martuva lent ve saviste!”
Ar henduttat camnet *céne. Ente, Yésus tulcave canne tun, quétala: “Cena in *úquen ista sa!”
Mal tú, íre lendette ettenna, nyarne pa se mi quanda sana ména.
Sí íre oantelte, yé! queni taller senna úpa nér haryaina lo rauco,
ar apa i rauco náne et-hátina, i úpa nér carampe. I şangar náner quátine elmendanen ar quenter: “Nat ve si ua oi anaie cénina Israelde!”
Mal i Farisar quenter: “I raucoron herunen et-hatis i raucor!”
Ar Yésus lende rindesse ilye i ostonnar ar mastonnar, peantala *yomencoaltassen ar cárala sinwa i evandilyon i araniéno ar nestala ilya nostale hlíveo ar ilya nostale nimpiéno.
Íre cennes i şangar, endarya etelende téna, an anelte helde ar vintane ve mámar ú mavaro.
Tá quentes hildoryannar: “I yávie hoa ná, mal i molir nar mance. Etta arca i yaviéno Herunna i mentauvas *molindor yávieryanna!”
Ar tultanes i hildor yunque ar antane tien túre or úpoice fairi, et-hatieltan te, ar nestieltan ilya hlíve ar ilya nimpa sóma.
Essi i hildoron yunque nar sine: Minyave Símon ye ná estaina Péter, ar Andréas hánorya, ar Yácov yondorya Severai, ar Yohannes hánorya,
Filip ar Varşoloméo, Tomas ar Mattéo i *tungwemo, Yácov yondorya Alféo ar Şandéo,
Símon i Canáanya ar Yúras Iscariot, ye apa antane se olla.
Yunque sine Yésus etementane, antala tien canwar sine: “Áva mene i mallesse i nórennar, ar áva tule mir Samárea osto.
I mende mena i vanwe mámannar Israélo coallo!
Lan lelyalde, á cara sinwa i menta, quétala: 'Menelo aranie utúlie hare.'
Á nesta hlaiwar, á orta qualinar, á poita halmahlaiwar, et-hata raucor! Acámielde muntan, á anta muntan!
Áva panya malta hya telpe hya urus quiltaldasse
hya tala mattapoco i lendan, hya laupe atta, hya hyapat hya vandil, an ye mole ná valda mattaryo.
Ilya ostosse hya mastosse mir ya tulilde, ceşa man sasse valda ná, ar á lemya tasse tenna autalde.
Íre tulilde mir i coa, ása *suila,
ar qui i coa valda ná, nai i raine ya merilde san tuluva sanna, mal qui uas valda, nai i raine lello nanwenuva lenna.
Quiquie aiquen ua came le hya lasta quettaldannar, íre autalde sana coallo hya sana ostollo, á pale i asto taluldalto.
Násie quetin lenna: I Aure Namiéva nauva ambe *cólima Soromo nóren epe sana oston!
Yé! Mentan le ve mámar mici ñarmor. Etta na hande ve leucar ar *cámalóre ve cucuar.
Tira inde atannar, an antauvalte le olla *námocombin ar le-riptuvalte *yomencoaltassen.
Nauvalde tulyane epe nórecánor ar arani pa inye, ve *vettie tien ar i nórin.
Mal íre antalte le olla, áva na *tarastaine pa manen hya mana quetuvalde, an ya quetuvalde nauva len antana lúme entasse.
An i quetir uar rie lé, mal Atareldo Faire ye quete lénen.
Ente, háno antauva olla háno qualmenna, ar atar hínarya, ar híni ortuvar ontarultanta ar tyaruvar qualmetta.
Ar elde nauva tévine lo illi pa essenya, mal ye perpere i mettanna, isse nauva rehtana.
Íre roitalte le osto sinasse, uşa hyananna, an násie quetin lenna: Laume telyuvalde i rinde ostoron Israélo nó i Atanyondo tuluva.
Hildo ua or ye peanta se, hya mól or herurya.
Farya i hildon qui olis ve ye peanta se, ar i mól ve herurya. Qui queni estaner i *coantur Vélsevul, manen ita ambe estuvalte i queni coaryo sie?
Etta áva ruce tello. An ea munta tópina ya ua nauva apantana, ar munta nulda ya ua nauva sinwa.
Ya nyarin len i morniesse, queta i calasse, ar ya hlarilde hlussaina, ása care sinwa ingallon i coaron.
Ar áva ruce illon nahtar i hroa mal uar pole nahta i fea; ambe rongo ruca yello pole nancare fea ar hroa véla Ehennasse.
Ma uat filit atta vácine mittan urusteva? Ananta er mici tú ua lanta i talamenna pen Atareldo istya.
Mal yando ilye i findi careldasse nar nótine.
Etta áva ruce; nalde mirwe epe rimbe filici.
Etta ilquen ye *etequeta ni epe atani, yú inye *etequetuva epe Atarinya ye ea menelde;
mal aiquen ye láquete ni epe atani, yú inye laquetuva epe Atarinya ye ea menelde.
Áva sana i utúlien panien raine cemende; uan túle panien raine, mal macil.
An túlen tyarien şancier: nér ataryanna, ar selye amilleryanna, ar nessa veri i amillenna veruryo;
ar atano ñottor nar i queni coaryasse.
Qui queno atar hya amil ná sen melda epe ni, uas valda nin; ar qui queno yondo hya selye ná sen melda epe ni, uas valda nin.
Ar aiquen ye ua mapa tarwerya ar hilya ni ua valda nin.
Ye hire cuilerya, sen nauvas vanwa, mal i quén yeo cuile ole vanwa márienyan, hiruva sa.
Ye came lé came yú ni, ar ye came ni came yú ye ni-mentane.
Ye came Erutercáno pan náse Erutercáno, camuva Erutercáno *paityale, ar ye came faila nér pan náse faila, camuva faila nero *paityale.
Ar aiquen ye anta quenen mici pityar sine rie yulma ringa neno pan náse hildo – násie quetin lenna, *paityalerya laume nauva vanwa sen.
Apa Yésus telyane cane hildoryain yunque, etementanes te talo, peantien ar carien i menta sinwa ostoltassen.
Mal Yoháno, hlárala pa se mandosse, mentane vére hildoryainen
ar quente senna: “Ma elye ná ye tulumne, hya hópalme exen?”
Hanquentasse Yésus quente téna: “Á auta ar nyara Yohánon yar hláralde ar yar cénalde:
I laceníti cenir ata, ar i *lalevíti vantar, i halmahlaiwar nar poitaine ar i *hlárelórar hlárar, ar i qualinar nar ortaine, ar i penyar hlárar i evandilyon carna sinwa,
ar valima ná ye inyesse hire munta quérala se oa.
Íre té oanter, Yésus *yestane carpa i şanganna pa Yoháno: “Mana etelendelde mir i erume cenien? Lisce lévala i súrinen?
Lá, mana etelendelde cenien? Nér arwa mussi lannaron? Yé, i arwar mussi lannaron ear coassen araniva!
Mal mana castanen etelendelde? Cenie Erutercáno? Ná, quetin lenna, ar ambela Erutercáno!
Isse ná i quén pa ye ná técina: 'Yé, inye menta tercánonya epe lye, manwien mallelya epe lye.'
Násie quetin lenna: Mici i nar nóne lo nissi, *úquen ná túra epe Yoháno i *Sumbando, mal i ampitya menelo araniesse ná túra epe sé.
Mal ho réryar Yoháno i *Sumbando tenna sí, menelo aranie perpere orme, ar i carir orme mapar sa.
An ilye i Erutercánor ar i Şanye quenter apaceniltar tenna Yoháno,
ar qui merilde came sa, náse Elía ye tulumne.
Ye same hlaru, nai hlaruvas!
As mana sestuvan *nónare sina? Nalte ve híni hámala i mancanómessen, yámala i hyane hínannar:
'Tyaldelme i simpe len, mal ualde liltane; anelme yaimie, mal ualde pente inde nairesse.'
An Yoháno túle pen matie hya sucie, ar queni quetir: 'Náse haryana lo rauco!'
I Atanyondo túle matila ar sucila, ar queni quetir: 'Ela! Nér antana lapten ar sucien limpe, meldo *tungwemoron ar úcarindoron!' Ananta Sailie anaie tanana faila cardaryainen!”
Tá *yestanes naitya i ostor yasse i amarimbar túrie cardaryaron martaner, pan loitanelte hire inwis:
“Horro lyen, Corasin! Horro lyen, Vet-Saira! An au i túrie cardar yar amartier letse martaner mi Tír hya Síron, andanéya cestanelte inwis mi findilanne ar *litte!
Etta quetin lenna: Tíren ar Síronen i Aure Namiéva nauva *cólima epe ya nauvas lent!
Ar elye, Capernaum, ma nauval ortana menelenna? Undu Mandostonna tuluval, an au i túrie cardar yar martaner lyesse martaner Soromesse, tana osto tarne tenna aure sina.
Etta quetin lenna: Soromo nóren i Aure Namiéva nauva cólima epe ya nauvas len!”
Yana lúmesse Yésus ortane ómarya ar quente: “Laitan tye, Átar, Heru or menel cemenye, pan unurtiel nati sine i sailallon ar handallon ar ápantie tai vinimoin!
Ná, Átar, an carie sie náne ya fastane tye!
Ilye nati nar nin antane lo Atarinya, ar *úquen aqua ista i Yondo hequa i Atar; ente, *úquen aqua ista i Atar hequa i Yondo ar aiquen yen i Yondo mere apanta se.
Tula ninna, illi i mótar ar colir lunge cólar, ar antauvan lyen sére!
Á panya yaltanya indesse ar para nillo, an nanye moica ar nalda endasse, ar hiruvalde sére fealdain.
An yaltanya malda ná, ar cólanya *alalunga!”
Lúme yanasse Yésus lende ter i orirestar i *sendaresse. Hildoryar náner maite ar *yestaner lepta i cari oriva matien.
Ar cénala si i Farisar quenter senna: “Ela! Hildolyar carir ya ua lávina care i sendaresse!”
Quentes téna: “Ma ualde ehentanie pa ya Lavir carne íre isse ar i neri óse náner maite?
Lendes mir i coa Eruva ar mantelte i massar taniéva, yar úner lávina matta sen hya i náner óse, mal rie i *airimoin
Hya ualde ehentanie i Şanyesse i mi sendare i *airimor i cordasse racir i sendare ananta nar pen cáma?
Mal quetin lenna i sisse ea ya ná túra epe i corda.
Mal qui hanyanelde ya quetta sina tea: 'Merin óravie, ar lá *yanca,' ualde namne i nar pen cáma.
An i Atanyondo ná Heru i *sendareo!”
Apa oantes talo túles mir *yomencoalta.
Ar yé! enge tasse nér arwa hessa máo.
Quentes téna: “Man ná i nér mici te arwa erya mámo ye ua, qui máma sina lanta mir unque i *sendaresse, se-mapuva ar se-ortuva ettenna?
Mal manen ambe mirwa epe máma ná atan! Sie ná lávina care márie i *sendaresse!”
Tá quentes i nerenna: “Á rahta mályanen!” Ar rahtanes sanen, ar anes envinyanta ar náne ve i hyana má.
Mal i Farisar lender ettenna ar carner panoli pa manen nancarumneltes.
Istala ta, Yésus oante talo. Rimbali yú hilyaner se, ar nestanes illi mici te,
mal cannes te tulcave i ávalte carumne se sinwa,
carien anwa ya náne quétina ter Yesaia i *Erutercáno, ye quente:
“Yé! Núronya ye ilcílien, meldanya, pa ye feanya ná fastana. Panyuvan fairenya sesse, ar anan caruvas sinwa i nórennar.
Uas costuva hya yamuva, ar *úquen hlaruva ómarya i mallessen.
Rácina lisce uas ascatuva, ar *yúlala *urulanya uas *luhtyuva, tenna taluvas anan apairenna.
Ar esseryasse i nóri panyuvar estelelta!”
Tá tallelte senna raucoharyaina nér, laceníte ar úpa, ar nestanes se, tyárala i úpa nér carpa ar cene.
Ar ilye i şangar náner ara inte elmendanen, ar quentelte: “Cé nér sina ná i Lavirion!”
Hlárala ta, i Farisar quenter: “Nér sina ua et-hate i raucor hequa herunen i raucoron, Vélsevuv!”
Istala sanweltar, quentes téna: “Ilya aranie şanca insanna ná carna lusta, ar ilya osto ar coa şanca insanna ua taruva.
Ar qui Sátan et-hate Sátan, náse şanca insenna; tá manen aranierya taruva?
Ente, qui inye et-hate i raucor Vélsevuvnen, mannen yondoldar et-hatir te? Etta té nauvar námoldar.
Mal qui Eruo fairenen inye et-hate i raucor, Eruo aranie é utúlie lenna.
Hya manen aiquen pole lelya mir i coa polda nerwa ar mapa armaryar, qui uas minyave nute i polda nér? Ar tá piluvas i nati coaryasse.
Ye ua asinye ñottonya ná, ar ye ua comya asinye, vinta.
Etta quetin lenna: Ilya úcare ar naiquetie nauva apsénina Atanin, mal naiquetie i Fairenna ua nauva apsénina.
An aiquen ye quete quetta i Atanyondonna, sen nauvas apsénina, mal ye quete i Airefeanna, ta ua nauva apsénina sen, mi randa sina ar i tulila véla.
Cara i alda mára ar yáverya mára, hya cara i alda quelexa ar yáverya quelexa; an yáveryanen i alda sinwa ná.
Onnar leucoron, manen polilde quete máre nati, elde i nar ulce? An et i endo úvello i anto quete.
I mára atan tala máre nati mára harmaryallo, mal i olca nér tala olce nati olca harmaryallo.
Quetin lyenna i pa ilya quetie ya atani quetir ya ua aşea, hanquetuvalte Auresse Namiéva,
an vére quettalyainen nauval námina faila, ar vére quettalyainen nauval námina ulca!”
Tá queneli mici Farisar hanquenter senna: “*Peantar, merilme cene tanwa lyello!”
Hanquentasse quentes téna: “Olca ar úvoronda *nónare cestea tanwa, mal tanwa ua nauva antana san, hequa Yóna i Erutercáno tanwa.
An tambe Yóna náne i hoa lingwio mityasse auressen nelde ar lómissen nelde, síve i Atanyondo euva cemeno endasse auressen nelde ar lómissen nelde.
Neri Ninivello ortuvar i namiesse as *nónare sina ar namuvar sa ulca, an té hirner inwis íre Yóna carne mentarya sinwa, mal ye! sisse ea ya ná túra lá Yóna.
Hyarmeno tári ortuva i namiesse as *nónare sina ar namuva sa ulca, an túles cemeno mettallon hlarien Solomondo sailie, ar yé! sisse ea ya ná túra lá Solomon.
Íre úpoica faire ettule nerello, lahtas ter parce nómi cestala nóme séreva, mal uas hire.
Tá quetis: “Nanwenuvan coanyanna yallo léven”, ar túlala sanna, hiris i coa lusta, mal poica ar netyana.
Tá lelyas oa ar tala as inse hyane fairi otso, olce lá inse, ar apa menie minna marilte tasse, ar tana nero métima sóma ná faica lá i minya! Sie euva yú olca *nónare sinan!”
Íre en quétanes i şangannar, yé! amillerya ar hánoryar tarner i ettesse ar merner carpa óse.
Ar quén quente senna: “Yé! Amillelya ar hánolyar tárar i ettesse, mérala carpa aselye!”
Hanquentasse quentes yenna carampe senna: “Man ná amillinya, ar man nar hánonyar?”
Ar rahtala máryanen hildoryannar quentes: “Ela amillinya ar hánonyar!
Aiquen ye care i indóme Atarinyo ye ea menelde, isse ná hánonya ar néşanya ar amillinya!”
Aure yanasse Yésus, apa lendes i coallo, hámane ara i ear,
ar hoe şangali ócomner senna. Etta lendes mir lunte ar hamune, íre i quanda şanga tárane i fárasse.
Tá quentes téna rimbe natali sestielissen, quétala: “Yé! *Rerindo lende ettenna rerien.
Rerieryasse erdeli lantaner ara i malle, ar i aiwi túler ar manter tai.
Exeli lantaner i ondonna, yasse ualte sáme núra cemen, ar orontelte lintiénen, peniénen núra cemen.
Mal íre Anar oronte, anelte urtane, ar peniénen şundo hestanelte.
Exeli lantaner imíca i neceli, ar i neceli túler ama ar quorner te.
Mal exeli lantaner i mára cemenna ar carner yáve – mine, napánine lestali tuxa, exe, *enenquean, exe, *nelequean.
Ye same hlaru, nai sé hlára!”
Ar i hildor túler senna ar quenter: “Mana castalya carpien téna sestiessen?”
Hanquentasse quentes: “Len ná antana hanya menelo araniéno fóler, mal sane quenin uas antana.
An ye same, amba nauva sen antana, ar camuvas úve – mal ye ua same, yando ya samis nauva mapana sello.
Etta carpan téna sestiessen, pan yétala ualte cenuva, ar hlárala ualte tuntuva hya hanyuva,
ar pa té Yesaio apacen ná carna nanwa: 'Hlariénen hlaruvalde, mal ualde hanya, ar yétiénen yétuvalde, mal ualde cene.
An lie sino enda anaie carna tiuca, ar hlarultanten hlarilte ú hanquento, ar oholtielte hendultat; sie ualte oi cenuva hendultanten hya hlaruva hlarultanten ar hanyuva endaltanen ar hiruva inwis, nestienyan te!”
Mal valime nát henduldat, an yétatte, ar hlaruldat, an hlaritte.
An násie quetin lenna: Rimbe *Erutercánoli ar failali merner cene yar elde yétar, mal ualte cenner tai, ar hlare i nati yar elde hlarir, mal ualte hlasse tai.
Etta, elde hlara i sestie pa i *rerindo:
Íre aiquen hlare i quetta i araniéno mal ua hanya sa, i Olca tule ar rapa oa ya anaie rérina endaryasse; si ná ye náne rérina ara i malle.
Ar ye náne rérina i ondosse, ta ná ye hlare i quetta ar came sa mi alasse.
Ananta uas same şundo insesse mal lemya ter lúme, mal íre şangie hya roitie marta i quettanen, lantas oa mi imya lú.
Ar ye náne rérina imíca i neceli, si ná ye hlára i quetta, mal randa sino quáreler ar i laro úsahtie quorir i quetta, ar náse ú yáveo.
Ar ye náne rérina i mára cemende, si ná ye hlára i quetta ar hanya sa, ye é cole yáve ar care napánine lestali – quén sina, tuxa, exe, *enenquean, exe, *nelequean!”
Hyana sestie panyanes epe te, quétala: “Menelo aranie ná ve nér ye rende mára erde restaryasse.
Íre queni náner lorne, ñottorya túle ar rende úmirwe olvali imíca i ore, are oante.
Íre i lasse tuiane ar carne yáve, tá yando i úmirwe olvar túler ama.
Etta i *coanturo móli túler ar quenter senna: “Heru, ma ualye rende mára erde restalyasse? Mallo samis úmirwe olvali?”
Quentes téna: “Ñotto, nér, acárie nat sina!” Quentelte senna: “Ma meril i menuvalme comien te?”
Eques: “Vá; hya cé tucuvalde ama yando i ore as i úmirwe olvar.
Lava yúyon ale tenna i yávie, ar íre i yávie tuluva, quetuvan innar *cirihtuvar: Minyave á comya i úmirwe olvar ar nuta te mir loxi náven urtaine; tá mena comien i ore mir hauranya!”
Hyana sestie panyanes epe te, quétala: “Menelo aranie ná ve erde *sinapio, ya nér nampe ar empanne restaryasse.
Ta ná i ampitya ilye erdion, mal álienwa náse i anhoa i quearon ar anaie carna alda, ar sie menelo aiwi tulir ar hirir sére olvaryassen!”
Hyana sestie quentes téna: “Menelo aranie ná ve *pulmaxe, ya nís nampe ar nurtane hoe lestassen nelde moleva, tenna ilqua náne ortaina!”
Ilye sine nati Yésus quente i şangannar sestiessen, ar ú sestiéno uas carampe téna,
carien nanwa ya náne quétina ter i Erutercáno ye quente. “Pantuvan antonya sestielissen; caruvan sinwe natali nurtane i tulciello!”
Tá, apa mentie i şangar oa, lendes mir i coa. Ar hildoryar túler senna ar quenter: “Lava men hanya i sestie pa i úmirwe olvar i restasse!”
Hanquentasse eques: “Ye rere i mára erde ná i Atanyondo;
i resta ná i mar, ar i mára erde, té nar i araniéno yondor, mal i úmirwe olvar nar i Olco yondor.
I ñotto ye rende te ná i Arauco. I yávie ná i rando tyel, i *cirihtor nar vali.
Etta, ve íre i úmirwe olvar nar comyaine ar urtaine nárenen, sie euva i rando tyelde:
I Atanyondo mentuva valaryar, ar comyuvalte et aranieryallo ilye nati yar tyarir queni lanta ar i queni i racir i şanye,
ar hatuvaltet mir i urna náreva. Tasse euvar nieltar ar mulielta nelciva.
Tá i failar caltuvar ve Anar, Atarelto araniesse. Ye same hlaru, nai hláras!
Menelo aranie ná ve harma nurtana i restasse, ya nér hirne ar nurtane, ar alasseryanen autas ar vace i nati yar samis, ar mancas insen resta tana.
Ata menelo aranie ná ve macar cestala máre marillar:
Apa hirie er marilla ya náne ammirwa, oantes ar vance ilqua ya sámes ar mancane sa insen.
Ata menelo aranie ná ve rembe, hátina mir i ear ar comyala lingwili ilya nostaleo.
Íre anes quanta, tunceltes ama i fáranna ar hamuner ar comyaner i márar mir veneli, mal i úmirwar hantelte oa.
Sie euva mí rando tyel: I vali etelelyuvar ar hyamuvar i olcar i failallon,
ar hatuvaltet mir i uruite urna. Tasse euvar nieltar ar mulielta nelciva.
Ma hanyanelde ilye sine nati?” Quentelte senna: “Ná!”
Tá quentes téna: “Etta ilya parmangolmo, peantana pa menelo aranie, ná ve nér, *coantur, ye tala et harmaryallo vinyali ar yárali!”
Ar tá, apa Yésus telyane sestier sine, lahtanes nóre talo.
Ar íre túles vehteryanna, peantanes tien *yomencoaltasse. Sie anelte quátine elmendanen ar quenter: “Mallo nér sina camne sailie sina ar i taure cardar sine?
Ma si ua i tautamo yondo? Ma amillerya ua estaina María, ar hánoryar Yácov ar Yósef ar Símon ar Yúra?
Ar néşaryar, ma ualte illi aselve? Masse, tá, nér sina camne ilye sine nati?”
Etta anelte rúşie senna. Mal Yésus quente téna: “Erutercáno ua pene laitie hequa vehteryasse ar coaryasse!”
Ar uas carne tasse rimbe taure cardali, pan pennelte savie.
Lúme yanasse Herol i *Canastatur hlasse i nyarna pa Yésus,
ar quentes núroryannar: “Si ná Yoháno i *Sumbando. Anaies ortana qualinallor, ar etta i taure cardar martar sénen!”
An Herol yá nampe Yohannes ar nunte se ar se-panyane mandosse, castanen Heroliasso, Filip hánoryo veri.
An Yohannes quente senna: “Ualye lerta same se!”
Mal ómu mernes nahta se, runces i şangallo, an sannes pa se ve Erutercáno.
Man íre anelte merye Herolo *nónaresse, Heroliasso selye liltane sasse ar antane Herolen alasse ta hoa
i quentes hén vanda: Antauvanes hén *aiqua ya arcumnes.
Tá, peantaina lo amillerya, hé quente: “Ánin anta sisse venesse Yoháno i *Sumbando cas!”
Ómu ta tyarne sen naire, i aran canne i mo antauvane sa i venden,
ar mentanes ar hocirne Yoháno cas i mandosse.
Ar carya náne talana venesse ar antana i venden, ar hé talle sa amilleryanna.
Ar hildoryar túler ar namper oa i loico ar taller se sapsanna, ar túlelte ar nyarner Yésun.
Ire hlasses sa, Yésus lende oa talo luntesse, eressea nómenna, náven erinqua; mal i şangar hlasser sa ar hilyaner se talunen i ostollon.
Ar íre túles cennes hoa şanga, ar endarya etelende téna, ar nestanes hlaiwaltar.
Mal íre şinye lantane, hildoryar túler senna ar quenter: “I nóme eressea ná, ar i lúme ná telwa yando sí; á menta i şangar oa, menieltan mir i mastor mancien inten matta!”
Mal Yésus quente téna: “Ualte same maure autiéva. Elde áten anta matta matien!”
Quentelte senna: “Samilme munta sisse hequa massar lempe ar lingwe atta!”
Eques: “Á tala tai sir ninna!”
Tá cannes i şangar i caitumnelte undu i salquenna, ar nampes i massar lempe ar lingwe atta, ar yétala ama menelenna quentes aistie. Ar apa racie i massar etsanteset i hildoin, ar i hildor i şangain.
Ar illi manter ar náner quátine, ar ortanelte i lemyala rantar: quante *vircolcar yunque.
Mal i manter náner *os neri húmi lempe, hequa i nissi ar híni.
Tá, lintiénen, mauyanes hildoryar mene mir i lunte ar lelya epe se i hyana fáranna, íre mentanes i şangar oa.
Ar apa mentanes i şangar oa, lendes ama mir i oron eressea nómenna, hyamien. Íre şinye túle, anes erinqua tasse.
Sí i lunte náne rangar rimbe tuxali norello, ñwalyaina i falmainen, an sámelte i súre cendeleltanna.
Mal mí cantea tirisse i lómio túles téna, vantala i earesse.
Íre cenneltes, anelte *tarastaine, quétala: “Ta auşa ná!” Ar caureltanen yámelte.
Mal mi imya lú Yésus quente téna: “Huore! Inye ná! Áva ruce!”
Péter hanquente senna: “Heru, qui ta elye ná, ánin cane i tuluvan lyenna olla i neni!”
Eques: “Tula!”Tá Péter, autala i luntello, vantane olla i neni ar túle Yésunna.
Mal íre cennes i raumo, anes ruhtaina, ar íre i nén *yestane *hlucitas, yámes: “Heru, áni rehta!”
Mi imya lú Yésus rahtane máryanen ar nampe se, ar quentes senna: “Nér pitya saviéno, manen ná i savielya náne iltanca?”
Ar apa lendette mir i lunte, i raumo pustane.
Tá i queni i luntesse luhtaner epe se, quétala: “Nanwave nalye Eruo Yondo!”
Ar lahtanelte ar túler norenna mi Ennesaret.
Íre i neri tana nómesse cenner man anes, mentanelte mir i quanda pelila nórie, ar queni taller senna illi i náner hlaiwe.
Ar arcanelte senna i rie appumnelte collaryo lane, ar illi i appaner sa camner quanta mále.
Tá túler Yésunna Yerúsalemello Farisali ar parmangolmoli, quétala:
“Manen ná i hildolyar racir i haime cámina i amyárallon? An ualte sove máltat nó matilte matta!”
Hanquentasse quentes téna: “Manen ná i elde racir Eruo axan haimeldanen?
An Eru quente: 'Á anta alcar atarelyan yo amillelyan,' ar: 'Aiquen ye quete ulco pa atar hya amil, é qualuva.'
Mal elde quetir: 'Aiquen ye quete ataryanna hya amilleryanna: '*Aiqua aşea ya camumnetye nillo, ná anna Erun' –
mauya sen i aqua uas anta alcar ataryan hya amilleryan.
*Imnatyandoli, Yesaia quente mai ve Erutercáno pa le, íre quentes:
Lie sina nin-anta alcar péltanten, mal endalta ná haira nillo.
Cumnave *tyerilte ni, peantala Atanion peantier!”
Ar tultanes i şanga ar quente téna: “Á lasta ar hanya!
Lá ya tule minna ter queno anto vahta atan, mal ya ettule antoryallo ná ya vahta atan!”
Tá i hildor túler ar quenter senna: “Ma istal in íre i Farisar hlasser i quetta, anelte rúşie?”
Hanquentasse quentes: “Ilya olva ye meneldea Atarinya ua empanne, nauva túcina ama as şunduryar.
Lava tien náve! Laceníti *tulyandoli nalte. Mal qui laceníte quén tulya laceníte quén, yúyo lantuvat mir sapta!”
Hanquentasse Péter quente senna: “Ámen anta tercen pa i sestie!”
Tá quentes: “Ma yú elde loitar hanya?
Ma ualde ista in ilqua ya tule minna ter i anto, mene mir i hirdi ar auta mir i *aucelie?
Mal yar ettulir i antollo ettulir i endallo, ar tai vahtar atan.
An et i endallo tulir olce sanwar, nahtier, vestaleracie, *úpuhtale, pilwi, húrala *vettier, naiquetier.
Tai nar yar vahtar atan, mal qui atan mate matta úsóvine mánten, ta ua vahta se!”
Autala talo, Yésus sí lende oa mir i ménar Tíro ar Sírono.
Ar yé! Canáanya nís ente nóriellon ettúle ar yáme, quétala: “Órava nisse, Lavirion! Selyenya ná ulcave haryana lo rauco!”
Mal isse ua hanquente senna quetta. Ar hildoryar túler senna ar arcaner senna, quétala: “Áse menta oa, an yámas ca vi!”
Hanquentasse eques: “Únen mentana aiquenna hequa i vanwe mámannar Israélo coallo!”
Íre i nís túle, luhtanes epe se, quétala: “Heru, áni manya!”
Hanquentasse quentes: “Ua vanima mapie i hínion massa ar hatie sa i huonnar!”
Hé quente: “Ná, Heru, mal i huor matir i mier yar lantar sarnollo herultaiva!”
Tá Yésus hanquente senna: “A nís, savielya túra ná; nai martuva lyen ve asáviel!” Ar i selye náne nestana lúme yanallo.
Lahtala nórie talo, Yésus túre hare i Earenna Alileo, ar apa menie ama mir oron hamunes tasse.
Tá hoe şangali túler senna, túlula queneli i náner *úlévime, tapte, lalevíte, úpe, ar nimpe rimbe hyane lélissen,
ar sie i şanga náne quanta elmendanen íre cennelte i úpar quéta ar i *úlévimar vantea ar i lalevíti céna, ar antanelte alcar Israélo Ainon.
Mal Yésus tultane hildoryar insenna ar quente: “Endanya etelelya i şanganna, an sí elémielte asinye auressen nelde, ar samilte munta matien; ar uan mere menta te oa ú matiéno. Cé nauvalte acca lumbe i mallesse!”
Mal hildoryar quenter senna: “Mallo, mi eressea nóme sina, ñetuvalme fárea massa şangan ya ta hoa ná?”
Ar Yésus quente téna: “Mana nóte massaron samilde?” Quentelte: “Otso, ar nótime lingwili!”
Tá, apa cannes i şangan i caitumnelte i talamesse,
nampes i matsar otso ar, apa antave hantale, rance tai ar etsante tai i hildoin, ar i hildor i şangain.
Ar illi manter ar náner quátine, ar comyanelte i lemyala rantar: quante *vircolcar otso.
Ananta i manter náner neri húmi canta, hequa i nissi ar híni.
Ar apa mentave i şangar oa, lendes mir i lunte ar túle i ménannar Maharáno.
Ar i Farisar ar Sandúcear túler senna, ar tyastien se arcanelte sello i tanumnes tien tanwa menello.
Hanquentasse quentes téna:
“Ulca ar úvoronda *nónare cestea tanwa, mal tanwa ua nauva antaina san, hequa Yóno tanwa!” Ar oantes tello ar lende oa.
Sí i hildor lahtaner i hyana fáranna, ar ualte *renne tala massa.
Yésus quente téna: “Cima ar ettira pa i *pulmaxe Farisaron ar Sandúcearon!”
Carampelte pa si mici inte, quétala: “Ualve talle massar!”
Istala ta, Yésus quente: “Manen ná i carpalde mici inde pa penie massar?
Ma en ualde hanya, ar ualde enyale i massar lempe i húmin lempe, ar i nóte *vircolcaron yar ortanelde?
Hya i massar otso i húmin canta, ar i nóte *vircolcaron yar ortanelde?
Manen, tá, ualde hanya i uan quente lenna pa massar? Mal *ettira pa i *pulmaxe Farisaron ar Sandúcearon!”
Tá hanyanelte i quentes téna i *ettirumnelte, lá pa i *pulmaxe massaron, mal pa i peantie Farisaron ar Sandúcearon.
Apa Yésus túle i ménannar Cesaréa Filippio, maquentes hildoryannar: “Man queni quetir in i Atanyondo ná?”
Quentelte: “Queneli quetir: Yohannes i *Sumbando, exeli: Elía, exeli: Yeremía hya quén i Erutercánoron!”
Quentes téna: “Mal elde, man quetilde i nanye?”
Hanquentasse Péter quente senna: “Elye i Hristo ná, i coirea Aino yondo!”
Yésus hanquente senna: “Valima nalye, Símon Var-Yóna, an hráve yo serce uat apantane sa lyen, mal meneldea Atarinya.
Ar quetin lyenna: Elye Péter, ar ondo sinasse carastuvan ocombenya, ar Mandosto andor uar turúva sa.
Antauvan lyen menelo araniéno *latili, ar ilqua ya nutuvalde cemende nauva nútina menelde, ar ilqua ya lehtal cemende nauva lehtaina menelde!”
Tá tulcave cannes i hildoin i ávalte quetumne aiquenna in anes i Hristo.
Lúme yanallo Yésus *yestane tana hildoryain i mauyane sen lelya Yerúsalemenna ar perpere rimbe natali ho i *amyárar ar parmangolmor, ar náve nahtana, ar i nelya auresse náve ortana.
Mal Péter talle se arana ar *yestane carpa tulcave senna, quétala: “Na moica imlen, Heru! Sie laume euva elyen!”
Mal sé quernexe ar quente Péterenna: “Heca ca ni, Sátan! Meril tyare ni lanta, an sanal, lá Eruo sanwi, mal tai atanion!”
Tá Yésus quente hildoryannar: “Qui aiquen mere tule apa ni, nai laluvas inse ar ortauva tarwerya ar hilyuva ni.
An aiquen ye mere rehta cuilerya, sen nauvas vanwa; mal aiquen yeo cuile ná vanwa márienyan, hiruva sa.
An qui atan came i quanda mar, mal cuilerya ná vanwa, manen ta ná sen aşea?
An i Atanyondo tuluva Ataryo alcaresse as valaryar, ar tá paityuvas ilquenen ve lengierya.
Násie quetin lenna in ear queneli i tárar sisse i laume tyavuvar qualme nó cenilte i Atanyondo túla aranieryasse!”
Auri enque epeta Yésus talle Péter ar Yácov ar Yohannes hánorya, ar tulyane te ama mir tára oron, té erinque.
Ar anes vistana epe te, ar cendelerya caltane ve Anar, ar lanneryar náner alcarinque ve cala.
Ar yé! Móses ar Elía tanner intu tien, quétala óse.
Mal Péter carampe ar quente Yésunna: “Heru, vanima ná i nalme sisse! Qui meril, ortauvan sisse *lancoar nelde: er lyen ar er Mósen ar er Elían!”
Íre i quettar en náner antoryasse, yé! calima fanya teltane te, ar enge óma et i fanyallo ya quente: “Si yondonya ná, i melda, pa ye nanye fastaina; á lasta senna!”
Hlárala si, i hildor lantaner cendeleltasse ar náner ita caurie.
Tá Yésus túle hare téna, ar appala te quentes: “Á orta, ar áva ruce!”
Íre ortanelte hendultat, cennelte *úquen hequa Yésus immo.
Tieltasse undu i orontello, Yésus canne tien, quétala: “Áva nyare aiquenen pa i cenie nó i Atanyondo anaie ortana qualinillon!”
Mal i hildor maquenter: “Tá manen ná in i parmangolmor quetir in Elía maurenen tuluva minyave?”
Hanquentasse eques: “Elía é tuluva ar envinyatauva ilye nati.
Mal quetin lyenna i Elía utúlie yando sí, ar ualte hanyane man anes, mal carner sen ve mernelte. Mi imya lé i Atanyondo perperuva máltanten.”
Tá i hildor hanyaner i quentes téna pa Yohannes i *Sumbando.
Ar íre túlelte i şanganna, nér túle senna, lantala occaryanta epe se
ar quétala: “Heru, órava yondonyasse, an náse *ránahlaiwa ar nimpa, an lantas rimbave mir i náre ar rimbave mir i nén,
ar tallenyes hildolyannar, mal ualte polde nesta se!”
Hanquentasse Yésus quente: “Úvoronda ar rícina *nónare, manen andave mauya nin lemya aselde? Manen andave mauya nin cole le? Áse tala ninna!”
Tá Yésus carampe tulcave senna, ar i rauco ettúle sello, ar i seldo náne nestana lúme yanallo.
Epeta i hildor túler vérave Yésunna ar quenter: “Manen ná in elme uar polde et-hatitas?”
Quentes téna: “Pitya savieldanen. An násie quetin lenna: Qui samilde savie ve erde *sinapio, quetuvalde oron sinanna: Leva silo tar, ar levuvas, ar munta nauva úcárima len.
Íre anelte comyane Alileasse Yésus quente téna: “I Atanyondo nauva *vartaina mannar atanion,
ar nahtuvaltes, ar i nelya auresse nauvas ortana!” Etta sámelte lunga naire.
Íre túlelte Capernaumenna, i neri comyala i racma atta túler Péterenna ar quente: “Ma *peantarelda ua paitya i *racma atta?”
Quentes: “Caris.” Mal íre lendes mir i coa, nó polles quete, Yésus quente: “Mana sanal, Símon? Ho man cemeno arani camir *tungwe hya *rantie? Yondoltallon hya etteleallon?”
Íre hé quente, “I etteleallon,” Yésus quente senna: “Tá i yondor é nar lére.
Mal in uangwe caruva te rúşie, alye lelya i earenna, hata ampa, ar á mapa i minya lingwe tulila ama – ar íre latyal antorya, hiruval stater. Á mapa ta ar áten anta sa nin ar lyen!”
Lúme yanasse i hildor túler hare Yésunna ar quenter: “Man nanwave ná i antúra menelo araniesse?”
Ar apa yaldes pitya hína insenna, se-tyarnes tare i endesse
ar quente: “Násie quetin lenna: Qui ualde hire inwis ar nar ve pitye híni, laume tuluvalde mir menelo aranie.
Etta, aiquen ye care inse nalda ve hína sina, isse ná i antúra menelo araniesse;
ar aiquen ye came erya taite hína essenyanen, came ní.
Mal aiquen ye tyare lanta er mici sine i savir nisse – sen nauva ambe aşea qui *mulondo náne panyana *os yahtya, ar anes quórina mí núra ear.
Horro i marden úsahtieryain! An i úsahtier maurenen tuluvar, man horro i atanen yenen i úsahtie tule!
Mal qui málya hya talelya tyára lantelya, ása aucire ar hata sa lyello! Mene *lalevíte hya tapta mir coivie ná lyen arya epe náve hátina arwa má atto hya tál atto mir i oira náre!
Ar qui hendelya tyára lantelya, ása narca ettenna ar hata sa lyello! Mene mir coivie arwa erya hendo ná lyen arya epe náve hátina arwa hen atto mir i uruite Ehenna!
Cena in ualde nattire aiquen mici pityar sine! An quetin lenna i valaltar menelde illume yétar meneldea Atarinyo cendele.
An i Atanyondo utúlie rehtien ya náne vanwa.
Mana sanalde? Qui nér same mámar tuxa, ar er mici te ranya, ma uas auta ho i nerte *neterquean i orontissen, ar menis cestien i ranyala máma?
Ar qui é hirises – násie quétan lenna, pa máma tana samis alasse túra epe ya samis pa i nerte *neterquean i uar aránie.
Mi imya lé, meneldea Atarinyo indóme ua in erya mici pityar sine nauva vanwa.
Ente, qui hánolya úcare, mena ar áse naitya imbe lyé ar sé erinque. Qui lastas lyenna, eñétiel hánolya.
Mal qui uas lasta, á tala aselye er hya atta ambe, an i antonen astarmor atto hya neldeo ilya natto nauva tulcana.
Qui uas lasta túna, queta i ocombenna. Qui loitas lasta yú i ocombenna, nauvas lyen ve quén i nórion ar ve *tungwemo.
Násie quetin lenna, ilqua ya nutilde cemende nauva nútina menelde, ar ilqua ya lehtalde cemende nauva lehtana menelde.
Ata násie quetin lenna: Qui atta mici le nát er sámo cemende arcien nat, ta martuva tunt meneldea Atarinyanen.
An yasse atta hya nelde nar comyane mi essenya, tasse ean endaltasse.
Tá Péter túle ar quente senna: “Heru, mana nóte i lúron yassen hánonya lerta úcare ninna ar apsenuvan sen? Tenna lúr otso?”
Yésus quente senna: “Quétan lyenna, lá tenna lúr otso, mal tenna *otoquean lúr otso!
Etta menelo aranie ná ve nér, aran, ye merne same onótie as móliryar.
Íre *yestanes i onótie, qúen arwa rohto talention húmi quean náne tulyana senna.
Pan uas sáme fáre paitien sen, herurya canne i mo vacumne se, as verirya ar hínaryar, ar ilqua ya sámes, paitien i rohta.
Etta i mól lantane undu ar luhtane epe se, quétala: “Sama cóle nin, ar paityuvan nan ilqua lyen!”
Ápaina endaryanna, sana mólo heru lehtane se ar apsenne rohtarya.
Mal sana mól etelende ar hirne quén i hyane mólion, ye sáme sen rohta lenárion tuxa. Ar mápala se, *yestanes quore se, quétala: “Ánin paitya rohtalya!”
Tá i hyana mól lantane undu ar arcane senna, quétala: “Sama cóle nin, ar paityuvan lyen!”
Mal uas merne, ono lendes ar hante hé mir mando tenna hé paityumne rohtarya.
Etta, íre i hyane móli cenner ya martane, sámelte tumna naire, ar lendelte ar nyarner herultan ilqua ya martanelyane.
Tá herurya tultane se ar quente senna: “Olca mól, quanda rohtalya apsennen lyen, íre arcanel ninna!
Lau tá carumnel mai qui yú lyé oravumne i hyana mólesse ye enge aselye, ve inye antane lyen oravie?”
Ar nála rúşea, herurya antane se ollo i *mandonturin, tenna paityumnes quanda rohtarya.
Mí imya lé meneldea Atarinya yú caruva len qui ualde ilquen apsene hánoryan endaldallo!”
Ar apa Yésus telyane ilye quettar sine oantes Alileallo ar túler i ménannar Yúreo, Yordan pella.
Ar hoe şangali hilyaner se, ar te-nestanes tasse.
Ar Farisali túler senna tyastien se, quétala: “Ma lerta nér lehta inse veriryallo ilya castanen?”
Hanquentasse eques: “Ma ualde hentane i ye ontane tu, i yestallo carne tu hanu yo ní
ar quente: Sina castanen nér autuva ataryallo ar amilleryallo ar himyuva verirya, ar nauvatte hráve er – ?
Sie uatte ambe atta, mal hráve er. Etta, ya Eru apánie nu er yalta, áva lave atanen hyare!”
Quentelte senna: “Tá manen ná i Móses canne i mo antauva sen tecie *aumentaléva ar panyuva se oa?”
Quentes téna: “Hranga endaldanen Móses láve i lertalde lehta inde verildallon, mal lá enge sie i yestallo.
Ar quetin lenna: Aiquen ye lehta immo veriryallo hequa *úpuhtiénen, ar verya exenna, race vestale!”
I hildor quenter senna: “Qui ea sie pa veru yo veri, vestale ua aşea!”
Quentes téna: “Illi uar pole came i quetie, mal rie i samir i anna.
An ear *vielórali i náner nóne sie amillelto mónallo, ar ear *vielórali i náner cárine *vielóre lo atani, ar ear *vielórali i acárier inte *vielóre menelo araniéno márien. Lava yen pole camitas, came sa!”
Tá hínali náner talaine senna, panieryan máryat tesse ar hyamieryan tien, mal i hildor naityaner te.
Mal Yésus quente: “Lava i hínin tule, ar ávate pusta tuliello ninna, an menelo aranie taitin ná!”
Ar panyanes máryat tesse ar oante talo.
Ar yé! quén túle senna ar quente: “*Peantar, mana i márie ya mauya nin care, camien oira coivie?”
Quentes senna: “Manen ná i maquétal ní pa márie? Ea er ye mára ná. Mal qui meril mene mir coivie, á himya i axani!”
Quentes senna: “Mana nalte?” Yésus hanquente: “Ávalye nahta, ávalye pile, ávalye quete hurila *vettie,
alye anta alcar atarelyan ar amillelyan, ar mela armarolya ve imle!”
I nessa nér quente senna: “Ihímien ilye axani sine; mana en penin?”
Yésus quente senna: “Qui meril náve ilvana, mena ar vaca armalyar ar átai anta i penyain, ar camuval harma menelde; tá tula ar áni hilya!”
Íre i nessa nér hlasse ta, oantes nairesse, an sámes rimbe armali.
Mal Yésus quente hildoryannar: “Násie quétan lenna in urda nauva lára quenen tule mir menelo aranie.
Ata quétan lenna: Ulumpen menie ter nelmo assa ná ancárima epe tulie mir Eruo aranie lára quenen!”
Íre i hildor hlasser ta, anelte quátine túra elmendanen, quétala: “Man pole náve rehtana?”
Yétala te, Yésus quente: “Atanin nas úcárima, mal Erun ilye nati nar cárime!”
Tá Péter carampe, quétala: “Yé, ehehtielme ilqua ar ihílier lyé. Mana, nanwave, euva men?”
Yésus quente téna: “Násie quetin lenna: I *enontiesse, íre i Atanyondo hamúva alcarinqua mahalmaryanna, elde i ihílier ni yú hamuvar mahalassen yunque, namila Israélo nossi yunque.
Ar ilquen ye oantie ho coar hya hánor hya néşar hya restar castanen essenyo, camuva rimbe lúli amba ar nauva aryon oira coiviéno.
Mal rimbali i nar minye nauvar métime, ar i métimar, minye.
An menelo aranie ná ve nér, *coantur, ye etelende arinyave hirien *molindoli in poldes paitya molieltan tarwaryasse liantassion.
Apa carie vére as i *molindor pa er lenár i auren, mentanes te mir tarwarya.
Etelendes yú *os i neldea lúme ar cenne exeli tára pen molie mí mancalenóme,
ar quentes yú téna: “Yú elde, á lelya mir i tarwa, ar antauvan len ya faila ná!”
Ar lendelte. Ata lendes ettenna *os i enquea ar i nertea lúme, ar carnes i imya nat.
Mal *os i minquea lúme lendes ettenna ar hirne exeli tára, ar quentes téna: “Manen ná i táralde sisse i quanda aure pen molie?”
Quentelte senna: “Pan *úquen acárie vére aselme pa molie!” Quentes téna: “Mena i tarwanna liantassion, yú elde!”
Íre şinye túle, i *tarwantur quente *ortirnoryanna: Yala i *molindor ar á paitya tien *paityalelta, i métimallon i minyannar!
Íre i minquea lúmeo neri túler, ilquen mici te camne lenár.
Etta, íre i minyar túler, intyanelte i camumnelte amba, mal ilquen mici té yú camne er lenár.
Apa camie sa nurrunelte i *tarwanturenna
ar quenter: “Métimar sine omólier erya lúme, ananta antal tien i imya ya antal men i ocólier i aureo cólo ar i úre!”
Mal hanquentasse quenenna mici te quentes: “Málo, acárien munta úfaila lyen! Şáquentel íre quenten in antauvanen lyen er lenár, lá?
Á mapa ya *lyenya ná ar mena! Merin anta métima sinan i imya ya antan lyen.
Ma uan lerta care ya merin vére natinyain? Hya ma hendelya olca ná pan inye mane ná?
Sie i métimar nauvar minye, ar i minyar métime!”
Sí, nó lendes ama Yerúsalemenna, Yésus tulyane i hildor yunque véra nómenna ar quente téna:
“Yé! Lelyuvalme ama Yerúsalemenna, ar i Atanyondo nauva antana olla i hére *airimoin ar i parmangolmoin, ar namuvaltes valda qualmeo
ar antauvaltes olla i nórin, lengieltan senna yaiwenen ar riptieltan se ar tarwestieltan se, ar i neldea auresse nauvas ortana!”
Tá Severaio yondoron amil túle senna as yondoryar, luhtala epe se ar arcala nat sello.
Yésus quente senna: “Mana meril?” Isse quente senna: “Queta i yondonya atta sine haruvat, quén ara formalya ar quén ara hyarmalya, aranielyasse!”
Hanquentasse Yésus quente: “Uaste ista ya arcaste! Ma lét polit suce i yulma ya inye sucuva?” Quentette senna: “Polimme!”
Quentes túna: “É sucuvaste yulmanya, mal harie ara formanya ar hyarmanya ua anna inye lerta anta; ta ná in nar manwane lo Atarinya!”
Íre i exi quean hlasser pa si, anelte rúşie i háno attanna.
Mal Yésus yalde te insenna ar quente: “Istalde i nórion turi turir tai, ar tauraltar samir túre or te.
Mal ua ea sie mici elde, ono ye mere náve túra mici lé, sen mauya náve núrolda,
ar yen mere náve minya mici le, sen mauya náve mólelda
– ve yú i Atanyondo ua túle náven *veuyaina, mal *vevien ar antaven cuilerya ve *nanwere quaptalesse rimbaiva!”
Sí, íre lendelte et Yericollo hoa şanga hilyane se.
Ar yé! laceníte nér atta hámanet ara i malle. Íre hlassette i Yésus náne i quén lahtala, yámette quétala: “Heru, órava metse, Lavirion!”
Mal i şanga carampe túna tulcave, návettan quilde; ananta yámette i ambe: “Heru, órava metse, Lavirion!”
Ar Yésus pustane, yalde tu ar quente: “Mana meriste i caruvan lent?”
Quentette senna: “Heru, nai hendummat nauvat pantaine!”
Ápaina endaryanna, Yésus appane henduttat – ar mi yana lú ence tunt cene, ar hilyanettes.
Íre túlelte hare Yerúsalemenna ar túler Vet-Fahenna i Orontenna *Milloaldaron, tá Yésus etementane hildo atta,
quétala túna: “Mena mir i osto ya caita epe tu, ar lintave hiruvaste *pellope nútina, ar *rocolle óse; átu lehta ar átu tulya ninna.
Ar qui quén quete *aiqua lenta, queta: I Heru same maure túva. Tá, mi imya lú, mentauvas tu!”
Nat sina martane carien anwa i quetta ya i Erutercáno quente:
“Nyara Siono selyen: Yé, aranelya túla lyenna, nalda ar hámala *pellopesse, ar *rocollesse, onna celvo cólova!”
Sie i hildor lender ar carner ve Yésus canne tun.
Ar tulunette i *pellope ar *rocollerya, ar panyanette tusse collattar, ar hamnes tusse.
I ambe hoa ranta i şango pantaner collaltar i mallesse, íre exeli hocirner olvali i aldallon ar pantaner tai i mallesse.
Ar i şangar yar lender epe se ar i hilyaner yámer: “Hosanna yondon Laviro! Aistana ná ye túla i Heruo essenen! Hosanna i tarmenissen!”
Ar íre lendes mir Yerúsalem, i quanda osto náne valtana, quétala: “Man ná quen sina?”
Mal i şangar hanquenter: “Si ná Yésus i Erutercáno, Nasaretello Alileasse!”
Ar Yésus lende mir i corda, ar et-hantes ilye i vancer ar *homancaner i cordasse, ar sarnor i *telpevistandoiva nuquernes, ar i hammar iva vancer i cucuar.
Ar quentes téna: “Ná técina: Coanya nauva estaina coa hyamiéva – mal elde acárier sa rotto piluiva!”
Ente, laceníti ar *lalevíti queneli túler senna i cordasse, ar nestanes te.
Íre i hére *airimor ar i parmangolmor cenner i elmendar yar carnes ar i seldor yámala i cordasse: “Hosianna Yondon Laviro!”, anelte rúşie
ar quenter senna: “Ma hláral ya té quétar?” Yésus quente téna: “Ná! Ma ualde oi hentane si: Et antollo lapsion ar vinimoron amanwiel laitale – ?”
Ar oantes tello ar lende et i ostollo Vetanianna ar lemne tasse ter i lóme.
Nanwénala i cordanna arinyave, anes maita.
Ar cennes *relyávalda ara i malle ar lende senna, mal hirnes munta sesse hequa rie lasseli, ar quentes senna: “Nai *úquen oi camuva yáve lyello!” Ar mi imya lú i *relyávalda hestane.
Mal íre i hildor cenner ta, anelte elmendasse, quétala: “Manen i *relyávalda hestane mi erya lú?”
Hanquentasse Yésus quente téna: “Násie quetin lenna: Qui samilde savie ar uar iltance, ualde rie caruva ya inye carne i *relyávaldan, mal yú qui quetilde oron sinanna: Na ortana ar hátina mir i ear!, ta martuva.
Ar ilye i nati yar arcalde hyamiesse, arwe saviéno, camuvalde!”
Sí apa lendes mir i corda, i hére *airimor ar i amyárar imíca i lie túler senna íre peantanes i şanga ar quenter: “Mana i hére yanen caril nati sine? Ar man antane lyen hére sina?”
Hanquentesse Yésus quente téna: “Yú inye maquetuva lenna pa er nat. Qui nyarilde nin, yú inye nyaruva len pa i hére yanen carin nati sine.
Yoháno sumbie, mallo anes? Menello hya atanillon?” Mal carnelte úvie mici inte, quétala: “Qui quetilve: Menello, quetuvas venna: Tá manen ná i ualde sáve sesse?
Mal qui quetilve: Atanillon, rucuvalve i şangallo, an illi mici te savir i Yoháno náne Erutercáno!”
Ar hanquentasse quentelte Yésunna: “Ualme ista!” Yú isse quente téna: “Tá inye véla ua nyare len pa i hére yanen carin nati sine.
Mal mana sanalde? Enge nér as hína atta. Lendes i minyanna ar quente: Hína, síra mola i tarwasse liantassion!
Hé quente: Ménan, heru! – mal uas lende.
Lelyala i exenna, quentes i imya nat. Mal hanquentasse hé quente: Uan mere. Ono epeta vistanes sámarya ar lende.
Man imíca i atta carne i ataro nirme?” Quentelte: “I attea!” Yésus quente téna: “Násie quetin lenna: I *tungwemor ar i *imbacindi lelyar epe lé mir Eruo aranie.
An Yoháno túle lenna mí malle failiéva, mal ualde sáver sesse. Mal i *tungwemor ar i *imbacindi sáver sesse, ar elde, ómu cennelde ta, uar epeta vistane sámalda savien sesse.
Hlara hyana sestie: Enge nér, *coantur, ye empanne tarwa liantassion, pelde sa pelonen, sampe *limpevorma ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende ettelea nórenna.
Íre i lúme yáviva túle, mentanes núroryar ñetien yáveryar.
Mal i *alamor namper núroryar, ar er palpanelte, exe nacantelte, exenna hantelte sarni tenna qualles.
Ata mentanes hyane núroli, rimbe epe i minyar, mal carnelte i imya tien.
I mettasse mentanes yondorya téna, quétala: “Samuvalte áya yondonyan!”
Cénala i yondo, i *alamor quenter mici inte: “Si i aryon ná; tula, alve nahta se ar ñete *aryonierya!”
Ar mápala se, hanteltes et i tarwallo ar nacanter se.
Etta, íre i *tarwantur tuluva, mana caruvas sine *alamoin?”
Quentelte: “Sine ulcain antauvas ulca metta, ar lavuvas hyane *alamoin *yuhta i tarwa, i antauvar sen i yávi íre i lúme utúlie!”
Yésus quente téna: “Lau hentanelde i tehtelessen: I ondo ya i şamnor querner oa, sá olólie cas i vinco. I Hérullo si utúlie, ar nas elmenda hendulmatse – ?
Etta quetin lenna: Eruo aranie nauva mapana oa lello ar antana nóren cárala yáveryar.
Ar ye lanta ondo sinanna nauva rácina; mal aiquen yenna lantas – aqua ascatuvas se.
Ar íre i hére *airimor ar i Farisar hlasser sestieryar, sintelte i quentes pa té,
ar ómu cestanelte mapa se runcelte i şangallo, pan sannelte pa se ve Erutercáno.
Ar hanquentasse Yésus ata carampe téna sestielissen, quétala:
“Menelo aranie ná ve nér, aran, ye carne veryangweo meren yondoryan.
Ar mentanes núroryar yalien i náner yáline i veryangweo merendenna, mal ualte merne tule.
Ata mentanes hyane núroli, quétala: Nyara i yálinain: 'Yé, amanwien mattinya, mundonyar ar maitane celvanyar anaier nahtane, ar ilqua manwa ná! Tula i veryangweo merendenna!'
Mal ualte cimne ar oanter, quén véra restaryanna, quén mancaleryanna;
mal i exi, mápala núroryar, lenganer tien yaiwenen ar nacanter te.
Mal i aran olle rúşea, ar mentanes hosseryar ar nancarne tane *nahtari ar urtane ostolta.
Tá quentes núroryannar: I veryangweo meren é manwa ná, mal i yálinar úner valde.
Etta mena i mallennar yar tulyar et i ostollo, ar yala aiquen ye hirilde i veryangweo merendenna.
Ar núror sine etelender ar comyaner illi i hirnelte, olcar ar mani véla, ar i şambe yasse carnelte i veryangweo meren náne quátina inen cainer ara i sarno.
Íre i aran túle minna yétien i caitalar ara i sarno, cennes nér ye úne netyana lanninen veryangweo.
Ar quentes senna: “Málo, manen túlel minna sir ú lannion veryangweo?” Sámes munta ya poldes quete.
Tá i aran quente núroryannar: 'Áse nute talusse ar mátse ar áse et-hate mir i mornie i ettesse; tasse nauvar i yaime ar i mulie nelciva.'
An rimbar nar yáline, mal mancar nar cíline!
Tá i Farisar lender ar carner panoli uo, rembien se questaryasse.
Ar mentanelte senna hildoltar as i Herolyar, i quenter: “*Peantar, istalme i nalye nanwa ar peanta Eruo malle nanwiesse, ar ualye cime aiquen, an ual yéta Atanion cendele.
Etta queta menna ya sanalye: Ma ná lávaina anta *tungwe i Ingaranen, hya lá?”
Mal Yésus, istala ulculta, quente: “Manen ná i tyastalde ni, *imnetyandor?
Ánin tana telpemitta i *tungweo!” Tulunelte senna lenár.
Ar quentes téna: “Mano emma ar tecie ná si?”
Quentelte: “I Ingarano!” Tá quentes téna: “Etta á paitya i Ingarano nati i Ingaranen, mal Eruo nati Erun!”
Ar íre hlasselte ta, anelte elmendasse, ar autala sello lendelte oa.
Yana auresse túler senna Sanduceáli, i quetir i lá ea enortie, ar maquentelte senna:
“*Peantar, Móses quente: Qui aiquen quale ú hínion, mauya hánoryan verya veriryanna ar orta erde hánoryan.
Enger aselte hánor otso, ar i minya, apa veryanes nissenna, qualle; ar pan pennes hína, hehtanes verirya hánoryan.
I imya nat martane yú i attean ar i neldean, é tenna i ostean.
Ve i métima i nís qualle.
Tá man se-samuvas ve veri i enortiesse? An illi mici te sámer se!”
Hanquentasse Yésus quente téna: “Loitalde, pan ualde ista i Tehteler ar Eruo túre véla.
An i enortiesse ualte verya exennar hya nar vertane, mal nalte ve menelo vali.
Mal pa i enortie qualinaron, ma ualte hentane ya náne quétina lenna lo Eru, íre quentes:
Inye Avrahámo Aino ar Ísaco Aino ar Yácovo Aino – ? Uas qualinaron Aino, mal coirearon!”
Hlárala ta, i şangar náner quátine elmendanen peantieryanen.
Íre i Farisar hlasser manen pustanes anto i Sandúcearon, ocomnelte uo.
Ar quén imíca te, *şanyengolmo, maquente tyastien se:
“*Peantar, mana i antúra axan i Şanyesse?”
Quentes senna: “Alye mele i Héru Ainolya quanda endalyanen ar quanda fealyanen ar quanda sámalyanen.
Ta ná i antúra ar minya axan.
I attea vávea ná: Alye mele armarolya ve imle.
Sine nát i axan atta yanten i quanda Şanye linga, ar i Erutercánor!”
Sí lan i Farisar aner comyane Yésus maquente téna:
“Mana sanalde pa i Hristo? Mano yondo náse?” Quentelte senna: “Laviro!”
Quentes téna: “Manen, tá, lerta Lavir i Fairenen esta se Heru, quétala:
I Héru quente herunyanna: Hara ara formanya tenna panyan ñottolyar nu talulyat – ?
Qui, tá, Lavir esta se Heru, manen náse yondorya?”
Ar ence úquenen hanquete senna erya quetta; ente, lá enge aiquen ye veryane maquete senna ambe.
Tá Yésus carampe i şangannar ar i hildonnar, quétala:
“I parmangolmor ar Farisar hárar Móseo hammasse.
Etta á care ar himya ilye i nati yar nyarilte len, mal áva care ve cardaltar, an quetiltes, mal uar care.
Nutilte lunge cólar ar panyar tai quenion pontesse, mal té uar mere appa tai lepereltanen.
Ilye carieltar carilte náven cénine lo atani, an carilte *colcalleltar palle ar laniltar hoe.
Melilte same i amminde nómi mattissen ar i minye hammar i *yomencoassen,
ar came *suilier i mancalenómessen ar náve estaina Ravi lo atani.
Mal elde, áva lave aiquenen esta le Ravi, an er ná *peantarelda, mal illi mici lé nar hánor.
Ente, áva esta aiquen cemende atarelda, an samilde erya Atar, i meneldea.
Ar áva na estaine *tulyandor, an samilde erya *Tulyando, i Hristo.
Mal yen ná i antúra mici le mauya náve núrolda.
Ye orta inse nauva nucumna, ar ye nucumne inse nauva ortaina.
Horro len, parmangolmor ar Farisar, *imnetyandor! An holtalde Eruo aranie epe Atani, an elde uar lelya minna, ar i lelyar minna pustalde!
Horro len, parmangolmor ar Farisar, *imnetyandor! An lelyalde olla ear yo nor querien erya quén savieldanna, ar íre acámies sa, carildes valda Ehennan lú atta ambe epe lé.
Horro len, *cénelóre *tulyandor, i quetir: 'Qui aiquen anta vandarya i cordanen, ta ná munta, mal qui aiquen anta vandarya i cordo maltanen, náse nauta.'
Aucor ar *cénelórar! An mana i ambe túra, i malta hya i corda ya airita i malta?
Yando quetilde: 'Qui aiquen anta vandarya i *yangwanen, ta ná munta; mal qui aiquen anta vandarya i annanen sasse, náse nauta.'
*Cénelórar! An mana i ambe tura, i anna hya i *yangwa ya airita i anna?
Etta ye anta vandarya i *yangwanen care sie sánen ar ilye i natinen sasse,
ar ye anta vandarya i cordanen care sie sánen ar sénen ye mare sasse,
ar ye anta vandarya menelden care sie Eruo mahalmanen ar yenen hára sasse!
Horro len, parmagolmor ar Farisar, *imnetyandor! An antalde i quaista i minto ar i lillo ar i cúmino, mal ehehtielde i ambe valdie nati i Şanyesse: failie ar óravie ar voronwie. Mauyane care nati sine, ananta lá hehta i exi.
*Cénelórar, i *saltar i pí, mal ammatir i ulumpe!
Horro len, parmangolmor ar Farisar, *imnetyandor, an poitalde ette i yulmo ar i veneo, mal i mityasse nalte quante pilweo ar peniéno *immoturiéva.
*Cénelóra Farisa, á poita minyave i mitya i yulmo ar i veneo, yando etteryo náven poica!
Horro len, parmangolmor ar Farisar, *imnetyandor, an nalde ve ninquinte sapsar, yar é *şéyar vanye i ettesse, mal i mityasse nalte quante axoinen qualinion ar ilya nostalénen úpoitiéva.
Mi sana lé yando elde i ettesse nemir faile atanin, mal i mityasse nalde quante *imnetyaleo ar *şanyelóriéno.
Horro len, parmangolmor ar Farisar, *imnetyandor, an caraste i *Erutercánoron sapsar ar netyar i failaron noiri,
ar quetilde: “Qui engelve auressen atarilvaron, ualve sáme ranta aselte i *Erutercánoron sercesse!”
Sie *vettalde indenna i nalde yondor ion nacanter i *Erutercánor!
Yando elde, tá, quata atarildaron lesta!
Leucar, vembion hín, manen uşuvalde Ehenno námiello?
Etta, yé! inye mentea lenna *Erutercánoli ar sailali ar parmangolmoli. Queneli mici te nahtuvalde ar tarwestuvalde, ar queneli mici te riptuvalde *yomencoaldassen ar roituvaldet ostollo ostonna,
ar sie tuluva lenna ilya faila serce *etulyana cemende, sercello faila Avelo sercenna Sacarío Varaciaion, ye nacantelde imbi i yána ar i *yangwa.
Násie quetin lenna: Ilye nati sine tuluvar *nónare sinanna!
Yerúsalem, Yerúsalem, ye nahta i *Erutercánor ar *sarya i nar mentaine senna – manen rimbave mernen comya hínalyar, ve poroce comya nessaryar nu rámaryat! Mal ualde merne.
Yé! Coalda ná len hehtana!
An quetin lenna: Ho sí ualde ni-cenuva tenna quetuvalde: Aistana ná ye túla i Héruo essenen!”
Autala, Yesus lende oa, mal hildoryar túler senna tanien sen i cordo ataqui.
Hanquentasse quentes téna: “Ma ualde cene ilye sine nati? Násie quetin lenna: Sisse ondo laume lemyuva to ondo ya ua nauva hátina undu!”
Íre hamnes Orontesse *Milloaldaron, i hildor túler senna íre anelte erinque, quétala: “Nyara men: Mana i lúme yasse nati sine martuvar, ar mana nauva i tanwa entulesselyo ar i tyeldo i rando?”
Hanquentasse Yésus quente téna: “Á yéta in *úquen tyare le ranya,
an rimbali tuluvar essenyanen, quétala: 'Inye i Hristo ná!', ar tyaruvalte rimbali ranya.
Hlaruvalde pa ohtali ar camuvar sinyali ohtalion; cima in ualde ruhtane! Maurenen martuvas, mal i metta en ua utúlie.
An nóre ortuva nórenna ar aranie aranienna, ar euvar saiceléli ar *cempaliéli rimbe nómelissen.
Ilye nati sine nar yesta naicelion.
Tá queneli antauvar le olla mir şangie ar nahtuvar le, ar nauvalde tévaine lo ilye i nóri castanen essenyo.
Ar tá rimbali lantuvar oa ar *vartuvar quén i exe ar tevuvar quén i exe.
Ar rimbe *hurutercánoli ortuvar ar tyaruvar rimbali ranya,
ar pan i *şanyelórie ale, melme oluva ringa i ambe rimbe quenissen.
Mal ye perpere i mettanna, isse nauva rehtana.
Ar i evandilyon i araniéno nauva carna sinwa i quanda ambaresse ve *vettie ilye i nórin, ar tá i metta tuluva.
Etta, íre cenilde i Faica Nat Nancariéva, pa ya Laniel i Erutercáno carampe, tárala aire nómesse” – lava yen hentea hanya –
“tá mauya in nar Yureasse uşe i orontinnar.
Nér i tópasse áva lelya undu talien armaryar et coaryallo,
ar nér i restasse áva nanwene coaryanna leptien collarya.
Horro i *lapsarwain ar i *tyetir vinimo ente auressen!
Hyama i *lercielda ua martuva hrívesse hya i *sendaresse,
an tá euva túra şangie, ve ua amartie i mardo yestallo tenna sí, ar ua martuva ata.
Qui ente auri úner nuhtane, ua enge hráve nála rehtana, mal i cílinaron márien, ente auri nauvar nuhtane.
Tá, qui aiquen quete lenna: 'Ela! Sisse ná i Hristo!', hya 'Tasse!', ávasa save.
An *huruhristoli ar *hurutercánoli ortuvar ar antauvar túre tanwali ar elmendali, tyarien yando i cílinar ranya, qui tá náne cárima.
Yé, len-anyárien nóvo.
Etta, qui queni quetir lenna: 'Yé! Náse i erumasse!', áva lelya tar. Qui quetilte: 'Yé, náse i mitye şambessen!', ávasa save!
An ve i íta hundiéva ettule Rómello ar calta Númenna, sie euva i Atanyondo tulesse!
I nóme yasse i quelet ea, tasse i şorni ocomuvar!
Ron apa i şangie mi ente rí, Anar oluva morna, ar Işil ua antuva işilmerya, ar i tinwi lantuvar menello, ar menelo túri nauvar pálaine.
Ar tá i tanwa i Atanyondova nauva cénina menelde, ar tá ilye nossi cemeno palpuvar inte nainiesse, ar cenuvalte i Atanyondo túla menelo fanyassen arwa túreo ar olya alcaro;
ar mentauvas valaryar taura rómanen, ar comyauvalte cílinaryar et i súrillon canta, menelo rénallo hyana rénaryanna.
Sí para sestie sina i *relyávaldanen: Íre olvarya ole musse ar tuias lasseryar, istalde i laire hare ná.
Sie yando elde, íre cenilde ilye nati sine, istar i náse hare, epe i fendi.
Násie quetin lenna i laume autuva *nónare sina nó ilye nati sine martar.
Menel cemenye autuvat, mal ninye quettar uar autuva.
Pa ré ar lúme enta úquen ista, lá i vali menelde ar lá i Yondo, mal i Atar erinqua.
An ve réryar Noaħ náner, sie euva mí Atanyondo tulesse.
An tambe anelte yane auressen nó i Oloire, matila ar sucila, veryala ar nála vertane, tenna i aure ya Noaħ lende mir i marcirya,
ar hanyanelte munta tenna i Oloire túle ar colde te illi oa, síve i Atanyondo tulesse euva.
Tá nér atta euvat er restasse: Qúen nauva talana ar i exe nauva hehtana.
Nís atta muluvat i imya *mulmanen: quén nauva talana ar i exe nauva hehtana.
Etta na cuive, an ualde ista mi mana ré Herulda tuluva.
Mal á ista si: Qui i *coantur sinte mi mana tiresse i arpo tuluva, anes cuiva ar ua láve aiquenen race mir coarya.
Etta yando elde na manwe, an i Atanyondo tuluva mi lúme yasse ualde sana sie.
Man ná i voronda ar saila mól ye herurya panyane or coaryo queni, antaven tien matta i vanima lúmesse?
Valima ná mól tana qui herurya, íre hé tule, hire se cára sie.
Násie quetin lenna: Hé panyuva se or ilqua ya hé same.
Mal qui tana ulca mól quetuva endaryasse: 'Herunya ná telwa',
ar *yestuvas palpa i hyane móli ar sucuva as i quatir inte limpenen,
tá tana mólo heru tuluva auresse ya uas apacene ar lúmesse ya uas ista,
ar se-paimetuvas naraca lénen ar antauva sen i imya metta ya i *imnetyandor camir. Tasse euvar níreryar ar mulierya nelciva.
Tá menelo aranie nauva ve vendi quean i taller calmaltar ar lender ettenna velien i ender.
Lempe mici te náner alasaile, ar lempe náner saile.
An i aucar namper calmaltar mal uar talle millo aselte,
íre i sailar namper millo olpeltassen as calmaltar.
Íre i ender náne telwa, illi mici té náner lorde ar húmer.
Endesse i lómio enge yáme: 'Yé i ender! Mena se-velien!'
Tá ilye tane vendi oronter ar manwaner calmaltar.
I alasailar quenter i sailannar: 'Ámen anta et milloldo, an calmalmar ron nauvar *luhtyane!'
I sailar hanquenter: “Cé i millo ua faryuva men ar len. I mende mena ar á *homanca elden!”
Apa oantelte *homancien, i ender túle, ar i vendi i náner manwe lende óse i merendenna veryangweo, ar i fenna náne holtana.
Epeta i hyane vendi yando túler, quétala: 'Heru, heru, ámen latya!'
Hanquentasse eques: 'Násie quetin lenna, uan ista le!'
Etta cima, an ualde ista i aure hya i lúme.
An i natto ná ve íre nér ye lelyumne hyana nórenna tultane mólyar ar antaner armaryar mir hepielta.
Ar quenen mici te antanes talenti lempe, exen atta, exen er – ve fintielta. Tá lendes oa.
Lintiénen ye camne i talenti lempe lende oa ar mancane tainen, ar ñentes an lempe.
Mí imya lé ye camne i atta ñente an atta.
Mal ye camne i er, oante ar sampe i talamesse ar nurtane i telpe heruryava.
Apa anda lúme i heru sane mólion túle ar sáme onótie aselte.
Ar túlala ompa, ye camne i talenti lempe talle an talenti lempe, quétala: “Heru, antanel talenti lempe mir hepienya. Ela, eñétien an talenti lempe!”
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo alasse!”
Yú ye camne i talent atta túle ompa ar quente: “Heru, talent atta antanel mir hepienya. Ela, eñétiel an talent atta!”
Herurya quente senna: “Mai carna, mára ar voronda mól! Anel voronda or nótime natali. Panyuvan lye or rimbe natali. Tula mir herulyo alasse!”
I mettasse ye camne i er talent túle ompa ar quente: “Heru, istan i nalye naraca nér, *cirihtala yasse ual rende ar comyala yasse ual vintane.
Etta anen ruhtaina ar oante ar nurtane talentelya i talamesse. Sisse samil ya *lyenya ná!”
Hanquentasse eques: “Olca ar lenca mól, tá sintel i *cirihtan yasse uan rende ar comyan yasse uan vintane?
Etta mauyane lyen panya telpenya as i *sarnomor, ar íre túlen ence nin came ya ninya né as napánina telpe!
Etta alde mapa i talent sello ar ása anta yen same i talenti quean!
An ilquenen ye same, amba nauva antana ar samuvas úve, mal ye ua same – yando ya samis nauva mapana sello!
Ar alde hate i *alamára mól mir i mornie i ettesse! Tasse euvar níreryar ar molierya nelciva.
Mal íre i Atanyondo tuluva alcareryasse, ar ilye i vali óse, tá hamuvas undu alcarinqua mahalmaryasse.
Ar ilye i nóri nauvar comyane epe se, ar te-*ciltuvas quén i exello, ve íre mavar *cilta i mámar i nyénillon.
Ar panyuvas mámaryar ara formarya, mal i nyénir ara hyarmarya.
Tá i aran quetuva innar tárar foryaryasse: 'Tula, le i nar aistane lo Atarinya, á harya i aranie manwana len tulciello mardeva!
An anen maita ar antanelde nin matta; anen soica, ar antanelde nin nat sucien; anen ettelea ar camnelde ni,
helda, ar tumpelde ni. Anen hlaiwa ar *cimbanelde ni, anen mandosse ar túlelde ninna.'
Tá i failar hanquetuvar senna sie: 'Heru, mana i lú ya cennelme lye maita ar antaner lyen matta, hya soica, ar antane lyen nat sucien?
Mana i lú ya cennelme lye ve ettelea quén ar camner lye, hya helda, ar tumper lye?
Mana i lú ya cennelme lye hlaiwa hya mandosse ar túler lyenna?'
Ar hanquentasse i aran quetuva téna: 'Násie quetin lenna: I lestasse ya carnelde ta quenen mici ampitye hánonyar sine, mí imya lesta inyen carneldes.'
Tá quetuvas yú innar tárar hyaryaryasse: 'Á auta nillo, elde i nar húne lo Atarinya, mir i oira náre ya anaie manwana i Araucon ar valaryain!
Anen maita, ar antanelde nin munta matien, anen soica ar antanelde nin munta sucien,
anen ettelea ar ualde camne ni, helda ar ualde tumper ni, hlaiwa ar mandosse, mal ualde *cimbane ni.'
Tá yú té hanquetuvar, quétala: 'Heru, mana i lú ya cennelme lye maita hya soica hya ettelea hya helda hya hlaiwa hya mandosse, ar ualme *veuyane len?'
Tá hanquetuvas lenna sie: 'Násie quetin lenna: I lestasse ya loitanelde care sie quenen mici sine ampityar, mí imya lesta loitanelde caritas inyen.'
Ar autuvalte mir oira paime, mal i failar, mir oira coivie.
Ar apa Yésus telyane ilye quetier sine, quentes hildoryannar:
“Istalde i aure atta ho sí i *Lahtale tuluva, ar i Atanyondo nauva antana olla náven tarwestana!”
Tá i hére *airimor ar i amyárar imíca i lie ocomner i pacasse i héra *airimóva, yeo esse náne Caiafas,
ar carner panoli uo mapien Yésus curunen ar nahtien se.
Mal quentelte: “Lá i aşaresse, hya euva amortie imíca i lie!”
Íre Yésus enge Vetaniasse, mi coarya Símon Helmahlaiwa,
nís arwa ondocolco quanta níşima millo túle senna, ar sa-ulyanes caryanna íre caines ara i sarno.
Cénala ta, i hildor náner rusce ar quenter: “Mana i casta hatien oa i millo?
An mo polde vacitas hoa nóten, antaven sa penyain!”
Istala si, Yésus quente téna: “Manen ná i cáralde *tarastie i nissen, íre acáries mára carda nin?
An i penyar samilde illume mici le, mal ualde illume samuva ni.
An íre nís sina panyane níşima millo sina hroanyasse, carneses ni-manwien i noirin.
Násie quetin lenna: Quiquie evandilyon sina nauva carna sinwa i quanda mardesse, ya nís sina carne yando nauva nyárina enyalien sé!”
Tá quén imíca i yunque, isse yeo esse náne Yúras, lende i hére *airimonnar
ar quente: “Mana antauvalde nin qui antanyes olla len?” Ar panyanelte telpemittar *nelequean epe se.
Ar cestanes manen antauvanes hé olla íre ence sen.
Minya auresse *Alapalúne Massaron i hildor túler Yésunna, quétala: “Masse meril i manwalme lyen, matielyan i Lahtie?”
Quentes téna: “Mena mir i osto ana sie-yo-sie ar queta senna: I Peantar quete: 'Lúmenya hare ná; coalyasse caruvan i Lahtie as hildonyar.'”
Ar i hildor carner ve Yésus canne tien, ar manwanelte i Lahtien.
Íre şinye túle, caines ara i sarno as i hildor yunque.
Íre mátanelte, quentes: “Násie quetin lenna: Quén imíca le antauva ni olla!”
Núra nairesse ilya quén mici te *yestane quete senna: “Lau inye ná sé, Heru?”
Hanquentasse eques: “Ye sumba márya asinye i salpesse ná ye antauva ni olla.
I Atanyondo é autuva, ve ná técina pa sé, mal horro i neren lo ye i Atanyondo antane olla ná! Náne arya tana neren qui únes nóna!”
Hanquentasse Yúras, ye antauvane se olla, quente: “Lau inye ná sé, Rappi?” Quentes senna: “Elye equétie sa!”
Lan mátanelte, Yésus nampe massa, ar apa quetie aistie ranceses, ar antala sa hildoryain quentes: “Ása mapa, mata! Si hroanya ná!”
Ar nampes yulma, ar apa antave hantale sa-antanes tien, quétala: “Suca et sallo, illi mici le!
An si ná sercenya i véreo, ya nauva etulyaina rá rimbalin apsenien úcariva.
Mal quetin lenna: Laume sucuvan sina yávello i liantasseo tenna enta aure yasse sucuvanyes vinya aselde Atarinyo araniesse!”
Ar apa lindie i *airilíri etelendelte Orontenna *Milloaldaron.
Tá Yésus quente téna: “Illi mici le lantuvar oa nillo lóme sinasse, an ná técina: 'Petuvan i mavar, ar i lamáreo mámar nauvar vintane.'
Mal apa návenya ortana, lelyuvan epe le mir Alilea!”
Mal hanquentasse Péter quente senna: “Qui illi lantuvar oa lello, inye ua oi lantuva oa!”
Yésus quente senna: “Násie quetin lenna: Lóme sinasse, nó tocot lamyuva, ni-laquetuval nel!”
Péter quente senna: “Qui mauya nin quale aselye, uan oi laquetuva lye!” Ilye i hyane hildor quenter i imya nat.
Tá Yésus túle aselte i nómenna estaina Etsemane, ar quentes hildoryannar: “Hama sisse íre lelyan tar hyamien!”
Ar tálala Péter ar i yondo atta Severaio as inse, *yestanes same naire ar náve *tarastaina.
Tá quentes téna: “Feanya ná lunga tenna qualme nairenen. Á lemya sisse ar hepa inde coive asinye!”
Apa menie şinta vanta ompa, lantanes cendeleryanna, hyámala ar quétala: “Atarinya, qui ta ná cárima, nai yulma sina autuva nillo! Ananta, lá ve inye mere, mal ve tyé mere!”
Ar túles i hildonnar ar hirne te lorne, ar quentes Péterenna: “Ma ualde polde hepe inde coive ter erya lúme asinye?
Na coive ar hyama, hya lantuvalde mir úşahtie! An i faire mína ná, mal i hráve milya ná!”
Ata, mi tatya lú, lendes oa ar hyamne, quétala: “Atarinya, qui ua cárima i si autuva ú sucienyo sa, nai indómelya martuva!”
Ar túles ata, ar hirneset lorne, an hendultat nánet lunge.
Ar lendes oa tello ar hyamne mi neldea lú, quétala i imya quetta ata.
Tá túles i hildonnar ar quente téna: “Lau en húmalde ar séralde? Yé, i lúme ná hare yasse i Atanyondo nauva antana olla mannar úcarindoron!
Á orta, alve mene! Ela! Ye anta ni olla utúlie hare!”
Ar íre i quettar en enger antoryasse, yé, tasse túle Yúras, quén i yunqueo, ar óse hoa şanga arwa macillion ar rundalion túle ho i hére *airimor ar i amyárar mici i lie.
Ye antane se olla antanelyane tien tanwa, quétala: “Aiquen ye miquin, áse mapa mir mando!”
Ar térave lendes Yésunna ar quente: “Hara máriesse, Rappi!”, ar minqueses.
Mal Yésus quente senna: “Málo, mana castalya tulien?” Tá túlelte ompa ar panyaner má Yésusse ar nampe se mir mando.
Mal yé, quén mici i náner as Yésus rahtane máryanen ar tunce macilya ar pente i héra *airimo mól ar aucirne hlarya.
Tá Yésus quente senna: “Á panya macilelya nómeryanna ata, an illi i mapar i macil nauvar nancarne i macilden.
Hya ual intya i lertan arca Atarinyanna, mentaveryan ninna lú sinasse or lehióni yunque valion?
Mal tá manen nauvas carna nanwa, i Tehtele ya quete i sie mauyas marta?”
Lúme yanasse Yésus quente i şangannar: “Ma utúlielde arwe macillion ar rundalion, ve pilunna, ni-mapien? Aure apa aure hamnen i cordasse peantala, ananta ualde nampe ni.
Mal ilqua sina amartie carien i Tehtele nanwa!” Tá ilye i hildor hehtaner se ar úşer.
Tá i namper Yésus tulyane se oa Caiafas i héra *airimonna, íre i parmangolmor ar i amyárar ocomner.
Mal Péter hilyane te hairallo, tenna paca i héra *airimóva, ar lendes minna ar hamune as i núror, cenien i metta.
I hére *airimor ar i Tára Combe cestaner húrala *vettie Yésunna, nahtieltan se,
mal hirnelte munta, ómu rimbe húrala astarmoli túler ompa. Mí tyel, atta túlet ompa
ar quentet: “Nér sina equétie: 'Polin hate undu i corda Eruva ar carasta sa ama ter auri nelde!'”
Tá i héra *airimo oronte ar quente senna: “Ma munta hanquétal? Mana ya queni sine *vettar lyenna?”
Mal Yésus náne quilda. Ar i héra *airimo quente senna: “I coirea Erunen panyan lye nu vanda i nyaruval men qui nalye i Hristo, i Eruion!”
Yésus quente senna: “Elye equétie sa. Mal quetin lenna: Ho sí cenuvalde i Eruion hára ara i Túreo forma ar túla menelo fanyassen!”
Tá i héra *airimo narcane larmaryar, quétala: “Naiequéties! Mana an maure astarmoiva samilve? Yé! Ahlárielde i i naiquetie.
Mana sanalde?” Hanquentelte: “Náse valda qualmeo!”
Tá piutanelte cendeleryanna ar penter se nondaltainen,
quétala: “Queta men ve Erutercáno, a Hristo! Man ná ye pente lye?”
Sí Péter hamne i ettasse i pacasse, ar *núre túle senna, quétala: “Yando elye náne as Yésus Alileallo!”
Mal laquentes ta epe illi, quétala: “Uan ista pa mana quétal!”
Apa lendes ettenna i andonna, hyana vende túne se ar quente innar enger tasse: “Nér sina náne as Yésus Nasaretello!”
Ar ata laquentes ta, vandanen: “Uan ista i nér!”
Apa şinta lúme i tarner *os se túler ar quenter Péterenna: “É nalye mici te, an questalya apanta lye!”
Tá *yestanes húta ar quete vandalínen: “Uan ista i nér!” Ar mí imya lú tocot lamyane.
Ar Péter enyalde ya Yésus quente: 'Nó tocot lamyuva, ni-laluval nel!”
Ar lendes ettenna ar láve sáre nírin sire.
Íre arin túle, ilye i hére *airimor ar i amyárar imíca i lie carner úvie Yésunna nahtien se.
Nunteltes ar tulyane se oa ar antane se olla Pílato i *nórecánonna.
Tá, íre Yúras cenne i hé náne námaina, sámes inwis ar talle nan i telpemittar *nelequean i hére *airimoin ar i amyárain.
Quentes: “Úcarnen íre antanen olla faila serce!” Quentelte: “Mana ta elmen? Elye cena sanna!”
Tá hantes i telpemittar mir i corda ar oante, ar lendes oa ar lingane inse.
Mal i hére *airimor namper i telpemittar ar quenter: “Ua lávina hatitat mir i aire harwe, an nalte *paityale sercen!”
Apa carie úvie *homancanelte tainen i cemnaro resta, ve sapsanóme etteleain.
Etta tana nóme anaie estaina “Serceresta” tenna aure sina.
Tá ya náne quétina ter Yeremia i Erutercáno náne carna nanwa: “Ar nampelte i telpemittar *nelequean, i *paityale i neren ye sáme valdierya tulcana, yen i Israelindi tulcane *paityale,
ar antanelte tai i cemnaro restan, ve i Héru canne nin!”
Mal Yésus tarne epe i nórecáno, ar i nórecáno maquente senna: “Ma elye aran Yúraron ná?” Yésus hanquente: “Elye quete sa!”
Mal íre i hére *airimor ar i amyárar quenter i carnelyanes ongweli, uas hanquente.
Tá Piláto quente senna: “Ma ual hlare ilye i nati yar *vettalte lyenna?”
Ananta uas hanquente senna, lá erya quettanen. Etta i nórecáno sáme túra elmenda.
Aşarello aşarenna náne haime i nórecánova lehtie i şangan quén i mandollo, i nér ye mernelte.
Mi yana lúme enge mandoltasse nér estaina Varavas, arwa ulca esseo.
Etta, apa ocomnelte, Piláto quente téna: “Man ná i quén ye merilde i lehtuvan len, Varavas hya Yésus estaina Hristo?”
An sintes i antaneltes olla *hrúcennen.
Ente, íre hamnes i hammasse namiéva, verirya mentane senna, quétala: “Nai nauva munta imbi lyé ar tana faila nér, an sámen túra naice síra oloresse pa sé!”
Mal i hére *airimor ar i amyárar *mirquenter i şangar arcienna Varavas, mal i nancarumnelte Yésus.
Hanquentasse i nórecáno quente téna: “Man imíca i atta merilde i lehtuvan lenna?” Quentelte: “Varavas!”
Piláto quente téna: “Tá mana caruvan Yésun ye ná estaina Hristo?” Illi mici te quenter: “Nai nauvas tarwestana!”
Eques: “Tá mana ulco acáries?” Mal yámelte en ambe: “Nai nauvas tarwestana!”
Íre cennes i poldes care munta aşea, mal ambe ron amortie ortumne, Piláto nampe nén ar sóve máryat epe i şanga, quétala: “Uan same cáma serceo nér sino! Elde cena sanna!”
Ar hanquentasse i lie quente: “Nai sercerya tuluva menna ar hínalmannar!”
Tá leryanes tien Varavas, mal Yésus rípes ar antane se olla náven tarwestaina.
Tá i nórecáno ohtari taller Yésus mir i túrion ar comyaner i quanda ohtarhosse senna.
Ar nampelte sello lanneryar ar panyaner sesse culda colla,
ar carnelte ríe neceliva ar panyaner sa caryasse, ar esce formaryasse.Ar lantala occaltanta epe se carampelte senna yaiwenen, quétala: “Aiya, aran Yúraron!”
Ar piutanelte senna ar namper i esce ar se-penter caryasse.
Ar apa yaiwelta senna nampelte oa sello i colla ar panyaner sesse vére larmaryar, ar tulyaneltes oa náven tarwestana.
Mí malle hirnelte nér ho Ciréne estaina Símon. Nér sina mauyanelte cole tarwerya.
Ar íre túlelte nómenna estaina Olyaşa, tá ná, *Caraxo,
antanelte sen limpe ostimesse as *sála, mal apa tyavie sa uas merne suce.
Apa tarwestie se etsantelte larmaryar hatiénen *şanwali,
ar hámala tasse tíraneltes.
Ente, or carya panyanelte i ongwequetta senna, técina: “Si ná Yésus, aran Yúraron!”
Tá pilu atta nánet tarwestane óse, quén foryaryasse ar quén hyaryaryasse.
I lahtaner naiquenter senna, pálala cariltar
ar quétala: “Elye ye hatumne i corda undu ar encarastumne sa ter auri nelde, á rehta imle! Qui nalye Eruo Yondo, tula undu i tarwello!”
Mí imya lé yú i hére *airimor as i parmangolmor caramper senna yaiwenen ar quenter:
“Exeli erehties; inse uas pole rehta! Náse Israélo aran; lava sen sí tule undu i tarwello ar savuvalme sesse!
Apánies estelya Erusse; sí lava hén etelehta se qui hé mere se, an quentes: Nanye Eruo Yondo!”
Mí imya lé yú i pilur tarwestane óse naityaner se.
I enquea lúmello mornie lantane or i quanda nóre, tenna i nertea lúme.
*Os i nertea lúme Yésus yáme taura ómanen, quétala: “Eli, eli, lema savahtáni?”, ta ná: “Ainonya, Ainonya, mana castalya hehtien ni?”
Íre hlasselte si, queneli imíca i tarner tasse quenter: “Nér sina yála Elía!”
Ar mi imya lú quén mici te nampe hwan ar quante sa sára limpenen ar panyane sa escesse ar láve sen suce.
Mal i exi quenter: “Lava sen náve! Alve cene qui Elia tule rehtien se!”
Mal Yésus ata yáme taura ómanen ar effirne.
Ar yé! i fanwa i cordasse náne narcana mir atta, telmello talmanna, ar i cemen palle, ar i ondor náner şance.
Ar i noiri náner pantane, ar rimbe hroali i lorne airion náner ortane,
ar túlala et i noirillon apa anes ortana túlelte mir i aire osto ar tanner inte rimbain.
Mal i *tuxantur ar óse i tíraner Yésus, íre cennelte i *cempalie ar i nati martala, náner ita ruhtaine, quétala: “Nér sina é náne Eruo Yondo!”
Enger tasse rimbe nisseli tírala hairallo, i hilyaner Yésus Alileallo *vevien sen.
Mici té náner María Mahtaléne, yú María amil Yácovo ar Yósefo, ar i amil yondoron Severaio.
Íre şinye lantane, túle tar lára nér Arimaşeallo estaina Yósef, ye yú náne Yésus hildo.
Nér sina lende Pilátonna ar arcane Yésuo hroa. Tá Piláto antane i canwa, ar anes antana sen.
Ar Yósef nampe i hroa ar sa-vaitanes poica *páşenen,
ar sa-panyanes vinya noiriryasse, ya nóvo carnes ve hróta i ondosse. Apa *peltanes sar epe i noirio fennanna oantes.
Mal María Mahtaléne ar i hyana María enger en tasse, hámala epe i noire.
I hilyala auresse, ya náne apa i Manwie, i hére *airimor ar i Farisar ocomner epe Piláto,
quétala: “Heru, enayálielme ya tana nér huruiva quente íre en anes coirea: 'Apa auri nelde nauvan enortana.'
Etta á anta i canwa, ar i noire nauva varyaina tenna i neldea aure, pustien hildoryar ho tulie ar pilie se ar quetie i lienna: 'Anes ortana qualinallon!', ar i métima loima nauva olca epe i minya!”
Piláto quente téna: “Samilde cundor. Mena ar ása care ve varna ve istalde!”
Lendelte ar carner i noire varna *lihtiénen i ondo ar paniénen i cundor tasse.
Apa i *sendare, árasse i minya auresse i otsolo, María Mahtaléne ar i hyana María túler cenien i noire.
Ar yé! martane hoa *cempalie, an i Héruo vala túle undu menello ar panyane oa i ondo, ar hamnes sasse.
Ilcerya náne ve íta, ar larmaryar náner lossie.
Ruciénen sello i cundor paller ar náner ve qualine.
Mal i vala quente i nissennar: “Ávas ruce, an istan i cesteaste Yésus, ye náne tarwestana.
Uas sisse, an anaies ortana, ve quentes. Tula, cena i nóme yasse caines!
Ar mena lintiénen ar nyara hildoryain in anaies ortana qualinallon, ar yé! ménas epe le mir Alilea; tasse cenuvaldes. Yé, anyárien lent!”
Ar lintiénen autala i noirillo, arwa ruciéno ar túra alasseo, nornette ta-nyarien hildoryain.
Ar yé! Yésus velde tu ar quente: “Hara máriesse!” Túlette senna ar nampet talyat ar lantanet undu epe se.
Tá Yésus quente túna: “Áva ruce! Mena, nyara hánonyain, menieltan mir Alilea, ar tasse cenuvalte ni!”
Mal apa lendette, yé, queneli i cundoron lender mir i osto ar nyarne i héra *airimon pa ilqua ya martanelyane.
Ar yomeniesse as i amyárar carnelte úvie, ar antanelte fárea nóte telpemittalíva i ohtarin
ar quenter: “Queta: 'Hildoryar túler i lómisse ar pilder se íre anelme lorne.'
Ar qui si tule i nórecáno hlarunnar, elme quetuvar senna, ar elde áva ruce!”
Ar nampelte i telpemittar ar carner ve náne tien peantana, ar quetie sina anaie vintaina mici Yúrar tenna aure sina.
Mal i hildor minque lender mir Alilea, i orontenna yasse Yésus quente i velumnes te,
ar íre cenneltes, lantanes undu epe se, mal queneli úner tance i saviesse.
Ar Yésus lende téna ar carampe téna, quétala: “Ilya hére anaie nin antana mi menel cemenye.
Etta á lelya ar cara ilye nóri hildoli, sumbala te mí esse i Ataro ar i Yondo ar i Aire Feo,
peantala te care ilye i nati yar inye canne len. Ar yé, inye ea aselde ilye i rí tenna tyel i rando!”
Yácov, Eruo ar i Heru Yésus Hristo mól, i nossin yunque yar nar vintaine: Hara máriesse!
Hánonyar, íre samilde tyastier ilye nostaleron, nota ilye tai ve poica alasse,
istala, ve carilde, i tyastaina savielda care voronwe.
Mal lava voronwen telya mótierya, náveldan ilvane ar telyaine ilye natassen, pénala munta.
Sie, qui aiquen mici le pene sailie, nai arcuvas Erullo, ye anta illin, mérala ar ú narace quettaron, ar sailie nauva sen antaina.
Mal nai arcas saviesse, aqua ú iltance sanwion, an ye ná iltanca ná ve falma earo, mentaina i súrinen ar vávaina sir yo tar.
Áva lave taite neren intya i camuvas *aiqua i Herullo;
náse nér atwa sámo, iltanca ilye malleryassen.
Mal nai i nalda háno samuva alasse pan náse carna arta,
ar i lára pan náse nucumna, an ve lóte i salqueo autuvas.
An Anar orta mi úre, ar hestas i salque, ar lóterya lanta oa ar vanima ilcerya fire. Sie yando i lára quén fifirúva endesse roitieryaron.
Valima ná i nér ye perpere tyastiesse, an aqua tyestaina camuvas i ríe coiviéva, pa ya i Heru ánie vanda in melir se.
Nai quén ye ná tyestaina ua quéta: "Nanye tyestaina lo Eru." An Eru ua *tyestima ulcunen; ente, sé ua tyasta aiquen.
Mal ilquen ná tyestaina návenen túcina ar şahtaina véra maileryanen.
Tá i maile, íre nostas, cole úcare, ar íre i úcare anaie cárina, tyaris qualme.
Lava *úquenen tyare le ranya, melde hánonyar!
Ilya mára nat antaina ar ilya ilvana anna tule tarmenello, an tulis undu calaron Atarello: Pa sé ea munta ya ahya hya leo ya leve.
Pan mernes, vi-tyarnes ea i quettanen nanwiéno, návelvan minye yáveli onnaryaron.
Á ista nat sina, melde hánonyar: Ilya atanen mauya náve linta pa hlarie, lenca pa quetie, lenca pa orme;
an atano orme ua tyare Eruo failie tule.
Etta á panya oa ilya soa ar ulco, an ua ea maure ulcuva. Moicave cama i empanie i quettava ya pole rehta fealdar!
Mal na carindoli i quetto, lá eryave *hlarindoli, tyárala inde ranya.
An qui aiquen ná *hlarindo i quetto, ar lá carindo, sana quén ná ve nér ye yéta hroaryo cendele cilintillanen.
An yétas inse, mal mí lú ya autas, ilcerya talta *réneryallo.
Mal ye ceşe i ilvana şanye lériéva, ar perpere sasse – pan acáries inse, lá *hlarindo ye enyale munta, mal carindo moliéno – nauva valima molieryasse.
Qui aiquen sana pa inse ve *tyerindo, ananta uas ture lambarya, mal lave endaryan ranya, tá quén sino *tyerme ná cumna.
I *tyerme ya ná poica ar ú vaxeo Ainolva i Ataren ná si: Cimie i mauri hínion ú ontaruo ar *verulóraron şangieltasse, ar hepie immo ú vaxeo i mardello.
Hánonyar, lau cimilde cendeler mí imya lúme ya savilde mi Yésus Hristo Herulva?
An qui nér tule yomenieldanna, arwa cormalion maltava ar maira larmo, mal penya quén vára larmasse yando tule,
ar mi alasse camilde ye cole i maira larma ar quetir: "Elye hama sisse minda nómesse!", mal quetilde i penyanna: "Elyen ece tare", hya: "Hama tasse ara i tulco talunyant"
– tá notilde quelli mici le mirwe lá exeli, ar nalde námoli i quetir olce námier. Ma si ua nanwa?
Á lasta, meldanyar: Ma Eru ua cille mar sino penyar náveltan láre saviesse ar aryoni i araniéno, pa ya antanes vanda in melir se?
Mal elde alálier i penyan áyarya. Ma láre queni uar lumna len ar tucir le epe námor?
Ma ualte nanquete i essenen yanen anelde yáline?
Carilde mai qui é lengalde i arna şanyenen, ve i Tehtele quete: "Mela armarolya ve imle."
Mal qui cimilde cendeler, úcarilde, pan camilde *tulcarpie i Şanyello, pa racie sa.
An aiquen ye himya i quanda Şanye, mal loita pa erya nat, arácie ilye i axani.
An ye quente: "Ávalye *úpuhta!", yando quente: "Áva nahta!" Qui ual *úpuhta, mal é nahtal, aráciel i Şanye.
Ve queni i nauvar námine i şanyenen lériéva, sie alde carpa, ar sie alde lenga!
An ye ua care oravie, camuva námie ú óraviéno. Yellolissen oravie túrua namie.
Manen ná aşea, hánonyar, qui quén quete i samis savie, mal uas same cardar? Lau i savie pole rehta se?
Qui háno hya néşa ná helda ar pene matso i auren,
ananta quén mici le quete téna: "Mena rainesse, na lauca ar quátina", mal uas anta tien hroalto mauri, manen ta ná aşea?
Sie yando savie, qui uas same cardar, ná insasse qualin.
Ananta quén cé quetuva: "Elye same savie, ar inye same cardar." Ánin tana savielda hequa cardainen, ar inye lyen-tanuva savienya cardanyainen!
Savil i ea Eru er, lá? Caril mai. Yando i raucor savir – ar palir!
Mal ma meril ista, a cumna atan, i ú cardaron savie care munta mára?
Ma Avraham atarelva úne quétina faila apa *yances Ísac yondorya i *yangwasse?
Ve cenil, savierya móle as cardaryar, ar cardaryainen savierya náne carna ilvana.
Sie i tehtele náne *amaquátina ya quete: "Avraham sáve i Hérusse, ar ta náne nótina sen ve failie", ar anes estaina "i Héruo meldo".
Cenil i ná quén *failanta cardainen, ar lá saviénen erinqua.
Mí imya lé, ma Ráhav i *imbacinde úne *failanta cardalínen, apa camnes i larmor ar mentaner te oa hyana mallenen?
Ve i hroa ná qualin ú şúleo, sie yando savie ú cardaron ná qualin.
Áva lave rimbalin mici le náve *peantari, hánonyar, an istalde i camuvalve ambe lunga námie.
An illi mici vi taltar rimbave. Qui aiquen ua talta quettasse, sé ná ilvana nér, ye yando pole ture quanda hroarya.
Qui panyalve *pérappar mir roccoron antor, carieltan ve merilve, mahtalve yando quanda hroalta.
Yé! Yando ciryar, ómu nalte ta hoe ar taure súri tyarir tai leve, nar túrine ampitya *tullanen, yanna i ciryaher mere.
Sie yando i lamba ná pitya hroaranta, ananta laitaxe ita. Yé, manen pitya ná i náre ya pole narta ta hoa taure!
Ar i lamba ná náre! I lamba ná mar *úfailiéva mici hroarantalvar, an vahtas i quanda hroa ar narta i querma coiviéva, ar sá ná nartaina Ehennanen.
An ilya nostale hravani celvaron ar aiwion ar hlócion ar onnaron earesse anaier cárine núror Atanion.
Mal i lamba – ea *úquen mici Atani ye pole care sá núrorya. *Útúrima, ulca nat, nas quanta qualmetúlula hloimo.
Sánen aistalve i Héru ar Atar, ananta sánen yando hútalve atani i nar ontaine Eruo emmasse.
Et i imya antollo tulir aistie ar hútie.Ua mára, hánonyar, i nati sine martar sie.
Ehtelesse, lisse ar sára nén ua ulya i imya assallo, lá?
Hánonyar, *relyávalda ua pole anta *milpior, hya liantasse *relyávi, lá? Mí imya lé, singwa nén ua pole anta lisse nén.
Man ná saila ar handa mici le? Nai tanuvas cardaryar mára lengieryanen, i milya lénen sailiéva.
Mal qui samilde sára *hrúcen ar melme costiéva endaldasse, áva laita inde ar queta huruli i nanwienna.
Si ua i melme ya tule tarmenello, mal nas cemello – *celvavea, *raucea.
An yasse ear *hrúcen ar melme costiéva, tasse ear *úpartale ar ilya şaura nat.
Mal i sailie tarmenello ná or ilqua poica. Ente, melis raine, nas milya, cimis exion nirme, nas quanta óraviéno ar máre yávion, uas cime cendeler, uas *imnetyala,
mal i erde yáveo failiéva ná rérina rainesse in carir raine.
An mallo tulir ohtali, mallo ear costiéli mici le? Ma ualte tule silo: maileldallon, i ohtacarir mi hroarantaldar?
Merilde, mal ualde same. Nahtalde ar milyalde, ananta ualde pole came. Mahtalde ar ohtacarilde. Ualde same, pan ualde arca.
Arcalde, ananta ualde came, an arcalde &perqua castanen, sa-*yuhtien maileldasse alassiva i hráveo.
Elde i racir vestale, ma ualde ista i qui mo ná nilda i marden, mo ná cotya Erun? Etta, aiquen ye mere náve meldo i mardo care inse ñotto Eruo.
Hya ma sanalde i ú cesto i Tehtele quete: "*Hrucennen i faire ya mare vesse milyea" – ?
Mal ambe túra ná i Erulisse ya antas. Etta quetis: "Eru tare i &turquimannar, mal antas lisse i naldain."
Etta á panya inde nu Eru, mal tara i Arauconna, ar uşuvas lello.
Tula hare Erunna, ar tuluvas hare lenna. Á poita máldat, a úcarindor, ar cara endalda poica, le arwe atwa sámo.
Á anta inde angayassen, sama nyére, lava níreldain ulya! Nai lalielda ahyuva mir nainie, ar alasselda mir nyére!
Nucuma inde mí Héruo hendu, ar le-ortuvas ama.
Áva quete ulco quén pa i exe, hánor! Ye quete ulco pa háno hya name hánorya quete ana şanye ar name şanye. Mal qui namil şanye, ual carindo şanyeo, mal námo.
Ea Er ye antane i Şanye ar ná námo, sé ye pole rehta ar nancare. Mal elye, manen lertal name armarolya?
Tula, sí, elde i quetir: "Síra hya enwa lelyuvalve osto sinanna ar lemyuvalve tasse ter loa, ar mancuvalve ar caruvar telpe"
– ar elde uar ista manen coivielda nauva enwa! An nalde híşie, cénina mi şinta lú ar tá autala.
Mí men, carnelde mai qui quentelde: "Qui i Heru mere, nauvalve en coirie ar yando care si hya ta."
Mal sí samilde *valate *immolaitieldainen. Ilya taite *immolaitie ná olca.
Etta, qui quén ista care ya ná vanima, ananta uas care sa, ta ná sen úcare.
Tula, sí, elde i nar láre; nírelissen á naina pa i nyéri yar túlar lenna!
Larelda uruxie, ar larmaldar nar mátine lo malwi;
i malta yo i telpe nar nwárine mir asto, ar i asto *vettuva lenna ar ammatuva hroalda. Nas ve náre, ta ya ocómielde i métime auressen!
Hlara! I *paityale yan i queni i *cirihtaner restaldar omótier, mal ya elde ehépier, yáma lenna! I *cirihtoron rambi utúlier hlarunna i Héruo Hossion.
Cemende asámielde úve ar coivie maileva. I auresse nahtiéva acárielde endalda tiuca.
Anámielde, anahtielde i faila; uas pustane le.
Sama cóle, tá, tenna i Heru tule! *Cemendur yéta ompa cemeno mirwa yávenna; mauya sen same cóle tenna i miste alantie mi quelle ar tuile véla.
Sie mauya yando len same cóle ar huore, an i Heruo tulesse ná hare.
Áva sique quén i exenna, hya cé nauvalde námine. Yé! I Námo tára epe i fennar.
Hánor, para ho i lé yasse i *Erutercánoron – i caramper mí Héruo esse – perpérer ulco ar sámer cóle.
Yé, estalve valime i eperpérier! Ahlárielde pa voronwerya Yóv ar istar i manar ya i Héru antane, manen i Héru same núra *ofelme ar ná quanta lisseo.
Or ilqua, hánonyar, áva anta vandar – menelden hya cemennen hya ilya hyana vandanen. Nai quetilde "ná, ná" – "vá, vá", hya lantuvalde nu námie.
Ma ea aiquen mici le ye same perperie? Nai hyamuvas! Mal ea aiquen arwa alasseo? Nai linduvas airelíri!
Ma ea aiquen hlaiwa mici le? Nai yaluvas i amyárar i ocombeo, hyamieltan or se, *lívala se millonen i Héruo essenen.
Ar i hyamie saviéva tulúva mále i caimeassean, ar i Héru ortuva se. Ente, qui acáries úcareli, ta nauva sen apsénina.
Etta queta pantave, quén i exenna, pa úcareldar; ar hyama quén i exen, náveldan nestaine. Faila queno arcande cole úvea túre íre móteas.
Elía náne nér arwa i imye felmion yar elve samir, ananta hyamiesse arcanes i ua lantumne miste, ar miste ua lantane ter loar nelde ar astar enque.
Ar hyamnes ata, ar menel antane miste ar i nóre colle yáverya.
Hánonyar, qui aiquen mici le ná tyárina ranya i nanwiello ar exe *nanquere se,
á ista si: Aiquen ye quere úcarindo malleryo loimallo rehtuva fearya qualmello ar tupuva liyúme úcarelion.
Péter, Yésus Hristo apostel, i etyannar vintaine mi Pontus, Alatia, Capparocia, Ásia ar Vişinia – i cílinnar
i náner sinwe nóvo lo Eru i Atar, airitiénen lo i Faire, carieltan ve Eruo indóme ar náveltan poitaine Yésus Hristo sercenen: Nai samuvalde Erulisse ar raine úvesse!
Aistana na Eru, Yésus Hristo Herulvo Atar, an túra óravieryanen ven-antanes vinya *nónie coirea estelenna i *enortiénen Yésus Hristova qualinillon,
*aryonienna ya ua vahtaina hya fífírula. Nas sátina menelde len
i nar varyaine lo Eruo túre, saviénen, rehtien ya ná manwa náven apantaina i métima lúmesse.
Sinen yando sí samilde túra alasse, ómu mi lúme sina, qui ea maure, samilde nyére mi sina hya tana tyastie.
Sie tyastaina savielda, mirwa ambela malta ya ná *hastima ómu nas tyastaina nárenen, nauva hírina ve casta laitaleo ar alcaro ar laitiéno i apantiesse Yésus Hristova.
Ualde oi cenne se, ananta melildes. Ualde yéta se sí, ananta savilde sesse ar samir alasse alcarinqua han quettar,
pan cámalde i met savieldo, i rehtie fealdaivar.
Pa rehtie sina i *Erutercánor cestaner ar cenşer, i quenter apacelli i Erulisseo ya elde camumner.
Névelte hanya mana náne i lúme ya i Faire tesse tenge i Hriston, íre *vettanes nóvo pa Hristo ñwalmi ar i alcari yar tulumner apa tai.
Náne tien apantaina i lá inten, mal elden, anelte núror i nation yar sí nar *nyardaine len lo i tuluner lenna i evandilyon, Aire Feanen mentaina menello. Nati sine vali merir ceşe!
Etta aqua hepa laicelda, hepiénen sámalda manwaina. Sama estel mí Erulisse ya nauva tulúna lenna i apantiesse Yésus Hristova.
Ve *canwacimye híni, áva na cátine ve i íri yar yá sámelde, íre pennelde istya.
Mal, indómenen i Aireo ye le-yalle, na yando elde airi ilye carieldassen,
an ná técina: «Mauya len náve airi, pan inye aire ná.»
Ente, qui yálalde i Atarenna ye ua cime man aiquen ná, mal name ve ilqueno mótie ná, á lenga ruciénen íre marilde ve etyali.
An istalde i únelde rehtaine *yávelóra lengiello hastime natalínen, telpenen hya maltanen.
Úsie, anelte rehtaine mirwa sercenen, ve ta euleo ú vaxeo hya mordo – Hristo serce.
É anes sinwa nóvo nó i tulcie i mardeva, mal anes apantaina i mettasse lúmion márieldan,
lé i sénen savir Erusse, ye ortane se qualinillon ar antane sen alcar, panieldan savielda ar estelelda Erusse.
Apa poitie fealdar cimiénen i nanwie, ya anta hánonilme ú *etyanetiéno, mela quén i exe tumnave i endallo.
An vinya *nónie ná len antaina, lá *hastima mal *alahastima erdenen, coirea ar vórea Eruo quettanen.
An «ilya hráve ná ve salque, ar ilya alcarerya ná ve salqueo lóte; i salque hesta, ar i lóte lanta oa,
mal i Héruo quetie lemya tennoio.» Ar si ná i quetie, i evandilyon *nyardaina len.
Etta á panya oa ilya ulco ar ilya *ñaunie ar ilye *hrúceni ar ilye narace hanquentar.
Ve *vinyanóne lapsar á milya i alahasta ilin i quettava, alieldan mir rehtie,
qui atyávielde i Heruo márie.
Íre tulilde senna, i ondonna ya náne quérina oa lo atani, mal ná cílina ar mirwa Erun,
yando elde nar carastaine ama ve coirie ondoli, cárala ataque faireva, ve aire *airimosse, *yacien *yancar faireva yar Eru mere came, ter Yésus Hristo.
An ve i Tehtele quete: «Yé, panyan Siondecílina ondo, tulcala *vincondo, mirwa, ar aiquen ye save sasse laume nauva naityana.»
Etta náse mirwa len i savir; mal in uar save, «i imya ondo ya i şamnor querner oa ná sí cas i vinco»,
ar «sar taltiéva ar ondo tyárala queni lanta». Lantalte pan ualte cime i quetta; sie anaielte martyaine.
Mal elde nar cílina nosse, arna *airimosse, aire nóre, mirwa lie, náveldan tercánoli i máriéno yeo le-yalle et morniello mir calarya elmendava.
An enge lúme ya únelde lie, mal sí nalde Eruo lie; anelde i uar camne óravie, mal nalde sí i é acámier óravie.
Meldar, hortan le ve etyali ar quelli i marir ettelie nóressen: Hepa inde i hráveo mailellon, yar ohtacarir i feanna.
Á lenga mai imíca i nóri! Tá i sí quetir ulco pa le, ve pa ulcarindor, nauvar astarmor máre cardaldaron, ar antuvar alcar Erun i auresse ya ceşis te.
I Heruo márien á panya inde nu ilya ontie atanion – cé nu i aran, ve inga quén,
cé nu cánor mentaine sello pamietien i carir ulco, mal laitien i carir márie.
An si ná Eruo indóme: Cariénen márie á pusta i *úhandea carpie quenion i penir istya!
Na ve lére queni, mal á *yuhta lérielda, lá nurtien ulco, mal ve Eruo móli.
Á anta alcar illin, mela i otornasse, ruca Erullo, á anta alcar i aranen!
Nai núror panyuvar inte nu herultar ilya caurenen, lá nu i mani ar i moicar erinque, mal yando nu i hrangar.
An ná vanima nat qui quén, tuntala mana rohtarya Erun, perpere nyéri ar same urdie úfailave.
An qui, apa úcarie, nalye palpaina ar perperilyes, manen ta ná *laitima? Mal qui samil urdie íre caril márie, ar perperilyes, ta ná vanima nat Erun.
Nat sinanna é anelde yáline, an yando Hristo perpére ñwalmeli elden, hilieldan ca se tieryasse.
Uas carne úcare, ar huru úne hírina antoryasse.
Íre exeli caramper senna yaiwenen, uas carampe yaiwenen téna. Íre anes ñwalyaina, uas *ordane, mal panyane nattorya epe ye name failave.
Sé colle úcarelvar véra hroaryasse i tarwesse, levielvan han úcari ar samielvan coire failiénen. Ar «nahteryainen anelde nestaine».
An anelde ve mámar, ranyala; mal sí enutúlielve i mavarenna ye ortire fealvar.
Mi imya lé, nai i nissi i evérier panyuvar inte nu verulta. Sie, qui aiqueni uar cime i quetta, cé nauvalte ñétine ú quetto, veriltaron lengiénen,
apa tirie poica lengielda ar i áya ya tanalde.
Ar áva netya inde i ettesse, *partiénen findeléva, hya paniénen neteli maltava indesse, hya coliénen larmar,
mal á netya inde i endo nulda quennen, i quilda ar moica feo *alahastima larmanen, ya ná ammirwa Eruo hendusse.
An sie yando i airi nissi i sámer estel Erusse netyaner inte, panyala inte nu verultar,
ve Sara carne ya Avraham quente, estala se heru. Ar elde anaier cárine Saro híni, qui carilde márie ar uar caurie pa *aiqua rúcima.
A veruvi, mara as verildar handenen, antala tien alcar ve ambe milya venen, i inya, pan nalde yando aryoni aselte i Erulisseo coiviéva – hya hyamieldar nauvar hampe.
Teldave, á na erya sámo ar sama *ofelme, arwe melmeo hánoldaiva ar lauca endo, nucúmala inde.
Áva *nampaitya ulcunen ulcun hya narace quettainen narace quettain; úsie, alde aista, an nat sinanna anaielde yáline, náveldan aistiéno aryoni.
An «ye mere mele coivie ar cene máre aureli, nai hepuvas lambarya ulcullo ar péryat quetiello huru,
mal lava sen quere inse ulcullo ar care márie; nai cestuvas raine ar roituva sa!
An i Héruo hendu nát quérine failannar, ar hlaryat arcandeltanna, mal i Héru apánie cendelerya ana i carir ulqui.»
Ar man ná i quén ye caruva ulco len qui uryalde yan mára ná?
Ono qui é samuvalde urdie carieldanen márie, valime nalde! Ávalde ruce tello, ar áva na ruhtaine,
mal alde airita i Hristo ve Heru endaldasse! Illume sama hanquentalda manwa ilquenen ye cane casta i estelen ya mare lesse, ono cara sie mi milya lé ar arwe áyo.
Hepa *immotuntielda mára! Sie, íre camilde cotye quettali, i queni i quetir ulco pa mára lengielda Hristosse nauvar naityaine.
An samie urdie pan cáralde márie, qui Eruo indóme mere sa, ná len arya epe samie urdie pan cáralde ulco.
Yando Hristo qualle erya lú pa úcari, faila quén rá úfailain, ecien sen le-tulya Erunna, apa cennes qualme i hrávesse, mal náne carna coirea i fairenen.
Sie yando oantes ar *nyardane i fairennar mandosse,
i uar cimne canwaltar íre Eru cóleryasse *lartane Nóho auressen, i carastiesse i marciryava, yasse mancali, tá ná, queni tolto, náner cóline mi varnasse ter i nén.
Ya hanquete ana si, *tumyale, sí rehta lé. Uas panya oa i hráveo soa, mal nas arcande Erunna pa mára *immotuntie, i *enortiénen Yésus Hristova.
Háras ara Eruo forma, an oantes menelenna, ar vali ar héri ar túri anainer panyaine nu se.
Etta, pan i Hristo náne nwalyaina i hrávesse, mapa ve carmalda i imya nostale sámo – an ye asámie nyére i hrávesse ná han úcari.
Sie polis *yuhta i lúme ya lemya coivieryo i hrávesse, lá ambe atanion írin, mal Eruo indómen.
An asámielde fárea lúme i vanwiesse arwe taite coiviéno ya queni i nórion *cilir, vantala lehta lengiesse, mailesse, súcala limpe úvesse, arwe hravani merendion, *accasúcala, ar *tyérala cordoni.
Sí nalte quátine elmendanen, pan ualde ambe nore aselte celume sinasse vára lengiéva, ar carpalte lenna yaiwenen.
Mal queni sine hanquetuvar pa si yen ná manwa namien coirear ar qualini.
Etta i evandilyon náne tulúna yando qualinnar, náveltan námine i hrávesse, mi Atanion lé, mal samieltan coivie i fairesse, mi Eruo lé.
Mal ilye nation metta utúlie hare. Etta na málesse sámo ar hepa laicelda, hyamien.
Or ilye nati, mela quén i exe tumna melmenen, pan melme tupe liyúme úcariva.
Cama exeli mir coaldar, lá nurrula.
Pan ilquen mici le acámie anna, ása *yuhta íre *veuyalde quén i exe, mí *alavéle Erulissi.
Qui aiquen quete, nai quetuvas ve qui quetieryar náner Eruo quettar. Qui aiquen *veuya, nai *veuyuvas ve i poldorénen ya Eru anta; sie Eru camuva alcar ilye natissen, Yésus Hristonen, ye same i alcar ar i melehte tennoio ar oi. Násie!
Meldar, áva na elmendasse pa i ruive ya marta mici le tyastien le, ve qui ettelea nat amartie len.
Úsie, sama alasse pan samilde ranta i Hristo ñwalmessen, náveldan valime han lesta yando i apantiesse alcareryava.
Qui camilde yaiwe Hristo essenen, valime nalde, pan i faire alcaro, é i faire Eruo, caita lenna!
Mal nai *úquen mici le samuva urdie pan acáries nahtie hya pilwe hya ulco hya *atarastie exeli pa vére nattoltar.
Ono qui samis urdie pan náse Hristondur, nai uas nauva naityana, mal nai laituvas Eru pa esse sina!
An utúlie i lúme yasse i namie nauva *yestaina coallo Eruva. Qui nas *yestaina vello, mana nauva i tyel ion uar *canwacimye Eruo evandilyonen?
«Ar qui i faila nér ná rehtaina urdave, mana martuva i Erucotyan ar i úcarindon?»
Sie, nai i samir urdie pan cimilte Eruo indóme panyuvar fealtar máryasse, íre carilte márie.
Etta, i amyárain mici le antan hortale sina – an yando inye ná amyára nér aselde, ar astaro i ñwalmion i Hristo, quén ye same ranta i alcaresse ya nauva apantaina:
Na mavari i lámáreo Eruo nu ortírielda, lá maustanen, mal nirmenen, lá camien úfaila *ñéte, mal úruva şúlenen,
lá ve qui turilde i queni nu ortírielda, mal návenen *epemmali i lámáren.
Ar íre i Héra Mavar nauva apantaina, camuvalde i alcarinqua ríe ya ua fifírula.
Mí imya lé, a nessar, á panya inde nu i amyárar. Mal á cole ve quilta, illi mici le, naldie sámo, quén i exenna, an Eru tare i turquimannar, mal quenin i nucumixer antas lisse.
Etta nucuma inde nu Eruo taura má, carieryan le arte íre i lúme tule.
Hata ilya caurelda senna, an nalde valde sen.
Hepa laicelda, na cuive! Ñottolda, i Arauco, vanta sir yo tar cestala quelli i polis ammate.
Mal tara senna, tulce i saviesse, istala i perpere otornasselda i imye urdier i quanda mardesse.
Mal, íre asámielde urdie ter şinta lúme, i Aino ilya Erulisseva, ye le-yalle oialea alcareryanna Hristonen, immo envinyatuva le. Le-caruvas tulce, le-caruvas polde.
Na sen i melehte tennoio, násie!
Ter Silváno, ye notin ve voronda háno, etécien mance quettalissen, hortala ar *vettala i si ná i nanwa lisse Eruo; tara tulce sasse.
Ye ea Vavelde, cílina aselde, *suila le, ar sie care Marco yondonya.
Á *suila quén i exe miquenen melmeva! Nai illi mici le samuvar raine Hristosse!
Símon Péter, Yésus Hristo mól ar apostel, innar acámier savie ve mirwa ve véralma, i faliénen Ainolvo ar *Rehtolvo, Yésus Hristo.
Nai samuvalde Erulisse ar raine úvesse, i istyanen pa Eru ar Yésus Hristo Herulva,
pan valaina túreryanen ánies ven ilqua ya tulya coivienna ar *ainocimienna, i istyanen pa ye vi-yalle alcarnen ar mariénen.
Sine natinen ánies ven i mirwe ar antúre vandar, samieldan ranta valaina nasseo, apa uşie i hastie ya ea i mardesse mailenen.
Sina castanen, ilya hormenen, alde napane savieldan márie, márieldan istya,
istyaldan *immoturie, *immoturieldan voronwie, voronwieldan *ainocimie,
*ainocimieldan hánonilme, hánonilmeldan melme.
An qui nati sine ear lesse úvesse, pustuvalte le návello ú cardaron hya yáveo apa istie Yésus Hristo Herulva.
An qui nati sine uar ea mi quén, náse *cénelóra hya *şintacénea, ar ataltie enyalieryallo manen anes poitaina úcareryallon andanéya.
Etta, hánor, cara ilqua ya polilde carien tulca yalielda ar cilmelda, an sie cárala laume lantuvalde.
Sinen landa fenna nauva len letyaina mir i oira aranie Herulvo ar *Rehtolvo, Yésus Hristo.
Etta illume le-tyaruvan enyale nati sine, ómu istaldet ar nar tulce i nanwiesse ya ea lesse.
Mal íre en ean mi *lancoa sina notin vanima hortie le, tyariénen le enyale.
An istan i *lancoanya rato nauva panyaina oa, ve yando Yésus Hristo Herulva nin-carne sinwa.
Etta, ilya hormenen, caruvan ya ece nin, polieldan enyale nati sine apa autienya.
Íre nyarnelme len pa i túre ar i tulie Yésus Hristo Herulvo, ualme enquete fincave autaine nyariéli; úsie, anelme astarmoli ar cenner melehterya vére hendulmanten.
An Eru Atarello camnes laitie ar alcar, íre síti quettali náner cóline senna lo i Meletya Alcar: «Si ná yondonya, meldanya, pa ye inye asánie mai.»
Ar quettar sine hlasselme cóline menello íre anelme óse i aire orontesse.
Etta samilve i quetta i *Erutercánoron carna ambe tulca, ar cáralde mai cimiesse sa, ve mo cime calma caltala morna nómesse, tenna aure tule ar auretinwe orta endaldasse.
An or ilqua istalde i ua ea apacen i Tehtelesse ya tule aiqueno véra tercenello.
An apacen úne oi tulúna atano nirmenen, mal atalli caramper Erullo, nála cóline Aire Feanen.
Mal enger yando *hurutercánoli imíca i lie, ve euvar hurupeantalli mici lé. Té quildave tulúvar minna nancárala *hurupeantiéli ar laluvar yando i Heru ye mancane te insen, túlula linta nancarie intenna.
Ente, rimbali hilyuvar cardaltar lehta lengiéva, ar ténen i malle nanwiéva nauva naiquétina.
Nála milce, le-*yuhtuvalte véra *ñételtan, húrala quettalínen. Mal námielta vanwiello ua leve lencave, ar nancarielta ua lorna.
An Eru ua pustanexe paimetiello i vali i úcarner, mal te-hante mir i undume ar antane te olla saptalin huinéva náven sátine námien.
Ente, uas pustanexe paimetiello i enwina mar, mal hempe Noha, tercáno failiéva, mi varnasse as hyane otso ire tulunes oloire mardenna olce quenion.
Ente, namnes i ostu Sorom yo Omorra vistiénen tu mir asto, cárala raxetanwa olce quenin túlala randalissen.
Mal etelehtanes faila Lót, ye náne ita *tarastaina i lehta lengiénen ion tarner şanyenna.
An *şanyelóre cardaltainen, sana faila nér náne ñwalyaina faila fearyasse yainen cennes ar hlasses íre marnes imíca te.
Sie i Héru ista etelehta Erunildar et şangiello, mal sate úfailar i auren namiéva. Tá nauvalte aucirne,
or ilqua i queni hilyala hráve mérala vahta sa, ar i nattirir hére.Canye, *nirmunque, ualte pale epe alcarinquar, mal naiquetir,
ómu vali, i nar polde ar taure lá té, uar quete narace quettar namiéva ana te epe i Héru.
Mal queni sine, ve celvar ú handeo – nóne náven mapaine ar nancarne – nauvar hastaine véra hastieltanen, i natinen pa yar istalde munta ar yannar naiquetilte,
cárala ulco inten ve *paityale *úfailien. Ya notilte alasse ná marie úvesse. Nalte mordoli ar vaxeli, hírala úvea alasse húrala peantieltassen íre cariltexer merye aselde.
Hendultat illume cestat *imbacinde; ualte pole hepe inte úcarello, ar tyarilte iltance queni ranya. Endalta apárie maile. Húne hínali,
hehtala i téra malle, anaielte tulyaine mir ránea tie. Ihílielte tierya Valam Veorion, ye méle i *paityale uscaren,
mal camne *tulcarpie pa véra şanyeracierya: Ómalóra celva cólava, carpala atano ómanen, pustane i úhandea lenda i *Erutercáno.
Neri sine nar ehteléli ú neno, híşiéli cóline raumonen, ar tien i huine morniéva ná sátina.
An quetilte hoe quettali yar carir munta mára, ar i hráveo írinen ar lehte haimalínen tyarilte ranya i *şintanéya úşer i quenillon ion lengie ná ránea.
Ómu antalte tien vandali pa lérie, té nar *hastaleo móli. An ye ná turúna lo exe, ná sina exeo mól.
An qui, apa uşie i mardo vaxellon, istyanen pa i Heru ar *Rehto Yésus Hristo, nalte ata rembine lo nati sine ar nar turúne, tá i métime nattor nar ulce tien lá i minye.
An qui ualte oi sinte i tie failiéva, tá náne tien arya lá querie inte oa sallo apa istie i aire axan antaina tien.
I nanwa equesso quetie ten-amartie: «I huo enutúlie véra *quamnaryanna, ar i polca ya náne sóvina, pelienna i luxusse.»
Meldar, tenna sina ná i attea ya técan lenna, yasse, ve minyanyasse, valtan sámalda cuiva handenna *rentiénen le,
enyalieldan i quetier nóvo quétine lo i airi *Erutercánor ar i axan i Heruo ar *Rehto ter i aposteli mentaine lenna.
An istalde si or ilqua, i mí métime auri tuluvar *naiquétoli quétala yaiwe, lengala ve vére íreltar
ar quétala: «Masse sina entulesse pa ya ánes vanda? An atarilmar nar qualini, mal ilqua ná ena ve anaies ontiéno yestallo.»
Véra íreltanen ualte tunta si, i enget menel cemenye vanwiesse, ortala et i nenillon ar endesse neno Eruo quettanen,
ar tane neninen i enwina mar náne nancarna, nála oloiyaina nennen.
Mal i imya quettanen i menel cemenye yat eat sí nát martyaine ruiven ar nát sátine i auren námiéva, yasse atani cotye Erun nauvar nancarne.
Mal a meldar, áva loita tunta si: I Hérun erya ré ná ve loar húme, ar loar húme ve erya ré.
I Héru ui lenca *amaquatiesse vandarya, ve quelli notir lencie, mal tanas cóle len, pan uas mere i aiquen nauva nancarna, mal i illi hiruvar inwis.
Ono i Heruo aure tuluva ve arpo, yasse menel autuva túra ramesse, mal i meneldie hrondor *ticuvar rúcima úresse, ar cemen ar yar anaier cárine sasse nauvar apantaine.
Pan ilye nati sine ticuvar oa, á hanya mana i nostale quenion ya mauya len náve, airi cardalínen ar lengiénen ar carielínen ainocimiéva,
íre yétalde ompa i Heruo aurenna ar hortar i tulie enta aureo, yanen menel, nála ruivesse, tique ar i meneldie hrondor tiquir oa rúcima úresse.
Mal vandaryanen yétalve ompa vinya menelenna ar vinya cemenna, yatse failie maruva.
Etta, meldar, pan yétalde ompa sine natinnar, cara ilqua ya polilde náven hírina ú vaxeo hya mordo, ar rainesse.
Ente, nota Herulvo cóle ve ecie rehtiéva, aqua ve Paulo, melda hánolva, yando tence len i sailiénen antaina sen,
carpala pa nati sine ve caris ilye tennaryassen. Mal mi tai ear natali yar nar urde hanien, yar i úpeantainar ar i iltancar rícar, ve yando ricilte i hyane tehteler, véra nancarieltan.
Etta, a meldar, pan elde istar si nóvo, cima i ualde tulyaine oa aselte i loimanen ion tarir i Şanyenna, ar lantar véra voronweldallo.
Mal ala i Erulissenen ar i istyanen pa Herulva ar *Rehtolva, Yésus Hristo. Na sen i alcar, sí ar oireo auresse véla.
Ya enge i yestallo, ya hlasselme, ya ecénielme hendulmanten, ya eyétielme ar málmat paltanet pa i quetta coiviéno
– é i coivie náne apantaina, ar ecénielmes ar *vettalme ar nyarilme len pa i oira coivie ya enge as i Atar ar náne apantaina men –
ta ya ecénielme ar hlasselme nyáralme yando len, i yando lé samuva ranta aselme. Ar menya ranta ná as i Atar ar as Yésus Hristo Yondorya.
Ar sie técalme nati sine, i alasselma nauva quanta.
Ar si ná i menta ya ahlárielme sello ar nyáralme len, i Eru cala ná, ar mornie laume ea sesse.
Qui quetilve i samilve ranta óse, ananta vantalve i morniesse, húralve ar uar care i nanwie.
Ono qui vantalve i calasse tambe sé ea i calasse, samilve ranta quén as i exe, ar i serce Yésus Yondoryo vi-sove ilya úcarello.
Qui quetilve i ualve same úcare, *útulyalve inwe ar i nanwie lá ea vesse.
Qui *etequentalve úcarelvar, náse voronda ar faila apsenien ven úcarelvar ar vi-sovien ilya *úfailiello.
Qui quetilve i ualve úacárie, carilves *hurindo, ar quettarya lá ea vesse.
Hinyar, técan lenna nati sine i ualde úcaruva. Ar qui aiquen é úcaruva, samilve şámo as i Atar: Yésus Hristo, i Faila.
Ar náse tópala *yanca úcarelvain, ananta lá *venyain erinque, mal i quanda mardo.
Ar sie samilve i istya i istalve sé: qui himyalve axanyar.
Ye quete: ”Istanyes”, mal axanyar uas himya, sé *hurindo ná, ar i nanwie lá ea mi síte quén.
Mal aiquen ye himya quettarya, mi sé i melme Eruo ná carna ilvana. Sinen istalve i nalve sesse.
Yen quete i náse sesse mauya vantie ve sé vantane.
Meldar, uan tece lenna vinya axan, mal yára axan ya sámelde i yestallo. Yára axan sina ná i quetta ya hlasselde.
Ata, tecin lenna vinya axan, ya ná nanwa sen ar len, pan i mornie auta, ar i nanwa cala caltea yando sí.
Ye quete i eas i calasse, ananta yeltas hánorya, ea en i morniesse.
Ye mele hánorya lemya i calasse, ar sesse ea munta ya tyare aiquen lanta.
Mal ye yelta hánorya ea i morniesse ar vanta i morniesse, ar uas ista yasse ménas, pan i mornie acárie henyat *cénelóre.
Técan lenna, híni, pan úcareldar nar apsénine len esseryanen.
Técan lenna, atari, pan istalde ye ea i yestallo. Técan lenna, nessar, pan *orutúrielde i Olca. Técan lenna, híni, pan istalde i Atar.
Técan lenna, atari, pan istalde ye ea i yestallo. Técan lenna, nessar, pan nalde polde ar Eruo quetta lemya lesse, ar *orutúrielde i Olca.
Áva mele i mar hya yar ear i mardesse. Qui aiquen mele i mar, i melme i Atarwa lá ea sesse,
pan ilqua ya ea i mardesse – i hráveo maile ar i henduo maile ar i *valate tyárina almanen – ui i Ataro, mal i mardo.
Ar i mar auta, ar sie care mailerya, mal ye care Eruo indóme lemya tennoio.
Hinyar, si ná i métima lúme, ar ve ahlárielde i túla i *Anahristo, yando sí ear rimbe *Anahristoli. Sie istalve i si ná i métima lúme.
*Etemennelte vello, mal úmelte nostalelvo, an qui anelte nostalelvo, lemyanelte aselve. Mal martanes i umne tanaina i umilte illi nostalelvo.
Ar lé samir *livyale i Airillo; illi mici le samir istya.
Tecin lenna, lá pan ualde ista i nanwie, mal pan istaldes, ar pan lá ea huru ya tule i nanwiello.
Man ná i *hurindo, qui lá ye lala i Yésus ná i Hristo? Si ná i *Anahristo, sé ye lala i Atar yo i Yondo.
Ilquen ye pene i Yondo yando pene i Atar. Ye *etequenta i Yondo *etequenta yando i Atar.
Lé, lava yan ahlárielde i yestallo lemya lesse. Qui ya ahlárielde i yestallo lemya lesse, yando lé lemyuvar mí Yondo yo i Atar.
Ar si ná i vanda ya antanes ven: oira coivie.
Nati sine tecin lenna pa i nevir *útulya le.
Ar lé – i *livyale ya camnelde sello lemya lesse, ar ualde same maure i aiquen peanta len. An sá peanta len pa ilqua, ar nas nanwa ar pen huru. Ve sá peánie len, á lemya sesse.
Ar sí, hinyar, alde lemya sesse, i, íre nauvas apantaina, samuvalve lérie quetiéva ar lá nauvalve naityane oa sello tulesseryasse.
Qui istalde i náse faila, istalde i ilquen ye care failie anaie nóna sénen.
Yé i nostale melmeo ya i Atar ánie ven, i umnelve estaine Eruhíni, ar ta nalve. Etta i mar ua ista ve, pan sé uas isintie.
Meldar, sí nalve Eruhíni, mal en ua apantaina ya nauvalve. Istalve i íre sé nauva apantaina nauvalve ve sé, an cenuvalves ve náse.
Ar ilquen ye same estel sina sesse, poita immo ve sé ná poica.
Ilquen ye care úcare yando care *şanyelórie, ar úcare ná *şanyelórie.
Ar istalde i sé náne apaintaina mapien oa úcari, ar lá ea úcare sesse.
Ilquen lemyala sesse ua úcare; *úquen ye úcare var ecénie se var isintie se.
Hinyar, lava úquenen *útulya le; ye care failie faila ná, tambe sé ná faila.
Ye care úcare ho i Arauco ná, pan i Arauco úacárie i yestallo. Casta sinan i Eruion náne apantaina: nancarien i Arauco cardar.
Ilquen ye anaie nóna Erullo ua care úcare, pan erderya lemya sesse, ar uas pole úcare, pan anaies nóna Erullo.
I híni Eruo ar i híni i Arauco nar apantaine sinen: Ilquen ye ua care failie ui Eruo, yando lá ye ua mele hánorya.
An si ná i menta ya ahlárielde i yestallo: samie melme quén i exeva,
lá ve Cain, ye náne i Olco ar nahtane hánorya. Ar mana i casta yanen se-nahtanes? Pan vére cardaryar náner ulce, mal hánoryo cardar faile.
Áva fele elmenda, hánor, qui i mar yelta le.
Istalve i oantielve ñurullo coivienna, pan melilve i hánor. Ye ua mele lemya ñurusse.
Ilquen ye yelta hánorya *atannahtar ná, ar istalde i lá ea *atannahtar ye same oira coivie lemyala sesse.
Sinen istalve melmerya, pan sé antane coivierya elven, ar ná rohtalva antie coivielva hánolvain.
Mal ye ná lára mar sinasse ar cene hánorya arwa maureo, ananta holtas i fenna *ofelmeryo sen – manen Eruo melme lemya sesse?
Hinyar, alve mele, lá quettanen hya lambanen, mal cardanen ar nanwiénen.
Sinen istuvalve i nalve i nanwiéno, ar panyuvalve endalva séresse epe se.
An qui endalva *ulquete ve, Eru ná túra lá endalva ar ista ilye nati.
Meldar, qui endalva ua *ulquete ve, samilve lérie quetiéva epe Eru,
ar *aiqua ya arcalve camilve sello, pan himyalve axanyar ar carir yar nar máre henyant.
Ar si ná axanya, i savilve mí esse Yésus Hristo Yondoryo ar melir quén i exe, ve cannes ven.
Ente, ye himya axanya lemya sesse, ar sé immo i exesse, ar sie istalve i lemyas vesse, i fairenen ya ven-antanes.
Meldar, áva save ilya fairesse, mal á tyasta i fairi cenien qui nalte Eruo, pan rimbe *hurutercánoli *eteménier mir i mar.
Sinen istalde Eruo faire: Ilya faire ye *etequenta Yésus Hristo túlienwa i hrávesse ná Eruo,
mal ilya faire ye ua *etequenta Yésus Hristo ui Eruo. Ar si ná i *Anahristo faire, pa ya hlasselde i anes túlala, ar yando sí eas i mardesse.
Lé nar Eruo, hinyar, ar *orutúrielde te, pan ye ea lesse ná túra lá ye ea i mardesse.
Té nar i mardo; etta quetilte i mardo lénen, ar i mar lasta téna.
Vé nar Eruo.Ye ista Eru lasta venna; ye ui Eruo ua lasta venna. Sie istalve i faire nanwiéno ar i faire huruo.
Meldar, alve mele quén i exe, pan melme ná Eruo, ar ilquen ye mele ná nóna Eruo ar ista Eru.
Ye ua mele ua ista Eru, pan Eru ná melme.
Sinen Eruo melme náne apantaina imíca ve: Eru mentane *ernóna Yondorya mir i mar, i pollelve same coivie sénen.
Melme ná apantaina mi si, lá i elve emélier Eru, mal i sé méle vé ar mentane Yondorya ve tópala *yanca úcarelvain.
Meldar, qui Eru vi-emélie sie, rohtalva ná i yando vé melir quén i exe.
*Úquen oi ecénie Eru. Qui melilve quén i exe, Eru lemya vesse ar melmerya ná carna ilvana vesse.
Sinen istalve i sesse lemyalve, ar sé vesse, pan ánies ven et faireryo.
Ar elve ecénier ar *evettier i ementie i Atar Yondorya ve *Rehtando i mardo.
Aiquen ye *etequenta i Yésus Hristo i Eruion ná, sesse Eru lemya, ar sé Erusse.
Ar elve istar ar asávier i melme ya Eru same elveva.Eru ná melme, ar ye lemya melmesse lemya Erusse, ar Eru lemya sesse.
Sie melme ná carna ilvana aselve, i samuvalve lérie quetiéva i auresse anamo. An tambe sé ná, sie yando elve nar i mar sinasse.
Lá ea caure melmesse, mal ilvana melme hate caure etsenna, pan caure cole paime as imma. Ye ruce ui carna ilvana melmesse.
Elve melir, pan sé méle vé minyave.
Qui aiquen quete: ”Melin Eru”, ananta yeltas hánorya, sé *hurindo ná. An ye ua mele hánorya ye ecénies, ua pole mele Eru ye uas ecénie.
Ar axan sina samilve sello, i mauya yen mele Eru mele yando hánorya.
Ilquen ye save i Yésus ná i Hristo ná nóna Eruo, ar ilquen ye mele i Atar meluva ye náne nóna sénen.
Sinen istalve i melilve i Eruhíni, íre mélalve Eru ar cárar axanyar.
An si ná i melme Eruva, i himyalve axanyar. Ar axanyar umir lumne,
pan ilqua ya ná nóna Eruo *orture i mar. Ar si ná i apaire ya *orutúrie i mar: savielva.
Man ná ye *orture i mar qui lá ye save i Yésus ná i Eruion?
Si ná ye túle nennen ar sercenen, Yésus Hristo, lá i nennen erinqua, mal i nennen ar i sercenen. Ar i faire ná ya *vettea, pan i faire nanwie ná.
An ear nelde yar *vettar,
i faire ar i nén ar i serce, ar nelde sine nar er.
Qui camilve i *vettie Atanion, Eruo *vettie ambe túra ná. An si ná i *vettie Eruo, i *evetties pa Yondorya.
Ye save mí Eruion harya i *vettie immosse. Ye ua save Erusse acárie se *hurindo, pan uas asávie i *vettie ya sé ánie pa Yondorya.
Ar si ná i *vettie, i Eru antane ven oira coivie, ar coivie sina ea Yondoryasse.
Ye same i Yondo same i coivie; ye ua same i Eruion ua same i coivie.
Tecin lenna nati sine i istuvalde i samilde oira coivie, lé i savir mí esse i Eruiono.
Ar si ná i lérie quetiéva ya savilve senna, i qui arcalve *aiqua ve indómerya, hlaris ve.
Ente, qui istalve i hlaris ve pa *aiqua ya arcalve, istalve i samuvalve i arcainar, pan arcanielvet sello.
Qui aiquen cene hánorya cára úcare ya ua tulya qualmenna, arcuvas, ar Eru antuva sen coivie – qui uas mici queni ion úcare tulya qualmenna. Eä úcare ya tulya qualmenna; uan quete i mo arcuva pa ta.
Ilya *şanyelórie úcare ná, ananta ea úcare ya ua tulya qualmenna.
Istalve i aiquen nóna Eruo ua úcare, mal i Quén ye náne nóna Eruo himya se, ar i Olca ua appa se.
Istalve i nalve Eruo, mal i quanda mar caita i túresse i Olco.
Ar istalve i utúlie i Eruion, ar ánies ven hande istien sé ye şanda ná, ar ealve yesse şanda ná, Yondorya Yésus Hristonen. Sé ná i nanwa Aino ar oira coivie.
Hinyar, *hepa inde cordonillon!
I amyára i cílina herinna ar hínaryannar, i melin nanwiesse, ar lá inye erinqua, mal yando illi i istar i nanwie,
i nanwiénen ya ea vesse, ar euvas aselve tennoio.
Euvar aselve lisse, óravie ar raine ho Eru i Atar ar ho Yésus Hristo, i Ataro Yondo, nanwiénen ar melmenen.
Samin i antúra alasse pan ihírien i queneli hínalyaron vantar i nanwiesse, ve i axan ya camnelve i Atarello.
Ar sí arcan lyello, a heri, lá ve qui técan lyen vinya axan, mal ta ya sámelve i yestallo: i melilve quén i exe.
Ar si ná i melme: i vantalve ve axanyar. Ta ná i axan, ve ahlárielde i yestallo, vantien sasse.
An rimbe *útulyandoli *eteménier mir i mar, i uar *etequenta Yésus Hristo túlienwa i hrávesse. Si ná i *útulyando ar i *Anahristo.
Tira inde, i yar acárielve uar nauva len vanwe, mal camuvalde quanta *paityale.
Ilquen ye ua lemya Hristo peantiesse, mal mene han sa, ua same Eru. Ye lemya i peantiesse, sé same i Atar yo i Yondo.
Qui aiquen tule lenna lá túlula peantie sina, ávase came mir coaldar, ar áva *suila se.
An ye se-suila same ranta óse ulce cardaryassen.
Samin rimbe natali tecien lenna, mal uan mere care sie hyalinnen ar móronen. Ono samin i estel i tuluvan lenna ar quetuva aselde cendelello cendelenna, i alasselda nauva ilvana.
Cílina néşalyo hínar *suilar lye.
I amyára Aio i meldanna, ye inye mele nanwiesse.
Melda, hyamin i ilye natissen nalye almárea ar samil mále, tambe fealya almárea ná.
An sámen i antúra alasse íre hánoli túler ar *vettaner pa i nanwie ya ea lyesse, ar manen vantal i nanwiesse.
Uan same casta hantalen túra lá si: i hlarin i hínanyar vantar i nanwiesse.
Melda, cáral voronda molie ilquasse ya caril i hánoin, yando etteleain,
i *evettier pa melmelya epe i ocombe. Iquin, áte menta oa lénen valda Erun.
An rá esseryan *etemennelte, lá mápala *aiqua quenillon nórion.
Etta mauya ven came taiti queni, i moluvalve uo i nanwien.
Tencen quettali i ocombenna, mal Liotréfes, ye mele náve minda mici te, ua came *aiqua mello.
Etta, qui tuluvan, tyaruvanyet enyale cardaryar yar cáras, *cacarpala pa me ulce quettalínen. Ente, pan ta ui fárea sen, yando váquetis came i hánor, ar i merir camitat nevis pusta ar hatitat et i ocombello.
Melda, á care, lá ve ya ulca ná, mal ve ta ya ná mane. Ye care mane Eruo ná. Ye care ulco ua ecénie Eru.
Lemetrius same mára *vettie ho illi mici te, ar ho i nanwie imma. É yando elme *vettear, ar istalye i menya *vettie nanwa ná.
Sámen rimbe natali tecien lyenna, ananta uan mere tece lyenna móronen ar tecilden.
Mal samin i estel i cenuvanyel vérave, ar quetuvangwe cendelello cendelenna. Raine na lyen! I meldor lye-*suilar. Á *suila i meldor, ilya quén essenen.
Yúr, Yésus Hristo mól, mal Yácovo háno, i yálinannar i nar méline lo Eru i Atar ar hépine Yésus Hriston:
Nai samuvalde óravie ar raine ar melme úvesse!
A meldar, ómu ilya hormenen mernen tece lenna pa i rehtie yasse lé ar ní véla samir ranta, mauyane nin tece lenna hortien le: Á mahta i savien ya mi lú er ar illumen anaie antaina i airin!
An quelli fincave utúlier minna, i andanéya náner sátine námien lo i Tehtele – olcali, i querir i Erulisse mir ecie lehta lengiéno, ar lalar erya Cánolva ar Herulva, Yésus Hristo.
Merin *renta le, ómu istalde ilqua er lú illumen: I Héru, apa rehtie lie et Mirrandorello, epeta nancarne i uar sáve.
Ar i vali i uar himyane i meni yar sámelte i yestasse, mal hehtaner véra nómelta, asáties oialie nótelínen nu huine i námien mí túra aure.
Sie yando Sorom yo Omorra ar i ostor *os tú nar panyaine epe vi ve tanwa raxeo, an *úpuhtanelte ar sámer maile ettelea hráveva, ar sí acámielte i paime oialea náreva.
Sie yando sine oloste queni vahtar i hráve ar querir oa hére, naiquétala alcarinquannar.
Mal íre Mícael i Héra Vala costane as i Arauco ar sáme cos pa hroarya Móses, uas veryane quete senna yaiwenen, mal eques: «Nai i Héru *tulcarpuva lyenna!»
Ananta neri sine naiquetir ilye i natinnar yar ualte hanya, mal i nati yar é hanyalte – nasseltanen, ve celvar ú handeleo – tainen hastaltexer.
Horro tien, an avantielte mi tierya Cain, ar mir loimarya Valam hanteltexer, camien *paityale, ar nanacárielte inte mi amortierya Córa.
Té nar i ondor nurtaine nu nén merendildassen melmeva, mavalli i maitar inte ú caureo, lumbuli ú neno, cóline sir yo tar súrinen, aldali mi quelle, ú yáveo, lú atta quálienwe, túcine ama as şundultar,
hravani falmali i earo, falastala vére castaltainen náven *naityana, ránie tinweli, in i huine morniéva ná sátina tennoio.
É i otsea i ontalesse Atanello, Énoc, quente apacen pa té, quétala: «Yé! I Héru túle as airi *quaihúmeryar,
carien námie ilinnar, ar *tulcarpien ilye i olcannar pa ilye olce cardaltar yar acárielte olcave, ar pa ilye i naiquetier yar i olce úcarindor equétier senna.»
Queni sine nar nurrula, quétala ulco pa umbartalta, vantala vére íreltassen, ar antoltar quetir hoe quettali; arwe lisso lambaltasse carpalte exennar véra *ñételtan.
Mal elde, a meldar, enyala i quetier quétine nóvo lo Yésus Hristo Herulvo aposteli,
manen quentelte lenna: «I métima lúmesse tuluvar quelli quétala yaiwe, vantala vére íreltassen olce nativa.»
Té nar i tyarir şancier, neri ve celvar, pénala faire.
Mal elde, a meldar, á carasta inde amaire savieldanen, ar hyama Aire Feanen.
Sie hepa inde Eruo melmesse, íre yétalde ompa Yésus Hristo Herulvo óravienna, camien oira coivie.
Ente, alde órava i quenissen ion savie ná iltanca;
áte rehta *rapiénen te et i ruivello. Sine quenissen órava, mal cima, yeltala yando lanne vahtaina i hrávenen.
Yen pole tire le lantiello ar panya le alahaste epe alcarerya arwe túra alasseo,
erya Erun, yen vi-rehta ter Yésus Hristo Herulva, na alcar, melehte, túre ar hére ter i quenda vanwa oiale ar sí ar tennoio! Násie!
Yésus Hristo apantie, ya Eru sen-antane, tanien núroryain yar rato martuvar. Mentanes valarya ar sa-carne sinwa tanwalínen Yoháno núroryan,
ye *vettane pa i quetta Eruo ar i *vettie pa Yésus Hristo, ilqua ya cennes.
Valima ná ye et-henta, ar valime nar i hlarir i quettar i apacéno ar i cimir yar nar técine sasse, an i lúme hare ná.
Yoháno i ocombennar otso Ásiasse: Lisse ar raine len ho ye ea ar ye enge ar ye tuluva, ar i fairellon otso i ear epe mahalmarya,
ar Yésus Hristollo, i Voronda *Vetto, i Minnóna Qualinillon, ar i Tur aranion cemeno. Yen ve-mele ar vi-elérie úcarilvallon véra serceryanen
– ar vi-carne aranie, airimóli Ainoryan ar Ataryan – na sen i alcar ar i túre tennoio! Násie!
Yé! Túlas as i fanyar, ar ilya hen cenuva se, yando i se-*terner; ar ilye nossi cemeno palpuvar inte nyéresse sénen. Ná, násie!
“Inye Tinco ar Úre,” quete i Héru Eru, “i ea ar i enge ar i tuluva, i Iluvala.”
Inye Yoháno, hánolda ye same ranta aselde i şangiesse ar araniesse ar voronwiesse mi Yésus, enge i tollosse estaina Patmos quetiénen pa Eru ar *vettiénen pa Yésus.
I Héruo auresse anen fairesse, ar hlassen ca ni taura óma ve hyólallo,
quétala: “Teca ya cenil parmasse ar ása menta i ocombennar otso, mi Efesus ar Mirna ar Peryamum ar Şiatíra ar Sardis ar Filarelfia ar Laoricea.”
Quernen immo cenien mano óma quente ninna, ar apa querie cennen calmatarmar otso maltava,
ar i endesse i calmatarmaron quén ve atanyondo, vaina vaimasse ya rahtane talyanta ar arwa laurea quilto nútina *os i ambas.
Ente, carya ar findelerya náner ninqui ve ninque tó, ve losse, ar henyat ve uruite ruine.
Talyat nánet ve laurea urus, lúşina urnasse, ar ómarya né ve i lamma rimbe nenion.
Formaryasse sámes tinwi otso; et antoryallo lende aica, *yucílea andamacil. Cendelerya calle ve íre Anar cala túreryasse.
Íre cennenyes lantanenye ve qualin epe talyat. Panyanes formarya nisse ar quente: “Áva ruce! Inye i Minya ar i Métima
ar i coirea ná; ar anen qualin, mal yé! nanye coirea tennoio ar oi, ar samin i *latili ñuruo ar Mandosto.
Etta teca yar ecéniel, yúyo yar ear sí ar yar martuvar apa si.
Pa i fóle i tinwion otso yar cennel formanyasse, ar i calmatarmaron otso: i tinwi otso nar valali i ocombion otso, ar i calmatarmar otso nar i ocombi otso.”
“I valanna i ocombeo Efesusse teca: Sin quete ye same i tinwi otso formaryasse, ye vanta endesse i calmatarmaron otso maltava:
Istan cardalyar ar molielya ar voronwielya, ar i ual pole cole ulce queni, ar i atyastiel i quetir i nalte aposteli, mal uar, ar hirnelyet *hurindoli.
Ente, samil voronwie, ar *terocóliel essenyan, ar ual sí lumba.
Ananta samin lyenna i oantiel i melmello ya sámel i yestasse.
Etta enyala yallo alantiel, ar hira inwis ar cara i minye cardar. Qui lalye, túlan lenna, ar mapuvan oa calmatarmalya nómeryallo, qui ual hire inwis.
Ananta é samil si, i tevil i cardar i Nicolaiyaron, yar yando inye teve.
Nai ye same hlas hlaruva ya i Faire quete i ocombennar: Yen same i apaire antuvan mate i Aldallo Coiviéva, ya ea i tarwasse Eruva.
Ar i valanna i ocombeo Mirnasse teca: Sin quete i Minya ar i Métima, ye náne qualin ar nanwenne coivienna:
Istan şangielya ar únielya – mal nalye lárea – ar i yaiwe ion estar inte Yúrar, ananta ualte, mal nar Sátano *yomencoa:
Áva ruce i ñwalmallon yar sí tuluvar lyenna. Yé! I Arauco sí hatuva queneli mici le mir mando, i nauvalde aqua tyastaine, ar i samuvalde şangie ter rí quean. Na voronda tenna ñuru, ar lyen-antuvan i ríe coiviéva.
Nai ye same hlas hlaruva ya i Faire quete i ocombennar: Ye same apaire laume nauva malaina i attea ñurunen.
Ar i valanna i ocombeo Peryamumesse teca: Sin quete ye same i aica *yucílea andamacil:
Istan masse marilye, yasse ea i mahalma Sátanwa; ananta hépal essenya. Ual lalane savielya nisse yando mi réryar Antipas voronda *vettonya, ye náne nanca ara lye, yasse mare Sátan.
Ananta samin mance nattoli lyenna, i ear aselye i hepir i peantie Valamo, ye peantane Valacen panie *lantasar epe Israelindi, i mantelte yar náner *yácine cordollin ar *hrupuhtaner.
Sie ear yando aselye i hepir i peantie i Nicolaiyaron.
Etta hira inwis! Qui lalye, tuluvan lyenna rato, ar mahtuvan téna antonyo macilden.
Nai ye same hlas hlaruva ya i Faire quete i ocombennar: Yen same apaire antuvan i nurtaina máno, ar sen-antuvan ninque sar, ar i sardesse vinya esse ya *úquen ista, hequa ye came sa.
Ar i valanna i ocombeo Şiatírasse teca: Sin quete i Eruion, ye same henyat ve uruite ruine, ar talyat ve urus:
Istan cardalyar ar melmelya ar savielya ar *veumelya ar voronwielya, ar anvinye cardalyar nar ambe lá i minyar.
Ananta samin lyenna i ual pusta i nís Yesavel, ye esta immo *Erutercáne, ar peantas ar tyaris ranya núronyar mir carie *úpuhtale, ar matie nati *yácine cordollin.
Antanen sen lúme hirien inwis, mal uas mere cesta inwis pa *hrupuhtalerya.
Yé! Hatuvanyes mir caima, ar i *hrupuhtar óse mir túra şangie, qui ualte hire inwis pa cardaryar.
Ar hínaryar lavuvan ñurun mapa, i istuvar ilye i ocombi i inye ná ye ceşe sámar ar endar, ar antuvan ilquenen mici le ve cardaldar.
Mal quetin i exennar mici le Şiatírasse, illinnar i uar same peantie sina ar uar isintie 'i tumne nati Sátano', ve quetilte: Uan panya lesse hyana cólo.
Ananta hepa ya samil, tenna tuluvan.
Ar ye same apaire ar cime cardanyar tenna i metta – antuvan sen hére or nóri.
Turuvas tai angaina vantilden, návelte rácine mittannar ve cemne veni – i imya hére ya inye acámie Atarinyallo.
Ar antuvan sen i arintinwe.
Nai ye same hlas hlaruva ya i Faire quete i ocombennar!”
“Ar i valanna i ocombeo Sardisse teca: Sin quete ye same i fairi otso Eruva ar i tinwi otso: Istan cardalyar, i nalye coirea essenen – mal nalye qualin.
Na cuiva ar á *torya i nati lemyala yar nar hari qualmenna, an uan ihírie cardalyar telyaine epe Ainonya.
Etta enyala manen acámiel ar manen hlassel, ar ása hepe ar hira inwis! Qui ual nauva sí aqua cuiva, tuluvan ve pilu, ar laume istuval i lúme ya tuluvan lyenna.
Ananta é samil mance esseli Sardisse i uar avahtie lanneltar, ar vantuvalte asinye mi ninque, an nalte valde.
Ye same apaire nauva sie vaina ninqui lannelínen, ar laume mapuvan oa esserya i Parmallo Coiviéva, mal *etequetuvan esserya epe Atarinya ar epe valaryar.
Nai ye same hlas hlaruva ya i Faire quete i ocombennar!
Ar i valanna i ocombeo Filarelfiasse teca: Sin quete ye aire ná, ye şanda ná, ye same Laviro *latil, ye latya, i *úquen holtuva, ar holta, i *úquen latyuva:
Istan cardalyar – yé! apánien epelye latyaina fenna, ya úquen pole holta. An elye same pitya melehte, mal hempel quettanya ar ua lalane essenya.
Yé! I queni *yomencoallo Sátanwa, i estar inte Yúrali ananta umir, mal húrar – yé! tyaruvanyet tule ar lanta undu epe talulyat, ar te-tyaruvan ista i emélienyel.
Pan ehépiel i quetta voronwienyo, yando inye lye-hepuva i lúmello tyastiéva ya tuluva i quanda ambarenna, tyastien i marir cemende.
Tuluvan rato. Hepa ya samil, pustien aiquen mapiello ríelya.
Ye same apaire caruvan tarma i cordasse Ainonyava, ar laume oi etelelyuvas, ar tecuvan sesse i esse Ainonyava ar i esse i osto Ainonyava, i vinya Yerúsalem ya tule undu menello Ainonyallo, ar yando ninya vinya esse.
Nai ye same hlas hlaruva ya i Faire quete i ocombennar!
Ar i valanna i ocombeo Laoriceasse teca: Sin quete i Násie, i voronda ar şanda *Vetto, i Yesta ontiéno Eruo:
Istan cardalyar, i ual ringa hya lauca. Merin i anel ringa hya lauca!
Etta, pan nalye *pellauca, ar lá lauca hya ringa, quamuvanyel et antonyallo.
Pan quetil: “Nanye lárea, nanye herenya; samin maure muntava” – mal ual ista i elye ná angayanda ar *nainima ar úna ar *cénelóra ar helda –
quetin lyenna i caril mai qui mancal nillo malta poitaina nárenen, návelyan lárea, ar ninqui larmali, návelyan vaina ar pustien heldielyo nucumnie návello tanaina, ar laive panien hendulyatse, cenielyan.
Illin i melin antan paimesta ar *tératie. Etta á mína ar hira inwis!
Yé! Táran epe i fenna, tambala. Qui aiquen hlare ómanya ar latya i fenna, tuluvan mir coarya, ar matuvan i *şinyemat óse, ar sé asinye.
Yen same apaire antuvan hame undu asinye mahalmanyasse, tambe inye sáme apaire ar hamne undu as Atarinya mahalmaryasse.
Nai ye same hlas hlaruva ya i Faire quete i ocombennar!”
Apa si cennen, ar yé! fenna latyaina menelde. Ar i minya óma ya hlassen quéta asinye náne ve i lamma hyólo, ar eques: “Tula amba sir, ar lyen-tanuvan yar rato martuvar.”
Mí imya lú túlen nu i túre i Faireo, ar yé! enge mahalma menelde, ar Quén hande i mahalmasse.
Ilce yeo hára tasse ná ve *nambíre ar *culmíre, ar *os i mahalma ea helyanwe, ve *laimaril ilcesse.
Ar *os i mahalma ear mahalmar canta *yúquean, ar i mahalmassen hárar amyárar canta *yúquean sámala ninqui larmali, ar ríeli maltava cariltasse.
Ar et i mahalmallo túlar ítali ar ómali ar hundiéli, ar ear calmar otso uryala epe i mahalma, yar nar i fairi otso Eruo.
Ar epe i mahalma ea ya *şéya ve ailin calcava.I endesse yasse i mahalma tare, *os i mahalma ear coiti canta, quante hendion epe ar cata:
i minya coite ná ve rá, i attea ve mundo, i neldea same cendele ve atano, ar i cantea ná ve vílala şoron.
Ilya quén i coition canta same rámar enque, ar ilya coite ná quanta hendelion *os quanda hroarya ar mityave. Sére ualte same, auresse yo lómisse quétala: “Aire, aire, aire ná i Héru Eru Iluvala, ye ea ar ye enge ar ye tuluva!”
Ar mí ilya lú ya i coiti antar alcar ar laitale ar hantale yen hára i mahalmasse, ye coirea ná tennoio ar oi,
i amyárar canta *yúquean lantar undu epe ye hára i mahalmasse ar *tyerir ye coirea ná tennoio ar oi, ar hatilte ríeltar epe i mahalmanna, quétala:
“Elye ná valda, Herulma ar Ainolma, camiéno i alcar ar i laitie ar i túre, an ontanelye ilye nati, ar indómelyanen engelte ar náner ontaine.”
Ar cennen i formasse yeo hande i mahalmasse parma técina mityave ar i ettesse, *lihtaina *lihtainen otso.
Ar cennen polda vala ye yáme taura ómanen: “Man ná valda pantiéno i parma ar race *lihtaryar?”
Mal *úquen menelde hya cemende hya nu cemen lertane panta i parma hya yéta minna sa.
Ar anen turúna túra yaimenen, an *úquen náne hirna valda pantiéno i parma hya yétiéno minna sa.
Mal quén i amyáraron quete ninna: “Áva na yaimea! Yé! I Rá ye ná Yehúro nosseo, Laviro şundo, asámie apaire ar lerta latya i parma ar *lihtaryar otso.”
Ar cennen tárala endesse i mahalmo ar i coition canta, ar endesse i amyáraron, eule ye sáme i ilce náveo nanca, arwa rassion otso ar hendion otso, yar nar Eruo fairi otso, i mentainar mir i quanda cemen.
Ar lendes ar nampe i parma et formallo yeo hande i mahalmasse.
Ar íre nampes i parma, i coiti canta ar i amyárar canta *yúquean lantaner undu epe i Eule, ilya quén sámala ñande ar tolpoli maltava yar nér quante *nisqueo, ya ná i hyamier i airion.
Ar lirilte vinya líre, quétala: “Nalye valda mapiéno i parma ar pantiéno *lihtaryar, an anel nanca, ar sercelyanen urúniel queneli Erun et ilya nossello ar lambello ar nórello.
Acárielyet aranie ar *airimóli Ainolvan, ar turuvalte cemende.”
Ar cennen, ar hlassen óma rimbe valalion *os i mahalma ar i coiti ar i amyárar, ar nótelta náne ve *quaihúmi *quaihúmion ar húmi húmion,
quétala taura ómanen: “I Eule ye náne nanca ná valda camiéno i túre ar alma ar sailie ar poldore ar laitie ar alcar ar aistie.”
Ar ilya onna ye ea mi menel cemenye ar nu cemen ar i earesse, ar ilye i nati tais, hlassen quéta: “Yen hára i mahalmasse ar i Eulen na i aistie ar i laitie ar i alcar ar i melehte tennoio ar oi.”
Ar i coiti canta quenter: “Násie!”, ar i amyárar lantaner undu ar *tyerner.
Ar cennen íre i Eule latyane mine i *lihtaron otso, ar hlassen quén i coition canta quéta ómanen ve hundier: “Tula!”
Ar cennen, ar yé! ninque rocco, ar ye hamne sesse sáme quinga; ar ríe náne sen antaina, ar etelendes arwa apaireo ar samien apaire.
Ar íre latyanes i attea *lihta, hlassen i attea coite quéta: “Tula!”
Ar exe ettúle, narwa rocco; ar yen hamne sesse náne antaina mapa raine oa cemello, nahtieltan quén i exe; ar haura macil náne sen antaina.
Ar íre latyanes i neldea *lihta, hlassen i neldea coite quéta: “Tula!” Ar cennen, ar yé! more rocco; ar ye hamne sesse sáme tolpot máryatse.
Ar hlassen ya náne ve óma endesse i coition canta quéta: “Lesta oriva *aurepaityalen, ar lestar nelde *findoriva *aurepaityalen, ar áva mala i millo ar i limpe.”
Ar íre latyanes i cantea *lihta, hlassen i cantea coiteo óma quéta: “Tula!”
Ar cennen, ar yé! malwa rocco, ar ye hamne sesse sáme i esse Ñuru, ar Mandos se-hilyane. Ar hére náne tien antaina or i canasta cemeno, nahtien andamacilden ar maitiénen ar qualmequámenen ar i hravaninen cemeno.
Ar íre latyanes i lempea *lihta, cennen nu i *yangwa fear i nancaron castanen Eruo quetto ar i *vettiéno ya sámelte.
Ar yámelte taura ómanen, quétala: “Manen andave, aire ar şanda Hér, ual namuva ar ahtaruva sercelma issen marir cemende?”
Ar ninque vaima náne antaina ilquenen mici te; ar náne tien quétina i mauyane tien náve sende an şinta lúmesse, tenna i nóte náne quátina yando ion náner núror aselte ar hánoltaron, ion umbar náne náve nance ve inte.
Ar cennen íre latyanes i enquea *lihta, ar túra *cempalie martane, ar Anar ahyane more ve *fillanne findiva, ar quanda Işil náne ve serce,
ar menelo tinwi lantaner cemenna, ve íre i úmanwe *relyávi lantar *relyávaldallo pálina taura súrinen.
Ar menel oante ve *toluparma nála tolúna, ar ilya oron ar tol léve yallo anelte.
Ar cemeno arani ar i taurar ar i cánor ar i lárear ar i poldar ar ilya mól ar ilya léra quén nurtaner inte i rottossen ar imíca i ondor i orontion.
Ar quétalte i orontinnar ar i ondonnar: “Á lanta menna ar áme nurta cendelello yeo hára i mahalmasse ar i rúşello i Euleo,
an i túra aure rúşeltava utúlie, ar man pole tare?”
Epeta cennen vali canta tárala i vincassen canta cemeno, avaleryala i súri canta cemeno; sie súre ua hlápula cemende hya earesse hya ilya aldasse.
Ar cennen hyana vala ortala anarórello, sámala i coirea Eruo *lihta, ar yámes taura ómanen i valannar canta in náne lávina mala cemen earye,
quétala: “Áva mala cemen hya ear hya i aldar, tenna apa *ilihtielme i núror Ainolvo timbareltassen.”
Ar hlassen i nóte i *lihtainaron, canta ar canaquean ar húmi tuxa, *lihtaine et ilya nossello Israelindion:
Et nossello Yehúro *lihtainar húmi yunque, et nossello Reuveno húmi yunque, et nossello Áro húmi yunque,
et nossello Ahyero húmi yunque, et nossello Naftalio húmi yunque, et nossello Manasseo húmi yunque,
et nossello Simeondo húmi yunque, et nossello Levio húmi yunque, et nossello Issacáro húmi yunque,
et nossello Sevulundo húmi yunque, et nossello Yósefo húmi yunque, et nossello Venyamíno lihtainar húmi yunque.
Apa si cennen, ar yé! haura rimbe ya *úquen ista onote, et ilye nórellon ar nossellon ar liellon ar lambellon, tárala epe i mahalma ar epe i Eule, vaine ninqui vaimalissen; ar enger olvali *nindornélion máltassen.
Ar yámelte taura ómanen, quétala: “Rehtie tule Ainolvallo, ye hára i mahalmasse, ar i Eulello.”
Ar ilye i vali tarner *os i mahalma ar i amyárar ar i coiti canta, ar lantanelte cendeleltanna epe i mahalma ar *tyerner Eru,
quétala: “Násie! I aistie ar i alcar ar i sailie ar i hantale ar i *laitie ar i túre na Ainolvan, tennoio ar oi! Násie!”
Tá quén i amyáraron carampe ninna, quétala: “Man nar queni sine i nar vaine ninqui vaimalínen, ar mallo utúlielte?”
Quenten senna: “Herunya, elye ista!” Ar nyarnes nin: “Queni sine nar i túlar et i túra şangiello, ar opoitielte vaimaltar ar ininquier tai sercesse i Euleo.
Etta ealte epe i mahalma Eruva, ar *veuyaltes auresse ar lómisse cordaryasse, ar ye hára i mahalmasse panyuva *lancoarya or te.
Ualte nauva maite hya soice ata; Anar ua petuva te, hya urtala úre.
An i Eule endesse i mahalmo nauva mavarelta, ar te-tulyuvas ehtelennar coirea nenwa, ar Eru mapuva oa ilya níre hendultalto.”
Íre i Eule latyane i otsea *lihta enge quilde menelde *os perta lúmeo.
Tá cennen i vali otso i tarir epe Eru, ar hyólar otso náner ten antaine.
Ar hyana vala, ye sáme *nisquema maltava, túle ar tarne ara i *yangwa, ar náne sen antaina haura lesta *nisqueo, carien ostime as i hyamier ilye i airion, i *yangwasse maltava epe i mahalma.
Ar ho i valo má, epe Eru, i usque i *nisqueo oronte as i hyamier i airion.
Tá i vala nampe i *nisquema ar sa-quante nárenen i *yangwallo. Ar sa-hantes cemenna; ar enger hundiéli ar ómali ar ítali ar *cempalie.
Sí i vali otso i sámer i hyólar otso manwaner lamien tai.
I minya vala lamyane hyólarya, ar hilyaner *helexe ar úr ostimesse as serce, ar anes hátina cemenna; ar nelesta cemeno náne urtaina, ar nelesta i aldaron náne urtaine, ar ilya laica salque náne urtaina.
I attea vala lamyane hyólarya, ar nat ve haura oron, uryala nárenen, náne hátina mir ear;
ar nelesta earo ahyane mir serce, nelesta i onnaron earesse qualle, i sámer coivie; ar nelesta i ciryaron náne nancarna.
I neldea vala lamyane hyólarya, ar haura tinwe lantane menello, uryala ve calma, ar lantanes i nelestanna i sírion ar ehteleron nenwa.
I tinweo esse *Alasára ná. Nelesta i nenion ahyane mir *alasára, ar rimbe queneli qualler i nennen, an anes carna sára.
I cantea vala lamyane hyólarya, ar nelesta Anaro náne pétina, ar nelesta Işilo, ar nelesta i tinwion. Sie nelesta calalto ahyane mir mornie; ua enge cala nelestasse i aureo, hya nelestasse i lómio.
Ar cennen, ar hlassen şoron yáma taura ómanen íre villes endesse menelo: “Ai, ai, ai in marir cemende, i lemyala hyólalammainen ho i vali nelde i sí lamyuvar hyólalta!”
Ar i lempea vala lamyane hyólarya, ar cennen tinwe lantaina menello cemenna, ar náne sen antaina i *latil i samno i undumeo.
Latyanes i samna i undumeo, ar usque oronte ve haura urnallo, ar Anar ar i vilya náner cárine morne i usquenen i samnallo.
Ar et i usquello túler *salquecápoli cemenna, ar náne ten antaina hére ve i hére i *nastaroron cemende.
Né tien quétina i ávalte malumne cemeno salque hya ilya laica olva hya ilya alda, mal eryave i queni lá arwe i *lihto Eruva timbareltasse.
Né tien antaina te-ñwalie astassen lempe, mal lá te-nahtie, ar ñwalmelta náne ve ñwalme *nastaro, íre nastas quén.
Ar mí ente rí queni cestuvar ñuru, mal laume hiruvalte sa; milyuvalte quale, mal ñuru uşuva tello.
Ilce i *salquecáporon náne ve roccor manwaine ohtan; ar cariltasse enger yar *şéne ve ríer maltava; cendelelta náne ve atanion cendele,
ar sámelte findele ve nission findele, ar nelciltar náner ve rávion nelci;
sámelte ambasseli ve angaine ambassi, ar i lamma rámaltaron náne ve i lamma rimbe luncaron arwe roccoron i nórar ohtanna.
Samilte pimpe arwe nasseo ve *nastaror samir, ar ea pimpeltasse túrelta malien queni astassen lempe.
Ve aran te-túrala ea tien i undumeo vala; esserya Heveryasse Avardon ná, ar Hellenyasse samis i esse Apollion.
I minya naice avánie. Yé! tuluvat an naice atta apa si.
I enquea vala lamyane hyólarya, ar hlassen óma i rassellon canta i *yangwasse maltava epe Eru
quéta i enquea valanna, sé ye sáme i hyóla: “Á lerya i vali canta i nar nútine ara i haura síre Perat!”
Ar i vali canta náner leryaine, i náner manwaine i lúmen, i auren, i astan ar i loan, nahtien nelesta Atanion.
I nóte i roquenion hosseltasse náne húmi *yúquean lúli húmi quean; hlassen nótelta.
Ar sie yentenye i roccor ar i hamner tesse mí maur: i roqueni coller ambasseli arwe i quileo náreo ar lúleo ar *ussardo, ar i cas i roccoiva náne ve cas rávion, ar ruine ar usque ar *ussar lender et antoltallo.
Nelde sine ungwalínen nelesta Atanion náne nanca, i nárenen ar i usquenen ar i *ussardenen yar lender et antoltallo.
An i túre i roccoron ea antoltasse ar pimpeltasse; pimpelta náne ve leuca, arwe caro, ar sanen harnalte.
I lemyala ranta Atanion, i úner nance sine ungwalínen, uar hirne inwis pa i cardar máltato hya pustane *tyere raucoli ar cordolli maltava ar telpeva ar urusteva ar ondova ar toava, yar uar pole cene hya hlare hya vanta;
yando ualte hirne inwis pa nahtieltar hya ñólelta hya *hrupuhtalelta hya pilweltar.
Ar cennen hyana polda vala túla undu menello, vaina fanyasse ar arwa helyanweo or carya, ar cendelerya náne ve Anar, ar telcoryat ve tarmar náreva.
Sámes pitya pantaina parma. Ar panyanes forya talya earesse ar i hyarya noresse,
ar yámes taura ómanen, ve íre rá ná rávea. Ar íre yámes, i hundier otso quenter vére ómaltainen.
Apa i hundier otso quenter, inye tecumne, mal hlassen óma menello ya quente: “Á *lihta yar i hundier otso equétier ar ávatai tece!”
Ar i vala ya cennen tárala earesse ar noresse ortane márya menelenna
ar antane vandarya yenen coirea ná tennoio ar oi, ye ontane menel ar ya ea sasse, ar cemen ar ya ea sasse, ar ear ar ya ea sasse: “I lúme avánie!
Mal mí rí yassen i otsea vala nauva hlárina – íre lamyuvas hyólarya – tá Eruo fóle nauva telyaina, ve i evandilyon ya antanes núroryain, i *Erutercánor.”
Ar i óma ya hlassen menello quente ninna ata, quétala: “Á lelya, á mapa i parma ya ná pantaina ho i má i valo ye tára earesse ar noresse.”
Ar lenden i valanna ar quente senna i nin-antumnes i pitya parma; ar quentes ninna: “Ása mapa ar mata; nauvas sára cumbalyan, mal lisse ve lís antolyasse.”
Ar nampen i pitya parma ho i valo má ar sa-mante; anes lisse ve lís antonyasse, mal apa mantenyes cumbanya náne carna sára.
Ar náne quétina ninna: “Mauya lyen ata quete ve Erutercáno, pa lieli ar nóreli ar lambeli, ar rimbe aralli.”
Ar sirpe ve vandil náne nin antaina, ar mo quente: “Á orta ar á *lesta i corda Eruva ar i *yangwa ar i *tyerir sasse,
mal á hehta i cordo etya paca ar ávasa *lesta, an nás antaina nórin, ar *vattuvalte i aire ostonna astassen atta ar *canaquean.
Ar lavuvan *vettonya attan quete ve *Erutercánor ter rí enenquean ar tuxar yunque, vaine *fillannesse.”
Tú nát i *milpialda atta ar i calmatarma atta yar tarir epe i Heru cemeno.
Qui aiquen mere mala tú, náre mene et antottallo ar ammate ñottottar, ar qui aiquen mere mala tú, nauvas nanca sie.
Tú samit hére holtien menel, pustien miste lantiello mí rí yar quétatte ve Erutercánor, ar samitte hére or i neni, vistien tai mir serce ar petien cemen ilya ungwalénen, quiquie meritte.
Ar íre etéliette *vettietta, i hravan ye orta i undumello ohtacaruva túna, ar samuvas apaire or tu ar nahtuva tu.
Ar loicottat caituvat i palla mallesse i haura ostosse ya mi lé i faireo ná estaina Sorom ar Mirrandor, yasse yando Herutta tarwestaina né.
Queneli i lieron ar nossion ar lambion ar nórion yétuvar loicottat ter rí nelde ar perta, ar ualte lave i loicottat nát panyaine noirisse.
Ar i marir cemende samir alasse pa tú ar nar valime, ar mentuvalte annali quén i exenna, an Erutercáno atta sine ñwalyanet i marir cemende.
Mal apa i auri nelde ar perta coiviefaire Erullo túle minna tu, ar orontette taluttatse, ar túra caure lantane innar yenter tu.
Ar hlassette taura óma et menello quéta túna: “Tula amba sir!” Ar lendette mir menel i fanyasse, ar cotumottar cenner tu.
Ar lúme yanasse enge túra *cempalie, ar quaista i osto atalante, ar queni húmi otso náner nance i *cempaliénen, ar i lemyaner náner ruhtaine ar antaner alcar Erun menelo.
I attea naice avánie. Yé! i neldea naice tuluva rato.
Ar i otsea vala lamyane hyólarya. Ar taure ómali náner hlárine menelde, quétala: “I aranie i mardeva ná sí Herulvo ar Hristoryo, ar turuvas tennoio ar oi.”
Ar i amyárar canta ar *yúquean i hamner epe Eru mahalmaltassen lantaner cendeleltannar ar *tyerner Eru,
quétala: “Hantalme lyen, Héru Eru, i Iluvala, i ea ar i enge, an amápiel túra túrelya ar ture ve aran.
Mal nóri náner rúşie, ar véra rúşelya túle, ar i lúme namien i qualini ar antien *paityale núrolyain i Erutercánor, ar i airin ar in rucir esselyallo, i pityain yo i túrain, ar nancarien i nancarir cemen.”
Ar i corda Eruva ya ea menelde náne latyaina, ar i colca véreryo náne cénina cordaryasse. Ar enger ítali ar ómali ar hundiéli ar *cempalie ar túra *helexe.
Ar túra tanwa náne cénina menelde, nís vaina Anaresse, ar Işil enge nu talyat, ar caryasse enge ríe tinwiva yunque.
Anes *lapsarwa, ar yamas naiceryassen ar ñwalmeryasse colien.
Ar hyana tanwa náne cénina menelde, ar yé! haura narwa hlóce, arwa carion otso ar rassion quean, ar caryassen samis *şarnuntar otso.
Pimperya tuce nelesta i tinwion menelo, ar hantes tai undu cemenna. Ar i hlóce tarne epe i nís ye náne manwa colien, ammatieryan hínarya íre columnes.
Ar colles yondo, seldo, ye turuva ilye nóri angaina vandilden. Ar hínarya náne mapaina oa Erunna ar mahalmaryanna.
Ar i nís úşe mir i ravanda, yasse samis nóme manwaina lo Eru, antieltan sen tasse maureryar ter rí *enenquean ar tuxar yunque.
Ar ohta martane menelde: Mícael ar valaryar mahtaner i hlócenna, ar i hlóce ar valaryar mahtaner,
mal uas sáme apaire, ar tien nóme úne ambe hírina menelde.
Ar i túra hlóce hátina undu né, i enwina leuca, ye ná estaina i Arauco ar Sátan, ye tyare quanda Ambar ranya – anes hátina undu cemenna, ar valaryar náner hátine undu óse.
Ar hlassen taura óma menello quéta: “Sí utúlier i rehtie ar i túre ar i aranie Ainolvo ar i hére Hristoryo, an i *ulquéto hánolvaron hátina undu ná, ye te-*ulquente mi aure yo lóme epe Ainolva!
Ar sámelte apaire i Euleo sercenen ar *vettielto quettanen, ar coivieltar úner tien ta melde i avanelte vele qualme.
Etta na valime, a menel ar i marir sasse! Ai cemenen ar earen! An i Arauco utúlie undu lenta, arwa túra rúşeo, istala i lúmerya şinta ná.”
Ar íre i hlóce cenne i anes hátina undu cemenna, roitanes i nís ye colle i seldo.
Mal rámat i túra şorno nánet antaine i nissen, polieryan vile mir i ravanda nómeryanna; tasse camis maureryar ter lúme ar lúmeli ar perta lúmeo, haira i leuco cendelello.
Ar i leuca mentane nén ve síre antoryallo apa i nís, se-colien oa i sírenen.
Mal cemen manyane i nís, ar cemen latyane antorya ar *hlunce i síre ya quamne i hlóce antoryallo.
Ar i hlóce náne rúşea i nissenna, ar lende oa ohtacarien innar lemner erderyo, i hepir i axani Eruo ar samir i *vettie pa Yésus.
Ar tarnes i litsesse ara ear.Ar cennen hravan ortala et earello, arwa rassion quean ar carion otso, ar rasseryassen samis *şarnuntar quean, ar caryassen ear esseli yaiweo Eruva.
Ar i hravan ye cennen náne ve *picara, mal talyar náner ve morco tali, ar antorya náne ve rávo anto. Ar i hlóce antane sen túrerya ar mahalmarya ar túra hére.
I ilce ero caryaron náne ve qui anes harna qualmen, mal qualmeharwerya náne nestaina, ar i quanda cemen hilyane i hravan elmendasse.
Ar *tyernelte i hlóce pan antanes i hére i hravanen, ar *tyernelte i hravan quétala: “Man ve i hravan ná, ar man pole mahta senna?”
Ar náne sen antaina anto quétala túrali ar yaiweli Erunna, ar camnes hére carien ya meris astain atta ar *canaquean.
Ar latyanes antorya yaiwelissen Erunna, quetien yaiweli esseryanna ar vehteryanna, ar innar marir menelde.
Ar né sen antaina care ohta i airinnar ar same apaire or te, ar camnes hére or ilya nosse ar lie ar lambe ar nóre.
Ar ilye i marir cemende se-*tyeruvar, té ion essi uar técine Parmasse Coiviéva i Euleva ye náne nanca i tulciello i mardeva.
Aiquen arwa hlaro, nai hlaruvas!
Qui quén mandon ná, mir mando lelyas. Qui quén nahtuva macilden, nauvas nanca macilden. Sís ea maure voronweva ar saviéva i airion.
Ar cennen hyana hravan, ye oronte et cemello, ar sámes rasset ve eule, mal carampes ve hlóce.
Mahtas i quanda hére i minya hravano epe henyat. Tyaris cemen ar i marir sasse *tyere i minya hravan, yeo qualmeharwe náne nestaina.
Ar caris túre tanwali; yando náre tyaris lanta undu et menello cemenna epe hendu atanion.
Ar tyaris ranya i marir cemende, i tanwainen yar nar sen lávina care epe hendu i hravano. Quetis innar marir cemende i caruvalte emma i hravano, ye camne i macilharwe ar nanwenne coivienna.
Ente, náne sen lávina anta şúle i hravano emman, tyárala i hravano emma carpa, ar yando tyárala náve nance illi i váquenter *tyere i hravano emma.
Ar tyaris illi, i pityar ar i túrar, i lárear ar i únar, ar i lérar ar i móli, came tehta formaltasse hya timbareltasse,
ar *úquen náne lávina manca hya vace hequa quén arwa i tehto – i hravano esse hya i nóte esseryo.
Sís ea maure sailiéva: Lava i handan note i hravano nóte, an nás atano nóte, ar nóterya ná enque ar *enenquean ar tuxar enque.
Ar cennen, ar yé! i Eule tarne Oron Síonde, ar as sé húmi canta *canaquean ar tuxa i samir esserya ar i esse Ataryo técine timbareltasse.
Ar hlassen lamma et menello, ve i lamma rimbe nenion ar ve i lamma túra hundiéno; ar i lamma ya hlassen náne ve ñandaror i tyalir ñandeltainen.
Ar lirilte vinya líre epe i mahalma ar epe i coiti canta ar i amyárar, ar ua ence aiquenen pare líre tana hequa i húmi canta *canaquean ar tuxa, i nar mancaine cemello.
Té nar i uar avahtie inte nissínen, an nalte vendeli. Té nar i hilyar i Eule ilya nómenna ya lelyas. Anelte mancaine ho mici Atani ve minye yáveli Erun ar i Eulen,
ar huru úne hírina antoltasse; nalte ú mordo.
Ar cennen hyana vala víla endesse menelo, arwa oira evandilyono, sa-carieryan sinwa in marir cemende, ar ilya nóren ar nossen ar lamben ar lien,
quétala taura ómanen: “Ruca Erullo ar ásen anta alcar, an i lúme námieryo utúlie, ar *tyera ye carne menel cemenye ar ear ar ehteler nenwa.”
Ar exe, attea vala, hilyane quétala: “Alanties! Alantie Vável Túra, ye tyarne ilye nóri suce i limpeo i rúşeva *hrupuhtaleryo!”
Ar hyana vala, neldea, hilyane tu, quétala taura ómanen: “Qui aiquen *tyere i hravan ar emmarya, ar came i tehta timbareryasse hya máryasse,
sé yando sucuva i limpeo i ormeva Eruo ya ná ulyaina poica mir i yulma rúşeryava, ar nauvas ñwalyaina nárenen ar *ussardenen epe i airi vali ar epe i Eule.
Ar i usque ñwalmelto orta ama tennoio ar oi, ar auresse yo lómisse ualte same sére, i *tyerir i hravan ar emmarya, ar aiquen ye came i tehta esseryo.
Sís ea maure i voronwiéva i airion, i hepir i axani Eruo ar i savie Yésuo.”
Ar hlassen óma et menello quéta: “Teca: Valime nar i qualini i qualir i Herusse, ho sí. Ná, i Faire quete, lava tien sere molieltallon, an cardaltar te-hilyar.”
Ar cennen, ar yé! fána fanya, ar i fanyasse quén ve atanyondo hamne, sámala ríe maltava caryasse ar aica circa máryasse.
Ar hyana vala etelende i cordallo, yámala taura ómanen yenna hamne i fanyasse: “Á menta circalya ar *cirihta, an i lúme yáviéno utúlie, ar cemeno yávie manwa ná.”
Ar ye hamne i fanyasse mentane circarya olla cemen, ar cemen náne *cirihtaina.
Ar hyana vala etelende i cordallo ya ea menelde, yando sé arwa aica circo.
Ar hyana vala etelende i *yangwallo, ye sáme hére or i náre. Ar yámes taura ómanen yenna sáme i aica circa, quétala: “Á menta aica circalya ar á comya i loxi liantasseo cemeno, an *tiumaryar nar manwe.”
Ar i vala mahtane circarya cemende ar comyane i yáve liantasseo cemeno, ar sa-hante mir i haura *limpevorma rúşeva Eruo.
Ettesse i osto i *limpevorma náne *vattaina, ar serce ulle i *limpevormallo ve tárave ve i *antolattar i roccoron, *restandier tuxar enque ar húme oa sallo.
Ar cennen hyana tanwa menelde, túra ar *elmendea: vali otso arwe ungwaleron otso, i métimar, an tainen Eruo orme ná telyaina.
Ar cennen nat ve ailin calcava, ostimesse as náre, ar i asámier apaire or i hravan ar emmarya ar esseryo nóte tarner ara i ailin calcava, arwe ñandélion Eruva.
Ar lindalte i líre Móseo, Eruo núro, ar i líre i Euleo, quétala: “Túre ar *elmendie nar cardalyar, Héru Eru, i Iluvala. Faile ar şande nar tielyar, Aran nórion.
Man ua rucuva lyello, a Héru, ar antuva alcar esselyan? An elye erinqua aire ná. An ilye nóri tuluvar ar *tyeruvar epe lye, an faile námielyar anaier apantaine.”
Ar apa nati sine cennen, ar i yána i *Lancavo *Vettiéva náne latyaina menelde,
ar i vali otso arwe i ungwaleron otso etelender i yánallo, vaine poica, calima *páşesse ar nútine *os ambostelta laurie quiltailínen.
Ar quén i coition canta antane i valin otso tolpor otso maltava yar náner quante i ormeo Eruo, ye ná coirea tennoio ar oi.
Ar i yána náne quátina usqueo, i alcarnen Eruo ar túreryanen, ar *úquen polle tule mir i yána tenna i ungwaler otso i valion otso náner telyaine.
Ar hlassen taura óma et i yánallo quéta i valannar otso: “Á lelya ar ulya i tolpor otso ormeva Eruo mir cemen!”
Ar i minya etelende ar ulyane tolporya mir cemen. Ar túle ulca ar naicalea *siste i atanissen i sámer i hravano tehta ar *tyerner emmarya.
Ar i attea ulyane tolporya mir ear. Ar ahyanes mir serce ve i serce loicollo, ar ilya coirea onna qualle, i enger earesse.
Ar i neldea ulyane tolporya mir i síri ar i ehteler nenwa, ar ahyanelte mir serce.
Ar hlassen i vala i nenion quéta: “Faila nalye, i ea ar i enge, i aire, an sie anámiel!
An ulyanelte serce airion ar Erutercánoron, ar ániel tien serce sucien. Nalte valde!”
Ar hlassen i *yangwa quéta: “Ná, Héru Eru, i Iluvala, şande ar faile nar námielyar!”
Ar i cantea ulyane tolporya Anarenna, ar náne Anaren antaina urta atani nárenen.
Ar atani náner urtaine túra úrenen, mal huntelte i esse Eruo, ye sáme hére or ungwaler sine, ar ualte hirne inwis sen-antien alcar.
Ar i lempea ulyane tolporya i mahalmanna i hravanwa. Ar aranierya náne carna morna, ar queni ñwarner lammalta i ñwalmanen,
mal Eru menelo huntelte ñwalmaltainen ar sisteltainen, ar ualte hirne inwis pa cardaltar.
Ar i enquea ulyane tolporya mir i haura síre Perat, ar nenerya náne *parahtaina, manwien i malle i aranin anarórello.
Ar cennen úpoice fairi nelde, ve quáci, i túler et i antollo i hlóceo, ar et i antollo i hravano, ar et i antollo i *hurutercáno.
An nalte raucofaireli i carir tanwali, ar etelendelte i arannar quanda Ambaro, te-comien i ohtan i túra auresse Eru Iluvalo.
– “Yé! túlan ve pilu! Valima ná ye hepe inse cuiva ar hepe larmaryar, i ua mauyuva sen vanta helda ar queni cenuvar nucumnierya!” –
Ar comyaneltet i nómesse ya Heveryasse ná estaina Armaherdon.
Ar i otsea ulyane tolporya vilyanna. Ar taura óma ettúle i yánallo, i mahalmallo, quétala: “Amarties!”
Ar enger ítali ar ómali ar hundiéli, ar martane *cempalie ta túra i lá amartie síte nat íre Atani amárier cemende – *cempalie ta palla hya ta túra.
Ar i haura osto náne şanca mir rantar nelde, ar atalanter ostor nóríva. Eru enyalle Vável Túra, ar sen-antanes i yulma i limpeo i ormeva rúşeryava.
Ilya tol úşe, ar oronti úner ambe hírine.
Sarneli *helexeva, haure ve talenti, lantaner undu menello i queninnar. Mal queni hunter Eru i ungwalénen *helexeva, an i ungwale rúcima né.
Ar quén i valion otso i sámer i tolpor otso túle ar quente asinye, quétala: “Tula, tanuvan lyen i námie i túra *imbacindeva ye hára or rimbe neneli,
as ye cemeno arani *hrupuhtaner, ar i marir cemende náner cárine *limpunque *hrupuhtaleryo limpenen.”
Ar ni-colles oa i Fairesse mir i ravanda. Ar cennen nís hárala culda hravande quanta ession yar náner yaiwe Eruva, arwa carion otso ar rassion quean.
I nisso larmar náner *luicarni ar culde. ar anes netyaina maltanen ar mírelínen ar marillalínen, ar máryasse sámes yulma maltava quanta şaure nation ar i úpoice nation *hrupuhtataleryo.
Ar timbareryasse náne técina esse, fóle: “Vável Túra, amil *imbacindion ar i şaure nation cemeno.”
Ar cennen manen i nís náne *limpunqua i sercenen i airion ar i sercenen i *vettoron Yésus.Íre cennenyes anen túra elmendasse.
Mal i vala quente ninna: “Manen ná i nalye elmendasse? Nyaruvan lyen i fóle i nisso ar i hravano se-cólala ar arwa i carion otso ar i rassion quean:
I hravan ye cennel enge, mal ea lá, ananta ortuvas et i undumello ar menuva oa mir nancarie. I marir cemende, ion essi uar técine i Parmasse Coiviéva tulciello i mardeva, nauvar quátine elmendanen íre cenilte manen i hravan enge, mal ea lá, ananta entuluva.
Sís ea maure handaléva arwa sailiéno: I cari otso nar oronti otso, ar tais i nís hára.
Ar ear arani otso: lempe alantier, er ea sí, ar i exe en ua utúlie, ar íre tuluvas, lemyuvas şinta lúmesse.
Ar i hravan ye enge mal ea lá, náse yando toltea aran; tulis i otsollo ar lelya oa mir nancarie.
Ar i rassi quean yar cennel nar arani quean, i uar en acámie aranie, mal é camilte hére ve aralli erya lúmesse as i hravan.
Samilte i imya sanwe ar antar túrelta ar hérelta i hravanen.
Mahtuvalte i Eulenna, mal pan náse Heru heruion ar Aran aranion, i Eule samuva apaire or te, as i yálinar ar *cílinar ar vorondar.”
Ar eques ninna: “I neni yar cennel, yassen hára i *imbacinde, nar lieli ar şangali ar nóreli ar lambeli.
Ar i rassi quean yar cennel, ar i hravan – tevuvalte i *imbacinde ar se-caruvar lusta ar helda, ar hráverya matuvalte, ar sé urtuvalte nárenen.
An Eru panyane endaltassen care indómerya, carieltan erya sanwe ar antieltan aranielta i hravanen, tenna Eruo quettar nar telyaine.
Ar i nís ye cennel ná i túra osto ya same aranie or i arani cemeno.”
Apa nati sine cennen hyana vala túla undu menello, arwa túra héreo, ar cemen náne calyaina alcareryanen.
Ar yámes polda ómanen: “Alanties! Alantie Vável Túra, ar anaies carna vehte raucoron ar mando ilya úpoica aiweo, ar mando ilya úpoica ar tévina hravano!
An i limpeo ormeva *hrupuhtaleryo usúcier ilye nóri, ar cemeno arani *hrupuhtaner óse, ar cemeno macari náner cárine lárie i túrenen úveryo ú landaron!”
Ar hlassen hyana óma et menello quéta: “Á lelya et sello, lienya, pustien inde samiello ranta óse úcariryassen ar camiello ungwaleryon.
An i hahta úcariryaiva rahta menelenna, ar Eru *erénie úfaile cardaryar.
Ásen nan-anta tambe ve sé nan-antane, ar cara sen atwa yaron sé acárie! Mir i yulma yanna ulyanes, á ulya atwa lesta sén!
Ve antanes alcar insen ar marne úvesse ú landaron, tambe ásen anta ñwalme ar nyére! An enderyasse quetis: ’Háran tári, ar uan *verulóra, ar uan oi istuva nyére.’
Etta ungwaleryar tuluvar erya auresse, qualme ar nyére ar maitie, ar nauvas urtaina nárenen, an polda ná i Héru Eru ye anámie se!
Ar cemeno arani, i hrupuhtaner óse ar marner óse úvesse ú landaron, euvar níressen ar palpuvar inte nyérenen séva, íre yétalte i usque urtieryallo.
Táralte hairave caureltanen ñwalmeryava ar quétar: ’Horro, horro, a túra osto, Vável i polda osto, an erya lúmesse námielya utúlie!’
Ar cemeno macari nar níressen ar se-nainar, an sí ea *úquen yen polilte vace armaltar,
armar ve malta ar telpe ar míreli ar marillali ar mára *páşe ar *luicarne ar samin ar culda, ar ilya níşima tavar ar ilya vene *mortavarwa ar ilya vene ammaira tavarwa ar urus ar anga ar alas,
ar cinnamon ar amomum ar *nisque ar níşima millo ar *ninquima ar limpe ar *piemillo ar mulma ar ore ar yaxeli ar mámali, ar roccoli ar luncali ar móleli – coirie queneli.
É i mára yáve ya náne fealyo íre oantie lyello, ar ilye i netye nati ar i calwe nati nar vanwe lyen, ar mo lá oi enhiruva tai.
I macari mancala sine nati, i náner cárine lárie sello, taruvar hairave caureltanen ñwalmeryava ar nauvar níressen ar samuvar nyére,
quétala: ’Horro, horro – i túra osto, vaina mi *páşe ar *luicarne ar culda, ar netyaina maltanen ar mírenen ar marillanen,
an erya lúmesse lar ta túra nancarna né!’ Ar ilya hesto ar ilquen ye lelya ciryanen, ar ciryamóli ar illi i mótar earesse, tarner hairave
ar yámer íre yentelte i usque urtieryallo ar quenter: ’Man mici ostor ná ve i túra osto?’
Ar hantelte asto careltanna ar yámer mi níreli ar nyére, ar quenter: ’Horro, horro – i túra osto, yasse illi i samir ciryar earesse náner cárine lárie túra almaryanen, an erya lúmesse nése nancarna!’
Na valime sénen, a menel ar i airi ar i aposteli ar i *Erutercánor, an eldenen Eru equétie námie senna!”
Ar polda vala ortane haura *mulondo ar sa-hante mir ear, quétala: ”Sie, lintave, Vável Túra nauva hátina undu, ar uas oi nauva enhirna.
Ar i lamma ñandaroron ar nyelloron ar simpetarion ar *hyólamoron laume nauva hlárina lyesse ambe, ar tanor ilye curwion laume nauvar hírine lyesse ambe, ar i lamma *mulondollo laume nauva hlárina lyesse ambe,
ar cala calmo laume caltuva lyesse ambe, ar óma endero hya indisso laume nauva hlárina lyesse ambe; an macarilyar náner i minde neri cemeno, an ñúlelyanen ilye nóri náner tyárine ranya.
Ente, sesse náne hírina i serce Erutercánólion ar airilion ar illion i náner nance cemende.”
Apa nati sine hlassen ya né ve taura óma haura rimbeo menelde. Quentelte: “Hallelúya! I rehtie ar i alcar ar i túre nar Ainolvo,
an şande ar faile nar námienyar! An anámies i túra *imbacinde ye hastane cemen *hrupuhtaleryanen, ar Eru atacárie núroryaron serce et máryallo.”
Ar i attea lusse quentelte: “Hallelúya! Ar i usque sello orta amba tennoio ar oi!”
Ar i amyárar canta *yúquean ar i coiti canta lantaner undu ar *tyerner Eru i hande i mahalmasse, ar quenter: “Násie! Hallelúya!”
Ar óma tule i mahalmallo, quétala: “Á laita Ainolva, ilye núroryar i rucir sello, i pityar yo i túrar!”
Ar hlassen ya náne ve óma haura şango ar ve lamma rimbe nenion ar ve lamma polde hundiéron. quétala: “Hallelúya, an i Héru Ainolva anaie carna aran, i Iluvala!
Alve na valime ar same alasse, ar alve anta sen i alcar, an i Euleo veryanwe utúlie, ar vesserya amanwie immo.
Ar né sen antaina náve vaina calima, poica *páşesse, an i *páşe i faile cardar i airion ná.”
Ar nyaris nin: “Teca: Valime i nar tultaine i veryanwe-mattenna i Euleo!” Ente, eques ninna: “Sine nar i nanwe quettar Eruo.”
Ar lantanen undu epe talyat *tyerien se. Mal nyaris nin: “Cima! Áva care! Nanye eryave núro as lyé ar hánolyar, i samir i *vettie Yésuo! Eru alye *tyere!” An i *vettie Yésuo ná i apacéno faire.
Ar cennen menel latyaina, ar yé! ninque rocco! Ar ye hamne sesse ná estaina Voronda ar Şanda, ar namis ar mahtas failiesse.
Henyat nát ve uruite ruine, ar caryasse ear rimbe *şarnuntali. Samis esse técina ya *úquen ista hequa sé erinqua.
Náse vaina collasse *tumyaina sercesse, ar i esse yanen náse estaina ná Eruo Quetta.
Menelo hossi se-hilyaner ninqui roccossen ar vaine ninque ar poica *páşesse.
Ar et antoryallo mene aica andamacil, yanen petuvas nóri, ar tai-turuvas angaina vandilden. *Vattas i *limpevorma i ormeva i rúşeva Eru Iluvalo.
Ar collaryasse, tiucoryasse, samis esse técina: Aran Aranion ar Heru Heruion.
Ar cennen vala tárala Anaresse, ar yámes taura ómanen ar quente ilye aiwennar i vilir menelo endesse: “Tula, ócoma Eruo túra merendenna,
matien hráve aranion, hráve cánoron ar hráve polde nerion, hráve roccoron ar ion hamner tesse, i hráve illion, léraron ar mólion, pityaron yo túraron.”
Ar cennen i hravan ar cemeno arani ar hosseltar comyaine ohtacarien yenna hamne i roccosse ar hosseryanna.
Ar i hravan náne mapaina, ar as sé i *hurutercáno ye carne i tanwar epe se, yainen tyarnes ranya i camner i tehta i hravano ar *tyerner emmarya. Anette yúyo hátine coirie mir i ailin náreva ya urya *ussardenen.
I exi nér nance i andamacilden ya lende et antollo yeo hamne i roccosse, ar ilye i aiwi nér quátine hráveltanen.
Ar cennen vala túla undu et menello arwa i *latilo i undumeo ar haura naxa máryasse.
Ar nampes i hlóce, i yára leuca, ye ná i Arauco ar Sátan, ar se-avaleryane loain húme.
Ar se-hantes mir i undume ar sa-holtane ar sa-*lihtane or se, pustien se tyariello nóri ranya nó i húme loar náner vanwe. Apa ta nauvas leryaina şinta lúmen.
Ar cennen mahalmali, ar enger i hander undu tais, ar námotúre náne ten antaina. Ar cennen i fear ion náner nance pelecconen *vettiénen pa Yésus ar quetiénen pa Eru, ar ion uar *etyérie i hravan hya emmarya ar uar acámie i tehta timbareltasse hya máltasse. Túlelte coivienna ar turner as Hristo ter i loar húme.
I exi i qualinion uar túle coivienna nó i loar húme nér vanwe. Si i minya enortie ná.
Valima ar aire ná ye same ranta i minya enortiesse. Or té i attea ñuru ua same hére, mal nauvalte *airimor Eruo ar Hristo, ar turuvalte óse ter i loar húme.
Íre i loar húme avánier, Sátan nauva leryaina mandoryallo.
Etelelyuvas tyarien ranya i nóri i vincassen canta cemeno, Cóc yo Mácoc, te-comien i ohtan, únótime ve earo litse.
Lendelte ama olla i palla cemen ar peller i *estolie i airíva ar i melda osto. Mal náre túle undu menello ar te-ammante.
Ar i Arauco ye tyarne te ranya náne hátina mir i ailin náreva ar *ussardeva, yasse yúyo i hravan ar i *hurutercáno enget, ar nauvalte ñwalyaine auresse yo lómisse tennoio.
Ar cennen túra ninque mahalma ar ye hande sasse. Cendeleryallo cemen yo menel úşet oa, ar nóme úme hírina tún.
Cennen yando qualini, i túrar yo i pityar, tárala epe i mahalma, ar parmali náner latyaine. Tá náne latyaina hyana parma, ya i Parma Coiviéva ná. Ar qualini náner námine i natinen técine i parmassen, ve cardaltar.
Ar ear nan-antane i qualini sasse, ar ñuru ar Mandos nan-antanet i qualini mi tú, ar anelte námine, ilquen ve cardaryar.
Ar ñuru ar Mandos nánet hátine mir i ailin náreva. Si i attea ñuru ná: i ailin náreva.
Ar qui aiquen úne hirna técina i Parmasse Coiviéva, anes hátina mir i ailin náreva.
Ar cennen vinya menel ar vinya cemen, an i minya menel ar i minya cemen nánet vanwe, ar ear ua ambe enge.
Cennen yando i aire osto, Vinya Yerúsalem, túla undu et menello, manwaina ve indis netyaina veruryan.
Ar hlassen taura óma i mahalmallo quéta: “Yé! I már Eruva ea as Atani, ar maruvas aselte, ar nauvalte lieryar. Ar Eru immo euva aselte.
Mapuvas oa ilya níre hendultalto, ar ñuru ua ambe euva; yando nyére ar rambe ar ñwalma uar ambe euvar. I noe nati avánier.”
Ar ye hande i mahalmasse quente: “Yé! Envinyatan ilye nati!” Ar eques: “Teca, an quettar sine nar voronde ar şande!”
Ar quentes ninna: “Nas cárina! Inye Tinco ar Úre, i yesse ar i metta. Ilquenen ye ná soica inye antuva i ehtelello i nenwa coiviéva, muntan.
Aiquen ye same apaire nauva aryon sine nation; inye nauva Ainorya, ar sé nauva yondonya.
Mal i caurear ar i úsávalar ar i şaurar ar i nehtari ar i *hrupuhtandor ar i carir ñúle ar i *tyerir cordoni, ar ilye *hurindor – masselta ea i ailinde uryala nárenen ar *ussardenen. Si i attea ñuru ná.”
Quén i valion otso i sámer i tolpor otso quante i métime ungwaleron otso túle quetien óni ar quente: “Tula! Lyen-tanuvan i indis, i Euleo veri.”
Fairenen colles ni oa haura ar tára orontenna, ar tannes nin i aire osto Yerúsalem túlala undu et menello, Erullo,
ar sámala i alcar Eruo. Ñaltarya ná ve ammaira míre, ve *nambíre caltala ve maril.
Sámes haura ar tára ramba ar andor yunque, ar ara i andor vali yunque, ar i andossen náner técine i essi i nossion yunque i yondoron Israélo:
andor nelde Rómenna, andor nelde Formenna, andor nelde Hyarmenna, ar andor nelde Númenna.
Ar i ostoramba sáme *talmondor yunque, ar técine tais enger i essi i apostelion yunque i Euleo.
Ar ye quente ninna sáme ve lesta maltasirpe, polieryan *lesta i osto ar andoryar ar rambarya.
I osto ná *cantil, ar nás ve anda ve nás palla. Ar *lestanes i osto i sirpenen, *restandier húmi yunque; andierya ar pallierya ar tárierya nár imye.
*Lestanes yando rambarya, *perrangar canta *canaquean ar tuxa, atano lestanen, ar yando valo.
I ramba náne cárina *nambíreva, ar i osto náne poica malta, ve poica cilin.
I talmar i ostorambo náner netyaine ilya nostalénen mírion: I minya *talmondo *nambíre né, i attea lúle, i neldea *ostimmir, i cantea *laimaril,
i lempea *fanorcarne, i enquea *culmíre, i otsea *malatsar, i toltea elessar, i nertea *sinile, i quainea *orvamir, i minquea *linquemir, ar i yunquea *helissar.
Ente, i andor yunque náner marillar yunque; ilya ando náne er marilla. Ar i osto malle náne poica malta, ve *tercénima calca.
Ar uan cenne corda sasse, an i Héru Eru Iluvala cordarya ná, ar sie ná i Eule.
I osto ua same maure Anarwa hya Işilwa caltien sanna, an Eruo alcar sa-calyane, ar calmarya i Eule né.
Ar nóri vantuvar i calanen i ostollo, ar cemeno arani mentuvar alcarelta minna sa.
Ar andoryar laume nauvar holtaine auresse, an lá euva lóme tasse.
Ar mentuvalte i nórion alcar ar *laitie minna sa.
Mal aiqua úaire ar aiquen ye care şaura nat ar huru ua tuluva minna sa, mal eryave i nar técine i parmasse coiviéva i Euleva.
Ar tannes nin síre i nenwa coiviéva, *tercénima ve maril, sírala et i mahalmallo Eruva ar i Euleva,
i endesse i palla malleo i ostosse. Ar foryasse ar hyaryasse i síreo enge i Alda Coiviéva ye cole yáve lússen yunque, antala yáverya ilya astasse. Ar i Aldo lassi nar i nestien nóriva.
Ua euva ambe *aiqua húna. Mal i mahalma Eruva ar i Euleva euva tasse, ar núroryar se-*veuyuvar.
Cenuvalte cendelerya, ar esserya euva timbareltasse.
Ar lóme ua ambe euva, ar ualte samuva maure calava calmo hya calava Anaro, an i Héru Eru te-calyuva, ar turuvalte tennoio ar oi.
Ar eques ninna: “Quettar sine nar voronde ar şande. I Héru, i Aino fairion i Erutercánoron, ementie valarya tanien núroryain yar rato martuvar.
Yé! tuluvan rato! Valima ná ye cime i quettar i apacéno parma sino.”
Ar inye Yoháno náne ye hlasse ar cenne nati sine. Ar apa hlarie ar cenie, lantanen undu *tyerien epe talu i valo ye nin-tanne nati sine.
Mal quetis ninna: ”Cima! Áva care! Nanye hyana núro as lyé ar hánolyar i nar Erutercánoli, ar as i cimir i quettar parma sino! *Tyera Eru!”
Ar quetis ninna: ”Áva *lihta i quettar i apacéno parma sino, an i lúme hare ná.
Yen care úfailie lava care úfailie en; ar i váran lava náve carna vára en; mal lava i failan care failie en, ar lava i airen náve carna aire en.
Yé! Tuluvan rato, ar i *paityale ya antan ea asinye, *atantien ilquenen ve molierya.
Inye Tinco ar Úre, i minya ar i métima, i yesta ar i metta.
Valime nar i *sovir collaryar, i lertuvalte tule i Aldanna Coiviéva ar lelya mir i osto ter i andor.
I etsesse ear i huor ar i carir ñúle ar i *hrupuhtandor ar i nehtari ar illi i melir ar carir huru.
Inye Yésus mentane valanya *vettien lyen pa nati sine i ocombin. Inye Laviro şundo ar indyo, ar i calima arintinwe.”
I Faire ar i indis quétat: “Tula!” Ar lava yen hlare quete: “Tula!” Ar lava i soican tule; lava yen mere, mapa i nén coiviéva muntan.
Inye *vetta ilquenen i hlarir i quettar i apacéno parma sino: Qui aiquen napane sine natinnar, Eru napanuva senna i ungwaler pa yar ná técina parma sinasse;
ar qui aiquen mapa *aiqua oa i quettallon i apacéno parma sino, Eru mapuva oa rantarya i Aldasse Coiviéva ar i aire ostosse, pa yar ná técina parma sinasse.
Ye *vetta pa sine nati quete: “Ná, tuluvan rato!” Násie! Tula, a Heru Yésus!
Nai i Heru Yésus Hristo lisse euva as i airi!
Yesta i evandilyono pa Yésus Hristo.
Yesaia i *Erutercáno etécie:Yé! Mentan tercánonya epe cendelelya,manwien mallelya!
Óma queno ye yáma i ravandasse:Alde manwa i Héruo malle!Cara tieryar tére!
Sie Yoháno i *Tumyando enge i ravandasse, *nyardala *tumyale inwisto, apsenien úcariva.
Ar i quanda Yúrea-ména ar illi i marner Yerúsalemesse *etelender senna, ar anelte *tumyaine lo sé Yordan-síresse, *etequétala úcareltar.
Ar Yoháno náne vaina mi larmali findeva ulumpion ar sáme quilta *os oşwerya, ar mantes *salquecápor ar verca lís.
Ar *nyardanes, quétala: “Apa ni túla ye ná tulca lá inye – inye ua valda luhtiéno undu ar *avanutiéno hyapalattaryat.
Inye *tumya le nennen, mal sé le-*tumyuva Aire Feanen.”
Yane auressen Yésus túle Nasaretello Alileasse ar náne *tumyaina Yordande lo Yoháno.
Ar mí imya lú, túlala amba et i nenello, cennes menel şanca ar i Faire ve cucua túla undu senna.
Ar óma náne hlárina et menello: “Tyé Yondonya, i melda; pa tyé asánien mai.”
Ar mí imya lú i Faire se-tulyane mir i ravanda.
Ar anes i ravandasse auressen *canquean, nála şahtaina lo Sátan, ar anes as i hravani, ar i vali *veuyaner se.
Ar apa Yoháno náne hátina mandonna Yésus lende mir Alilea, cárala sinwa Eruo evandilyon
ar quétala: “I lúme quanta ná, ar Eruo aranie utúlie hare! Hira inwis ar sava mí evandilyon!”
Lan vantanes ara Ear Alileo cennes Símon ar Andreas, Símondo háno, hátala rembettar mir i ear, an anette *raitandoli.
Tá Yésus quente túna: “Tula apa ní, ar caruvanyet *raitandoli atanion!”
Ar mí imya lú hehtanette rembettar ar hilyanet sé.
Lendes an şinta vanta ar cenne Yácov Severaion ar Yoháno hánorya, lan anette i luntesse envinyatála rembettar,
ar mí yana lú tu-yaldes. Ar hehtanette ataretta i luntesse as i paityaine neri ar oantet apa se.
Ar lendelte mir Capernaum.Ar ve rongo ve i *sendare túle, lendes mir i *yomencoa ar peantane.
Ar peantierya te-quante elmendanen, pan peantanes ve quén arwa héreo, ar lá ve i parmangolmor.
Mí yana lú enge *yomencoaltasse nér túrina lo úpoica faire, ar yámes:
“Mana men ar lyen, Yésus Nasaretello? Ma utúliel nancarien me? Istan man nalye – Eruo Aire!”
Mal Yésus carampe tulcave senna, quétala: “Na quilda, ar ettula sello!”
Ar i úpoica faire, apa se-tyarnes rihta ar yame túra ómanen, ettúle sello.
Ar illi mici te tatallaner, ar ilquen maquente i exi: “Mana si? Vinya peantie! Hérenen canis yando i úpoice fairi, ar carilte ve quetis.”
Ar i sinyar pa sé náner palyaine i quanda Alilea-ménasse.
Ar ve rongo ve hehtanelte i *yomencoa, lendelte mir coa Símon ar Andreaswa, as Yácov ar Yoháno.
Símondo verio amil náne caimassea úrenen, ar nyarnelte sen pa sé lintiénen.
Ar lelyala senna ortanes se, mápala márya, ar i úre váne sello, ar *veuyanes tien.
Ar apa şinye túle, íre Anar náne nútiéla, i queni taller Yésunna illi i náner hlaiwe ar i náner haryaine lo raucor,
ar i quanda osto náne ocómiéla epe i fenna.
Ar nestanes rimbali i náner hlaiwe *alavéle hlívelínen, ar et-hantes rimbe raucoli, mal uas láve i raucoin quete, pan sintelte man anes.
Ar arinyave, lan en morna né, orontes ar lende ettenna ar oante erinqua nómenna, ar tasse hyamnes.
Mal Símon ar i náner óse cestaner se
ar se-hirner, ar quentelte senna: “Illi lye-cestear!”
Ono quentes téna: “Alve lelya hyana nómenna, i hari mastonnar, *nyardienyan yando tasse, an ta ná i casta yanen etelenden.”
Ar lendes, *nyardala *yomencoaltassen ter quanda Alilea, ar et-hátala i raucor.
Ar túle senna helmahlaiwa quén, ye arcane sello ar lantane occaryanta, quétala senna: “Qui meril, polilye care ni poica.”
Ar quanta *ofelmeo Yésus rahtane máryanen ar appane se, ar eques senna: “Merin! Ola poica!”
Ar mí imya lú i helmahlíve váne sello, ar anes poica.
Mal Yésus antane sen tulce canwali, ar lintiénen se-mentanes oa,
ar eques senna: “Cena i nyaril munta aiquenen, mal mena ar alye tanaxe i *airimon, ar poitielyan *yaca ya Móses canne, ve *vettie tien.”
Mal apa menie oa i nér *yestane nyare sa palan, palyala i nyarna pallave. Etta ua ence Yésun ambe lelya pantave mir osto, mal anes ettesse, eressie nómelissen. Ananta queni túler senna ilye tiellon.
Apa aureli ata túles Capernaumenna, ar náne nyárina i anes maresse.
Ar rimbali ocomner, tenna lá enge amba nóme, yando lá epe i fenna, ar quentes téna i quetta.
Ar queneli túler, tálala senna *úlévima nér cólina lo canta.
Mal i şanganen ualte polde colitas Yésunna. Etta nampelte oa i tópa or i nóme yasse anes, ar apa sapie assa mentanelte undu i *colma yasse caine i *úlévima.
Íre Yésus cenne savielta quentes i *úlévimanna: “Hína, úcarelyar nar apsénine.”
Mal enger tasse parmangolmoli, hámala ar sánala endaltasse:
“Manen nér sina lerta carpa sie? Naiquétas! Man lerta apsene úcari hequa er, Eru?”
Ono Yésus, ye mí imya lú túne faireryanen i sámelte taite sanwi, quente téna: “Mana castalda sanien nati sine endaldasse?
Mana i ambe *aşquétima, quete i *úlévimanna: Úcareldar nar apsénine, hya quete: Á orta, mapa *colmalya ar vanta!
Mal istieldan i same i Atanyondo hére apsenien úcari cemende, ” – quentes i *úlévimanna:
“Quetin lyenna: Á orta, mapa *colmalya, ar mena coalyanna!”
Tá orontes, ar mí imya lú nampes *colmarya ar menne ettenna epe te illi, ar anelte illi ara inte elmendanen, ar antanelte alcar Erun, quétala: “Taite nat ualve oi ecénie!”
Ata etelendes ara i earenna, ar i quanda şanga túle senna, ar peantanes tien.
Ar lelyala cennes Lévi yondorya Alfaio hárala i *tungwemende, ar quentes senna: “Áni hilya!” Ar orontes ar hilyane se.
Ar martane i caines ara i sarno coaryasse, ar rimbe *tungwemóli ar úcarindoli cainer tasse as Yésus ar hildoryar, an anelte rimbe queni se-hilyala.
Mal i parmangolmor imíca i Farisar, cénala i mantes as i úcarindor ar i *tungwemor, quenter hildoryannar: “Ma matis as i *tungwemor ar úcarindor?”
Hlárala si, Yésus quente téna: “I tulcar uar same maure *nestandova, mal i hlaiwar. Utúlien yalien, lá failar, mal úcarindor.”
Hildoryar Yoháno ar i Farisar sámer lamate. Ar túlelte ar quenter Yésunna: “Manen ná i hildoryar Yoháno ar i Farisaron hildor samir lamati, mal hildolyar uar same lamati?”
Ar Yésus quente téna: “Lau i endero meldor polir same lamate lan i ender ea aselte? Mí quanda lúme yasse samilte i ender endaltasse ualte pole same lamate.
Mal aureli tuluvar íre i ender nauva mapaina oa tello, ar tá samuvalte lamate, enta auresse.
*Úquen neme *lilma *alapihta lanneo yára collasse, mal qui é caris sie, quanta túrerya tuce sallo, i vinya i yárallo, ar euva ambe faica narcie.
Ente, *úquen ulya vinya limpe yáre *helmolpessen; qui caris, i limpe *ruve i *helmolpi, ar i limpe ná vanwa ar i *helmolpi nar nancarne. Mal queni panyar vinya limpe vinye *helmolpessen!”
Ar martane, íre lahtanes i orirestar i sendaresse, i hildoryar *yestaner lepta i cari oriva.
Ar i Farisar quenter senna: “Ela! Manen ná i hildolyar cárar ya ua lávina i *sendaresse?”
Mal sé quente téna: “Ma ualde oi ehentie ya Lavir carne íre sámes maure ar náne maita, sé ar i neri i náner óse?
Lendes mir coarya Eru, Avyaşar i Héra *Airimo lúmesse, ar manter i mastar taniéva, yar uar lávina matta aiquenen hequa i *airimoin, ar antanes yando i nerin i náner óse.”
Ar quentes téna: “I *sendare náne ontaina Atano márien, ar lá Atan márien i *sendareo.
Sie i Atanyondo ná Heru yando i *sendareo.”
Ata lendes mir *yomencoa, ar enge tasse nér arwa hessa máo,
ar tirneltes cenien qui nestumnes i nér i *sendaresse, ecien ten *ulquete se.
Ar quentes i nerenna arwa i hessa máo: “Á orta ar tara i endesse!”
Tá quentes téna: “Ma mo lerta i *sendaresse care márie hya care ulco, rehta quén hya nahta quén?” Mal anelte quilde.
Ar apa yétie quenello quenenna mici te arwa rúşeo, nyéresse pan endalta náne ta hranga, quentes i nerenna: “Á rahta mályanen!” Ar rahtanes sanen, ar márya náne envinyanta.
Tá i Farisar lender ettenna, ar anelte linte carien panoli senna as i Heroldili, nancarien se.
Mal Yésus as hildoryar nanwenner i earenna; ar hoa liyúme Alileallo ar Yúreallo se-hilyane.
Yando ho Yerúsalem ar Irúmea ar han Yordanello ar i ménallo *os Tír ar Síron hoa liyúme túle senna, pan hlasselte pa ilye i nati yar carnes.
Ar quentes hildoryannar i samumnelte lunte manwa sen, pustien i şanga niriello senna.
An rimbali nestanes, ar sie illi i sámer naicelie hlíveli lantaner senna appien se.
Ar i úpoice fairi, quiquie cenneltes, hantexer undu epe se ar yámer, quétala: “Elye ná i Eruion!”
Mal rimbe quettalínen carampes tulcave téna, pustien te cariello se sinwa.
Ar lendes ama mir oron ar yalde insenna i queni i sé merne, ar lendelte senna.
Ar cilles yunque, i yando estanes aposteli, náveltan óse ar etementieryan te *nyardien
ar samieltan i túre et-hatiéva raucor.
Ar i yunque i cilles náner Símon, yen antanes i epesse Péter,
ar Yoháno Severaion ar Yoháno Yácovo háno – yent antanes i epesse Voaneryes, ya tea Yondor Hundiéva –
ar Andréas ar Filip ar Vartoloméo ar Matteo ar Tomas ar Yácov yondorya Alfeo, ar Tardeo, ar Símon i Cananya,
ar Yúras Iscariot, ye apa *vartane se.Ar lendes mir coa.
Ata i şanga ocomne, ar sie yando ua ence ten mate mattalta.
Mal íre nosserya hlasse sa, lendelte mapien se, an quentelte: “Avánies et sámaryallo!”
Ente, i parmangolmor i túler undu Yerúsalemello quenter: “Náse haryaina lo Vélsevuv, ar et-hatis i raucor túrenen i heruo raucoron!”
Ar apa yalie te insenna, quentes ten sestielínen: “Manen Sátan pole et-hate Sátan?
An qui aranie ná şanca insanna, sana aranie ua pole tare;
ar qui coa ná şanca insanna, sana coa ua poluva tare.
Ente, qui Sátan ortanie insenna ar şanca ná, uas pole tare, mal túla mettaryanna.
É *úquen ye utúlie mir coa polda nerwa pole pile armaryar qui uas minyave nute i polda nér, ar tá piluvas i nati coaryasse.
Násie quetin lenna i ilye nati nauvar apsénine atanin, ilye i úcari ar naiquetier yainen naiquetilte.
Mal aiquen ye naiquete i Aire Feanna ua oi camuva apsenie, mal cole i cáma oira úcareo.”
Ta quentes pan quentelte: “Samis úpoica faire.”
Sí amillerya ar hánoryar túler, ar tárala i ettesse mentanelte minna senna, yalien se.
Şanga hamne *os se, ar quentelte senna: “Yé! Amillelya ar hánolyar i ettesse lye-cestear.”
Mal hanquentes ten: “Man nar amillinya ar hánonyar?”
Ar apa yétie quenello quenenna imíca i hamner *os se rindesse, quentes: “Ela amillinya ar hánonyar!
Aiquen ye care Eruo indóme, sé hánonya ar néşanya ar amillinya ná.”
Ar ata *yestanes peanta ara i ear. Hoa şanga ocomne hare senna, ar etta lendes mir lunte ar hamne i earesse, mal i quanda şanga ara i ear náne i hrestasse.
Ar peantanes tien rimbe natali sestielínen ar quente téna peantieryasse:
“Á lasta! Yé, i *rerindo lende ettenna rerien.
Ar rerieryasse erdeli lantaner ara i malle, ar i aiwi túler ar manter te.
Hyane erdeli lantaner i ondonna, yasse ualte sáme núra cemen, ar orontelte lintiénen, peniénen núra cemen.
Mal íre Anar oronte, anelte urtaine, ar peniénen şundo hestanelte.
Hyane erdeli lantaner imíca i neceli, ar i neceli túler ama ar quorner te, ar ualte antane yáve.
Mal exeli lantaner i mára cemenna, ar orontelte ar aller ar carner yáve, ar collelte napánine lestali, *nelquean ar *enenquean ar tuxa.”
Ar eques: “Ye same hlaru hlarien, lava sen hlare!”
Ar íre anes erinqua, i queni *os se as i yunque maquenter senna pa i sestier.
Ar quentes téna: “Elden menelo araniéno fóle ná antaina, mal in nar i ettesse ilqua marta sestiessen,
ecien ten yéta ar yéta, mal lá cene, ar ecien ten hlare ar hlare, mal lá hanya – lá oi *nanquérala inte, ar sie cámala apsenie.”
Ente, quentes téna: “Ualde ista sestie sina; tá manen hanyuvalde ilye i hyane sestier?
I *rerindo rere i quetta.
I nar ara i malle nar i queni issen i quetta ná rérina, mal íre ahlárieltes, Sátan tule mí imya lú ar mapa oa i quetta ya náne rérina tesse.
Ar mí imya lé, queni sine nar i rérinar i ondosse: Mí lú ya ahlárielte i quetta, camiltes arwe alasseo.
Ananta ualte same şundo intesse, mal lemyar ter lúme; tá, ve rongo ve şangie hya roitie marta i quettanen, lantalte oa.
Exeli nar i rérinar imíca i neceli; sine nar i ahlárier i quetta,
mal mar sino cimier ar laro úsahtie ar i íri i hyane nativa tulir minna ar quorir i quetta, ar uas cole yáve.
Ono i rérinar i mára cemende nar i lastar i quettanna ar camir sa ar colir yáve – er, napánine lestar *nelequean, ar exe, *enequean, ar exe, tuxa.
Ar quentes téna: “Lau calma ná talaina náven panyaina nu colca hya nu caima? Ma uas talaina náven panyaina i calmatarmasse?
An ea munta ya ná nurtaina hequa náven apantaina; munta anaie halyaina hequa tulieryan mir i pantie.
Qui aiquen same hlaru hlarien, lava sen hlare.”
Ar quentes téna: “Cima ya hláralde: I lestanen yanen *lestalde nauva *lestaina len; é amba nauva napánina len.
An ye same, sen nauva antaina, ar ye ua same, yando ya samis nauva mapaina sello.”
Ar eques: “Sie Eruo aranie ná ve íre nér hate erde i cemenna,
ar náse lorna lómisse ar orya auresse, ar i erde tuia ar ale halla – i nér ua ista manen.
Insanen i cemen cole yáve: minyave lasseli, tá caris cas i sirpeo, teldave quanta ore caresse i sirpeo.
Ve rongo ve i yáve ná manwa, i nér menta i circa, pan i yávie utúlie.”
Ar eques: “As mana sestuvalve Eruo aranie, ar mana i sestie ya caruvalve pa sa?
Nás ve erde *sinápio, ya i lúmesse ya nás rérina i cemende ná i ampitya ilye erdion yar ear cemende –
mal íre nas rérina, tulis ama náven hoa lá ilye hyane quear ar care hoe olvali, tenna menelo aiwi polir mare laimeryasse.”
Sie rimbe taiti sestielínen quentes ten i quetta, ilqua ya ence ten lasta.
Hequa sestiénen uas carampe téna, mal íre anes erinqua as hildoryar, antanes tien tercen pa ilye nati.
Ar mi yana ré, íre şinye náne túlienwa, quentes hildoryannar: “Alve lahta i hyana hrestanna.”
Ar apa mentie oa i şanga talleltes aselte i luntenen yasse hamnes, ar enger hyane lunteli óse.
Tá túrea raumo oronte, ar i falmar penter mir i lunte, tenna i lunte náne hare návenna quátina.
Mal sé náne pontisse i lunteo, lorna nirwasse, ar eccoitaneltes ar quenter senna: “Heru, ma ua valda lyen i quélalme?”
Tá ortanes inse ar carampe tulcave i súrinna ar quente i earenna: “Na quilda, na senda!” Ar i súre pustane, ar enge hoa quilde.
Ar quentes téna: “Manen ná i nalde caurie? Ma en ualde same savie?”
Mal hoa caure te-nampe, ar quentelte quén i exenna: “É man náse, pan yando súre yo ear carit ve quetis?”
Ar túlelte i hyana hrestanna i earo, mir ména Erasenyaiva.
Mí imya lú ya hehtanes i lunte, nér arwa úpoica faireo velle se, túlala i noirillon.
Sámes vehterya imíca i sapsar, ar tenna lúme yana *úquen ena polde nutitas, yando lá naxainen.
Rímbe lúr anes nútina nútelínen talyatse ar naxalínen, mal narcanes i naxar ar ascante i núti, ar *úquen sáme i poldore turien se.
Illume, lómisse yo auresse, anes imíca i sapsar ar i orontissen, yámala ar palpala inse sarnínen.
Íre cennes Yésus hairallo, nornes ar lantane undu epe se.
Yámala túra ómanen eques: “Mana nin ar lyen, Yésus, Eru Antaro yondo? Lye-panyan nu vanda Erunen, ávani ñwalya!”
An Yésus quente senna: “Úpoica faire, tula et i nerello!”
Ar maquentes senna: “Mana esselya?” Ar hanquentes: “Essenya Lehion, an nalme rimbe.”
Tá hormenen arcanes i ua Yésus te-mentumne et i ménallo.
Mal enge tasse ara i oron hoa polcalámáre mátala.
Ar arcanelte sello, quétala: “Áme menta mir i polcar, tulielman mir té.”
Ar láves tien. Tá i úpoice fairi ettúler ar lender mir i polcar, ar i lámáre lende rimpa olla i lanca mir i ear, nóte *os húme atta, ar quornelte i earesse.
Mal i mavari ortírala te úşer ar nyarner sa i ostosse ar i restassen, ar queni túler cenien mana náne martiéla.
Sie túlelte Yésunna, ar cennelte i nér ye nóvo náne raucoharyaina hámala málesse sámo, sé ye yá náne haryaina lo i lehion; ar anelte ruhtaine.
I cenner sa nyarner tien pa ya náne martiéla i raucoharyainan ar i polcain.
Ar *yestanelte arca se, autieryan ménaltallon.
Íre lendes mir i lunte, i nér ye yá náne raucoharyaina arcane náve as Yésus.
Ono uas láve sen, mal quente senna: “Mena coalyanna ar nosselyanna, ar nyara tien pa ilqua ya i Heru acárie lyen ar i órávie ya sámes lyesse.”
Ar lendes oa ar *yestane care sinwa ter i Ostor Quean ilqua ya Yésus carne sen, ar illi náner quátine elmendanen.
Apa Yésus ata lahtane i hyana hrestanna i luntenen, hoa liyúme ocomne senna, ar anes ara i ear.
Sí túle quén i *yomencoanturion, yeo esse náne Yairo. Cénala Yésus lantanes undu epe talyat
ar arcane se hormenen, quétala: “Cinta selyenya ná coivie-lancasse! Tula ar á panya mályat sesse, náveryan rehtaina ar hepe coivierya!”
Tá oantes óse. Ar hoa şanga hilyane se, nírala senna.
Ar enge tasse nís ñwalyaina lo celume serceva ter loar yunque.
Anes panyaina rimbe naicennar lo rimbe *nestandoli, ar apa *yuhtie ilqua ya sámes camnes munta aşea; úsie, anes sí ambe hlaiwa.
Íre hlasses pa Yésus, túles ca se i şangasse ar appane vaimarya,
pan quentes insenna: “Qui eryave appan vaimarya, nauvan nestaina!”
Ar mí imya lú i celume serceva pustane, ar túnes hroaryasse i anes nestaina naicelea hlíveryallo.
Mí imya lú Yésus túne insesse manen túre lende et sello, ar quernes immo ar quente: “Man appane vaimanya?”
Mal hildoryar quenter senna: “Cénal i şanga níra lyenna, ar tá quétal: Man ni-appane?”
Ono yentes *os se cenien i quén ye carne si.
Mal i nís, ruhtaina ar pálala, istala ya náne martiéla sen, túle ar lantane undu epe se ar nyarne sen i quanda nanwie.
Yésus quente senna: “Selye, savielya erehtie lye. Mena rainesse, ar na málesse naicelea hlívelyallo.”
Nó telyanes quete, quelli túler i coallo i *yomencoanturwa ar quenter: “Selyelya ná qualin. Manen ná i en *tarasteal i *peantar?”
Mal Yésus, hlárala ya náne quétina, eque i *yomencoanturenna: “Áva ruce, eryave sava!”
Sí uas láve aiquenen tule óse hequa Péter ar Yácov ar Yoháno, Yácovo háno.
Ar túlelte coanna i *yomencoanturwa, ar túnes i yalme ar i yaimie queni ar i yámer rimbe yaimelínen,
ar lelyala minna quentes téna: “Mana castalda carien yalme ar yaime? I hína ua aquálie, mal náse lorna.”
Tá landelte senna yaiwenen. Mal sé, apa mentie te illi ettenna, talle i híno atar ar amil ar i náner óse, ar lendes i şambenna yasse náne i hína.
Ar mápala i hino má quentes senna: “Talişa cumi!”, ya tea: “Vende, quétan tyenna, á orta!”
Ar mí yana lú i vende oronte ar vantane, an sámes loar yunque. Tá, mí imya lú, anette ara *intu túra elmendanen.
Mal cannes tun ata ar ata: “Áva lava aiquenen ista si!”, ar quentes i antumnette i venden nat matien.
Ar oantes talo ar túle atarnómeryanna, ar hildoryar hilyaner se.
Íre i *sendare túle, *yestanes peanta i *yomencoasse, ar rimbali imíca i lastaner tatallaner, ar quentelte: “Mallo nér sina camne nati sine? Ar mana sailie sina ya anaie antaina nér sinan, ar síti taure cariéli yar martar máryanten?
Ma uas i tautamo, Marío yondo, ar hánorya Yácov ar Yósef ar Yúras ar Símon? Ar uar néşaryar sisse aselve?” Ar ualte merne camitas.
Mal Yésus quente téna: “Erutercáno ua pene laitie hequa atarnómeryasse ar mici nosserya ar véra coaryasse.”
Ar uas polde care erya taura carda tasse, hequa i panyanes máryat mance engwalissen ar te-nestane.
Ar anes elmendasse pa penielta saviéva. Tá lendes nómello nómenna rindesse i mastonnar, peantala.
Sí tultanes i yunque, ar *yestanes menta te, atta ar atta, ar antanes tien túre or i úpoice fairi.
Ar cannes tien cole munta i lendan hequa vandil erinqua – lá massa, lá *mattapocolle, lá urus i quiltasse,
mal nutumnelte intesse hyapat, ar úne tien lávina cole laupe atta.
Ente, quentes téna: “Ilya coa mir ya tulilde, á lemya tasse tenna autalde nóme tanallo.
Ar ilya nómesse ya ua mere came le hya hlare le, ire autalde talo á pala oa i asto ya himya nu taluldat, ve *vettie tien.”
Ar lendelte ar *nyardaner quenin hirieltan inwis;
ar et-hantelte rimbe raucoli ar *lívelte rimbe engwali millonen ar nestaner te.
Ar Aran Herol hlasse pa Yésus, an esserya sí náne sinwa palan, ar queni quenter: “Yoháno i *Tumyando anaie ortaina qualinillon, ar etta i taure cardar mólar sesse.”
Mal exeli quenter: “Náse Elía.” En exeli quenter: “Náse Erutercáno, ve quén i Erutercánoron i vanwiéno.”
Mal íre Herol hlasse sa, quentes: “Tana Yoháno yeo cas inye aucirne, sé anaie ortaina!”
An Herol immo mentane ar nampe Yoháno ar nunte se, castanen Heroliasso, Filip hánoryo veri, pan Herol veryane senna.
An Yoháno quente Herolenna: “Ualye lerta same hánolyo veri.”
Mal Herolias sáme tevie Yohánova ar merne nahta se, mal ua polde.
An Herol runce Yohánollo, istala i anes faila ar aire nér, ar se-varyanes. Apa hlarie se, anes ita útanca pa mana náne sen i arya nat carien, ananta se-hlasses arwa alasseo.
Mal aure eciéva túle íre Herol, *nónareryasse, carne merende amminde neryain ar i cánoin ar i arátor Alileo.
Ar selyerya Herolias túle immo ar liltane ar fastane Herol ar i queni ara i sarno óse. I aran quente i vendenna: “Cana nillo *aiqua ya meritye, ar antuvanyes tyen.”
É antanes sen vandarya: “*Aiqua ya canitye nillo, antuvanyes tyen, tenna perta aranienyo!”
Ar i vende lende ettenna ar quente amilleryanna: “Mana canuvan?” Eques: “Yoháno i *Tumyando cas!”
Lintiénen lendes térave i aranna ar canne: “Merin i nin-antal, mi lúme sina, Yoháno i *Tumyando cas venesse!”
Tumna nyére nampe i aran, ananta uas merne váquete ya i vende canne, pan antanes vandaryar epe i queni ara i sarno.
Sie, mí imya lú, i aran mentane hroacundo, cánala sen tala Yoháno cas. Ar lendes oa ar aucirne carya i mandosse.
Talles carya venesse ar antane sa i venden, ar i vende antane sa amilleryan.
Íre hildoryar hlasser sa, túlelte ar ortaner loicorya ar sa-panyaner noirisse.
Ar i hildor ocomner epe Yésus ar nyarner sen ilye i nati yar carnelte ar peantanelte.
Ar quentes téna: “Tula, elde erinque, eressea nómenna ar sera pityave.” An rimbali túler ar oanter, ar ualte sáme lúme yando matien.
Ar oantelte i luntenen eressea nómenna, té erinque.
Mal queni te-cenner íre oantelte, ar rimbali sinter, ar ilye i ostollon nornelte tar talanen ar túler nó te.
Ar íre lendes et i luntello cennes hoa şanga, mal endarya etelende senna, pan anelte ve mámar ú mavaro. Ar *yestanes peanta tien rimbe natali.
Sí i lúme náne telwa, ar hildoryar túler senna ar quenter: “I nóme ná eressea, ar yando sí i lúme telwa ná.
Áte menta oa, menieltan i *oscaitala restannar ar mastonnar, mancien inten matso.”
Hanquentes téna: “Elde áten anta ya matuvalte!” Tá quentelte senna: “Ma menuvalme ñetien massali lenárin tuxa atta antien i quenin matien?
Quentes téna: “Mana nóte massaron samilde? Mena ar cena!” Apa ceşie quentelte: “Lempe, ar yando hala atta.”
Ar cannes ilye i quenin caitie undu i laica salquenna, hosta ara hosta.
Ar hamunelte, téma ara téma, mi tuxali ar *lepenquealli.
Mápala i massar lempe ar i hala atta ortanes henyat menelenna ar quente aistie, ar rances i massar ar antane tai hildoryain, panieltan tai epe i queni, ar *ciltanes i hala atta illin.
Ar illi mici te manter ar náner quátine.
Ar ortanelte massarantali, quantala *vircolcar yunque, ar enger yando i halar.
Ente, i manter i massaron náner neri húmi lempe.
Tá, lintiénen, mauyanes hildoryar mene mir i lunte ar lelya nó inse i hyana hrestanna, Vetsairanna, lan sé mentane oa i şanga.
Mal apa quetie namárie téna lendes ama orontenna hyamien.
Sí şinye náne túlienwa, ar i lunte náne i earo endesse, mal sé náne erinqua i noresse.
Ar íre cennes i tien náne urda levie ompa, an i súre náne cendeleltasse, *os i cantea tirisse túles téna, vantala i earesse, mal mernes mene han te.
Íre cenneltes vantala i earesse, sannelte: “Ta ná auşa!”, ar etyámelte.
An illi mici te cenner se ar náner ruhtaine. Mal mí imya lú carampes téna, ar quentes téna: “Huore! Inye ná; áva ruce!”
Ar túles mir i lunte aselte, ar i súre firne undu.
Ono té náner quátine túra elmendanen, an ualte ñente hande i massainen, mal endalta náne en hranga.
Ar apa lahtie i nórenna túlelte Ennesaretenna ar tancer i lunte tasse.
Mal íre lendelte et i luntello queni lintiénen *atsinter Yésus,
ar nornelte *os i quanda ména tana, ar *yestanelte panya illi i náner hlaiwe *colmalissen ar colitat yanna hlasselte i anes. Ar ilya nóme yasse lendes mir mastor hya ostor hya restar, panyanelte i hlaiwar mí *mancanómi, ar inquelte sello appa eryave larmaryo lane, ar i ilye i queni appala sa náner nestaine.
Sí ocomner *os Yesus i Farisar ar queneli i parmangolmoron i náner túlienwe Yerúsalemello.
Cennelte queneli hildoryaron máta mattalta *alairi – ta ná, *alasóvine – mainen.
Mal Farisar ar ilye Yúrar uar mate ú soviéno máltat tenna i óleme, himyala i yáraron sito,
ar entúlala i *mancanómello ualte mate ú poitiéno inte hatiénen nén. Ear rimbe hyane situli yar acámielte ar hepir, *tumyala yulmar ar *ulmali ar urusteli.
Ar i Farisar ar i parmengolmor maquenter senna: “Manen ná i hildolyar uar lenga ve i yáraron sito, mal matir mattalta *alairi mainen?”
Quentes téna: “Yesaia carampe mai ve *Erutercáno pa lé *imnetyandor, ve ná técina: 'Lie sina nin-anta alcar péltanten, mal endalta ná haira nillo.'
Lustave ni-*tyerilte, peantala Atanion peantier.'
Hehtala Eruo axan himyalde Atanion sito.”
Ente, quentes téna: “Fincave panyalde oa Eruo axan, himien véra situlda.
An Móses quente: 'Alye anta alcar atarelyan yo amillelyan,' ar: 'Ye quete ulco pa atar hya amil qualuva.'
Mal lé quetir: Íre nér quete ataryanna hya amilleryanna: '*Aiqua aşea ya camumnetye nillo ná corvan' (ta ná, anna Erun),
ualde ambe lave sen care erya nat ataryan hya amilleryan.
Sie, i situnen ya antalde voro, tyarilde Eruo quetta pene túre. Ar rimbe vávie natali carilde.”
Ar ata tultala i şanga quentes téna: “Á lasta ninna, illi mici le, ar á hanya!
Ea munta ettello ya pole care quén úpoica tuliénen minna se, mal i nati yar ettulir quenello nar yar se-carir úpoica.
Hlara, aiquen ye same hlaru hlarien!”
Ar íre lendes mir coa, oa i şangallo, hildoryar maquenter senna pa i sestie.
Tá quentes téna: “Tá yando elde penir hande? Ma ualde ista i munta ettello ya tule mir nér pole vahta se,
pan ta lelya, lá mir endarya, mal mir hirdiryar, ar autas mir i *aucelie.” Sie tenges i ilya nostaleo matta poica ná.
Ar quentes: “Ya tule et quenello ná ya vahta quén,
an mityallo, et atanion endallo, tulir i ulce sanwi: *hrupuhtaler, pilwi, nahtier,
vestaracier, milce felmi, olce cardar, *ñaume, lehta lengie, ulca hen, naiquetie, *valate, *úhande.
Ilye ulqui sine tulir mityallo ar vahtar quén.”
Talo orontes ar lende mir i ménar Tíro ar Sírono. Ar lendes mir coa ar ua merne i aiquen istumne, mal uas polde lemya muina.
Rongo nís yeo selye náne haryaina lo úpoica faire hlasse pa se ar túle ar lantane undu epe talyat.
I nís náne Hellenya, Siro-Foinicea ontaleo, ar arcanes sello i et-hatumnes i rauco selyeryallo.
Mal Yésus quente senna: “Minyave lava i hínin quate inte, an ua vanima mapa i massa i híniva ar hatitas i huonnar.”
Mal hanquentes senna: “Ná, heru, ananta i huor nu i sarno matir i hínion mier.”
Tá quentes i nissenna: “Pan quentes si, mena; i rauco oantie et selyelyallo.”
Ar i nís lende oa coaryanna ar hirne i hína caitala i caimanna, ar i rauco náne vanwa.
Ar entúlala et ménallon Tíro, Yésus lende ter Síron earenna Alileo, ama ter ende i ménaron yassen cainer i Ostor Quean.
Sisse tallelte senna nér ye náne *hlárelóra ar úpa, ar arcanelte sello i panyumnes márya sesse.
Ar se-tulyanes oa i şangallo eressea nómenna ar panyane leperyar mir i nero hlaru ar, apa piutie, appanes lambarya.
Apa yétie ama menelenna Yésus síque ar quente senna: “Effata!” – ta ná: “Na latyaina!”
Ar *hlárerya náne latyaina, ar lambaryo núte náne lehtaina, ar carampes şanyave.
Ar canneset i quetumnelte munta aiquenna, mal i ambe te-cannes, i ambe sa-carnelte sinwa.
É anelte quátine antumna elmendanen ar quenter: “Acáries ilye nati mai, ar tyaris i *hlárelórar hlare ar i úpar quete!”
Yane auressen, lan ata enge hoa şanga óse ar sámelte munta matien, tultanes hildoryar ar quente téna:
“Endanya etelelya i şanganna, an yando sí elémielte asinye ter auri nelde, ar samilte munta matien.
Qui mentanyet oa marenna lustacumbe, nauvalte acca lumbe i mallesse. Ente, queneli mici te utúlier hairallo.”
Mal hildoryar hanquenter senna: “Mallo aiquen, eressea nóme sinasse, ñetuva massar fárie sine quenin?”
Mal maquentes téna: “Mana nóte massaron samilde?” Quentelte: “Otso.”
Ar cannes i şangan caita undu i talamenna, ar nampes i massar otso, antane hantale, rance tai ar antane tai hildoryain, panieltan tai epe i queni, ar panyaneltet epe i şanga.
Sámelte yando mance halali, ar apa aistie tai, cannes tien panya yando tai epe i queni.
Ar mantelte ar náner quátine, ar comyanelte i lemyala rantar, quátala matsocolcar otso.
Ananta nóte i quenion náne *os húmi canta. Ar mentanéset oa.
Tá, lintiénen, lendes mir i lunte as hildoryar ar túle i ménannar *os Lalmanúşa.
Sisse i Farisar ettúler ar *yestaner costa óse, cestala sello tanwa menello, tyastien se.
Ar síques faireryasse ar quente: “Mana i casta yanen *nónare sina cesta tanwa? Násie quetin: *Nónare sina ua camuva tanwa.”
Ar te-hehtala nanwennes i luntenna ar oante i hyana hrestanna.
Mal ualte *renne mapa massar aselte, ar hequa massa er sámelte munta aselte i luntesse.
Ar cannes tien, quétala: “Cima, ettira pa Farisaron *pulmaxe ar Herolo *pulmaxe.”
Ar sámelte cos, quén as i exe, pa penielta massar.
Tuntala si, quentes téna: “Mana castalda samien cos pa penie massar? Ma en ualte tunta ar hanya? Ma endalda lenca handeo ná?
Arwe henduo, lau cenilde, ar arwe hlaruo, lau hlarilde? Ar ma ualde enyale,
íre rancen i massar lempe i nerin húmi lempe, i nóte *vircolcaron quante rantaron comyanelde?” Quentelte senna: “Yunque.”
“Íre rancen i otso i nerin húmi canta, mana nóte hoe matsocolcaron quante rantaron comyanelde?” Ar quentelte senna: “Otso.”
Tá quentes téna: “Ma en ualde hanya?”
Ar túlelte Vetsairanna. Sisse queni coller senna lomba nér, ar arcanelte sello appa se.
Ar nampes i *cénalóra nero má ar talle se ettenna i masto, ar apa piutie henyanta panyanes máryat sesse ar maquente senna: “Ma cénal *aiqua?”
Ar i nér yente ama ar quente: “Cénan i queni, an yétan ya *şéya ve vantala aldali!”
Tá panyanes ata máryat henyatse, ar cennes aqua mai; anes envinyanta ar cenne i cantar ilye nation.
Tá se-mentanes marenna, quétala: “Áva lelya mir i masto!”
Yésus ar hildoryar sí oanter i mastonnar mi Caisarea-Filippi, ar i mallesse maquentes hildoryannar, quétala téna: “Man queni quetir i nanye?”
Quentelte senna: “Yoháno i *Tumyando, ar exeli: Elía, ar exeli: Quén imíca i Erutercánor.”
Ar maquentes téna: “Mal man elde quetir i nanye?” Péter hanquente senna: “Elye ná i Hristo!”
Tá cannes tien tulcave i ávalte nyerumne aiquenen pa se.
Ente, *yestanes peanta tien i mauyane i Atanyondon perpere rimbe ñwalmeli ar náve *auquerna lo i amyárar ar i hére *airimor ar i parmangolmor, ar náve nanca, ar orta apa auri nelde.
É quentes ta pantave. Mal Péter se-tulyane véra nómenna ar *yestane quete ana se.
Ono Yésus quernexe, ar cénala hildoryar carampes tulcave Péterenna, quétala: “Heca nillo, Sátan, an sanwelyar uar Eruo sanwi, mal tai atanion.”
Sí yaldes i şanga insenna as hildoryar ar quente téna: “Qui aiquen mere tule apa ni, mauya sen váquete insen ar orta tarwerya ar ni-hilya.
An aiquen ye mere rehta cuilerya, sen nauvas vanwa, mal aiquen yeo cuile ná vanwa márienyan hya márien i evandilyono, rehtuva sa.
É manen aşea ná quenen qui camis i quanda mar, mal cuilerya vanwa ná?
É mana antuva quén quaptalesse cuileryan?
An aiquen ye ná *naityana pa ni ar quattanyar mi *nónare sina ya race vestale ar ná quanta úcareo, pa sé i Atanyondo nauva *naityana íre tuluvas Ataryo alcaresse as i airi vali.”
Ente, quentes téna: “Násie quetin lenna: Ear queneli i tárar sisse i laume tyvavuvar qualme nó ecénielte Eruo aranie túlienwa túresse.”
Ar apa auri enque Yésus tulyane oa Péter ar Yácov ar Yoháno as inse ar te-talle mir tára oron, té erinque. Ar anes vistaina epe te,
ar larmaryar mirilyaner ninquissenen ambela ya aiquen sóvala lanni cemende pole ninquita.
Ente, Elía yo Móses tannet intu tien, ar carampette as Yésus.
Tá Péter carampe ar quente Yésunna: “Ravi, mára ná i nalme sisse! Alve orta *lancoar nelde, er lyen ar er Mósen ar er Elían”
– an uas sinte mana náne i arya sen quete, an anelte ruhtaine.
Ar enge lumbo teltala te, ar óma túle et i lumbollo: “Si ná yondonya, i melda; á lasta senna!”
Rincenen, íre cennelte *os inte, ualte ambe cene aiquen aselte hequa Yésus erinqua.
Ar lan ununtelte i orontello cannes tien i ávalte nyarumne aiquenna pa yar cennelte, tenna apa i Atanyondo náne ortaina qualinillon.
Cimnelte i quetta, mal carampelte mici inte pa mana “ortie qualinillon” tenge.
Ar maquentelte senna, quétala: “Manen ná i quetir i parmangolmor i minyave mauya Elían tule?”
Quentes téna: “Elía é tule minya ar envinyatuva ilye nati, mal mana i casta i ná técina pa i Atanyondo i mauya sen cole rimbe ñwalmeli ar náve nótina ve munta?
Mal quetin lenna i Elía é utúlie, ar carnelte sen ilqua ya mernelte, ve ná técina pa sé.”
Ar íre túlelte i hyane hildonnar, cennelte hoa şanga *os te, ar parmangolmoli costala aselte.
Mal ve rongo ve i quanda şanga cenne se, anelte captaine, ar nórala senna *suilaneltes.
Ar maquentes tello: “Pa mana sámalde cos aselte?”
Ar quén i şango hanquente senna: “*Peantar, tallen yondoya lyenna pan náse haryaina lo úpa rauco;
ar ilya lú yasse se-mapas, se-hatis talamenna, ar falastas ar mule nelciryar ar ná cárina tauca. Ar arcanen hildolyallon i *etehatumneltes, mal ualte polde.”
Ar carampes ar quente téna: “Úvoronda *nónare, manen andave mauya nin náve aselde? Manen andave mauya nin perpere le? Áse tala ninna! ”
Ar talleltes senna. Mal íre i faire cenne se, lintiénen tyarnes i seldo rihta, ar apa lantie undu anes pélala i talamesse, falastala.
Ar Yésus maquente ataryallo: “Manen andave si amartie sen?” Eques: “Ho anes hína.
Rimbe lúli se-aháties mir náre ar mir nén véla, nancarien se. Mal qui polil, áment manya, orávala metse!”
Yésus quente senna: “Quetta sina: Qui polil! Ilye nati nar cárime yen save!”
Mí imya lú, yámala, i híno atar quente: “Savin! Ánin manya yasse penin savie!”
Yésus, íre túnes şanga ocóma, carampe tulcave i úpoica fairenna, quétala senna: “A úpa ar *hlárelóra faire, inye cána lyen: Á auta sello ar áva lelya minna se ata!”
Ar apa yamie ar tyarie rimbe rihtiéli ettúles; ar i seldo náne ve qualin. Etta i amarimbar quenter: “Aquálies.”
Mal Yésus nampe márya ar ortane se, ar orontes.
Ar apa Yésus lende mir coa, hildoryar maquenter senna lan anelte erinque: “Manen ná i elme ua polde et-hatitas?”
Ar quentes téna: “Síte rauco ua *et-hátima hequa hyamiénen.”
Talo oantelte ar lender ter Alilea, mal uas merne i aiquen istumne ta.
An peantanes hildoryain ar nyarne tien: “I Atanyondo nauva antaina mannar atanion, ar nahtuvaltes, mal ómu nauvas nanca, ortuvas apa auri nelde.”
Mal ualte hanyane i quetie, ar runcelte maquetiello senna.
Ar túlelte mir Capernaum. Lan anes i coasse maquentes téna: “Pa mana sámelde cos i mallesse?”
Anelte quilde, an i mallesse sámelte cos pa man mici te i antúra né.
Etta hamunes ar yalde i yunque ar quente téna: “Qui aiquen mere náve minya, mauya sen náve métima illion ar náve illion núro.”
Ar nampes hína, tyarne se tare endeltasse ar panyane rancuryat *os se ar quente téna:
“Aiquen ye came taiti híni essenyanen, came ní; ar aiquen ye came ni, ua came ni, mal ye ni-mentane.”
Yoháno quente senna: “*Peantar, cennelme quén et-háta raucoli, ar névelme pusta se, pan uas vi-hilyane.”
Mal eque Yésus: “Ávase neve pusta, an ea *úquen ye caruva túrea carda essenyanen ye rongo quetuva ulco pa ni.
An ye ua tare venna, elven ná.
An aiquen ye anta len yulma neno sucien návelyanen Hristova – násie quetin lenna, *paityalerya ua nauva vanwa sen.
Mal aiquen ye tyare i lante queno mici pityar sine i savir nisse, ná arya sen qui *mulondo i nostaleo túcina lo *pellope ná panyaina *os yahtya, ar náse hátina mir ear.
Ar qui málya tyára lantelya, ása aucire; menie mir coivie ú hroaranto ná lyen arya lá autie arwa má atto mir Ehenna, mir i náre ya urya tennoio.
Ar qui talalya tyára lantelya, ása aucire; menie *úlévima mir coivie ná lyen arya lá náve hátina mir Ehenna arwa tál atto.
Ar qui hendelya tyára lanterya, ása hate oa, an menie mir Eruo aranie arwa er hendo ná lyen arya lá náve hátina mir Ehenna arwa hen atto,
yasse vembelta ua quale ar i náre ua *luhtyaina.
An ilquen nauva carna singwa singenen.
Singe mára ná, mal qui i singeo túre avánie, mananen antuvalde tyáve i singe imman? Sama singe eldesse, ar hepa raine imbe quén ar quén.”
Ar talo orontes ar túle i ménannar Yúreo ar han Yordanna, ar ata şangali ocomner senna, ar haimeryanen ata peantanes tien.
Farisáli túler senna, ar tyastien se maquentelte senna qui nér lerta lehta inse veriryallo.
Hanquentes ar eque téna: “Mana Móses canne len?”
Quentelte: “Móses láve i tecie tecettava *aumentiéva ar i mo lehta inse sello.”
Mal Yésus quente téna: “Hranga endaldanen tences len axan sina.
Mal i ontiéno yestallo carnes tu hanu yo ní.
Etta nér hehtuva atarya yo amillerya,
ar i atta nauvat er hráve. Sie uatte ambe atta, mal er hráve.
Etta, ya Eru acárie er, atan áva *cilta.”
Lan anelte i coasse i hildor ata maquenter senna pa si.
Ar quentes téna: “Aiquen ye lehtaxe veriryallo ar verya exenna care vestaracie senna,
ar qui nís, apa lehtie inse veruryallo, verya exenna, caris vestaracie.”
Sí queni taller senna hínali, appieryan te, mal i hildor caramper téna naraca lénen.
Ono Yésus, cénala si, náne rusca ar quente téna: “Lava i hínin tule ninna; áva neve pusta te, an taition Eruo aranie ná.
Násie quetin lenna: Aiquen ye ua came Eruo aranie ve hína, laume tuluva mir sa.”
Ar nampes i hini mir rancuryat ar aistane te, panyala máryat tesse.
Ar íre oantes tieryasse, nér norne senna ar lantane occaryanta epe se ar maquente senna: “Mane *peantar, mana mauya nin care, náven aryon oira coiviéno?”
Yésus quente senna: “Manen ná i estal ní mane? *Úquen mane ná, hequa er, Eru.
I axani istal: Áva nahta, áva care vestaracie, áva pile, áva quete únanwa *vettie, áva na *ñauna, á anta alcar atarelyan yo amillelyan.”
I nér quente senna: “*Peantar, ilye nati sine ihímien néşenyallo.”
Yésus yente se ar méle se, ar quentes senna: “Er nat penil. Mena, vaca ilqua ya samil ar ása anta penyain, ar samuval harma menelde. Tá tula ar áni hilya.”
Mal endarya lantane i quetiénen, ar oantes nyéresse, an sámes rimbe armali.
Apa yétie quenello quenenna Yésus quente hildoryannar: “Manen urda tule mir Eruo aranie ná quenin arwe telpeo!”
Mal hildoryar tatallaner quettaryainen. Ono Yésus carampe ata ar quente téna: “Híni, manen urda tule mir Eruo aranie ná!
Ulumpen mene ter nelmo assa ná ancárima lá lára quenen tule mir Eruo aranie.”
Anelte aqua captaine ar quenter mici inte: “Tá man pole náve rehtaina?”
Yétala te, Yésus quente: “Atanin nas úcárima, mal lá Erun, an ilye nati nar cárime Erun.”
Péter *yestane quete senna: “Yé, elme ehehtier ilqua ar ihílier lyé.”
Eque Yésus: “Násie quetin lenna: Ea *úquen ye ehehtie coa hya hánor hya amil hya néşar hya atar hya híni hya restar márienyan ar márien i evandilyono,
ye ua camuva napánine lestar tuxa mi randa sina, coar ar hánor ar néşar ar amilli ar híni ar restar, as roitier – ar mí túlala randa oira coivie.
Mal rimbali i nar minye nauvar métime, ar i métimar minye.”
Sí anelte i mallesse ortala Yerúsalemenna, ar Yésus lende epe te. Ar anelte elmendasse, mal i hilyaner runcer. Ata talles i yunque véra nómenna ar *yestane carpa téna pa yar martumner sen:
“Yé, lelyealve ama Yerúsalemenna, ar i Atanyondo nauva antaina olla i hére *airimoin ar i parmangolmoin, ar namuvaltes qualmen ar antuvar se olla i *nóreain,
ar laluvalte senna yaiwenen ar piutuvar senna ar se-riptuvar ar se-nahtuvar, mal apa auri nelde ortuvas.”
Ar Yácov ar Yoháno, yondo atta Severaio, túlet senna ar quentet senna: “*Peantar, merimme i caruval ment ya arcamme lyello.”
Quentes túna: “Mana meriste i caruvan lent?”
Quenteste senna: “Áment anta i haruvamme, quén ara formalya ar quén ara hyarmalya, alcarelyasse.”
Mal Yésus quente túna: “Uaste ista ya arceaste. Ma poliste suce i yulma ya inye súca, hya náve *tumyaine i *tumyalénen yanen inye ná *tumyaina?”
Quentette: “Polimme.” Tá Yésus quente túna: “I yulma ya inye súca sucuvaste, ar i *tumyalénen yanen inye ná *tumyaina nauvaste *tumyaine.
Ono i harie undu foryanyasse hya hyaryanyasse ua nin antien, mal nas i quenin in anaies sátina.”
Íre i hyane quean hlasser pa si, *yestanelte náve rúşie Yácovenna ar Yohánonna.
Mal yaliéla te insenna, Yésus quente téna: “Ve istalde, i queni i *şéyar náve nórion heruvi samir hére or te, ar túraltar samir túre or te.
Mal ua ea sie mici lé. Úsie, aiquenen ye mere náve túra mici lé mauya náve núrolda,
ar aiquenen ye mere náve minya mici lé mauya náve illion mól.
An yando i Atanyondo túle, lá samien núror, mal náven núro ar antien cuilerya ve *nanwere quaptalesse rimbalin.”
Ar túlelte mir Yerico. Mal lan sé ar hildoryar ar hoa şanga náner tieltasse et Yericollo, Vartiméo yondorya Timéo, lomba *iquindo, hamne ara i malle.
Íre hlasses i si náne Yésus Nasaretello, *yestanes yame ar quete: “Lavirion, Yésus, órava nisse!”
Tá rimbali caramper tulcave senna náveryan quilda, mal i ambe yámes: “Lavirion, órava nisse!”
Ar hautiéla Yésus quente: “Áse yale!” Ar yaldelte i lomba nér, quétala senna: “Huore! Á orta, lye-yálas!”
Hátala oa collarya campes ama ar túle Yésunna.
Ar Yésus carampe senna ar quente: “Mana meril i caruvan lyen?” I lomba nér quente senna: “Rappóni, lava nin came *céne.”
Ar Yésus quente senna: “Mena! Savielya erehtie lye.” Ar mí imya lú camnes *céne, ar se-hilyanes i mallesse.
Ar íre túlelte hare Yérusalemenna, ana Vet-Fahe ar Vetánia ara Oron *Milpioiva, mentanes atta hildoryaron
ar quente túna: “Mena mir enta masto epe let, ar ve rongo ve tuliste minna sa, hiruvaste *pellope nútina, yesse *úquen atanion ahámie tenna sí; áse lehta ar áse tulya sir.
Ar qui aiquen quete lenta: Mana castasta carien si? – tá queta: I Heru same maure séva, ar rongo se-*nanwentuvas.”
Ar oantette ar hirnet i *pellope nútina ara i fenna, mí ette i *aramallesse, ar lehtanettes.
Mal queneli imíca i tarner tasse quenter túna: “Mana cáraste lehtala i *pellope?”
Quentette téna aqua ve Yésus canne, ar lávelte tun auta.
Ar tallette i *pellope Yésunna, ar panyanette collattar sesse, ar Yésus hamne sesse.
Ente, rimbali pantaner collaltar i mallesse, mal exeli cirner olvali i restallon.
Ar i lender epe se ar i túler ca se yámer: “Hosanna! Na aistana ye túla i Héruo essenen!
Na aistana i túlala aranie Lavir atarelvo! Hosanna i tarmenissen!”
Ar lendes mir Yerúsalem, mir i corda, ar yentes ilye nati. Mal yando tá i lúme náne telwa, ar sie etelendes Vetánianna as i yunque.
I hilyala auresse, íre anelte túlienwe et Vetániallo, anes maita.
Hairallo cennes *relyávalda arwa lasselion, ar lendes cenien qui cé hirumnes *relyáveli sesse. Mal íre túles senna, hirnes munta hequa lasseli, an ta úne i lúme *relyávion.
Ar carampes ar quente senna: “Nai *úquen oi matuva yáve lyello ata!” Ar hildoryar hlasser sa.
Sí túlelte Yerúsalemenna. Tasse lendes mir i corda ar *yestane et-hate i vancer ar i mancaner inten i cordasse, ar nuquernes i sarnor i queniva i quaptaner telpe ar i hammar iva vancer cucuar,
ar uas láve aiquenen cole tamma ter i corda.
Mal peantanes ar quentes: “Ma ua técina: 'Coanya nauva estaina coa hyamiéva ilye i nórin'? Mal elde acárier sa rotto piluiva!”
Ar i hére *airimor hlasser sa, ar cestanelte manen ence ten nancare se, an runcelte sello, an peantierya quante i quanda şanga elmendanen.
Íre i lúme náne telwa, lendelte et i ostollo.
Mal íre lahtanelte arinya lúmesse, cennelte i *relyávalda hestiéla i şundullon.
Ar enyálala sa, Péter quente senna: “Rappi, ela! I *relyávalda ye huntel hestiéla ná.”
Ar hanquentes ar eque: “Sama savie Erusse.
Násie quetin lenna i aiquen ye quete sina orontenna: 'Na ortaina ar hátina mir ear!', ar uas útanca endaryasse mal save i ya quétas martuva, sen martuvas.
Etta quetin lenna: Ilye i nati pa yar hyamilde ar arcalde, sava i acámieldet, ar ta martuva len.
Ar íre tarilde hyámala, apsena *aiqua ya samilde aiquenna. Tá Atarelda ye ea menelde yando apsenuva len ongweldar.”
Ar túlelte ata Yerúsalemenna. Tá i hére *airimor ar i parmangolmor ar i amyárar túler senna
ar quenter senna: “Mana hérelya carien nati sine? Hya man antane lyen hére sina carien nati sine?”
Yésus quente téna: “Maquetuvan lello maquetie er. Alde hanquete nin, ar yando inye nyaruva len pa i hére yanen carin nati sine.
Yoháno *tumyale, ma anes menello hya atanillon? Ánin hanquete!”
Ar carnelte úvie mici inte, quétala: “Qui quetilve: Menello, quetuvas: Tá manen ná i ualde sáve sesse?
Mal ma veryalve quete: Atanillon – ?” Runcelte i şangallo, an ilye i queni sáver i Yoháno é náne *Erutercáno.
Ar hanquentelte Yésunna ar quenter: “Ualme ista.” Ar Yésus quente téna: “Tá yando inye váquete nyare len pa i hére yanen carin nati sine.”
Ar *yestanes quete téna sestielínen: “Nér empanne tarwa liantassion, pelle sa pelonen, sampe unque i *limpevorman ar carastane mindo. Tá láves *alamólin *yuhta sa telpen, ar lende ettelea nórenna.
Íre i lúme túle, mentanes mól i *alamonnar camien i *alamollon yáveli i tarwallo.
Mal nampelte i mól, se-palpaner ar se-menter oa lusta.
Ar ata mentanes téna hyana mól, ar sé pentelte i caresse ar lenganer senna yaiwenen.
Ar exe mentanes, ar sé nacantelte, ar rimbe exeli: Queneli rípelte ar queneli nacantelte.
Enge óse an er, melda yondo. Mentanes téna sé métima, quétala: “Samuvalte áya yondonyan.”
Mal i *alamor quenter mici inte: 'Si ná i aryon. Tula, alve nahta se, ar elve haryuvar i aryono masse!'
Ar nampeltes ar nacanter se, ar hanteltes et i tarwallo.
Mana, tá, caruva i *tarwantur? Tuluvas ar nancaruvas i *alamor ar antuvas i tarwa exelin.
Lau hentanelde tehtele sina? 'I ondo ya i şamnor querner oa, sá olólie cas i vinco.
I Hérullo nat sina utúlie, ar nás elmenda hendulmatse.'”
Ar rincelte mapa se, mal runcelte i şangallo. An sintelte i pa té quentes i sestie. Ar lendelte sello ar oanter.
Tá mentanelte senna queneli Farisaron ar Heroldilion, mapien se questaryasse.
Apa tulie senna quentelte senna: “*Peantar, istalme i nalye nanwa ar ua cime aiquen, an ualye yéta atanion cendele, mal peantalye Eruo malle nanwiesse: Ma lertalme paitya *tungwe i ingaranen, hya lá? Ma mauya men paitya, hya lá paitya?”
Mal sé, tuntala *imnetyalelta, quente téna: “Mana castalda tyastien ni? Á tala ninna lenár, cenienyan sa!”
Té taller sen er. Ar quentes téna: “Mano ná emma sina, ar i tecie?” Té quenter senna: “I Ingarano.”
Tá Yésus quente téna: “Á *nampaitya i Ingaranen i Ingarano nati, mal Eruo nati Erun.” Ar anelte elmendasse pa se.
Tá túler senna Sarducali, i quetir i ua ea enortie, ar maquentelte senna:
“*Peantar, Móses tence ven i qui aiqueno háno quale ar hehta veri, mal ua enge hína óse, mauya hánoryan verya i nissenna ar orta erde hánoryan.
Enger hánor otso, ar i minya veryane nissenna, mal íre qualles, uas hehtane erde.
Ar i attea veryane senna, mal qualles ú hehtiéno erde, ar sie yando i neldea.
É ilye i otso uar hehtane erde. Métima illion i nís yando qualle.
I enortiesse, íre oryuvalte, man imíca i neri se-samuva ve veri? An anes veri ilye nerion otso.”
Yésus quente téna: “Ma si ua loimaldo casta, loitielda ista i Tehtele ar Eruo túre véla?
An íre oryuvalte qualinillon, neri uar verya ar nissi uar vertaine, mal nalte ve vali menelde.
Mal pa qualini, i nalte ortaine, ma ualde hentane Móseo parmasse, nyarnasse i neceltusso, manen Eru quente senna: 'Inye Avrahámo Aino ar Ísaco Aino ar Yácovo Aino' – ?
Uas qualinion Aino, mal cuinaron. Loitalde palan.”
Ar apa hlarie costelta, quén i parmangolmoron túle hare, an túnes i Yésus hanquente téna mai. Maquentes senna: “Mana ná i minya ilye axanion?”
Yésus hanquente: “I minya ná: Hlara, Israel! I Héru Ainolva, i Héru ná er,
ar alye mele i Héru Ainolya quanda endalyanen ar quanda fealyanen ar quanda sámalyanen ar quanda melehtelyanen.
I attea ná si: Alye mele armarolya ve imle. Lá ea hyana axan túra epe sine.”
I parmangolmo quente senna: “*Peantar, quentel mai ve nanwa ná: Náse er, ar ea *úquen hequa sé;
ar melie sé i quanda endanen ar i quanda handenen ar i quanda melehtenen, ar melie queno armaro ve immo, ná mirwa ambela ilye urtaine *yancar ar *nancayancar.”
Tá Yésus, tuntala i hanquentes handenen, quente senna: “Ualye haira Eruo araniello.” And *úquen ambe veryane maquete senna.
Ono Yésus carampe ar quente lan peantanes i cordasse: “Manen ná i quetir i parmangolmor: 'I Hristo ná Laviro yondo' – ?
I Aire Feanen Lavir immo quente: “I Héru quente herunyanna: 'Hama ara formanya tenna panyan ñottolyar nu talulyat.'
Lavir immo esta se heru; tá manen náse yondorya?”Ar i hoa şanga lastane senna alassenen.
Ar peantieryasse quentes: “Tira inde pa i parmangolmor i merir vanta ande collassen ar merir náve *suilaine i *mancanómessen
ar merir i minye sondar i *yomencoassen ar i amminde nómi i *şinyemattissen,
ar i ammatir *verulóraron coar ar carir ande hyamier náven cénine lo exi. Té camuvar ambe lunga námie.”
Ar hamunes hare i harwenna ar tirne manen i şanga hanter telpe mir i harwe; ar rimbe lárali hanter rimbe culustali minna sa.
Tá penya *verulóra túle ar antane pitye *urustamitta atta, yat únet mirwe.
Ar Yésus yalde hildoryar insenna ar quente téna: “Násie quetin lenna: Penya *verulóra sina antane amba lá ilye i exi i antaner i harwen.
An illi mici té antaner et úveltallo, mal sé et maureryallo antane ilqua ya sámes, quanda laulestarya.”
Ar íre lendes et i cordallo quén mici hildoryar quente senna: “*Peantar, ela! Yé i ondor ar yé i ataqui!”
Mal Yésus quente senna: “Ma cénal hoe ataqui sine? Laume lemyuva sisse ondo ondosse ya ua nauva hátina undu.”
Ar lan hamnes Orontesse *Milpioiva olla i cordallo, Péter ar Yácov ar Yoháno ar Andréas maquenter senna, sí íre anelte erinque:
“Nyara men, mana i lúme yasse nati sine martuvar, ar mana i tanwa i ilye nati sine nauvar hari náven telyaine?”
Ar Yésus *yestane quete téna: “Cima i *úquen tyare le ranya.
Rimbali tuluvar essenyasse quétala: “Inye sé!”, ar tyaruvalte rimbali ranya.
Ar íre hlarilde pa ohtar ar sinyar ohtaron, áva na ruhtaine! Nati sine maurenen martuvar, mal i metta ua en.
An nóre ortuva nórenna ar aranie aranienna, euvar *cempaliéli rimbe nómelissen, euvar saiceléli. Nati sine nar yesta naicelion.
Ar elde, tira inde! Queni antuvar le olla námocombin, ar nauvalde palpaine *yomencoassen ar tyárine tare epe nórecánoli ar arani pa inye, ve *vettie tien.
Ar maurenen i evandilyon nauva minyave carna sinwa ilye i nóressen.
Mal íre tulyalte le ompa antien le olla, áva *tarasta inde nóvo pa mana quetuvalde; mal ilqua ya nauva antaina len mi enta lúme, queta ta, an elde uar i quétar, mal i Aire Fea.
Ente, háno antuva háno olla qualmenna, ar atar hína, ar híni ortuvar nostarunna ar tyaruva qualmetta,
ar nauvalde tévine lo illi pa essenya. Mal ye voronwa ná i mettanna, sé nauva rehtaina.
Mal íre cenilde i Faica Nat Nancariéva tare yasse ua vanima nómerya” – lava yen hentea hanya – “tá mauya in nar Yúreasse uşe mir i oronti.
I nér tópasse coaryo áva tule undu hya mene mityanna mapien *aiqua et coaryallo,
ar i nér i restasse áva nanwene i nómenna yallo túles ñetien collarya.
Horro i *lapsarwe nissin ar in *tyetir vinimor ente auressen!
Hyama i uas martuva hrívesse,
an ente auressen tuluva şangie nostaleo ya ua amartie yestallo i ontiéno ya Eru ontane tenna enta lúme, ar uas martuva ata.
Ar qui i Héru ua nuhtane i auri, aqua ua enge hráve nála rehtaina. Mal i cílinain i ícílies unuhties i auri.
Ar qui aiquen tá quete lenna: 'Yé! Sís ná i Hristo!', hya: 'Ela, tasse náse!', áva save.
An *huruhristoli ar *hurutercánoli ortuvar ar caruvar tanwali ar elmendali, tyarien ranya – qui ta ná cárima – i cílinar.
Mal alde cime; len-anyárien ilqua nóvo.
Mal ente auressen, apa enta şangie, Anar oluva morna, ar Işil ua antuva calarya,
ar i tinwi lantuvar menello, ar i túri yar ear menelde nauvar páline.
Ar tá cenuvalte i Atanyondo túla fanyalissen arwa taura túreo ar alcaro.
Tá mentuvas i vali ar comyuva cílinaryar i súrillon canta, cemeno rénallo menelo rénanna.
I *relyávaldallo para sestie sina: Ve rongo ve olvarya olle musse ar tuias lasseryar, istalde laire hare ná.
Sie yando elde, íre cenilde i nati sine martar, istar i náse hare, epe i fennar.
Násie quetin lenna i laume autuva *nónare sina nó ilye nati sine martar.
Menel cemenye autuvat, mal ninye quettar uar autuva.
Pa enta aure ar lúme *úquen ista, lá i vali menelde ar lá i Yondo, mal i Atar erinqua.
Cima, na coive, an ualde ista i lú yasse tuluva i lúme.
Si ná ve íre nér lelya hyana nórenna, autala coaryallo ar antala mólyain hére, ilquenen pa véra molierya, ar cánala i andocundon náve coiva.
Etta na coive, an ualde ista i lú yasse i *coantur tuluva, telwa hya endesse i lómio hya íre i tocot lamya hya arinya
– i uas tuluva rincenen ar le-hiruva lorne.
Mal ya quetin lenna, quetin ilyannar: Na coive!”
Sí aure atta lemnet nó i Lahtie ar i Aşar *Alapulúne Massaron. Ar i hére *airimor ar i parmangolmor cestaner manen mapumnelte Yésus curunen ar se-nahtumner.
Mal quentelte: “Lá i aşaresse, hya euva amortie imíca i lie!”
Ar lan anes Vetaniasse, ara i sarno mi coarya Símon Helmahlaiwa, túle nís arwa ondocolco níşima millo, nanwa *alanarda, ole mirwa. Rances panta i colca ar ulyane i millo Yésuo carenna.
Tá, ahanen, queneli quenter mici inte: “Mana i casta hatien oa níşima millo sina?
An millo sina mo polde vace telpenóten or lenári tuxar nelde ar anta sa i penyain!” Ar anelte rúşie senna.
Mal Yésus quente: “Lava sen náve! Mana castalda carien nati urde sen? Mára carda acáries nin.
An i penyar samilde illume mici le, ar quique merilde polilde care tien márie, mal ní ualde illume same.
Carnes ya poldes; nóvo apánies níşima millo hroanyasse nó nás talaina i noirinna.
Násie quetin lenna: Mi ilya nóme yasse i evandilyon ná carna sinwa, i quanda mardesse, yando ya nís sina carne nauva nyárina enyalien sé.”
Ar Yúras Iscariot, quén i yunqueo, oante i hére *airimonnar antien Yésus olla ten.
Íre hlasselte sa, anelte valime ar antaner vanda i antumnelte sen telpe. Ar cestanes manen antumnes Yésus olla íre ence sen.
Minya auresse i Aşaro *Alapalúne Massaron, íre nacantelte i *yanca Lahtiéva, hildoryar quenter senna: “Manna meril i menuvalme manwien, matielyan i Lahtie?”
Ar mentanes atta hildoryaron ar quente túna: “Mena mir i osto, ar nér cólala calpa neno veluva let. Áse hilya,
ar yasse menis minna queta i *coanturenna: I *Peantar quete: Masse *aşalşambenya, yasse ece nin mate i Lahtie as hildonyar?”
Ar sé tanuva lent hoa oromar, arwa farmeo ar manwaina. Tasse áven manwa.”
Ar i hildo atta oantet ar lendet mir i osto ar hirnet ilqua ve quentes, ar manwanette i Lahtien.
Apa şinye lantane túles as i yunque.
Ar lan anelte ara i sarno, mátala, Yésus quente: “Násie quetin lenna: Qúen mici lé, ye máta óni, ni-antuva olla.”
*Yestanelte same nyére ar quete senna, quén apa i exe: “Lau inye ná sé?”
Quentes téna: “Náse quén i yunqueo, ye *tumya asinye mir i imya salpe.
I Atanyondo é auta, ve ná técina pa sé, mal horro sana atanen lo ye i Atanyondo ná antaina olla! Arya náne sana atanen qui únes nóna.”
Matieltasse nampes massa, quente aistie, rance sa ar antane sa tien, ar eques: “Ása mapa, si hroanya ná.”
Ar mápala yulma quentes hantale ar antane sa tien, ar illi mici te suncer sallo.
Ar quentes téna: “Si sercenya ná, i véreo serce, ya nauva etulyaina rimbalin.
Násie quetin lenna: Laume sucuvan ata i liantasseo yávello tenna enta aure yasse sucuvanyes vinya mi Eruo aranie.”
Ar apa lindie airelindeli etelendelte Orontenna *Milpioiva.
Ar Yésus quente téna: “Illi mici te nauvar tyárine lanta, an ná técina: Petuvan i mavar, ar i mámar nauvar vintaine.
Mal apa nauvan ortaina lelyuvan epe le Alileanna.”
Mal Péter quente senna: “Yando qui ilye i exi nauvar tyárine lanta, en inye lauva!”
Tá Yésus quente senna: “Násie quetin lyenna: Síra, é lóme sinasse, nó tocot lamya lú atta, nel ni-laluval.”
Mal Péter quente ata ar ata: “Yando qui mauya nin quale aselye, laume lye-laluvan!” Sin quenter yando ilye i exi.
Tá túlelte i nómenna estaina Etsemane, ar Yésus quente hildoryannar: “Hama sisse lan hyamin.”
Ar talles Péter ar Yácov ar Yoháno as inse, ar *yestanes same lumne felmeli ar náve *tarastaina.
Ar quentes téna: “Feanya ná lunga tenna qualme nyérenen. Á lemya sisse ar hepa inde coive!”
Apa menie şinta vanta ompa, lantanes cemenna ar arcane i autumne sello i lúme, qui ta náne cárima.
Ar eques: “Appa, Atar, ilye nati nar cárime tyen; á mapa oa yulma sina nillo! Ananta lá ya inye mere, mal ya tyé mere.”
Ar túles ar hirne te lorne, ar quentes Péterenna: “Símon, ma nalye lorna? Lau sámel melehte náven coiva ter er lúme?
Alde na coive ar alde hyame, i ualde tule úsahtienna. I faire mérala ná, mal i hráve milya ná.”
Ar ata oantes ar hyamne, quétala i imya quetta.
Ar ata túles ar hirne te lorne, an hendultat nánet lunge, ar ualte sinte mana hanquetumnelte senna.
Ar túles i neldea lú ar quente téna: “Nalde en lorne ar sende? Ta farya! I lúme utúlie! Yé, i Atanyondo ná antaina olla mannar úcarindolion.
Á orta, alve lelya! Ela, ye ni-anta olla utúlie hare!”
Ar mi yana lú, lan Yésus en carampe, túle Yúras, quén i yunqueo, ar óse şanga arwe macillion ar rundalion, ho i hére *airimor ar i parmangolmor i amyárar.
Ye se-antumne ollo antane tien nóvo tanwa, quétala: “I quén ye miquin, ta ná sé; áse mapa ar á tulya se oa varnave.”
Ar íre túles tar, lendes mi imya lú Yésunna ar quente senna: “Rappi” – ar minqueses.
Tá panyanelte máltat sesse ar namper se.
Mal quén imíca i tarner tasse tunce macilya ar pente i héra *airimo mól ar aucirne hlarya.
Mal Yésus carampe ar quente téna: “Ma etutúlielde arwe macillion ar rundalion, ve pilunna ye tulyuvalde oa?
Aure apa aure anen aselde i cordasse peantala, ananta ualde nampe ni. Mal martas *amaquatien i Tehtele.”
Ar illi se-hehtaner ar úşer.
Mal enge nessa nér ye hilyane se, arwa *páşelarmo or helda hroarya; ar rincelte mapa se,
mal hehtanes i *páşelarma ar úşe helda.
Sí tulyanelte Yésus oa i héra *airimonna, ar ilye i hére airimor ar i amyárar ar i parmangolmor ocomner.
Mal Péter hairallo hilyane se mir paca i héra *airimóva, ar hámanes epe ruine as i núror náven lauca.
Ono i hére *airimor ar i quanda Tára Combe cestaner *vettie Yésunna nahtieltan se, mal hirnelte munta.
Rimbali antaner húrala *vettie senna, mal i *vettier úner vávie.
Ar quelli oronter ar quenter huruli senna, quétala:
“Elme ahlárier se quéta: Hatuvan undu corda sina ya náne cárina mainen, ar ter auri nelde carastuvan exe ya ua cárina mainen.”
Mal yando mi natto sina *vettielta úne vávea.
I mettasse i héra *airimo oronte endaltasse ar maquente Yésunna: “Ma hanquétal munta? Mana i nati yar queni sine *vettar lyenna?”
Mal anes quilda ar hanquente munta. Ata i héra *airimo maquente senna, ar eques: “Ma elye ná i Hristo, yondorya i Aistana?”
Ar Yésus quente: “Inye ná; ar cenuvalde i Atanyondo hára ara i Túreo forma ar túla as menelo fanyar.”
Tá i héra *airimo narcane lauperya ar quente: “Mana amba maure samilve *vettiéva?
Hlasselde i naiquetie! Mana sanalde?” I námie illion mici te náne i anes valda qualmeo.
Ar quelli *yestaner piuta senna ar tope cendelerya ar petitas ar quete senna: “Tyala i *Erutercáno!” Ar pétala cendelerya, i *námonduri se-namper oa.
Sí lan Péter náne undu i pacasse, quén imíca i *núri túle,
ar íre cennes Péter tára tasse *lautala inse, yenteses ar quente: “Yando elye náne as i nér ho Násaret, Yésus sina.”
Mal sa-lalanes, quétala: “Uan ista se ar uan hanya ya quétal!” Tá lendes ettenna i *andoşambenna.
Tasse i *núre, cénala se, ata *yestane quete innar tarner tasse: “Nér sina ná imica té!”
Ata sa-lalanes. Ar apa şinta lúme i tarner tasse ata quenter Péterenna: “É nalye mici té, an nalye Alileallo!”
Mal sé *yestane húta ar antane vandarya: “Uan ista nér sina pa ye elde quétar!”
Ar mí imya lú tocot ata lamyane, ar Péter enyalde i quetie ya Yésus quente senna: “Nó tocot lamya lú atta, ni-laluval nel.” Ar talantes nírelissen.
Ar ve rongo ve ára túle, i héra *airimo as i amyárar ar i parmangolmor ar i quanda Tára Combe carner úvie, ar nuntelte Yésus ar tulyaner se oa ar antaner se olla Pilátonna.
Ar Piláto maquente senna: “Ma elye ná aran Yúraron?” Sé hanquente: “Elye quete sa.”
Mal i hére *airimor quenter i anes cáriéla rimbe ulculi.
Ar Piláto maquente senna ata, quétala: “Ma hanquétal munta? Yé ilye i ulqui yar quétalte i acáriel!”
Mal Yésus hanquente munta amba, ar ta quante Piláto elmendanen.
Mal aşarello aşarenna leryanes tien nútina nér er, i quén ye arcanelte.
Yana lúmesse i nér estaina Varavas náne naxalissen as i amortiélar, i carner nahtie amortieltasse.
Ar i şanga túle ama ar *yestane arca i náne carna tien ve i sito né.
Mal Piláto maquente téna, quétala: “Ma merilde i leryan len i aran Yúraron?”
An sintes i hrúcen náne ya tyarne i hére *airimor anta se olla.
Mal i hére *airimor valtaner i şanga arcien i leryumnes tien Varavas mí men.
Mal Piláto maquente ata ar quente téna: “Tá mana merilde i caruvan yen estalde aran Yúraron?”
Ata yámelte: “Áse tarwesta!”
Mal Piláto quente téna: “Tá mana i ulco ya acáries?” Ono té yámer rúcima yalmenen: “Áse tarwesta!”
Tá Piláto, névala care ya i şanga merne, leryane tien Varavas. Mal riptiéla Yésus se-antanes olla náven tarwestaina.
Tá i ohtari se-tulyaner oa mir i paca, ta ná i túrion, ar comyanelte i quanda ohtarhosse.
Tá panyanelte sesse *luicarne colla ar carner ríe necellíva ar sa-panyane sesse.
Ar *yestanelte *suila se: “Hara máriesse, aran Yúraron!”
Ente, penteltes caryasse liscenen ar piutaner senna ar lender undu occaltatse ar lantaner undu epe se.
Ar láliéla senna yaiwenen nampelte sello i *luicarne colla ar panyaner sesse véra larmarya. Tá talleltes ettenna tarwestien se.
Ar lahtala nér ye túle i restallo mauyanelte cole tarwerya – Símon Círenello, Alexander ar Rúfuso atar.
Ar talleltes i nómenna estaina Olyoşa, ya tea *Caraxomen.
Ar névelte anta sen limpe arwa *lornatála suhteo, mal uas merne sucitas.
Ar tarwestaneltes ar etsanter larmaryar hatiénen *sanwali pa man ñetumne mana.
Tarwestaneltes i neldea lúmesse.
Ar i tanwasse teala i *ulquetie senna náne técina: “Aran Yúraron.”
Ente, tarwestanelte pilu atta óse, quén foryaryasse ar quén hyaryaryasse.
Ar i lahtaner naiquenter senna, *quequérala carilta ar quétala: “Orro! Elye ye hatumne undu i carda ar encarastumne sa ter auri nelde,
á rehta imle tuliénen undu i tarwello!”
Mí imya lé yando i hére *airimor, quén as i exe, lander senna yaiwenen as i parmangolmor, quétala: “Exeli erehties; inse uas pole rehta.
Nai i Hristo, aran Israélo, tuluva undu i tarwello cenielvan ar savielvan!” Yando i nánet tarwestaine óse naiquentet senna.
Íre i enquea lúme túle, mornie lantane i quanda nórenna tenna i nertea lúme.
Ar i nertea lúmesse Yésus yáme túra ómanen: “Eli, eli, lama savahtáni?”, ya tea: “Ainonya, Ainonya, mana castatya hehtien ni?”
Ar quelli imíca i tarner hare, íre hlasselte ta, quenter: “Á lasta! Yálas Elía!”
Mal quén norne, quantane hwan sára limpenen, panyane sa liscesse, ar láve sen suce, quétala: “Lava sen náve! Alve cene qui Elía tule se-mapien undu.”
Mal Yésus yáme túra ómanen ar effirne.
Ar i fanwa i yánasse náne narcaina mir atta, telmello talmenna.
Íre i *tuxantur ye tarne ara se cenne i sie effirnes, quentes: “Nér sina é náne Eruo Yondo!”
Enger yando nisseli i tíraner hairallo. Mici té náner María Mahtaléne ar i María ye náne i amil Yácov i Amnesso ar Yóseso, ar Salóme,
i hilyaner se ar *veuyaner sen lan anes Alileasse, ar rimbe hyane nisseli i lender óse ama Yerúsalemenna.
Sí şinye náne hare, ar náne Manwie, ta ná, i ré nó i *sendare.
Etta Yósef Arimaşeallo túle, mai-sinwa nér i Combeo, ye yando immo yente ompa Eruo araniéno tulienna. Veryanes mene minna Pilátonna ar arcane Yésuo hroa.
Mal Piláton ua ence save i qualles ta rongo, ar yaldes i *tuxantur ar maquente senna: “Ma náse sí qualin?”
Nyárina lo i *tuxantur i Yésus é náne qualin, antanes i loico Yósefen.
Ar apa mancie insen *páşe nampeses undu, ar se-vaitanes i *pásenen ar se-panyane hrótanoirisse, ar *peltanes ondo epe i noirio fenna.
Mal María Mahtaléne ar María amil Yóseso cennet i nóme yasse Yésus náne panyaina.
Íre i *sendare náne vanwa, María Mahtaléne ar Mária, Yácovo amil, ar Salóme, mancaner inten *tyávelasseli ar túler panien laive sesse.
Ar ita arinyave, i otsolo minya auresse, túlelte i noirinna, apa anaróre.
Ar quétanelte, quén i exenna: “Man *peltuva oa i ondo i noirillo men?”
Mal íre yentelte ama, cennelte in i ondo náne *peltaina oa, ómu anes ita hoa.
Méniéla mir i noire cennelte nessa nér háma i foryasse, arwa ninque andalaupeo, ar anelte captaine.
Mal sé quente téna: “Áva na captaine! Cestealde Yésus Násaretello, ye náne tarwestaina. Anes ortaina, uas sisse! Ela i nóme yasse panyaneltes!
Mal mena, nyara hildoryain ar Péteren: Menuvas epe le mir Alilea. Tasse cenuvaldes, ve quentes lenna.”
Ar apa tulie ettenna úşelte i noirillo, an anelte mapaine peliénen ar náner ara inte, ar nyarnelte munta aiquenen, an runcelte.
Pan rimbali amápier mir má *partie nyarna pa i nati yar amartier mici me,
ve acámielmet illon minyave náner astarmoli ar núroli i quetto,
*şéne mára yando nin, apa hilie ilye nati harive i yestallo, tece tai ve lúmequenta elyen, alcarinqua Şeofílo,
istielyan i tanca talma i nation pa yar anaiel peantaina.
Auressen Herolo, aran Yúreo, enge *airimo estaina Secaría, i *ciltiéno Avio, ar sámes veri Árono yeldion, ar esserya náne Elísavet.
Ar anette yúyo faile epe Eru, vantala ilye i axanissen ar şanyessen mi ilvana lé.
Mal ua enge hína asette, an Elísavet ua polle nosta, ar yúyo sámet rimbe loali.THE GOSPEL OF LUKE
I lú túle *ciltieryanna, ar carnes i *airimo molie epe Eru.
Hatiénen şanwali, ve i *airimoron lé náne, lantane senna mene mir i Héruo yána urtien *nisque.
Ar i quanda rimbe i lieo tarne hyamiesse i ettesse i lúmesse urtiéva *nisque.
Tá i Héruo vala tannexe sen, tárala foryasse i *yangwava *nisqueva.
Mal Secaría náne *tarastaina ceniénen se, ar caure lantane senna.
Mal i vala quente senna: “Áva ruce, Secaría, an arcandelya anaie hlárina, ar Elísavet verilya nauva amil yondon lyen, ar alye anta sen i esse Yoháno.
Ar samuval alasse ar márie, ar rimbali nauvar valime pan anaies nóna,
an nauvas túra epe i Héru, ar limpe ar polda yulda ávas suce, ar nauvas quátina Aire Feanen aqua et amilleryo mónallo.
Ar rimbali yondoron Israélo *nanqueruvas i Héru Ainoltanna,
ar sé menuva epe se mi Elío faire ar túre, *nanquerien atarion endar hínannar ar i avari i tercenna failaron, panien epe i Héru manwaina lie.”
Ar Secaría quente i valanna: “Manen polin ista nat sina? An nanye yára, ar verinya same rimbe loali.”
Hanquentasse i vala quente senna: “Inye Avriel, ye tare epe Eru, ar anen *etementaina carpien aselye ar carien i máre sinyar pa nati sine sinwe lyen.
Mal yé! nauval quilda ar ua poluva quete tenna i aure yasse nati sine martuvar, pan ual sáve quettanyassen, yar nauvar cárine nanwe lúmeltasse.”
Ar i lie *lartane Secarían, ar anelte elmendasse pan anes ta andave i yánasse.
Mal íre ettúles uas polle carpa téna, ar túnelte i cennes maur i yánasse, ar carnes hwermeli tien, en nála úpa.
Íre i auri *veuyaleryo náner telyaine, oantes coaryanna.
Mal apa auri sine Elísavet nostane, ar lanyanes inse exellon ter astari lempe, quétala:
“Sie i Héru acárie nin, mí auri yassen cimnes ni mapien oa nucumnienya mici atani.”
I enquea astaresse i vala Avriel náne mentaina Erullo ostonna Alileasse estaina Nasaret,
vendenna nauta verien nér estaina Yósef, Laviro nosseo, ar i vendeo esse náne María.
Ar íre i vala lende minna epe se, quentes: “Hara máriesse, elye ye acámie Eruanna; i Héru aselye.”
Mal Mária náne *tarastaina i quetiénen ar néve hanya mana nostale *suiliéva ta náne.
Ar i vala quente senna: “Áva ruce, María, an ihíriel lisse as Eru,
ar yé! nostuval mónalyasse ar coluva yondo, ar alye anta sen i esse Yésus.
Sé nauva túra ar nauva estaina i Antaro Yondo, ar i Héru Eru antuva sen Lavir ataryo mahalma,
ar turuvas Yácovo nosse tennoio, ar ua euva metta aranieryo.”
Mal María quente i valanna: “Manen nat sina martuva, pan uan ista nér?”
Ar hanquentasse i vala quente senna: “Aire Fea tuluva lyenna, ar i Antaro túre teltuva lye; sie yando i aire nála nóna nauva estaina Eruo Yondo.
Ar yé! Elísavet, ye ná nosselyo, inse onostie yondo yárieryasse, ar astar sina ná i enquea sen ye náne estaina *yávelóra.
An Erun ea munta úcárima.”
Tá María quente: “Ela i Héruo *núre! Nai martuva nin ve equétiel.” Ar i vala oante sello.
Mal yane auressen María oronte ar lende hormenen ostonna Yúreasse,
ar mennes mir coarya Secaría ar *suilane Elísavet.
Íre Elísavet hlasse Marío *suilie, i lapse mónaryasse campe, ar Elísavet náne quátina Aire Feanen,
ar yámes taura ómanen ar quente: “Aistana elye imíca nissi, ar aistana i yáve mónalyo!
Ar manen ná i nat sina amartie nin, i Herunyo amil túla ninna?
An yé! íre *suilielyo lamma túle hlarunyanta, i lapse mónanyasse campe túra alassenen!
Valima sé ye sáve i nauvar cárine nanwe i nati quétine senna lo i Héru!”
Tá María quente: “Feanya laita i Héru,
ar fairenya ná mi alasse Eru *Rehtonyanen,
an ecénies *núreryo naldie. Ar yé! ho sí ilye *nónari estuvar ni valima,
an i Taura acárie túre cardali nin, ar aire ná esserya;
ar ter *nónaréli ar nónaréli oravierya ea issen melir se.
Acáries taure natali, ivinties i nar turinque endalto sanwessen.
Hantes undu taurali mahalmallon ar ortane naldali;
aquáties i maitar máre natalínen ar ementie oa luste i sámer lar.
Amánies Israel núroryan, enyalien oravie,
ve nyarnes atarilvain, Avrahámen ar nosseryan, tennoio.”
Ar María lemyane as Elísavet *os astari nelde. Tá nanwennes coaryanna.
Elisavéten túle i lúme colien lapserya, ar colles yondo.
Ar i armaror ar nosseryo queni hlasser i sen-antane i Héru túra oravie, ar anelte mi alasse óse.
Ar i toltea auresse túlelte *oscirien i hína, ar estumneltes ataryo essenen, Secaría.
Mal amillerya hanquente ar eque: “Vá, mal esserya nauva Yoháno.”
Tá quentelte senna: “Ea *úquen nosselyasse ye same esse tana.”
Tá cestanelte hanquenta i atarello hwermelínen, mana esse mernes i hínan.
Ar arcanes *palma ar tence: “Esserya ná Yoháno.” Ar illi mici te náner elmendasse.
Mí imya lú antorya náne pantaina ar lamberya lehtaina, ar quentes, aistala Eru.
Ar rucie lantane ilye armaroryannar, ar mí quanda ortoména Yúreasse mo caramper pa ilye nati sine,
ar illi i hlasser túner sa endaltasse, quétala: “É mana nauva hína sina?” An yando i Héruo má enge óse.
Ar Secaría atarya náne quátina Aire Feanen ar quente ve *Erutercáno, quétala:
“Na aistana i Héru, Israélo Aino, pan icímies ar etelehties lierya!
Ar ortanes ven rasse rehtiéva Lavir núroryo coasse,
ve equéties ter anto i airi tercánoryaron et i vanwiello,
pa rehtie ñottolvallon ar ho i má ion tevir ve,
tanien oravie atarilvassen ar enyalien aire vérerya,
i vanda ya antanes Avraham atarelvan,
lavien ven náve núroli sen apa anelve rehtaine ho ñottolvaron má,
airesse ar failiesse epe se ilye aurelvassen.
Mal tyé, hína, nauva estaina Erutercáno i Antaro, an menuvatye epe i Heru manwien malleryar,
antien istya rehtiéno lieryan apseniénen úcareltaiva,
Ainolvo *ofelmenen oraviéno, yanen anaróre et tarmenello tuluva venna,
antien alcar in hámar morniesse ar ñuruhuinesse, tulien talulvat i tiesse raineva.”
Ar i hína alle ar náne carna taura fairesse, ar anes i erumessen tenna i aure yasse tanumnes inse Israélen.
Ar túle ente auressen i *etelende canwa Auhusto i Ingaranello, i mo notumne i quanda mar.
Minya notie sina martane íre Quirinio náne cáno Sírio.
Ilye queni lender náven nótine, ilquen véra ostoryanna.
Yando Yósef lende ama Alileallo, et i ostollo Nasaret, mir Yúrea, Laviro ostonna, ya ná estaina Vet-Lehem, pan anes maro ar nosseo Laviro,
náven nótina as María ye náne antaina sen vestalesse, ar ye sí náne *lapsarwa.
Íre engette tasse, i lúme túle yasse columnes lapserya.
Ar colles yondorya, i minnóna, ar se-vaitanes ar panyane se *salquecolcasse, pan ua enge tún nóme mí *sendasse.
Enger mavalli i imya nóresse i marner i restasse, tírala lámáreltar i lómisse.
Ar i Héruo vala tarne ara te, ar i Héruo alcar caltane *os te, ar túra caure nampe te.
Mal i vala quente téna: ”Áva ruce, pan inye cára sinwa len túra alasse ya nauva i quanda lien,
an anaie cólina len síra *Rehto, ye ná Hristo, i Heru, Laviro ostosse.
Ar si nauva tanwa len: Hiruvalde vinimo, vaitana ar caitala *salquecolcasse.”
Ar rincanen enge as i vala rimbe i meneldea hosseo, laitala Eru ar quétala:
”Alcar i tarmenissen na Erun, ar cemende raine i atanin pa i sanas mai.”
Ar apa i vali oanter tello mir menel, i mavari quenter quén i exenna: ”Alve lelya Vet-Lehemenna cenien nat sina ya amartie, ya i Héru acárie sinwa ven.”
Ar lendelte hormenen ar hirner María ar Yósef ar i vinimo caitala i *salquecolcasse.
Íre cennelte se, carnelte sinwe i nati yar náner quétine téna pa hína sina.
Ar elmenda nampe ilquen hlárala pa i nati nyárine ten lo i mavari,
mal María hempe ilye quetier sine ar sanne pa tai endaryasse.
Tá i mavari nanwenner, antala alcar ar laitale Erun pa ilqua ya hlasselte ar cennelte, aqua ve ta náne nyárina ten.
Ar íre i toltea auresse túle, náveryan *oscirna, camnes i esse Yésus, yanen i vala estane se nó anes nostaina i mónasse.
Íre i auri poiciéva náner telyaine Móseo Şanyenen, tallettes Yerúsalemenna panien se epe i Héru,
ve ná técina i Héruo Şanyesse: “Ilya hanu pantala súma nauva estaina aire i Hérun”,
ar talien *yanca ve ná quétina i Héruo Şanyesse: “*Cucuolle atta hya nessa cucuo atta.”
Ar yé! enge nér Yerúsalemesse yeo esse náne Símeon, ar nér sina náne faila ar rúcala Erullo, yétala ompa i tiutalenna Israelwa, ar aire fea caine senna.
Ente, náne sen apantaina i uas cenumne qualme nó cenie i Héruo Hristo.
I Fairenen sí túles i cordanna, ar íre i nostaru tallet i hína Yésus carien sen ya náne senwa i Şanyenen,
Símeon immo camne se mir rancuryat, ar laitanes Eru ar quente:
“Sí, Tur, lavuval mólelyan auta rainesse ve equétiel,
an hendunyat ecéniet rehtielya,
ya amanwiel epe ilye i lier,
cala mapien oa i vaşar i nórellon ar alcar Israel lielyan.”
Ar atarya yo amillerya nánet elmendasse pa i nati quétine pa se.
Ar Símeon aistane tu, mal quentes María amilleryanna: “Yé, si anaie panyaina i lanten ar i ortien rimbalion Israelde, ar tanwa yanna mo quete,
– é lango menuva ter véra fealya – apantien rimbe endaron sanwi.”
Ar enge nís estaina Anna, yelderya Fanuel nosseo Ahyero, ye náne *Erutercáne. Anes yára ar sáme rimbe loali, apa marnes as nér ter loar otso apa venesserya,
ar epeta anes *verulóra tenna sí sámes loar canta *toloquean. Únes oi oa i cordallo, mal *veuyanes Eru auresse yo lómisse, *avamátala ar yálala senna.
Ar yana lúmesse orontes ar antane Erun hantale, ar carampes pa i hína ilyannar i yenter ompa i ehtelehtienna Yerúsalenwa.
Ar apa telyanette ilqua i Héruo Şanyenen nanwennette Alileanna ar ostottanna, Násaret.
Ar i hína alle ar *yestane náve polda, nála quátina sailiénen, ar Erulisse caine senna.
Ilya loasse nostaruryat lendet Yerúsalemanna, i aşarenna Lahtiéno.
Ar íre sámes loar yunque, lendelte ama i aşaro haimenen
ar telyaner i auri. Mal íre nanwennette, Yésus i seldo lemyane Yerúsalemesse, ar nostaruryat uat túne sa.
Sánala i anes imíca i queni lelyala uo, lendette er aureo tie; tá cestanettes mici i queni nossetto ar i queni i sintette.
Mal íre uatte hirne se, entúlette Yerúsalemenna, cestala se.
Ar apa auri nelde hirnettes i cordasse, hámala imíca i *peantari, lastala téna ar maquétala téna.
Mal illi i lastaner senna náner captaine handeryanen ar hanquentaryainen.
Sí íre cennettes anette elmendasse, ar amillerya quente senna: “Hína, mana castatya carien ment sie? Yé, ataretya yo inye nánet ñwalyaine, cestala tye!”
Mal quentes túna: “Mana castasta cestien ni? Ma uaste sinte i mauya nin náve i coasse Atarinyava?”
Mal uatte hanyane i quetie ya quentes túna.
Ar lendes undu asette ar túle Nasaretenna, ar cimnes canwattar. Ar amillerya hempe ilye nati sine endaryasse.
Ar Yésus lende ompa mi sailie ar alie ar lisse mici Eru ar atani.
I *lepenquea loasse yasse Tivério turne ve Ingaran, íre Pontio Piláto náne nórecáno Yúreo, ar Herol turne ve *canastatur mi Alilea, ar Filip hánorya turne ve *canastatur mí ménar Ituréa ar Traconitis, ar Lisánias náne i *canastatur Aviléneo,
íre Annas ar Caiafas náner hére *airimor, Eruo quetta túle ana Yoháno Secarío yondo i ravandasse.
Ar lendes i quanda ménanna *os Yordan, *nyardala *tumyale inwistiéno ya anta apsenie úcariva,
ve ná técina i parmasse quettaron Yesaia i Erutercáno: “Óma queno ye yáma i ravandasse: Alde manwa i Heruo malle, cara tieryar tére!
Ilya yáwe nauva quátina, ar ilya oron ar ambo nauva cárina lára, ar i lúvar nauvar cárine tére tieli ar i tumpur paste malleli,
ar ilya hráve cenuva Eruo rehtie.”
Etta quentes i şangannar ettúlala náven *tumyaine lo sé: “A hínar leucoron, man anyárie len manen uşe i túlala rúşello?
Etta cara yávi valde inwisto. Ar áva *yesta quete indenna: 'Ve atar samilve Avraham.' An quetin lenna i Eru pole orta híni Avrahámen sine sarnollon.
É i pelecco yando sí caita ara i aldaron şundo; etta ilya alda ye ua care mára yáve nauva círina undu ar hátina mir i náre.”
Ar i şangar maquenter senna: “Tá mana caruvalme?”
Hanquentasse quentes téna: “Mauya i neren ye same laupe atta, anta er i quenen ye ua same, ar mauya yen same natali matien care i imya.”
Mal yando *tungwemóli túler náven tumyaine, ar quentelte senna: “*Peantar, mana caruvalme?”
Quentes téna: ”Áva cane telpe han i vanima nonwe.”
Ente, i móler mí hosse maquenter senna: “Mana elme caruvar?” Ar quentes téna: “Áva *tarasta aiquen, ar áva quete huruvi pa ongwi yar queni uar acárier, mal lava *paityalelda farya len.”
Sí i quanda lie yente ompa yanna martumne, ar illi sanner endaltasse pa Yoháno: ”Cé sé i Hristo ná?”
Yoháno hanquente illin: “Inye *tumya le nennen; mal túla ye ná polda lá ni; inye ua valda lehtien hyaparyato latta. Le-*tumyuvas Aire Feanen ar nárenen.
*Saltamarya ea máryasse poitien *vattarestarya, ar comyuvas orirya mir i haura, mal i *ospor urtuvas nárenen ya *úquen pole *luhtya.”
Etta antanes yando rimbe hyane hortiéli ar *nyardane i evandilyon i lien.
Mal Herol i *canastantur, íre anes *naityaina lo se pa Herólias hánoryo veri ar pa ilye i olce cardar yar Herol carne,
napanne yando nat sina ilye tane cardain: panyanes Yoháno mandosse.
Sí íre i quanda lie náne *tumyaina, yando Yésus náne *tumyaina, ar íre hyamnes, menel náne pantaina
ar i Aire Fea, *hroacantasse cucuo, túle undu senna, ar óma túle et menello: “Elye ná yondonya, i melda; pa lyé asánien mai.”
Ar Yésus immo, molieryo yestasse, sáme *os loar *nelequean. Anes, mo intyane, yondorya Yósef, yondorya Heli,
yondorya Mattat, yondorya Levi, yondorya Melci, yondorya Yannai, yondorya Yósef,
yondorya Mattatías, yondorya Ámos, yondorya Nahum, yondorya Esli, yondorya Nangai,
yondorya Maat, yondorya Mattatías, yondorya Semein, yondorya Yósec, yondorya Yóra,
yondorya Yoanan, yondorya Resa, yondorya Seruvável, yondorya Hyealtiel, yondorya Neri,
yondorya Melci, yondorya Atti, yondorya Cosam, yondorya Elmáram, yondorya Ér,
yondorya Yésus, yondorya Eliéser, yondorya Yórim, yondorya Mattat, yondorya Levi,
yondorya Símeon, yondorya Yúras, yondorya Yósef, yondorya Yónam, yondorya Elyacim,
yondorya Melea, yondorya Menna, yondorya Mattata, yondorya Nátan, yondorya Lavir,
yondorya Yesse, yondorya Over, yondorya Voas, yondorya Salmon, yondorya Naxon,
yondorya Amminarav, yondorya Arni, yondorya Hesron, yondorya Peres, yondorya Yehúra,
yondorya Yácov, yondorya Ísac, yondorya Avraham, yondorya Tera, yondorya Nahor,
yondorya Seru, yondorya Reu, yondorya Pele, yondorya Ever, yondorya Hyela,
yondorya Cainan, yondorya Arpaxar, yondorya Hyem, yondorya Noa, yondorya Lamec,
yondorya Metúsela, yondorya Enoc, yondorya Yarel, yondorya Mahalaleél, yondorya Cainan,
yondorya Enos, yondorya Set, yondorya Atan, Eruo yondo.
Ar Yésus, quanta Aire Feo, lende oa Yordanello ar náne tulyaina lo i Faire mir i erume
aurin *canaquean, nála şahtaina lo i Arauco. Ar mantes munta yane auressen, ar íre anelte vanwe, anes maita.
Tá i Arauco quente senna: “Qui nalye Eruo yondo, queta sar sinanna: Na vistaina mir massa!”
Mal Yésus hanquente senna: “Ná técina: Atan ua samuva coire massanen erinqua.”
Tá se-tulyanes ama ar tanne sen ilye aranier ambaro mí erya lú,
ar i Arauco quente senna: “Antuvan lyen ilya túre sina ar alcarelta, an anaies antaina nin, ar antuvanyes aiquenen ye merin.
Etta, qui elye *tyere ní, ilqua nauva *lyenya.”
Hanquentasse Yésus quente senna: “Ná técina: I Héru Ainolya alye *tyere, ar sen erinqua alye *veuya.”
Sí se-tulyanes mir Yérusalem ar panyane i cordo rámasse ar quente senna: “Qui nalye Eruo yondo, hata imle undu silo,
an ná técina: Canuvas valaryain pa lye, hepien lye,
ar: Coluvalte lye máltatse, pustien lye petiello talelya sarnenna.”
Hanquentasse Yésus quente senna: “Ná quétina: Ávalye tyasta i Héru Ainolya.”
Tá i Arauco, apa telie i quanda úşahtie, lende oa sello tenna hyana lúme.
Sí Yésus nanwenne i Faireo túresse mir Alilea. Ar mára quetie pa sé vintane ter i quanda *oscaitala norie.
Ar peantanes *yomencoaltassen, cámala laitale ho illi.
Ar túles Nasaretenna, yasse anes ortaina, ar ve haimerya náne, lendes mir i *yomencoa, ar orontes hentien.
Tá Isaia i *Erutercáno *toluparma náne antaina sen, ar pantanes i parma ar hirne i nóme yasse náne técina:
“I Héruo faire ea nisse, an *líves ni antien máre sinyali penyain; ni-mentanes carien sinwa quenin mandosse i nauvalte leryaine ar *cénelórain i camuvalte *céne, mentien i rácinar oa lehtiénen,
carien sinwa i Héruo loa mára nirmeo.”
Ar tolunes i parma, sa- *nanantane i núron ar hamne undu; ar hendu illion i enger i *yomencoasse nánet panyaine sesse.
Tá *yestanes quete téna: “Síra tehtele sina anaie carna nanwa íre hlasseldes.”
Ar illi *vettaner pa se, ar enger elmendasse pa i raine quettar yar túler et antoryallo, ar quentelte: “Ma nér sina ua Yósefo yondo?”
Tá quentes téna: “É merilde quete ninna &quenna sina: '*Nestando, á nesta imle! I nati yar ahlárielme martaner Capernaumesse, cara tai yando sisse véra ménalyasse!”
Mal eques: “Násie quetin lenna i ua ea *Erutercáno ye ná mai cámina véra ménaryasse.
An mi nanwie quetin lenna: Enger rimbe *verulórali Israelde Elío auressen, íre menel náne holtaina ter loar nelde ar astari enque, tyárala túra saicele lanta i nórenna;
ananta Elía úne mentaina aiquenna mici té, mal Sarefatenna mi Sírono nóre, *verulóra nissenna tasse.
Ente, enger rimbe helmahlaiwali Israelde Elíhya i Erutercáno lúmesse, ananta *úquen mici té náne poitaina, mal Náman Siriallo.”
Ar illi i hlasser nati sine i *yomencoasse náner quátine ahanen,
ar orontelte ar se-hortaner i osto ettenna, ar tulyaneltes lancanna i oronto yasse ostolta náne carastaina, hatien se undu talo.
Mal sé menne ter endelta ar lende oa.
Ar lendes undu Caparnaumenna, osto Alileasse. Ar peantanes tien i *sendaresse,
ar anelte quátine elmendo i lénen ya peantanes, an quetierya sáme túre.
Ar enge i *yomencoasse nér haryaina lo faire, úpoica rauco, ar yámes taura ómanen:
“Ai! Mana men ar lyen, Yésus Nasaretello? Ma utúliel nancarien me? Istan man nalye, Eruo Aire!”
Ar Yésus naityane se, quétala: “Na quilda ar tula et sello!” Ar apa hatie i nér undu endeltasse, i rauco túle et sello, ú maliéno se.
Tá elmenda lantane illinnar, ar carampelte quén as i exe, quétala: “Mana nostale quetiéno ná si? An túrenen ar melehtenen canis i úpoice fairi, ar ettulilte!”
Ar i sinyar pa se *etelender mir ilya nóme i *oscaitala noriesse.
Apa autie i *yomencoallo lendes mir coarya Símon. Mal Símondo verio amil náne *tarastaina túra úrenen, ar carnelte arcande sen pa sé.
Ar tarnes or i nís ar naityane i úre, ar ta oante sello. Mí imya lú orontes ar *veuyane tien.
Mal Anaro nútiesse, illi i sámer queni hlaiwe *alavéle hlívelínen taller te senna. Paniénen máryat tesse nestanes te.
Ente, raucoli ettúler rimbalillon, yámala ar quétala: “Elye ná Eruo yondo.” Mal naityanes te ar ua láve tien quete, pan sintelte i anes i Hristo.
Ono íre aure túle, *etelendes ar menne eressea nómenna. Mal i şangar cestaner se, ar túlelte yanna anes, ar névelte pusta se autiello tello.
Mal quentes téna: “Yando i hyane ostoin mauya nin care sinwa i evandilyon, an ta ná i casta yanen anen *etementaina.”
Ar lendes *nyardala Yúreo *yomencoassen.
Mi lú yasse i şanga ninde senna, lastala Eruo quettanna, tarnes ara i ailin Ennesaret.
Ar cennes lunte atta caitala ara i ailin, mal i *lingwimor náner ménienwe et tullo ar náner sóvala rembeltar.
Lelyala mir er i luntion, ya náne Símonwa, arcanes sello lelya şinta tie i norello. Tá hamnes undu, ar i luntello peantanes i şangain.
Íre pustanes quete, quentes Símonna: “Á lelya yanna núra ná, ar alde panya undu rembeldar mapien.”
Mal hanquentasse Símon quente: “*Peantar, ter i quanda lóme omótielme ar namper munta, mal canwalyanen panyuvan i rembeltar undu.”
Íre carnelte sie, yórelte hoa rimbe lingwilíva, ar rembeltar náner narcaine.
Ar hwernelte *rantarwaltain i hyana luntesse tulieltan manien tien, ar túlelte, ar quantelte yúyo lunte, tenna anette nútala.
Íre cennes ta, Símon Péter lantane undu epe Yésuo occat, quétala: “Á auta nillo, an nanye *úcarunqua nér, Heru!”
An i mapiénen lingwilíva anes pétina elmendanen, sé ar illi i náner óse,
ar yando yúyo Yácov yo Yoháno, yondoryar Severai, i nánet *rantarwali as Símon. Mal Yésus quente Símonna: “Áva ruce! Ho sí mapuval atalli coirie.”
Ar tulunelte i lunti norenna ar hehtaner ilqua ar hilyaner sé.
Ar íre anes mi er i ostoron, yé! nér quanta helmahlíveo. Íre cennes Yésus lantanes cendeleryanna ar inque sello, quétala: “Heru, qui meril, polil poita ni!”
Ar *eterahtala máryanen Yésus se-appane, quétala: “Merin! Na poitaina!” Ar mí imya lú i helmahlíve váne sello.
Ar cannes i neren: “Nyara *uquenen, mal á auta ar tana imle i *airimon, ar ve Móses canne cara *yanca poitielyan, ve *vettie tien.”
Mal i quetta pa se vintane ta ita ambe, ar hoe şangali túler uo lastien ar náven nestaine hlíveltallon.
Ono lendes oa mir i erumi, hyámala.
Martane mi aure i anes peantala, ar Farisali ar *peantalli i Şanyeo i náner túlienwe et ilya mastollo Alileo ar Yúreo ar Yerúsalémo hamner tasse, ar i Héruo túre náne tasse, polieryan nesta.
Ar yé! quelli cólala caimasse nér ye náne *úlévima, ar cestanelte tala se ompa ar panya se epe Yésus.
Mal íre ualte hirne manen pollelte cole se ompa, castanen i şango, rentelte i tópanna, ar ter i tupse panyaneltes undu as i caima, i endesse epe Yésus.
Ar íre cennes savielta quentes: “Atan, úcarelyar nar apsénine lyen!”
Ar i parmangolmor ar i Farisar *yestaner sana, quétala: “Man ná nér sina ye quéta naiquetiéli? Man lerta apsene úcari hequa Eru erinqua?”
Mal Yésus, tuntala sanweltar, quenter hanquentasse téna: “Pa mana sánealde endaldassen?
Mana i ambe *ascare: quetie 'Úcarelyar nar apsénine lyen' hya quetie: 'Á orta, mapa caimalya ar mena coalyanna.'
Mal istieldan i same i Atanyondo túre cemende apsenien úcari –” quentes i *úlévima nerenna: “Quetin lyenna, á orta ar mapa ama caimalya ar mena coalyanna.”
Ar mi imya lú orontes epe te, nampe ama ta yasse yá caines, ar lende coaryanna antala alcar Erun.
Tá illi mici te náner ara inte, ar antanelte alcar Erun ar náner quátine caureo, quétala: “Ecénielve elmendali síra!”
Ar apa nati sine *etelendes ar cenne *tungwemo estaina Lévi hámala mí *tungwemen, ar quentes senna: “Áni hilya.”
Ar hehtala ilqua orontes and hilyane se.
Ente, Lévi carne túra merende sen coaryasse, ar enge hoa şanga, *tungwemóli ar exeli, i náner aselte ara i sarno.
Ar i Farisar ar parmangolmoltar nurruner hildoryannar, quétala: “Manen ná i matilde ar sucilde as *tungwemóli ar úcarindoli?”
Hanquentasse Yésus quente téna: “I samir mále uar same maure *nestondova, mal i nar engwe.
Utúlien yale, lá failar, mal *úcarindor inwistenna.”
Quentelte senna: “Hildoryar Yoháno rimbave *avamatir ar carir arcandeli, mal *lyenyar matir ar sucir.”
Yésus quente téna: “Lau polilde mauya i endero meldoin *avamate íre i ender ea aselte?
Ananta aureli tuluvar yassen i ender nauva mapaina oa tello; tá *avamatuvalte, ente auressen.”
Ente, quentes téna sestie: “*Úquen cire lanneranta ho vinya colla ar neme sa yára collasse, mal qui mo é care sie, tá i vinya lanneranta narca oa; ente, i lanneranta ho i vinya colla ua vávea i yáran.
Ar *úquen panya vinya limpe yáre *helmolpessen, mal qui mo é care sie, tá i vinya limpe *ruve i *helmolpi, ar ulyas ettenna ar i *helmolpi nar nancarne.
Mal mauya panya vinya limpe vinye *helmolpessen!
*Úquen ye usúcie yára limpe mere vinya, an quetis: 'I yára ná mára.'”
Túle *sendaresse i lendes ter orirestali, ar hilmoryar leptaner ar manter i cari oriva, ascátala tai máltatse.
Mal quelli i Farisaron quenter: “Manen ná i cáralde ya ua lávina i *sendaresse?”
Mal Yésus hanquente téna: “Ma ualde oi ehentie pa ya Lavir carne íre sé ar i neri óse náner maite?
Lendes mir i coa Eruva ar camne i mastar taniéva ar mante ar antane tai i nerin óse, ómu tai uar lávina matso aiquenen hequa i *airimoin erinque.”
Ar quentes téna: “I Atanyondo ná Heru i *sendareo.”
Túle hyana *sendaresse i lendes mir i *yomencoa ar peantane. Ar enge tasse nér yeo forma náne hessa.
I parmangolmor ar i Farisar tirner se, cenien qui nestumne aiquen i *sendaresse, hirieltan lé yanen pollelte *ulquete se.
Mal sintes manen sannelte, ananta quentes i nerenna arwa i hessa máo: “Á orta ar tara i endesse.” Ar orontes ar tarne tasse.
Tá Yésus quente téna: “Maquetin lello: Ma ná lávina i *sendaresse care márie hya care ulco, rehta quén hya nancare se?”
Ar apa yentes illi mici te, quenello quenenna, quentes i nerenna: “Á rahta mályanen!” Carnes sie, ar márya náne envinyanta.
Mal anelte quátine urşanen, ar quentelte pa mana carumnelte Yésun.
Túle ente auressen i lendes ama mir i oron hyamien, ar lemnes tasse ter i lóme, hyamiesse Erunna.
Mal íre aure túle, yalles hildoryar ar ciller ho mici te yunque, i yando estanes aposteli:
Símon, ye yando estanes Péter, ar Andréo hánorya, ar Yácov and Yoháno, ar Filip ar Varşoloméo
ar Mattéo ar Şomas ar Yácov yondorya Alfaio, ar Símon ye ná estaina i Selot,
ar Yúra yondorya Yácov, ar Yúras Iscariot, ye *vartane se.
Ar túles undu aselte ar tarne palaresse, ar enge hoa liyúme queniva ho i quanda Yúrea ar Yerúsalem ar ho i earménar *os Tír ar Siron, i túler hlarien se ar náven nestaine hlíveltaron.
Yando i náner *tarastaine lo úpoice faireli náner nestaine.
Ar i quanda şanga cestaner appa se, an túre lende et sello ar nestane illi mici te.
Ar ortanes henduryat hilmoryannar ar quente: “Valime nar i penyar, an Eruo aranie ná *lenya!
Valime nar elde i nar maite sí, an nauvalde quátine!Valime nar elde i nar yaimie, an laluvalde!
Valima nalde íre atani tevir le, ar íre queriltexer lello ar naityar le ar et-hatir esselda ve ulca, i Atanyondo márien.
Sama alasse aure entasse ar capa, an yé! *paityalelda ná túra menelde. An tai nar i imye nati yar atariltar carner i Erutercánoin.
Mal horro len i nar lárie, an asámielde quanda tiutalelda!
Horro len i nar quátine sí, an nauvalde maite!Horro len i lalar sí, an samuvalde nyére ar níri!
Horro len íre ilye atani quetir mai pa le, an mí imya lé atariltar quenter pa i úanwe Erutercánor!
Mal quetin lenna i lastar: Mela ñottoldar ar cara mai in tevir le,
á aista i hútar le ar hyama in quetir ulco pa le.
Yenna pete le er ventasse quera yando i exe, ar ye mapa collalya áva pusta mapiello yando i laupe.
Á anta illin i arcar lyello, ar yello mapa natilyar áva arca *nancame tai.
Ar ve merilde i atani caruvar len, sie cara tien.
Ar qui melilde i melir lé, manen ta nauva aşea len? An yando i úcarindor melir i melir té.
Ar qui carilde márie in carir márie elden, manen ta nauva aşea len? Yando i úcarindor carir i imya.
Ar qui *yutyal queni illon samil estel camiéva, manen ta nauva aşea len? Yando úcarindor *yutyar úcarindor *nancamien i imya nonwa.
Úsie, mela ñottoldar ar cara márie ar á *yutya exeli, ú estelo *nancamiéva, ar *paityalelda nauva túra, ar nauvalde i Antaro yondor, an sé ná aşea in uar anta hantale ar i ulcain.
Á órava ve Atarelda órava!
Ente, áva name, ar laume nauvalde námine; áva quete i exi nar valde paimeo, ar ua nauva quétina pa lé i nalde valde paimeo. Á lehta, ar nauvalde lehtaine.
Á anta, ar mo antuva len. Mára lesta – nírina undu, pálina ar úvea – panyuvalte súmaldasse. An i lestanen yanen *etelestalde, *etelestuvalte len.
Tá yando quentes sestie téna: “Lau *cénelóra quén pole tulya *cénelóra quén? Ma yúyo uat lantuva mir unque?
Ye pare ua or ye peanta, mal ilquen yen anaie aqua peantaina nauva ve ye peanta.
Manen ná, tá, i yétal i lisce ya ea hánolyo hendesse, mal ualye cime i pano ya ea véra hendelyasse?
Manen ece lyen quete hánolyanna: 'Háno, lava nin mapa i lisce et hendelyallo,' íre elye ua yéta i pano véra hendelyasse?
An ua ea mára alda cárala şaura yáve; ata ua ea şaura alda cárala mára yáve.
An ilya alda ná sinwa yáveryanen. An queni uar hosta *rélyávi necelillon; ente, ualte cire *tiumar ho neceltussa.
Mára atan tala márie et endaryo mára harmallo, mal ulca atan tala ulco et ulca harmaryallo, an et i endo úvello antorya quete.
Manen ná i estalde ni 'Heru, Heru', ananta ualde care i nati yar quetin?
Ilquen ye tule ninna ar hlare quettanyar ar care tai – tanuvan len as man mo pole sesta se:
Náse ve quén carastala coa, ye sampe ar panyane i talma i ondosse. Etta, íre enge oloire, i síre palpane sana coanna, mal uas polle pale sa, pan anes mai carastaina.
Mal ye ahlárie ar ua care, ná ve nér ye carastane coa i talamesse ú talmo. Sanna i síre pente, ar mí tana lú talantes, ar coa tano atalantie náne túra.”
Apa telyanes ilye quetieryar íre i lie lastane, lendes mir Capernaum.
Mal *tuxanturo mól, ye náne melda sen, náne nimpa ar hare effírienna.
Íre hlasses pa Yésus, mentanes senna amyárali i Yúraron arcien sello tule ar etelehta mólya.
Tá i túler Yésunna carner arcande sello ar quenter: “Náse valda i caril nat sina sen,
an melis nórelva, ar sé carastane men i *yomencoa.”
Ar Yésus lende aselte. Mal íre únes haira i coallo, i *tuxantur nóvo mentane meldoli quetieltan senna: “Heru, áva *tarasta imle, an uan valda i tulil nu tópanya.
Etta únen note imne valda tulien lyenna. Mal queta i quetta ar tyara mólinya náve nestaina.
An yando inye ná nér nu túre, arwa ohtallion nu ni, ar qui quetin sinanna: Mena! – tá ménas, hya exenna: Tula! – tá tulis, hya mólinyanna: Cara sie! – tá caris ta.”
Ar íre Yésus hlasse nati sine anes quátina elmendanen pa se, ar quernes inse i şanganna hilyala se ar quente: “Nyarin len, yando Israelde uan ihírie savie ta túra.”
Ar i náner mentaine, íre nanwennelte i coanna, hirner i mól málesse.
Ar epeta martane i lendes mir osto estaina Nain, ar hildoryar ar hoa şanga lender óse.
Ar íre túles hare i osto andonna, qualin nér náne cólina ettenna, amilleryo *ernóna yondo. Ente, verurya náne qualin. Hoa şanga i ostollo enge yando as i nís.
Ar íre i Heru cenne se, endarya etelende senna, ar quentes senna: “Áva na yaimea!”
Ar túlienwa hare appanes i tulma, ar i colindor pustaner, ar quentes: “Nessa nér, quetin lyenna: Á orta!”
Ar i qualin nér hamne ama ar *yestane carpa, ar Yésus se-antane amilleryan.
Sí rucie nampe te illi, ar antanelte alcar Erun, quétala: “Túra Erutercáno anaie ortaina ama mici ve,” ar: “Eru icímie lierya!”
Ar quetta sina vistane mir quanda Yúrea ar i quanda *oscaitala nórienna.
Ar Yoháno hildor nyarner sen pa ilye nati sine.
Ar Yoháno yalle atta hildoryaron ar mentane tu i Herunna quetien: “Ma elye ná ye tulumne, hya ma mauya men yéta ompa exenna?”
Íre túlette senna, i neri quenter: “Yoháno i *Tumyando mentane met lyenna quetien: Ma elye ná ye tulumne, hya ma mauya men yéta ompa exenna?”
Lúme yanasse nestanes rimbali, leryala te hlívellon ar perperiellon ar olce fairellon, ar rimbe *cénelóralin tyarnes *céne.
Ar hanquentes túna: “Á auta, nyara Yohánon ya cennette ar hlassette:
I *cénelórar camir *céne, i helmahlaiwar nar poitaine ar i penner hlarie hlarir, i qualini nar ortaine, ar valima ná ye ua hire nisse casta taltiéva.”
Apa i neri mentaine lo Yoháno oanter, *yestanes carpa i şangannar pa Yoháno: “Mana lendelde mir i erume yétien? Lisce lévala i şúrinen?
Lá, mana etelendelde cenien? Nér arwa mussi lannion? Yé, i arwar mussi lannion ear coassen araniva!
Mal mana castalda etelelien? Cenie Erutercáno? Ná, quetin lenna, ar ambela Erutercáno!
Sé náne pa ye anaie técina: 'Yé! Mentan tercánonya epe cendelelya, manwien mallelya epe lye.'
Nyarin len: Imíca i náner nóne lo nissi ea *úquen túra lá Yoháno, mal quén imíca i ambe pityar Eruo araniesse ná túra lá sé.”
Ar i quanda lie ar i *tungwemor, íre hlasselte ta, quenter i Eru náne faila, té nála *tumyaine Yoháno *tumyalénen.
Mal i Farisar ar i *şanyengolmor querner oa ya Eru merne tien, té i úner *tumyaine lo sé.
“As man, tá, sestuvan *nónare sina?
Nalte ve hínar hámala mi mancanóme ar yámala quén i exenna: 'Tyallelme i simpa len, mal ualde liltane; anelme yaimie, mal ualde sáme níri.'
An Yoháno i *Tumyando utúlie, lá mátala massa hya súcala limpe, mal quetilde: 'Náse haryaina lo rauco!'
I Atanyondo utúlie, mátala ar súcala, mal quetilde: 'Ela! Nér *accamátala ar antaina sucien limpe, meldo *tungwemoron ar úcarindoron!'
Ananta sailie ná tanaina faila lo ilye hínaryar.”
Quén imíca i Farisar arcane i Yésus matumne óse, ar túlala mir coa i Farisava caines undu ara i sarno.
Ar yé! nís ye náne sinwa i ostosse ve *úcarinde parne i enges ara i sarno coasse i Farisava, ar mancanes insen ondocolca níşima milleo,
ar tárala níressen ca Yésus ara talyat, i nís *yestane care talyat mixe níreryainen ar *aupsarne tai caryo findelénen, ar minques talyat ar *líve tu i níşima millonen.
Cénala ta, i Farisa ye yalle Yésus quente insenna: “Nér sina, qui anes Erutercáno, istumne man ar mana nostaleo ná nís sina ye appea se – i náse *úcarinde!”
Mal hanquentasse Yésus quente senna: “Símon, samin nat ya merin quete lyenna.” Eques: “*Peantar, queta sa!”
“Nér atta sámet rohta quenen yello camnette telpe; er sáme rohta lenárion tuxar lempe, i exe, *lepenquean.
Íre sámelte munta *nanantien sen, yúyon apsennes. Tá man mici tú meluva se ambe?”
Hanquentasse Símon quente: “Intyan i quén yenen apsennes i anhoa nonwe.” Quentes senna: “Namnes mai.”
Tá quernes inse i nissenna ar quente Símondenna: “Ma cénal nís sina? Túlen mir coalya; ual antane nin nén talunyant. Mal nís sina carne talunyat mixe níreryainen ar *aupsarne tai findeleryanen.
Ual antane nin mique, mal nís sina, i lúmello ya túlen, ua pustane mique talunyat.
Ual *líve carinya millonen, mal nís sina *líve talunyat níşima millonen.
Sina castanen quetin lyenna: Rímbe úcareryar nar apsénine sen, an méles ole; mal yen pitya ná apsénina, mele pityave.”
Tá quentes senna: “Úcarelyar nar apsénine.”
Mal i cainer óse as i sarno *yestaner quete intesse: “Man ná nér sina ye yando apsene úcari?”
Ono quentes i nissenna: “Savielya erehtie lye; mena rainesse.”
Ar túle epeta i lendes ostollo ostonna ar mastollo mastonna ar peantane ar carne sinwa i evandilyon pa Eruo aranie, ar i yunque hilyaner se,
ve carner nisseli i náner nestaine olce fairellon ar hlívellon: María ye náne estaina Mahtaléne, yello raucor otso náner ettúlienwe,
ar Yoanna verirya Cúsa, Herolo cáno, ar Susanna ar rimbe hyane nisseli, i *veuyaner sen armaltainen.
Sí íre hoa şanga náne ocómienwa as i lender óse ostollo ostonna, quentes téna sestiénen:
“*Rerindo etelende rerien. Rerieryasse erdeli lantaner ara i malle ar náne *vattaina undu, ar menelo aiwi manter tai.
Exeli lantaner i ondonna, ar apa tuianelte hestanelte, peniénen nén.
Exeli lantaner imíca i neceli, ar i neceli i aller ama aselte quorner te.
Exeli lantaner i mára cemenna, ar apa tuianelte carnelte yáve, napánine lestar túxa.” Apa nyarie nati sine, yámes: “Nai ye same hlaru hlaruva!”
Mal hildoryar maquenter senna: “Mana tea i sestie?”
Eques: “Elden ná antaina ista Eruo araniéno fóler, mal i exin nas sestiessen, tyárala te yéta ú ceniéno ar hlare ú haniéno.
Si ná ya tea i sestie: I erde ná Eruo quetta.
I nar ara i malle nar i ahlárier; tá i Arauco tule ar mapa oa i quetta endaltallo, pustien te saviello ar návello rehtaine.
I nar i ondosse nar i camir i quetta alassenen íre hlariltes, mal té uar same şundo; savilte ter lúme, mal lúmesse tyastiéno lantalte oa.
I lantaner imíca i neceli, té nar i ahlárier, mal nála cóline oa coive sino querelínen ar larnen ar alassínen, nalte quórine ar telyar munta.
Mal i nar i mára cemesse, té nar i hlarir i quetta mára ar vanya endanen ar hepir sa ar colir yáve voronwiénen.
Ea *úquen ye, apa nartie calma, tupe sa venenen hya panya sa nu caima, mal sa-panyas i calmatarmasse, lávala in tulir minna yéta i cala.
An ea munta nurtaina ya ua nauva apantaina, hya *aiqua halyaina ya ua oi nauva sinwa hya aşcénima.
Etta cima manen lastalde, an aiquen ye same, amba nauva antaina sen, mal aiquen ye ua same, yando ya savis i samis nauva mapaina sello.”
Sí amillerya ar hánoryar túler senna, mal ualte polle rahta senna i şanganen.
Mal mo nyarne sen: “Amillelya ar hánoldar tárar i ettesse, mérala vele lye.”
Hanquentasse quentes téna: “Amillinya ar hánonyar nar i hlarir Eruo quetta ar carir sa.”
Túle mi er i aurion i sé ar hildoryar lender mir lunte, ar quentes téna: “Alve lahta i hyana fáranna i ailino.” Sie cirnelte oa.
Mal íre cirnelte humnes. Sí raumo lantane i ailinna, ar luntelta náne quátina, ar anelte raxesse.
I mettasse lendelte senna ar eccoitaner se, quétala: “*Peantar, *peantar, quálalme!” Mal apa ortie naityanes i şúre ar i neno orme, ar anelte cárine sende, ar enge quilde.
Tá quentes téna: “Masse savielda?” Mal nála ruhtaine anelte elmendasse, quétala quén i exenna: “É man ná nér sina, an canis yando i şúrin ar i nenen, ar carilte ve quetis?”
Ar cirnelte nórienna Erasenyaiva, ya caita i hyana hrestasse Alileallo.
Ar íre lendes norenna, se-velle nér i ostollo ye náne haryaina lo rauco; ter anda lúme uas colle lanni, ar uas marne coasse, mal imíca i noiri.
Íre cennes Yesus, yámes ar lantane undu epe se, ar quentes taura ómanen: “Mana nin ar lyen, Yésus, Eru Antaro yondo? Iquin lyello, ávani ñwalya!'
An cannes i úpoica fairen tule et i nerello. Nóvo i rauco ter anda lúme hempe i nér tanca, ar anes nútina limillínen ar nútelínen talyatse, íre cundor tirner se, mal narcanes i núti ar náne élina lo i rauco mir i erinque nómi.
Yésus maquente senna: “Mana esselya?” Eques: “Lehion,” an rimbe raucoli náner ménienwe mir se.
Ar arcanelte sello i ávas mentumne te mir i undume.
Mal enge tasse hoa lámáre polcaiva nesselesse mí oron, ar arcandelte sello i lavumnes tien auta mir té. Ar láves tien.
Tá i raucor etelender et i nerello ar menner mir i polcar, ar i lámáre lende rimpa olla i lanca mir i ailin ar quorner.
Mal íre i mavari cenner ya náne martienwa, úşelte ar nyarner sa i ostosse ar i restassen.
Tá queni ettúler cenien ya náne martienwa, ar túlelte Yésunna ar hirne i nér yello i raucor náner ettúlienwe, cólala lanneli ar málesse sámo, hámala ara Yésuo talu, ar anelte ruhtaine.
I cenner sa nyarner tien manen i raucoharyaina nér náne rehtaina.
Ar i quanda liyúme i *oscaitala nóriello Erasenyaiva arcaner sello i autumnes tello, an anelte mapaine túra ruciénen. Tá lendes mir i lunte ar cirne oa.
Ono i nér et yello i raucor náner túlienwe inque i lavumnes sen lemya óse. Mal mentanes i nér oa, quétala:
“Nanwena marelyanna, ar nyara pa i nati yar Eru acárie lyen.” Ar lendes oa ter i quanda osto, cárala sinwa ilqua ya Yésus carne sen.
Íre Yésus nanwenne, anes cámina lo i şanga, an illi *lartaner sen.
Mal yé! nér estaina Yairo túle, ar sé náne i turco i *yomencavo. Ar lantanes undu epe Yésuo occat ar arcane sello i tulumnes coaryanna,
an enge óse yelde, erya hínarya, ye sáme *os loar yunque, ar sé náne hare qualmenna.Íre Yésus náne malleryasse, i şangar ninder senna.
Ar nís, ye ter loar yunque perpére celuménen serceva, yen ua ence ñete nestie ho aiquen,
túle ca se ar appane collaryo lane, ar mí imya lú i celume serceva pustane.
Ar eque Yésus: “Man náne ye appane ni?” Íre illi laquenter sa, Péter quente: “*Peantar, i şangar lumnar lyen ar nírar lyenna.”
Ananta Yésus quente: “Quén appane ni, an túnen i lende túre et nillo.”
Cénala i uas úşe tuntie, i nís túle pálala ar lantane undu epe se ar nyarne epe i quanda lie i casta yanen appanes Yésus ar manen anes nestaina mí imya lú.
Mal Yésus quente senna: “Yelde, savielya erehtie lye; á auta rainesse!”
Íre en quentes, nér túle coallo i turcova i *yomencavo, quétala: “Yeldelya aquálie. Áva *tarasta i *peantar ambe.”
Mal hlárala ta, Yésus hanquente senna: “Áva ruce; eryave sava, ar nauvas rehtaina.”
Íre túles i coanna uas láve aiquenen lelya minna, hequa Péteren ar Yohánon ar Yácoven ar i vendeo ataren yo amillen.
Mal queni náner yaimie ar palpaner inte nyéresse pa i vende. Ar eques: “Áva na yaimie, an uas qualle, mal nas lorna.”
Tá lalanelte senna, pan sintelte i anes qualin.
Mal nampes márya ar yáme, quétala: “Vende, á orta!”
Ar fairerya nanwenne, ar orontes mí imya lú, ar Yésus canne i mo antane sen nat matien.
Ar nostaryat nánet ara *intu; mal peantanes tún i ávatte nyare aiquenen pa ya náne martienwa.
Tá tultanes i yunque ar antane tien turie ar túre or ilye i raucor, ar láve tien nesta hlívi.
Ar te-mentanes carien sinwa Eruo aranie ar nestien,
ar quentes téna: “Cola munta i lendan – lá vandil hya poco, lá masta hya telpe, ar áva tala laupe atta.
Mal íre lelyalde mir coa, á lemya tasse ar á auta talo.
Ar mi ilya nóme yasse queni uar cime le, íre autealde sana ostollo á pala i asto taluldalto ve *vettie tien.”
Tá, etelelyala, lendelte ter i ména mastollo mastonna, cárala sinwa i evandilyon ar nestala queni ilya nómesse.
Sí Herol i *canastantur hlasse pa ilqua ya martane, ar anes rúcina, pan náne quétina lo quelli i Yoháno náne ortaina et qualinillon,
mal lo exeli i Elía náne túlienwa, mal lo exeli i náne ortaina er imíca i enwine Erutercánor.
Eque Herol: “Yoháno cas aucirnen. Man, tá, ná quén sina pa ye hlarin taiti nati?” Ar cestanes velitas.
Ar íre i aposteli nanwenner nyarnelte Yésun pa ilye cardaltar. Tá talleset ar lende eressea nómenna, ara osto estaina Vet-Saira.
Mal i şangar, istala yanna lendes, hilyaner se. Ar camneset ar carampe téna pa Eruo aranie, ar nestanes i sámer maure nestiéva.
Tá, íre i aure náne fifírula, i yunque túler senna ar quenter senna: “Á menta i şanga oa, menieltan mir i mastor ar restar *os vi, hirien nóme lemien ar matso, an sisse nalve eressea nómesse.”
Mal quentes téna: “Elde áten anta nat matien.” Quentelte: “Ualme same amba lá mastar lempe ar lingwe atta, qui lá cé menuvalme ar mancuvalme immen matso ilye sine quenin.”
An anelte *os neri húmi lempe. Mal quentes hildoryannar: “Tyara te caita undu mi hostar queniva *os *lepenquean mi ilya hosta.”
Ar carnelte sie ar tyarne te illi caita undu.
Tá nampes i mastar lempe ar lingwe atta ar yente mir menel, aistane tai ar rance tai, ar antanes tai i hildoin panien tai epe i şanga.
Ar illi manter ar náner quátine, ar i lemyala rantar yar hostanelte quanter *vircolcar yunque.
Ar túle, íre hyamnes nála erinqua, i ocomner senna i hildor, ar maquentes téna sie: “Man i şangar quetir i nanye?”
Hanquentasse quentelte: “Yoháno i *Tumyando; hya exeli, Elía, hya exeli: i er i enwine Erutercánoron órie.”
Tá quentes téna: “Mal elde, man quetilde i nanye?” Hanquentasse eque Peter: “Eruo Hristo!”
Tá mi naraca quetie peantanes tien i ávalte quetumne sie aiquenna,
mal eques: “Mauya i Atanyondon mene ter rimbe perperiéli ar náve *auquerna lo i amyárar ar hére *airimor and parmagolmor, ar náve nanca, ar i neldea auresse nauvas ortaina ama.”
Tá quentes: “Qui aiquen mere tule apa ni, mauya sen váquete insen ar orta tarwerya ilya auresse ar hilya ní.
An aiquen ye mere rehta cuilerya, sen nauvas vanwa, mal aiquen yeo cuile ná vanwa márienyan, sé rehtuva sa.
É manen ná aşea atanen qui ñetis i quanda mar, mal véra quenerya ná sen vanwa hya nancarna?
An aiquen ye ná *naityana inyenen ar quettanyanen, sénen i Atanyondo nauva *naityana íre tuluvas alcareryasse ar mí Ataro ar i airi valion alcar.
Mal quetin lenna nanwiesse: Ear quelli i tarir sisse i uar tyavuva qualme nó cenuvalte Eruo aranie.”
Túle apa quettar sine, apa auri tolto, i talles Péter ar Yoháno ar Yácov ar lende ama mir i oron hyamien.
Ar hyamieryasse cendeleryo ilce ahyane, ar lanneryar mirilyaner ninquissenen.
Ar yé! nér atta carampet óse, yet nánet Móses ar Elía.
Tannette *intu alcaresse ar quenter pa i autie ya telyumnes Yerúsalemesse.
Ono Péter ar i enger óse náner lunge húmenen; mal apa coivie cennelte alcarerya ar i nér atta tára óse.
Ar íre tú oantet sello, Péter quente Yésunna: “*Peantar, mára ná i nalme sisse. Etta alme orta *lancoar nelde, er elyen ar er Mósen ar er Elían” – an uas sinte ya quentes.
Mal íre quentes nati sine enge lumbo ya teltane te, ar anelte ruhtaine íre mennelte mir i lumbo.
Ar óma túle et i lumbullo, quétala: “Si ná yondonya, ye anaie cílina. Á lasta senna!”
Ar apa i óma túle, Yésus náne hírina erinqua. Mal anelte quilde ar uar nyarne aiquenen yane auressen pa i nati yar cennelte.
I hilyala auresse, íre túlelte undu i orontello, hoa şanga velle se.
Ar yé! nér yáme et i şangallo, quétala: “*Peantear, iquin lyello i cenil yondonyanna, an náse erya hínanya,
ar yé! rauco mapa se, ar rincenen yamis, ar tyarises rihta ar falasta, ar urdave autas sello apa harnie se.
Ar inquen hildolyallon i et-hatumner i rauco, mal ua ence tien.”
Hanquentasse Yésus quente: “A úvoronda ar rícina *nónare, manen andave mauyuva nin lemya aselde ar cole le? Á tulya yondolya sir!”
Mal íre en anes túlala hare, i rauco hante se i talamenna ar tyarne se rihta. Ono Yésus naityane i úpoica faire ar nestane i seldo ar se-antane ataryan.
Ar illi náner elmendasse pa Eruo meletya túre.Sí íre anelte mapaine elmendanen pa ilye i nati yar carnes, quentes hildoryannar:
“Oi hepa quettar sine sámasse, an i Atanyondo nauva antaina olla mannar atanion.”
Mal ualte hanyane quetie sina; anes nurtaina tello, pustien te tuntiello mana tenges, ar runcelte maquetiello senna pa quetie sina.
Tá enge cos mici te pa man náne i antúra mici te.
Yésus, istala manen sannelte endaltasse, nampe hína ar panyane se ara inse
ar quente téna: “Aiquen ye came hína sina essenyasse came ní, ar aiquen ye came ní came ye ni-mentane. I ampitya mici le ná i antúra.”
Hanquentasse eque Yoháno: “*Peantar, cennelme nér et-háta raucor esselyanen, ar névelme pusta se, pan únes aselme.”
Mal Yésus quente senna: “Ávase pusta, an ye ua tare lenna ná elden!”
Sí íre i lúme túle hare náveryan cámina ama, quernes cendelerya Yerúsalemanna menien tar.
Mentanes tercánoli epe inse, ar oantelte ar lender mir masto Samáreaiva, manwien sen.
Mal ualte merne camitas, pan anes malleryasse Yerúsalemenna.
Íre i hildor Yácov yo Yoháno cennet ta, quentette: “Heru, ma meril i canuvalve náren tule undu menello nancarien te?”
Mal quernes inse ar tu-naityane.
Tá lendelte hyana mastonna.
Íre lendelte i mallesse quén quente senna: “Hilyuvan lye ilya nómenna yanna autal.”
Ar Yésus quente senna: “Rusqui samir eccar ar menelo aiwi samir *haustar, mal i Atanyondo ua same nóme panien carya.”
Tá quentes hyana nerenna: “Áni hilya!” I nér quente: “Lava nin minyave auta ar tala atarinya i sapsanna.”
Mal quentes senna: “Lava qualinin tala qualiniltar i sapsanna, mal elye mena ar cara menelo aranie sinwa!”
Ar exe quente: “Hilyuvan lye, Heru, mal minyave lava nin quete namárie innar ear coanyasse.”
Ono Yésus quente senna: “*Úquén ye apánie márya i hyaresse ar yéta i nati yar caita ca se, ná mára Eruo aranien.
Apa nati sine i Heru cille exi *otoquean ar etementane te atta ar atta epe inse, ilya mastonna yanna sé immo tulumne.
Quentes téna: “I yavie ná úvea, mal i molir nar mance. Etta á arca i yaviéno Herullo, mentieryan queni hostien yavierya.
Á etelelya! Yé! Mentan le ve euler mici ñarmor.
Áva cole *pocolle, hya poco matson, hya hyapat; ar áva *suila aiquen i mallesse.
Íre tulilde mir coa, minyave queta: 'Nai coa sina samuva raine!'
Ar qui ea tasse meldo raineo, rainelda seruva senna. Mal qui ua ea er, nanwenuvas lenna.
Etta á lemya coa tanasse, mátala ar súcala i nati yar antalte len, an ye mole ná valda *paityaleryo. Áva leve coallo coanna.
Ente, íre menilde mir osto ar camilde le, mata i nati panyaine epe le,
ar á nesta i hlaiwar sasse, ar nyara tien: 'Eruo aranie utúlie hare lenna.'
Mal quiquie tulilde mir osto ar ualte came le, mena mir palle malleryar ar queta:
'Yando i asto ya himyane talulmanta ostoldallo palalme oa lenna. Ono nat sina alde ista, i utúlie hare Eruo aranie!'
Nyarin lenna: Soromen i aure namiéva nauva *cólima lá ya nauvas sana oston.
Horro lyen, Corasin! Horro lyen, Vet-Saira! An qui i taure cardar yar amartier letse martaner mi Tír ar Síron, andanéya hirnelte inwis, hámala mi *fillanne ar *litte.
Etta Tíren ar Síronen i namie nauva *cólima lá lent!
Ar elye, Capernaum, ma cé nauval ortaina menelenna? Undu Mandostonna tuluval!
Ye lasta lenna lasta ninna. Ar ye loita cime lé, loita cime ní. Ente, ye loita cime ní, loita cime ye ni-mentane.”
Tá i *otoquean nanwenner mi alasse, quétala: “Heru, yando i raucoin mauya cime canwalmar mi esselya!”
Tá quentes téna: “Cennen Sátan lantienwa ve íta et menello.
Yé! Ánien len i túre *vettien nu taluldat leucar ar *nastaror ar i quanda melehte i ñottova, ar munta pole harna le.
Mal áva same alasse pa turielda or i raucor, mal sama alasse pan esseldar anaier técine menelde.”
Lúme yanasse anes anvalima i Aire Feanen ar quente: “Laitan tye, Atar, Heru or menel cemenye, an unurtiel nati sine sailallon ar handallon, ar ápantiel tai lapsin. Ná, Átar, an carie sie náne mára hendulyatse.
Ilye nati anaier antaine olla nin lo Atarinya, ar man i Yondo ná *úquen ista hequa i Atar, ar man i Atar ná, *úquen ista hequa i Yondo, ar aiquen yen i Yondo mere apanta se.”
Ar apa querie inse i hildonnar quentes: “Valime nát i hendu yat yétat yar elde yétar.
An quetin lenna: Rimbe Erutercánoli ar aralli merner cene i nati yar elde yétar, mal ualte cenne tai, ar hlare i nati yar elde hlarir, mal ualte hlasse tai.”
Ar yé! şanyengolmo oronte, tyastien se, ar eques: “*Peantar, mana caruvan náven aryon oira coiviéno?”
Quentes senna: “Mana técina i Şanyesse? Manen hental?”
Hanquentasse quentes: “Alye mele i Héru Ainolya quanda endalyanen ar quanda fealyanen ar quanda poldorelyanen ar quanda sámalyanen, ar armarolya ve imle.”
Yésus quente senna: “Hanquentes mai; cara sie ar samuval coivie.”
Mal sé, mérala tana i anes faila, quente Yésunna: “Ar man ná armaronya?”
Hanquentasse Yésus quente: “Nér lelyala undu Yerúsalemello Yericonna lantane mici piluli, i namper lanneryar ar se-palpaner, se-hehtala imbe coive ar qualme.
Ve martane, *airimo lende undu sana mallesse, mal íre se-cennes, langanes i hyana rímasse i malleo.
Mí imya lé yando Levíon, íre túles undu i nómenna ar cenne se, langane oa sello.
Mal Samárea nér lelyala i mallesse túle senna, ar íre cenneses, endarya etelende senna.
Túles hare senna ar vaitane nahteryar, ulyala millo ar limpe or tai. Tá se-panyanes celvaryasse ar talle se *sendassenna ar cimne maureryar.
Ar i hilyala auresse nampes lenár atta, antane tu i turcon i *sendasseo, ar quente: 'Cima maureryar, ar *aiqua ya *yuhtal han si, inye *nampaityuva lyen íre nanwenuvan sir.'
Man mici nelde sine sanal carne inse armaro i neren ye lantane imíca i pilur?”
Eques: “Ye oráve sesse.” Yésus tá quente senna: “Alye mene ar care i imya nat.”
Sí íre oantelte lendes mir masto. Sisse nís estaina Marşa camne se mir coarya.
Enge as nís sina néşa yeo esse náne María, ye hamne undu ara i Heruo talu ar lastane quettaryanna.
Mal Marşa sáme máryat quante olya moliénen, ar túles ar quente: “Heru, ma ua valda lyen i néşanya ni-ehehtie erinqua molienyasse? Etta queta senna i mauya sen manya ni.”
Hanquentasse i Heru quente senna: “Marşa, Marşa, samil quárele ar nalye *tarastaina pa rimbe natali.
Mal erya natwa ea maure. María icílie i mára ranta, ar sá ua nauva mapaina sello.”
Ar túle, íre enges nómesse hyámala, i íre pustanes quén hildoryaron quente senna: “Heru, ámen peanta hyame, ve Yoháno yando peantane hildoryain.”
Tá quentes téna: “Quiquie hyamilde, queta: Atar, na aire esselya. Aranielya na tuluva.
Ámen anta ilaurea massalma aure apa aure,
ar ámen apsene úcarelmar, an yando elme apsenir ilquenen ye same rohta men; ar ávame tulya úşahtienna.”
Ar quentes téna: “Man mici le ye same meldo lelyuva senna endesse i lómio ar quetuva senna: 'Meldo, áni *yutya massainen nelde,
an meldo ninya utúlie ninna lendallo ar samin munta panien epe se'?
Ar sé i mityallo quete hanquentasse: 'Ávani *tarasta! I fenna ná pahta, ar hínanyar nar asinye i caimasse; ua ece nin orta antien lyen.'
Quetin lenna: Qui uas orta ar anta sen pan náse meldorya, é ortuvas ar antuva sen vorongandeleryanen.
Etta quetin lenna: Á arca, ar nauva len antaina; á cesta, ar hiruvalde; á tamba, ar nauva len latyaina.
An ilquen arcala came, ar ilquen cestala hire, ar ilquenen tambala nauva latyaina.
Lau ea atar mici le ye, qui yondorya arca lingwe, cé antuva sen leuca mende lingwio?
Hya qui yando arcas ohte, lau antuvas sen *nestaro?
Etta, qui elde i nar olce istar anta máre annar hínaldain, manen ole i menelda Atar antuva Aire Fea in arcar sello!”
Ar et-hantes úpa rauco. Apa i rauco ettúle, i úpa nér carampe. Ar i şangar náner elmendasse.
Mal quelli mici te quenter: “Et-hatis i raucor turconen i raucoron, Vélsevul.”
Ono exeli, tyastien se, cestaner sello tanwa et menello.
Istala símaltar quentes téna: “Ilya aranie şanca insanna ná nancarna, ar már şanca insanna lanta.
Mal qui yando Sátan ná şanca insenna, manen aranierya taruva? An quetilde i et-hatin i raucor Vélsevúlo túrenen.
Qui túrenen Vélsevúlo et-hatin raucor, mannen yondolyar et-hatir te? Sina castanen nauvalte námoldar.
Mal qui Eruo lepernen et-hatin i raucor, Eruo aranie é utúlie lenna.
Íre polda nér, arwa carmaron, varya véra pacarya, armaryar nar rainesse.
Mal íre nér polda lá sé tule ar *orture se, mapas oa i carmar ya i exe sanne antumner sen varnasse, ar etsatis i nati yar píles sello.
Ye ua asinye tare ninna, ar ye ua hosta, vinta.
Íre úpoica faire tule et nerello, langas ter parce ménar cestala nóme séreva, ar íre uas hire, quetis: 'Nanwenuvan coanyanna yallo léven.'
Ar íre tulis, hiris sa carna poica ar netyaina.
Tá autas ar tala óse hyane fairi otso, olce lá inse, ar apa tulilte minna marilte tasse, ar tana neren i métima sóma nauva ulca lá i minya.”
Sí íre sé náne quétala nati sine, nís et i şangallo ortane ómarya ar quente senna: “Valime i súma ya lye-colle ar i i tyetsu yalto anel tyétina!”
Mal eques: “Arya ná qui quetil: Valime nar i hlarir Eruo quetta ar hepir sa!”
Íre i şangar ocomner, quentes: “*Nónare sina ná ulca *nónare! Arcas tanwar, mal tanwa ua nauva antaina san, hequa Yóno tanwa.
An ve Yóna náne carna tanwa i lien Ninevesse, sie yando i Atanyondo nauva tanwa *nónare sinan.
Hyarmeno tári nauva ortaina i namiesse as i queni *nónare sino ar namuva te ulce, an túles cemeno mettallo hlarien Solomondo sailie – ar yé, sisse ea amba lá Solomon!
I queni Ninevello ortuvar i namiesse as *nónare sina ar namuva sa ulca. An hirnelte inwis Yóno *nyardiénen, ar yé, sisse ea amba lá Yóna!
Apa nartie calma, mo ua panya sa cambosse hya nu lestacolca, mal i calmatarmasse, lávala in tulir minna cene i cala.
I hroan, i calma ná hendelya. Íre heldenya ná málesse, quanda hroalya ná yando calima, mal íre nas olca, hroalya ná yando morna.
Etta cima i sana cala ya ea lyesse ua mornie!
Sie, qui quanda hroalya ná calyaina, lá arwa ranto ya ná morna, ilqua nauva calyaina ve íre i calma calya lye alcaryanen!”
Apa quentes ta, Farisa arcane i matumnes mat óse. Etta lendes minna ar caine undu ara i sarno.
Mal i Farisa náne elmendasse íre cennes i uas minyave sove inse nó i mat.
Mal i Heru quente senna: “Sí, a Farisar! Poitalde i ette i yulmo ar veneo, mal mityaldasse nalde quante pilweo ar olciéno.
Úhandar! Ye ontane i ette, ma uas ontane yando i mitya?
Mal á anta annar penyain et yallo ea mityaldasse, ar yé! ilqua nauva poica len.
Mal horro len, a Farisar, pan antalde i quaista ranta i minto ar i *laiquelisso ar ilya queo, mal langalde i failie ar i melme Eruva! Nati sine mauyaner len care, ú lengiéno i exi.
Horro len, a Farisar, an melilde same i minde sondar i *yomencoassen ar came i *suilier mí mancalenómi.
Horro len, an nalde ve i noiri yar uar aşcénime, tyárala queni vanta olla tai ú istiéno.”
Hanquentasse quén imíca i şanyengolmor quente senna: “*Peantar, quetiénen nati sine antal ulca esse yando elmen.”
Tá quentes: “Horro yando len i nar şanyengolmor, an panyalde atanissen cólor urde colien, mal elde uar appa i cólar eryanen leperildaron!
Horro len, an carastalde i noiri i Erutercánoin, mal atarildar nacanter te!
É nalde astarmor ar sanar mai pa atarildaron cardar, an té necanter i Erutercánor, mal elde carastar noiriltar!
Sina castanen Eruo sailie quente: Mentuvan téna Erutercánor ar aposteli, ar nahtuvalte ar roituvalte quelli mici te,
ar sie ilye i Erutercánoron serce ya anaie ulyaina i mardo tulciello tenna sí nauva cánina nónare sinallo,
sercello Ávelo tenna serce Secarío, ye náne nanca imbi i *yangwe ar i coa. Ná, nyarin len, nauvas cánina *nónare sinallo!
Horro len i nar şanyengolmor, an amápielde oa i *latil istyava; elde uar lende minna, ar i merner lelya minna pustanelde!”
Íre etelendes talo i parmangolmor ar i Farisar *yestaner nire senna aicave ar ulya senna maquetiéli pa hyane natali,
se-tírala mapien nat et antoryallo.
Lúme yanasse, íre i şanga náne ocómienwa mi *quaihúmeli, ta rimbe i quén *vattane i exe undu, *yestanes quete hildoryannar minyave: “Hepa inde oa i *pulmaxello i Fariryaron, yá *imnetie ná.
Mal ea munta nurtaina ya ua nauva apantaina, hya nulda ya ua nauva sinwa.
Etta, ilye nati yar quetilde i morniesse nauvar hlárine i calasse, ar ya hlussalde vére şamberyassen nauva carna sinwa i coaron tópallon.
Ente, quetin lenna i nar meldonyar: Áva ruce illon nahtar i hroa ar epeta uar pole care amba.
Mal tanuvan len manello mauya len ruce: Ruca yello apa nehtie same i túre hatiéva mir Ehenna. Ná, quetin lenna, ruca sello!”
Filit atta nát vácine mitta urusteva attan, lá? Ananta Eru ua loita enyale er mici tu.
Mal yando careldo findi nar illi nótine. Áva ruce; nalde mirwe lá rimbe filici.
Mal quetin lenna: Ilquen ye *etequenta ni epe atani, sé i Atanyondo yando *etequentuva epe Eruo vali.
Mal ye laquete ni epe atani nauva laquétina epe Eruo vali.
Ar ilquenen ye quete quetta i Atanyondonna, ta nauva apsénina, mal yen naiquete i Aire Feanna, ta ua nauva apsénina.
Mal íre talalde le epe combi ar cánor ar túri, áva same quárele pa manen hya mananen varyuvalde inde, hya pa mana quetuvalde;
an i Aire Fea peantuva lúme entasse pa i nati yar mauyar len quete.”
Tá quén i şangasse quente senna: “*Peantar, queta hánonyanna i mauya sen anta nin masse i *aryoniesse!”
Quentes senna: “A nér, man ni-panyane namien let hya etsatien lent *aryoniesta?”
Tá quentes téna: “Cima ar á varya inde ilya milciello, an yando íre quén same úve, coivierya ua tule armaryallon.”
Ente, quentes téna sestie, quétala: “I nóre lárea nerwa antane hoa yávie.
Ar carnes úvie sámaryasse, quétala: 'Mana caruvan, sí ire penin nóme yasse polin hosta yávienya?'
Ar quentes: 'Caruvan sie: Nancaruvan hauranyar, ar carastuvan haurali hoe lá tai, ar tasse hostuvan ilya orenya ar ilye máre natinyar;
ar quetuvan imninna: 'Sí samil rimbe máre natali haurasse rimbe loalin. Sera, mata, suca, sama alasse!'
Mal Eru quente senna: 'Úhanda nér! Lóme sinasse canilte cuilelya lyello. Tá man samuva yar ahastiel?' `
Sie euva yen hosta harma insen, mal ua lárea Erun.”
Tá quentes hildoryannar: “Sina castanen quetin lenna: Áva same quárele pa inde, pa mana matuvalde, hya pa hroaldar, pa i lanni yar coluvalde.
An i cuile ná mirwa lá matso, ar i hroa lá lanni.
Cima i quácor, i ualte rere hya *cirihta, ar ualte same haurar hya ataqui hepien nati, ananta Eru anta tien masto. Manen mirwe lá aiwi elde nar!
Man mici le, quáreleryanen, pole napane erya *perranga coivieryo andienna?
Qui sie ualde pole care i ampitya nat, manen ná i samilde quárele pa i lemyala nati?
Cima manen i indili alir; ualte móta hya lanya, mal nyarin len: Yando Solomon ilya alcareryasse úne netyaina ve er mici té!
Qui Eru sie netya i celvar i restasse, i ear síra ar enwa nar hátine mir urna, manen ole ambe netyuvas lé, elde pitya saviéno!
Etta áva cesta mana matuvalde hya mana sucuvalde, ar áva same quárele,
an ilye nati sine nar yar i mardo nóri cestar, ono Atarelda ista i samilde maure sine nativa.
Mal á cesta minyave aranierya, ar nati sine nauvar len napánine!
Áva ruce, á pitya lámáre, an Atarelda asánie mai anta len i aranie!
Vaca yar samilde ar á anta annar i penyain. Cara len *pocoller i ua oi yeryuvar, *alaloitala harma menelde, yasse arpo ua tule hare hya malo ammate.
An yasse harmalda ea, tasse yando endalda euva.
Hepa i quilta oşweldasse ar calmaldar uryala,
ar elde na ve atani yétala ompa herulto entulessenna i veryanwello, polieltan latya sen mí imya lú ya tulis tambala.
Valime nar i móli ion heru, íre nanwenis, hire te tíra! Násie quetin lenna: Notuvas *os oşwerya ar lavuva tien caita ara i sarno, ar tuluvas náven núro tien.
Ar qui tulis mí attea tirisse, hya mí neldea, ar hire te sie, valime nalte!
Mal á ista nat sina, i qui i *coantur sinte i lúme yasse i arpo tulumne, anes coiva ar ua láve aiquenen race mir coarya.
Yando elde na manwaine, an i Atanyondo túla mi lúme ya ualde sana.
Tá Péter quente: “Heru, ma quetil sestie sina elmenna hya yando ilyannar?”
Ar i Heru quente: “É man ná i voronda mardil, i handa, yeo ortíriesse herurya panyuva ilye núroryar, antieryan tien matsolta mí vanima lúme?
Valima ná mól tana, ye herurya íre tulis hiruva cára sie.
Nanwave quetin lenna i or ilye armaryar se-panyuvas.
Mal qui mól tana quete endaryasse: 'Herunya ua tuluva rato', ar *yestas pete i núror ar i *núri, ar mate ar suce ar quate inse limpenen,
mól tano heru tuluva auresse ya uas sana ar lúmesse ya uas ista, ar se-peryuvas ar sen-antuva masserya as i úvorondar.
Tá mól tana ye hanyane heruryo şelma, mal ua manwane inse hya carne şelmarya, nauva palpaina rimbe tarambolínen.
Mal ye ua hanyane sa, ar sie carne nati valde palpiéno, nauva palpaina mance tarambolínen. An ho ilquen yen olya anaie antaina, olya nauva cánina, ar ho quén yeo ortíriesse apánielte olya, canuvalte amba lá senwa.
Náre utúlien hate cemenna, ar mana meruvan qui anaies nartaina yando sí?
*Tumyale samin yanen nauvan *tumyaina, ar manen nírina nanye tenna anaies telyaina!
Ma sanalde i utúlien antien raine cemende? Ui, quetin lenna, mal şancie!
An ho sí euvar lempe şance erya coasse, nelde attanna ar atta neldenna.
Nauvalte şance, atar yondonna ar yondo atarenna, amil yeldenna ar yelde amillenna, ar nís yondoryo verinna, ar i veri veruryo amillenna.”
Tá yando quentes i şangannar: “Íre cenilde lumbo ortea Númesse, mí imya lú quetilde: Raumo túla – ar sie euva.
Ar íre cenilde şúre ya váva Hyarmello, quetilde: Lauca nauva – ar sie euva.
*Imnetyandor, istalde henta cemeno ar menelo ilce, mal manen ná i ualde ista henta lúme sina?
Manen ná i ualde name yando elden mana faila ná?
An íre ménal as ñottolya námonna, rica mí malle náven léra costelyallo óse – hya tucuvas lye epe i námo, ar i námo antuva lye olla i *námondurenna, ar i *námondur hatuva lye mir mando.
Nyarin lyen: Ual ettuluva talo nó apaitiel i métima pitya ranta urusteva!”
Lúme yanasse quelli i enger tasse nyarner sen pa i queni Alileallo ion serce Piláto etulyane mir serce *yancaltaron.
Etta hanquentasse quentes téna: “Ma intyalde i queni sine Alileallo náner úcarindoli ambe túre lá ilye exi Alileasse, pan perpérelte ta?
Laume, quetin lenna; mal qui ualde hire inwis, illi mici le nauvar nancárine mí imya lé.
Hya i toloque innar i mindo Siloamesse lantane, nahtala te – ma intyalde i sámelte rohta túra lá ya ilye exi i marir Yerúsalemesse samir?
Laume, quetin lenna; mal qui ualde hire inwis, illi mici le nauvar nancárine mí imya lé.
Tá quentes téna sestie sina: “Nér sáme *relyávalda tarwaryasse liantassion, ar túles cestien yáve sesse, mal hirnes munta.
Tá quentes i *tarwandurenna: 'Yé, loassen nelde utúlien cestala yáve *relyávalda sinasse, mal ihírien munta. Áse cire undu! Mana casta ea hepien se tasse, hépala i talan ú antiéno yáve?'
Hanquentasse quentes senna: 'Heru, ásen lave tare yando loa sinasse, ar sapuvan *os se ar panyuva múco tasse.
Qui tá antuvas yáve i túlala loasse, mára ná. Mal qui laias, ciruvalyes undu.'”
Sí Yésus náne peantala mi er i *yomencoaron i *sendaresse.
Ar yé! enge tasse nís ye ter loar toloque náne haryaina lo faire se-cárala milya: Anes cúna, ar ua ence sen tare téra.
Mal íre cennes i nís, Yésus yalle se insenna ar quente: “Nís, nalye leryaina i hlívello lye-cárala milya.”
Ar panyanes máryat sesse, ar mí imya lú orontes tarien téra, ar antanes alcar Erun.
Mal i turco i *yomencavo, nála rúşea pan Yésus nestane i *sendaresse, quente i şanganna: “Ear auri enque yassen mauya mole! Etta tula mi tai náven nestaina, ar lá i *sendaresse!”
Mal i Heru hanquente senna: “*Imnetyandor, ma ua ilquen mici le i *sendaresse lehta mundorya hya *pelloporya ho i salquecolca ar tulya se oa sucien?
Mal nís sina, ye ná Avraham yelde ye Sátan ehépie hampa ter loar toloque – ma ua mauyane se-lehta núte sinallo i *sendaresse?”
Ar íre quentes ta, ilye i tarner senna náner nucumne, ar i quanda şanga sáme alasse pa ilye i alquarinque nati yar martaner sénen.
Etta quentes: “Ve mana Eruo aranie, ar as mana sestuvanyes?
Nas ve erde *sinapio, ya nér nampe ar panyane tarwaryasse, ar alles mir alda, ar menelo aiwi marner olvaryassen.”
Ar ata quentes: “As mana sestuvan Eruo aranie?
Nas ve *pulmaxe, ya nís nampe ar nurtane lestassen nelde poriva, tenna ilqua náne púlienwa.”
Ar lendes ostollo ostonna ar mastollo mastonna, peantala íre lelendes Yerúsalemenna.
Sí quén quente senna: “Heru, ma nar i rehtainar mance?” Quentes téna:
“Rica menien minna ter i náha fenna! An rimbali, quetin lenna, cestuvar mene minna, mal ualte poluva,
i lúmello ya i heru i cavo orontie ar apahtie i fenna, ar tarilde i ettesse, tambala i fennasse, quétala: 'Heru, ámen latya!' Mal hanquentasse quetuvas lenna: 'Uan ista mallo utúlielde.'
Tá quetuvalde: 'Mantelme ar suncelme epe lye, ar peantanel mallelmassen!'
Mal hanquetuvas lenna sie: 'Uan ista mallo utúlielde. Heca nillo, ilye i carir úfailie!'
Tasse yaimelda ar molielda nelciva euvar, íre cenilde Avraham ar Ísac ar Yácov ar ilye i Erutercánor mi Eruo aranie, mal elde nar hátine ettenna.
Ente, queni tuluvar Rómello ar Númello ar Formello ar Hyarmello, ar caituvalte ara i sarno mi Eruo aranie.
Ar yé! ear teldali i nauvar minye, ar ear minyali i nauvar telde.”
Lúme yanasse túler Farisáli i quenter senna: “Mena ettenna ar á auta silo, an Herol mere nahta lye!”
Ar quentes téna: “Mena ar queta rusco tananna: Yé, et-hatin raucor ar carin nestie síra ar enwa, ar i neldea auresse nauvan telyaina.
Ananta mauya nin lelelya mallenyasse síra ar enwa ar i hilyala auresse, an ua lávina Erutercánon náve nancarna ettesse Yerusalémo.
Yerúsalem, Yerúsalem, ye nahta i Erutercánor ar *sarya i anaier mentaine senna – manen rimbave mernen comya hínalyar ve poroce comya nessaryar nu rámaryat! Mal ualde merne.
Yé, coalda ná hehtaina elden! Nyarin lenna i laume cenuvalde ni tenna quetilde: Aistana ná ye túla mí Héruo esse!”
Ar mi lú íre lendes mir coa turcova i Farisaron matien masta, tirneltes harive.
Ar yé! enge tasse nér epe se ye náne púlienwa, arwa acca olya neno hroaryasse.
Etta Yésus carampe i şanyengolmonnar, quétala: “Ma ná lávina i *sendaresse nesta, hya lá?”
Mal anelte quilde. Tá nampes i nér, nestane se ar mentane se oa.
Ar quentes téna: “Man mici le, qui yondorya hya mundorya lanta mir tampo, ua tucuva se ama i *sendaresse?”
Ar ualte pole hanquete nati sine.
Tá nyarnes sestie i nerin i náner tultaine, íre túnes manen cillelte i amminde nómi inten, quétala téna:
“Íre nalde tultaine lo quén veryanwenna, áva caita undu i amminda nómesse. Cé quén minda lá elye anaie tultaina lo se,
ar i quén ye lye-tultane tuluva ar quetuva lyenna: 'Lava nér sinan same i nóme!' Ar tá mauya lyen mene nucumna i amnalda nómenna.
Mal íre nalye tultaina, mena ar á caita i amnalda nómesse. Sie, íre i nér ye tultane lye tule, quetuvas lyenna: 'Meldo, tula ama, ambe inga!' Tá samuval alcar epe ilye i queni i anaier tultaine aselye.
An ilquen ye orta inse nauva nucumna, mal ye nucume inse nauva ortaina.”
Ente, quentes yenna tultane se: “Íre caril şinyemat hya ahtumat, áva tulta meldolyar, hya hánolyar, hya i queni nosselyo, hya lárie armarolyar. Cé yando té tultuvar lyé ata, ar ta nauva lyen *nampaityale.
Mal íre caril merende, á tulta penyar, *hroaloicali, *úlévimar, *cénelórar,
ar nauval valima, an té samir munta yanen ece tien paitya lyen. An nauva paityaina lyen qualinion ortiesse!”
Hlárala ta, quén mici i cainer ara i sarno quente: “Valima ná ye mate massa mi Eruo aranie!”
Yésus quente senna: “Enge nér ye manwane túra ahtumat, ar tultanes rimbali.
Ar mentanes núrorya i ahtumatto lúmesse, quetien innar náner tultaine: “Tula, an sí ilqua manwa ná!”
Mal illi mici te *yestaner anta castali uien tule. I minya quente senna: 'Amancien imnin resta, ar mauya nin lelya cenien sa. Iquin lyello, áva sana ulco pa ni pan ua ece nin tule!'
Ar exe quente: 'Amancien imnin yantar lempe mundoron, ar ménan tyastien te. Iquin lyello, áva sana ulco pa ni pan ua ece nin tule.'
Ar exe quente: 'Evérien nissenna, ar etta ua ece nin tule.'
Íre nanwennes, i mól nyarne nati sine heruryanna. Tá i cavo turco náne rúşea ar quente mólyanna: 'Mena lintiénen mir i maller ar tier i ostosse, ar á tala sir i penyar ar *hroaloicar ar *cénelórar ar *úlévimar!'
Ar i mól quente: “Heru, ya cannel anaie carna, ar en ea nóme.”
Ar i heru quente i mólenna: “Á etelelya mir i maller ar i pelerinnar ar áten mauya tule minna, quatien coanya.
An quetin lenna: *Úquen imíca i neri i náner tultaine tyavuvar ahtumattinya!'”
Sí hoe şangali lelender óse, ar quernes inse ar quente téna:
“Qui aiquen tule ninna ar ua teve atarya ar amillerya ar verirya ar hínaryar ar hánoryar ar néşaryar, é véra cuilerya, ua ece sen náve hildonya.
Yen ua cóla tarwerya ar túla apa ni, ua ece náve hildonya.
An man mici le i mere carasta mindo ua minyave hame undu onotien i nonwe carastiéno, istien qui samis fárea telpe telien sa?
Qui laias, cé panyas i mindo talma, mal ua ece sen telya i mindo, ar ilye i tírar cé *yestuvar quete yaiwe pa se,
quétala: 'Nér sina *yestane carasta, mal ua ence sen telya.'
Hya man ná i aran ye veluva hyana aran ohtasse ar ua minyave hame undu carien úvie qui polis ohtarinen húmi quean mahta yenna tule senna arwa húmi *yúquean?
Qui uas pole mahta senna, mentas queni i quetuvar rá sen íre i exe en haira ná, cestala raine.
Sie, etta, *úquen mici le ye ua quete namárie ilye armaryannar pole náve hildonya.
Singe é ná mára, mal qui i singeo tyáve nauva vanwa, mananen mo antuva san tyáve ata?
Uas mára cemnen hya ve múco véla. Queni hatir sa ettenna. Lava yen same hlaru lastien, lasta!
Ilye i *tungwemor ar úcarindor túler hare senna hlarien se.
Etta i Farisar ar i şanyengolmor nurruner, quétala: “Nér sina came úcarindor ar mate aselte!”
Tá quentes sestie sina téna:
“Man ná i nér mici le arwa mámaron tuxa ye ua, qui er mici te ná vanwa, hehtuva i nerte *neterquean i erumasse ar cestuva i vanwa máma tenna hirises?
Ar apa hirie se panyuvas se pontiryasse, arwa alasseo.
Ar apa tulie marda tultas meldoryar ar armaroryar ar quete téna: 'Na valime asinye, an ihírien mámanya, ye náne vanwa.'
Quetin lenna i mí imya lé, íre er úcarindo hire inwis, ea menelde alasse túra lá ya ea pa úcarindor nerte *neterquean i uar same maure inwisteva.
Hya man ná i nís ye same racmar quean ye ua narta calma ar poita coarya qui er mici tai ná vanwa, cestala harive tenna hirises?
Ar íre sa-ihíries, comyas melderyar ar *armareryar ar quete: 'Na valime asinye, an ihírien i racma ya náne vanwa nin!'
Sie, quetin lenna, euva alasse mici Eruo valar pa er úcarindo ye hire inwis.”
Tá quentes: “Enge nér as ye enget yondo atta.
Ar i ambe nessa mici tu quente ataryanna: “Atar, ánin anta i ranta i armaron ya lanta massenyan.” Tá *ciltanes laulestarya tún.
Epeta, apa lá rimbe auri, i ambe nessa yondo comyane ilqua ar lende oa mir haira nóre, ar tasse hantes oa telperya verca coiviesse.
Apa *yuhtanes ilqua enge urda saicele nóre tanasse, ar *yestanes same maure.
Lendes himyala er i quenion i marner nóre tanasse, ye mentane se mir restaryar tirien polcar.
Ar mernes quate inse i vainínen yar i polcar manter, mal *úquen antane sen nat.
Mal íre nanwennes laicenna quentes: “Ilye i paityane queni i molir atarinyan samir úve massava, íre inye quéla saicelénen!
Ortuvan ar lelyuvan atarinyanna, ar quetuvan senna: Atar, úacárien menelenna ar tyenna.
Uan valda en náven estaina yondotya, mal áni care ve er i paityane quenion i molir tyen.'
Ar orontes ar túle ataryanna. Íre en anes haiya, i atar cenne se, ar endarya etelende senna, ar nórala senna lantanes axeryanna ar minque se.
Mal i yondo quente: 'Atar, úacárien menelenna ar tyenna. Uan valda en náven estaina yondotya. Áni care ve er i paityane quenion i molir tyen.'
Ono i atar quente núroryannar: 'Lintave, á tala colla, i arya, ar ása panya sesse, ar á panya corma máryasse ar hyapat taluryatse,
ar á tala i maitana nessa mundo, áse nahta ar alve mate ar same alasse!
An yondonya sina náne qualin ar enutúlie coivienna; anes vanwa ar anaie hírina!' Ar *yestanelte same alasse.
Mal ambe yára hánorya náne i restasse, ar íre túles ar lende hare i coanna, hlasses lindale ar quelli liltala.
Yalles er i núrion ar maquente pa mana nati sine tenger.
Quentes senna: 'Hánolya utúlie, ar atarelya anahtie i maitana nessa mundo, pan *nanacámies se málesse.'
Mal anes rúşea ar ua merne mene minna. Tá atarya túle ettenna ar arcane sello.
Hanquentasse quentes ataryanna: 'Ter ta rimbe loar omótien tyen, ar canwatya uan oi rance, ananta uatye oi antane nin nessa *naico samienyan alasse as meldonyar.
Mal íre utúlie yondotya sina ye ammante laulestatya as *imbacindeli, nahtatye i maitana mundo sen!'
Tá quentes senna: 'Hína, illume anaietye asinye, ar ilye ninyar nar *tyenyar,
mal mauyane same alasse ar náve valime, an sina hánotya náne qualin ar nanwenne coivienna, ar anes vanwa ar anaie hírina!”
Tá quentes yando hildoryannar: “Enge atan ye náne lárea ar sáme quén ortírala coarya, mal hlasses ulca nyarie pa se – i hantes oa armaryar.
Etta se-tultanes ar quente senna: 'Mana nat sina ya hlarin pa lye? Á anta onótie pa molierya ve mardil, an ualye mahtuva i coa ambe.'
Tá i mardil quente insenna: 'Mana caruvan, sí íre herunya méra mapa oa molienya ve mardil? Poldorenya ua farya sapien. Nauvan nucumna qui mauya nin ique.
Mal istan mana caruvan, tyárala queni came ni mir coaltar íre ortírienya ná mapaina nillo!'
Ar yálala insenna i quanda rohtalie heruryo, quentes i minyanna: 'Mana i nonwe rohtalyo herunyan?'
Eques: 'Lestar tuxa ilmava.' Quentes senna: 'Á mapa hyalinilyar ar hama undu ar lintave teca *lepenquean!'
Epeta quentes exenna: 'Ar elye, mana i nonwe rohtalyo?' Quentes: 'Cor-lestar tuxa oriva.' Quentes senna: 'Á mapa hyalinelyar ar teca *toloquean!'
Ar i heru laitane i úfaila mardil, pan lenganes sailave –an *nónareltasse, randa sino yondor nar saile lá i calo yondor.
Ente, quetin lenna: Cara meldor elden i úfaila larnen! Sie, íre ta loita, le-camuvalte mir i oire mardi.
I quén ye ná voronda mi ta ya ná ampitya, ná voronda yando olyasse, ar i quén ye ná úfaila mi ta ya ná ampitya, ná úfaila yando mi olya.
Etta, qui ualde voronde pa i úfaila lar, man panyuva hepieldasse ta ya nanwa ná?
Ar qui ualde atánie voronwelda pa ta ya ná exeva, man antuva len *aiqua ya nauva véralda?
Ua ea núro yen ece náve mól heru attan, an tevuvas er ar meluva i exe, hya himyuvas er ar nattiruva i exe. Ua ece len náve móli Erun ar laren véla.”
Mal i Farisar, i méler telpe, lastaner ilye sine natinnar, ar carampelte senna yaiwenen.
Ar quentes téna: “Elde nar i quetir i nalde faile epe atani, mal Eru ista illion enda. An ta ya ná varanda atanin ná yelwa Erun.
I Şanye ar i Erutercánor náner tenna Yoháno; ho tá mo care i evandilyon sinwa, ar illi nírar sanna.
Menel cemenye autuvat nó erya tehta i Şanyesse lantuva oa.
Ilquen ye *cilta inse veriryallo ar verya exenna, race vestale, ar ye verya nissenna *ciltaina veruryallo, race vestale.
Mal enge nér ye náne lárea, ar tompes inse *luicarninen ar *páşenen, arwa alasseo ilya auresse mi alcarinqua lé.
Mal enge *iquindo yeo esse náne Lásaro, panyaina ara andorya, quanta sistelínen.
Mernes náve quátina yainen lantaner sarnollo i lárea nerwa, mal yando i huor túler ar láver sisteryar.
Túle i qualle i *iquindo, ar i valar colle se oa Avrahámo súmanna. Ente, yando i lárea nér qualle ar náne talaina sapsaryanna.
Ar latyanes henyat mi Mandos, yasse enges ñwalmelissen, ar cennes Avraham haiya ar Lásaro sumaryasse.
Etta yámes ar quente: 'Atar Avraham, órava nisse ar á menta Lásaro, panieryan leperyo tille nenesse, carien lambanya ringa! An nanye ñwalyaina náre sinasse!”
Mal Avraham quente: “Hína, enyala i camnel máre natilyar coivielyasse, ar Lásaro mí imya lé i úmáre nati; mal sí camis tiutale sisse, mal elye same naicele.
Ar ara ilye nati sine, hoa cilya anaie panyaina imbi elme ar elde. Sie i merir lelya silo lenna uar pole; mi imya lé queni uar pole lahta talo menna.”
Mal eques: “Tá arcan lyello, atar: Áse menta i coanna atarinyava –
an samin hánor lempe – *vettieryan tien, pustien yando té tuliello nóme sinanna ñwalmeva!”
Ono eque Avraham: “Samilte Móses ar i Erutercánor; á lastar téna!”
Mal sé quente: “Vá, atar Avraham, mal qui quén tuluva téna qualinillon, hiruvalte inwis!”
Ono quentes senna: “Qui ualte lasta Mósenna ar i Erutercánonnar, quén nanwénala qualinillon yando loituva vista sámalta.”
Tá Yésus quente hildoryannar: “Mauya i tulir i castar lantiéva, mal horro i quenen ter ye tulilte!
Qui ondo muliéva náne panyaina *os axerya, ar anes hátina mir i ear, ta náne sen arya lá tyarie i lante ero mici pityar sine!
Cima inde! Qui hánolya úcare, áse naitya, ar qui samis inwis, ásen apsene.
Ar qui lúr otso mi erya aure úcaris lyenna ar nanwenis lyenna lúr otso, quétala, 'Samin inwis!', ásen apsene!
Ar i aposteli quenter i Herunna: “Ámen anta amba savie!”
Mal i Heru quente: “Qui sámelde savie ve erde *sinapio, pollelde quete morpialda sinanna: 'Tuca ama şundulyar ar na empánina i earesse' – ar carumnes ve quetielda.
Mal man mici le, arwa mólo ye mole i hyarnen hya ve mavar, quetuva senna íre tulis i restallo: 'Tula sir lintiénen ar á caita undu ara i sarno'?
Ma uas ambe rato quetuva senna: 'Ánin manwa nat ahtumattinyan, ar nuta *os oşwelya ar *nura nin tenna amátien ar usúcien, ar epeta elye lerta mate ar suce' – ?
Lau sanas i samis rohta i mólen pan é carnes i nati pa yar camnes canwali?
Sie yando elde: Apa acárielde ilye i nati pa yar camnelde canwali, queta: 'Nalve úaşie móli. Acárielve ya rohtalva náne.'”
Ar lendaryasse Yerúsalemenna langanes ter i ende Samário ar Alileo.
Ar íre túles mir masto, helmahlaiwe neri quean veller se, mal pustanelte haire sello.
Ar ortanelte ómalta ar quente: “Yésus, *peantar, órava messe!”
Ar íre cennes te quentes téna: “Mena ar á tana inde i *airimoin.” Tá, íre anelte i tiesse, anelte poitaine.
Er mici te nanwenne íre cennes i anes nestaina, ar hoa ómanen antanes alcar Erun.
Ar lantanes cendeleryanna epe Yésuo talu ar hantane sen; anes Samárea nér.
Hanquentasse Yésus quente: “Ma únelte i quean nestaine? Masse, tá, nar i hyane nerte?
Ma enge *úquen ye nanwenne antien alcar Erun, hequa nér sina hyana nórello?”
Ar quentes senna: “Á orta ar mena; savielyanen acámiel mále.”
Mal íre anes maquétina lo i Farisar pa i lúme yasse Eruo aranie tulumne, quentes téna hanquentasse: “Eruo aranie ua tule mi lé lávala quenin cenitas hendultanten.
Ente, queni uar quetuva: 'Cena sisse!' hya 'Tasse!' An yé, Eruo aranie ea endeldasse.”
Tá quentes i hildonnar: “Tuluvar aureli yassen meruvalde cene er i aurion i Atanyondova, mal ualde cenuva sa.
Ar queni quetuvar lenna: 'Cena tasse!', hya: 'Cena sisse!' Áva lelya tar ar áva nore ca te.
An ve i íta, caltala er rantallo nu menel hyana rantanna nu menel, sie i Atanyondo nauva.
Mal minyave mauya sen perpere ole ar náve quérina oa lo *nónare sina.
Ente, ve martane auressen Noaho, sie euva yando i Atanyondo auressen:
Mantelte, suncelte, neri veryaner, nissi náner vertaine, tenna i aure yasse Noah lende mir i marcirya, ar i oloire túle ar nancarne te illi.
Mí imya lé, ve martane auressen Loto: mantelte, suncelte, mancanelte, vancelte, empannelte, carastanelte.
Mal i auresse yasse Lot ettúle Soromello, náre ar *ussar lantaner menello ar nancarne te illi.
Mí imya lé euva íre i Atanyondo nauva apantaina.
Enta auresse mauya i quenen tópasse coaryava, ye same armaryar i coasse, lá mene undu tultien tai, ar mi imya lé mauya i quenen i restasse lá nanwene i natinnar ca se.
Enyala Loto veri!
Aiquenen ye cesta hepe cuilerya nauvas vanwa, mal quén yen nas vanwa rehtuva sa.
Nyarin lenna: Mi enta lóme atta euvat er caimasse; er nauva talaina, mal i exe nauva hehtaina.
Euva nís atta múlala i imya *mulmanen; er nauva talaina, mal i exe nauva hehtaina.
Ar hanquentasse quentelte senna: “Masse, Heru?” Quentes téna: “Yasse i hroa ea, tasse i şorni ocomuvar.”
Tá nyarnes tien sestie pa i maure hyamiéva ar lá pustiéva neve,
quétala: “Ostosse enge námo ye ua sáme rucie Eruva ar ua sáme áya atanen.
Mal enge *verulóra nís sana ostosse ar túles i námonna, quétala: “Ánin anta failie cotumonyallo şanyesse!”
Mal ter lúme uas merne, mal epeta quentes insenna: “Ómu uan ruce Erullo hya same áya atanen,
en taluvan failie nís sinan, hya tuluvas ni-petien i cendelesse!”
Tá i Heru quente: “Hlara ya úfaila námo sina quéta!
Ma Eru ua tyaruva failie náve carna in icílies i yamir senna mi aure yo lóme, ómu samis cóle pa te?
Nyarin lenna, antuvas ten failie rato! Ananta, íre i Atanyondo tuluva, ma hiruvas i savie cemende?”
Yando quentes sestie sina innar náner tance pa véra failielta ar nonter i exi ve munta:
“Nér atta lendet ama mir i corda hyamien. Er náne Farisa ar er náne *tungwemo.
I Farisa tarne eressea ar hyamne sie: 'A Eru, antan lyen hantale pan uan ve hyane atani, pilur, úfailar, queni i racir vestale, hya ve *tungwemo sina.
*Avamatin lú atta i otsolasse, ar ilye nation yar ñetin antan quaista.'
Mal i *tungwemo yando ua merne orta henyat menelenna, mal palpanes ambosterya, quétala: 'A Eru, órava nisse, úcarindo!'
Quetin lenna: Nér sina lende marda nála faila lá i exe, an ilquen ye orta inse nauva nucumna, mal sé ye nucume inse nauva ortaina.”
Sí queni taller senna vinimoltar appieryan te, mal íre i hildor cenner sa, naityaneltet.
Mal Yésus yalle te ar quente: “Lava i hínain tule ninna, ar áva pusta te, an Eruo aranie taitin ná.
Násie quetin lenna: Aiquen ye ua came Eruo aranie ve hína, laume tuluva mir sa.”
Ar er i cánoron maquente senna: “Mane *peantar, mana caruvan náve aryon oira coiviéno?”
Yésus quente senna: “Manen ná i estal ní mane? *Úquen ná mane hequa er, Eru.
Istal i canwar: Áva race vestale. Áva nahta. Áva pile. Áva *vetta hurunen. Á anta alcar atarelyan ar amillelyan.”
Tá quentes: “Ilye nati sine ehépien et néşello.”
Hlárala ta, Yésus quente senna: “Ea en er nat ya penil. Vaca ilqua ya samil ar etsata i telpe penyain, ar samuval harma menelde; tá tula ar áni hilya.”
Íre hlasses ta, sámes tumna nyére, an anes ita lárea.
Yésus yente se ar quente: “Manen urda ná in samir telpe tule mir Eruo aranie!
É ulumpen autie ter nelmo assa ná *aşcárima lá tulie mir Eruo aranie lárea quenen.”
I hlasser ta quenter: “Tá man pole náve rehtaina?”
Eques: “I úcárimar atanin nar cárime Erun.”
Mal eque Péter: “Yé, ehehtielme vére natilmar ar ihílier lye.”
Quentes téna: “Násie quetin lenna, ea úner ye ehehtie coa hya veri hya hánor hya nostaru Eruo aranien
ye ua *nancamuva rimbe lúli amba lúme sinasse, ar i túlala randasse oira coivie.”
Tá talles i yunque oa véra nómenna ar quente téna: “Yé! Ménalve ama Yerusalemenna, ar ilye i nati técine lo i Erutercánor pa i Atanyondo nauvar telyaine.
An nauvas antaina ollo quellin i nórion ar perperuva yaiwe ar orme, ar mo piutuva senna,
ar apa riptie se nahtuvaltes, mal i neldea auresse ortuvas.”
Mal hanyanelte munta sine nation, mal quetie sina náne nurtaine tello, ar ualte sinte i nati quétine.
Sí, íre túles hare Yericonna, *cénelóra nér hamne ara i malle, íquala.
Pan hlasses i şanga léva, maquentes pa mana si tenge.
Nyarnelte sen: “Yésus Násaretello langea!”
Ar yámes, quétala: “Yésus Lavirion, órava nisse!”
Ar i lender opo Yésus naityaner se, náveryan quilda, mal ta ole ambe yámes: “Lavirion, órava nisse!”
Tá Yésus pustane ar canne i tulyumnelte i ner senna. Apa túles hare, Yésus quente:
“Mana meril i caruvan lyen?” Eques: “Heru, ánin lave came *céne!”
Ar eque Yésus: “Cama *céne; savielyanen acámiel *céne.”
Ar mi imya lú camnes *céne, ar hilyanes Yésus, antala alcar Erun. Ar i quanda lie, cénala ta, antane laitale Erun.
Ar íre túles mir i osto, lendes ter Yerico.
Ar enge tasse nér yeo esse náne Saccaio, ar anes héra *tungwemo ar lárea.
Ar néves cene Yésus, mal uas polle i şanyenen, an únes halla.
Ar nórala nóvo nómenna epe te, rentes mir vilwarinda alda, cenien se, an Yésus langumne sana nóme.
Ar íre túles tar, Yésus yente ama ar quente: “Sakkaio, tula undu lintiénen, an síra mauya nin lemya coalyasse!”
Ar túles lintave undu ar camne se mi alasse.
Ar íre illi cenner sa, anelte nurrula, quétala: “As nér ye ná úcarindo lendes minna matien.”
Mal Saccaio oronte ar quente i Herunna: “Yé, i perta armanyaron antuvan i penyain, ar qui maustanen nampen telpe ho aiquen, *nanantuvan lúr canta i nonwe!”
Tá Yésus quente senna: “Síra rehtie utúlie coa sinanna, an yando sé ná Avraham yondo.
An i Atanyondo túle cestien ar rehtien ya náne vanwa.”
Íre lastanelte sine natinnar yando quentes téna sestie, pan anes hare Yerúsalemenna, ar sannelte i Eruo aranie tanumne insa tasse ar tá.
Etta quentes: “Nér, aryon, lelyumne haira nórenna camien aranie ar epeta nanwenien.
Yalles móli quean ar antane tien minar quean ar canne tien: 'Á manca tainen tenna tuluvan.'
Mal nóreryo queni téver se ar mentaner ca se quelli icarpumner tien, quetien: 'Ualme mere nér sina ve aran or me.'
Ar túle íre nanwennes, apa camie i aranie, i cannes i yalumnelte senna móli sine in antanes i telpe, istieryan mana ñentelte mancalénen.
Tá i minya túle ar quente: “Heru, minalya ñente minar quean!”
Etta quentes senna: “Mai carna, mára mól! Pan anaiel voronda mí ampitya nat, sama túre or ostor quean.”
Ar i attea túle, quétala: “Minalya, heru, carne minar lempe!”
Quentes yando nér sinanna: 'Tá elye tura ostor lempe!'
Mal i exe túle, quétala: 'Heru, sisse ea minalya, ya ehépien caitala mi lanne.
An runcen lyello, pan nalye naraca nér; mapal ama ya ual panyane undu ar *cirihtal ya ual rende.'
Quentes senna: “Et véra antolyallo náman lye, olca mól! Sintel i nanye naraca nér ye mapa ya uan panyane undu ar *cirihta ya uan rende?
Tá manen ná i ual panya telpenya sarnosse? Tá, íre túlen, camumnenyes as napánina nonwe.'
Ar quentes innar tarner ara se: 'Á mapa i mina sello ar ása anta yen same i minar quean!'
Mal quentelte senna: 'Heru, samis minar quean!'
'Quetin lenna: Ilquenen ye same, amba nauva antaina, mal yen ua same, yando ya samis nauva mapaina oa.
Ente, ñottonyar sine i uar merne ni ve aranelta – áte tala sir ar áte nahta epe hendunyat!'”
Sie, apa quentes nati sine, lendes opo te Yerúsalemenna.
Ar íre túles hare Vet-Fahenna ar Vetanianna i orontesse estaina Oron *Milpieron, etementanes atta i hildoron,
quétala: “Mena mir i osto epe let! Íre tuliste tar, hiruvaste *pellope nútina, yesse *úquen atanion oi ahámie. Áse lehta ar áse tala!
Mal qui aiquen quete lenta: 'Mana castasta lehtien se?', aste quete: I Heru same maure seva.”
Ar i nánet etementaine hirnet se aqua ve quentes túna.
Mal íre lehtanette i *pellope, i haryaner se quenter túna: “Mana castasta lehtien i *pellope?”
Quentette: “I Heru same maure seva.”
Ar tulyanettes Yésunna, ar hantette collattar i *pellopenna ar panyanet Yésus sesse.
Mi levierya ompa queni pantaner collaltar i mallesse.
Íre túles hare i pendenna undu Orontello *Milpieron i quanda liyúme i hildoron *yestaner same alasse ar laita Eru pa ilye i túrie cardar yar cennelte,
quétala: “Aistana ná ye túla, i Aran, mí Héruo esse. Raine menelde, ar alcar i tarmenissen!”
Mal quelli i Farisaron i şangallo quenter senna: “*Peantar, á naitya hildolyar!”
Mal hanquentasse quentes: “Nyarin lenna: Qui queni sine náner quilde, i sarni yamumner!”
Ar íre túles hare ar cenne i osto, níeryar uller san,
ar quentes: “Nai elye sinte mi aure sina yar tulyar rainenna! Mal sí anaielte nurtaine hendulyalto.
An tuluvar aureli yassen ñottolyar peluvar lye caraxenen ar niruvar lyenna ilye tiellon,
ar hatuvalte lyé ar hínalyar talamenna, ar ualte lavuva ondon lemya ondosse lyesse, pan ualye sinte i lúme yasse anel céşina!”
Ar túlala mir i corda *yestanes et-hate i vancer,
quétala téna: “Anaie técina: 'Ar coanya nauva coa hyamiéva', mal elde carir sa felya piluron!”
Ar peantanes ilya auresse i cordasse. Mal i hére *airimor ar i parmagolmor ar i lieo mindar cestaner nancare se,
ananta ualte hirne mana carumnelte, an i quanda lie himyane se lastien.
Ar martane mi er i aurion, íre sé peantane i lien i cordasse ar carne i evandilyon sinwa, i túler senna i hére *airimor ar i amyárar
ar quenter senna: “Queta men mana túrenen cáral nati sine, ar man ánie lyen túre sina!”
Hanquentasse quentes téna: “Yando inye maquetuva lenna pa nat, ar alde hanquete ninna:
Yoháno *tumyale, ma anes et menello hya atanillon?”
Té caramper mici inte ar quenter: “Qui quetilve: Et menello, quetuvas: Tá manen ná i ualde sáve sesse?
Mal qui quetilve: Atanillon, i quanda lie vi-nahtuva sarninen, an savilte tancave i Yoháno náne Erutercáno.”
Sie quentelte i ualte sinte.
Ar eque Yésus: “Tá yando inya ua quetuva lenna mana túrenen carin nati sine.”
Ar *yestanes quete sestie sina i lienna: “Nér empanne tarwa liantassion ar láver *alamolin *yuhta sa telpen, ar lendes hyana nórenna anda lúmen.
Ar íre i lúme túle, mentanes mól i *alamonnar, antieltan sen i yáveo i tarwallo liantassion. Mal i *alamor, apa palpie se, mentaner se oa lusta.
Mal mentanes yando hyana mól. Yando sé mentanelte oa, apa palpie se ar nucumie se.
Ananta ata mentanes neldea; yando sé hantelte ettenna, apa harnie se.
Tá quente i heru ye sáme i tarwa lantassion: 'Mana caruvan? Mentuvan melda yondonya; sen samuvalte áya.'
Íre i *alamor cenner se, carampelte mici inte ar quenter: 'Ta ná i aryon! Alve nahta se, ar elme haryuvar masserya!'
Ar hanteltes et i tarwallo lantassion ar nacanter se. Tá mana caruva i heru ye same i tarwa?
Tuluvas ar metyuva tane *alamor ar antuva i tarwa exelin.” Íre hlasselte ta, quentelte: “Nai uas oi martuva!”
Mal ata yenteset ar quente: “Tá mana tea nat sina ya ná técina: 'I ondo ya i şamnor querner oa, sá anaie carna cas i vinco' – ?
Ilquen ye lanta tana ondonna nauva rácina. Aiquen yenna lantas, ascatuvas se.”
Ar i parmangolmor ar i hére *airimor cestaner panya máltat senna yana lúmesse, mal runcelte i liello. An hanyanelte i nyarnes sestie sina pa té.
Ar apa tirneltes harive mentanelte nelli i nuldave camner telpe lengien ve qui anelte faile, nevien mapa se questasse, antieltan se oa in turner ar i mir i nórecáno túre.
Ar maquentelte senna: “*Peantar, istalme i quetil ar peantal ve nanwa ná, ar ua valda lyen man quén ná, mal peantal Eruo malle i nanwiénen.
Ma lertalve paitya *tungwe i Ingaranen hya lá?”
Mal túnes curulta ar quente téna:
“Ánin tana lenár. Mano emma ar tecie samis?” Quentelte: “I ingarano.”
Ar quentes téna; “Alde *nananta, tá, i Ingarano nati i Ingaranen, ar Eruo nati Erun.”
Ar ua ence tien mapa se mi quetie sina epe i lie, mal quentelte munta, elmendasse hanquentaryanen.
Mal quelli i Farisaron, i quetir i ua ea *enortale, túler ar maquenter senna:
“*Peantar, Móses etécie men: 'Qui nero háno quale ú híno, apa veryanes nís, mauya hánoryan verya i nissenna ar orta erde hánoryan.'
Mal enger hánor otso, ar i minya veryane nissenna, ono qualle ú híno.
Yando i attea
ar i neldea veryaner senna, mí imya lé ilye i otso ú híno hilien te, ar quallelte.
Mí metta yando i nís qualle.
Mano veri tá nauvas i *enortalesse? An anes veri ilye i otson.”
Yésus quente téna: “Randa sino hínar veryar ar nar vertaine,
mal i nar nótine valdie enta randan ar i *enortalen qualinillon uar verya hya nar vertaine.
Ente, ualte pole ambe quale, an nalte ve valar, pan nalte Eruo híni návenen i *enortaleo híni.
Mal yando Móses tanne qualini nar ortaine, i nyarnasse i neceltusso, íre estas i Héru Aino Avrahámo ar Aino Ísaco ar Aino Yácovo.
Uas qualinion Aino, mal coivearon, an illi mici te nar coivie sen.”
Hanquentasse quelli i parmangolmoron quenter: “*Peantar, carampel mai!”
An ualte ambe varyane maquete senna erya maquetie.
Mal quentes téna: “Manen ná i quetilte i ná i Hristo Laviro yondo?
An Lavir immo quete i parmasse airilírion: I Héru quente herunyanna: Hara ara formanya,
tenna panyuvan ñottolyar nu talulyat.
Sie Lavir esta se heru. Tá manen náse yondorya?”
Íre i quanda lie lastane, quentes hildoryannar:
“Tira inde i parmangolmoin i merir vanta mi larmar ar melir *suilier i mancanómessen ar i minya sondar i *yomencoassen ar i minde nómi i ahtumattissen,
ar i ammatir i coar *verulóraiva ar hyamir ande hyamier náven cénine lo exi. Té camuvar ambe lunga namie.”
Sí íre yentes cennes i lárear panyea annaltar i harwessen.
Tá cennes penya *verulóra nís ye minahante pitye urustamitta atta,
ar eques: “Nyarin len násie: Penya *verulóra nís sina antane amba lá te illi.
An illi sine antaner annar et úveltallo, mal nís sina antane i quanda laulesta ya sámes.”
Ar íre quelli caramper pa i corda, i sámes írime ondor ar náne netyaina annalínen, quentes:
“Nati sine yar yétalde – aureli tuluvar yassen ondo ua lemyuva ondosse ya ua nauva hátina undu.”
Ar maquentelte senna: “*Peantar, mana i lú yasse nati sine euvar, ar mana i tanwa i nati sine rato martuvar?”
Ar eques: “Cima i ualde nauva tyárine ranya! An rimbali tuluvar essenyasse, quétala: Inye sé!, ar: I lúme hare ná. Áva lelya ca te!
Ente, íre hlarilde pa ohtali ar amortiéli, áva na ruhtaine! An mauya i nati sine tuluvar minyave, mal i metta ua tule mí imya lúme.”
Tá quentes téna: “Nóre ortuva nórenna ar aranie aranienna,
ar euvar *cempaliéli, ar mi nóme apa nóme quolúviéli ar saiceléli, ar euvar rúcime natali cénine, ar menello túre tanwali.
Mal nó ilye nati sine queni panyuvar máltat lesse ar roituvar le, antala le olla i *yomencoain ar i mandoin, íre nalde túcine epe arani ar nórecánor pa essenya.
Ta nauva len ecie *vettien.
Etta na tance endaldasse i ualde caruva panor nóvo pa manen quetuvalde inden,
an inye antuva len anto ar sailie yanna ilye cotumaldar uar poluva tare hya quete.
Ente, nauvalde antaine olla yando lo nostaru ar lo hánor ar lo nosselda ar lo meldor, ar nahtuvalte quelli mici le,
ar nauvalde tévine lo illi essenyanen.
Ananta fine careldasse laume nauva nancarna.
Voronweldanen rehtuvalde coivieldar.
Ente, íre cenuvalde Yerúsalem pélina lo hosseli, á ista i nancarierya utúlie hare.
Tá mauya in ear Yúreasse uşe i orontinnar, ar mauya in ear i osto endesse auta, ar in ear i ménassen lá lelya mina sa,
an ente auri nar auri ahtariéva, carien nanwe ilye nati i anaier técine.
Horro in *lapsarwe nissin ar in *tyetir vinimor ente auressen! An tuluva túra maure i nóresse ar orme lie sinanna,
ar lantuvalte i macilden ar nauvar tulyaine oa mir ilye i nóri, ar Yerúsalem nauva *vattaina nu talu lo ilye i nóri, tenna i nórion lúmi nauvar telyaine.
Ente, euvar tanwali mi Anar ar Işil ar eleni, ar cemende quárele mici nóri yar uar cene uşwe, castanen ramo i earo ar amortieryo,
íre atani nauvar hlaiwe caurenen, apacénala i nati túlala ambarenna. An menelo túri nauvar páline.
Ar tá cenuvalte i Atanyondo túla fanyasse arwa túreo ar túra alcaro.
Mal íre nati sine *yestar marta, tara halle ar á orta carelda, an etelehtielda túla hare.
Ar quentes téna sestie: “Á tunta i *relyávalda ar ilye i hyane aldar:
Yando íre samilte tuimar, istalde inden cendiénen i sí i laire hare ná.
Sie yando istuvalde, íre cenuvalde nati sine martea, i Eruo aranie hare ná.
Násie quetin lenna: *Nónare sina ua autuva nó ilye nati martuvar.
Menel cemenye autuvat, mal ninye quettar uar oi autuva.
Mal cima inde, pustien endalda návello lunga *accamatiénen ar *accasuciénen ar quárelénen coireo, ar rincanen enta aure tuluva lenna
ve remma. An tuluvas illinnar i marir palúresse i quanda cemeno.
Mal na coive, illume hyámala i poluvalde uşe ilye nati sine yar tuluvar, ar tare epe i Atanyondo.”
I auressen anes i cordasse, peantala, mal lómisse etelendes ar marne i orontesse estaina Oron *Milpieron,
ar i quanda lie túle arinyave senna i cordasse hlarien se.
Sí i aşar *Alapulúne Massaron, ya ná estaina Lahtie, náne hare.
Ar i hére *airimor ar i parmangolmor cestaner manen pollelte nancaritas, pan runcelte i liello.
Mal Sátan lende mir Yúras, i quén estaina Iscariot, ye náne nótina imíca i yunque,
ar lendes oa ar quente as i hére *airimor ar i cordahestor pa manen antumneses olla tien.
Tá anelte valime ar quenter i anelte mérala anta sen telpe.
Ar quentes ná yan intyanelte ar cestane mára lú antien se olla íre ua enge şanga aselte.
Sí túle i aure *alapulúne massaron, yasse mauyane nahta i eule lahtiéva,
ar mentanes Péter ar Yoháno, quétala: “Mena ar áven manwa i lahtie, matielvan.”
Quentette senna: “Masse meril i caruvammes manwa?”
Quentes túna: “Yé, íre tuliste mir i osto, nér ye cóla cemina vene veluva let. Áse hilya mir i coa mir ya menuvas.
Ar atte quete yenna same i coa: “I *Peantar quete lyenna: Masse ea i nóme yassen polin mate i lahtie as hildonyar?”
Ar tana nér tanuva lent hoa *sorostarwa oromar. Á manwa tasse!”
Ar oantette ar hirne i nati ve quentes túna, ar manwanette i lahtien.
Ar íre i lúme túle, Yésus caine ara i sarno, ar i aposteli óse.
Ar quentes téna: “Ita emérien mate lahtie sina aselde nó perperin,
an quetin lenna: Uan matuva sa ata nó nauvas telyaina Eruo araniesse.”
Ar cámala yulma antanes hantale ar quente: “Á mapa si ar ása menta quenello quenenna mici le.
An quetin lenna: Ho sí uan sucuva ata i yávello i liantasseo nó Eruo aranie tuluva.”
Ente, nampes massa, antane hantale, rance sa ar quente: “Si ná hroanya, ya nauva antaina rá len. Cara si enyalien ni.”
Ar i yulma mí imya lé apa mantelte i ahtumat, sé quétala: “Yulma sina ná i vinya vére hroanyanen, ya nauva etulyaina rá len.
Mal yé, *vartonyo má ná asinye i sarnosse!
An i Atanyondo autea, ve náse martyaina; ananta horro i quenen ter ye náse antaina olla!”
Etta *yestanelte carpa mici inte pa man mici te náne ye carumne nat sina.
Mal enge yando costie imbi te pa man mici te *şéne náve i antúra.
Ono quentes téna: “I nórion arani nar herultar, ar i samir túre or te nar estaine *maicarindor.
Mal elde áva na sie. Mauya yen ná i antúra mici le náve ve i amnessa, ar yen cane, náve ve núro.
An man i ambe túra ná, ye caita ara i sarno hya i núro? Ma uas ye caita ara i sarno? Ono mici le nanye ve i núro.
Mal elde nar i elémier asinye tyastienyassen,
ar carin vére aselde, ve Atarinya acárie asinye vére, pa aranie,
matieldan ar sucieldan ara sarnonya aranienyasse, ar harieldan mahalmassen namien i nossi yunque Israélo.”
Símon, Símon, yé! Sátan acánie silta le ve ore!
Mal inye ahyámie pa elye, i savielya ua nauva loica, ar íre mi lú enutúliel, á tala antoryame hánolyain.”
Tá quentes senna: “Heru, nanye manwa menien aselye mir mando ar qualme véla!”
Mal eques: “Quetin lyenna, Péter: Tocot ua lamyuva síra nó equétiel nel i ual ista ni.”
Ar quentes téna: “Íre le-mentanen ú *pocolleo ar matsocolco ar hyapato, lau pennelte erya nat?” Quentelte: “Lánelme.”
Tá quentes téna: “Mal sí mauya yen same *pocolle tala sa, mi imya lé yando matsocolca, ar mauya yen pene macil vace collarya ar ñete macil i telpen.
An quetin len i mauya i nat sina técina pa ni nauva carna nanwa: 'Ar anes nótina mici şanyelórali.' An ya ape ni nauva telyaina.”
Tá quentelte: “Heru, ela, sisse eat macil atta.” Quentes téna: “Tú faryat.”
Apa etelendes mennes, ve haimerya né, i Orontenna *Milpieron, ar hildoryar se-hilyaner.
Túlienwa i nómenna quentes téna: “Hyama, pustien inde tuliello úşahtienna!”
Ar sé lende oa telo, *os hatie sarneva, ar lantanes occaryanta ar hyamne,
quétala: “Atar, qui meril, á mapa yulma sina nillo! Ananta, nai indómelya martuva, lá ninya.”
Tá vala menello tannexe sen ar talle sen antoryame.
Mal túlala mir anquale hyamnes en ambe urdave, ar cendeleya náne linque yanen náne ve limbar serceva, lantala i talamenna.
Ar orontes hyamiello, lende i hildonnar ar hirne te lorne nyérenen,
ar quentes téna: “Mana castalda náven lorne? Á orta ar hyama, pustien inde tuliello úşahtienna!”
Íre ena carampes, yé! şanga, ar i nér estaina Yúras, quén i yunqueo, lende opo te. Túles Yésunna miquien se.
Mal Yésus quente senna: “Yúras, ma *vartal i Atanyondo miquenen?”
Íre i queni óse cenner ya martumne, quentelte: “Heru, ma petuvalme i macilden?”
Ar quén mici te pente i héra *airimo mól ar aucirne forya hlarya.
Mal hanquentasse Yésus quente: “Lava yando si marta!” Ar appanes i hlas ar nestane se.
Yésus tá quente i hére *airimonnar ar i cordahestannar ar amyárannar i náner túlienwe tar mapien se: “Ma etutúlielde arwa macillion ar rundalion, ve arponna?
Aure apa aure engen aselde i cordasse, ar ualde panyane máldat nisse. Mal si ná *lenya lúme, ar mornie ture.”
Tá nampeltes ar tulyane se oa ar talle se mir i héra *airimo coa, mal Péter hilyane haiya.
Íre nartanelte náre endesse i paco ar hamner undu uo, Péter hamne mici te.
Mal enge *núre ye cenne se ara i ruine, ar apa tirie Péter quentes: “Yando nér sina náne aselte.”
Mal laquentes sa, quétala: “Uan ista se, nís!”
Apa şinta lúme hyana quén, cénala se, quente: “Yando elye er mici té!” Mal Péter quente: “Nér, uan!”
Ar apa *os lúme náne vanwa, hyana nér quente tancave: “É yando nér sina náne óse! Náse yando Alileallo!”
Mal Péter quente: “Nér, uan ista ya quétal!” Ar mi yana lú, íre en quequentes, tocot lamyane.
Ar i Heru querne inse ar yente Péterenna, ar Péter enyalle i Heruo quetie íre quentes senna: “Nó tocot lamya síra, ni-laquetuval nel.”
Ar lendes ettenna ar talante níressen.
Sí i neri i sámer se mandosse lenganer senna yaiwenen, pétala se.
Ar apa tompeltes maquentelte: “Queta ve *Erutercáno! Man náne ye pente lye?”
Ar rimbe hyane naiquetiéli quentelte senna.
Íre i aure túle, i ocombe amyáraron i lieo, i hére *airimor ar i parmangolmor véla, ocomner. Ar tunceltes epe ocombelta, quétala:
“Qui elye ná i Hristo, ámen nyare!” Mal quentes téna: “Yando qui nyarnen len, laume savumneldes.
Ente, qui maquenten lenna, laume antumnelde nin hanquenta.
Mal ho sí i Atanyondo haruva ara Eruo forma túreva.”
Tá illi mici te quenter: “Ma elye, tá, ná Eruo yondo?”
Quentes téna: “Elde quetir i nanye.”
Quentelte: “Mana amba maure samilve astaroiva? An elve inwe ahlárier sa véra antoryallo!”
Ar apa quanda rimbelta oronte, tulyaneltes Pilátonna.
*Yestanelte quete pa intyaine ongweryar, quétala: “Nér sina ihírielme nuquéra nórelma ar váquéta i aiquen paitya *tungwi i Ingaranen, ar quetis pa inse i náse Hristo, aran.”
Mal Piláto maquente senna: “Ma elye aran Yúraron ná?” Hanquentasse eques: “Elye quete sa.”
Mal Piláto quente i hére *airimonnar ar i şangannar: “Uan hire ongwe atan sinasse.”
Mal té ninder se, quétala: “Valtas i lie, peantala ter i quanda Yúrea, ar apa *yestie Aileasse utúlies yando sir!”
Ono Piláto, íre hlasses ta, maquente qui i atan náne Alileallo.
Ar apa sintes i é anes Herolo araniello, se-mentanes Herolenna, pan yando sé náne Yerúsalemesse yane auressen.
Mal Herol, íre cennes Yésus, sáme túra alasse, an nóvo andave mernes velitas, apa hlarie pa se, ar sámes i estel i cenumnes tanwa martea sénen.
Maquentes senna rimbe maquetier, ono sé hanquente sen munta.
Mal i hére *airimor ar i parmangolmor oronter ar caramper pa ongweli yar quentelte carnes.
Tá Herol as cundoryar sanner i anes munta, ar yaiwesse tompeltes calima larmanen ar *nanwentaner se Pilátonna.
Yana auresse Herol ar Piláto *yestaner nilme, quén as i exe, an nó si anette cottoli.
Piláto tá comyane i hére *airimor ar i lie
ar quente téna: “Tallelde nér sina ninna ve quén ye valta i lie, ar yé! cendanenyes epe le, mal uan hirne sesse cáma pa i ongwi yar quételde acáries.
É yando Herol láne, an se-*nanwentanes venna, ar yé! munta valda qualmeo anaie carna lo se.
Etta paimestuvanyes; tá lehtuvanyes.”
Mal quanda liyúmelta yáme: “Á mapa oa nér sina, mal ámen lehta Varavas!”
Sé náne nér hátina mir mando castanen amortiéno ya náne martienwa i ostosse, ar nahtiéno.
Ata Piláto yalle te, pan mernes lehta Yésus.
Tá yámelte, quétala: “Áse tarwesta, áse tarwesta!”
Mí neldea lú quentes téna: “Tá mana ulco nér sina acárie? Munta valda qualmen hirnen sesse. Apa antie sen paimesta etta lehtuvanyes.”
Mal nindeltes hoe rambelínen, cánala tarwestierya, ar rambeltar *orturner.
Ar Piláto namne i ñetumnelte ya mernelte.
Lehtanes tien i nér hátina mir mando amortiénen ar nahtiénen, ye arcanelte, mal Yésus antanes olla nirmeltan.
Ar íre mentaneltes oa, nampelte quén estaina Símon Cirénello ye túle i restallon, ar mauyanelte sen cole tarwerya.
Mal hilyane se hoa liyúme i liello, ar nisseli i pentexer nyérenen ar náner yaimie sen.
Yésus querne inse téna ar quente: “Yerúsalémo yeldi, áva na yaimie nin! Na yaimie elden ar hínaldain, an yé!
auri túlar yassen queni quetuvar: 'Valime nar i nissi ú yáveo, ar i súmar yar uar nostane ar i tyetsi yar uar *tyente!'
Tá *yestuvalte quete i orontinnar: 'Á lanta menna!', ar i ambonnar: 'Áme tope!'
An qui carilte nati sine íre i alda ná níte, mana martuva íre ehesties?”
Mal hyana nér atta, ulcarindor, nánet yando tulyaine tar náven nance óse.
Ar íre túlelte i nómenna estaina *Caraxo, tarwestanelte sé ar i ulcarindor, er foryaryasse ar er hyaryaryasse.
Mal Yésus quente: “Atar, áten apsene, an ualte ista mana cáralte.” Ente, satien larmaryar hantelte *şanwali.
Ar i queni tirner. Mal i turi caramper yaiwenen, quétala: “Hyanali erehties; lava sen rehta inse, qui nér sina ná Eruo Hristo, i cílina.”
Yando i ohtari lenganer senna yaiwenen. Túlelte hare ar antaner se sára limpenen
ar quenter: “Qui nalye Yúraron aran, á rehta imle!”
Yando enge tecie or se: “Si ná aran Yúraron.”
Mal er i lingala úcarindoron naiquente senna: “Ma elye ua i Hristo? Á rehta imle ar met!”
Hanquentasse i exe naityane se ar quente: “Ma ual ruce Erullo, sí íre nalye i imya námiesse?
Ar engwe failave sie, an cámangwe yan nangwe valde i natinen yar carnengwe, mal nér sina acárie munta ulca.”
Ar quentes senna: “Yésus, áni enyale íre tulil aranielyasse!”
Ar quentes senna: “Násie quetin lyenna: Síra euval asinye mi Paralis.”
Sí i lúme náne *os i enquea, ar mornie lantane i quanda cemenna tenna i nertea lúme,
pan i áre loitane; tá i fanwa i cordasse náne narcaina undu i ende.
Ar yámala taura ómanen, Yésus quente: “Atar, mir mátyat antan olla fairenya!” Apa quetie ta effirnes.
Íre i *tuxantur cenne ya martane, antanes alcar Erun ar quente: “Nanwave nér sina náne faila!”
Ar ilye i şangar yar náner ocómienye cenie sinan, íre cennelte yar martaner, lender oa pétala ambostelta,
Mal i sinter Yésus tarner haiya, yando i nissi i túler óse Alileallo, íre cennelte nati sine.
Ar túle nér yeo esse náne Yósef, quén i Combeo, mane ar faila nér.
Sé ua quente ná panoltanna hya cardaltanna. Túles Arimaşeallo, osto i Yúraron, ye yente ompa Eruo aranienna.
Apa menie minna Pilátonna arcanes Yésuo hroa.
Ar nampeses undu ar vaitane sa *páşenen, ar se-panyanes hrótanoirisse i ondosse, yasse *úquen náne caitienwa nóvo.
Si náne i aure Manwiéva, ar i *sendareo yesta náne hare.
Mal i nissi i túler óse et Alileallo hilyaner ar cenner i noire ar i nóme yasse hroarya náne panyaina.
Tá nanwennelte manwien *tyávelasseli ar níşime milloli. Mal sendelte i *sendaresse, i axannen.
I minya auresse i otsolasse lendelte ita arinyave i noirinna, cólala i *tyávelassi manwaine lo te.
Mal hirnelte i ondo *peltaina oa i noirillo,
ar íre lendelte minna, ualte hire i Heru Yésus Hristo hroa.
Íre anelte elmendasse pa si, yé! nér atta arwe alcarinque larmar tarnet ara te.
Íre i nissi náner ruhtaine ar querner cendelelta i talamenna, i neri quenter téna: “Manen ná i cestealde i Coirea mici qualini?
Uas sisse, mal anaies ortaina! Enyala manen carampes lenna íre en anes Alileasse,
quétala i mauya i Atanyondon náve antaina olla mannar atanion úcareva ar náve tarwestaina ar orta i neldea auresse.”
Ar enyallelte quetieryar,
ar nanwennelte i noirillo ar nyarner ilye nati sine i minquen ar ilye i exin.
Anelte María Mahtaléne, ar Yoanna, ar María amil Yácovo. Yando i hyane nissi aselte nyarner i apostelin nati sine.
Mal quetier sine şéner tien ve *aucie, ar ualte sáve ya i nissi quenter.
Mal Péter oronte ar norne i noirinna, ar cúnala ompa cennes i vaimar erinque. Tá lendes oa, elmendasse pa mana náne martienwa.
Mal yé! i imya auresse atta mici te nánet tiettasse mastonna estaina Emmaus, lár atta ar canasta Yerúsalemello,
ar quequentette quén as i exe pa ilye nati sine yar náner martienwe.
Sí íre quequentette ar quaptanette quettali, yando Yésus immo túle hare ar vantane asette,
mal henduttat nánet hépine istiello se.
Quentes túna: “Mana quettar sine yar quapteaste íre vanteaste?” Ar tarnette arwe nyére mi cendeletta.
Hanquentasse i quén estaina Cleopas quente senna: “Ma nalye i erya quén márala Yerúsalemesse ye ua ista i nati yar amartier sasse mi auri sine?”
Ar quentes túna: “Mana?” Quentette senna: “I nattor yar ape Yésus Násaretello, ye náne Erutercáno taura mi carda ar quetta epe Eru ar i quanda lie
– ar manen hére *airimonyar antaner se olla námien qualmeva ar tarwestaner se.
Mal sámelme i estel i nér sina náne ye etelehtumne Israel. Ono ara ilye nati sine, síra ná i neldea aure apa si martane.
Ente, nisseli mici me acaptier me, an apa menie arinyave i noirinna
ú hiriéno hroarya túlelte quétala i cennelte maur valalíva, i quenter i náse coirea.
Yando, quelli mici me lender oa i noirinna, ar hirnelte i enge sie, aqua ve i nissi quenter, mal ualte cene sé.”
Tá quentes túna: “A úhandar, lence endeo savien ilye i nati yar i Erutercánor quenter!
Ma ua mauyane i Hriston perpere nati sine ar mene mir alcarerya?”
Ar *yestala ho Móses, ar ilye i Erutercánor, tyarnes tu hanya i nati yar apir inse ilye i Tehtelessen.
Ar túlette i mastonna ya náne lendatto met, ar sé carne ve qui lelyumnes ompa talo.
Mal mauyanette sen, quétala: “Á lemya asemme, an i şinye hare ná, ar i aure rato nauva vanwa.”
Ar íre caines asette ara i sarno nampes i massa, aistane sa, rance sa ar antane sa tun.
Ar mi yana lú henduttat nánet aqua pantaine, ar sintettes, mal vánes tullo.
Ar quentette, quén i exenna: ”Ma endangwa ua uryane íre quequentes asengwe i mallesse, íre pantanes i Tehteler vent?”
Ar mi yana imya lúme orontette ar nanwennette Yerúsalemenna, ar hirnette i yunque ar i náner ocómienwe aselte,
i quenter: “I Heru é anaie ortaina ar atánie inse Símonden!”
Ar tú nyarnet pa yar náner martienwe i mallesse ar manen anes carna sinwa tun raciénen i massa.
Íre en quentelte pa nati sine, Yésus immo tarne endeltasse ar quente téna: “Nai samuvalde raine!”
Mal pan anelte ruhtaine ar sámer caure, sannelte i cennelte faire.
Etta quentes téna: “Mana castalda rucieldan, ar manen ná i ortear sanweli úsaviéva endaldasse?
Cena mányat ar talunyat, i inye ná; áni appa ar cena, an faire ua same hráve yo axor ve cénalde i inye same.”
Ar quétala si tannes tien máryat ar talyat.
Mal íre en ualte sáve, i alassenen ar i elmendanen, quentes téna: “Ma sámalde sisse nat matien?”
Ar antanelte sen ranta *aptaina lingwiva,
ar nampeses ar mante sa epe hendultat.
Ar quentes téna: ”Nati sine nar quettanyar yar quenten lenna íre en anen aselde, i ilye i nati técine Móseo Şanyesse ar mí Erutercánor ar mi Airelíri nauvar carne nanwe.”
Tá pantanes sámalta, lávala tien hanya i Tehteler,
ar quentes téna: “Sie ná técina, i perperumne i Hristo, ar i ortumnes i neldea auresse,
ar i esseryanen mo *nyardumne inwis úcariva ilye i nóressen. *Yestala Yerúsalemello
elde nauvar *vettoli pa nati sine.
Ar yé! inye menta lenna ta pa ya Atarinya antane vanda. Mal elde hara i ostosse tenna nauvalde tópine túrenen et tarmenello.”
Epeta tulyanes te tenna Vetánia, ar ortanes máryat ar aistane te.
Ar íre aistanes te, anes mapaina oa tello ar cólina ama mir menel.
Ar lantanelte undu sen ar nanwenner Yérusalemenna arwe túra alasseo.
Ar anelte illume i cordasse, aistala Eru.
I yestasse enge i Quetta, ar i Quetta enge as Eru, ar i Quetta né Eru.
Sé enge i yestasse as Eru.
Ilqua né ontaina sénen, ar hequa sénen erya nat úme ontaina. Ya né ontaina
sénen coivie né, ar i coivie náne Atanion cala.
Ar i cala calta i morniesse, mal i mornie lá *orutúrie sa.
Oronte nér mentaina lo Eru; esserya né Yoháno.
Sé túle *vettien, i *vettumnes pa i cala, i savumner sénen ilye atani.
Lá anes cala sina, mal *vettumnes pa cala sina.
I nanwa cala, caltala ilye atanin, né túlala mir i mar.
Enges i mardesse, ar i mar né ontaina sénen, mal i mar ua sinte se.
Túles i véranna, mal véraryar uar camne se.
Mal illi i é se-camner, tien antanes hére náven hínali Eruo, an sámelte savie esseryasse;
ar anelte nóne, lá sercenen hya hrávenen hya nero şelmanen, mal Erunen.
Ar i Quetta ahyane mir hráve ar marne imíca me, ar cennelme alcarerya, taite alcar ya *ernóna yondo same ataryallo; ar anes quanta Erulisseo ar nanwiéno.
Yoháno *vettane pa se, é etyámes: “Nér sina ná i quén pa ye quenten: Ye túle ca ni ná sí opo ni, pan enges nó ni.”
An camnelme illi et quantieryallo, lisse or lisse.
An i Şanye náne antaina ter Móses, mal i lisse ar i nanwie túler ter Yésus Hristo.
Eru *úquen oi ecénie; i *ernóna Yondo ye ea i Ataro súmasse acárie se sinwa.
Si ná Yoháno *vette íre i Yúrar mentaner *airimóli ar Levindeli Yerúsalemello ceşien se: “Man nalye?”
Ar *etequentanes ar ua lalane, mal *etequentane: “Uan i Hristo.”
Ar cenşeltes: “Tá mana? Ma nalye Elía?” Ar eques: “Uan.” “Ma nalye i Erutercáno?” Ar hanquentes: “Lá!”
Etta quentelte senna: “Man nalye? Lava men same hanquenta in mentaner me. Mana quetil pa immo?”
Eques: “Nanye óma yámala i erumasse: Cara i Héruo malle téra! – tambe Yesaia i Erutercáno quente.”
I mentainar náner i Farisaron.
Ar cenşelte ar quenter senna: “Tá mana i casta yanen *tumyal, qui umilye i Hristo hya Elía hya i Erutercáno?”
Yoháno hanquente ar eque: “Inye *tumya nennen; endeldasse quén tára ye lé uar ista,
ye túla ca ni, mal inye ui valda lehtien i latta hyapatyo.”
Nati sine martaner Vetaniasse Yordan pella, yasse *tumyane Yoháno.
I hilyala auresse cennes Yésus túla senna, ar eques: “Ela Eruo Eule ye mapa oa i mardo úcare!
Sé ná i quén pa ye quenten: Yé! Túla nér ye sí ná opo ni, an nó ni enges.
Inye ua sinte se, mal túlen *tumyala nennen i sé umne apantaina Israélen.”
Yoháno yando *vettane, quétala: “Cennen i Faire túla undu et menello ve cucua, ar lemyanes sesse.
Ar inye ua sinte se, mal ye ni-mentane tumien nennen quente ninna: I quén yenna cenuval i Faire túla undu ve cucua, sé ná ye *tumya Aire Feanen.
Ar inye ecénie sa, ar *evettien i sé ná i Eruion.”
I hilyala auresse ata tarne Yoháno as atta hildoryaron,
ar cénala Yésus vantea eques: “Ela Eruo Eule!”
Ar i hildo atta hlasset se quéta, ar hilyanette Yésus.
Mal Yésus querne immo ar cenne tú hilyea, ar quentes túna: “Mana cesteaste?” Tú quentet senna: “Ravi” – ya tea *Peantar – “masse máral?”
Eques túna: “Tula ar cena!” Etta túlette ar cennet i nóme yasse marnes, ar lemyanette óse aure yanasse. Náne *os i quainea lúme.
Andréas, hánorya Símon Péter, náne quén i hildo atto *yet hlasset ya quente Yoháno ar hilyanet Yésus.
Sé hirne minyave véra hánorya, Símon, ar eque senna: “Ihíriemme Messías” – ya tea Hristo.
Se-tulyanes Yésunna. Íre Yésus se-cenne eques: “Elye ná Símon Yohannion. Elye nauva estaina Céfas” – ya tea Péter .
I hilyala auresse mernes mene Alileanna. Yésus hirne Filip ar quente senna: “Áni hilya!”
Filip náne Vetsairallo, i osto Andreas ar Pétero.
Filip hirne Natanael ar quente senna: “Ihírielme i quén pa ye Móses i Şanyesse ar i Erutercánor tencer: Yésus Yosefion Nasaretello.”
Mal eque senna Natanael: “Ma *aiqua mára pole tule Nasaretello?”
Yésus cenne Natanael túla senna ar quente pa se: “Ela nanwa Israelinde, yesse lá ea *vartie!”
Eque senna Natanael: “Manen elye ista ní?” Yésus hanquente ar eque: “Nó Filip yalde lye, íre engel nu i *relyávalda, lye-cennen.”
Natanael hanquente senna: “Ravi, elye ná i Eruion, elye ná i Aran Israélo!”
Yésus hanquente ar eque: “Pan anyárien lyen i cennen lye nu i *relyávalda savil? Cenuval natali túre lá ta.”
Ar eques téna: “Násie, násie quetin lenna: Cenuvalde menel latyaina ar Eruo valar ména amba ar undu i Atanyondosse.”
I neldea auresse enge veryanwe Cánasse Alileo, ar Yésuo amil náne tasse.
Yando Yésus ar hildoryar náner nahamne i veryanwenna.
Íre ualte sáme amba limpe Yésuo amil quente senna: “Pénalte limpe.”
Mal eque senna Yésus: “Nís, mana samin asetye? Lúmenya en ua utúlie.”
Amillerya quente innar *veuyaner: “Á care *aiqua ya quetis lenna!”
Tarner tasse calpar enque, ve i şanyer sovalleo Yúraron; ilya calpa polde mapa lestar atta hya nelde.
Eque téna Yésus: “Quata i calpar nennen!” Ar quanteltet penquante.
Ar eques téna: “Á calpa sallo ar ása mapa i *merenturenna!” Sie nampeltes.
Mal íre i *merentur tyáve i nén, ya náne vistaina mir limpe, mal ua sinte yallo túles – ómu i *veuyaner sinter – i *merentur yalde i ender
ar eque senna: “Ilya hyana nér *etepanya minyave i mára limpe, ar íre queni nar *limpunque, i faica. Elye asátie i mára limpe tenna sí.”
Yésus carne si Cánasse Alileo ve i yesta tannaryaron ar apantane alcarerya, ar hildoryar sáver sesse.
Epeta sé ar amillerya ar hánoryar ar hildoryar lender undu Capernaumenna, mal ualte lemyane tasse ter rimbe aureli.
Sí Yúraron Lahtie né hare, ar Yésus lende amba Yerúsalemenna.
Ar hirnes i cordasse i vancer mundor ar mámar ar cucuar ar i *quaptandor hammaltassen.
Apa carie *falampa *rappalíva, hanteset illi etsenna as i mámar ar mundor; *etulyanes i tyelpemittar i *quaptandoiva ar nuquerne i sarnor.
Ar innar vancer cucuar eques: “Á mapa nati sine oa sio! Áva care i coa Atarinyava coa mancaleo!”
Hildoryar enyalder i ná técina: “I uryala felme coalyan matuva ni.”
Tá i Yúrar quenter senna: “Mana i tanna ya tanuval men, pan caril nati sine?”
Yésus hanquente ar eque: “Nancara corda sina, ar auressen nelde enortuvanyes.”
Etta i Yúrar quenter: “Corda sina náne carastaina ter loar enque *canaquean, ar elye enortuva sá auressen nelde?”
Mal sé carampe pa i corda hroaryava.
Sie, apa anes ortaina et qualinillon, hildoryar enyalder i quentes si, ar sávelte ya quete i Tehtele ar i quetta ya Yésus quente.
Mal íre enges Yerúsalemesse i Lahtiesse, i aşaresse, rimbali sáver esseryasse, pan cennelte i tannar yar carnes.
Ono Yésus immo ua pantane endarya tien, pan sinteset illi, ar pan uas sáme maure i aiquen *vettumne pa atan, an sé sinte ya enge i atande.
Enge nér i Farisaron; esserya náne Nicorémus, turco Yúraron.
Nér sina túle senna i lómisse ar eque senna: “Ravi, istalme i elye ná *peantar túlienwa Erullo, an ea *úquen ye pole care tannar sine qui Eru ui óse.”
Yésus hanquente ar eque senna: “Násie, násie quetin lyenna, qui aiquen ui *ennóna, uas pole cene Eruo aranie.”
Eque senna Nicorémus: “Manen quén pole náve nóna íre náse yára? Ma polis nanwene amilleryo mónanna náven nóna ata?”
Yésus hanquente: “Násie, násie quetin lyenna, qui aiquen ui nóna nennen ar fairenen, uas pole tule mir Eruo aranie.
Ya ná nóna hrávenen hráve ná, ar ya ná nóna fairenen faire ná.
Áva fele elmenda pan quenten lyenna: Mauya len náve ennóna.
I súre ná hlápula yasse meris, ar hlaril lammarya, mal ual ista yallo tulis hya yanna lelyas. Sie ná ilquen ye ná nóna i fairenen.”
Nicorémus hanquente ar eque senna: “Manen nati sine polir marta?”
Yésus hanquente ar eque senna: “Ma elye ná *peantar Israélo ar ual ista nati sine?
Násie, násie quetin lyenna, ya istalme quetilme ar ya ecénielme *vettalme, mal lé uar came *vettielma.
Qui anyárien len pa i nati cemeno ar ualde save, manen savuvalde qui nyarin len pa i nati menelo?
Ente, *úquen eménie amba menelenna hequa ye utúlie undu menello, i Atanyondo.
Ar tambe Móses ortane i leuca i erumasse, sie mauya i Atanyondon náve ortaina,
i ilquen ye save sesse samuva oira coivie.
An Eru emélie i mar tenna antie *ernóna Yondorya, i ilquen ye save sesse lá nauva nancarna, mal samuva oira coivie.
An Eru mentane Yondorya, lá i namumnes i mar, mal i umne i mar rehtaina sénen.
Ye save sesse lá nauva námina. Ye ua save anaie námina yando sí, pan uas asávie mí esse i *ernóna Eruiono.
Ar si ná i námie, i utúlie i cala mir i mar, mal Atani méler i mornie or i cala, an cardaltar nar ulce.
An ye care şaure nati yelta i cala ar ua mere tule i calanna, i cardaryar lá nauvar naityane.
Mal ye é care ya ná nanwa tule i calanna, i nauva apantaina i cardaryar nar cárine Erusse.”
Apa nati sine Yésus ar hildoryar lender i Yúrea ménanna, ar tasse lemyanes aselte ar *tumyane.
Yoháno *tumyane Ainonde, hare Salimenna, pan enge úve nenwa tasse, ar queni túler ar náner *tumyaine,
an Yoháno en úme hátina mir mando.
Tá enge costie imbi Yohanneo hildor as Yúra pa sovalle.
Ar túlelte Yohannenna ar quenter senna: “Ravi, i nér ye enge aselye han Yordan, pa ye *vettanel, yé! sé *tumyea ar illi ménar senna.”
Yoháno hanquente ar eque: “Atan ua pole came erya nat qui ta ui antaina sen menello.
Lé inde *vettar nin i quenten: Inye ui i Hristo, mal nanye mentaina opo sé.
Ye same i indis i ender ná. Mal i endero meldo, íre taris hlárala se, same túra alasse íre hlaris i endero óma. Sie sina alassenya ná quanta.
Sé aláluva, mal inye fifíruva.”
Ye tule táriello ea or ilye exi. Ye ná cemeno cemello ná, ar quetis pa i nati cemeno. Ye tule menello ea or ilye exi.
Ya ecénies ar ahláries, pa ta *vettas, mal *úquen came *vettierya.
Ye acámie *vettierya apánie *lihtarya i voronwesse Eruo.
An ye Eru mentane quete Eruo quetier, an uas anta i Faire lestanen.
I Atar mele i Yondo ar ánie ilye nati máryanna.
Ye save i Yondosse same oira coivie; ye ua care ve i Yondo quete ua cenuva coivie, mal Eruo rúşe lemya sesse.
Íre i Heru sinte i náner i Farisar hlárienwe i Yésus carne ar *tumyane hildoli rimbe lá i hildor Yohanneo –
ómu Yésus ua *tumyane, mal hildoryar carner –
oantes Yúreallo ar lende ata Alileanna.
Mal mauyane sen lelya ter Samária.
Etta túles ostonna Samário estaina Sícar, hare i restanna ya Yácov antane Yósef yondoryan.
Tasse enge Yácovo ehtele. Sie Yésus, lumba i lendanen, hamne undu ara i ehtele. I lúme náne *os i enquea.
Nís Samáriallo túle calpien. Yésus quente senna: “Ánin anta yulda.”
An hildoryar náner autienwe mir i osto *homancien matso.
Etta i nís Samáriallo quente senna: “Manen elye, ye ná Yúra, arca yulda nillo ye ná Samárea nís?” An Yúrar ar Samárear samir munta uo.
Yésus hanquente ar eque senna: “Qui sintel Eruo anna ar nasse yeo quéta lyenna: Ánin anta yulda, elye arcane sello, ar antanes lyen coirea nén.”
Eques senna: “Heru, ual same calpa, ar i tampo ná núra. Mallo, tá, samil i coirea nén?
Lau elye ná túra lá Yácov aterelma, ye antane men i tampo ar ye immo sunce sallo as yondoryar ar celvaryar?”
Yésus hanquente ar eque senna: “Ilquen ye suce nén sino nauva soica ata.
Aiquen ye suce i neno ya inye antuva sen laume nauva soica, mal i nén ya inye antuva sen nauva sesse ehtele nenwa ya orta amba mir oira coivie.”
I nís quente senna: “Heru, ánin anta nén sina, i lá nauvan soica ar lá mauyuva nin tule sinomenna calpien.”
Eques senna: “Mena, yala verulya ar tula sir!”
I nís hanquente ar eque: “Uan same veru.” Eque senna Yésus: “Quentel mai: Veru uan same.
An asámiel veruvi lempe, ar ye samil sí ui *lyenya veru. Pa nat sina quentel i nanwie.”
I nís quente senna: “Heru, cenin i elye Erutercáno ná.
Atarilmar *tyerner oron sinasse, mal lé quetir i Yerúsalem ná i vanima nóme *tyerien.”
Eque senna Yésus: “Sava quetienya, nís: I lúme túla yasse ualde *tyeruva i Atar var oron sinasse var Yerúsalemesse.
Lé *tyerir ya ualde ista; elme *tyerir ya istalme, an rehtie tule Yúrallon.
Mal i lúme túla, ar eas sí, yasse i nanwe *tyerindor *tyeruvar i Atar fairenen ar nanwiénen, an taiti *tyerindor i Atar cesta.
Eru faire ná, ar mauya in se-*tyerir *tyere fairenen ar nanwiénen.”
I nís quente senna: “Istan i túla Messías, ye ná estaina Hristo. Quiquie sé tuluva, nyaruvas men ilqua.”
Eque senna Yésus: “Inye ye quéta aselye ná sé.”
Sí hildoryar nanwenner, ar engelte elmendasse pan carampes as nís; ananta *úquen quente: “Mana cesteal?” hya “Mana i casta yanen quétal óse?”
Sie i nís hehtane calparya ar oante mir i osto ar quente i queninnar:
“Tula sir, cena nér ye anyárie nin ilqua ya acárien! Cé sé ná i Hristo?”
Lendelte et i ostollo ar túler senna.
I imya lúmesse i hildor hortaner se, quétala: “Ravi, mata.”
Mal sé quente téna: “Samin matso matien ya lé uar ista.”
Etta i hildor quenter, quén i exenna: “Ma aiquen tulune senna *aiqua matien?”
Eque téna Yésus: “Matsonya ná i inye care i indóme yeo ni-mentane ar telya molierya.
Ma ualde quete i ear en astar canta nó i yávie tule? Yé! Quetin lenna: Á orta henduldat ar á yéta i restar, i nalte ninqui comien i yávie. Yando sí
i *rerindo cáma *paityalerya ar comyea yáve oira coivien, i polit i *cerihtando ar i *rerindo same alasse uo.
An sís eques sina nanwa ná: Quén erérie ar exe *cirihtea.
Inye mentane le *cirihtien ya ualde omólie. Exeli omólier, ar lé utúlier mir molielta.”
Ar sana ostosse rimbali i Samárearon sáver sesse quettanen i nisso ye *vettane: “Nyarnes nin ilqua ya carnen.”
Etta, ire i Samárear túler senna, arcanelte sello lemie aselte; ar lemyanes tasse ré attasse.
Ar an rimbali sáver yanen quentes,
ar quentelte i nissenna: “Ualme ambe save *lyenya quetiénen, an imme ahlárielmes, ar istalme i ná nér sina nanwave i *rehtando i mardo.”
Apa i aure atta oantes talo Alileanna.
Mal Yésus immo *vettane i véra nómeryasse Erutercáno ua same laitie.
Sie, íre túles Alileanna, i queni Alileo camner se, pan anelte cénienwe ilye i nati yar carnes Yerúsalemesse i aşaresse, an yando té lender i aşarenna.
Sie túles ata Cánanna Alileo, yasse vistanes i nén mir limpe. Ar enge arandur yeo yondo náne hlaiwa Capernaumesse.
Íre nér sina hlasse i Yésus náne túlienwa et Yúreallo mir Alilea, lendes senna ar arcane sello i tulumnes undu *nestien yondorya, an anes hare qualmenna.
Mal Yésus quente senna: “Qui ualde cene tannar ar elmendar, laume savilde.”
I arandur quente senna: “Heru, tula undu nó seldonya quale.”
Eque senna Yésus: “Mena! Yondolya ná coirea.” I nér sáve i quetta ya Yésus quente senna ar oante.
Mal íre enges en i mallesse mólyar se-veldaner nyarien i yondorya náne coirea.
Etta cenşeset pa i lúme ya anes ambe alwa. Ar quentelte senna: “Noa, i otsea lúmesse, i úre váne sello.”
Sie i atar sinte i martanes i imya lúmesse ya Yésus quente senna: “Yondolya ná coirea.” Ar sé ar quanda nosserya sáver.
Ta né i attea tanna ya Yésus carne, íre túles et Yúreallo mir Alilea.
Apa nati sine enge aşar Yúraron, ar Yésus lende amba Yerúsalemenna.
Yerúsalemesse ea ailin estaina Heveryasse Vetsáşa, arwa *otarmiéron lempe.
Mi tai rimbe caine: hlaiwali, lombali, *úlévimáli ar hessali, *lartala coiren i nenwa.
An lúllo lúnna Héru vala túle undu mir i ailin, tyárala coire i nenwa, ar aiquen ye lende minya mir i ailin apa i *tarastie i nenwa náne *nestaina ilya nostaleo hlíveva ya sámes.
Mal enge tasse nér sámienwa hlíverya loassen tolto *nelequean.
Cénala nér sina caitea tasse, ar istala i *nollo anes hlaiwa anda lúmesse, Yésus quente senna: “Ma meril mále?”
I hlaiwa nér hanquente senna: “Heru, uan same nér panien ni i ailinde íre i nén *tarastaina ná, ar íre inye túla, exe lelya undu nó ni.”
Yésus quente senna: “Á orta, á mapa amba caimalya ar á vanta!”
Ar mí imya lú i nér camne mále, ar ortanes caimarya ar *yestane vanta.Mal sana aure náne sendare.
Etta i Yúrar quenter i *nestaina nerenna: “Si ná sendare; colie i caima ui lávina lyen.”
Mal hanquentes téna: “Ye nin-antane mále, sé quente ninna: Á mapa amba caimalya ar á vanta.”
Quentelte senna: “Man ná i nér ye quente lyenna, Ása mapa amba ar á vanta – ?”
Mal i *nestaina nér ua sinte man anes, an Yésus náne autienwa, pan enge şanga i nómesse.
Apa nati sine Yésus se-hirne i cordasse ar quente senna: “Cena, sí samil mále. Áva úcare ambe, i ua martuva lyen ambe ulca nat.”
I nér oante ar nyarne i Yúrain i náne Yésus ye antane sen mále.
Ar sina castanen i Yúrar roitaner Yésus, pan carnes nati sine i sendaresse.
Mal hanquentes téna: “Atarinya mole tenna sí, ar sie inye móla.”
Sina castanen i Yúrar merner en ambe nahta se, an lá rie rances i sendare, mal yando estanes Eru véra atarya, cárala immo ve Eru.
Etta Yésus hanquente ar eque téna: “Násie, násie quetin lenna, i Yondo ua pole care erya nat immonen, mal rie ya cenis i Atar cára. An *aiqua ya sé care, ta i Yondo yando care i imya lénen.
An i Atar mele i Yondo ar tana sen ilqua ya sé care, ar sen-tanuvas cardali túre lá sine, i nauvalde elmendasse.
An tambe i Atar orta qualini ar care te coirie, sie yando i Yondo care i meris coirie.
An i Atar ua name aiquen, mal ánies ilya namie i Yondon,
i illi laituvar i Yondo ve laitalte i Atar. Ye ua laita i Yondo ua laita i Atar ye mentane se.
Násie, násie quetin lenna: Ye hlare quettanya ar save yesse ni-mentane same oira coivie, ar uas tule namienna, mal oantie ñurullo coivienna.
Násie, násie quetin lenna: I lúme túla, ar eas sí, íre qualini hlaruvar i Eruiono óma, ar i hlarir tuluvar coivienna.
An tambe i Atar same coivie immosse, sie ánies i Yondon samie coivie immosse.
Ar ánies sen hére namiéva, pan Atanyondo náse.
Áva *fele elmenda pa si, pan i lúme túla yasse illi i ear i noirissen hlaruvar ómarya
ar ettuluvar, i carner máre nati enortiesse coivienna, mal i carner şaure nati enortiesse namiéno.
Uan pole care erya nat immonen; ve hlarin namin, ar námienya faila ná, an uan cesta véra şelmanya, mal i şelma yeo mentane ni.
Qui inye *vetta pa immo, *vettienya ui nanwa.
Ea exe ye *vetta pa ni, ar istan i *vettierya pa ni ná nanwa.
Lé mentaner quelli Yohannenna, ar sé *evettie pa i nanwie.
Inye ua came atano *vettie, mal quetin nati sine i cé nauvalde rehtaine.
Sana nér náne uryala ar caltala calma, ar ter lúme mernelte *alasta calaryasse.
Mal inye same i *vettie ya ná túra lá Yohanneo, an i cardar yar ánie nin Atarinya telien – i cardar yar inye cára – sane cardar *vettar pa ni i Atarinya mentane ni.
Ente, i Atar ye ni-mentane, sé *evettie pa ni. Ualde ahlárie ómarya hya ecénie cantarya,
ar ualde same quettarya lemyala lesse. An ye mentanes, sesse ualde save.
Ceşilde i tehteler, pan sanalde i tainen samuvalde oira coivie, ar tai nar yar *vettar pa ní.
Ananta ualde mere tule ninna, i samuvalde coivie.
Uan mere came alcar atanillon,
mal istan pa lé i ualde same melme Eruva lesse.
Inye utúlie i essenen Atarinyo, mal ualde came ni. Qui exe tule véra esseryanen, camuvalde sé.
Manen polilde save, íre camilde alcar quén i exello mal uar cesta i alcar ya tule i erya Ainollo?
Áva *sana i *ulquetuvan le epe i Atar. Ye le-*ulquete ná Móses, yesse apánielde estelelda.
An qui sávelde Móseo quetie sávelde ninya, an sé tence pa ni.
Mal qui ualde save tecieryar, manen savuvalde ninye quetier?”
Apa nati sine Yésus oante langala i Ear Alileo, hya Ear Tiverias.
Mal haura şanga se-hilyane, pan cennelte i tannar yar carnes i hlaiwassen.
Tá Yésus lende amba mir oron, ar tasse hamnes undu as hildoryar.
Sí i Lahtie, i aşar Yúraron, náne hare.
Íre Yésus ortane henyat ar cenne haura şanga túla senna, quentes Filipenna: “Masse *hómancuvalve massali i sine lertuvar mate?”
Mal si quentes tyastien se, an sé sinte ya carumnes.
Filip hanquente senna: “*Lenári tuxa atta uar fárie massain faryala tien, i ilya quén camuva pitya mitta.”
Quén i hildoron, Andréas i háno Símon Pétero, quente senna:
“Ea sís pitya seldo arwa massaron *findoriva lempe ar hala atto. Mal mana tai ta rimbe quenin?”
Eque Yésus: “Á tyare i atani hame undu.” Enge olya salque i nómesse. Etta i atani hamner undu, nóte *os húmi lempe.
Tá Yésus nampe i massar ar, apa antie hantale, etsanteset i hámala queninnar, ar sie yando i halar, ilya ya mernelte.
Mal apa anelte quátine eques hildoryannar: “Á comya i hehtaine mittar, i munta nauva vanwa.”
Etta comyaneltet, ar quantelte *vircolcar yunque mittalínen i massaron *findoriva lempe – i lembar hehtaine lo i mátienwe queni.
Sie, íre i atani cenner i tannar yar carnes, quentelte: “Nér sina nanwave ná i Erutercáno ye tulumne mir i mar.”
Etta Yésus, istala i sí tulumnelte ar mapumneltes carien se aran, oante mir i oron, sé erinqua.
Íre şinye túle, hildoryar lender undu i earenna,
ar apa menie mir lunte anelte lelyala olla ear Capernaumenna. Mal sí enge mornie, ar Yésus ena úme túlienwa téna.
Ente, i ear amoronte, pan polda súre náne hlápula.
Mal íre anelte círienwe *os *restandier lempe *yúquean hya *nelequean, cennelte Yésus vantea i earesse ar túla hare i luntenna, ar runcelte.
Mal eques téna: “Inye ná; áva ruce!”
Etta anelte mérala camitas mir i lunte, ar mí imya lú i lunte enge ara i nór yanna névelte lelya.
I hilyala auresse i şanga ya tarne i hyana fárasse i earo cenner i ua enge tasse lunte. Anelte cénienwe i ua enge hyana lunte hequa i er, ar i Yésus ua oante sánen as hildoryar, mal hildoryar erinque náner autienwe.
Mal lunteli Tiveriasello túler i nómenna yasse mantelte i massa apa i Heru antane hantale.
Etta, íre i şanga cenne i uar enge tasse var Yésus var hildoryar, mennelte mir lunteltar ar lender Capernaumenna cestien Yésus.
Íre hirneltes han i ear maquentelte senna: “Ravi, mana i lú yasse túlel sir?”
Yésus hanquente téna ar eque: “Násie, násie quetin lenna, cestealden, lá pan cennelde tannali, mal pan mantelde i massaron ar náner quátine.
Mola, lá i matson ya autuva, mal i matson ya lemya oira coivien, ya i Atanyondo antuva len; an sesse i Atar, Eru, apánie *lihtarya.”
Etta quentelte senna: “Mana caruvalme carien molierya Eru?”
Yésus hanquente ar eque téna: “Si molierya Eru ná, i savuvalde yesse sé mentane.”
Etta quentelte senna: “Mana, tá, cáral ve tanna, i cenuvalmes ar savuvalme lyesse? Mana cáral?
Atarilmar manter i mán i erumasse, ve ná técina: Antanes tien massa menello matien.”
Etta Yésus quente téna: “Násie, násie quetin lenna: Móses ua antane len i massa menello, mal Atarinya é anta len i nanwa massa menello.
An Eruo massa ná ye tule undu menello ar anta cala i marden.”
Etta quentelte senna: “Heru, illume ámen anta massa sina!”
Eque téna Yésus: “Inye ná i massa coiviéva. Ye tule ninna laume oi nauva maita, ar ye save nisse laume oi nauva soica.
Mal equétien lenna: ecénielden, ananta ualde save.
Ilquen ye i Atar nin-anta tuluva ninna, ar ye tule ninna laume hatuvan etsenna,
pan utúlien undu menello carien, lá ninya indóme, mal i indóme yeo mentane ni.
Si ná i indóme yeo mentane ni, i munta nauva nin vanwa ilyo ya ánies nin, mal enortuvanyes i métima auresse.
An si ná Atarinyo indóme, i ilquen ye cene i Yondo ar save sesse samuva oira coivie, ar enortuvanyes i métima auresse.”
Tá i Yúrar nurruner senna pan quentes: “Inye ná i massa ya túle undu menello.”
Ar quentelte: “Ma nér sina ua Yésus Yosefion, yeo atar ar amil istalve? Manen lertas quete: Utúlien undu menello – ?”
Yésus hanquente ar eque téna: “Áva na nurrula mici inde.
*Úquen pole tule ninna qui i Atar ye ni-mentane ua tuce se, ar inye enortuvanyes i métima auresse.
An ná técina mí Erutercánor: Ar illi mici te nauvar peantaine lo i Héru. Ilquen ye ahlárie i Atarello ar apárie tule ninna.
Lá i aiquen ecénie i Atar, hequa ye ná Erullo: sé ecénie i Atar.
Násie, násie quetin lenna: Ye save same oira coivie.
Inye ná i massa coiviéva.
Atarildar manter i mán i erumesse, ananta quallelte.
Si ná i massa ya tule undu menello, i aiquen pole mate sallo ar ua quale.
Inye ná i coirea massa ya tule undu menello; qui aiquen mate sina masso nauvas coirea tennoio. Ar i massa ya inye antuva ná hrávenya i mardo coivien.”
Etta i Yúrar costaner quén as i exe, quétala: “Manen nér sina pole anta ven hráverya matien?”
Sie Yésus quente téna: “Násie, násie quetin lenna, qui ualde mate i Atanyondo hráve ar suce sercerya, ualde same coivie indesse.
Ye mate hrávenya ar suce sercenya same oira coivie, ar enortuvanyes i métima auresse.
An ninya hráve ná nanwa matso, ar ninya serce ná nanwa yulda.
Ye mate hrávenya ar suce sercenya lemya nisse, ar ní sesse.
Tambe i coirea Atar ni-mentane ar samin coivie i Atarnen, sie yando ye mate ní samuva coivie inyenen.
Si ná i massa ya túle undu menello, lá ve ta ya atarildar manter ananta qualler. Ye mate massa sina nauva coirea tennoio.”
Nati sine quentes íre peantanes *yomencoasse Capernaumesse.
Etta rimbali hildoryaron, íre hlasselte si, quenter: “Quetie sina hranga ná; man pole lasta sanna?”
Mal Yésus, istala immonen i hildoryar nurruner pa si, quente téna: “Ma si le-tyare lanta?
Mana, tá, qui cenuvalde i Atanyondo ortea yanna enges yá?
I faire ná ya anta coivie; i hráve care munta aşea. I quetier yar inye equétie lenna nar faire ar nar coivie.
Mal ear mici le quelli i uar save.” An i yestallo Yésus sinte i uar sáve ar man né i quén ye *vartumne se.
Ar eques: “Sina castanen equétien lenna: *Úquen pole tule ninna qui ta ui antaina sen lo i Atar.”
Epeta rimbali hildoryaron oanter i natinnar ca te ar uar merne vanta óse ambe.
Etta Yésus quente i yunquenna: “Ma ualde mere auta, yando lé?”
Símon Péter hanquente senna: “Heru, ana man menuvalme? Elye same quetier oira coiviéva,
ar asávielme ar istalme i elye ná Eruo Aire.”
Yésus hanquente téna: “Ma uan icílie lé yunque? Ar mici lé quén arauco ná!”
Carampes pa Yúras, yondorya Símon Iscáriot. An nér sina, quén i yunqueo, *vartumne se.
Ar apa nati sine Yésus vantane Alileasse, an uas merne vanta Yúreasse, pan i Yúrar cestaner nahta se.
Mal i aşar Yúraron, i Meren Lassemárion, náne hare.
Etta hánoryar quenter senna: “Á auta silo ar mena mir Yúrea, i yando hildotyar cenuvar i cardar yar caritye!
An úquen care *aiqua nuldave qui cestas náve sinwa. Qui caritye nati sine, á apanta immo i marden!”
An hánoryar uar sáve sesse.
Etta Yésus quente téna: “Lúmenya en ua utúlie, mal lén i lúme illume manwa ná.
I mar ua pole yelta lé, mal yeltas ní, an *vettan pa sa i cardaryar nar ulce.
Alde lelya amba i aşarenna! Inye ua lelya amba aşar sinanna, an lúmenya en ua utúlie.”
Apa quetie téna nati sine lemyanes Alileasse.
Mal apa hánoryar lender i aşarenna, yando sé lende amba, lá pantave, mal nuldave.
Etta i Yúrar cestaner se i aşaresse ar quenter: “Masse ná sana nér?”
Ar enge olya *nurrule pa sé imíca i şangar. Quelli quenter: “Náse mane nér.” Exeli quenter: “Uas, mal tyaris i şanga ranya!”
Ve hanyaina, *úquen carampe pantave pa sé, an runcelte i Yúrallon.
Íre perta i aşaro náne autienwa, Yésus lende amba mir i corda ar peantane.
Etta i Yúrar enger elmendasse, quétala: “Manen nér sina ista i Tehteler íre uas apárie ñóle?”
Yésus etta hanquente téna ar eque: “Ya peantan ui ninya, mal ná yeva ni-mentane.
Qui aiquen mere care indómerya, istuvas pa i peantie qui nas Erullo hya qui quetin et immollo.
Ye quete et immollo cestea véra alcarerya; mal ye cesta i alcar i queno ye se-mentane, sé şanda ná, ar lá ea *úfailie sesse.
Ma Móses ua antane len i Şanye? Mal *úquen mici le care ve i Şanye quete. Mana i casta yanen cestealde nahta ni?”
I şanga hanquente: “Nalye haryaina lo rauco! Man cestea nahta lye?”
Yésus hanquente ar eque téna: “Erya carda carnen, ar lé enger illi elmendasse.
Sina castanen Móses antane len i *oscirie – lá i nas Mósello, mal i atarillon – ar *oscirilde atan sendaresse.
Qui atan came *oscirie i sendaresse, i Móseo şanye lá nauva rácina, ma nalde rúşie nin pan antanen ilvana mále atanen i sendaresse?
Áva name ve cenilde, mal nama failie!”
Etta quelli Yerúsalemello quenter: “Ma sé ui i quén ye cestealte nahta?
Ar yé! pantave quétas, ar quetilte munta senna. Lau i turcor nanwave ihírier i náse i Hristo?
Mal istalve yallo quén sina ná. Íre Messías tuluva, *úquen istuva yallo náse.”
Etta Yésus, íre peantanes i cordasse, etyáme ar quente: “Ní istalde, yando istalde yallo nanye. Lá utúlien immonen, mal ye ni-mentane şanda ná, sé ye lé uar ista.
Inye ista se, an sello tulin, ar sé mentane ni.”
Tá névelte mapa se, mal *úquen panyane márya sesse, an lúmerya en úme túlienwa.
Mal rimbali i şango sáver sesse ar quenter: “Íre i Hristo tuluva, lau caruvas cardar rimbe lá yar acárie nér sina?”
I Farisar hlasser i şanga nurrua nati sine pa se, ar i hére *airimor ar i Farisar mentaner cánoli se-mapien.
Etta eque Yésus: “En euvan aselde şinta lúme, nó autuvan yenna ni-mentane.
Cestuvalden, mal ualde hiruva ni, ar yanna ean lé uar poluva tule.”
Etta i Yúrar quenter mici inte: “Manna nér sina autuva, i elve uar hiruva se? Lau autuvas innar nar vintaine mici Hellenyar, peantien Hellenyar?
Mana tea quetie sina ya quentes: Cestuvalden, mal ualde hiruva ni, ar yanna ean lé uar poluva tule – ?”
Mal i métima auresse, i túra aure i aşaro, Yésus oronte ar etyáme, quétala: “Qui aiquen soica ná, lava sen tule ninna sucien!
Ye save nisse, et ammitya rantaryallo ulyuvar celuméli coirea nenwa, ve i Tehtele equétie.”
Mal si quentes pa i faire ya camumner i sáver sesse. An ena lá enge faire, an Yésus en úme *alcaryaina.
Etta quelli i şango i hlasser quettar sine quenter: “Quén sina é ná i Erutercáno.”
Exeli quenter: “Si ná i Hristo.” Mal enger i quenter: “Lau i Hristo tule Alileallo?
Ma i Tehtele ua quete i tule i Hristo i erdo Laviro, ar Vet-Lehemello, i masto yasse enge Lavir?”
Etta enge şanca pa se i şangasse.
Mal quelli mici te merner mapatas, mal *úquen panyane mát sesse.
Etta i cánor nanwenner i hére *airimonnar ar Farisannar, ar té quenter téna: “Manen ualde túlua se aselde?”
I cánor hanquenter: “Atan ua oi acarpie sie.”
I Farisar etta hanquenter: “Lau yando lé nar *útulyaine?
Ma aiquen imíca i turcor hya Farisar asávie sesse?
Mal şanga sina ya ua ista i Şanye, té nar húne!”
Nicorémus, quén imíca te ye náne túlienwa senna yá, quente téna:
“Lau Şanyelva name nér qui mo ua minyave ánie sen hlarie ar ista ya acáries?”
Hanquentelte senna: “Ma yando elye Alileallo ná? Ceşa ar cena i lá nauva Erutercáno ortaina et Alileallo.”
Tá oantelte, ilquen maryanna.
Mal Yésus lende Orontenna *Milpioron.
Árasse nanwennes i cordanna, ar i quanda lie túle senna, ar hamnes undu ar peantane tien.
Mal i ingolmor ar i Farisar tuluner senna nís mapaina *úpuhtiesse. Panyala se endeltasse
quentelte Yésunna: “*Peantar, nís sina anaie mapaina i cardasse *úpuhtiéva.
I Şanyesse Móses canne i *saryuvalme taiti nissi. Mana, tá, elye quete?”
Quentelte si tyastien se, i samumnes nat yanen ence tien *ulquete se. Mal luhtanes undu ar tence i talamesse leperyanen.
Íre maquentelte senna voronwiénen, orontes ar eque tien: “Lava yen ná pen úcare hate senna i minya sar!”
Ar ata luhtala undu tences i talamesse.
Mal i hlasser si lender oa, quén apa quén, i amyárar minye, tenna lemyanes erinqua as i nís ye tarne endeltasse.
Yésus oronte ar quente senna: “Nís, masse nalte? Ma *úquen namne lye?”
Eques: “*Úquen, heru.” Yésus quente: “Yando inye ua name lye. Mena; ho sí áva úcare ambe.”
Tá Yésus carampe téna ata, quétala: “Inye i mardo cala ná. Ye hilya ní ua vantuva morniesse, mal samuva i cala coiviéva.”
Sie i Farisar quenter senna: “*Vettal pa immo; *vettielya ua nanwa.”
Yésus hanquente ar eque: “Yando qui *vettan pa immo, *vettienya nanwa ná, pan istan yallo tulin ar yanna autan.
Lé namir i hrávenen; inye laume name aiquen.
Ananta, qui é namin, námienya nanwa ná, pan uan erinqua, mal i Atar ye ni-mentane ea óni.
Ente, véra Şanyeldasse ná técina: Atan atto *vettie nanwa ná.
Inye ná quén ye *vetta pa immo, ar i Atar ye ni-mentane *vetta pa ni.”
Etta quentelte senna: “Masse ea Atarelya?” Yésus hanquente: “Ualde ista var ni var Atarinya. Qui sintelde ní, sintelde yando Atarinya.”
Quetier sine quentes i harwesse íre peantanes i cordasse. Mal *úquen nampe se, pan lúmerya en úme túlienwa.
Sie quentes téna ata: “Autan, ar cestuvalden, ananta qualuvalde úcareldasse. Yanna inye auta lé uar pole tule.”
Etta i Yúrar quenter: “Lau nahtuvas immo? Pan quetis: Yanna inye auta lé uar pole tule.”
Mal quentes téna: “Lé nar i tumnallon; inye i tarmenillon ná. Lé nar mar sinallo; inye ui mar sinallo.
Etta quenten lenna i qualuvalde úcareldassen. An qui ualde save i ní ná sé, qualuvalde úcareldassen.”
Etta quentelte senna: “Man nálye?” Eque téna Yésus: “Ye nyarnen len i yestallo.
Samin rimbe natali quetien ar namien pa lé. Mal ye ni-mentane şanda ná, ar i nati yar hlassen sello quétan i mardesse.”
Ualte hanyane i carampes téna pa i Atar.
Etta Yésus quente: “Íre ortanielde i Atanyondo, tá istuvalde i ní ná sé, ar i carin munta immonen, mal tambe i Atar peantane nin quetin nati sine.
Ar ye ni-mentane ea asinye; uas ehehtie ni erinqua, pan inye illume care ya ná sen mára.”
Íre quentes nati sine, rimbali sáver sesse.
Etta Yésus quente i Yúrannar i sáver sesse: “Qui lemyalde quettanyasse, nalde nanwave hildonyar,
ar istuvalde i nanwie, ar i nanwie leryuva le.”
Hanquentelte senna: “Nalme Avrahámo erde, ar ualme oi anaie aiqueno móli. Manen lertal quete: Nauvalde lére –?”
Yésus hanquente téna: “Nanwie, nanwie quetin lenna: Ilquen ye care úcare ná i úcareo mól.
Ente, i mól ua lemya i coasse tennoio; i yondo lemya tennoio.
Etta, qui i Yondo le-lerya, é nauvalde lére.
Istan i lé nar Avrahámo erde; mal cestealde nahta ní, an quettanya ua hire nóme imíca le.
Yar ecénien ara Atarinya quetin; ar sie lé carir yar ahlárielde atareldallo.”
Hanquentes senna: “Atarelma Avraham ná!” Eque téna Yésus: “Qui nalde Avrahámo híni, cara Avrahámo cardar!
Mal sí cestealde nahta ní, atan ye anyárie len i nanwie ya hlassen Erullo. Avraham ua carne sie.
Carilde i cardar atareldo.” Quentelte senna: “Umilme nóne *úpuhtiénen; samilme erya Atar, Eru.”
Yésus hanquente téna: “Qui Eru náne Atarelda, mélelde ní, an Erullo etutúlien ar nanye sís. Ente, uan túle immonen, mal sé ni-mentane.
Manen ualde hanya ya quétan? Pan ua ece len lasta quettanyanna.
Nalde atareldo, i Arauco, ar merilde care atareldo íri. Sé náne *atannahtar i yestallo, ar uas tarne tulca i nanwiesse, pan nanwie lá ea sesse. Íre quetis i huru quetis et véraryallon, pan náse *hurindo ar i huruo atar.
Mal pan inye quete lenna i nanwie, ualde save quetienya.
Man mici lé naitya ní pa úcare? Qui quetin nanwie, manen ualde save ya quetin?
Ye ná Erullo lasta Eruo quetiennar. Sinen elde uar lasta, an ualde Erullo.”
I Yúrar hanquenter senna: “Ma ualme quete vanimie i nalye Samárea ar haryaina lo rauco?”
Yésus hanquente: “Umin haryaina lo rauco, mal laitan Atarinya, ar lé *úlaitar ní.
Mal uan cestea alcar inyen; ea Quén ye cestea ar náma.
Násie, násie quetin lenna: Qui aiquen himya quettanya, laume oi cenuvas qualme.”
I Yúrar quenter senna: “Sí é istalme i nalye haryaina lo rauco. Avraham qualle, yando i Erutercánor, mal elye quete: Qui aiquen himya quettanya, laume oi cenuvas qualme.
Lau elye ná túra lá Avraham atarelma, ye qualle? Yando i Erutercánor qualler. Man carilye immo?”
Yésus hanquente: “Qui *alcaryuvan immo, alcarinya munta ná. Ye ni-*alcarya Atarinya ná, ye lé quetir ná Ainolda,
ananta ualde ista se. Mal inye ista se. Ar qui quenten i uan ista se, umnen ve lé – *hurindo. Mal istanyes, ar quettarya himyan.
Avraham atarelda sáme túra alasse i sanwenen i cenumnes ninya aure, ar cenneses ar *alastane.”
Etta i Yúrar quenter senna: “Elye en ua same loar *lepenquean, ananta ecéniel Avraham?”
Yésus quente téna: “Násie, násie quetin lenna, nó Avraham enge, inye ea.”
Etta leptanelte sardeli hatien tai senna, mal Yésus nurtane immo ar oante i cordallo.
Íre lahtanes, cennes nér lomba i mónallo.
Ar hildoryar cenşer se: “Ravi, man úacárie, nér sina hya nostaryat, pan anes nóna lomba?”
Yésus hanquente: “Nér sina ua úcarne, hya nostaryat, mal si martane i umner Eruo cardar apantaine senen.
Mauya ven care i molie yeo ni-mentane íre ea aure; i lóme túla yasse *úquen pole mole.
Íre ean i mardesse, nánye i mardo cala.”
Apa quentes nati sine, piutanes i talamenna ar carne luxo i piutanen, ar panyanes i luxo i nero hendunna
ar eque senna: “Mena, sova immo i ailinde estaina Siloam” – ya tea Mentaina. Ar sie oantes ar sóve immo, ar nanwennes cénala.
Etta i armaror ar i yá cenner i anes *iquindo quenter: “Nér sina ná ye hamne íquala, lá?”
quelli quenter: “Náse sé.” Exeli quenter: “Ui, mal náse ve sé.” I nér quente: “Inye ná sé!”
Etta quentelte senna: “Manen, tá, nét hendulyat latyaine?”
Hanquentes: “I nér estaina Yésus carne luxo ar sa-panyane hendunyatse ar quente ninna: Mena Siloamenna ar sova immo. Etta lenden ar sóve immo, ar tá polden cene.”
Ar quentelte senna: “Masse ná sana nér?” Eques: “Uan ista.”
Tulunelte i nér ye yá náne lomba i Farisannar.
Mal enge sendare i auresse ya Yésus carne i luxo ar latyane henyat.
Sie yando i Farisar cenşer se manen camnes *céne. Eques téna: “Panyanes luxo hendunyatse ar sóven immo, ar polin cene.”
Etta quelli i Farisaron quenter: “Nér sina ui Erullo, pan uas *hepe i sendare.” Exeli quenter: “Manen pole nér ye ná úcarindo care taiti tannali?” Ar enge şanca mici te.
Etta quentelte i lomba nerenna ata: “Mana elye quete pa se, pan latyanes hendulyat?” I nér quente: “Náse Erutercáno.”
Mal i Yúrar uar sáve pa se i yá anes lomba ar náne cámienwa *céne, tenna yaldelte i nostaru i nero ye camne *céne.
Ar cenşelte tú: “Ma quén sina ná yondosta, ye quetiste náne nóna lomba? Manen, tá, polis cene sí?”
Sie nostaryat hanquentet: “Istamme i si ná yondomma ar i anes nóna lomba.
Mal manen sí polis cene uamme ista, hya man latyane henyat uamme ista. Ceşa se! Náse veaner. Polis quete immon.”
Nostaryat quentet nati sine pan runcette i Yúrallon, an *nollo i Yúrar náner túlienwe *essámalenna i qui aiquen *etequentane Yésus ve Hristo, umnes hehtaina i *yomencoallo.
Sinen nostaryat quentet: “Náse veaner; ceşa se.”
Etta yaldelte attea lússe i nér ye yá náne lomba ar quenter senna: “Á anta Erun alcar; istalme i ná nér sina úcarindo.”
Mal sé hanquente: “Qui náse úcarindo uan ista. Er nat istan, i apa náve lomba sí cenin.”
Etta quentelte senna: “Mana carnes lyen? Manen latyanes hendulyat?”
Hanquentes téna: “Nyarnen len *nollo, ar ualde lastane. Mana i casta yanen merilde hlaritas ata? Ma yando lé merir náve hildoryar?”
Ar naityaneltes ar quenter: “Elye ná hildo sana nero, mal elme nar hildoli Móseo.
Istalme i Eru acarpie Mósenna, mal pa nér sina ualme ista mallo náse.”
I nér hanquente ar eque téna: “Si é ná elmenda, i ualde ista mallo náse, ananta latyanes hendunyat!
Istalve i Eru ua lasta úcarindonnar, mal qui aiquen ruce Erullo ar care indómerya, lastas senna.
Yalúmello *úquen oi ahlárie i aiquen latyane hendu queno ye náne nóna *lomba.
Qui nér sina úne Erullo, uas polde care *aiqua.”
Hanquentelte senna: “Elye náne aqua nóna úcarelissen, ananta peantal men?” Ar hanteltes etsenna.
Yésus hlasse i anelte hátienwe se etsenna, ar apa hirie se quentes: “Ma elye save i Atanyondosse?”
I nér hanquente: “Ar man náse, heru, i polin save sesse?”
Eque senna Yésus: “Ecénielyes; é náse ye quéta aselye.”
Tá eques: “Savin, Heru!” Ar hantes immo undu epe se.
Ar eque Yésus: “Namien túlen mir mar sina, i poluvar i lombar cene, ar i nauvar i cénalar lombe.”
Quelli i Farisaron i enger óse hlasser nati sine, ar quentelte senna: “Lau yando elme nar lombe?”
Eque téna Yésus: “Qui anelde lombe, pennelde úcare. Mal sí quetilde: Cenilme. Úcarelda lemya.”
”Násie, násie quetin lenna: Ye ua tule minna i mámannar ter i fenna, mal *rete amba hyana nómesse, sana quén ná arpo ar pilu.
Mal ye tule minna ter i fenna ná mavar i mámaron.
Sen i *fennatir latya, ar i mámar lastar ómaryanna, ar yalis vére mámaryar essenen ar te-tulya etsenna.
Íre utúlies véraryar etsenna, vantas epe te, ar i mámar hilyar se, pan istalte ómarya.
Ettelea quén laume hilyalte, mal uşuvalte sello, pan ualte ista i óma ettelearon.”
Yésus quente sestie sina téna, mal ualte hanyane yar quentes téna.
Etta Yésus quente ata: “Násie, násie quetin lenna: Inye ná i fenna i mámaron.
Illi i túler nó ni nar arpor ar pilur, mal i mámar uar lastane téna.
Inye ná i fenna; aiquen ye tule minna ter ní nauva rehtaina, ar menuvas minna ar etsenna ar hiruva nessele.
I arpo ua tule hequa pilien ar nahtien ar nancarien. Inye utúlie i samuvalte coivie ar samuvalte úve.
Inye ná i mane mavar; i mane mavar anta coivierya rá i mámain.
I paityana nér, ye ua mavar ar yeva i mámar uar véraryar, cene i ñarmo túla ar uşe – ar i ñarmo mapa te ar vinta te –
pan náse paityana nér ar i mámar uar valdie sen.
Inye ná i mane mavar, ar istan mámanyar ar mámanyar istar ní,
síve i Atar ista ni ar inye ista i Atar, ar antan coivienya rá i mámain.
Samin yando hyane mámali, i uar mici mámar sine; yando té mauya nin tulya, ar lastuvalte ómanyanna, ar nauvalte erya lámáre, erya mavar.
Sina castanen i Atar mele ni, pan antan coivienya, i encamuvanyes.
*Úquen amápie sa nillo, mal antanyes véra nirmenen. Samin hére antien sa, ar samin hére encamien sa. I canwa pa si camnen Atarinyallo.”
Sine quettainen enge hyana şanca imíca i Yúrar.
Rimbali mici te quenter: “Náse haryaina lo rauco ar ea sámaryo etsesse! Mana i casta yanen lastalde senna?”
Exeli quenter: “Sine uar quetier raucoharyaina nero. Rauco lau pole latya i hendu lombaron?”
Lúme yanasse enge i aşar ceutiéva Yérusalemesse. Enge hríve,
ar Yésus vantane i cordasse, Solomondo *otarmiesse.
Etta i Yúrar peller se ar quenter senna: “Manen andave me-*hepuval *útanciesse? Qui elye ná i Hristo, ámen nyare pantie!”
Yésus hanquente téna: “Nyarnen len, ananta ualde save. I cardar yar inye cára Atarinyo essenen, tai *vettar pa ni.
Mal lé uar save, pan umilde ninye mámaron.
Ninye mámar lastar ómanyanna, ar istanyet, ar hilyalten.
Ar antan tien oira coivie, ar laume oi nauvalte nancarne, ar *úquen te-*rapuva et mányallo.
Atarinya, ye ánie te nin, ná túra lá ilye exi, ar *úquen pole te-*rapa et i Ataro mállo.
Ní ar i Atar nát er.”
Ata i Yúrar ortaner sardeli *sarien se.
Yésus hanquente téna: “Tannen len rimbe máre cardali i Atarello. Pa mana tane cardaron merilde *sarya ni?”
I Yúrar hanquenter senna: “*Saryealme lye, lá pa mára carda, mal pa naiquetie, ar pan elye ye ná atan care immo aino!”
Yésus hanquente téna: “Ma ua técina Şanyeldasse: Quenten: Nalde ainoli – ?
Qui estanes ainoli i queni innar Eruo quetta túle – ar i Tehtele mo ua pole *aupanya –
ma quetilde ninna ye i Atar airitáne ar mentane mir i mar: Naiquétal, pan quenten: Nanye Eruion – ?”
Qui uan care Atarinyo cardar, áva save quetienyar.
Mal qui cáran tai, ar lé uar save quetienyar, sava i cardar, i istuvalde ar hanyuvalde i ea i Atar inyesse ar inye i Ataresse.”
Etta névelte ata mapa se, mal úşes et máltalto.
Ar oantes ata han Yordanna i nómenna yasse *tumyane Yoháno i yestasse, ar tasse lemyanes.
Ar rimbali túler senna, ar quentelte: “Yoháno ua carne erya tanna, mal ilqua ya Yoháno quente pa nér sina nanwa né.”
Ar rimbali sáver sesse tanome.
Enge nér ye né hlaiwa, Lasarus Vetaniallo, i masto Mário yo Marşa néşaryo.
I María yeo háno Lásarus náne hlaiwa né i nís ye *livyane i Heru níşima millonen ar *parahtane talyat findileryanen.
Etta i néşar mentaner quetta senna, quétala: “Heru, yé! ye melilye hlaiwa ná.”
Mal íre Yésus sa-hlasse quentes: “Hlíve sina ua qualmen, mal Eruo alcaren, i nauva i Atanyondo *alcaryaina sanen.”
Yésus méle Marşa ar néşarya ar Lasarus.
Mal íre hlasses i anes hlaiwa, lemyanes aure attasse i nómesse yasse enges.
Epeta, apa si, quentes i hildonnar: “Alve mene mir Yúrea ata.”
I hildor quenter senna: “Ravi, *şintanéya i Yúrar cestaner *sarya lye, ar ménal tar ata?”
Yésus hanquente: “Ear lúmi yunque aureo, lá? Qui aiquen vanta auresse, uas talta, pan cenis i cala mar sino.
Mal qui aiquen vanta i lómisse, taltas, pan i cala lá ea sesse.”
Quentes nati sine, ar tá eques téna: “Lasarus meldolva acaitie undu serien, mal menuvan tar eccoitien se.”
Etta i hildor quenter senna: “Heru, qui acaitie undu serien, nauvas alwa.”
Yésus carampe pa qualmerya, mal té sanner i carampes pa serie húmesse.
Etta Yésus tá quente téna pantie: “Lásarus aquálie;
ar márieldan nanye *alassea i uan náne tanome, i savuvalde. Mal alve lelya senna.”
Etta Tomas, ye né estaina i Onóno, quente i hyane hildonnar: “Alve lelya, yando elve, i qualuvalve óse.”
Sie Yésus, íre túles, hirne i Lasarus *nollo náne caitienwa auressen canta i noirisse.
Vetania né hare Yerúsalemenna, ve er ar perta lár sallo.
Sie rimbali Yúraron náner túlienwe Marşa yo Maríanna tiutien tú pa hánotta.
Etta Marşa, íre hlasses i túle Yésus, velde se; mal María lemyane i coasse.
Sie Marşa quente Yésunna: “Heru, qui anel sís, hánonya úne qualina.
Ananta istan i ilqua ya arcal Erullo, Eru antuva lyen.”
Eque senna Yésus: “Hánolya ortuva.”
Marşa quente senna: “Istan i ortuvas i *enortalesse i métima auresse.”
Eque senna Yésus: “Inye i *enortale ar i coivie. Ye save nisse, ómu qui qualis, tuluva coivienna,
ar ilquen ye ná coirea ar save nisse laume oi qualuva. Ma savil si?”
Eques senna: “Ná, Heru, savin i elye ná i Hristo, i Eruion ye tulumne mir i mar.”
Ar apa quetie si lendes oa ar yalde María néşarya, quétala nuldave: “I *Peantar ea sís, ar yálas tye.”
Sé, íre hlasses si, oronte lintiénen ar lende senna.
Yésus en ua túlelyane mir i masto, mal enges ena i nómesse yasse Marşa velde se.
Íre i Yúrar i enger as María mí coa tiutien se cenner i orontes ar *etemenne, etta hilyaneltes, intyala i anes lelyala i noirinna *nítien tasse.
Ar sie María, íre túles i nómenna yasse enge Yésus ar cenne se, lantane epe talyat, quétala senna: “Heru, qui anel sís hánonya úme qualin!”
Sie Yésus, íre cennes i *nítanes, ar i *nítaner i Yúrar i enger óse, ñónane faireryasse ar náne pálina.
Ar eques: “Masse apánieldes?” Quentelte senna: “Heru, tula ar cena.”
Yésus *nítane.
Etta i Yúrar quenter: “Yé manen méleses!”
Mal quelli mici te quenter: “Ma nér sina ye latyane i lombo hendu ua polde pusta quén sina qualiello?”
Sie Yésus, apa ñonie ata immosse, túle i noirinna. Sá náne rotto, ar ondo caine epe sa.
Eque Yésus: “Á mapa oa i ondo!” Marşa, i qualino néşa, quente senna: “Heru, sí ea holme, an acaities tasse auressen canta!”
Eque senna Yésus: “Ma uan quente lyenna i qui savil, cenuval Eruo alcar?”
Tá nampelte oa i ondo. Yésus ortane henyat ar quente: “Atar, hantan tyen pan ahlárietyen.
Inye ista i illume hlarityen, mal i márien i şango ya tára sís carampen, i savuvalte i tyé mentane ni.”
Ar apa quetie si etyámes taura ómanen: “Lasarus, ettula!”
Ar i quálienwa nér ettúle, hampa talyatse ar máryatse vaimalínen, ar arwa lanneo nútina or cendelerya. Eque téna Yésus: “Áse lerya ar lava sen lelya!”
Etta rimbali i Yúraron i náner túlienwe Maríanna ar cenner ya carnes sáver sesse.
Mal quelli mici te lender i Farisannar ar nyarner tien i nati yar carne Yésus.
Etta i hére *airimor ar i Farisar ócomner i Combe ar quenter: “Mana caruvalve, pan atan sina care rimbe tannali?
Qui lavilve sen *cacare sie, illi savuvar sesse, ar Rómear tuluvar ar mapuvar oa yúyo aire nómelva ar nórelva.”
Mal quén mici te, Caiafas, ye náne i héra *airimo loa yanasse, quente téna: “Laume istalde *aiqua,
ar ualde hanya i ná len aşea i erya atan qualuva rá i lien, i lá nauva i quanda nóre nancarna.”
Mal ta uas quente immonen, mal pan anes i héra *airimo, quentes ve Erutercáno i náne Yésus martyaina qualien i nóren,
ar lá rie i nóren, mal i polumnes comya mir er i Eruhíni i nar vintaine.
Etta sana aurello carnelte panoli nahtien se.
Etta Yésus ua ambe vantane pantie mici Yúrar, mal oantes talo i nórenna hare i erumenna, ostonna estaina Efraim, ar tasse lemyanes as hildoryar.
Sí i Lahtie Yúraron náne hare, ar rimbali lender amba et i ménallon poitien inte.
Etta cestanelte Yésus ar quentelte, quén i exenna: “Mana sanalde? I laume tuluvas i aşarenna?”
I hére *airimor ar i Farisar náner cánienwe i qui aiquen sinte yasse enges, nyarumnes, i polumnelte mapa se.
Sie Yésus, auri enque nó i aşar, túle Vetanianna, yasse enge Lásarus ye Yésus náne ortienwa qualinillon.
Etta carnelte sen *şinyemat, ar Marşa *veuyane, mal Lásarus enge imíca i cainer ara i sarno óse.
Tá María nampe *lungwe níşima millova – anwa, ammirwa *alanarda – ar *líve Yésuo talu ar *parahtane talyat findileryanen. I ne i níşima millo quante i coa.
Mal Yúras Iscariot, quén mici hildoryar, ye *vartumne se, quente:
“Manen níşima millo sina úne vácina lenári húmi nelden ar i tyelpe antaina i únain?”
Quentes si, lá pan únar náner valdie sen, mal pan anes arpo: Sámala i tyelpecolca nampes yar náner panyaine sasse.
Etta eque Yésus: “Áva hranga se! Lava sen hepitas i auren yasse nauvan talaina sapsanyanna.
An únar illume samuvalde mici le, mal ní ualde illume same.”
Íre haura şanga Yúralíva hanyaner i enges tasse, túlelte, lá rie Yésunen, mal yando cenien Lásarus, ye ortanes qualinillon.
Mal i hére *airimor carner panoli nahtien yando Lásarus,
pan sénen rimbali Yúraron lender tanna ar sáver Yésusse.
I hilyala auresse, íre i haura şanga ya náne túlienwa i aşarenna hlasse i Yésus né túlala Yerusalemenna,
nampelte olvali *nindornelion ar etemenner omentien se. Ar yámelte: “Hosanna! Aistana ná ye tule i Héruo essenen, i aran Israélo!”
Mal apa hirie nessa *pellope hamnes undu senna, ve ná técina:
“Áva ruce, Síoniel! Yé! Aranelya túla, hámala *pellopeo onnasse.”
I hildor uar hanyane nati sine i yestallo, mal íre Yésus náne *alcaryaina, tá enyaldelte manen nati sine náner técine pa se ar i carnelte sie sen.
Sie *vettaner i queni i enger óse íre yaldes Lásarus et i noirillo ar se-ortane qualinillon.
Sinen i şanga, pan hlasselte i carnes tanna sina, yando velde se.
Etta i Farisar quenter mici inte: “Cenilde i polilde care munta. Yé! I mar elendie ca se.”
Enger Hellenyali mici i lender amba *tyerien i aşaresse.
Etta túlelte Filipenna ye náne Vestairallo Alileo, ar arcanelte sello, quétala: “Hér, merilme cene Yésus.”
Filip túle ar nyarne Andréan. Andréas ar Filip túlet ar nyarnet Yésun.
Mal Yésus hanquente túna, quétala: “I lúme utúlie yasse i Atanyondo nauva *alcaryaina.
Násie, násie quetin lenta: Qui ore erdeo ua lanta mir i talan ar quale, lemyas i erya ore; mal qui qualis, tá colis olya yáve.
Ye mele coivierya nancare sa, mal ye yelta coivierya mar sinasse varyuva sa oira coivien.
Qui aiquen mere *veuya ni, lava sen hilya ni, ar yasse inye ea, tasse yando núronya euva.
Sí feanya *tarastaina ná, ar mana quetuvan? Atar, áni rehta et lúme sinallo! Mal casta sinan utúlien lúme sinanna.
Atar, á *alcarya esselya!” Etta óma túle et menello: “Yúyo *alcaryanenyes ar *alcaryuvanyes ata!”
Etta i şanga ya tarne tasse ar sa-hlasse quente i anes hundie. Exeli quenter: “Vala acarpie senna.”
Yésus hanquente ar eque: “Óma sina ua túle ninya márien, mal *lenyan.
Sí mar sina ná námina, sí i túr mar sino nauva hátina etsenna!
Ar inye, íre nauvan ortaina et cemello, tucuva ilye atani ninna.”
Mal sá quentes tanien i nostale qualmeo ya qualumnes.
Etta i şanga hanquente senna: “Ahlárielme i Şanyello i termare i Hristo tennoio, ar manen lertal quete i mauya i Atanyondon náve ortaina? Man ná Atanyondo sina?”
Yésus etta quente téna: “I cala euva mici le an şinta lúmesse. Á vanta íre samilde i cala, i ua mornie *orturuva le, an ye vanta i morniesse ua ista yasse lelyas.
Íre samilde i cala, sava i calasse, i nauvalde yondor calo.”Nati sine Yésus quente, ar oantes ar nurtane immo tello.
Mal ómu anes cárienwa ta rimbe tannali opo te, ualte sáve sesse,
*amaquatien i quetta ya quente Yesaia i Erutercáno: “Héru, man asávie ya ahlárielme? Ar i Héruo ranco, ana man anes apantaina?”
I cesta yanen ua ence tien save ná i ata Yesaia quente:
“Acáries hendultat lombe ar endalta sarda, i ualte cene hendultanten ar hanya endaltanen ar quere inte, i *nestumnenyet.”
Yesaia quente nati sine pan cennes alcarerya ar carampe pa se.
Ananta rimbali yando i turcoron sáver sesse, mal Farisainen ualte *etequentane se, i lá umnelte hehtaine i yomencoallo.
An mélelte i alcar atanillon ambe lá i alcar Erullo.
Mal Yésus etyáme ar eque: “Ye save nisse ua save nisse, mal yesse ni-mentane,
ar ye cene ní cene yando ye ni-mentane.
Utúlien ve cala mir i mar, i ilquen ye save nisse ua lemyuva i morniesse.
Mal qui aiquen hlare quetienyar ar ua cime tai, inye ua name se, an túlen, lá namien i mar, man rehtien i mar.
Ye *úcime ní ar ua came quetienyar same ya name se. I quetta ya inye equétie ná ya namuva se i métima auresse;
pan inye ua equétie immonen, mal i Atar ye ni-mentane, sé ánies nin canwa pa ya quetuvan ar carpuvan.
Ar istan i canwarya oira coivie ná. Etta, i nati yar quetin – tambe i Atar equétie tai ninna, sie carpan.
Nó i aşar i Lahtiéno Yésus sinte i sí lúmerya né túlienwa autien mar sinallo menien i Atarenna, ar apa melie véryaryar i enger i mardesse, méleset tenna i metta.
Anelte mátala i şinyemat, ar i Arauco náne *nollo pánienwa mir endarya Yúras Iscariot Símondion i se-vartumnes.
Yésus, istala i náne i Atar antienwa ilye nati máryanta, ar i ettúles Erullo ar menumne Erunna,
oronte i şinyematello ar panyane oa larmaryar. Nampes *paşelanne ar *quiltane immo.
Epeta ulyanes nén mir salpe, ar *yestanes sove i hildoron talu ar *parahta tú i *paşelannenen yanen anes *quiltaina.
Sie túles Símon Péterenna. Quentes senna: “Heru, ma sóval talunyat?”
Yésus hanquente ar eque senna: “Ya cáran ual hanya sí, mal hanyuvalyes apa nati sine.”
Símon Péter quente senna: “Elye laume oi sovuva ninye talu!” Yésus hanquente senna: “Qui uan sove lye, ual same ranta asinye.”
Símon Péter quente senna: “Heru, lá talunyat erye, mal yando mányat ar carinya!”
Yésus quente senna: “Ye osóvie immo ua same maure i *aiqua nauva sóvina hequa talyat, mal aqua poica ná. Ar lé nar poice, mal lá illi.”
An sintes i nér se-vartala. Etta quentes: “Umilde illi poice.”
Apa sovie talultat ar mapie larmaryar encaines undu ara i sarno ar quente téna: “Ma istalde ya acárien len?
Quetilde ninna, *Peantar ar Heru, ar quetilde mai, an ta nanye.
Etta, qui inye, i Heru ar i *Peantar, sóve taluldat, ná yando lenya rohta i sovilde quén i exeo talu.
An apánien len emma, i ve inye acárie, yando lé caruvar.
Násie, násie quetin lenna: Núro ua túra epe herurya, hya mentaina quén lá ye mentane se.
Qui istalde nati sine, *alassie nalde qui carilde tai.
Uan quete pa le illi; istan i icílien. Mal martas *amaquatien i tehtele: Ye mante massanya equérie ponterya ninna.
Lú sinallo nyarin len nó martas; sie, íre martuvas, savuvalde i ní ná sé.
Násie, násie quetin lenna: Ye came aiquen ye inye menta came ní, ar ye came ní, came ye ni-mentane.”
Apa quetie nati sine, Yésus náne *tarastaina fairesse, ar *vettanes ar quente: “Násie, násie quetin lenna: Quén imíca le *vartuva ni.”
I hilmor yenter quén i exenna, lá hanyala pa man quentes.
Epe Yésuo súma caine quén hildoryaron, ye Yésus méle.
Etta Símon Péter hiutane quén tananna ar eque senna: “Nyara man ná pa ye quétas!”
Sie i hildo, talta Yésuo ambostenna, quente senna: “Heru, man náse?”
Etta Yésus hanquente: “Náse yen antuvan i ranta massava ya *tumyan.” Apa *tumie i ranta sa-antanes Yúras yondoryan Símon Iscáriot.
Ar apa camie i ranta, tá Satan lende minna se. Etta Yésus quente senna: “Ya cáral, ása care lintie!”
Mal *úquen imíca i cainer ara i sarno sinte i casta yanen quentes si senna.
An quelli sanner, pan Yúras sáme i tyelpecolca, i Yésus quente senna: “Vaca yaiva samilve maure i aşaren,” hya i antumnes nat i únain.
Etta, íre camnes i ranta, *etemennes mí imya lú. Ar enge lóme.
Sie, apa *etemennes, Yésus quente: “Sí i Atanyondo ná *alcaryaina, ar Eru ná *alcaryaina sénen.
Qui Eru ná *alcaryaina sénen, Eru immo yando *alcaryuva sé, ar se-*alcaryuvas rato.
Hinyar, ean aselde an şinta lúmesse. Cestuvalden, ar tambe quenten i Yúrannar: Yanna inye auta lé uar pole tule, sie quetin yando lenna sí.
Vinya axan antan len: melie quén i exe. Aqua ve inye emélie le, sie yando lé meluvar quén i exe.
Sinen illi istuvar i nalde hildonyar, qui samilde melme mici inde.”
Símon Péter quente senna: “Heru, manna auteal?” Yésus hanquente: “Yanna autean ual pole hilya ni sí, mal hilyuval apa.”
Eque senna Péter: “Heru, manen uan pole hilya lye sí? Antuvan coivienya elyen!”
Yésus hanquente: “Ma antuval coivielya inyen? Násie, násie quetin lyenna: Tocot laume lamyuva nó alálielyen nel.”
”Áva lave endaldan náve *tarastaina. Sava Erusse; sava yando nisse.
I coasse Atarinyava ear rimbe şambeli. Qui lá, nyarnen len, pan autean manwien nóme len.
Ente, qui autan ar manwa nóme len, entuluvan ar le-camuva inyenna, i yasse inye ea yando lé euvar.
Ar yanna autean istalde i tie.”
Eque senna Tomas: “Heru, ualme ista yanna auteal. Manen istalme i tie?”
Yésus quente senna: “Inye i tie ar i nanwie ar i coivie. *Úquen tule i Ataranna hequa ter ní.
Qui sintelden, sintelde yando Atarinya. Lú sinallo istaldes ar ecénieldes.”
Eque senna Filip: “Heru, ámen tana i Atar, ar ta farya men.”
Yésus quente senna: “Ta andave engien aselde, Filip, ananta ual ista ni? Ye ecénie ní ecénie i Atar. Manen ece lyen quete: Ámen tana i Atar – ?
Ma ual save i inye ea i Ataresse ar i Atar inyesse? I nati yar quetin lenna uan quete et inyello, mal i Atar ye lemya nisse cára cardaryar.
Á save i inye ea i Ataresse ar i Atar inyesse; qui lá, á save i cardainen ve taiti!
Násie, násie quetin lenna: Ye save nisse, sé caruva yando i cardar yar inye care; ar caruvas cardali túre lá tai, pan inye auta i Atarenna.
Ar *aiqua ya arcalde essenyanen, ta caruvan, i nauva i Atar *alcaryaina i Yondonen.
Qui arcalde aiqua essenyanen, caruvanyes.
Qui melilden, himyuvalde axaninyar,
ar inye arcuva i Atarello, ar antuvas len hyana şámo ya euva aselde tennoio,
i Faire nanwiéva, ya i mar ua pole came, pan uas var cene sa var ista sa. Lé istar sa, pan lemyas aselde ar ea lesse.
Uan hehtuva le *nostarence. Túlan lenna.
Apa an şinta lúme i mar ua cenuva ni ambe, mal lé cenuvar ni, pan nanye coirea ar lé nauvar coirie.
Enta auresse istuvalde i inye ea Atarinyasse ar lé nisse ar ní lesse.
Ye same axaninyar ar himya tai, sé ná ye mele ni. Ente, ye mele ní nauva mélina lo Atarinya, ar inye meluva se ar apantuva immo sen.”
Yúras, lá Iscáriot, quente senna: “Heru, mana amartie, pan apantuval immo men ar lá i marden?”
Yésus hanquente ar eque: “Qui aiquen mele ni, himyuvas quettanya, ar Atarinya meluva se, ar tuluvamme senna ar maruvat óse.
Ye ua mele ni ua himya quettanyar, ar i quetta ya hláral ua ninya, mal i Ataro ye ni-mentane.
Íre lemyan aselde equétien nati sine len.
Mal i şámo, i Aire Fea, ye i Atar mentuva essenyanen, sé peantuva len ilye nati ar tyaruva le enyale ilqua ya nyarnen len.
Raine tyarin lemya aselde, rainenya antan len. Uan anta sa ve i mar anta. Áva lave endaldan náve *tarastaina hya ruhtaina!
Hlasselde i quenten lenna i autean ar nanwenuva lenna. Qui mélelden, sámelde alasse menienyanen i Atarenna, pan i Atar ná túra lá ni.
Ar sí anyárien len nó martas, i, íre é martas, savuvalde.
Uan carpuva ole aselde ho sí, an i mardo turco túla. Ar uas same túre or ni,
mal i istuva i mar i melin i Atar, etta cáran ve i canwa ya i Atar ánie nin. Á orta, alve lelya silo!”
”Inye i nanwa liantasse, ar Atarinya i *alamo ná.
Ilya olva nisse lá cólala yáve mapas oa, ar ilya ya cole yáve poitas, i coluvas amba yáve.
Lé nar *nollo poice i quettanen ya equétien len.
Á lemya nisse, ar inye lesse. Tambe i olva ua pole cole yáve qui uas lemya mí liantasse, sie lé uar pole, qui ualde lemya nisse.
Inye ná i liantasse, lé nar i olvar. Ye lemya nisse, ar ní sesse, sé cole olya yáve; pan au nillo polilde care munta.
Qui aiquen ua lemya nisse, náse hátina etsenna ve olva ar ná *parahtaina, ar queni hostar tane olvar ar hatir tai mir i ruine, ar nalte urtaine.
Qui lemyalde nisse ar quetienyar lemyar lesse, á arca *aiqua ya merilde, ar martuvas len.
Atarinya ná *alcaryaina sinen, i colilde olya yáve ar tanar inde ve hildonyar.
Síve i Atar emélie ní ar inye emélie lé, á lemya melmenyasse!
Qui himyalde axaninyar, lemyuvalde melmenyasse, síve inye ihímie i Ataro axani ar lemya melmeryasse.
Nati sine equétien len, i alassenya euva lesse ar alasselda nauva carna quanta.
Si axaninya ná, i melilde quén i exe ve emélien lé.
*Úquen same melme túra lá si, i quén anta coivierya meldoryain.
Lé nar meldonyar qui carilde ya canin len.
Uan ambe esta le móli, pan mól ua ista ya herurya care. Mal estanienye le meldor, an ilqua ya ahlárien Atarinyallo acárien sinwa len.
Lé uar cille ní, mal inye cille lé, ar apánien le *etemenien ar colien yáve, ar yávelda lemyuva, i len-antuva i Atar ya arcalde essenyanen.
Nati sine canin len, i meluvalde quén i exe.
Qui i mar yelta le, istalde i yeltanes ní nó le.
Qui anelde ranta i mardo, i mar méle vérarya. Mal pan umilde ranta i mardo, mal inye icílie le et i mardello, sinen i mar yelta le.
Enyala i quetta ya quenten lenna: Mól ua túra lá herurya. Qui ní roitanelte, yando lé roituvalte. Qui ihímielte quettanya, himyuvalte yando lenya.
Mal caruvalte ilye nati sine len essenyanen, pan ualte ista ye ni-mentane.
Qui uan túle ar carampe téna, ualte sáme úcare; mal sí ualte same casta antien pa úcarelta.
Ye yelta ní yelta yando Atarinya.
Qui uan acárie mici te i cardar yar *úquen hyana acárie, ualte sáme úcare; mal sí ecénielte ar eyétielte yúyo ní ar Atarinya.
Mal martas *amaquatien i quetta técina Şanyeltasse: Yeltanelten ú casto.
Íre i Şámo tuluva ye inye mentuva lenna i Atarello, i Faire nanwiéva, ye ettule i Atarello, sé *vettuva pa ní;
yando lé *vettuvar, pan engielde asinye i yestallo.”
”Nati sine equétien lenna i ualde nauva tyárine lanta.
Queni le-hehtuvar et i *yomencoallo. É i lúme túla yasse ilquen le-nahtala savuva i sie *veuyas Eru.
Mal caruvalte nati sine pan ualte ista var i Atar var ní.
Mal nati sine equétien lenna i, íre lúmelta tule, enyaluvalde i nyarnen len pa tai. Nati sine uan quente lenna i yestallo, pan engen aselde.
Ono sí autean yenna ni-mentane, ananta *úquen mici le maquete ninna: Manna auteal?
Mal pan equétien nati sine, nyére aquantie endalta.
Mal nyáran len i nanwie: Ná len aşea i autean. An qui uan auta, i Şámo laume tuluva lenna, mal qui autan, mentuvanyes lenna.
Ar íre sé tuluva tyaruvas i mar save, pa úcare ar pa failie ar pa namie:
pa úcare, pan ualte save nisse,
pa failie, pan autean i Atarenna ar ualde cenuva ni ambe,
pa namie, pan i turco mar sino anaie námina.
En samin rimbe natali quetien lenna, mal ualde pole colitat sí.
Ono íre sé tuluva, i Faire nanwiéva, le-tulyuvas mir i quanda nanwie, an uas quetuva et immollo, mal yar hlaris quetuvas, ar caruvas sinwe len yar tuluvar.
Sé *alcaryuva ni, pan camuvas et ninyallon ar caruvas tai sinwe len.
Ilye i nati yar i Atar same nar ninye. Tanen quentes i camis et ninyallon ar care tai sinwe len.
Apa şinta lúme ualde cenuva ni ambe, ar apa an şinta lúme encenuvalden.”
Etta quelli hildoryaron quenter, quén i exenna: “Mana tea si ya quetis venna: Apa şinta lúme ualde cenuva ni ambe, ar apa an şinta lúme encenuvalden, ar: Pan autean i Atarenna – ?”
Sie quentelte: “Mana tea si ya quetis: şinta lúme? Ualve hanya ya quétas.”
Yésus sinte i mernelte ceşitas, ar quentes téna: “Ma céşalde mici inde pa si, pan quenten: Apa şinta lúme ualde cenuva ni, ar apa an şinta lúme encenuvalden – ?
Násie, násie quetin lenna: *Nítuvalde ar nauvalde yaimie, mal i mar *alastuva; mal nyérelda nauva vistaina mir alasse.
Nís, íre cólas lapse, same nyére, pan lúmerya utúlie; mal íre ocólies i hína, uas ambe enyale şangierya, alasseryanen i anaie atan nóna mir i mar.
Sie yando lé samir nyére sí. Mal cenuvan le ata, ar endalda samuva alasse, ar *úquen mapuva alasselda lello.
Ar aure entasse ualde maquetuva ninna pa *aiqua. Násie, násie quetin lenna: *Aiqua ya arcalde i Atarello essenyanen antuvas len.
Tenna sí ualde arcanie *aiqua essenyanen. Á arca ar camuvalde, i alasselda nauva quanta.
Nati sine equétien lenna sestiessen. Lúme túla yasse uan ambe quetuva len sestiessen, mal pantave nyaruvan len pa i Atar.
Lúme entasse ualde arcuva *aiqua essenyanen, ar uan quete len i inye arcuva i Atarello pa le.
An i Atar immo mele le, pan lé emélier ní ar asávier i inye ettúle Erullo.
Etutúlien i Atarello ar utúlie mir i mar. Ente, autan i mardello ar mene i Atarenna.
Hildoryar quenter: “Yé! Sí quétal pantie, ar ual quete sestie!
Sí istalme i istal ilye nati ar ua same maure i aiquen ceşuva lye. Sinen istalme i ettúlel Erullo.”
Yésus hanquente téna: “Sí savilde?
Yé! I lúme túla, é utúlies, yasse nauvalde vintaine ilquen véra coaryanna, ar ní hehtuvalde erinqua, ananta uan erinqua, pan i Atar ea óni.
Equétien nati sine lenna, i nínen samuvalde raine. Mar sinasse samilde şangie, mal sama verie! Inye *orutúrie i mar.”
Yésus quente nati sine, ar ortala henyat menelenna eques: “Atar, i lúme utúlie; á *alcarya Yondotya, i Yondotya *alcaryuva tyé,
pan ánietye sen hére or ilya hráve, i antuvas oira coivie illin i ánietye sen.
Si oira coivie ná, i istalte tyé, i erya nanwa Aino, ar ye mentanetye, Yésus Hristo.
Tye-*alcaryanien cemende, telyala i molie ya ánietye nin carien.
Ar sí, Atar, áni *alcarya ara tyé i alcarnen ya sámen ara tyé nó i mar enge!
Apantanien essetya i atanin i antanetye nin et i mardello. Anelte *tyenye, ar antanetyet inyen, ar ihímielte quettatya.
Sí istalte i ilye nati yar antanetye nin nar tyello,
an i quetier yar antanetye nin ánien tien, ar acámieltet ar é istalte i tyé mentane ni.
Arcan pa te. Uan arca pa i mar, mal pa i ánietye nin, pan nalte *tyenye,
ar ilye natinyar nar *tyenye ar *tyenyar nar ninye, ar anaien *alcaryaina mici te.
Ente, uan ea ambe i mardesse, mal té ear i mardesse, ar inye túla tyenna. Aire Atar, áte varya véra essetyan ya ánietye nin, i nauvalte er, ve vet.
Íre engen aselte varyanenyet véra essetyan, ar *ehépienyet, ar *úquen mici te ná nancarna hequa ye náne martyaina nancarien, *amaquatien i tehtele.
Mal sí túlan tyenna, ar quetin nati sine i mardesse i samuvalte quanta alassenya intesse.
Ánien quettatya tien, mal i mar etévie te, pan umilte ranta i mardo, síve inye ua ranta i mardo.
Arcan tyello, lá i mapatyet et i mardello, mal i varyatyet ulcullo.
Ualte ranta i mardo, síve inye ua ranta i mardo.
Áte airita i nanwiénen; quettatya nanwie ná.
Ve mentanetye ní mir i mar, yando inye mentane té mir i mar.
Ar airitan immo tien, i nauvalte airinte i nanwiénen.
Arcan, lá pa té erinque, mal yando pa i savir nisse quettaltanen,
i nauvalte illi er, síve tyé, Atar, ea nisse ar inye tyesse, i yando té euvar vetse, i savuva i mar i tyé mentane ni.
Ente, i alcar ya ánietye nin ánien tien, i nauvalte er, síve vet nát er,
ní tesse ar tyé nisse, i nauvalte cárine ilvane mir er, i istuva i mar i tyé mentane ni ar i méletyet, ve méletye ní.
Atar, merin i yasse inye ea, yando i ánietye nin euvar asinye, i cenuvalte alcarinya ya ánietye nin, pan méletye ni nó i mar náne tulcaina.
Faila Atar, i mar ua ista tye, mal inye ista tye, ar queni sine istar i tyé mentane ni.
Ar acárien essetya sinwa tien ar caruvanyes sinwa ata, i euva tesse i melme yanen méletye ní, ar inye tesse.
Apa quetie nati sine, Yésus *etemenne as hildoryar olla i nelle estaina Cirron, nómenna yasse enge tarwa, ar sé ar hildoryar lender minna sa.
Mal yando Yúras, ye *vartane se, sinte i nóme, pan Yésus rimbe lúlissen velde as hildoryar tasse.
Etta Yúras nampe i ohtarhosta ar cánoli i hére *airimoron ar Farisaron ar túle tanna, túlula *narrundoli ar calmali ar carmali.
Sie Yésus, istala ilye i nati túlala senna, lende ompa ar quente téna: “Mana cestealde?”
Hanquentes: “Yésus Nasaretello.” Eques téna: “Inye sé.” Yando Yúras, ye *vartane se, tarne mici te.
Mal íre quentes téna: “Inye sé”, *nantarnelte ar lantaner i talamenna.
Etta hanquente téna ata: “Man cestealde?” Quentelte: “Yésus Nasaretello.”
Yésus hanquente: “Equétien lenna i inye ná sé. Etta, qui ní cestealde, lava tien auta”
– *amaquatien i quetta ya quentes: “Imíca i antanetye inyen *úquen ná nin vanwa.”
Tá Simon Péter, ye sáme macil, tunce sa ar pente i héra *airimo núro ar aucirne forya hlarya. I núro esse né Malcus.
Mal Yésus quente Péterenna: “Á panya macilelya i vainesse! I yulma ya i Atar ánie nin, lau sucuvanyes?”
Tá i ohtarhosta ar i *tuxantur ar i cánor i Yúraron namper Yésus ar nunter se.
Tulyaneltes minyave ana Annas, an sé náne i atar veriryo Caiafas, ye náne i héra *airimo loa yanasse.
Caiafas né ye quente i Yúrannar i náne tien aşea i erya atan qualle rá i lien.
Mal Símon Péter ar hyana hildo hilyanet Yésus. Sana hildo náne sinwa i héra *airimon ar túle as Yésus mir i paca i héra *airimóva,
mal Péter lemyane i etsesse ara i ando. Etta i hyana hildo, ye náne sinwa i héra *airimon, ettúle ar carampe yenna tirne i ando ar tulyane Péter minna.
Tá i vende ye tirne i ando quente: “Ma ua yando elye i hildoron atan tano?” Sé hanquente: “Uan.”
I núror ar i cánor náner cárienwe *hyulmaruine, an ringa né, ar tarnelte *lautala inte. Yando Péter tarne aselte *lautala immo.
Tá i héra *airimo maquente Yésunna pa hildoryar ar pa peantierya.
Yésus hanquente senna: “Acarpien i mardenna pantave. Illume peantanen *yomencoasse ar i cordasse, yasse ilye Yúrar tulir uo, ar quenten munta nuldiesse.
Mana i casta yanen ní céşal? Ceşa i hlasser ya quenten téna. Yé! Té istar ya quenten.”
Apa quentes nati sine, quén i cánoron i tarner ara se palpane Yésus i cendelesse ar eque: “Ma hanquétal i héra *airimonna sie?”
Yésus hanquente senna: “Qui quetin ya raica ná, á *vetta pa i raicie, mal qui quenten ya ná vanima, mana i casta yanen palpal ni?”
Tá Annas se-mentane nútina Caiafas i héra *airimonna.
Sí Símon Péter tarne *lautala immo. Tá quentelte senna: “Ma ua yando elye mici hildoryar?” Sa-lalanes ar eques: “Uan.”
Imíca i móli i héra *airimo enge quén i nosseo i nero yeo hlas Péter aucirne, ar eques: “Ma uan cenne lyé i tarwasse óse?”
Mal Péter lalane sa ata, ar mí imya lú tocot lamyane.
Tá tulyanelte Yésus Caiafasello i túriondenna i *nóreturwa. Sí arinya né. Mal té uar lende mir i túrion i *nóreturwa, lá náven *úpoice, mal lertumnelte mate i *lahtiémat.
Tá Piláto túle etsenna téna ar eque: “Mana i *ulquetie ya túlualde nér sinanna?”
Hanquentelte senna: “Qui nér sina úme *ongwemo, ualme *arantane se lyenna.”
Tá eque téna Piláto: “Lé áse mapa ar áse name şanyeldanen.” I Yúrar quenter senna: “Ualme lerta nahta aiquen”
– *amaquatien i quetta ya quente Yésus tanien i nostale qualmeo ya qualumnes.
Etta Piláto lende mir i túrion i *nóreturwa ar yalde Yésus ar quente senna: “Ma elye ná i aran Yúraron?”
Yésus hanquente: “Ma quetil si immonen, hya ma exeli acarpier lyenna pa ní?”
Piláto hanquente: “Lau inye Yúra ná? Véra nórelya ar i hére *airimor arantaner lye inyen. Mana acáriel?”
Yésus hanquente: “Aranienya ua mar sino. Qui náne aranienya mar sino, núronyar mahtaner, i inye lá umne *arantaina i Yúrain. Mal sí aranienya ui silo.”
Etta Piláto quente senna: “Sie nalye aran?” Yésus hanquente: “Elye quéta i nanye aran. Nat sinan anaien nóna, ar nat sinan utúlien mir i mar, i *vettumnen pa i nanwie. Ilquen ye ná i nanwiéno lasta ómanyanna.”
Eque senna Piláto: “Mana nanwie?”Apa quetie si *etemennes ata i Yúrannar ar quente téna: “Uan hire cáma sesse.
Mal samilde haime i senin len nér i Lahtiesse. Ma merilde, tá, i senuvan len i aran Yúraron?”
Tá yámelte ata, quétala: “Lá nér sina, mal Varavas!” Ono Varavas náne pilu.
Tá Piláto etta nampe Yésus ar *falpane se.
Ar i ohtari carner ríe necellíva ar sa-panyaner caryasse ar hanter *luicarne larma *os se,
ar túlelte epe se ar quenter: “Aiya, Aran Yúraron!” Ar palpaneltes i cendelesse.
Ar Piláto lende etsenna ata ar quente téna: “Cena! Túluanyes etsenna lenna ata, i istuvalde i uan hire cáma sesse.”
Etta Yésus ettúle, cólala i ríe neceliva ar i *luicarne larma. Ar quentes téna: “Ela i atan!”
Mal íre i hére airimor ar i cánor cenner se, yámelte quétala: “Áse tarwesta! Áse tarwesta!” Piláto quente téna: “Lé áse mapa ar áse tarwesta, an inye ua hire cáma sesse.”
I Yúrar hanquenter senna: “Samilme şanye, ar sana şanyenen náse valda qualmeo, pan acárien immo Eruo yondo.”
Etta, íre Piláto hlasse quetie sina, anes ambe ruhtaina,
ar lendes mir i túrion ata ar eque Yésunna: “Mallo tulilye?” Mal Yésus ua antane sen hanquenta.
Etta Piláto quente senna: “Ma ual carpa ninna? Ma ual ista i inye same hére lye-senien ar hére lye-tarwestien?”
Yésus hanquente senna: “Laume sámel hére or ní qui úmes antaina lyen táriello. Sinen i nér ye ni-*arantane lyen same ambe túra úcare.”
Etta Piláto cestane lé senien se. Mal i Yúrar yámer, quétala: “Qui senil nér sina, umilye meldo i Táraráno! Ilquen ye care immo aran amorta i Táraranna!”
Etta Piláto, apa hlarie quettar sine, tulune Yésus etsenna, ar hamnes undu námohammasse nómesse estaina i Sarna Paca, hya Heveryasse Avaşa.
Sí enge i manwie i Lahtiéva; náne *os i nertea lúme. Ar quentes i Yúrannar: “Ela aranelda!”
Mal yámelte: “Áse mapa oa, áse mapa oa! Áse tarwesta!” Piláto quente téna: “Ma tarwestuvan aranelda?” I hére *airimor hanquenter: “Ualme same aran enga i Táraran!”
Tá, etta, *arantanes Yésus tien tarwestien.Sie nampelte Yésus.
Cólala tarwerya insen *etemennes i nómenna estaina *Caraxomen, yó Heverya esse ná Olyoşa.
Tasse tarwestaneltes, ar exe atta óse, quén foryaryasse ar quén hyaryaryasse, as Yésus i endesse.
Piláto tence yando tanwa ar panyane sa i tarwesse. Né técina: “Yésus Nasaretello, Aran Yúraron.”
Sie rimbali i Yúraron hentaner tanwa sina, pan i nóme yasse Yésus náne tarwestaina caine hare i ostonna, ar anes técina Heveryasse, Latinde ar Hellenyasse.
Mal i hére airimor Yúraron quenter Pilátonna: “Áva tece: Aran Yúraron, mal i quentes: Inye ná Aran Yúraron.”
Piláto hanquente: “Ya etécien etécien.”
I ohtari, íre tarwestanelte Yésus, namper larmaryar ar carner mittar canta, er mitta ilya ohtaren, ar nampelte yando lauperya. Mal i laupe náne pen yanwe, *vírina ve erya mitta telmello talmanna.
Etta quentelte, quén i exenna: “Ávalve narca sa, mal alve hate *şanwali pa man samuva sa” – *amaquatien i tehtele ya quete: “*Ciltanelte larmanyar mici inte, ar pa laupenya hantelte *şanwali.” Ar i ohtari é carner sie.
Tarner ara Yésuo tarwe amillerya ar amilleryo néşa, María verirya Clópas, ar María Mahtaléne.
Sie, íre Yésus cenne amillerya ar i hildo ye méles tárala tasse, quentes amilleryanna: “Nís, ela yondotya!”
Tá quentes i hildonna: “Ela amillelya!” Ar sana lúmello i hildo nampe se mir véra coarya.
Epeta, íre Yésus cenne i ilqua náne telyaina, quentes *amaquatien i tehtele: “Nanye soica.”
Enge tasse salpa quanta sára limpeo, ar panyanelte hwan ehtisse ar tulune sa antoryanna.
Apa camie i sára limpe, Yésus quente: “Nás telyaina!”, ar láves caryan lanta ar effirne.
Pan si martane i auresse Manwiéva, i Yúrar arcaner Pilálollo i telcoltat umnet rácine ar i hroar mapaine oa, i lá lemyumnelte i tarwesse i sendaresse, an sana sendare náne túra.
Etta i ohtari túler ar rancer i telcu i minyo ar i exeo *yet nánet tarwestaine óse.
Mal túlala Yésunna cennelte i anes *nollo qualin ar uar rance telcoryat.
Ananta quén i ohtarion *terne etterya ehtinen, ar mí imya lú serce ar nén ettúlet.
Ar ye sa-ecénie *evettie, ar *vettierya nanwa ná, ar sé ista i nyáras nanwali, i yando lé savuvar.
An nati sine martaner *amaquatien i tehtele: “Erya axo hroaryo lá nauva rácina.”
Ar ata, hyana tehtele quete: “Yétuvalte yenna *etérielte.”
Apa nati sine Yósef Arimaşeallo, ye náne Yésus hildo, mal nuldiesse ruciénen i Yúrallon, arcane Pilátollo mapie oa Yésuo hroa, ar Piláto láve sa. Etta túles ar nampe hroarya oa.
Yando Nicorémus, i nér ye túle senna lómisse i yestasse, túle túlula ostime níşima suhteva ar *şortoava, *os *lungwi tuxa.
Sie nampette Yésuo hroa ar vaitanet sa vaimalissen as i níşime laiqui, ve Yúraron haime íre manwalte loico i sapsan..
Mal i nómesse yasse anes tarwestaina enge tarwa, ar i tarwasse vinya noire, yasse *úquen en náne panyaina.
Etta panyanette Yésuo hroa sanome, pan ta náne Yúraron aure Manwiéno, ar pan i noire hare né.
I minya auresse i otsolo María Mahtaléne túle i noirinna arinyave, íre en enge mornie, ar cennes i náne i ondo mapaina oa i noirillo.
Etta nornes ar túle Símon Péterenna ar i hyana hildonna, ye Yésus méle, ar quente túna: “Amápielte i Heru et i noirillo, ar ualme ista yasse apánieltes.”
Tá Péter *etemenne as i hyana hildo, ar lendette i noirinna.
Yúyo nér nornet, mal i hyana hildo norne linta lá Péter ar túle minya i noirinna,
ar luhtanes ar cenne i lanni caitala tasse, mal uas lende minna.
Tá Símon Péter túle, hilyala se, ar lendes mir i noire. Cennes i vaimar caitala tasse,
ar i lanne ya enge caryasse, lá caitala as i vaimar, mal tolúna satya nómesse.
Tá i hyana hildo, ye túle minya i noirinna, yando lende minna; ar cennes ar sáves.
An en uatte sinte i tehtele, i mauyane sen orta qualinillon.
Tá i hildor nanwenner marittannar.
Mal María tarne *nítala ara i noire. Ar íre *nítanes luhtanes yétien mir i noire,
ar cennes vala atta mi ninque háma, quén ara i cas ar quén ara i talu yasse Yésuo hroa náne caitienwa.
Quentette senna: “Nís, mana i casta yanen níteal?” Quentes túna: “Amápielte oa Herunya, ar uan ista yasse apánieltes.”
Quétala si quernes immo ar cenne Yésus tárala tasse, mal uas sinte i anes Yésus.
Yésus quente senna: “Nís, mana i casta yanen níteal? Man cesteal?” Intyala i anes i *tarwandur, quentes senna: “Heru, qui elye se-ocólie oa, nyara nin i nóme yasse apánielyes, ar inye se-mapuva oa.”
Yésus quente senna: “María!” Quérala immo, quentes senna Heveryasse: “Ravóni!” – ya tea *peantar.
Yésus quente senna: “Ávani appa, an en uan ortanie amba Atarinyanna. Mal mena hánonyannar ar queta téna: Ortean Atarinyanna ar Atareldanna, Ainonyanna ar Ainoldanna.”
María Mahtaléne túle ar tulune i vinyar i hildonnar: “Ecénien i Heru!”, ar i quentes nati sine senna.
I şinyesse aure yano, i minya ré i otsolo, íre i fennar náner pahte yasse i hilmor enger, ruciénen i Yúrallon, Yésus túle ar tarne mici te ar quente téna: “Raine na len!”
Apa quetie si tannes tien máryat ar etterya. Tá i hildor nér *alassie íre cennelte i Heru.
Yésus enquente téna: “Raine na len! Tambe i Atar ementie ní, síve inye menta lé.”
Ar apa quetie si şúyanes téna ar quente téna: “Cama i Aire Fea!
Qui apsenilde quenion úcari, nalte apsénine tien; qui tulcalde quenion úcari, nalte tulcaine.”
Mal Tomas, quén i yunqueo, estaina i Onóno, úme aselte íre Yésus túle.
Sie i hyane hildor quenter senna: “Ecénielme i Heru!” Mal quentes téna: “Qui uan cenuva máryatse i tehtar i taxion, ar uan panya ninya má etteryasse, lá savuvan.”
Auri tolto apa si hilmoryar enger ata i coasse, ar Tomas enge aselte. I fennar nér pahte, mal Yésus túle téna ar quente: “Raine na len!”
Tá quentes Tomanna: “Á panya leperelya sís, ar cena mányat, ar á menta málya, ar ása panya ettenyasse; áva na *savielóra, mal sávala.”
Tomas hanquente senna: “Herunya ar Ainonya!”
Yésus quente senna: “Ma savil ceniénen ni? *Alassie nar i uar ecénie ananta savir.”
Yésus carne yando rimbe hyane tannali epe i hildor, yar uar técine parma sinasse;
mal sine nar técine i savuvalde i Yésus ná i Hristo, i Eruion, ar i sávala samuvalde coivie esseryanen.
Apa nati sine Yésus apantane immo i hildoin ata ara Ear Tiverias; ar apantanes immo sie:
Enger uo Símon Péter ar Tomas, estaina i Onóno, ar Natanael Cánallo Alileo ar Severaio yondor ar exe atta hildoryaron.
Símon Péter quente téna: “Ménan raitien lingwili.” Quentelte senna: “Yando elme túlar aselye.” *Etemennelte ar lender mir i lunte, mal ter lóme yana raitanelte munta.
Mal íre arin túle, Yésus tarne i fárasse, ono i hildor uar hanyane i anes Yésus.
Tá Yésus quente téna: “Hinyar, ma ualde same lingwi?” Hanquentelte senna: “Penilme.”
Quentes téna: “Hata i rembe i foryasse i lunteo ar hiruvalde lingwili!” Sie hanteltes, ar tá ualte polde tucitas amba i nótenen lingwilíva.
Etta i hildo ye Yésus méle quente Péterenna: “Náse i Heru!” Sie Símon Péter, hlárala i anes i Heru, *quiltane immo vaccoryanen, an anes helda, ar hante immo mir i ear.
Mal i hyane hildor túler i luntenen – an úmelte haire nórello, er *os rangar tuxa – túcala i rembe lingwion.
Mal íre *hótarnelte nórenna cennelte caitea tasse *hyulmaruine ar lingwili caitala sanna, ar massa.
Yésus quente téna: “Á tulu lingwili ion raitanelde sí.”
Etta Símon Péter lende mir i lunte ar tunce i rembe nórenna, quanta haure lingwion nelde *lepenquean ar tuxa. Mal ómu anelte ta rimbe, i rembe úme narcaina.
Eque téna Yésus: “Tula, sama *arinwat!” *Úquen i hildoron veryane maquete senna: “Man elye?”, pan sintelte i anes i Heru.
Yésus túle ar nampe i massa ar antane sa tien, ar yando i lingwi.
Si náne i neldea lú yasse Yésus apantane immo i hildoin apa anes ortaina qualinillon.
Íre anelte mátienwe i *arinwat Yésus quente Símon Péterenna: “Símon Yohánion, ma melil ní ambe lá té?” Eque senna Péter: “Ná, Heru, istalye i nalye melda nin.” Quentes senna: “Á anta matso eulenyain!”
Ata eques senna, attea lússe: “Símon Péter, ma melilyen?” Quentes senna: “Ná, Heru, istal i nalye melda nin.” Eques senna: “Na mavar mámanyain!”
Quentes senna neldea lússe: “Símon Yohánion, ma nanye melda lyen?” Péter felle nyére pan quentes senna i neldea lússe: “Ma nanye melda lyen?” Sie eques senna: “Heru, elye ista ilqua; elye ista i nalye melda nin!” Yésus quente senna: “Á anta matso eulenyain.
Násie, násie quetin lyenna: Íre anel ambe nessa, *quiltanel immo ar vantane yasse mernel. Mal íre nauval yára, *eterahtuval mályanten ar exe lye-*quiltuva ar lye-tulyuva yanna ual mere.”
Si quentes tanien i nostale qualmeo yanen talumnes alcar Erun. Ar apa quetie si quentes senna: “Áni hilya.”
Íre quernes immo, Péter cenne i hildo ye Yésus méle hilyea – i quén ye i *şinyematse náne talta ambostyanna ar quente: “Heru, man ná ye *varta lye?”
Etta, íre Péter cenne se, quentes Yésunna: “Heru, mana nér sina caruva?”
Eque senna Yésus: “Qui şelmanya ná i lemyuvas tenna tulin, mana ta lyen? Elye áni hilya!”
Etta quetie sina *etemenne imíca i hildor: “Sana hildo ua qualuva.” Mal Yésus ua quente senna i uas qualumne, mal: “Qui şelmanya ná i lemyuvas tenna tulin, mana ta lyen?”
Sé ná i hildo ye *vetta pa si ar ye etécie nati sine, ar istalme i *vettierya nanwa ná.
Mal ear yando rimbe hyane natali yar Yésus acárie. Qui mo tecumne pa ilya erya nata, intyan i lá ea fárea nóme mí mar imma i parmain nála técine.
Minya quentanya, alcarinqua Şeofílo, tencen pa ilye i nati yar Yésus *yestane care ar peanta,
tenna i aure yasse anes mapana ama, apa antanes canwali i apostelin i cildes.
Tien yú tannes inse coirea rimbe tanwalínen apa perpéres, nála cénaina lo té ter auri *canaquean, cárala sinwe i nati yar apir Eruo aranie.
Ar omentieryassen aselte antanes tien i canwar: “Áva lelya oa Yerúsalemello, mal á lemya, hopila tan pa ya i Atar antane vandarya, ar pa ya hlasselde nillo.
An Yoháno sumbane nennen, mal elde nauvar sumbane Aire Feanen lá rimbe auri ho sí.”
Sí íre anelte ocómienwe, maquentelte senna: “Heru, ma envinyatal i aranie Israélen lúme sinasse?”
Quentes téna: “Ua len ista i lúmi ar lúr yar i Atar apánie véra túreryasse,
mal camuvalde túre íre i Aire Fea tuluva lenna; ar nauvalde astarmonyar mi Yerúsalem ar i quanda Yúrea ar Samária véla, ar cemeno anhaira rantanna.”
Ar apa quentes nati sine, lan tíranelte, anes ortaina, ar lumbo se-nurtane hendultalto.
Ar íre yentelte ama mir menel íre oantes, yé! nér atta mi ninqui larmali tarnet ara te,
ar quentette: “Neri Alileallo, manen ná i táralde yétala menel? Yésus sina, ye náne cámina ama lello mir i lumbo, entuluva sie, mí imya lé ya tirneldes lelyea mir menel.”
Tá nanwennelte mir Yerúsalem i orontello estaina Oron *Milloaldaron, ya hare ná Yerúsalemenna, *os *sendarelenda oa.
Tá, apa menie minna, lendelte ama mir i oromar yasse marnelte, Péter ar Yoháno ar Yácov ar Andréo, Filip ar Şomas ar Mattéo, Yácov Alféoion ar Símon i Sélot, ar Yúras Yácovion.
Illi mici te náner voronde hyamiesse, as nisseli ar María amil Yésuo, ar as hánoryar.
Sí yane auressen Péter oronte et endello i hánoron – i şánga náne *os queni *yúquean tuxa – ar quente:
“Neri, hánor, mauyane i Tehtelen náve carna nanwa, ta ya i Aire Fea quente nóvo ter Laviro anto pa Yúras, ye tanne i tie in namper Yésus.
An anes nótina mici vi ar camne ranta núromolie sinasse.”
(Nér sina ñente resta i *paityalénen úfailiéno, ar lantala hatsenen rúves i endesse, ar ilye hirdiryar uller et sello.
Ta olle sinwa illin i marner Yerúsalemesse, ar etta sana resta náne estaina lambeltasse Aceldama, ya tea: Resta Serceva.)
“An anaie técina i Parmasse Airelírion: 'Nai mardarya nauva lusta, ar nai *úquen maruva tasse', ar: 'Nómerya ortíriéno lava exen mapa.'
Etta mauya ven cile er i nerion i túler uo aselve ter i quanda lúme yasse i Heru Yésus lende minna ar ettenna mici vi,
i yestallo íre Yoháno sumbeáne ar tenna i aure yasse anes cámina ama vello. Quenen mici té mauya náve astarmo aselve pa enortierya.”
Ar tulyanelte ompa atta, Yósef estaina Varsappas, ye sáme i epesse Yusto, ar Mattías.
Ar hyamnelte ar quenter: “Elye, Héru ye ista illion endar, cara sinwa man icíliel
camien i núromolie ar nóme ve apostel yar Yúras hehtane menien véra nómeryanna.”
Ar hantelte *şanwali pa tu, ar i *şanwa tenge Mattías, ar anes nótina imíca i minque aposteli.
Íre i aure aşaro Otsolaron Otso náne tulinwa, anelte illi uo mi er nóme,
ar rincanen túle menello ran ve hlápula naraca súrio, ar quantes i quanda coa yasse engelte.
Ar lambali ve etsátaina náreva nemner tien, ar hamunes ilquende mici te.
Tá illi mici te náner quátine Aire Feanen ar *yestaner quete *alavéle lambelissen, ve i Faire antane tien carpa.
Ar enger Yerúsalemesse Yúrali márala tasse, *ainocimye neri, ilya nórello nu menel.
Sie, íre ran sina martane, i liyúme ocomne ar náne rucina, an ilquen mici te hlasse te quéta véra lamberyasse.
Anelte captaine ar mi elmenda, quétala: “Yé, ilye queni sine i quétar nar Alileallo, lá?
Ananta manen ná i hláralve, ilquen mici vi, véra lamberya yassen anelve nóne?
Queni ho Parşea ar Meria ar Elam, queni i marir mi Mesopotamia, Yúrea ar Capaprocia, Pontus ar Ásia,
ar Frihia ar Pamfylia, Mirrandor ar i ménar Livio, hare Cirénenna, ar i utúlier Rómallo, Yúrar ar quérinar véla,
queni ho Hréte ar Aravia – hláralvet quéta vére lambelvassen pa Eruo túre cardar!”
Anelte ara inte elmendanen ar hanyaner munta, quétala quén i exenna: “Mana ná nat sina?”
Mal exeli, yaiwesse téna, quenter: “Nalte quante lisse limpeo!”
Mal Péter oronte as i minque ar ortane ómarya ar quente téna: “Neri Yúreallo ar ilye elde i marir Yerúsalemesse, nai si nauva sinwa len, ar á lasta quetienyannar:
Queni sine é uar quante limpeo ve intyalde, an si i neldea lúme i aureo ná.
Úsie, si ná ya náne quétina ter Yoel i Erutercáno:
'Ar i métime auressen,' Eru quete, 'ulyauvan fairenyo ilya hrávenna, ar yondoldar ar yeldeldar quetuvar ve Erutercánor, ar nesse nerildar cenuvar mauri, ar yáre nerildain óluva olollínen,
ar yú hanwe ar inye mólinyannar ulyauvan fairenyo ente auressen, ar quetuvalte ve Erutercánor.
Ar antauvan tanwali menelde or le ar tannali cemende nún, serce ar náre ar híşie usqueva;
Anar oluva mornie ar Işil oluva serce, nó i Héruo túra ar meletya aure tuluva.
Ar ilquen ye yale i Héruo essenen nauva rehtana.'
Neri Israélo, hlara quettar sine: Yésus Násaretello, nér ye Eru panyane epe le taure cardalínen ar tanwalínen ar tannalínen yar Eru carne sénen endeldasse, ve elde istar,
nér sina, ve quén antana olla, Eruo tanca panonen ar i istyanen ya sámes nóvo, tancelde tarwesse úşanyaron mánen ar nacanter se.
Mal Eru ortane se lehtiénen qualmeo núti, an úne cárima qualmen hepitas.
An Lavir quete pa sé: 'Illume cennen i Héru epe hendunyat, an náse ara formanya, ni-pustien návello pálina.
Etta endanya náne valima ar lambanya quente pa túra alasse. Ente, yú hrávenya maruva estelde,
an ual hehtuva feanya Mandostosse; yú ual lavuva vorondalyan cene quelexima sóma.
Acáriel sinwe nin i tier coiviéva; ni-quantuval alassenen cendelelyallo.'
Neri, hánor, nin ná lávina quete lérave lenna pa i *atartur Lavir, i effirnes ar náne talana sapsaryanna, ar noirirya ea mici vi tenna aure sina.
Etta, pan anes Erutercáno ar sinte i Eru antane sen vanda pa panie mahalmaryasse quén yáveo oşweryo,
sámes apacen ar quente pa i enortave i Hristova, in únes hehtana Mandostosse; ente, hráverya ua cenne quelexima sóma.
Yésus sina Eru enortane, pa ya illi mici me nar astarmor.
Etta, pan anes ortana Eruo formanna ar camne i Aire Fea Ataryallo i vandanen ya hé antanelyane, et-ulyanes si ya cénalde ar hláralde.
An Lavir ua oronte menelenna, mal isse immo quete: 'I Héru quente herunyanna: Hara ara formanya,
tenna panyuvan ñottotyar ve tulco talutyant.'
Etta nai i quanda nosse Israélo istuva in Eru carne se Heru ar Hristo, Yésus sina ye tarwestanelde.”
Íre hlasselte ta anelte círine endaltasse, ar quentelte Péterenna ar i hyane apostelinnar: “Neri, hánor, mana caruvalme?”
Péter quente téna: “Hira inwis, ar nai ilquen mici le nauva sumbana Yésus Hristo essenen apsenien úcarildaiva, ar camuvalde i anna Aire Feava.
An elden i vanda ná, ar hínaldain ar in nar haire – illi i yaluva i Heru Ainolva insenna.”
Ar rimbe hyane quettalínen *vettanes tien ar hortane te, quétala: “Á na rehtane quarca *nónare sinallo!”
Etta i camner quettarya náner sumbane, ar yana auresse *os fear húmi nelde náner napánine.
Ar anelte voronde mí peantie i apostolion ar mí otornasse, i raciesse i massava ar i hyamiessen.
Rucie lantane ilquenna, ar rimbe elmendali ar tanwali martaner ter i aposteli.
Illi i sáver náner uo samiesse ilqua *alasatya,
ar vancelte armaltar ar i restar yar haryanelte ar etsanter i telpe illin, aiqueno maurenen.
Ar aure apa aure anelte i cordasse, nála er sámo, ar rancelte massa i coassen, matila mattalta mi alasse ar arwe poica endo,
laitala Eru ar arwe i lisseo i quanda lieo, lan ilya auresse i Heru napánane tien i náner rehtane.
Sí Peter ar Yoháno lender ama mir i corda i lúmesse hyamiéno, i nertea lúme.
Ar nér ye náne *úlévima amilleryo súmallo náne cólaina tar, ar panyaneltes ilya auresse ara i ando i cordo ya náne estaina i Mairea, arcieryan annar oraviéva ho i ménaner mir i corda.
Íre cennes Péter ar Yoháno lelyea i cordanna, arcanes tullo annar oraviéva.
Mal Péter, as Yoháno, yente senna ar quente: “Ámet yéta!”
Etta tances henyat tusse, sánala i camumnes nat tullo.
Mal Péter quente: “Telpe ar malta uan harya, mal ya é samin antan lyen: Essenen Yésus Hristo Nasaretello, á vanta!”
Ar nampeses formaryasse ar ortane se. Mí imya lú talluniryat ar i oxor imbi tál ar telco oller tulce,
ar campes ama ar vantane, ar lendes asette mir i corda, vantala ar cápala ar laitala Eru.
Ar i quanda lie cenne se vantea ar laitea Eru.
Ente, sintelte man anes, in isse náne i nér ye nóvo hamne camien annar oraviéva ara i Mairea Ando i cordasse, ar anelte quátine elmendanen ar náner ara inte yanen martanelyane sen.
Íre i nér himyane Péter ar Yoháno, i quanda lie, nála captaine, norner uo téna i nómesse estaina i Tarmatéma Solomondo.
Cénala ta, Péter quente i lienna: “Neri Israélo, manen ná i nalde elmendasse pa si, ar mana castalda yétien met ve qui véra melehtemmanen hya *ainocimiemmanen atyáriemmes vanta?
I Aino Avrahámo ar Ísaco ar Yácovo, atarelvaron Aino, acárie Yésus núrorya alcarinqua, sé ye elde antaner olla ar laquente epe Piláto cendele, ómu hé merne lerya se.
Elde laquenter i aire ar faila, ar arcanelde i camumnelde nér ye náne nahtar,
mal nacantelde i Turco coiviéva. Mal Eru ortane se qualinallon, pa ya illi mici me nar astarmor.
Etta, saviemmanen esseryasse, esserya acárie nér sina polda, sé ye yétalde ar ye istalde, ar i savie ya ea ter se ánie sen mále epe henduldat.
Ar sí, hánor, istan i carnelde sie peniesse istyava, ve turcoldar yú carner.
Mal sie Eru acárie nanwe i nati yar nóvo carnes sinwe ter ilye tercánoryaron anto: i Hristorya perperumne.
Etta hira inwis ar quera inde! Tá úcareldar nauvar *aupsárine, lavila lúmin ceutiéva tule i Héruo cendelello.
Sie mentauvas i Hristo sátina len, Yésus,
ye mauya menelen hepe tenna i lúmi envinyatáveva ilye nativa, pa yar Eru quente antonen airi tercánoryaron, yalúmesse.
É Móses quente: “I Héru Ainolda ortauva Erutercáno mici hánoldar, ve ní.
É ilya quen ye ua lasta sana Erutercáno nauva nancarna lieryallo.”
Ar ilye i Erutercánor, ho Samuel innar hilyaner se – illi i equétier – yú acárier sinwe auri sine.
Elde nar yondor i Erutercánoron ar i véreo ya Eru tulcane as atarildar, quétala Avrahamenna: 'Ar erdelyasse ilye i nossi cemeno nauvar aistane.'
Apa Eru ortane Yondorya, eldenna minyave se-mentanes aistien le, queriénen ilquen oa olce cardaldallon.”
Íre carampette i lienna, i hére *airimor ar i cordo hesto ar i Sanducear túler túna,
nála rúşie pan peantette i lien ar carnet sinwe i *enortale qualinallon Yésunen.
Ar panyanelte máltat tusse ar panyaner tu mandosse tenna i hilyala aure, an şinye náne tulinwa.
Mal rimbali imíca i hlasser i quetta sáver. Ar i nóte i nerion oronte tenna húmi lempe.
Túle i hilyala auresse i ocomner i turi ar i amyárar ar i parmangolmor Yerúsalemesse,
as Annas i héra *airimo ar Caiafas ar Yoháno ar Alexander ar illi i náner i nosseo i héra *airimo.
Apa panie tu endaltasse maquentelte: “Mana túrenen hya mano essenen carneste nat sina?”
Tá Péter, quanta Aire Feo, quente téna: “Turcor i lieo ar amyárar,
qui namme aure sinasse céşaine pa mára carda nimpa neren, ar merilde ista mannen nér sina acámie málerya,
nai nauva sinwa illin mici le ar i quanda lien Israélo i sá martane i essenen Yésus Hristo Nasaretello, ye elde tarwestaner mal Eru ortane et qualinaillon. Issenen nér sina tára sisse epe le málesse.
Si ná i ondo ya náne nótina ve munta lo elde i carastaner, ya olólie i vinco cas.
Ar rehtie ua ea aiquen hyananen, an yú ua antana hyana esse nu menel yanen mauya ven náve rehtane.”
Íre cennelte manen lérave Péter ar Yoháno quenter, ar sinter i anette *tengwalóre ar senwe neri, anelte quátine elmendanen, pan yú sinteltet ve queni i náner as Yésus.
Íre cennelte i nér ye tarne asette, sé ye camne mále, sámelte munta quetien ana tu.
Etta, apa canie tun lelya et i ocombello, carnelte úvie uo,
quétala: “Mana caruvalve sine nerin? An tanwa ya queni istar amartie ter tu, aşcénima illin i marir Yerúsalemesse, ar ua ece ven laquete sa.
Mal pustien sa návello vintana ambe palan i liesse, alve naitya tu, in ávatte quetuva ambe esse sinanen aiquenna.”
Ar apa yalie tu cannelte tun i mauyane tun aqua pusta quete ar peanta Yésuo essenen.
Mal hanquentasse Péter quente téna: “Qui faila ná Eruo hendusse lasta lenna ar lá Erunna, alde name inden!
Mal emme uat pole pusta quete pa yar ecéniemme ar ahláriemme.”
Apa ambe quentelte túna aice quettalínen lehtaneltet, pan ualte hirne casta paimetien tu, ar castanen i lieo, an illi mici te antaner alcar Erun pa ya martanelyane.
An i nér yesse tanwa sina nestiéva náne martienwa sáme amba lá loar *canaquean.
Apa anette lehtane lendette vérattannar ar nyarner tien i nati yar i hére *airimor ar i amyárar quentelyaner túna.
Hlárala ta, er sámanen ortanelte ómalta Erunna ar quenter: “Hér, elye ná ye carne menel cemenye ar yar ear tusse,
ar ye Aire Feanen quente ter i anto Lavir atarelmo, núrolya: 'Manen ná i nóreli amortar ar lieli sánear pa luste natali?
Cemeno arani atárier ar i cánor ócómier ve er, i Hérunna ar Hristoryanna.'
An é ocomnelte osto sinasse, Herol ar Pontio Piláto ar ennoli i nórion aire núrolyanna, Yésus, ye *lível,
carien yar mályanen ar cilmelyanen nóvo utulciel martumner.
Ar sí, Héru, á tunta *lurweltar, ar lava mólilyain quete quettalya ilya veriénen,
íre rahtal mályanen nestien, ar íre tanwali ar tannali martar essenen aire núrolyo, Yésus.
Ar apa quentelte i arcande, i nóme yasse anelte ócómienwe náne pálaina, ar ilquen mici te náne quátina i Aire Feanen ar quente Eruo quetta veriénen.
Ente, i liyúme ion sáver sámer er enda ar fea, ar *úquen mici te quente pa *aiqua ya sámes i ta náne vérarya, mal haryanelte ilye nati uo.
Ar túra túrenen i aposteli et-antaner *vettielta pa i enortave Yésus Hristova, ar túra Erulisse caine illinnar mici te.
É enge *úquen arwa maureo mici te, an illi i sámer restar hya coar vancer tai ar talle i telpe ya ñentelte i vácine natin
ar panyaner ta epe i apostelion talu. Tá mo etsate ilquenen ve maurelta náne.
Sie Yósef, ye i apostellon camne i epesse Varsappa – ya tea Tiutaleo Yondo – quén i Leviron, nóna Ciprusse,
ye sáme resta, vance sa ar talle i telpe ar panyane sa epe i apostelion talu.
Mal nér yeo esse náne Ananías, as Saffíra verirya, vance resta ya sámes
ar nuldave hempe ranta i telpeo ya ñentes, ve yú verirya sinte. Talles eryave ranta ar panyane sa epe i apostolion talu.
Mal Péter quente: “Ananías, manen ná i Sátan ánie lyen verie hurien i Aire Fean ar panya oa elyen ranta i telpeo ya ñentel i restan?
Lan haryanelyes, ma únes véralya? Ar apa anes vácina, ma ual en lertane cile mana carumnel? Mana castalya carien taite carda? Uhúriel, lá atanin, mal Erun!”
Íre hlasses quettar sine, Ananías lantane undu ar effirne. Ar túra rucie túle illinnar i hlasser sa.
Mal i nesse neri oronter, se-vaitaner lannelissen, ar coller se ettenna sapsaryanna.
Apa lúmi nelde verirya túle minna, lá istala ya martanelyane.
Péter quente senna: “Ánin nyare, ma vanceste i resta nonwe sinan?” Quentes: “Ná, ta náne i nonwe.”
Mal Péter quente senna: “Manen ná i aneste er sámo tyastien i Héruo faire? Yé! I talu ion taller verulya sapsaryanna nát epe i fenda, ar coluvalte lyé ettenna.”
Mí imya lú lantanes undu epe Pétero talu ar effirne. Íre i nesse neri túler minna, hirneltes qualina, ar colleltes ettenna ar antane sen sapsa ara verurya.
Etta túra rucie túle or i quanda ocombe ar or illi i hlasser pa nati sine.
Ente, ter i apostolion mát rimbe tanwali ar tannali martaner imíca i lie, ar illi mici te ocomner Solomondo tarmatémasse, nála er sámo.
*Úquen i exion veryane *erta inse téna, ananta i lie laitane te.
Ente, queneli i sáver i Herusse náner napánine, liyúmeli nelliva ar nisseliva véla.
Etta tallelte i hlaiwar ettenna ar panyaner te tasse pitye caimalissen ar tulmalissen, mérala rie i Péter, íre lahtanes, hatumne leo quenelinnar mici te.
Ar i liyúme i ostollon pelila Yerúsalem ocomne, cólala hlaiwali ar i náner nwalyaine lo úpoice fairi, ar ilquen mici te náne nestana.
Mal i héra *airimo ar i enger óse, i heren Sanducearon ya enge tá, oronter ar náner quátine *hrúcennen,
ar panyanelte mát i apostelinnar ar panyaner te i ostomandosse.
Mal i lómisse i Héruo vala pantane i mando andor, talle te ettenna ar quente:
“Á lelya, ar tárala i cordasse á quete i lienna ilye i quetier pa coivie sina!”
Apa hlasselte ta, lendelte árasse mir i corda ar peantaner.Íre i héra *airimo ar i enger óse túler, comyanelte i Tára Combe ar i quanda ocombe amyáraron i yondoron Israélo, ar mentaneltet i mandonna talien te.
Mal íre i cánor túler tar, ualte hirne te i mandosse. Apa nanwenie nyarnelte:
“I mando hirnelme pahta ilya varnassenen, ar i cundor tárala ara i fendar, mal íre latyanelme i mando ualme hirne aiquen i mityasse.”
Íre i cordo hesto ar i héra *airimo véla hlasser quettar sine, anelte útance pa nati sine ar pa mana martumne sina nattonen.
Mal nér túle ar nyarne tien: “Yé! I neri i panyanelde i mandosse nar i cordasse, tárala ar peantala i lien!”
Tá i hesto lende oa as cánoryar ar taller te, mal pen orme, pan runcelte návello *sartaine lo i lie.
Talleltet ar panyaner te epe i Tára Combe. Ar i héra *airimo quente:
“Antanelme len i canwa: Á pusta peanta esse sinasse! Ananta, yé! aquátielde Yerúsalem peantieldanen, ar merilde tala ner sino serce menna!”
Hanquentasse Péter ar i hyane aposteli quenter: “Mauya men hilya Eruo canwar or tai atanion.
Atarelvaron Aino ortane Yésus, ye elde nacanter lingiénen se aldasse.
Eru ortane se formaryanna ve Turco ar *Rehto, antaven inwis Israélen, ar apsenie úcariva.
Ar elme nar astarmoli pa nattor sine, ve ná i Aire Fea, ya Eru antane in lastar canwaryannar.”
Íre hlasselte ta, fellelte harne ar merner nahta te.
Mal enge nér ye oronte i Tára Combesse, Farisa yeo esse náne Amaliel, *şanyepeantar melda i quanda lien. Cannes: “Á tulya i neri i ettenna şinta lúmen.”
Tá quentes téna: “Neri Israélo, cima inde pa ya merilde care sine nerin.
An nó auri sine Şeuras oronte, quétala i anes valdea, ar nóte nelliva, *os tuxar canta, lender óse. Mal anesnahtana, ar illi i cimner canwaryar náner vintane ar nancarne.
Apa sé Yúras Alileallo oronte, mí rí yassen i lie náne nótina, ar tunces oa queneli apa inse. Ananta sana nér qualle, ar illi i cimner canwaryar náner vintane.
Ar ve nati tarir sí, quetin lenna: Lava sine nerin náve ar áva *tarasta te, an qui natto sina hya carie sina atanillon ná, nauvas nancarna,
mal qui Erullo nás, ualde poluva nancare te. Tira inde, i ualde nauva hírine mahtala Erunna!”
Tá cimnelte ya quentes. Apa tultave i aposteli rípeltet ar canner tien: “Ava quete mi esse Yésuo!” Tá lávelte tien mene.
Té, etta, oanter i Tára Combello mi alasse, pan anelte nótine valde perperien yaiwe i essen.
Ar ilya auresse, i cordasse ar i coassen, ualte pustane care sinwa i evandilyon pa Yésus Hristo.
Mal yane auressen, lan i nóte hildoron oronte, i Hellenyar náner nurrula i Heveryannar, pan *verulóraltar úner címine i ilaurea etsatiesse.
Etta i yunque yalder i hildoron liyúme intenna ar quenter: “Ua mára men hehta Eruo quetta etsatien matso i sarnossen.
Etta, hánor, á cesta mici inde neri otso i samir mára *vettie, quante faireo ar sailiéno, ar panyuvalmet topien maure sina,
mal elme en antauvar imme i hyamien ar i núromolien i Quetto.”
Ar ya quentelte náne mára i quanda liyúmen, ar cildelte Stefáno, nér quanta saviéno ar Aire Feo, ar Filip ar Procoro ar Nicanor ar Timon ar Parmenas ar Nicolao, *quernamo Antiocello,
ar panyaneltet epe i aposteli, ar apa hyamie panyanelte máltat tesse.
Ar Eruo quetta alle, ar i hildoron nóte oronte lintave Yerúsalemesse, ar hoa şanga *airomolíva panyaner inte nu i savie.
Mal Stefáno, quanta Erulisseo ar túreo, carne taure tanwali ar tannali imíca i lie.
Mal oronter nelli i *yomencoallo estaina Leryainaiva, ar ennoli i túler Cireneallo ar Alexandriallo, costien as Stefáno.
Mal ualte polde tare i sailienna ar i fairenna yainen carampes.
Tá nuldave carnelte nelli quete: “Ahlárielmes quéta naiquetiéli ana Móses ar Eru.”
Ar valtanelte i lie ar i amyárar ar i parmangolmor, ar lantanelte senna ar namper se, ar tulyaneltes mir i Tára Combe.
Ar yaldelte húrala astarmoli, i quenter: “Nér sina ua pusta quete quettar ana aire nóme sina ar ana i Şanye.
An ahlárielmes quéta i sina Yésus Nasaretello hatuva undu nóme sina ar vistauva i haimi yar acámielve Mósello.”
Ar íre illi i hámaner i Tára Combesse yenter se, cennelte i cendelerya náne ve valo cendele.
Mal i héra *airimo quente: “Ma nattor sine ear sie?”
Eques: “Neri, hánor ar atari, hlara! I Aino Alcaro tanne inse Avraham atarinyan lan anes Mesopotamiasse, nó marnes mi Ħáran,
ar quentes senna: 'Á auta nórelyallo ar nosselyallo ar tula i nómenna ya tanuvan lyen.'
Tá lendes et i nórello Caldeaiva ar marne mi Ħáran. Ar talo, apa atarya qualle, Eru tyarne se leve nóre sinanna yasse elde sí marir.
Ananta hé ua antane sen aryono ranta sasse, yando lá i cemen nu tallunirya, mal hé antane sen i vanda i sa-antauvanes sen harieryan sa, ar apa sé erderyan, lan en pennes hína.
Ente, sin quente Eru, in erderya marumne ve etyar ettelea nómesse, ar i lie carumner te móli ar tyarumner tien moia, ter loar tuxi canta.
Ar 'i nóre yan nauvalte móli inye namuva,' quente Eru, 'ar epeta etelelyuvalte ar caruvar núromolie nin nóme sinasse.'
Ar sen-antanes vére *osciriéva, ar sie ónes Ísac ar *oscirne se i toltea auresse, ar Ísac óne Yácov, ar Yácov i *nossetúri yunque.
Ar i *nossetúri, quante hrúceno Yósefenna, vancer se mir Mirrandor. Mal Eru enge óse,
ar etelehtanes se et ilye şangieryallon ar antane sen lisse ar sailie epe Fáro aran Mirrandoro. Ar hé panyane se turien Mirrandor ar quanda coarya.
Mal saicele túle i quanda nórenna Mirrandoro ar Canáano, é túra şangie, ar atarilvar uar hirne matta.
Mal Yácov hlasse i enge matta Mirrandoresse, ar mentanes atarilvar i minya lú.
Ar mí attea lú Yósef apantane inse hánoryain, ar Yósefo nosse náne carna sinwa Fáran.
Mentala menta, Yósef yalde Yácov atarya ar ilya nosserya, queni lempe *otoquean.
Yácov ununte mir Mirrandor. Ar effirnes, ve carner atarilvar,
ar anelte talane Hyecemenna ar aner panyane i noirisse ya Avraham ñente Ħamoro yondollon nonwen telpeo.
Íre i lúme túle hare i vandan ya Eru carne sinwa Avrahámen, i lie alle ar oronte túra nótenna Mirrandoresse,
tenna hyana aran oronte or Mirrandor, ye ua sinte Yósef.
Sé *yuhtanefinie atarilvannar ar carne ulco tien, mauyala te panya lapseltar ettenna, qualieltan.
Lúme yanasse Móses náne nóna, ar anes vanima Erun, ar anes hépina ter astari nelde coasse ataryava.
Mal íre anes panyana ettenna, Fáro yelde nampe se ar ortane se ve véra yondorya.
Ar Móses náne peantana mi ilya sailie Mirrandoro, ar anes taura mi quettaryar ar cardaryar.
Íre sámes loar *canaquean, túle mir endarya i yétumnes hánoryar, i Israelindi.
Ar íre cennes nér ye perpére úfailie, se-varyanes ar antane ahtarie yen perpére orme, petiénen undu i Mirra.
Intyanes i hánoryar hanyumner in Eru antauva tien rehtie máryanen, mal ualte hanyane sa.
Ar i hilyala auresse túles téna lan mahtanelte, ar néves ata tala te uo rainesse, quétala: 'Neri, nalde hánor! Manen ná i carilde úfailie quén i exenna?'
Mal ye náne úfaila armaroryan nirne se oa, quétala: 'Man carne lyé turco ar námo or met?
Cé telil nahta ni, ve nacantel i Mirra noa?'
Sina quettanen Móses norne oa ar marne Mirian·nóresse, yasse yondo atta nánet nóne sen.
Ar íre loar *canaquean náner vanwe, vala tannexe sen i ravandasse ara Oron Sinai, i uruite náresse neceltusso.
Íre Móses cenne sa, ya cennes quante se elmendanen. Mal íre lendes hare ce$ien, i Héruo óma túle:
'Inye atarilyaron Aino, Avrahámo ar Ísaco ar Yácovo Aino.' Tá palie nampe Móses, ar uas veryane ce$e ambe.
I Héru quente senna: 'Á mapa i hyapat talulyalto, an i nóme yasse táral aire talan ná.
É ecécien i ulco carna lienyan Mirrandoresse, ar ahlárien ñónielta, ar utúlien undu etelehtien te. Ar sí tula, lye-mentauvan Mirrandorenna.'
Móses sina ye quernelte oa, quétala: 'Man carne lyé turco ar námo?', sina nér Eru mentane ve turco ar *etelehto véla, mánen i valo ye tanne inse sen mí neceltussa.
Nér sina ettulyane te apa carnes tanwali ar tannali Mirrandoresse ar i Carne Earesse ar i ravandasse ter loar *canaquean.
Sin quente Móses Israelindinnar: 'Eru ortauva len et hánoldallon Erutercáno ve ní.'
Isse ná ye enge imíca i ocombe i ravandasse as i vala ye quente senna to Oron Sinai ar as atarilvar, ar camnes coirie quetier antaven ven.
Canwaryannar atarilvar váquenter lasta, mal nirneltes oa ar querner inte endaltasse Mirrandorenna,
quétala Áronna: 'Cara men ainoli i polir mene epe me! An Móses sina, ye me-tulyane et Mirrandorello – ualme ista mana amartie sen.'
Etta carnelte *mundolle yane auressen ar taller *yanca i cordonen, ar sámelte alasse máltato tamannen.
Mal Eru querne inse ar antane te olla *vevien menelo hosse, ve anaie técina Erutercánoron parmasse: 'Lau inyen antanelde annar ar *yancar ter loar *canaquean i ravandasse, Nosse Israélo?
Mal coldelde i *lancoa Moloqua ar i elen Réfan ainoldava, i emmar yar carnelde *tyerien tai. Etta mapuvan le oa, Vável pella.'
Atarilvar sámer i *lancoa *vettiéva i ravandasse, ve ye quente Mósenna canne i mo carumne sa, ve i emma ya Móses cenne.
Ar atarilvar i camner sa yú talle sa as Yosua mir i nóre haryaina lo i lier yar Eru et-hante epe atarilvar. Tasse sa lemne tenna Laviro auri.
Hirnes lisse epe Eru ar arcane in isse antauva Yácovo Ainon nóme marien.
Mal Solomon náne ye carastane sen coa.
Ananta i Antara ua mare coassen cárine mainen, ve i Erutercáno quete:
'Menel mahalmanya ná, ar cemen tulco talunyant. Mana nostaleo coa carastuvalde nin, quete i Héru, hya mana nómenya serien?
Hya mánya úne ya carne ilye nati sine?'
A nornar, *úoscírine mi enda ar hlaru, tarilde illume i Aire Feanna – tambe atarildar, síve yú elde!
Man imíca i Erutercánor atarildar uar roitane? Ar nacantelte i nóvo carner sinwa tulesse i Failo, ye sí ánielde olla ar anahtielde,
elde i camner i Şanye ve antana ter vali, mal ualde ehépie sa.”
Íre hlasselde nati sine anelte círine endaltasse, ar múlelte nelciltar senna.
Mal isse, quanta Aire Feo, yente mir menel ar cenne Eruo alcar, ar Yésus tára ara Eruo forma.
Ar eques: “Yé, cenin menel pantana ar i Atanyondo tára ara Eruo forma!”
Mal yámelte taura ómanen ar tamper hlarultat, ar lendelte rimpe uo senna.
Ar apa hanteltes et i ostollo hantelte sarneli senna. Ar i astarmor panyaner oa lanneltar ara nessa nér yeo esse náne Saul.
Ar hantelte sarneli Stefánonna lan hyamanes ar quétane: “Heru Yésus, cama fairenya!”
Tá lantanes occaryanta ar yáme taura ómanen: “Heru, áva note úcar sina ana te!” Ar apa quetie ta, effirnes.
Saul $áquente i nahtie séva.Yana auresse túra roitie oronte i ocombenna ya enge Yerúsalemesse; illi hequa i aposteli náner vintane ter i ménar Yúreo ar Samário.
Mal nelli i runcer Erullo colder Stefáno sapsaryanna, ar carnelte túra nainie sen.
Ono Saul lantane i ocombenna mi ita naraca lé. Lelyala mir coa apa coa ar tucila ettenna neri ar nissi véla, hanteset mir mando.
Mal i náner vintane lender ter i nóre, cárala sinwa i evandilyon pa i Quetta.
Filip lende undu mir Samário osto ar carne i Hristo sinwa tien.
Er sámanen i şangar cimner i nati quétaine lo Filip lan lastanelte ar tirner i tanwar yar carnes.
An enger rimbali i sámer úpoice fairi, ar té etelender yámala taura ómanen. Ente, rimbali i náner *úlévime ar tapte náner nestane.
Etta enge olya alasse osto tanasse.
Mal enge i ostosse nér estaina Símon, ye nóvo carne ñúle ar cápe Samário nóre, quétala pa inse i anes túra quén.
Ar illi mici te, i ampityallo i antúranna, cimner se ar quenter: “Nér sina Eruo túre ná, ya ná estaina Túra.”
Etta cimneltes, pan ter anda lúme quantelyanes te elmendanen ñúleryanen.
Mal íre sávelte i quetier Filipo, ye carne sinwe Eruo aranie ar Yésus Hristo esse, anelte sumbane, neri ar nissi véla.
Yú Símon sáve, ar apa náve sumbana lemnes as Filip ar náne quátaina elmendanen íre tirnes i tanwar ar taure cardar yar martaner.
Íre i aposteli Yerúsalemesse hlasser i Samária camnelyane Eruo quetta, mentanelte Péter ar Yoháno téna.
Lendette undu ar hyamnet camieltan Aire Fea.
An en uas lantane téna, mal anelte rie sumbane mí esse i Heru Yésuo.
Tá panyanette máttat tesse, ar camnelte Aire Fea.
Sí íre Símon cenne manen i faire náne antaina paniénen mát, panyanes telpemittali epe te,
quétala: “Á anta yú inyen túre sina, in aiquen yesse panyan mányat camuva Aire Fea.”
Mal Péter quente senna: “Nai telpelya ar elye nauvat nancarne uo, pan sannel i haryumnel Eruo anna telpen!”
Ualye same ranta hya masse mi natto sina, an endalya ua téra epe Eru.
Etta hira inwis pa olcie sina, ar á arca i Herullo in endalyo sanwe nauva apsénina lyen,
an cenin i nalye hloirea *sála ar naxa úfailiéva.”
Hanquentasse Símon quente: “Alde arca i Herullo nin, pustien i nati pa yar quentel tuliello ninna.”
Sie, apa antanelte quanta *vettie ar quenter i Heruo quetta, nanwennelte Yerúsalemenna, ar carnelte i evandilyon sinwa rimbe mardolissen i Samáreaiva.
Mal i Héruo vala quente Filipenna, quétala: “Á orta ar mena hyarmenna i mallenna ya mene undu Yerúsalemello mir Ása.” Ta i ravandasse ná.
Ar orontes ar lende, ar yé! tasse túle arandur Etopiallo, taura nér nu Candáce, tári i lieo Etiopio. Lendelyanes Yérusalemenna *tyerien,
mal nanwennes ar hamne norolleryasse hentala Isaia i Erutercáno.
Ar i Faire quente Filipenna: “Mena hare i norollenna ar ása himya!”
Filip norne ara sa ar hlasse se hentea Isaia i Erutercáno, ar eques: “Ma nanwave hanyal mana henteal?”
Eques: “Manen ecuva nin hanya ire penin aiquen ye tanuva nin mana teas?” Ar arcanes Filipello i tulumnes mir i norolle hamien óse.
Mal i ranta i tehteleo ya hentanes náne si: “Ve máma anes talaina i nahtienna, ar ve eule ye ómalóra ná epe ye hocire tórya, uas pantane antorya.
Nucumieryasse i námie náne mapana oa sello. Pa *nónarerya man nyaruva? An coivierya anaie mapana oa cemello.”
Hanquentasse i arandur quente: “Maquetin lye, pa man i Erutercáno quete si? Pa inse hya pa hyana quén?”
Filip pantane antorya, ar *yestala tehtele sinallo carnes sinwa sen i evandilyon pa Yésus.
Sí íre lendette i mallesse, túlette nómenna yasse enge nén, ar i arandur quente: “Ela, nén! Mana pusta ni návello sumbana?”
Filip hanquente: “Qui savil quanda endalyanen, ta pole marta.”
Ar cannes i norollen hauta, ar yúyo lendet mir i nén, Filip ar i arandur uo, ar sumbanéses.
Apa túlette ama i nenello, i Héruo faire lintave tulyane Filip oa, ar i arandur ua cenne se ambe, an hé lende malleryasse mi alasse.
Mal Filip náne hírina Asotosse, ar lendes ter i ména ar carne i evandilyon sinwa ilye i ostoin tenna rahtanes Cesareanna.
Mal Saul, en şúyala *lurwe ar nahtie i Heruo hildonnar, lende i héra *airimonna ar arcane sello mentar i *yomencoain Lamascusse,
talieryan nútina Yerúsalemenna aiquen ye hirnes náne i Malleo, neri ar nissi véla.
Lendaryasse, íre túlanes hare ana Lamascus, rincanen caltane *os se cala menello,
ar apa lantie i talamenna hlasses óma quéta senna: “Saul, Saul, mana castalya roitien ni?”
Eques: “Man nalye, Heru?” Eques: “Inye Yésus, ye elye roita!
Mal á orya ar tula mir i osto, ar ya mauya lyen care nauva nyárina lyen.”
I neri i lender óse tarner úpe, an hlasselte i óma, mal uar cenne aiquen.
Mal Saul oronte i talamello, ar ómu henyat nánet pante cennes munta. Etta tulyaneltes i mánen ar taller se mir Lamascus.
Ar ter rí nelde cennes munta, ar uas mante hya sunce.
Enge Lamascusse hildo yeo esse náne Ananías, ar i Heru quente senna mi maur: “Ananías!” Eques: “Sisse ean, Heru.”
I Heru quente senna: “Á orya, mena i mallenna estaina Téra, ar mi coarya Yúras á cesta nér estaina Saul, ho Tarsus. An ye! hyámas,
ar ecénies mi maur nér estaina Ananías túla minna ar panyea máryat sesse, cenieryan ata.”
Mal Ananías hanquente: “Heru, ahlárien rimbalillon pa nér sina, pa ilye i olce nati yar carnes airilyain Yerúsalemesse.
Ar sisse acámies túre i hére *airimollon nutien illi i yalir esselyanen.”
Mal i Heru quente senna: “Mena, an nér sina ná nin cílina tamma, colien essenya i nórennar ar arannar ar Israelindinnar véla.
An inye tanuva sen ilqua ya mauyuva sen perpere essenyan.”
Tá Ananías lende, ar túles mir i coa, ar panyanes máryat sesse ar quente: “Saul, háno – i Heru, i Yésus ye tanne inse lyen i mallesse yasse túlel, ni-ementaye cenielyan ata ar návelyan quátina Aire Feo.”
Ar mi imya lú lantane henyalto ya nemne ve hyalmali, ar poldes ata cene, ar orontes ar náne sumbana.
Tá mantes ar náne turyana.Rélissen lemnes as i hildor Lamascusse,
ar pen hopie carnes Yésus sinwa i *yomencoassen, in isse i Eruion ná.
Mal illi i hlasser se náner quátine elmendanen ar quenter: “Ma nér sina ua ye Yerúsalemesse nancarne illi i yalir sina essenen, ar ye túle sir i ennen i tulyauvanes te nútine i hére *airimonnar?”
Mal Saul náne turyaina, ar carnes i Yúrar i marner Lamascusse aqua rúcine taniénen i Yésus ná i Hristo.
Apa fárea nóte rélion i Yúrar carner úvie uo, pa manen nancarumneltes.
Mal panolta senna olle sinwa Saulen. Ono yú tirnelte i andor harive, auresse yo lómisse véla, nancarien se.
Etta hildoryar namper se ar mentaner se undu *rappanen *vircolcasse ter assa i rambasse.
Íre túles Yerúsalemenna néves *erta inse i hildoin, mal illi runcer sello, pan ualte sáve i anes hildo.
Mal Varnavas nampe se ar talle se i apostelinnar, ar nyarnes tien ilqua pa manen i mallesse cennes i Heru, ar i hé quente senna, ar manen Lamascusse quentes veriénen mi Yésuo esse.
Ar lemnes aselte, lelyala minna ar ettenna Yerúsalemesse, quetila veriénen mí Heruo esse,
carpala ar costala as i Yúrar i quenter Hellenya. Mal té carner panoli nancarien se.
Íre i hánor túner ta, talleltes undu Césareanna ar mentaner se oa ana Tarsus.
Sí i ocombe mi quanda Yúrea ar Alilea sáme raine ar náne carastaina ama, ar lan vantanelte ruciesse i Herullo ar mí Aire Feo tiutale, nótelta oronte.
Sí íre Péter lende ter ilye ménar túles undu yú i airinnar i marner Lirdasse.
Tasse hirnes nér estaina Éneas, ye cainelyane tulmaryasse loassen tolto, pan anes *úlévima.
Ar Péter quente senna: “Éneas, Yésus Hristo nesta lye. Á orya ar á *parta caimalya!” Ar oronyes mí imya lú.
Ar illi i marner mi Lirda ar Hyaron cenner se, ar querneltexer i Herunna.
Mal Yoppasse enge *hilde yeo esse náne Tavişa, ya tea Lorcas. Anes quanta máre cardaron ar annaron oraviéva yar carnes.
Mal ente auressen olles hlaiwa ar qualle. Sóveltes ar panyaner se i oromardesse.
Pan Lirda náne hare Yoppanna, íre i hildor hlasser i Péter enge osto tanasse mentanelte nér atta senna arcien sello: “Áva na telwa tuliesse menna!”
Tá Péter oronte ar lende aselte. Ar íre túles tar, tulyaneltes mir i oromar, ar ilye i *verulórar tarner óse nírelissen, tánala sen rimbe laupeli ar collali yar Lorcas carnelyane íre en enges aselte.
Mal Péter mentane illi ettenna, lantane occaryanta ar hyamne. Tá, apa querie inse i hroanna, quentes: “Tavişa, á orya!” Hé pantane henyat, ar cénala Péter hé hamne ama.
Péter antane hén márya ar ortane hé, ar yalles i airi ar i *verulórar ar panyane hé epe te coirea.
Natto sina olle sinwa illin Yoppasse, ar rimbali sáver i Herusse.
Fárea nótesse rélion lemnes Yoppasse as nér estaina Símon, *alumo.
Enge nér Césareasse estaina Cornelio, *tuxantur i hosseranto estaina i Itála,
ye náne *ainocimya ar runce Erullo as quanda nosserya, ar antanes rimbe annali oraviéva i lien ar hyámane Erunna illume.
*Os i nertea lúme cennes mi maur Eru vala túla minna senna ar quéta senna: “Cornelio!”
I nér yente se, ar nála ruhtaina quentes: “Mana meril, Heru?” Quentes senna: “Hyamielyar ar annalyar oraviéva orontier ve enyalie epe Eru.
Ar sí, á menta nelli Yoppanna ar yala quén estaina Símon, ye same i epesse Péter.
Nér sina mare coasse quenwa yeo esse Símon ná, *alumo, ye same coa ara i ear.”
Apa i vala ye quente senna lende oa, yaldes atta i núroron coaryo ar *ainocimya ohtar ho imíca i lemner óse,
ar nyarnes ilqua tien ar te-mentane Yoppanna.
I hilyala auresse, íre engelte lendaltasse ar túlaner hare i ostonna, Péter lende ama i tópanna *os i enquea lúme, hyamien.
Mal anes maita ar merne mate. Lan manwanelte, lendes mir maur
ar cenne menel pantana ar nostaleo vene ya túle undu ve hoa talat *páşeva,
ar sasse enger ilye nostaleron *cantalyar ar hlicilar ar aiwi menelo.
Ar óma túle senna: “Á orta, Péter, á nahta ar á mate!”
Mal eque Péter: “Laume, Heru, an munta vahtana ar úpoica oi amátien!”
Ar i óma quente senna attea lú: “Áva esta vahtane yar Eru opoitie!”
Si martane neldea lú, ar tá i vene náne mapana mir menel.
Íre Péter en náne ita iltanca pa mana i maur ya cennelyanes tenge, yé!
i neri mentane lo Cornelio tarner epe i ando apa maquetie tielta i coanna Símonwa.
Lan Péter sanne pa i maur, i faire quente: “Yé! Neri nelde lye-cestear.
Mal mena undu ar áte hilya, ar áva na iltanca pa *aiqua, an inye ementaye te.”
Sie Péter ununte ar quente i nerinnar: “Yé, inye ná ye cestealde. Mana castalda tulien sir?”
Quentelte: “Cornelio i *tuxantur, nér faila ar *ainocimya, pa ye i quanda nóre Yúraron quetir mai, acámie canwa ter aire vala i mentauvanes elyen, tulielyan coaryanna ar hlarieryan quetielyar.”
Tá arcanes tello tule minna, ar camneset ve *naşali.I hilyala auresse orontes ar lende oa aselte, ar ennoli i hánoron Yoppasse lender óse.
Er ré epeta lendes mir Césarea. Cornelio, hópala tulieltan, nóvo yalde uo i queni nosseryo ar hari meldoryar.
Íre Péter lende minna, Cornelio velde se, lantane undu epe talyat ar *tyerne se.
Mal Péter ortane se, quétala: “Á orta; yando inye atan ná!”
Ar íre quentes óse, lendes minna ar hirne rimbe ennoli ocómienwe.
Ar quentes téna: “Istalde mai manen Yúran ná aqua ana i Şanye *erta inse quenen hyana nóreo, hya mene mir coarya. Ananta Eru tanne nin i ávan esta aiquen vahtana hya úpoica.
Etta é túlen, ú quetiéno ana yalielya íre anen tultaina. Etta maquetin: Mana castalya mentaven nin?”
Ar Cornelio quente: “Auri canta yá, nótala lúme sinallo, hyamnen coanyasse íre yé! nér arwa calima larmo tarne epe ni
ar quente: “Cornelio, hyamielya anaie hlárina, ar annalyar oraviéva anaier enyáline epe Eru.
Etta á menta Yoppanna ar yala Símon, ye same i epesse Péter. Nér sina ná *naşal coasse hyana Símonwa, *alumo, ara i ear.”
Etta mí imya lúme mentanen lyenna, ar carnel mai tuliénen sir. Ar sie lúme sinasse illi mici me nar sisse epe Eru, hlarien ilye i nati yar i Heru acánie lyen quete.”
Tá Péter pantane antorya ar quente: “Nanwave tuntan i Eru ua cime cendeler,
mal ilya nórello camis mai i quén ye ruce sello ar care failie.
Mentanes i quetta Israelindinnar carien sinwa tien i evandilyon raineva ter Yésus Hristo; isse Heru illion ná.
Istalde i quetie ya lende ter i quanda Yúrea, *yestala Alileallo apa i sumbie pa ya Yoháno carampe –
pa Yésus Násaretello, manen Eru se-*líve Aire Feanen ar túrenen, ar lendes ter i nóre, cárala mánar ar nestala illi innar i Arauco túre lumnane, an Eru náne óse.
Ar illi mici me nar astarmoli i nation yar carnes mí Yúraron nóre ar mi Yerúsalem véla, mal yú nacanteltes lingiénen se aldasse.
Sé Eru ortane i neldea auresse ar láve sen tana inse,
lá i quanda lien, mal astarmolin cíline lo Eru – elmen, i manter ar suncer óse apa ortaverya et qualinallon.
Ar cannes men care sinwa i lien, ar anta tien *vettie, i nér sina náne ye Eru apánie venámo coirearon ar qualinaron.
Pa sé ilye i Erutercánor quenter – in ilquen ye save sesse came apsenie úcariva esseryanen.”
Lan Péter en quétane pa nati sine, i Aire Fea lantane illinnar i hlasser i quetta.
Ar i vorondar i náner tulinwe as Péter, té i náner i *osciriéva, náner quátine elmendanen pan i anna Aire Feava náne ulyaina yando Úyúrannar.
An hlasseltet quéta lambalínen ar laitea Eru. Tá Péter quente:
“Ma aiquen pole váquete ten i nén, pustien návello sumbane neri sine i acámier i imya Aire Fea ya yando elve acámier?”
Ar cannes tien náve sumbane Yésus Hristo essenen. Tá arcanelte sello i lemyumnes aselte ter aureli.
Sí i aposteli ar i hánor i enger Yúreasse hlasser in Úyúrali yú camner Eruo quetta.
Mal íre Péter lende ama Yerúsalemenna, i queni *oscíriéva có$er óse,
quétala i lendes mir i coa neriva i úner *oscírine ar mante aselte.
Tá Péter *yestane nyare tien ilqua ve martanelyanes, quétala:
“Engen Yoppa·ostosse íre cennen maur, nostaleo vene ya túle undu ve hoa talat *páşeva, mentaina undu menello vincaryainen canta, ar túles hare ninna.
Yétala mina sa tirnen ar cenne cemeno *cantalyar ar i hravani celvar ar i hlicilar ar menelo aiwi.
Yú hlassen óma quéta ninna: Á orta, Péter, á nahta ar mate!
Mal quenten: Laume, Heru, an munta vahtana hya úpoica oi utúlie mir antonya!
Attea lú i óma menello hanquente: Áva esta úpoice yar Eru opoitie!
Si martane neldea lú, ar ilqua náne túcina nan mir menel.
Ar yé! mi yana lú neri nelde tarner ara i coa yasse anelme, an anelte mentane Césareallo ninna.
Etta i faire canne nin mene aselte, lá nála iltanca pa *aiqua. Mal hánor enque sine yú lender asinye, ar lendelme mir i coa i nerwa.
Nyarnes men manen cennes i vala tára mi coarya, quétala: Á menta nelli Yoppanna ar á tulta Símon ye same i epesse Péter,
ar quetuvas lyenna quetiéli yainen nauval rehtana, elye ar nosselya.
Mal íre inye *yestane quete, i faire lantane téna aqua ve lantanes yú venna i yestasse.
Tá enyalden i Heruo quetie, manen quentes: Yoháno sumbane nennen, mal elde nauvar sumbane Aire Feanen.
Etta, qui Eru antane i imya anna tien ve elven i asávier mí Heru Yésus Hristo, man inye ná, i poluvan pusta Eru?”
Sí íre hlasselte nati sine, quentelte munta amba ana se, ar antanelte alcar Erun, quétala: “Tá Eru é ánie inwis tulyala coivienna yú Úyúrain.”
Etta i náner vintane i şangiénen ya oronte pa Stefáno lender tenna Fenícia ar Ciprus ar Antioc, mal quentelte i quetta rie i Yúrannar.
Mal mici te enger nelli ho Ciprus ar Ciréne i túler Antiocenna ar caramper pa i evandilyon i Heru Yésuo yú innar quenter Hellenya.
Ente, i Heruo má enge aselte, ar hoa nóte sáver ar querner inte i Herunna.
I nyarna pa te túle i hlarunna i ocombeo ya enge Yerúsalemesse, ar mentanelte Varnavas tenna Antioc.
Íre túles tar ar cenne Eruo lisse, sámes alasse ar hortane te holmo lemya Eruo indómesse,
an anes mane nér ar quanta Aire Feo ar saviéno. Ar hoa şanga náne napánina i Herun.
Tá lendes oa Tarsusenna cestien Saul,
ar apa hirnes hé, tulyanes hé Antiocenna. Sie túle i ter quanta loa ocomnette aselte i ocombesse ar peantanet hoa şanga, ar Antiocesse i hildor náner mí minya lú estaine *Hristonduri.
Yane auressen túler Erutercánoli undu Yerúsalemello Antiocenna.
Quén mici te, yeo esse náne Ahavo, oronte ar carne sinwa i Fairenen i tulumne túra saicele i quanda ambarenna. Ta é túle mi réryar Claurio.
Etta i sámer amba lá maurelta cilder menta mána i hánonnar i marner Yúreasse.
Ar sie carnelte, mentala sa i amyárannar Varnavas ar Saulo mánen.
*Os yana lúme Herol i Aran panyane mát ennolinnar i ocombeo, carien tien ulco.
Nacantes Yácov, hánorya Yoháno, macilden.
Cénala manen i Yúrar sanner mai pa ta, yú nampes Péter. Ta martane auressen *alapulúne massaron.
Apa nampes hé, panyanes he mandosse, antala he mannar combion canta, ilya arwa ohtarion canta, an teldes tana he i lien apa i Lahtie.
Sie Péter náne hépina mandosse, mal i quanda ocombe veassenen hyámane Erunna sen.
I lómisse nó Herol se-tulyumne epe i lienna, Péter náne lorna, nútina naxenen atta imbe ohtar atta, ar cundoli epe i fenda hemper i mando.
Mal yé! i Heruo vala tarne ara se, ar cala caltane i şambesse. Támala Pétero hroa hé eccoitane se, quétala: “Á orta lintave!” Ar naxeryar lantaner máryalto.
I vala quente henna: “Á panya qualtalya *os oşwelya ar nuta hyapalyat talulyatse!” Carnes sie. Ar hé quente senna: “Á panya collalya hroalyasse ar áni hilya!”
Ar lendes ettenna ar hilyane hé, mal uas sinte i ya martane i valanen náne nanwa. Intyanes i cennes maur.
Lahtala i minya cundo, ar i attea, túlette i angaina andonna tulyala mir i osto, ar ta pantanexe tun. Ar apa lendette ettenna vantanette undu er malle, ar mi yana lú i vala oante sello.
Ar Péter, hírala inse, quente: “Sí istan i mentane i Heru valarya ar ni-etelehtane ho Herolo má ar ilquallo ya i Yúralie hópa.”
Apa carie úvie lendes coanna Maríava, amil Yoháno ye sáme i epesse Marco. I coasse fárea nóte queniva náner ocómienwe, hyámala.
Íre tambanes ana i fenna, inya mól yeo esse náne Rola túle latien,
ar íre *atsintes Pétero óma, alasseryanen uas latyane i ando, mal nornes minna ar nyarne i Péter tára epe i ando.
Quentelte senna: “Nalye ettesse sámalyo!” Mal quentes veassenen i sie ea. Quentelte: “Valarya ná.”
Mal Péter lemne yasse enges, tambala. Íre latyanelte, cenneltes ar náner captane.
Mal carnes tien hwerme máryanen, náveltan quilde, ar nyarnes tien ilqua pa manen i Heru se-tulyane et mandollo, ar eques: “Nyara nati sine Yácoven ar i hánoin.” Tá mennes ettenna ar lende hyana nómenna.
Íre aure túle, ua enge pitya valme imíca i ohtari, pa mana nanwave náne martienwa Péteren.
Herol se-cestane, ar íre uas hirne se, cannes i mo tulyumne i ohtari oa paimen. Ar lendes undu Yúreallo Césareanna ar lemne tasse.
Sámes túra rú$e i lienna mi Tír ar Síron. Ve er túlelte senna, ar carampelte as Vlasto, i turco i caimaşambo i aranwa, ar ñenter penastarya. Tá arcanelte raine, an nórelta camne mattarya ho ta i aranwa.
Mal sátina auresse Herol tumpexe arna larmanen ar hamne undu i sondasse namiéva ar carampe téna.
Tá i lie yáme: “Aino óma, ar lá atano!”
Mí imya lú i Héruo vala pente se, pan uas antane i alcar Erun. Anes mátina lo leucali ar effirne.
Mal i Heruo quetta alle ar vintane.
Varnavas ar Saul nanwenner Yerúsalemenna apa telyanette núromolietta, ar tallette Yóhano, ye sáme i epesse Marco.
Enger Antiocesse Erutercánoli ar *peantalli i ocombesse ya enge tasse, Varnavas ar Símeon ye náne estaina Niher, ar Lucio Cirenello, ar Manaen ye náne peantana as Herol i *canastatur, ar Saul.
Lan cáranelte núromolie i Herun ar hépaner lamate, i Aire Fea quente: “Ánin sate Varnavas ar Saul, i molien yanna yaldenyet.”
Tá hempelte lamate ar hyamner ar panyaner máltat tunna ar láver tun lelya.
Etta neri sine, mentane lo i Aire Fea, lender undu mir Seleucia, ar talo cirnette oa Ciprusenna.
Ar íre túlette mir Salamis carnette Eruo quetta sinwa i Yúraron *yomencoassen. Yando sámette Yoháno asette, manyala tu.
Íre lendelte ter i quanda tol tenna Pafos, veldelte nér ye náne sairon, hurutercáno.
Enges as Serhio Paulo, i nórecáno, ye náne handa nér. Apa yaldes Varnavas yo Saul insenna, nér sina cestane hlare Eruo quetta.
Mal Elimas i sairon – an ta ná ya esserya tea – tarne tunna, cestala quere i nórecáno oa i saviello.
Saul, ye ná yú Paulo, náne quátina Aire Feanen, yente se
ar quente: “A nér quanta ilya nostaleo huruo ar ilya olca cardo, a yondo i Arauco, a ñotto ilyo ya faila ná, ma ual pustuva rice i Heruo tére maller?
Ar sí, yé, i Héruo má ná lyenna, ar nauval laceníte, lá cénala i áre ter lúme.” Mí imya lú híşie ar mornie lantaner senna, ar lendes cestala ennoli tulien se i mánen.
Tá i nórecáno, íre cennes ya martanelyane, sáve; ar anes elmendasse i Heruo peantiénen.
I neri i enger as Paulo sí lender ciryanen Pafosello ar túler Peryanna Pamfiliasse. Mal Yoháno hehtane te ar nanwenne Yerúsalemenna.
Ono tú, apa oantette Peryallo, túler Antiocenna Pisiriasse. Lendette mir i *yomencoa i *sendaresse ar hamunet.
Apa i et-hentie i Şanyeva ar i Erutercánoiva, i *yomencoanturi mentaner túna, quétala: “Neri, hánor, qui samiste quetta tiutaléva i lien, ása nyare!”
Ar Paulo oronte, ar cárala hwerme máryanen quentes: “Neri, Israelyar ar elde i rucir Erullo, á lasta!
Israel nórelvo Aino cilde atarilvar ar carne lielva túra lan máranelte ve aianor Mirrandoresse; tá taura rancunen te-tulyanes et talo,
ar ter loar *os *canaquean antanes tien matta i ravandasse.
Apa nancarnes lier quean mi Canáan, tyarneset harya nórelta
loain *os *lepenquean ar tuxar canta. Epeta antanes tien námoli, tenna Sámuel i Erutercáno.
Tá arcanelte aran, ar Eru antane tien Saul Císion, nér nosseo Venyamíno. Apa loar *canaquean
panyanes hé oa ar ortane Lavir Yessaion náven aran, pa ye yú quentes ve *vettie: Ihírien Lavir Yessaion nér ve endanya, ye caruva quanda indómenya.
Nér sino erdello, Eru vandaryanen mentane *Rehtando Israelenna, Yésus,
apa Yoháno, nó tulierya, carne sumbie inwisteva sinwa i quanda lien Israélo.
Mal íre Yoháno ron telyumne normerya, quentes: Man intyalde i nanye? Uan sé. Mal yé, apa ni túla quén yeva hyapat inye ua valda lehtien!
Neri, hánor, yondor nosseo Avrahámo ar i queni mici le i rucir Erullo: Elmenna i quetta rehtie sino anaie mentana.
An i marir Yerúsalemesse ar turcoltar uar sinte quén sina, mal íre namneltes carnelte nanwe i Erutercánoron quetier, yar nar et-hentaine ilya *sendaresse,
ar ómu ualte hirne casta qualmeo, arcanelte Pilátollo i mo nahtumne se.
Apa telyanelte ilye i nati técíne pa se, nampeltes undu i aldallo ar panyaner se noirisse.
Mal Eru ortane se et qualinallon,
ar ter rimbe réli anes cénaina lo i lendelyaner óse Alileallo Yerúsalemenna, i nar sí astarmoryar i lien.
Ar elme carir sinwa len i evandilyon pa i vanda antana i atarin:
in Eru acárie sa nanwa elven, hínaltar, enortavénen Yésus, ve yú técina ná i attea airelíresse: Tyé yondonya ná, ónen tye síra.
I se-enortanes et qualinallon, ar in uas nanwenuva quelexima sómanna, equéties sie: Antauvan len Laviro oravier, i vorondar.
Etta yú quetis hyana airelíresse: Ual lavuva vorondalyan cene quelexima sóma.
An Lavir, apa anes núro Eruo nirmen véra *nónareryasse, qualle ar náne panyana as ataryar ar cenne quelexima sóma.
Mal ye Eru ortane ua cenne quelexima sóma.
Etta alde ista – neri, hánor – i ter nér sina i apsenie úcariva ná len carna sinwa.
Ar i cáma yallo lá ence len náve leryane Móseo Şanyenen – pa ta aiquen ye save ná quétina pen cáma.
Etta cena i ya ná quétina i Erutercánossen ua tuluva lenna:
Á yéta, elde i quetir yaiwe, ar na quátine elmendanen, an carin carie réldassen – carie ya laume savuvalde yando qui aiquen nyare len pa sa!”
Autala, i queni arcaner hlare amba pa nattor sine i hilyala *sendaresse.
Etta, apa i queni i náner ocómienwe vintaner, rimbali imíca i Yúrar ar i quérinar i runcer Erullo hilyaner Paulo yo Varnavas, yet quentet téna ar te-hortanet lemya mi Eruo lisse.
I hilyala *sendaresse harive i quanda osto ocomne hlarien i Heruo quetta.
Íre i Yúrar cenner i şangar, anelte quátine *hrucennen, ar naiquetiénen quentelte ana i nati quétine lo Paulo.
Tá, carpala veriénen, Paulo yo Varnavas quentet: “Elden mauyane minyave quete Eruo quetta. Pan nirildes oa ar uar note inde valde oira coivien, yé! querimme immet Úyúrannar.
An sie i Héru acánie ment: Apánien lye ve cala nórion, náveldan rehtie i anhaira rantanna cemeno.”
Íre i queni i nórion hlasser ta, sámelte alasse ar laitaner Eruo quetta, ar illi i náner martyane oira coivien sáver.
Ente, i Heruo quetta náne cólina ter i quanda nórie.
Mal i Yúrar valtaner i *ainocimye nissi ar i minde neri i ostosse, ar ortanelte roitie ana Paulo yo Varnavas ar hanter tu rénaltar pella.
Pallette i asto taluttalto ar lendet Iconiumenna.
Ar i hildor náner quátine alassenen ar Aire Feanen.
Sí Iconiumesse lendette uo mir i *yomencoa, ar quentette mi lé tyarila hoa liyúme save, Yúrali ar Hellenyali véla.
Mal i Yúrar i uar sáve valtaner i Úyúrar ar carner te cotye i hánonnar.
Ananta, andave, quentette veriénen túrenen i Héruo, ye *vettane i quettan lisseryo, laviénen tanwalin ar tannalin marta ter máttat.
Mal i liyúme i ostosse náne şanca, ar ennoli náner i Yúrain ar exeli i apostelin.
Enge amortie Úyúraron ar i Yúraron véla, as turiltar, nucumien tu ar nahtien tu sarninen.
Íre túnettes, úşette i ostonnar mi Licaonia, Listra ar Lerve ar i nórie pelila ta.
Ar tasse carnette i evandilyon sinwa.
Ar Listrasse enge nér hamila ye ua polde tare talyatse. Anes *lalevíte amilleryo mónallo ar ua oi vantanelyane.
Nér sina lastane Paulonna íre carampes, ar íre Paulo yente se ar cenne i sámes i savie náven nestana,
quentes senna taura ómanen: “Tara ama talulyatse, téra!” Ar campes ama ar vantane.
Ar i şangar, íre cennelte ya Paulo carnelyane, ortaner ómalta, quétala Licaonio lambesse: “I ainor olólier atani ar utúlier undu venna!”
Ar estanelte Varnavas Seus, mal Paulo Hermes, pan isse náne ye quente tún.
Ar i *airimo Seuso, yeo corda náne epe i osto, talle mondoli ar riendeli i andonnar ar merne *yace as i şangar.
Mal íre i aposteli Varnavas yo Paulo hlasset pa ta, narcanette collattar ar campet mir i şanga, yámala
ar quétala: “Neri, mana castalda carien nati sine? Yú emme nát atani i perperir i imye nati yar elde perperir, ar carimme i evandilyon sinwa len, merila i queruvalde inde oa sine luste natillon i coirea Ainonna, ye ontane menel ar cemen ar ear ar ilye i nati mi tai.
I vanwe *nónaressen láves ilye i nórin lelya vére tieltassen,
ómu uas láve insen pene *vettie pa carierya márie, antala len ulo menello ar yáve i vanima lúmesse, quantala endalda mattanen ar alassenen.”
Ar quétala sie pustanette – ómu urdiénen – i şangar *yaciello tun.
Mal túler Yúrali ho Antioc ar Iconium ar quenter i şangannar ar vistaner sámalta, ar hantelte sarni Paulonna ar tuncer se et i ostollo, intyala i anes qualina.
Mal íre i hildor pelder se, orontes ar menne mir i osto. Ar i hilyala auresse oantes as Varnavas, lelyala Lervenna.
Ar apa carnette sinwa i evandilyon osto tanan ar carnet fárea nóte hildolion, nanwennette ana Listra ar Iconium ar Antioc.
Turyanette i hildoron fear, hortala te lemya i saviesse quetiénen: “Mauya men mene mir Eruo aranie ter rimbe şangiéli.”
Ente, santette amyárali tien ilya ocombesse, ar hyamiénen ar lamatenen antanettet manna i Heruo yesse sávelte.
Ar lendette ter Pisiria ar túlet mir Pamfilia,
ar apa quentette i quetta Peryasse, lendette undu Attalianna.
Ar talo cirnette oa Antiocenna, yasse nóvo Eruo lisse náne panyana hepiettasse i molien ya sí telyanelyanette.
Apa túlette tar ar comyanet i ocombe, nyarnette pa ilye i nati yar Eru carnelyane ter tú, ar in aláties i Úyúrain i fenna saviéva.
Tá lemnette lá şinta lúmesse as i hildor.
Ar nelli túler undu Yúreallo ar peantane i hánor: “Qui ualde *oscírine, Móseo haimenen, lá ece len ñete rehtie.”
Mal apa lá pitya cos ar costie mici té ar Paulo yo Varnavas, *partanelte i Paulo yo Varnavas ar exeli ho té menumner ama i apostelinnar ar amyárannar Yerúsalemesse pa cos sina.
Etta té, mentane lo i ocombe, lender ter Foinicia ar Samária, nyárala manen Úyúrar querner inte, ar tallelte túra alasse i hánoin.
Íre túlelte Yerúsalemenna anelte cámine lo i ocombe ar i aposteli ar i amyáre neri, ar nyarnelte pa ilye i nati yar Eru carne ténen.
Ananta ennoli i herenello i Farisaron i sáver, oronter sondaltallon ar quenter: “Mauya *osciritat ar cane tien himya Móseo şanye.”
Ar i aposteli ar i amyárar ocomner carien úvie pa natto sina.
Apa olya cos Péter oronte ar quente téna: “Neri, hánor, istalde i ho i minye auri Eruo cilme náne i ter ninya anto i nóri hlarumner i quetta i evandilyono, savieltan.
Sie Eru, ye ista i endar, *vettane tien antavénen tien i Aire Fea, ve yú carnes elven.
Mi munta lenganes ven ar tien mi *alavéla lé, apa poitie endalta saviénen.
Etta, sí mana castalda tyastien Eru, paniénen sine hildoron axesse yalta ya ua ence elven ar atarilvain véla cole?
Úsie, savilve i nauvalve rehtane i Heru Yésuo lissenen, ve yú queni sine savir.”
Ar i quanda liyúme náne carna quilda, ar lastanelte Varnavas yo Paulonna íre nyarnette pa ilye i tannar ar tanwar yar Eru carnelyane imíca i nóri ter tú.
Apa pustanette carpa, Yácov quente: “Neri, hánor, á lasta ninna!
Símeon anyárie manen Eru minyave cimne i nóri mapien et tailo lie esseryan.
Ar i Erutercánoron quettar quetir i imya nat, ve ná técina:
'Apa nati sine nanwenuvan ar encarastuvan Laviro lantienwa cauma, ar encarastuvan atalantie rantaryar ar ortauvan sa ata,
tyárala i lemyar imíca i atani cesta i Héru, as ennoli ho ilye i nóri,' quete i Héru ye cára nati sine,
sinwe et yalúmello.
Etta námienya ná i ávalve tyare urdie i quenin i nórellon i querir inte Erunna,
mal i tecuvalve téna i mauya tien hepe inte oa ho cordoni ar *hrupuhtie ar yallo quórina ná, ar sercello.
An enwine lúmellon Móses asámie ilye ostossen ennoli i carir se sinwa, pan náse et-hentaina i *yomencoassen ilya *sendaresse.”
Tá nemne mai i apostelin ar i amyárain ar i quanda ocomben menta cíline nelli ho té Antiocenna as Paulo yo Varnavas: Yúras ye náne estaina Varsappas, ar Sílas, minde neri imíca i hánor,
ar máltanen tencelte:“I aposteli ar i amyárar, hánolyar, i hánoin i ear mi Antioc ar Siria ar Cilicia, té i nar i nórion: Hara máriesse!
Pan ahlárielme in ennoli ho mici mé atyárier len urdie quettaltainen, névala nuquere fealda ómu ualme ánie tien peantier,
er sámanen acárielme i cilme i mentauvalme nelli lenna as meldalmar, Varnavas yo Paulo,
neri i apánier coivietta raxesse, Herulva Yésus Hristo essen.
Etta mentalme Yúras ar Sílas, nyarien i imye nati quettanen.
An nemne mai i Aire Fean ar elmen lá napane hyana cólo lenna hequa nati sine yar mauyar:
Hepa inde oa ho nati *yácine cordonin, ar sercello, ar quórine celvallon, ar *hrupuhtiello. Qui hepildexer oa sine natillon, caruvalde mai. Mále len!”
Sie, íre neri sine náner mentaine, lendelte undu Antiocenna, ar comyanelte i liyúme ar antaner téna i menta.
Ar apa hentaneltes, sámelte alasse i hortalénen.
Ar Yúras ar Sílas, pan yú tú nánet Erutercánor, hortaner i hánor rimbe quettalínen ar turyaner te.
Apa marie tasse ter lúme, anelte lehtane lo i hánor nan innar mentaner te.
Ono Sílas sanne i náne mára sen lemya tasse.
Mal Paulo yo Varnavas termarnet mi Antioc, yasse tú ar rimbe exeli carner sinwa i evandilyon pa i Heruo quetta.
Apa réli Paulo quente Varnavanna: “Angwe nanwene ar cene i hánonnar ilya ostosse yasse carnengwe sinwa i Heruo quetta, cenien manen cáralte.”
Mal Varnavas merne tala as inse yú Yoháno, ye náne estaina Marco.
Mal Paulo ua sanne i náne mára tala i nér ye tu-hehtane Pamfiliasse ar ua lende asette i molienna.
I rúşe imbe tu náne ta aica i tyarneset hehta quén i exe, ar Varnavas nampe Marco ar cirne oa Ciprusenna.
Paulo cilde Sílas ar lende oa apa i Heruo lisse náne panyana hepieryasse lo i hánor.
Lendes ter Siria ar Cilicia, turyala i ocombi.
Tá túles Lervenna ar yú Listranna. Ar yé! enge tasse hildo yeo esse náne Timoşeo, i yondo savila Yúra nisso, mal Hellenya ataro.
Pa sé ilye i hánor mi Listra ar Iconium caramper mai.
Paulo merne i tulumnes óse, ar nampeses ar *oscirne se, castanen i Yúraron i enger tane ménassen, an illi sinter i atarya náne Hellenya.
Mal íre lendette ter i ostor, cannette tien hepe i namnar tulcane lo i aposteli ar amyárar i enger Yerúsalemesse.
Sie i ocombi náner turyaine i saviesse ar oronter nótesse aurello aurenna.
Ente, lendette ter Frihia ar i Alátia-nórie, pan i Aire Fea váquente quete i quetta Asiasse,
mal íre túlette Misianna, névette lelya Vitinianna, mal Yésuo faire ua láve tun.
Apa lendette ter Misia túlette undu Troasenna.
Ar i lómisse Paulo camne maur: Nér Maceroniallo tarne arcala sello, quétala: “Tula olla mir Maceronia ar áme manya!”
Apa cennes i maur, cestanelme lelya Maceronianna, intyala in Eru tultane me carien i evandilyon sinwa tien.
Etta cirnelme Troasello ar lender térave Samoşrácenna, mal i hilyala auresse ana Neapolis
ar talo Filippinna, vinyamar, ya ná i amminda osto Maceronia·ménasse. Hautanelme osto sinasse, lemyala ter réli.
Ar i *sendaresse lendelme ettenna ter i ando, nómenna ara síre ya sannelme náne nóme hyamiéva, ar hamunelme ar caramper i nissennar i náner ocómienwe.
Ar lastane nís yeo esse náne Liria, macar *luicarne lanneo Şiatíra·ostollo ye *tyerne Eru, ar i Heru latyane endarya cimien i nati yar Paulo quétane.
Apa isse ar nosserya náner sumbane, quentes arcandesse: “Qui namilde ni voronda i Herun, tula mir coanya ar á lemya!” Ar me-mauyanes tule.
Ar túle, íre ménanelme i nómenna hyamiéva, i me-velde inya mól arwa faireo apacenwa. Taldes heruryain olya telpe *yuhtiénen apacen.
Nís sina hilyane Paulo ar me, yámala quettar sine: “Neri sine nar Aino Antaro móli, ar carilte sinwa len i tie rehtiéva.”
Carnes sie rimbe aurelissen. Lumba, Paulo teldave quernexe ar quente i fairenna: “Canin lyen Yésus Hristo essenen: Tula et sello!” Ar hé ettúle i imya lúmesse.
Íre heruryar cenner in estelelta ñétiéva vanwa né, nampelte Paulo yo Sílas ar tuncer tu i nómenna mancaléva, i turconnar.
Tulyaneltet i námonnar ar quenter: “Neri sine *tarastar ostolva, nála Yúrar,
ar caritte sinwe haimeli yar uar lávine ven ve Rómear came hya hilya.”
Ar i şanga oronte túna, ar i námor, apa narcanelte collattar tullo, canner i mo palpumne tu rundainen.
Apa antanelte tun rimbe taramboli, hanteltet mir mando, cánala i mandocundon hepitat varnave.
Apa camnelte taite canwa, hanteset mir i ammitya mando ar tance taluttat i panonna.
Mal *os i lómio ende Paulo yo Sílas hyámaner, laitala Eru lírinen, ar i queni i mandosse lastaner tu.
Rincanen túra *cempalie martane, tyárala i mando talmar pale. Ente, mí imya lú ilye i fennar náner pantane, ar illion limili náner lehtane.
I mandocundo, íre nánes eccoitana ar cenne i mando fennar pantane, tunce macilya ar nahtumne inse, intyala in ilquen i mandosse vanwa né.
Mal Paulo yáme taura ómanen: “Áva harna imle, an nalme illi sisse!”
Ar arcanes calali ar campe minna, ar pálala lantanes undu epe Paulo ar Sílas.
Ar tulyanes tu ettenna ar quente: “Heruvi, mana mauya nin care ñetien rehtie?”
Quentette: “Sava i Heru Yésusse, ar nauval rehtana, elye ar nosselya.”
Ar quentette i Heruo quetta senna ar ilyannar i enger coaryasse.
Ar talleset óse yana lúmesse ar sóve rimpeltar, ar isse ar illi i enger óse náner sumbane pen hopie.
Ar talleset mir coarya ar panyane matta i sarnosse epe tu, ar sámes túra alasse as quanda nosserya, pan hirnes savie Erusse.
Íre aure túle, i námor mentaner i rundacolindor quetien: “Á lehta neri tane!”
Ar i mandocundo nyarne quettar sine Paulon: “I námor ementayer nelli leryaven let! Etta, sí ettula ar á auta rainesse!”
Mal Paulo quente téna: “Rípelte met epe i lie, ú námiéno, neri i nar Rómear, ar hantelte met mir mando – ar sí et-hátalte met nuldave? Laume! Lava tén tule ar tulya met ettenna!”
I rundacolindor nyarner quettar sine i námoin. Anelte ruhtane íre hlasselte i anette Rómear.
Etta túlelte ar carner arcande tun, ar apa talleltet ettenna, arcanelte i autumnette i ostollo.
Mal túlette et i mandollo ar lendet coaryanna Liria, ar íre cennette i hánor hortanettet. Tá oantette.
Sí lendette ter Amfipolis ar Apollonia ar túlet Şessalonicanna, yasse enge Yúra *yomencoa.
Sie, haimeryanen, Paulo lende minna téna, ar *sendaressen nelde quentes aselte pa i Tehteler,
antala ten hande ar tánala i mauyane i Hriston perpere ar orta qualinallon, ar eques: “Si i Hristo ná, Yésus sina ye carin sinwa len.”
Etta ennoli mici te sáver ar himyaner Paulo ar Sílas, as hoa liyúme i Hellenyaron i *tyerner Eru, ar lá mancali i amminde nission.
Mal i Yúrar náner quátaine *hrúcennen, ar apa comyanelte olce nelli i nómello mancaléva ar carner şanga, tyarnelte amortie i ostosse. Lantanelte coaryanna Yáson ar cestaner tala tu ettenna i lienna.
Íre ualte hirne tu tuncelte Yáson ar hánoli i turconnar i osto, yámala: “Queni sine i nuequérier ambar sí utúlier yú sir,
ar Yáson acámie te mir coarya! Ar ilye queni sine tarir ana i canwar i Ingaráno, an quetilte i ea hyana aran, Yésus.”
Valtanelte i şanga ar i osto turcor i hlasser nati sine,
ar apa mapie fárea varnasse ho Yáson ar i exi lehtaneltet.
Pen hopie, lóme yanasse i hánor mentaner yúyo Paulo ar Sílas Veroianna, ar íre túlette tar, lendette mir i *yomencoa i Yúraron.
Té náner arte lá i marner Şessalonicasse, ar camnelte i quetta ilya mára nirmenen, ar ilya auresse cenşelte i Tehteler cenien qui nati sine náner sie.
Etta rimbali mici te sáver, ar sie carner lá nótime minde Hellenye nisseli, yú rimbe nelli.
Mal íre i Yúrar Şessalonicallo parner in Eruo quetta náne carna sinwa yando Veroiasse lo Paulo, túlelte tar valtala ar *tarastala i şangar.
Pen hopie i hánor mentaner Paulo oa, tenna i ear, mal Sílas ar Timoşeo véla lemner yasse engette.
Mal i tulyaner Paulo taller se tenna Aşen, ar nanwennelte tálala canwarya Sílas ar Timoşeonna: Mauyane tun tule lintiettanen.
Lan Paulo hópane tun mi Aşen, fairerya náne *tarastaina ceniénen manen i osto náne quanta cordonion.
Etta carampes i *yomencoasse as i Yúrar ar i exi i *tyerner Eru – yú i nómesse mancaléva, as i túler tar.
Mal ennoli imíca i *sailiendili, i Epicurear ar i Stoici véla, lender mir cos óse, ar queneli mici te quenter: “Mana nyatila quén sina mere quete?” Exeli quenter: “Neme i cáras sinwe ettelie ainoli.” Ta náne pan carampes pa i evandilyon pa Yésus ar i enortave.
Etta nampeltes ar tulyane se mir i Areopahus, quétala: “Ma lertalme ista mana nás, peantie sina ya quétal?
An talas noali yar nar ettelie hlarulmant. Etta merilme ista mana nati sine tear.”
An illi mi Aşen ar ilye i ettelear tasse satir ilya lerina lú carien munta hequa nyare hya hlare sinyar.
Paulo oronte endesse Areopahusso ar quente: “Neri Aşenello, tuntan i mi ilye nati samilde áya i ainoin túra lá ya exi samir.
An lan ménanen ter i osto, yétala i nati yar *tyerilde, hirnen yú *yangwa yasse náne técina: “Úsinwa ainon.” Etta, ya *tyerilde pen istie sa, ta inye cára sinwa len.
I Aino ye carne i mar ar ilqua ya ea sasse ua mare cordassen cárine mainen.
Ente, uas *veuyaina atanion mainen, ve au samis maure erya natwa, isse immo antala illin coivie ar şúle ar ilye nati.
Ar carnes et er atanello ilya nóre ataniva, marieltan i quanda palúresse cemeno, ar santes i lúmi ar i rénar vehten atanion,
cestialtan Eru, qui ece tien rahta senna ar hiritas, ómu é uas haira ho erya quén mici vi.
An issenen samilve coivie ar levilve ar ealve, ve yando maitalli mici lé equétier: 'An nalve yú nosseryo.'
Etta, cenila i nalve Eruo nosse, ualve care mai qui sanalve pa i Aino i náse ve malta hya telpe hya ondo, hya tanwe cátina atano curwenen hya sanwenen.
Nanwa, Eru tanne cóle mi lúmi sine yassen queni epénier istya, mal sí quetis i atannar ilya nómesse i mauya tien hire inwis.
An asáties ré yasse namuvas cemen failiesse nernen ye icílies, ar ánies ilye atanin varnasse ortavénen se et qualinallon.”
Mal íre hlasselte pa enortave qualinaiva, ennoli quenter yaiwe, íre exeli quenter: “Lye-hlaruvalme ata pa natto sina yú hyana lúmesse.”
Sie Paulo etelende et endeltallo,
mal ennoli himyaner se ar sáver. Mici té náne Lionisio ye náne námo mí Areopahus, ar nís estaina Lamaris, ar exeli ara tu.
Apa nati sine oantes ho Aşen ar túle Corintenna.
Ar hirnes Yúra estaina Aquila, nér nóna mi Pontus, ye şinta lúme yá náne tulinwa Italiallo as Priscilla verirya, pan Claurio canwanen ilye Yúrar oantelyaner Rómallo. Ar túles túna,
ar pan sámelte i imya nostale moliéva, lemnes asette ar móle. An molielta náne care *lancoar.
Mal carampes i *yomencoasse ilya *sendaresse ar tyarne Yúrar ar Hellenyar save.
Íre Sílas ar Timoşeo ununter Maceroniallo, Paulo aqua antanexe carien i Quetta sinwa, *vettala i Yúrain i Yésus ná i Hristo.
Mal íre tarnelte ana se ar quenter yaiwie, palles i asto larmaryallon ar quente téna: “Nai sercelda tuluva véra careldanna! Inye poica ná. Ho sí lelyan Úyúrannar.”
Ar léves talo ar lende mir i coa quenwa estaina Titio Yusto, ye *tyerne Eru. Coarya náne carastana mir i imya ataque yasse i *yomencoa enge.
Crispo i *yomencoantur camne i savie i Herusse as quanda nosserya, ar apa hlarie, rimbali imíca i queni Corinto sáver ar náner sumbane.
Ar mi maur i lómisse i Heru quente Paulonna: “Áva ruce, mal queta ar áva na quilda,
an ean aselye ar *úquen caruva lyen ulco, an samin rimbe ennoli osto sinasse.”
Ar termarnes tasse ter er loa ar astar enque, peantala mici te Eruo quetta.
Sí íre Allio náne nóreturco Acaio, i Yúrar er sámanen oronter Paulonna ar tulyaner hé i námosondanna,
quétala: “Nér sina tyare atani save i mauya tien *tyere Eru mi lé ana i Şanye.”
Mal íre Paulo pantumne antorya, Allio quente i Yúrannar: “Au i natto náne pa ulco hya olca carda, sámen casta samien cóle aselde.
Mal qui i natto ná pa costiéli pa quettali ar esseli ar i Şanye ya samilde, mauya elden cene sanna. Inye ua mere náve námo pa nati sine.”
Ar éles te oa i námosondallo.
Ono illi mici te namper Sostenes i *yomencoantur ar palpaner se epe i námosonda. Mal Allio cimne munta sine nation.
Mal apa lemie tasse ter an rimbe réli, Paulo quente namárie i hánonnar ar cirne oa Sirianna, ar óse lendet Priscilla yo Aquila. Oantes apa cirie findelerya mi Cencreai, an sámes vanda.
Túlelte Efesusenna, ar tu-hehtanes tasse, mal isse lende mir i *yomencoa ar quente as i Yúrar.
Íre arcanelte senna lemya aselte an anda lúmesse, uas merne,
mal quentes namárie ar nyarne tien: “Nanwenuvan lenna ata, qui Eru mere.”
Ar cirnes oa Efesusello ar túle undu Césareanna. Ar lendes ama ar *suilane i ocombe, ar ununte Antiocenna.
Ar apa marnes tasse ter lúme oantes ar lende nómello nómenna ter Alatia·nórie ar Frihia, tulcala i hildor.
Sí túle Efesusenna Yúra yeo esse náne Apollo, nóna Alexandriasse. Sintes carpa mai ar náne taura i Tehtelessen.
Anes peantana mí Heruo tie, ar pan anes lúşina i fairenen, carampes ar peantanes ya nanwa náne pa Yésus, mal sintes rie Yoháno sumbie.
Ar nér sina *yestane quete canyave i *yomencoasse. Íre Priscilla yo Aquila hlasset se, nampettes asette ar tulyane se ambe tumna handenna i tieo Eruo.
Ente, pan mernes lelya mir Acaia, i hánor tencer i hildonnar, hortala i camumneltes mai. Sie, íre túles tar, ita manyanes i sáver i Erulissenen.
Ar veassenen tenges i Yúraron loima, tánala i Tehtelellon i Yésus ná i Hristo.
Íre Apollo enge Corintesse túle i Paulo lende ter i mitye nórier tenna Efesus. Ar hirnes hildoli
ar quente téna: “Ma camnelde Aire Fea íre sávelde?” Quentelte senna: “Laume ahlárielme i ea Aire Fea.”
Ar eques: “Mi mana, tá, anelde sumbane?” Hanquentelte: “Mi Yoháno sumbie.”
Eque Paulo: “Yoháno sumbane i sumbiénen inwisteva, hortala i lie save yesse tulumne apa se – ta ná, Yésusse.”
Íre hlasselte ta, anelte sumbane mi Yésus Hristo esse.
Ar íre Paulo panyane máryat tesse, i Aire Fea túle téna, ar quentelte lambelissen ar caramper ve Erutercánor.
Quanda nótelta náne *os neri yunque.
Lendes mir i *yomencoa ar quente canyave astassen nelde, carpala ar tálala exeli handenna pa Eruo aranie.
Mal íre ennoli carner inte sarde ar uar sáve, quétala yaiwe pa i Tie epe i liyúme, te-hehtanes ar hyarne i hildor ho te, ilya auresse antala carpiéli mi Tiráno ataque.
Ta martane ter loa atta, ar sie illi i marner Asiasse hlasser Eruo quetta, Yúrar ar Hellenyar véla.
Eru carne taure cardali, han ya senwa né, Paulo mánten.
Sie yando lanneli ar *quiltalanneli náner cóline helmaryallo ar panyane i hlaiwassen, ar i hlíwi váner tello, ar i olce fairi ettúler.
Mal enger yú vantala Yúrali imíca i et-hanter raucor i néver quete Yésuo esse innar náner haryaine lo raucor: “Canin le i Yésunen ye Paulo care sinwa.”
Enger yondor otso nero estaina Sceva, Yúra héra *airimo, i carner sie.
Mal hanquentasse i olca faire quente téna: “Yésus istan, ar ahlárien pa Paulo, mal man nar elde?”
Ar i nér haryaina lo i olca faire lantane téna ar turune te illi, tyárala te nore harne ar helde et sana coallo.
Natto sina túle illion istyanna, Yúraron ar Hellenyaron véla, té i marner Efesusse. Ar caure lantane ilyannar mici te, ar i Heru Yésuo esse náne laitaina.
Ar rimbali ion sáver túler ar quenter pantave pa cardaltar.
Lá mancali imíca i nóvo *yuhtaner ñúle comyaner parmaltar ar urtaner tai epe illion hendu. Ar onontelte i nonwe ya paityanelte tain ar hirner i mirwielta náne telpemittar *quaihúmi *lepenquean.
Sie, túrenen, i Heruo quetta alle ar náne carna taura.
Apa nati sine náner telyane, Paulo carne ennerya in apa lendes ter Maceronia ar Acaia lelyumnes Yerúsalemenna, quétala: “Apa tuluvan tar mauya nin yú cene Róma.”
Mentanes Maceronianna atta núroryaron, Timoşeo ar Erasto, mal isse lemne ter lúme Asiasse.
*Os lúme yana enge lá pitya amortie pa i Tie.
An nér estaina Lemetrio, *telpetan, antane lá pitya lar i tanoin cariénen telpine yánar Artemisso.
Ar comyanes té ar i móler as taiti nati ar quente: “Neri, istalde mai i sina moliénen samilve larelva.
Ar cenilde ar hlarilde manen lá rie Efesusse, mal harive quanda Asiasse, Paulo sina atálie hoa şanga vinya savienna, quetiénen i tane ainor i nar cárine mainen laume nar ainor.
Ente, ea raxe i lá rie sina molielva nauva nattíraina, mal yú i corda i túra Artemisso nauva nótaina ve munta, metyala yú túrierya, epe ya quanda Asia ar ambar luhtar.”
Hlárala si ar nála quátaina rúşenen, i neri yámer: “Túra ná Artemis Efesuyaron!”
Sie i osto náne quátaina rúciniénen, ar er sámanen lendelte rimpe mir i *tirmen, mápala Aio ar Aristarco ar tu-tálala aselte nirmenen. Anette ennoli Maceroniallo lelyala as Paulo.
Paulo inse merne lelya minna i lienna, mal i hildor úner mérala lave sen.
Ennoli imíca Asio turcor, i náner meldoryar, mentaner senna ar arcaner sello in ávas panyumne inse raxesse i *tirmende.
Ennoli yámer er nat ar exeli hyana nat, ar i amarimbar mici te uar sinte i casta yanen anelte ocómiéla.
Etta uo tallelte Alexander et i şangallo, an i Yúrar se-ninder ompa.
Mal íre hanyanelte i anes Yúra, yámelte er ómanen ter *os lúme atta: “Túra ná Artemis Efesuyaron.”
Apa i *ostotecindo carne i şanga quilda, quentes: “Neri, man ea mici atani ye ua ista in Efesuyaron osto ná cundo i cordo i túra Artemisso ar i emmo ya lantane menello?
Etta, pan mo ua pole láquete ta, caruvalde mai qui nalde quilde ar carir munta pen sanwe.
An atálielde neri sine i uar pilur cordaron ar uar equétie yaime ainilvanna.
Etta, qui Lemetrio ar i tanor óse samir cos as aiquen, ear auri namiéva ar ear nóreturcor: Lava tien tala i costi yar samilte, quén as i exe, i námonnar.
Mal qui cestalde *aiqua han ta, mauya i şanya combe tulca sa.
An ea raxe i nurreli pa amortie nauvar talaine venna apa ya amartie síra, an ualve same erya casta antaven pa yalme sina.”
Ar apa quentes ta, mentanes i ocómienwar oa.
Íre i amortie náne vanwa, Paulo tultane i hildor, ar apa hortanes te ar quente námarie téna, lendes oa menien mir Maceronia.
Apa lendes ter sane ménar ar hortane i enger tasse rimbe quettalínen, túles mir Hellas.
Ar apa termarnes astar nelde tasse, cirumnes oa Sirianna, mal pan i Yúrar carner panoli ana se, carnes i cilme i nanwenumnes ter Maceronia.
Óse i lendasse enger Sosipater yondorya Pirro Veroiallo, Aristarco ar Secundo Şessalonicallo, ar Aio Lervello, ar Timoşeo ar Trifimo Asiallo.
Té lender epe me ar homper men Troasse,
mal elme cirner oa Filippillo apa i auri *Alapúline Mastaron, ar túlelme téna Troasse nó auri lempe náner vanwe. Tasse lemnelme ter rí otso.
I minya auresse i otsolo, íre anelme ocómienwe racien massa, Paulo carampe téna, pan autumnes i hilyala auresse, ar quétanes tenna ende i lómio.
Mal enger rimbe calmali i oromardesse yasse anelme ocómienwe.
Hámala ara i lattin enge nessa nér estaina Eutico, ye lantane mir núra húme íre Paulo quétane, ar taltala húmesse lantanes undu i neldea talamello ar náne qualina íre anes ortana.
Mal Paulo ununte, hantanexe senna, panyane rancuryat *os se ar quente: “Áva care yalme, an náse en coirea.”
Tá nanwennes ama, rance i massa ar mante, ar carampes tenna ára ar tá oante.
Mal i seldo tallelte oa coirea, ar anelte tiutane lesta pella.
Sí lendelme epe te i luntenna ar cirner oa Assosenna, yasse camumnelme Paulo mir i cirya. An apa antanes canwar pa ta, isse merne lelya talyanten.
Íre túles tenna i nóme yasse anelme mi Assos, camnelmes mir i lunte ar lender Mitilenenna.
Ar apa cirnelme talo i hilyala auresse túlelme ara Cios, ar apa an ré rahtanelme tenna Samos, ar i hilyala auresse túlelme mir Miletus.
An Paulo carne i cilme i lahtumnes Efesus, lá merila hauta Asiasse, an anes lelyala hormesse: Qui poldes mernes tene Yerúsalemenna nó i Aşar Otsolaron Otso.
Mal ho Miletus mentanes Efesusenna ar tultane i amyárar i ocombeo.
Íre túlelte senna quentes téna: “Istalde mai manen i minya aurello ya túlen Asianna anen aselde i quanda lúmesse,
molila i Herun mi ilya naldie, arwa nírion ar perperila i şangier yar martaner nin i Yúraron ulce panoinen,
íre uan hempe lello erya nat aşea len, hya loitane peanta len epe i lie ar i coassen.
Mal *vettanenye Yúrain ar Hellenyain véla pa inwis Erunna ar savie Herulvasse, Yésus.
Ar sí, yé, nútina i fairesse lelyan Yerúsalemenna, ómu uan ista yar martuvar nin sasse,
hequa in ilya ostosse i Aire Fea *vetta nin i núteli ar şangiéli hópar nin.
Ananta uan note coivienya ta melda nin, qui ece nin telya núromolienya ya camnen i Heru Yésullo: *Vettie pa i evandilyon i lisseo Eruo.
Ar sí, yé, inye ista in ualde cenuva cendelenya ata, elde illi imíca i vantanenye, carila sinwa i aranie.
Etta *vettan illin mici le i nanye poica i sercello ilye atanion,
an uan ehépie imne nyariello len Eruo quanda pano.
Cima inde ar i quanda lámáre ya i Aire Fea apánie ortírieldasse. Na mavari i Heruo ocomben, ya ñentes véra serceryanen.
Inye ista i apa autan, lumne ñarmoli tuluvar mici le, i uar oravuva i lámáresse,
ar ho elde ortuvar nelli i quetir rícine natali, tucien oa i hildor apa inte.
Etta na coive, enyalila manen ter loar nelde, lómisse yo auresse, uan pustane peanta len nírelínen.
Ar sí antan le olla Erun ar lisseryo quettan, ya pole carasta le ama ar anta len rantalda ve indyoni, as ilye i airintar.
Telpe hya malta hya larma uan ecestie ho aiquen.
Elde istar i mát sine ániet nin ar in enger asinye maurelmar.
Illume tannen len in íre sie molilde mauya len manya i nar milye ar enyale i quetta ya i Heru Yésus immo quente: Antave ná alasse túra lá camie.
Ar apa quentes quettar sine, lantanes occaryanta as illi mici te ar hyamne.
Enge fárea yaimie mici te illi, ar lantanelte Paulo axenna ar minquer se.
An or ilqua anelte ñwalyaine i quettanen ya quentes, in ualte cenumne cendelerya ata. Tá hilyaneltes i luntenna.
Apa mauyanelme imme hehta te ar cirner oa, lendelme térave Cosenna, mal i hilyala auresse Rolosenna, ar talo Pataranna.
Ar apa hirnelme lunte ya lelyumne Foicinianna, lendelme mina sa ar cirner oa.
Apa cennelme Cipros, lendelme ara sa i hyaryasse, cirner Sirianna ar túler Tírenna, an ta náne i enne i armaron i luntesse.
Cestanelme i hildor ar hirner te, ar lemnelme tasse ter rí otso. Mal i Fairenen quentelte Paulonna: “Áva panya tál Yerúsalemesse!”
Mal íre i auri náner vanwe, etelendelme ar menner tielmasse. Illi mici te, as i nissi ar híni, hilyaner me tenna ette i osto. Ar apa lantie occalmanta i fárasse hyamnelme,
ar quentelme namárie quén i exenna, ar lendelme mir i lunte, mal té nanwenner marda.
Ho Tír túlelme Ptolemaisenna, telyala i luntelenda. *Suilanelme i hánor ar lemner er ré aselte.
I hilyala auresse cirnelme oa ar túler Césareanna, ar lendelme mir coarya Filip, nér ye cárane sinwa i evandilyon. Anes imíca i Otso, ar lemnelme óse.
Enger as nér sina selyer canta, vendeli, i sámer Erutercáno anna.
Mal lan lemnelme tasse ter nóte aurion, Erutercáno yeo esse náne Ahavo túle undu Yúreallo, ar túles menna ar nampe i quilta Paulova.
Nuntes vére talyat ar máryat ar quente: “Sin quete i Aire Fea: I nér ye harya quilta sina i Yúrar Yerúsalemesse nutuvar, ar antauvaltes olla Úyúrain.”
Íre hlasselme si, elme ar i marner tasse véla arcaner sello: “Áva lelya ama Yerúsalemenna!”
Tá Paulo hanquente: “Mana cáralde, yaimie ar rácala endanya? An inye manwa ná, lá rie náven nútina, mal yú qualien Yerúsalemesse i essen i Heru Yésuo.”
Íre lá ence men vista sámarya, pustanelme arca ar quenter: “Lava i Heruo indómen marta.”
Apa auri sine manwanelme ar lender lendalmasse Yerúsalemenna.
Mal ennoli i hildoron Yerúsalemello yú lender aselme, talien me i nerenna yeva coasse marumnelme: Mnason, ye náne i minye hildoron.
Íre túlelme Yerúsalemenna, i hánor me-camner mi alasse.
Mal i hilyala auresse Paulo lende aselme mir coarya Yácov, ar ilye i amyárar enger tasse.
Ar *suilaneltet ar nyarner pa ilya erya nat ya Eru carne imíca i nóri núromolieryanen.
Apa hlarie ta antanelte alcar Erun, ar quentelte senna: “Yétal, háno, manen ear liyúmeli i savir imíca i Yúrar, ar illi mici te uryar i Şanyen.
Mal ahlárielte in anaie quétina pa elye i peániel imíca i nóri i mauya tien tare oa Mósello, peantala tien i ávalte *oscire hínaltar hya hilya i haimi.
Mana, tá, caruvalve? Tanca ná i hlaruvalte in utúliel.
Etta cara nat sina ya nyarilme len: Ear aselme neri canta i samir vanda.
Áte tala aselye ar á poita imle aselte, ar alye paitya tien, cirieltan i findele careltallo. Tá illi hanyuvar i ya ahlárielte pa lye ua nanwa, mal i elye hilya i Şanye ar hepe sa.
Pa i Úyúrar i savir elve etécier téna, canila i mauya tien hepe inte oa yallo anaie *yácina cordonin, ho serce, ho hráve quórine celvaron ar ho *hrupuhtie.”
Tá Paulo talle i neri óse, ar i hilyala auresse poines inse aselte ar lende mir i corda, carien sinwa i ré yasse sovallelta nauvane telyana ar i *yanca nauvane talaina ilquenen mici te.
Mal şintave nó i rí otso náner telyane, i Yúrar Asiallo cenner se i cordasse ar tyarne amortie imíca i şanga, ar panyanelte máltat sesse
ar yámer: “Neri Israélo, á manya! Nér sina ná ye peanta ilquenen mi ilya nóme ana i lie ar i Şanye, ar ana nóme sina! Ente, atálies Hellenyali mir i corda ar avahtaye XXX aire nóme sina!”
An nóvo cennelte Trifimo Efesusello i ostosse óse, ar intyanelte i Paulo talle se mir i corda.
Ar i quanda osto náne valtana, ar i lie norne uo, ar nampelte Paulo ar tuncer se et i cordallo. Mí imya lú i andor náner avalatyane.
Ar íre cestenelte nahta se, quetta túle i turconna i hosseo pa quanda Yerúsalem nála rúcina.
Pen hopie talles ohtalli ar *tuxantulli ar norne undu téna. Cénala i turco ar i ohtari pustanelte palpa Paulo.
Tá i hosseturco túle hare ar nampe se ar canne: “Áse nute limil attanen!” Ar maquentes pa man anes ar pa mana carnelyanes.
Mal ennoli i şangasse yámer er nat ar exeli hyana nat. Castanen i ramo poldes pare munta tulca, ar cannes i talumnelte Paulo i *estolienna.
Mal íre túles i tyellennar mauyane i ohtarin colitas, i şango ormenen.
An i liyúme i lieo hilyane te, yámala: “Áse nahta!”
Íre tulyauvaneltes mir i *estolie, Paulo quente i hosseturconna: “Ma lertan quete nat lyenna?” Quentes: “Ma istal quete Hellenya?
Ma nanwave ual i Mirra ye nó auri sine tyarne amortie ar tulyane i *sicilmor húmi canta mir i ravanda?”
Tá Paulo quente: “Inye Yúra nér, nóna mi Tarsus Ciliciasse, osto lá nulla. Mal arcan lyello: Ánin lave quete i lienna!”
Apa láves ta, Paulo, tárala i tyellessen, léve máryanen i lien. Íre tumna quilde lantane, carampes téna i Heverya lambesse, quétala:
“Neri, hánor ar atari, hlara ni quéta inyen!”
Íre hlasselte i quétanes téna i Heverya lambesse, anelte en ambe quilde, ar eques:
“Inye Yúra ná, nóna mi Tarsus Ciliciasse, mal peantana osto sinasse epe talu Amaliélo mí tarya lé i Şanyeo atarilvaron. Anen uryala Erun ve illi mici lé aure sinasse.
Ar roitanen Tie sina i qualmenna, nútala ar antala olla mir mando neri ar nissi véla,
ve yú i héra *airimo ar i quanda combe amyáraron polir *vetta nin. Ho té yú ñenten mentali i hánonnar Lamascusse, ar engen tienyasse talien yú i enger tasse nútine Yerúsalemenna paimen.
Mal lendanyasse, íre túlen hare Lamascusenna, *os ende i aureo, rincanen et menello hoa cala caltane ninna,
ar lantanen cemenna ar hlasse óma quéta ninna: 'Saul, Saul, mana castalya roitien ni?'
Hanquenten: 'Man nalye, Heru?' Ar quentes ninna: 'Nanye Yésus Nasaretello, ye elye roita.'
I neri i enger asinye cenner i cala, mal ualte hlasse i óma yeo quétane ninna.
Tá quenten: 'Mana caruvan, Heru?' I Heru quente ninna: 'Á orta, mena mir Lamascus, ar tasse ilqua ya ná lyen sátina carien nauva nyárina lyen.'
Mal uan polde cene, i alcarnen sana calo, ar tulyaina i mánen lo i enger asinye túlen mir Lamascus.
Mal nér estaina Ananías, ye náne *ainocimya i Şanyenen ar sáme mára *vettie ilye i Yúrallon i marner tasse,
túle ninna, ar tárala ara ni quentes ninna: 'Saul, háno, cena ata!' Ar mí imya lúme polden ata cene, cénala se.
Quentes: 'Atarilvaron Aino icílie lyé istien indómerya ar cenien i Faila ar hlarien antoryo óma,
an elye nauva *vetto sen ilye atanin, pa i nati yar ecéniel ar ahláriel.
Ar sí, manen ná i hópal? Á orta, na sumbana ar sova úcarilyar oa yaliénen esseryanen.'
Mal apa nanwennen Yerúsalemenna ar hyamne i cordasse, camnen maur
ar cenne se quéta ninna: 'Na linta ar á auta Yerúsalemello hormenen, an ualte camuva *vettielya pa ní.'
Ar inye quente: 'Heru, té istar mai in inye hante mir mando ar rípe mi ilya *yomencoa i sáver lyesse,
ar lan Stefáno *vettolyo serce náne etulyaina, yú inye tárane hare ar sanne mai pa ya martane, ar tíranen i collar i queniva i nacanter se.'
Ananta quentes ninna: 'Mena, an lye-mentauvan haire nórennar!'
Lastaneltes tenna quentes quetta sina, ar tá ortanelte ómalta, quétala: “Á mapa taite nér oa cemello, an uas valda coiviéno!”
Ar pan yámelte ar paller collaltar ar hanter asto mir i vilya,
i hosseturco canne i mo talumne se mir i *estolie, náven céşina riptiénen, istieryan i casta yanen sie yámelte senna.
Mal apa lenuneltes lattalínen i riptien, Paulo quente i *tuxanturenna ye tárane tasse: “Ma lertal ripta nér ye Rómea ná, ar ye ua námina?”
Íre i *tuxantur hlasse ta, lendes i hosseturconna ar nyarne sa sen, quétala: “Mana caruval? An i nér Rómea ná!”
Etta i hosseturco túle senna ar maquente: “Ánin nyare, ma nalye Rómea?” Eques: “Nanye.”
I hosseturco hanquente: “Mancanen ninna lertier sine túra nonwenen.” Paulo quente: “Mal inye náne nóna mir tai.”
Etta, mí imya lú, i neri i ceşumner se ñwalmenen tarner oa sello, ar caure nampe i hosseturco íre hirnes in é anes Rómea, apa se-nuntelyanes.
I hilyala auresse mernes ista tancave mana ulco i Yúrar quenter in acáries. Etta se-lehtanes i limilillon ar canne i Tára Comben i ocomumnelte. Tá talles Paulo ar panyane se epe te.
Paulo, yétala i Tára Combe, quente: “Neri, hánor, elengien epe Eru mi ilvana *immotuntie, tenna aure sina.”
Tá Ananías i héra *airimo canne in tarner ara se i petumneltes i antosse.
Tá Paulo quente senna: “Eru petuva lyé, a ramba ninquinta! Ma hámal ni-namien i Şanyenen, ar mí imya lú rácal i Şanye caniénen i nauvan pétina?”
I tarner ara se quenter: “Ma quétal ulco pa i héra *airimo Eruo?”
Ar Paulo quente: “Hánor, uan sinte i náse i héra *airimo. An ná técina: Áva quete ulco pa turco lielyo.”
Sintes in imíca i enger tasse, er ranta náne Sanducearon ar i hyana Farisaron, ar etta yámes i Tára Combesse: “Neri, hánor, inye Farisa ná, yondo Farisaron. Pa i estel ar i enortave qualinaiva nanye námaina!”
Pan quentes nat sina, enge cos imbi Farisar ar Sanducear, ar i liyúme náne şanca.
An Sanducear quetir i lá ea enortave hya vala hya faire, mal i Farisar pantave savir ilye nati sine.
Tá enge hoa yalme, ar parmangolmoli i náner i ranto i Farisaron oronter ar quenter costesse: “Ualme hire ulco mi nér sina. Mana qui faire é equétie senna, hya vala?”
Íre aica cos oronte, i hosseturco runce ho i tucumnelte se mir rantali, ar cannes i ohtarin i menumnelte undu rapien se et endaltallo ar talien se mir i *estolie.
Mal i hilyala lómisse i Heru tarne ara se ar quente: “Huore! An ve *evettiel pa yar apir ni Yerúsalemesse, sie mauya lyen *vetta yú Rómasse.”
Íre aure túle, i Yúrar carner vére ar hunter inte qui mantelte hya suncelte nó nahtie Paulo.
I nóte i nerion i uo antaner vanda sina náne or *canaquean.
Lendelte i hére *airimonnar ar i amyárannar ar quenter: “Ánielme vanda i ualme matuva nó anahtielme Paulo.
Etta nyara i hosseturcon, elde as i Tára Combe, i mauya sen tala se undu lenna, ve qui merilde care centa istien ambe tancave i nati yar apir se. Mal nó tuluvas hare, elme nauvar manwe nahtien se.”
Mal íre Paulo néşo yondo hlasse i caitumnelte nurtane nahtien se, lendes senna, ar íre túles i *estolienna sa-nyarnes sen.
Etta Paulo yalde quén imíca i *tuxanturi ar quente: “Á tala nessa nér sina i hosseturconna, an samis nat nyarien sen.”
Etta nér sina nampe se ar talle se i hosseturconna, ar eques: “Paulo, i nútina nér, yalde ni ar arcane nillo i talumnen nessa nér sina lyenna, an samis nat quetien lyen.”
I hosseturco nampe márya ar lende oa véra nómenna ar maquente: “Mana samil nyarien ninna?”
Hé quente: “I Yúrar uo acárier pano in arcuvalte in enwa taluval Paulo undu i Tára Combenna, ve qui merilte pare ambe tancave mana acáries.
Mal elye áva lave tien şahta lye mir carie ta, an ambe lá *canaquean neri mici te cáyar nurtane hópala se, ar unútielte inte vandanen in ualte matuva hya sucuva nó anahtieltes, ar sí nalte manwe, hópala şáquetielyan.”
Tá i hosseturco oante i nessa nerello apa canie sen: “Áva quete aiquenna i ápantiel nati sine nin.”
Ar yaldes atta i *tuxanturion ar quente: “Á manwa ohtari tuxa atta menien Césareanna, as roqueni *otoquean ar neri tuxa atta i colir hatili, i neldea lúmesse i lómio.
Ente, á tala roccoli. Sie, lávala Paulon norta, taluvaltes mi varnasse Felix i nórecánonna.”
Ar tences menta arwa sine quettaron:
“Claurio Lisias i taura nórecánonna, Felix: Hara máriesse!
Nér sina náne mapana lo i Yúrar ar náne hare návenna nahtana lo te, mal lantanen téna as i ohtari ar se-rehtane, pan parnen in náse Rómea.
Ar merila ista i casta nurrelto ana se, tallenyes undu mir Tára Combelta.
Hirnen i nurreli pa costeli apila Şanyelta nar talane senna, mal pa munta valda qualmeo hya limilion.
Man pan pano ana nér sina anaie apantana nin, mentanenyes lyenna pen hopie, ar canin i samir nurreli pa se i mauya tien quete ana se epe lyé.”
Etta ohtari sine namper Paulo, ve náne tien cánina, ar taller se lómisse Antipatrisenna.
I hilyala auresse lávelte i roquenin lelya óse en, ar nanwenner i *estolienna.
I roqueni lender mir Césarea ar antane i menta i nórecánon, ar panyanelte yú Paulo epe se.
Apa cendanes i menta maquentes pa mana ménallo hé túle, ar parne i hé náne Ciliciallo.
Eques: “Hlaruvan lye, íre i queni i samir nurreli pa lyé yú tuluvar.” Ar cannes i mo hepumne se mandosse mí túrion Herolwa.
Apa rí lempe Ananías i héra *airimo túle undu as amyárali ar *carpando yeo esse náne Tertullo, ar quentelte ulco pa Paulo.
Íre anes yálina, Tertullo *yestane carpa ana se, quétala:“Túra raine samilme ter lyé, ar envinyatiéli martar nóre sinasse apacenelyanen!
Mi ilye lúmi ar nómi, taura Felix, camilme tai antúra hantalénen.
Mal lá mapien amba lúmelyo, arcan lyenna i me-lastuval şintave, raina ve nalye.
An ihírielme i nér sina qolúvie ná, tyarila amortiéli imíca ilye i Yúrar ter i quanda ambar, ar náse minda mí heren i Nasaryaron,
quén ye yú néve vahta i corda, ar ye nampelme. Mernelme name se Şanyelmanen,
mal Lísias i hosseturco túle ar se-nampe et málmalto ormenen ar se-talle oa.
Ho hé ece elyen, ceşiénen hé, pare pa ilye i ongwi yar quetilme in acáries.”
I Yúrar náner astarindoryar ar quenter i sie enge.
Ar Paulo, íre i nórecáno hwermenen caryo láve sen carpa, hanquente:“Istala mai i nóre sina asámie lye ve námo ter rimbe loali, quetin lérave inyen.
Ece lyen pare i rie auri yunque yá lenden ama *tyerien Yerúsalemesse.
Ualte hire ni i cordasse arwa costo as aiquen hya tyárala amortie, i *yomencoassen hya i ostosse.
Ente, ua ece tien tana in acárien i ongwi pa yar sí carpalte ana ni.
Mal nat sina pantave nyarin lyen: I Tienen ya estalte 'heren', sánen *veuyan Eru, savila ilye i nattor i nar i Şanyeo ar técine mí Erutercánor.
Ar samin estel Erunna, i imya estel ya yú neri sine samir: Euva enortave failaiva ar úfailaiva véla.
Etta ennenya ná in inye illume samuva poica *immotuntie Erunna ar atannar.
Apa loali váner, túlen talien annali oraviéva nórenyanna, ar *yacien.
Lan cáranen nati sine, hirnelte ni poitana i cordasse, mal lá enge şanga hya amortie. Mal enger tasse Yúrali Asiallo,
ar náne arya qui té túler epe lye quetien pa ongwenyar, qui samilte nur ninna.
Hya lava sine nerin quete mana ulco hirnelte nisse íre tarnen epe i Tára Combe,
qui únes sina erya quetie ya yámen íre tarnen epe te: 'Pa i enortave qualinaiva nanye námaina epe le síra!'
Mal Felix, istala mai i nattor yar amper i Tie, querner i neri oa ar quente: “Íre Lisias i hosseturco tuluva undu, caruvan cilmenyar pa i nattor yar apir lye.”
Ar cannes i *tuxanturen hepe i nér mi moica lé, ar uas pustumne aiquen mici véraryar *veviello se.
Apa réli Felix túle as Rusilla verirya, ye náne Yúra, ar talles Paulo ar lastane senna pa i savie mi Hristo Yésus.
Mal íre carampes pa failie ar *immoturie ar i tulila namie, Felix náne ruhtana ar hanquente: “Lú sinan á auta, mal íre samin ecie, lye-tultuvan ata.”
Mal mi imya lú sámes i estel i camumnes telpe Paulollo. Etta se-tultanes ta ambe rimbave, ar carampes óse.
Mal apa loa atta vánet, Felix náne hilyana ve nórecáno lo Porcio Festo, ar pan Felix merne same i Yúraron lisse, hehtanes Paulo nútina.
Etta Festo, íre camnes i túre or i ména, apa rí nelde lende ama Yerúsalemenna Césareallo,
ar i hére *airimor ar i mindar i Yúraron nyarne sen nurriltar Paulonna. Tá arcanelte Festonna,
cestala lis ana Paulo, i mentauvas hé Yerúsalemenna. An caitumnelte nurtane, nahtien hé i mallesse.
Mal Festo hanquente i Paulo lemyuuva hépina Césareasse, ar in isse nanwenuva tar ron.
Eques: “Etta nai i taurar mici le tuluvar undu quetien pa ongweryar, qui ea *aiqua úşanya pa se.”
Sie, apa lemie aselte ter rí lá or tolto hya quean, ununtes Césareanna, ar i hilyala auresse hamunes i námosondonna ar canne i mo taluva Paulo.
Íre túles, i Yúrar i náner tulinwe undu Yerúsalemello tarner *os se, quétala in acáries rimbe ar lunge ongweli, mal lá ence ten tana i ta nanwa né.
Mal Paulo quente, varyala inse: “Ana i Şanye hya ana i Yúrar hya ana i Ingaran véla uan acárie erya úcare.”
Festo, ye merne came i mára nirme i Yúraron, hanquente Paulonna: “Ma meril lelya ama Yerúsalemenna náven námina tasse epe ni pa nattor sine?”
Mal eque Paulo: “Táran epe i námosonda i Ingaranwa, yasse mauya nin náve námina. Acárien munta ulca ana i Yúrar, ve yú elye mai ista.
Qui é acárien ongwe ar nanye valda qualmeo, uan arca uşe qualmello, mal qui uan acárie erya i ongwion yar quetilte acárien, *úquen lerta anta ni mir málta. Panyan nattonya epe i Ingaran.”
Apa quetie as i combe, Festo hanquente: “Epe i Ingaran apánel nattolya, i Ingaranna menuval.”
Apa réli váner, Árippa i aran yo Vernice túlet Césareanna *suilien Festo.
Apa anette tasse ter nóte rélion, Festo nyarne i aranen pa i nattor yar amper Paulo, quétala:“Ea nér hehtana nútina lo Felix,
ar íre anen Yerúsalemesse i hére *airimor ar i amyárar i Yúraron nyarner ni pa se, arcala i namuvan se ve arwa cámo.
Mal hanquenten téna i mici Rómear ua haime anta aiquen olla, nó i quén pa ye exeli quetir in acáries ongweli vele i quetir ana se cendello cendelenna, ar ece sen quete insen pa i nur.
Etta, íre ocomnelte sisse, pen hautie hamunen i námosondasse ar canne i mo taluva i nér.
Tárala epe ni, i quetir in acáries ongweli uar carampe pa i olce nati yar inye intyane pa se.
Sámelte costeli óse pa véra *tyerielta i ainova, ar pa quén estaina Yésus, ye qualina ná, mal ye Paulo quente ná coirea.
Lá istala mana carumnen pa i cos sine nation, maquenten qui mernes lelya Yerúsalemenna, tasse náven námina pa nattor sine.
Mal íre Paulo arcane náve hépina i námien Meletyaryo, cannen i nauvas hépina tenna mentauvan se i Ingaranna.”
Tá Árippa quente Festonna: “Yu inye mere hlare i nér.” Hanquentes: “Enwa hlaruvalyes.”
Etta, i hilyala auresse, Árippa yo Vernice túlet mi túra alcar ar lendet mir i şambe veliéva as hosseturcoli ar minde nelli i ostollo, ar íre Festo canne, Paulo náne talana.
Ar Festo quente: “Aran Árippa ar elde illi i ear aselme, yétalde nér sina pa ye i quanda liyúme Yúraron uo acárie arcande nin, Yerúsalemesse ar sisse véla, yámala i uas valda coiviéno ambe.
Mal túnen in acáries munta valda qualmeo. Etta, íre nér sina panyane nattorya epe Meletyarya, namnen i mentauvan se.
Mal pa sé samin munta tanca tecien herunyanna. Etta ettallenyes epe lé, ar or illi epe elye, Aran Árippa: Apa i ceşie istuvan mana tecuvan.
An sanan i úhanda ná qui mentan nútina quén mal loita tea i nurri senna.
Árippa quente Paulonna: “Lertal quete.” Tá Paulo rahtane máryanen ar quente, varyala inse:
“Pa ilye i ongwi yar i Yúrar quetir i acárien, Aran Árippa, nótan inse valima in epe elye síra quetin insen,
or ilqua i castanen in istal mai ilye i haimi ar costi mici Yúrar. Etta arcan i ni-hlaruval cólenen.
Lénya coiviéva néşenyallo – manen lenganen i yestallo nórenyasse ar mi Yerúsalem – ná sinwa lo ilye Yúrar
i isintier ni nóvo, qui merilte *vetta. Istalte i coivienya ve Farisa náne i amnaraca herennen *tyerielmo.
Ananta sí, castanen i vando antana atarilmain, táran náven námina.
Nosselmar yunque samir i estel i cenuvalte i vanda carna nanwa qui *veuyalte se veassenen ter lóme yo aure. Pa estel sina i Yúrar quetir i acárien ongweli, a Aran.
Mana castalda namien i mo ua pole save in Eru orta qualinar?
Inye é sanne i mauyane nin tare ana i esse Yésus Hristova Násaretello mi rimbe léli,
ve yú carnen Yerúsalemesse, ar rimbali i airion panyanen mandossen apa camnen túre i hére *airimollon, ar íre mo quente pa nahtie te, illume antanen ómanya ana te.
Ente, rimbave paimetánen te ilye *yomencoassen ar mauyane te naiquete, an nála ita rúşea ana te yú roitanenyet mir ettelie ostor.
Lan cáranen nati sine – mi lenda Lamascusenna, apa camie túre ar lavie i hére *airimollon –
endesse i aureo, en i mallesse, cennen cala han Anaro alcar ya caltane *os ni ar *os i lender asinye.
Ar apa illi mici me lantaner i talamenna hlassen óma quéta ninna i Heverya lambesse: 'Saul, Saul, mana castalya roitien ni? Petie ana i tildi nauva naica lyen.'
Mal quenten: 'Man nalye, Heru?' Ar i Heru quente: 'Inye Yésus, ye roital.
Ananta á orta ar tara talulyatse. An sina ennen atánienyexe lyen, lye-cilien ve núro ar *vetto yúyo yaron ecéniel ar yaron tyaruvan lyen cene, pa ni.
Etelehtan lye et lie sinallo ar i nórellon, yannar lye-mentan
pantien hendultat, te-querien morniello calanna ar Sátano túrello Erunna, camieltan apsenie úcariva ar ranta ve aryoni imíca i nar airinte saviénen nisse.'
Etta, aran Árippa, uan loitane cime i maur menello,
mal minyave innar ear Lamascusse ar tá innar ear mi Yerúsalem ar i quanda nóre pelila Yúrea tallen i menta i mauya tien hire inwis ar quere inte Erunna, cariénen cardar valde inwisten.
Pa nati sine Yúrali ni-namper i cordasse ar néver nahta ni.
Mal pan anen manyana lo Eru atárien *vettala pityan yo túran, mal quétala munta hequa yar i Erutercánor ar Móses véla quenter martumner:
I Hristo perperumne, ar ve i minya nála ortana qualinallon, carumnes cala sinwa lie sinan ar i nórin véla.»
Íre quentes nati sine varyala inse, Festo quente taura ómanen: “Nalye sámalyo ettesse, Paulo! Túra ñolme nancára sámalya!”
Mal Paulo quente: “Uan sámanyo ettesse, taura Festo, mal quetin quetiéli nanwiéva ar máleva sámo.
An i aran hanya nati sine, isse yenna quétan veriénen, an nanye tanca i munta sino uşe tuntierya, an nat sina ua anaie carna vincasse.
Ma elye, Aran Árippa, save i Erutercánossen? Istan i savil.”
Mal Árippa quente Paulonna: “Mi şinta lúme quetumnel ni mir ole *Hristondur.”
Tá Paulo quente: “Ece nin mere Erunna i ronhya ambe telwave, lá rie elye mal yú illi i hlárar ni síra oluvar taiti ve inye ná, ómu pen limili sine.”
Ar i aran oronte, ar sie carner i nórecáno ar Vernice ar i neri i hamnelyaner aselte.
Mal íre lendelte oa carampelte quén as i exe, quétala: “Nér sina care munta valda qualmeo hya limilion.”
Ente, Árippa quente Festonna: “Ence lehta nér sina au uas panyane nattorya epe i Ingaran.”
Apa i cilme náne carna i cirumnelme Italianna, antanelte Paulo ar hyane nútine nelli olla *tuxanturen estaina Yúlio, i hosseo i Meletyaryo.
Lendelme mir cirya ho Arramittium ya cirumne i nómennar i falasseo Ásio. Cirnelme oa, ar aselme enge Aristarco i Maceronya, Şessalonicallo.
Ar i hilyala auresse rahtanelme Síronna, ar Yúlio tanne lisse Paulon ar láve sen lelya nilmoryannar ar came aşielta.
Ar cirnelme oa talo ar lender nu i cauma Ciprusso, pan i súri náner ana me.
Ar apa cirnelme ter i eare ara Cilicia ar Pamfilia túlelme hópanna Mírasse mi Licia.
Mal i *tuxantur hirne cirya Alexandriallo ya cirumne Italianna, ar cannes men lelya mir ta.
Apa cirie lencave ter fárea nóte aurion ar tulie Cnirusenna urdave, pan sámelme i súre cendelemasse, cirnelme nu i cauma Créteo Salmonesse,
ar círala urdave ara i falasse túlelme nómenna estaina Vanime Hópar, hare i ostonna Laséa.
Anda lúme náne vanwa, ar sí enge raxe mi lelie earesse, an yando i Lamate vanwa né. Etta Paulo quente pa ya sannes náne i arya,
quétala téna: “Neri, tuntan i cirie taluva nancarie, ar olya nauva vanwa ven, lá pa i armar ar i cirya erinque, mal yú pa cuilelvar.”
Mal i *tuxantur cimne i hesto ar i nér ye haryane i cirya ambe lá i nati yar Paulo quente.
I hópa úne mára termarien ter i hríve, ar etta i amarimbar sanner i náne arya cirie oa talo, qui mi tana hya sina lé ence tien rahta tenna Fénix ar lemya tasse ter i hríve. Ta ná hópa Créteo ya ná panta forrómenna ar hyarrómenna.
Ente, íre i súre hyarmello vávane sannelte in ennelta enge rahtieltasse, ar tuncelte ama i *ciryampa ar cirner ara Créteo falasse.
Mal apa lá anda lúme alaco, ta ya estaina Euroaquilo ná, túle rimpa i tollallo.
I cirya náne mapaina ar ua polle hepe langorya i súrinna, ar pustanelme neve turitas ar náner cóline.
Íre túlelme mir i cauma pitya tollo estaina Caura, poldelme urdiénen rehta i lunte.
Apa ortanelmes mir i cirya *yuhtanelte i mauresorastar ar nunter *os i cirya *rappalínen. Caurelma náne in i cirya petumne i nurtane ondor Sirtesse; etta nampelte undu i pota-velunte ar náner cóline.
Mal i raumo ita lumnane men, ar i hilyala auresse hantelte i armar mir i ear,
ar i neldea auresse hantelte vére máltanten i ciryo sorastar mir i ear.
Íre ualme cenne Anar hya tinwi rimbe rélissen, ar lá pitya raumo caine menna, firne ilya estel rehtielmo.
Ar apa anda lúme yasse ualte mante matta Paulo oronte mici te ar quente: “Neri, carnelde arya au lastanelde ni, lá autala Crétello, mal úşala sina nancarie ar *úhepie.
Mal sí hortan le: Sama huore, an lá ea erya quén imíca le ye nauva vanwa! Rie i cirya nauva.
An mi lóme sina tarne epe ni vala Eruo yeo núro nanye, ar ye *tyerin.
Eques: 'Áva ruce, Paulo! Mauya len tare epe i Ingaran, ar yé! Eru ánie len illi i círar aselye.'
Etta sama huore, neri, an savin Eruo quetie, in i natto nauva aqua ve anaie nyárina nin.
Mal nauvalme hátine er tollo hrestanna”
Íre i *canaquea lóme lantane ar anelme hátaine sir ar tar Arria-earesse, endesse i lómio i ciryamor intyaner i anelte túlala hare er norenna.
Ar *lestanelte manen núra i ear náne ar hirner i anes rangwi *yúquean. Lendelte şinta tie ompa ar *lestaner ata, ar hirnelte rangwi lepenque.
Ar pan runcelme i cé petumnelme i ondor, hantelte mir i ear *ciryampar canta i pontillo i ciryo ar milyaner tulesse aureo.
Mal i ciryamor, cestala uşe i ciryallo, panyaner i lunte i earesse, quétala i lingumnelte *ciryampali i langollo.
Paulo quente i *tuxanturenna ar i ohtarinnar: “Qui neri sine uar lemya i ciryasse, ua ece len náve rehtane!”
Tá i ohtari cirner i *rappar i lunteo ar láver san lutta oa.
Íre aure hare né, Paulo hortane illi i matumnelte, quétala: “Síra ná i *canaquea ré yasse hópalde ú matto, an amátielde munta.
Etta arcan lello i matuvalde matta an ta manyuva rehtielda. An erya finde i caro aiqueno mici le lá nauva vanwa.”
Apa quetie quettar sine nampes massa, antane hantale Erun epe te illi, ar sa-rance ar *yestane mate.
Tá sámelte alasse, ar yú té manter matta.
Quanda nótelma i ciryasse náne enque *otoquean ar tuxa atta.
Apa matie i matta ya mernelte, carnelte i cirya mis lunga hatiénen i ore mir i ear.
Íre aure túle, ualte sinte i nórie, mal cennelte londe yasse enge hresta, ar mernelte tala i cirya tar, qui ence tien.
Etta aucirnelte i *ciryampar ar láver tain lanta mir i ear, ar mí imya lú lehtanelte i lattar i *tullo. Tá ortanelte i pota-velunte i súrinna ar lender ana i hresta.
Íre lantanelte nómenna yanna ear pente yúyo tiello, tallelte i cirya talamenna. I lango náne tanca ar ua polde leve, mal i ciryo ponte náne rácina mir rantar ormenen i earo.
Tá i ohtari merner nahta i nútine neri, pustien aiquen uşiello *lutiénen oa.
Mal i *tuxantur merne tala Paulo varnassenna ar pustaner te cariello ya mernelte. Ar cannes in sinter *lute i capumnelte mir ear, rahtien norenna minyave.
I exi hilyaner, ennoli panolissen, ennoli natalissen i ciryallo. Ar sie illi náner talane varnave norenna.
Apa rahtanelme varnassenna parnelme in i tollo esse náne Melite.
Ar i queni tasse, i quenter lambe ya ualme hanyane, lenganer menna lissenen han ya senwa ná, an nartanelte ruine ar camner me illi, castanen i ulo ya lantane, ar i ringiéno.
Mal íre Paulo hostane loxe olwennelíva ar panyane tai i ruinesse, leuca ettúle i úrenen ar tancexe máryanna.
Íre i queni tasse cenner i hloirea onna lingala máryallo, quentelte quén i exenna: “Tancave nér sina nahtar ná, ar ómu anes rehtana i earello, umbar ua láve sen coita.”
Mal palles i hloirea onna mir i ruine ar perpére munta olca.
Ono sannelte i pulumnes, hya i lantumnes rincanen qualina. Apa hopie andave ar cenie munta olca martala sen, vistanelte sámalta ar quenter i náse aino.
Hare tana nómenna i tollo amminda nér, estaina Poplio, sáme restali. Camnes me mai ar nildave láve men mare coaryasse ter rí nelde.
Mal Poplio atar náne caimassea úrenen ar *hruhirdiénen. Paulo lende minna senna ar hyamne, panyane máryat sesse ar nestane se.
Apa ta martane, i hyane queni i tollasse i sámer hlaiweli yú túler senna ar náner nestane.
Epeta yú *alcaryanelte me rimbe annalínen, ar íre cirnelme oa, antanelte men natali maurelmain.
Apa astar nelde cirnelme oa ciryasse ya túle Alexandriallo. Termarnelyanes ter i hríve i tollasse, ar sámes i Onóni ve i langoemma.
Túlelme Siracusenna ar lemner tasse ter rí nelde.
Talo lendelme *os i falasse ar túler Rehiumenna, ar apa er ré enge súre hyarmello, ar tennelme Puteolisse mí attea ré.
Tasse hirnelme hánoli i arcaner i lemyumnelme aselte ter rí otso, ar sie túlelme mir Róma.
Ar talo i hánor, íre hlasselte i sinyar pa me, túler me-velien tenna Forum Appii ar i *Sendassi Nelde. Paulo, cénala te, antane hantale Erun ar sáme huore.
Íre lendelme mir Róma náne lávina Paulon mare vérave, as i ohtar tirila se.
Mal apa rí nelde yaldes i náner imíca i minde neri i Yúraron. Apa ocomnelte quentes téna: “Neri, hánor, ómu fcarnen munta ana i lie hya i haimi atarilvaron, inye náne antana olla Yerúsalemello mannar Rómearon.
Té, apa carie ceşie, merner lerya ni, pan ua enge cáma valda qualmeo nisse.
Mal íre i Yúrar quenter ana ta, mauyane nin panya nattonya epe i Ingaran – mal lá ve au samin *aiqua ulca quetien ana nórenya.
Casta sinanen etta arcanen le-vele ar quete lenna, an castanen i estelo Israélo cólan limil sina.”
Quentelte senna: “Ualme acámier mentar pa lyé Yúreallo; ente, *úquen i hánoron i utúlier equétie *aiqua olca pa lye.
Mal sanalme i vanima ná hlare lyello mana sanwelyar nar, an é istalme pa heren sina i mi ilya nóme mo quete ana sa.”
Tancelte aure yasse velumneltes ar túler yanna marnes, rimbe lá ya anelte mí minya lú. Arinello şinyenna nyarnes ar *vettanes tien pa Eruo aranie, ar i Şanyello ar i Erutercánollon néves anta tien hande pa Yésus.
Ennoli mici te lastaner quettaryannar, mal exeli loitaner save.
Pan anelte şance oantelte, mal Paulo quente téna erya quetie: “Nanwave i Aire Fea carampe atarildannar ter Yesaia i Erutercáno,
quétala: 'Mena lie sinanna ar queta: É hlaruvalde, mal laume hanyuvalde; ar é yétuvalde, mal laume cenuvalde.
An lie sino enda olólie ilaica, ar hlarultat nát lunge hlariéno, ar hendultat apahtielte – te-pustala tuntiello hendultanten ar hlariello hlarultanten ar haniello endaltanen ar hiriello inwis, nestienyan te.'
Etta, nai nauva sinwa len i rehtie sina anaie mentana i nórennar; té yú hlaruvar.”
Ar apa quentes quettar sine, i Yúrar oanter, arwe túra costo mici inte.
Tá termarnes loa attasse mí coa yasse paityanes marien, ar camnes illi i túler senna,
carila Eruo aranie sinwa ar peantala pa i Heru Yésus Hristo ilya veriénen, pen aiquen pustala se.
Paulo, Yésus Hristo mól, yálina náven apostel, sátina Eruo evandilyonen,
pa ya antanes vanda nóvo ter Erutercánoryar i airi tehtelessen,
pa Yondorya, ye oronte Laviro erdello i hrávenen,
mal ye náne panyana ve i taura Eruion i Fairenen aireva, ortavénen qualinallon – Yésus Hristo Herulva,
ter ye camnelme lisse ar nóme ve apostel, ontien cimie i saviéva mici ilye i nóri pa esserya,
mici yar elde nar Yésus Hristo yálinar –
illinnar i ear Rómesse ve Eruo meldar, yáline náven airi: Nai samuvalde lisse ar raine ho Eru Atarelva ar i Heru Yésus Hristo.
Minyave antan hantale Ainonyan ter Yésus Hristo pa illi mici le, an mo carpa pa savielda i quanda mardesse.
An Eru, yen nanye núro fairenyanen pa Yondoryo evandilyon, astaronya ná pa manen pen hautie estan le illume hyamienyassen,
arcala i teldave ecuva nin tule lenna, Eruo indómenen.
An milyan cene le, antaven len anna faireva náveldan tulce,
hya i quaptalve tiutale, ilquen i exeo saviénen, savielda ar savienya véla.
Mal hánonyar, uan mere i penuvalde i istya i mernen tule lenna rimbe lúlissen – mal anaien hampa tenna sí – camien lesta yáveva yú mici le, ve mici i hyane Úyúrar.
Hellenyain ar Varvaryain véla, sailain ar aucoin nanye rohtando.
Etta uryan carien sinwa i evandilyon yú len i ear Rómesse.
An uan nucumna pa i evandilyon, an nas Eruo túre rehtien ilquen ye save, i Yúra minyave, tá i Hellenya.
An sasse Eruo failie ná apantaina saviénen ar mir savie, ve ná técina: “Mal i faila – saviénen coituvas.”
An Eruo rúşe ná apantaina menello ilya *ainolórienna ar úfailienna atanion i hepir i nanwie undu mi úfaila lé,
an ya mo pole ista pa Eru ná carna sinwa mici te, an Eru carne sa sinwa tien.
An úcénima nasserya ná aşcénima i ontiello mardeva, an nalte tuntaine i cárine natinen, yú oira túrerya ar valasserya. Etta ualte pole varya inte,
an ómu sintelte Eru, ualte antane sen i alcar hya hantale yain náse valda ve Aino, mal sanweltar oller luste ar mornie quante úhanda endalta.
Ómu quentelte pa inte i anelte saile, ollelte aucor
ar querne Eru Ilfirino alcar mir nat ve emma fírima atano – ar aiwion ar lamnion ar hlicila onnaron.
Etta Eru antane te olla, ve endalto íri náner, úpoicien ya tyarne hroalto alcar auta imíca inte,
té i quaptaner Eruo nanwie i hurun, ar *tyerner ya náne ontana or ye ontane, ye ná aistaina tennoio. Násie!
Etta Eru antane te olla úmáre mailin, an i nissi mici te quaptaner i lé nasselto lén ara nasse,
and sie yú i neri hehtaner nasseo *yuhtie i nisseva ar náner nartane maileltanen quén i exeva, neri as neri, carila ya quarca ná, ar camila intesse i *paityale yan anelte valde loimaltanen.
Ar pan úne mára hendultatse same istya pa Eru, Eru antane te olla úmirwa sáman, carieltan úmani nati,
an anelte quátine ilya úfailiéno, olciéno, milmeo, ulcuo – queni quante *hrúceno, nahtiéno, costo, *huruleo, olca óreo – hlussala,
quetila ulco pa exi, tevila Eru, narace, turquime, laitala inte, autala ulce nati, lá cimila nostaru,
pen hande, racila quettalta, úméle, pen oravie.
Ómu istalte Eruo namna – i té i carir taiti nati nar valde qualmeo – lá rie carilte tai, mal yú antalte penestalta in carir tai.
Etta ualye same casta antaven, ilya quén ye name. An i natanen yanen namil exe, namil imle ulca, pan elye ye name care i imye nati.
Mal istalve, ve i nanwie ná, in Eruo namie lanta innar carir taiti nati.
Mal ma intyal, a atan ye name i carir taiti nati ómu yú elye care tai, in elye uşuva Eruo namie?
Hya ma nattiril i úve moicieryo ar lavieryo ar cóleryo, pan ual ista in Eruo nildie néva tulya lye inwistenna?
Mal hrangielyanen ar endanen pen inwis comyal lyen rúşe i auresse rúseva, íre Eruo faile namier nauvar apantane.
Ar antauvas ilquenen ve cardaryar nar:
oira coivie in cestar alcar ar laitie ar ilfirin sóma, voronwiénen mára moliesse,
mal in melir cos ar uar hilya i nanwie, ono hilyar úfailie, euvar rúşe ar aha,
roitie ar şangie, i feanna ilya queno ye care ulco, i Yúro minyave ar yú i Hellenyo;
mal alcar ar laitie ar raine ilquenen ye care márie, i Yúran minyave ar yú i Hellenyan.
An Eru ua cime cendeler.
An i úcarner pen şanye yú nauvar nancarne pen şanye, mal illi i úcarner nu şanye nauvar námine şanyenen.
An i hlarir şanye uar i failar epe Eru, mal i carir şanye nauvar cárine faile.
An quiquie Úyúrar i uar same şanye nassenen carir yar i şanye quete, queni sine, ómu ualte same şanye, nar şanye inten.
Tanalte i samilte i şanyeo nattor técine endaltasse, íre *immotuntielta *vetta aselte ar, vére sanweltassen, ore tien i colilte cáma hya cé penir cáma
– i auresse ya Eru name atanion nuldar i evandilyonnen ya inye acárie sinwa, ter Yésus Hristo.
Mal qui elye, ye estaina Yúra ná ar sere şanyenna ar laita imle Erusse,
ar istal indómerya ar ece lyen name mana mára ná pan acámiel peantie et i Şanyello,
ar nalye tanca i nalye tulyala i laceníti, cala in nar morniesse,
peantala in penir hande, *peantar lapsion, arwa i canta istyo ar i nanwiéno i Şanyesse –
elye ye peanta exin, ma ual peanta imlen? Elye ye peanta: “Áva pile,” ma pilil?
Elye ye quete: “Áva race vestale,” ma racil vestale? Elye ye quete i feuyal cordoni, ma pilil cordallon?
Elye ye laitaxe i Şanyenen, ma racielyanen i Şanyeva vahtal Eruo alcar?
An ve anaie técina: “Eldenen Eruo esse naiquétaina ná imíca i nóri”.
É *oscirie aşea ná qui himyal i Şanye; mal qui racil i Şanye, *oscirielya olólie penie *osciriéva.
Qui, tá, quén ye ua *oscirna hepe i Şanyeo faile axani, ma penierya *osciriéva ua nauva nótina ve *oscirie?
Ar i *úoscirna quén ye sie ná nassenen, himiénen i Şanye namuva lyé ye same i técina tehtele ar i *oscirie ananta arácie i Şanye.
An isse ua Yúra ye sie ná to i etse; ente, *oscirie ua ta ya ea to i etse, i hrávesse.
Mal isse ná Yúra ye sie ná i mityasse, ar *oscirierya ná ta i endava, fairenen ar lá tengwanen. Taite quén same laitalerya. lá atanillon, mal Erullo.
Mana, tá, arya ná i Yúran, hya mana aşea pa i *oscirie?
Rimbe nati, mi ilya lé. Minyave Eruo quettar náner panyane hepieltasse.
Tá mana? Qui ennoli mici te loitaner save, ma penielta saviéva cé care Eruo voronwe pen túre?
Laume! Mal nai Eru nauva hírina voronwa, ómu ilya atan nauva hírina *hurindo, an ná técina: “Tanien i nauval carna faila quettalyainen ar samuval apaire íre nalye námina.”
Mal qui úfailielva care Eruo failie aşcénima, mana quetuvalve? Lau Eru úfaila ná íre tanas rúşerya? Quétan ve atan.
Laume! Mana hyana lénen Eru namuva i mar?
Ananta, qui hurunyanen Eruo nanwie olólie ambe minda, alcareryan, manen ná in inye en ná námaina ve úcarindo?
Mana vi-pustuva quetiello: “Alve care ulqui, talien i márar!” Sie ennoli naiquetir pa vi, quetila in é carpalve sie, mal taiti queni camuvar faila námielta!
Tá mana? Ma nalme arya nómesse? Lau! An nóvo equétielme pa Yúrar ar Hellenyar véla in illi mici te nar nu úcare,
ve ná técina: “Lá ea faila atan, lá er;
ea *úquen ye same tercen, ea *úquen ye cesta Eru, lá erya quén.
Illi aránier, illi uo olólier úmirwe; ea úquen ye care moicie, lá erya quén.
Lanculta láta sapsa ná; lambaltanen uhúrielte. Hloire leucaron ea ca péltat,
ar antolta ná quanta hútiéno ar sáriéno.
Talultat nát linte ulyaven serce.
Atalante ar angayasse nar tieltassen,
ar i tie raineva ualte isintie.
Lá ea rucie Erullo epe hendultat.”
Istalve in ilqua ya i Şanye quete, quetis innar nar nu i Şanye, pustien ilya anto, ar i quanda mar nauva *námima Erun, paimen.
Etta cardainen şanyeo ua ea hráve ya nauva carna faila epe se, an şanyenen tule i istya úcareva.
Mal sí, pen şanye, Eruo failie anaie tanana, ve ná *vettaina lo i Şanye ar i Erutercánor –
Eruo failie saviénen Yésus Hristosse, illin i savir. An *úquen arya ná lá exe,
pan illi úacárier ar loitar Eruo alcar.
Ve anna, lisseryanen, nalte cárine faile i *nanwerenen Yésus Hristosse,
ye Eru panyane ve *cámayanca ter savie serceryasse. Sie tanumnes failierya, apseniénen i úcari yar martaner i vanwiesse
lan Eru tanne cóle, tanien failierya i lúmesse ya ea sí, náven faila yú íre quetis faila ye i saviéno Yésusse ná.
Masse, tá, i *immolaitie ná? Lá ea nóme san. Mana şanyenen? Ta cardaron? Lá, mal i şanyenen saviéva.
An notilve i atan came failie saviénen, lá cardainen şanyeo.
Hya ma Eru rie i Yúraron ná? Ma uas i Aino yú quenion i nórion? E náse i Aino yú quenion i nórion,
pan Eru ná er, ye quete faile i *oscirnar et saviello, ar i uar *oscirne, saviénen.
Ma sie panyalve oa şanye savielvanen? Laume, mal tulcalve şanye!
Mana, tá, quetuvalve pa Avraham, atarelva i hrávenen?
An au Avraham náne carna faila cardainen, lertiévanes laita inse, mal lá epe Eru.
An mana i Tehtele quete? “Avraham sáve i Hérusse, ar ta náne nótina sen ve failie.”
Mal yen mole i *paityale ná nótina, lá ve lisse, mal ve rohta.
Mal yen ua mole, mal save yesse care faila i *ainolóra, savierya ná nótina ve failie.
Sie yú Lavir quete pa i alasse i atano yen Eru note failie pen cardar:
“Valime nar i queni ion *şanyelóre cardar nar apsénine ar ion úcari anaier túpine;
valima i quén yeo úcare i Héru laume cimuva.”
Ma alasse sina túle rie *oscarinnar, hya yú queninnar i uar *oscirne? An quetilve: “Avrahámen savierya náne nótina ve failie.”
Manen, tá, sa náne nótina? Íre anes *oscirna, hya íre únes *oscirna? Lá íre anes *oscirna, mal íre únes *oscirna!
Ar camnes tanwa, *oscirie, ve *lihta failiéno i saviénen ya sámes íre en únes *oscirna, náveryan atar illion i sámer savie pen oscirie, tyarien failie náve nótina tien,
ar yú atar *oscirnaron, lá rie in himyar *oscirie, mal yú in hilyar Avraham atarelva i tiesse saviéno yasse vantanes nó anes *oscirna.
An lá Şanyenen Avraham hya erderya same i vanda pa olie aryon mardo, mal ter i failie saviénen.
An au i himyar şanye nar aryoni, savie olólie lusta ar i vanda anaie hehtana.
An i Şanye tyare rúşe, mal yasse lá ea şanye, yú lá ea ongwe.
Sie i vanda saviénen ná, náveryan Erulissenen, carien sa tanca ilya erden Avrahámo, lá rie ta ya himya i Şanye, mal yú ta ya himya Avrahámo savie. Náse i atar illion mici vi,
ve anaie técina: “Apánien lye atar rimbe nórion” – epe Eru yesse sáves, ye care i qualinar coirie ar yalir i nati yar lá ear ve au engelte.
Estel pella estelden sámes savie náveo atar rimbe nórion, ve náne quétina: “Sie nauva erdelya.”
Ar ómu savierya ua firne, túnes véra hroarya, ya náne ve qualina íre anes hare loannar tuxa, ar yú tunes manen qualin Saro móna né.
Mal Eruo vandanen uas talante peniesse saviéva, mal náne carna taura savieryanen,
ar anes tanca in isse ye antane i vanda yú sáme i túre carien ve hé quente.
Etta ta náne “nótina sen ve failie”.
Úne técina pa sé erinqua i ta náne nótina sen,
mal yú pa elve. Nauvas nótina elven – i quenin i savilve sesse ye ortane Yésus Herulva qualinallon.
Hé náne antana olla ongwelvainen ar náne ortana návelvan quétine faile.
Etta, pan anaielve quétine faile saviénen, nai samuvalve raine as Eru, Yésus Hristo Herulvanen.
Ter hé yú samilve ando saviéva tulyala i Erulissenna yasse sí tarilve; ar nai laituvalve inwe pa i estel alcaro Eruo.
É lá rie ta, mal nai yú laituvalve inwe pa i roitier, istala i roitie anta voronwie,
voronwie tulya tyastana sómanna, ar tyastana sóma anta estel.
Ar i estel ua loituva, an Eruo melme anaie ulyana mir endalva i Aire Feanen ya náne antana ven.
An é Hristo, íre en anelve milye, qualle *ainolóre atanin íre i lúme túle.
An mo ua lintave cile quale yando quenen ye faila ná – hya cé mo é verya quale quenen ye mane ná.
Mal Eru tana ven melmerya lé sinasse, i lan en anelve úcarindor, Hristo qualle ven.
Pan anaielve cárine faile serceryanen, ta ita ambe nauvalve rehtane issenen i rúşello.
An qui, íre anelve ñottor, raine náne tulcana imbi vi ar Eru i qualmenen Yondoryo, ta ita ambe nauvalve rehtane coivieryanen sí íre samilve raine óse.
Ar lá rie ta, mal yú laitalvexer Erusse, ter Yésus Hristo Herulva, ter ye raine sí anaie tulcana ven.
An úcare enge i mardesse nó i Şanye, mal úcare ua nótina aiquenna íre lá ea şanye.
Ananta qualme sáme túre Atanello tenna Móses, yú or i uar úcarner mí lé ongweo Atano, ye ná emma yeo tulumne.
Mal natto i ongweo ua ve natto i anno. An ómu rimbali qualler er queno ongwenen, i lisse Eruo ar i anna i lissenen er queno – Yésus Hristo – náner ita ambe úvie rimbalin.
Ente, i anno natto ua ve i natto yeo úcarne. An i anan hilyala erya ongwe talle namie cámava, mal i anna hilyala rimbe ongwi tulya návenna carna faila.
An qui i er queno ongwenen qualme sáme túre issenen, ita ambe i camir lisseryo úve ar i anna failiéva turuvar ter sana er quén – Yésus Hristo.
An ve er ongwenen ilye atani náner námine ulce, mi imya lé er faila cardanen ilye atani nar quétine faile, coivien.
An síve rimbali náner cárine úcarindor pan erya nér rance i canwar, tambe rimbali yú nauvar cárine faile pan erya quén cimne tai.
Mal i Şanye túle carien ongwe úvea. Ono yasse úcare úvea né, Erulisse náne en ambe úvea.
I enne náne i sive úcare turne qualmenen, tambe Erulisse turumne failiénen, tálala oira coivie Yésus Hristo Herulvanen.
Mana, tá, quetuvalve? Ma lemyuvalve úcaresse, carien Erulisse úvea?
Laume! Elve i qualler pa úcare, manen en coituvalve sasse?
Hya ualde ista in illi mici vi i náner sumbane mir Hristo Yésus náner sumbane mir qualmerya?
Etta anelve panyane sapsasse óse sumbieryanen. Sie, ve yú i Hristo náne ortana qualinallon i Ataro alcarnen, yú elve vantumner vinya coiviesse.
An qui utúlielve uo óse qualmesse ve qualmerya, yú nauvalve óse enortavesse ve enortaverya.
An istalve i yára atanelva náne tarwestana óse, panien oa i hroa úcareva, vi-pustien návello móli nu úcare.
An ye aquálie anaie leryana úcarello.
Ente, qui aquálielve as Hristo, savilve i yú coituvalve óse.
An istalve i sí, íre i Hristo anaie ortana qualinallon, uas quale ata; qualme ua ambe same túre or se.
An i qualme ya qualles, qualles pa úcare erya lú ar tennoio, mal i coivie ya coitas, coitas Erun.
Mi imya lé yú elde: nota inde ve qualine úcaren, mal ve coirie Erun, mi Hristo Yésus.
Etta áva lave úcaren ture fírima hroalda, lengieldan íreryainen.
Ente, áva anta hroarantaldar úcaren ve carmar úfailiéva, mal á anta inde Erun ve i coirear qualinallon, yú hroarantaldar Erun ve carmar failiéva.
An úcare ua le-turuva, an ualde nu şanye, mal nu Erulisse.
Mana hilya? Ma úcaruvalve, pan ualve nu şanye, mal nu Erulisse? Laume!
Ma ualde ista i qui antalde inde aiquenen ve móli, carien ya quetis, nalde móliryar pan carilde ya quetis – cé úcareo móli, tulyala qualmenna, cé canwacimiéno móli, tulyala failienna.
Mal na hantale Erun: Anelde móli úcareo, mal cimnelde holmo i nostale peantiéva yan anelde antane olla.
Apa anelde leryane úcarello, ollelde móli failiéno.
Carpan lenna mi lé Atanion, pan hrávelda milya ná. An ve carnelde hroarantaldar móli úpoiciéno ar şanyeraciéno, ya tulyane amba şanyeracienna, sí á panya hroarantaldar ve móli failiéno, ya tulyuva airenna.
An íre anelde móli úcareo, anelde lére pa failie.
Mal mana náne i yáve ya tá sámelde? Nati pa yar nalde sí nucumne, an i metta tane nation qualme ná!
Ono sí, pan anelve leryane úcarello mal olólier móli Erun, samilde yávelda tulyala airenna, ar i metta nauva oira coivie.
An ya úcare paitya qualme ná, mal Eruo anna oira coivie ná, mi Hristo Yésus Herulva.
Hya ualde ista, hánonyar – an quetin innar istar şanye – in i Şanye ture ve heru or atan ter i quanda lúme ya coitas?
An nís ye evérie nerenna ná nútina veruryanna lan i nér coirea ná, mal qui verurya quale, i nís ná lehtana i veruo şanyello.
Sie, tá, lan verurya coirea ná, mo quetumne in i nís arácie vestale qui antanesexe hyana neren. Mal qui verurya quale, náse léra i şanyello, ar uas race vestale qui veryas hyana nerenna.
Sie, hánonyar, yú elde oller qualine i Şanyen i Hristo hroanen, náveldan exeo, isse ye náne ortana qualinallon, colielvan yáve Erun.
An íre anelve i hrávesse, úcareo mailer yar náner valtaine i Şanyenen móler hroarantalvassen, vi-tyarila cole yáve qualmen.
Mal sí anaielve lertane i Şanyello, pan aquálielve ho ta yanen anelve nútine, návelvan núroli mi vinya lé, i fairenen, ar lá mí yára lé, tengwainen.
Mana, tá, quetuvalve? I Şanye úcare ná? Laume! Mal úcare uan sinte qui lá i Şanyenen. An milcie uan sinte au i Şanye ua quente: “Áva na milca!”
Mal úcare, arwa eciéno i axannen, ontane inyesse milcie ilya nostaleo, an oa şanyello úcare qualina né.
É enge lúme yasse anen coirea oa şanyello, mal íre i axan túle, úcare nanwenne coivienna, mal inye qualle.
Ar i axan ya náne coivien, ta hirnen náne qualmen.
An úcare, íre camnes ecie i axannen, ni-şahtane ar ni-nacante sánen.
Etta i Şanye aire ná, ar i axan aire ar faila ar mára.
Ma ya mára né tyarne nin qualme? Laume! Mal úcare carne sie, tyarila qualme nisse yanen mára ná, apantien úcareo nasse, ar náveryan *úcarunqua han lesta i axannen.
An ve istalve, i Şanye i Faireo ná, mal inye i hráveo ná, vácina nu úcarenna.
An ya molin uan hanya. An ya merin, ta uan care, mal ya tevin, ta ná ya carin.
Ono qui ya uan mere ná ya carin, náquetin as i Şanye i nas mára.
Mal sí ye mole sa ua ambe ní, mal úcare ya mare nisse.
An istan in inyesse, ta ná, hrávenyasse, mare munta mára; an ece nin mere, mal ua ece nin mole ya mára ná.
An i márie ya merin uan care, mal i ulco ya uan mere, ta carin.
Mal qui ya inye ua mere ná ya carin, ye care sa ua ambe ní, mal úcare márala nisse.
Sie hirin in ea şanye sina: Íre merin care ya vanima ná, ulco caita hare ninna.
É samin alasse Eruo Şanyesse mitya ataninyanen,
mal cenin hroarantanyassen hyana şanye ohtacarila sámanyanna ar ni-carila mól şanyen úcareo ya ea mi hroarantanyar.
Angayanda nér ye nanye! Man ni-rehtuva sina hroallo qualmeva?
Hantale Erun ter Yésus Hristo Herulva! É inye ná mól i şanyeo Eruo sámanyanen, mal hrávenyanen, i şanyeo úcareo.
Sie i nar mi Hristo Yésus lá nauvar námine ulce.
An i faireo şanye ya anta coivie mi Hristo Yésus eleryanie lye i şanyello úcareva ar qualmeva.
An ya i Şanye ua polle care, pan anes milya i hrávenen, Eru carne mentavénen véra Yondorya mi ya náne vávea *úcarunqua hráveo, ar pa úcare. Namnes úcare ulca i hrávesse.
Sie i Şanyeo faila axan polle ole nanwa elvesse i vantar, lá i hrávenen, mal i Aire Feanen.
An i nar i hráveo cimir i hráveo nattor, mal i nar i faireo, i faireo nattor.
Cime i hráve tala qualme, mal cime i faire tala coivie ar raine,
an ye cime i hráve ná cotya Erun, an i hráve ua luhta Eruo şanyen; é ta ua ece san.
Etta i nar i hrávesse uar pole fasta Eru.
Mal elde uar i hrávesse, mal i fairesse, qui Eruo faire mare lesse. Mal qui aiquen ua same i Hristo faire, isse ua senya.
Mal qui i Hristo ea lesse, i hroa é qualina ná, úcarenen, mal i faire coivie ná, failiénen.
Qui i faire yeo ortane Yésus qualinallon mare lesse, ye ortane Hristo Yésus qualinallon yú caruva fírime hroaldar coirie, faireryanen ya mare lesse.
Etta, hánor, é nalve rohtalie, mal lá i hráven, coitien i hrávenen.
An qui coitalde i hrávenen, qualuvalde, mal qui i fairenen nahtalde hroaldo cardar, coituvalde.
An illi i nar tulyaine lo Eruo faire, té nar Eruo yondor.
An ualde came mólo faire, ata tyarila caure, mal camnelde faire náveva cíline ve yondor, i faire yanen yamilve: “Appa, Atar!”
I faire imma astaro ná as fairelva i nalve Eruhíni.
Ar qui nalve híni, nalve yú aryoni – é aryoni Eruo, mal aryoni as i Hristo, qui perperilve óse, camielvan yú alcar óse.
Etta notin i perperier lúme sino ve munta sestane as i alcar ya nauva apantana elvesse.
An uryala írenen Ontie yéta ompa i apantienna Eruo yondoiva.
An Ontie náne panyana nu úaşea sóma, lá véra şelmaryanen mal lo ye panyane sa nu ta, ómu arwa estelo:
i yú Ontie imma nauva leryana návello mól hastiéno mir i alcarinqua lérie Eruhínion.
An istalve manen i quanda Ontie ñónea uo ar ea naicesse uo tenna sí.
Lá rie ta, mal yú elve i samir i minye yáve – ta ná, i Faire – elve ñonar inwesse, íre yétalve ompa návenna cíline ve yondor – i lehtie hroalvallo *nanwerenen.
An anelve rehtane estel sinasse; mal estel ya mo cene ua estel, an íre quén cene nat, manen samis estel san?
Mal qui samilve estel yan ualve cene, yétalve ompa sanna voronwiénen.
Mí imya lé i Faire yú vi-manya íre nalve milye. Ualve ista mana carumnelve mai arciénen sa, mal i Faire imma arca ven, alaquétime quettainen.
Mal ye cesta i endar ista mana i Faireo sanwe ná, pan Eruo şelmanen arcas i airin.
Istalve in Eru tyare ilye nati mole uo in melir Eru, márieltan – i nar yáline enneryan.
An té i sintes nóvo yú cildes nóvo, náveltan cátine ve i emma Yondoryo, carila sé i minnóna mici rimbe hánor;
mal té i cildes nóvo, yú yaldes, ar té i yaldes, yú carnes faile, ar té i carnes faile, yú carnes alcarinque.
Mana, tá, quetuvan pa nati sine? Qui Eru ea aselve, man taruva venna?
Ye ua oráve yando véra Yondoryasse mal antane se olla i márien illion mici ve, manen ná i uas yú antauva ven ilye hyane nati?
Man quetuva ulco innar Eru icílie? Eru ná ye equétie te faile;
man ná ye name te ulce? Hristo Yésus ná ye qualle; hya arya, náse ye náne ortana qualinallon, ye ea Eruo formasse, ye yú arca ven.
Man vi-mapuva oa i Hristo melmello? Ma şangie hya şahtie hya roitie hya saicele hya heldie hya raxe hya macil caruva sie?
Ve anaie técina: “Elyenen nalme nahtaine ter i quanda aure; anaielme nótine ve mámar nahtien.”
Ono mi ilye sine nati samilve i apaire, yenen méle vi.
An nanye tanca i munta – qualme hya coivie hya valar hya héri hya yar ear sí hya yar tuluvar, hya túri,
hya tumnie hya ilya hyana ontana nat – poluva vi-mapa oa Eruo melmello ya ea mi Hristo Yésus Herulva.
I nanwie nyáran i Hristosse – uan húra, pan *immotuntienya astarmonya ná asinye, i Aire Feasse –
i samin túra naire ar lemyala naice endanyasse.
An ece nin mere in inye náne aucirna ve húna i Hristollo márien hánonyaron, nossenya i hrávenen,
i nar Israelyar, i náner cíline ve híni ar samir i alcar ar i véri ar i antave i Şanyeva ar i núromolie ar i vandar,
té i samir i atari ar illon i Hristo same ontalerya i hrávenen. Nai Eru, ye or illi ea, nauva aistana tennoio! Násie!
Mal i natto ua ve au Eruo quetta oloitie. An lá illi i samir ontalelta Israello nar nanwave Israel.
Ente, illi mici te uar híni návenen Avrahámo erde, mal: “Ya nauva estaina erdelya tuluva ter Ísac.”
Ta ná, i híni i hrávesse uar nanwave Eruo híni, mal i híni i ear i vandanen nar nótaine ve i erde.
An i quetta vando sie né: “Lúme sinasse nanwenuvan, ar euva yondo as Sara.”
Ananta lá rie mi tana natto: I imya martane íre Revecca óne onóni i imya nernen, Ísac atarelva.
An íre en uatte nóne, ar uatte acárie márie hya ulco – tyarien Eruo enne pa i cilme linga, lá cardassen, mal yesse yale –
náne quétina senna: “I amyára nauva mól i amnessan.”
Aqua ve ná técina: “Mélen Yácov, mal Ésau téven.”
Tá mana quatuvalve? Ma ea úfailie as Eru? Laume!
An quetis Mósenna: “Oravuvan yesse oravuvan, ar tanuvan *ofelme yen tanuvan *ofelme!”
Etta i natto linga, lá yesse mere hya yesse nore, mal Erusse ye órave.
An i Tehtele quete Fáronna: “Sina ennen alávien lyen lemya, i lyesse tanuvan túrenya, ar carien essenya sinwa i quanda cemende.”
Sie é óravas yesse óravas, mal ye meris caris tarya.
Etta quetuval ninna: “Manen ná i en quetis in aiquen same cáma? An man opólie tare şelmaryanna?”
A atan – man, tá, elye ná ye hanquete nan Erunna? Lau ya anaie canta quetuva yenna acátie sa: “Mana castalya ni-carien sie?”
Hya ma i cemnaro ua same túre or i cén carien i imya umbollo vene alcarinqua ennen, ar exe ennen ya ua alcarinqua?
Mal á intya in Eru, ómu samis i túre tanien rúşerya ar carien túrerya sinwa, túra cólenen láve i venin rúşeva lemya, tai yar nar cárine náven nancárine,
carien sinwa úvea alcarerya i venin oraviéva, yar nóvo manwanes alcaren
– elve i yaldes lá rie i Yúrallon, mal yú et i nórellon.
Ta ná ve quetis Hoseasse: “I uar lienya estuvan lienya, ar ye úne melda, i melda;
ar i nómesse yasse náne quétina tien: 'Ualde lienya', tasse nauvalte estaine 'i coirea Eruo yondor'.”
Ar Isaia yame pa Israel: “Ómu Israelindion nóte ná ve earo litse, rie pitya lemyala ranta nauva rehtana.
An i Héru caruva onotie cemende, telyala sa ar nuhtala sa.”
Ente, ve Isaia quente nóvo: “Au i Héru Sevaot ua láve erde lemya ven, ollelve ve Sorom, ar anelve ve Omorra.”
Tá mana quetuvalve? Úyúrar, ómu ualte roitane failie, ihírier failie – i failie ya tule saviénen.
Mal Israel, ómu roitanelte şanye failiéva, loitaner rahta sana şanyenna.
Manen sie? I castanen i roitaneltes, lá saviénen, mal ve cardainen. Talantelte i ondonen taltiéva, ve ná técina: “Yé, panyan Sionde sar taltiéva ar ondo lantiéva, mal ye save sasse ua nauva nucumna.”
Hánor, endanyo íre ar arcandenya tien Erunna ná i nauvalte rehtane.
An nanye astarmo tien i samilte uryala súle Erun, mal lá istyanen.
An pan ualte ista Eruo failie, mal nevir tulca véralta, ualte panyane inte nu Eruo failie.
An Hristo i Şanyeo metta ná, i ennen in ilquen ye save nauva quétina faila.
An Móses tece pa i failie ye tule i Şanyenen: “I atan ye acárie sa coituva sánen.”
Mal i failie ya tule saviello quete sie: “Áva queta endalyasse: Man ortuva mir menel?” – ta ná, talien i Hristo undu –
“hya: Man untuva mir i undume?” – ta ná, talien Hristo ama qualinallon.
Mal mana sa quete? “I quetta hare lyenna ná, antolyasse ar endalyasse” – ta ná, i quetta pa i savie, ya elme carir sinwa.
An qui antolyanen teal savielya i Yésus Heru ná, ar savil endalyasse in Eru ortane se et qualinallon, nauval rehtana.
An i endanen mo save, ya tala failie; i antonen mo tea ya mo save, ya tala rehtie.
An i Tehtele quete: “Ye same estel sesse lá nauva nucumna.”
An i natto ua er i Yúran ar exe i Hellenyan; mal ea i imya Heru or illi, ye lára ná illin i yalir senna.
An “ilquen ye yale i Heruo essenen nauva rehtana”.
Ono manen yaluvalte senna yesse ualte save? Ar manen savuvalte sesse pa ye ualte ahlárie? Ar manen hlaruvalte qui penilte aiquen ye care sinwa i evandilyon?
Ar manen caruvaltes sinwa qui ualte mentane? Ve ná técina: “Manen vanime nát i talu ion talar máre sinyar mánaron!”
Mal illi mici te uar hilyane i evandilyon. An Isaia quete: “Héru, man sáve ta ya queni hlasser mello?”
Sie savie tule ho ta ya mo hlare, ar ta ya mo hlare tule i quetiénen pa Hristo.
Ananta maquetin: Ma é ualte ahlárie? É “mir i quanda cemen lammalta etelende, ar quetieltar mettannar ambaro.”
Mal maquetin: Ma Israel ua hanyane? Minyave Móses quente: “Tyarin *hrúcen mici le yanen ua nóre; tyarin le same rúşe úhanda nórenen!”
Mal Isaia ita canya ná ar quete: “Anen hírina lo i uar ni-cestane; anen carna sinwa in uar maquente pa ni.”
Mal pa Israel quetis: “Ter i quanda aure apantien mányat ana lie canwaracila ar váquetila.”
Maquetin, tá: Lau Eru hehtane lierya? Laume! An yú inye nér Israélo ná, Avrahámo erdeo, Venyamíno nosseo.
Eru ua hehtane lierya, ya nóvo sintes. Ma ualde ista ya i Tehtele quete pa Elía, íre arcas Erunna Israélen?
“Héru, anahtielte tercánolyar, anarcielte undu *yangwalyar, ar inye erinqua lemya, ar cestealte cuilenya.”
Mal mana i Eruquetta quete senna? “Alávien nerin húmi otso lemya nin, i uar uluhtayer occalta Vaalen.”
Mi lé sina, etta, yú lúme sinasse ea lemyala ranta, Erulisseo cilmenen.
Qui sa lisse ná, uas ambe cardainen; qui lá, i lisse uas ambe lisse.
Mana tá? Ya Israel cestea, ta uas eñétie, mal i cílinar eñétier sa. I exion tuntie olleilaica,
aqua ve anaie técina: “Eru ánie tien şúle núra húmeva, hendu yat uat cene ar hlaru yat uat hlare, tenna aure sina.”
Ente, Lavir quente: “Nai sarnolta nauva tien remma ar rembie ar casta lantien ar ahtarie;
nai hendultat oluvat morne loitieltan cene, ar illume cara pontelta cúna!”
Etta maquetin: Ma talantelte lantien aqua? Laume! Mal taltieltanen ea rehtie i nórin, tyarien *hrúcen mici te.
Sí íre taltielta tala almare i marden, ar návelta nehtane tala almare i nórin, manen ita ambe quanta nótelta caruva sie!
Sí quétan lenna, i Úyúrar: Nála ya nanye, é i nórion apostel, laitan núromolienya,
qui ea lé yanen polin tyare *hrúcen imíca i nar hrávenyo ar rehta ennoli mici te.
An qui návelta hátine oa tala rainecarie i marden, mana návelta cámine taluva, qui lá coivie i qualinallon?
Ente, qui i minya massa airinta ná, tá i quanda maxe aire ná. Ar qui i şundo aire ná, sie nar i olvar.
Ono qui olvali náner rácine oa, mal elye, ómu nalye ráva *milpialda, náne panyana mir i alda mici tai ar camne ranta i laro i şunduo i *milpialdo,
áva laita imle or i olvar! Mal qui laitalyexe or tai, enyala in elye ua cole i şundo, mal i şundo cole lyé.
Tá quetuval: “Olvali náner rácine oa návenyan panyana mir i alda.”
Mai: Peniénen saviéva anelte rácine oa, mal elye tare saviénen. Áva same varande noar, mal ruca!
An qui Eru ua láve i olvain i náner tasse nasseltanen lemya, mi imya lé uas lavuva elyen lemya.
Yé, tá, Eruo márie ar naracie: Innar lantaner ea naracie, mal elyenna ea Eruo márie, qui lemyalde márieryasse; qui lá, yú elye nauva aucirna.
Yú té, qui ualte lemya penieltasse saviéva, nauvar panyane mir i alda, an Eru pole panya te tar.
An qui anel hocirna *milpialdallo ya nassenen ráva ná, ar ara nasse anel panyana mir i *milpialda i tarwasse, manen ita ambe *aşcárima nauva in nassenen samir nómelta tasse náve panyane mir véra *milpialdalta!
An uan mere in elde, hánor, penuvar istya pa fóle sina, pustien le návello saile mi vére henduldat: Ea ranta Israélo ion tuntier olólier ilaice tenna utúlie i quanta lúme yasse i nórion quantie tuluva minna.
Ar sie quanda Israel nauva rehtana, ve anaie técina: “I *Rehtando tuluva Sionello ar queruva oa *ainolórie Yácovello.
Ar si ninya vére ná aselte, íre panyan úcariltar oa.”
Pa i evandilyon nalte ñottor castanen eldeo, mal pa i cilie nalte melde, castanen atariltaron.
An Eruo annar ar yalie uar nati pa yar samuvas tatye sanwi.
An ve elde vanwiesse rancer Eruo canwar, mal sí acámier oravie *canwaracieltanen,
sie yú té sí arácier i canwar, ya tyarne lé came oravie, i yú té sí camuvar oravie.
An Eru oiórie te illi canwaraciesse, oravien illisse mici te.
Yé manen tumne Eruo lar ar sailie ar istya nar! Manen *alacestime namieryar, ar manen *alahírime tieryar!
An “man sinte i Héruo sáma, hya man peantane issen?”
Hya, “Man minyave antane sen ar camuva *paityale nan?”
An sello ar sénen ar sen nar ilye nati. Issen na i alcar tennoio! Násie.
Etta hortan le Eruo *ofelmenen, hánor, in antauvalde hroaldar ve coirea, aire *yanca, fastala Erun: núromolie handaleldanen;
ar áva na cátaine sina randanen, mal na vistaine i envinyatiénen sámaldava, lavila len name mana Eruo indóme ná, i mára ar fastala ar ilvana.
An i Erulissenen antana nin nyarin ilquenen mici le: Áva sana pa imle ve tára lá ya mauya lyen sana, mal á sana mi lé antala mále sámo, ilquen ve Eru sen-ánie lesta saviéva.
An ve samilve er hroasse rimbe hroarantar, mal i hroarantar uar care i imya nat,
sie elve, ómu nalve rimbe, nar er hroa Hristosse, mal er ana er, quén i exeo hroarantar.
Samilve *alavéle annali i Erulissenen antana ven. Yen same apacen mauya *yuhta sa i lestanen saviéva sátina sen,
yen same núromolie mauya mole ve núro, i *peantaren mauya peanta,
hya yeo molie hortie ná, sen mauya horta; yen anta mauya anta alyave; yen tare epe i ocombe mauya panya endarya molieryasse; yen órava mauya care sie mi alasse!
Melme na anwa! Á feuya ya olca ná; á himya ya mára ná.
Hánomelmelda quén i exeva na tumna! Ilquen na tanuva i exin áya túra lá ya arcas!
Áva na lence, hehtala i horme; na uryala i fairesse, na móli i Herun!
Sama alasse i estelde! Na voronwe şangiesse! Á lemya hyamiesse!
Á anta ranta i airin, ve maureltar nar. Á hilya i melme etteleaiva!
Á aista i roitar – á aista ar áva húta!
Sama alasse as i samir alasse; na yaimie as i yaimear.
Na i imya sámo, quén i exenna; áva mere náve minde, mal na tulyaine lo i nalde nati. Áva ole saile vére henduldatse!
Áva anta aiquenen ulco ulcun! Á tala ompa mánar epe ilye atani!
Qui ta cárima ná, hepa raine as ilye atani, quiquie ta ece len.
Áva ahtare inden, mal á anta nóme i rúşen, an ná técina: “Ahtarie ninya ná, quete i Héru; inye paityuva nan.”
Mal “qui ñottolya maita ná, ásen anta nat matien; qui náse soica, ásen anta nat sucien; an cariénen sie comyal uruiti *hyulmali caryasse.”
Áva na turúna ulcunen, mal turua ulco mariénen.
Nai ilya quén panya immo nu i ambe táre héri, an lá ea hére hequa Erunen; i héri yar ear anaier panyane tasse lo Eru.
Etta ye tare i hérenna tare Eruo *partienna; i sanna atárier camuva námie intesse.
An i turir uar ruhtie i mára cardan, mal i ulcan. Elye mere i la mauyuva lyen ruce i hérello? Cara ya mára ná, ar camuval laitale sallo,
an nas Eruo núro márielyan. Mal qui caril ulco, tá ruca, an uas cole i macil ú casto. An nas Eruo tyaro, ahtarila túre ya tala rúşe yenna care ulco.
Etta samilde maure panien inde nu i hére, lá rie castanen i rúşeo, mal yú castanen *immotuntieldo.
An ta yú ná i casta yanen paityalde *tungwi, an nalte Eruo núror, oi cimila sina enne.
Á anta illin rohtaldar tien: *tungwe yen arca *tungwe, paitie yen arca paitie, áya yen arca áya, alcar yen arca alcar.
Áva sama rohta erya natwa aiquenen, hequa mele quén i exe, an ye mele armarorya aquantie i şanye.
An i axani: “Ávalye race vestale, Ávalye nahta, Ávalye pile, Ávalye na milca”,ar ilye hyane axani, samir nonwelta quetta sinasse: “Alye mele armarolya ve imle.”
Melme ua care ulco ana i armaro; etta melme i şanye carna nanwa ná.
Ente, cara sie pan istalde mana i lú ná, i yú sí utúlie i lúme cuivieldan húmello, an sí rehtielva ná hare epe ya anes íre camnelve savie.
Etta alve panya oa i cardar yar nar morniéno ar alve panya visse i carmar calava.
Ve auresse, alve lenga mai, lá mi verce merendi ar *accasucier, lá mi *hrupuhtie ar lehta lengie, lá mi costie ar *hrúcen.
Mal á panya lesse i Heru Yésus Hristo, ar áva care panor i hráveo írin.
Cama i nér ye ná milya i saviesse, mal lá namien mitye sanweryar.
Er quén same savie matien ilqua, mal ye milya ná mate quear.
Ye mate áva nattire ye ua mate, ar ye ua mate áva name ye mate, an Eru acámie se.
Man elye namien exeo núro? Véra heruryan taris hya lantas. É taruvas, an ece i Herun tyaritas tare.
Er quén name er ré ve túra lá hyana ré; hyana quén name er ré ve ilye hyane rí; nai ilquen nauva aqua tanca véra sámaryasse.
Ye hepe i ré, hepe sa i Herun, ar ye mate, mate i Herun, pan antas hantale Erun; ar ye ua mate, ua mate i Herun, ananta antas hantale Erun.
É *úquen mici vi coita insen, ar *úquen quale insen,
an qui coitalve, coitalve i Herun, ar qui qualilve, qualilve i Herun. Sie, qui coitalve hya qui qualilve véla, nalve i Heruo.
An sina ennen Hristo qualle ar nanwenne coivienna: náven Heru qualinaron ar coirearon véla.
Mal elye, mana castalya namien hánolya, hya mana castalya nattirien hánolya? An illi mici vi taruvar epe Eruo sonda anamo,
an ná técina: “Ve coitan, quete i Héru, ninna ilya occa cuvuva, ar ilya lamba laituva Eru.”
Sie ilquen mici vi antauva onótie pa inse Erun.
Etta ávalve name quén i exe, mal ambe ron cara si namielda: áva panya nat epe háno ya tyare se talta hya lanta.
An istan ar anaien lo i Heru Yésus tulyana i savienna i munta vahtana ná insasse; hequa íre atan note nat ve vahtana – issen nas vahtana.
An qui mattanen hánolya same naire, ualye ambe vanta melmenen. Áva mattalyanen nancare i quén yen Hristo qualle!
Etta áva lave aiquenen quete pa márielda yaiwenen.
An Eruo aranie ua matie ar sucie, mal failie ar raine ar alasse Aire Feanen.
An ye sie núro ná Hriston, fasta Eru ar anaie hirna mane lo atani.
Etta alve roita i nati yar tulyar rainenna ar i nati yar carastar quén i exe ama.
Áva mattan narca undu ya Eru acárie! Ilye nati é nar poice, mal nalte ulce atanen ye tunta sa ve lante íre matis.
Mára ná lá matie hráve hya sucie limpe hya carie *aiqua ya tyare hánolya lanta.
I savie ya samil, hepa sa imbe lyé ar Eru. Valima ná i quén ye ua tala namie insenna yanen quetis mára.
Mal qui savierya ua tanca, náse nóvo námina ulca qui matis, an uas mate et saviello. É ilqua ya ua et saviello úcare ná.
Mal i rohta elveo i nar tulce ná cole i milye sómar ion uar tulce, ar lá fasta inwe.
Ilquen mici vi na fastuva armaroryo, carastala se ama.
An yú i Hristo ua fastane inse, mal ve ná técina: “I narace quettar ion quenter naracave lyenna alantier ninna.”
An ilye i nati técine nóvo náner técine peantielvan, samielvan estel voronwielvanen ar i tiutalénen i Tehtelellon.
Sí nai Eru ye anta voronwie ar tiutale antauva len same i imya sóma sámo mici le ya Hristo Yésus sáme,
laitieldan er antonen Yésus Hristo Herulvo Aino ar Atar.
Etta cama quén i exe, ve i Hristo camne ví, Eruo alcaren.
An quetin in Eruo voronwenen i Hristo anaie núro in nar *oscírine, carien tance i vandar i atarin,
tyarila i nóri laita Eru castanen failieryo. Ve ná técina: “Etta *etequentuvan lyé imíca i nóri, ar esselyan linduvan!”
Ar ata quetis: “Sama alasse, nóri, as lierya.”
Ar ata: “Á laita i Héru, ilye nóri, ar nai ilye i lier laituvar se!”
Ar ata eque Isaia: “Euva Yesseo şundo, ar ye orta turien nóri; sesse nóri panyuvar estelelta.”
Nai Eru ye anta estel quantuva illi mici le alassenen ar rainenen pan savilde, samieldan úve estelwa, i Aire Feo túrenen.
Sí yú inye tulcave save pa lé, hánonyar, in elde nar quante máriéno, pan anaielde quátine ilya istyanen ar istar peanta quén i exe.
Mal pa natali técan len mi lé ita canya, ve qui tyarien le ata enyale – i lissenen antana nin Erullo.
Sie nauvan Yésus Hristo núro i nórin, molila ve *airimo Eruo evandilyonen, i ennen in i nóri nauvar fastala *yanca, airinta Aire Feanen.
Etta lertan laitaxe mi Hristo Yésus pa Eruo nati.
An uan verya nyare pa erya nat hequa yar Hristo acárie ter ní, ñetien canwacimie imíca i nóri – quettanen ar cardanen,
i túrenen tanwaron ar tannaron, i túrenen Aire Feo. Ar sie, Yerúsalemello ar rindesse tenna Illiricum, acárien sinwa i quanda evandilyon pa i Hristo.
Sie é carin ennenya in uan caruva sinwa i evandilyon yasse Hristo esse anaie quétina nóvo, pustien imne carastiello hyana queno talmasse.
Mal ve ná técina: “I queni in únes carna sinwa cenuvar, ar i uar ahlárie hanyuvar.”
Etta anen rimbave tapta tuliello lenna.
Mal sí, pan penin amba nóme mi ménar sine yasse polin care sinwa i evandilyon, ar pan imílien ter rimbe loar tule lenna,
samin estel sina: in íre lelyan Spanianna, ecuva nin vele le íre lahtuvan, ar i manyuvalde ni lelya tar íre nanye mi lesta quátina návenen aselde.
Mal sí lelyuvan Yerúsalemenna náven núro i airin.
An i ear Maceroniasse ar Acaiasse sanner mai anta ranta armaltaron i penyain Yerúsalemesse.
Care sie é náne mára mi hendulta, ananta anelte rohtandor tien, an qui i nóri samir ranta mi nattoltar faireva, yú rohtalte náve núror tien nattoinen i hráveo.
Etta, apa telyan si ar atálie comyane armar sine téna mi varnasse, autuvan ter vehtelda lendanyasse Spanianna.
Mal istan in íre tulin, tuluvan arwa quanta lesto aistiéva Hristollo.
Sí hortan le, hánor, Herulva Yésus Hristonen ar melmenen i faireo, i carilde ilqua ya polilde óni hyamiessen Erunna inyen,
návenyan etelehtana illon uar save Yúreasse, ar i núromolienya Yerúsalemesse nauva mára i airin.
Sie, íre tulin lenna arwa alasseo, Eruo indómenen nauvan envinyanta aselde.
Nai i Aino ye anta raine euva as illi mici le! Násie!
Antan len Foive néşalva, ye ná yú *veure i ocombeo mi Cencreai.
Áse came mi lé valda i airion, ar áse manya mi ilya natto yasse samis maure eldeva, an aváries rimbar, yú ní.
Á *suila Prisca yo Aquila, yet omóliet óni Yésus Hristosse,
yet panyanet véra axetta raxesse cuilenyan. Tún lá inye erinqua mal yú ilye i Úyúre ocombi antar hantale.
Ente, á *suila i ocombe coatto. Á *suila Epaineto meldanya, ye ná minya yáve Hriston Asiasse.
Á *suila María, ye acárie rimbe nati elden.
Á *suila Andronico yo Yúnia, yet nát nossenyo ar nánet óni mandosse. Samitte mára esse imíca i aposteli ar nánet Hristosse nó ni.
Á *suila Ampliáto, meldanya i Hristosse.
Á *suila Urváno, ye omólie óni Hristosse, ar Stacis meldanya.
Á *suila Apelles, ye anaie tyastana i Hristosse. Á *suila i queni mi coarya Aristovúlo.
Á *suila Herolion, ye nossenyo ná. Á *suila i queni mi coarya Narcisso i nar i Herusse.
Á *suila Trifaina yo Trifósa, yet molit i Herusse. Á *suila Persis, i melda, an acáries rimbe mótier i Herusse.
Á *suila Rúfo, i cílina i Herusse, ar amillerya ar ninya.
Á *suila Asincrito, Flehon, Hermes, Patrovas, Hermas, ar i hánor aselte.
Á *suila Filolóho ar Yúlia, Nereo ar néşarya, ar Olimpas, ar ilye i airi aselte.
Á *suila quén i exe aire miquenen! Ilye Hristo ocombi *suilar le!
Sí hortan le, hánor, i hepuvalde hendelda issen tyarir şancier ar taltier i peantiello ya camnelde, ar quera inde oa tello.
An taiti queni nar móli, lá Yésus Hriston, mál vére cumbaltain, ar vanime quettainen ar arwe lisso to lambalta tyarilte ranya i *cámalóraron enda.
An canwacimielda ná tuntaina lo illi. Etta nin-antalde alasse. Mal merin i nauvalde saile pa ya mára ná, mal *cámalóre pa ya ulca ná.
I Aino Raineva ron ascatuva Sátan nu taluldat. Nai Yésus Herulvo lisse euva aselde!
Timoşéo, ye mole óni, *suila len, ar sie carir Lúcio ar Yáson ar Sosípater, i nar nossenyo.
Inye, Tertio, ye etécie tecetta sina, *suila le i Herusse.
Aio, ye acámie ni ar i quanda ocombe mir coarya, *suila le. Erasto, i osto cáno, *suila le, ar sie care Quarto hánorya. 
Yésus Hristo Herulvo lisse na euva as illi mici le! Násie!
Yen pole hepe le tulce i evandilyonnen ya tallen ar cariénen sinwa Yésus Hristo, i apantiénen i fóleva ya anaie hépina quildesse ter oireli,
mal sí anaie tanana ar carna sinwa i Erutercánoron tehtelessen mici ilye i nóri Eru Oiro canwanen, tyarien i savie náve címaina
– Erun, saila erinqua, na i alcar ter Yésus Hristo tennoio. Násie!
Paulo, yálina náven Yésus Hristo apostel ter Eruo indóme, ar Sostenes hánolma,
Eruo ocombenna ya ea Corintesse – lenna i anaier airinte mi Hristo Yésus, yáline náven airi, as illi i ilya nómesse yálar i essenen Herulvo, Yésus Hristo, Herulta ar Herulma.
Nai samuvalde lisse ar raine ho Eru Atarelva yo i Heru Yésus Hristo.
Illume antan hantale Erun len, pa Eruo lisse antaina len mi Hristo Yésus,
i mi ilqua anaielde cárine lárie sénen, ilya quettasse ar istyasse,
pan i Hristo *vettie anaie carna tanca mici le.
Sie ualde pene erya anna, yétala ompa i apantie Herulva Yésus Hristova.
Sé yando caruva le tance tenna i metta, náveldan han napsa Yésus Hristo Herulvo auresse.
Eru ná voronda, lo ye anelde yáline samien ranta as Yondorya, Yésus Hristo Herulva.
Sí le-hortan, hánor, Yésus Hristo Herulvo essenen, i illi mici le quetuvar i imya nat, ar i uar euva şancier mici le, mal i nalde er mí imya sáma ar i imya sanwe.
An náne nyárina nin pa le, hánor, lo i queni coaryallo Hloe, i ear costiéli mici le.
Quétan pa nat sina, i ilquen mici le quete: "Inye ná Paulon", "mal inye Céfan", "mal inye Hriston".
I Hristo şanca ná. Lau Paulo náne tarwestaina len? Hya ma anelde *tumyaine Paulo essenen?
Antan hantale i uan *tumyane aiquen mici le hequa Hrispo ar Aio.
Tá ualde quetuva i anelde *tumyaine ninya essenen.
É *tumyanen yando nosserya Stefánas. Pa i exi uan ista i *tumyanen aiquen.
An Hristo ni-mentane, lá *tumien, mal carien sinwa i evandilyon, lá questo sailiénen, tyárala i Hristo tarwe pene túrerya.
An questa i tarweo ná *aucie in nauvar vanwe, mal ven i nar rehtaine nas Eruo túre.
An ná técina: "Tyaruvan i sailaron sailie auta, ar i handaron handasse panyuvan oa."
Masse i saila quén? Masse i parmangolmo? Masse ye mele quetta-cos mar sinasse? Ma Eru ua acárie i mardo sailie *auca?
An pan, Eruo sailiesse, i mar véra sailieryanen ua sinte Eru, náne mára Eruo hendusse i rehtumnes i savir i *auciénen ya carilme sinwa.
An Yúrar arcar tanwar ar Hellenyar cestar sailie,
mal elme carir sinwa Hristo tarwestaina – ya quere Yúrar oa ar ná *aucie i nórin.
Mal in nar i yálinar, Yúrain ar Hellenyain véla, náse Hristo, Eruo túre ar Eruo sailie.
An i *auca nat Eruo ná saila lá Atani, ar milya nat Eruo ná polda lá Atani.
An yétalde yalierya eldeva, hánor: i lá rimbali i náner saile mí hráveo lé náner yáline, lá rimbe taurali, lá rimbali nóne ve arator.
Mal Eru cille i mardo *auce nati, nucúmala i sailar; ar Eru cille i mardo milye nati, nucúmala i toryar,
ar Eru cille i mardo úartar ar i náner nattírine – té i nar munta – carien munta té i nar *aiqua.
Sie ua euva hráve ya laituvaxe epe Eru.
Mal sénen nalde mi Hristo Yésus, ye anaie carna ven sailie Erullo, ar failie ar airitie ar *nanwere.
Sie, ve anaie técina, 'Ye laitaxe, nai laituvas inse i Hérusse.'
Ar inye, íre túlen lenna, hánor, uan túle arwa vanye quettaron hya sailiéno íre carnen Eruo fóle sinwa len.
An carnen i cilme i uan merne ista erya nat imíca le hequa Yésus Hristo ar sé tarwestaina.
Ar túlen lenna nála milya ar cauresse ar túra paliesse,
ar questanya ar *nyardienya uar náne *mirquétala quettassen sailiéno, mal taniesse faireva ar túreva.
Sie savielda polle náve tulcaina, lá atanion sailiesse, mal Eruo túresse.
Sí quetilme sailie imíca i nar marine, mal lá randa sino sailie, hya ta ion turir mi randa sina – i nauvar nancarne.
Mal quetilme Eruo sailie fólesse, i nurtaina sailie, ya Eru tulcane nóvo nó i randar, alcarelvan.
Sailie sina *úquen ion turir mi randa sina isintie, an qui sinteltes, ualte tarwestane i Heru alcaro.
Mal ve ná técina: "Yar hen ua ecénie ar hlas ua ahlárie, yar atano enda ua sanne, tai Eru amanwie in se-melir."
An elven Eru apantane tai, faireryanen – an i faire şure mina ilye nati, yando Eruo tumne nati.
An man mici atani ista atano nattor hequa i atano faire ya ea sesse? Mí imya lé *úquen isintie Eruo nattor hequa Eruo faire.
Mal camnelve, lá i mardo faire, mal i faire ya tule Erullo, istielvan i nati yar lissenen nar ven antaine lo Eru.
Nati sine yando quetilve, lá quettainen peantaine atano sailiénen, mal tainen peantaine lo i faire, íre namilve i faireo nattor i faireo quettainen.
Mal atan turúna nasseryanen ua came yar tulir Eruo fairello, an sen nalte *aucie, ar uas pole ista tai, an nalte minaşurne mí faireo lé.
Mal atan faireva şure mina ilye nati, mal sé ná minaşurna lo *úquen.
An "man isintie i Héruo sáma, i polis peanta sen?" Mal elme samir Hristo sáma.
Ar inyen, hánor, ua ence quete lenna ve queninnar i faireo, mal ve queninnar i hráveo, ve lapsennar i Hristosse.
Ilin antanen len, lá tanca matso, an ta en ualde polle mate. É ena loitalde pole,
an nalde ena queni i hráveo. An íre ea *hrúcen ar cos imíca le, ma ualde i hráveo ar vantar ve atani carir?
An íre aiquen quete: "Inye ná Paulon," mal exe: "Inye ná Apollon", ma ualde lenga ve atani?
Mana, tá, Apollo? É mana Paulo? Núroli inen sávelde, ilya quén ve i Heru láve sen.
Inye empanne, Apollo ulyane nén, mal Eru tyarne sa ale.
Sie, ye empanya ar ye ulya nát yúyo munta; Eru ná ye anta alie.
Mal ye empane ar ye ulya nén nát er, mal ilya quén camuva véra *paityalerya, molieryanen.
An uo nalve Eruo núror. Mal elde nar resta Eruva, ataque Eruva.
Sí, Eruo lissenen ya anaie antaina nin, ve saila turco carastiéno apánien talma, mal exe carastea sasse. Mal nai ilquen cimuva manen carasteas sasse.
An *úquen pole panya hyana talma hequa ta ya ná panyaina, ya Yésus Hristo ná.
Sí qui aiquen carasta talma sinasse – maltanen, telpenen, mírinen, toanen, salquenen, şaranen –
ilqueno molie nauva tanaina, an i aure tanuva sa, ar nauvas apantaina ve nárenen, ar ilqueno molie, mana nostaleo sa ná, i náre imma tyastuva.
Qui aiqueno molie, ya acarasties sasse, lemya, camuvas *paityale.
Qui aiqueno molie ná urtaina, ta nauva vanwa sen, mal sé nauva rehtaina – ómu ve ter náre.
Ma ualde ista i nalde corda Eruva, ar i Eruo faire mare lesse?
Qui aiquen nancare corda Eruva, Eru nancaruva sé, an corda Eruva aire ná, i corda ya elde nar.
Nai *úquen huruva insen: Qui aiquen mici le sana i náse saila randa sinasse, nai nauvas carna auco, náven carna saila.
An mar sino sailie ná *aucie Erun, an ná técina: "Mapas i sailar véra curultasse",
ar ata: "I Héru ista i sailaron sanwi, i nalte luste."
Etta nai *úquen laituva inse mi Atani, an samilde ilye nati.
Qui Paulo hya Apollo hya Céfas hya i mar hya cuile hya qualme hya i nati yar ear sí hya i nati yar tuluvar – ilqua elde samir, íre elde nar i Hristova, mal Hristo ná Eruva.
Nai atan vi-notuva ve Hristo núror, mahtala Eruo fóli.
Ente, ya mo cesta mi *ortirmor ná i aiquen ná hírina voronda.
Inyen ná ita cinta natto náve hentaina lo elde hya lo fírime námor. É inye ua henta imne.
An uan ista *aiqua inyenna. Ananta uan sie tanaina faila, mal ye ni-henta ná i Heru.
Etta áva name *aiqua nó i lúme, tenna i Heru tule, ye taluva i morniéno nurtaine nati mir i cala ar yando caruva sinwe i endaron sanwi, ar tá euva laitale ilquenen Erullo.
Sisse, hánor, *yuhtan imne ar Apollo ve *epemmar, márieldan. Sie, cimiénen met, lertalde pare nat sina: "Áva mene han i técinar, pustien quén ortiello inse or exe."
An mana care lye *alavéla, sestima as exe? É mana samil ya ual acámie? Mal qui camnelyes, manen ná i laitalyexe ve qui ual camne sa?
Ma nalde quátine yando sí? Ma nalde lárie yando sí? Ma anaielde cárine arani ú elmeo? Merin i é anelde cárine arani, turielman aselde!
An *şéya nin i Eru apánie mé, i aposteli, métime, ve neri námine qualmen, an anaielme *ettanie i marden, ar valain, ar atanin.
Nalme aucor Hristonen, mal elde nar hande Hristosse; nalme milye, mal elde nar torye; nalde alcarinque, íre elme penir alcar.
Tenna lúme sina nalme maite ar soice ar helde ar pétine, ú maro,
ar mótalme, mólala vére málmanten. Íre camilme yaiwe, aistalme; íre nalme roitaine, colilmes;
íre mo quete ulco pa me, hanquetilme nilde quettainen. Anaielme ve i mardo *auhanta, vaxe náven sóvina ilye natillon, tenna sí.
Uan téca nati sine nucumien le, mal tanien len ya vanima ná, ve melde hínalmar.
An qui sámelde *peantari *quaihúme Hristosse, é ualde same rimbe atari, an i Hristo Yésusse anaien cárina atarelda i evandilyonnen.
Etta arcan lello: Á vanta tienyasse!
Sina castanen ementie lenna Timoşeo, melda ar voronda hínanya i Herusse, ye tyaruva le enyale lényar mi Hristo Yésus, ve peantan mi ilya nóme ar ilya ocombesse.
Ve qui uan túla lenna, quelli ortier inte,
mal tuluvan lenna rato, qui i Heru mere.
Tá istuvan, lá i quetie ion ortier inte, mal túrelta.
Mana merilde? Ma tuluvan lenna tálala vandil, hya tálala melme ar moica faire?
É mo hlare pa *hrupuhtie mici le, ar taite *hruputhie ya ua marta yando imíca i nóri – nér same ataryo veri!
Ar elde nar *valate! Carnelde arya qui sámelde nyére, hátala nér sina et endeldallo.
Ono inye, ómu nanye oa i hroasse, mal aselde i fairesse, anámie yando sí, ve qui anen tasse, i nér ye elengie sie:
Yésus Hristo Herulvo essenen, íre nalde uo ar yando ninya faire ea tasse arwa i túreo Yésus Hristo Herulvo,
alde anta taite nér olla Sátanen. Sie hráverya nauva nancarna, mal i faire nauva rehtaina i Heruo auresse.
Ualde same mára casta ortien inde. Ma ualde ista i pitya *pulmaxe tyare i quanda maxe tiuya?
Á mapa oa i yára *pulmaxe, náveldan vinya maxe ya ná ú *pulmaxeo. An *yancalva lahtiéva, Hristo, anaie nanca.
Sie alve care aşar, lá i yára *pulmaxenen, yando ú *pulmaxeo ulcuva ar olciéva, mal arwe *alapulúne massaron poiciéno ar nanwiéno.
Mentanyasse tencen lenna: Áva lemya mici *hrupuhtor.
Lá i polilde quere inde aqua oa ho *hrupuhtor, hya ho milce queni hya queni i *tyerir cordondi. Tá é mauyane len auta et i mardello!
Mal sí técan lenna: Áva lemya as aiquen estaina háno ye ná *hrupuhto hya quén ye quete yaiwe hya ye suce acca ole hya ná arpo. Ente, áva mate as taite quén.
An manen mauya nin name i ear i ettesse? Ma ualde namir i ear i mityasse,
íre Eru name i ear i ettesse? Á menta i ulca quén oa et endeldallo!
Ma aiquen mici le ye same cos exenna verya náve námina lo úfaile neri ar lá epe i airi?
Hya ma ualde ista i namuvar i airi i mar? Ar qui i mar nauva námina lo elde, ma ualde valde name i ancinte nattor?
Ma ualde ista i namuvalve valar? Tá manen laialve coivie sino nattor?
Qui, tá, é samilde nattoli coivie sino pa yar merilde námie, ma panyalde ve námor i neri nattírine i ocombesse?
Quétan tyarien le fele naityane. Ma ua nanwa i penilde erya saila nér mici le ye istuva name imbi hánoryar,
mal háno same cos hánonna, ar ta epe queni i uar save?
É nalde turúne mi lú ya samiénen costi aqua, quén i exenna. Manen ná i ualde ambe rato nehtaine? Manen ná i ualde ambe rato racine?
Ui, nehtalde ar carir exi racine, ar ta hánoldar!
Hya ualde istar i úfaile queni uar nauva aryoni Eruo araniéno? Áva ranya! *Hrupuhtor ar queni i *tyerir cordoni hya racir vestale, ar neri hépine mehten ara nasse, ar neri i caitar as neri,
ar pilur, ar milcar, ar queni i sucir acca ole, ar queni i quetir yaiwe, ar arpor, uar nauva aryoni Eruo araniéno.
Ananta ta ná ya quelli mici le náner. Mal anaielde sóvine poice, mal anaielde airinte, mal anaielde *failante essenen Yésus Hristo Herulvo, ar Ainolvo Fairenen.
Lertan care ilye nati, mal ilye nati uar aşie. Lertan care ilye nati, mal uan lavuva i nauvan tulyaina nu i túre *aiquava.
Matso i *cumbon, ar i *cumbo matson; mal i Eru nancaruva yúyo. I hroa ua *hrupuhtien, mal i Herun; ar i Heru ná i hroan.
Mal Eru ortane i Heru ar yando ortuva vé túreryanen.
Ma ualde ista i hroaldar nar Hristo hroarantar? Ma mapuvan oa i Hristo hroarantar ar caruva tai *imbacindeo hroarantar? Laume!
Hya ualde ista i ye anaie carna er as *imbacinde ná hroa er óse? An "i atta", quetis, "nauvat hráve er".
Mal ye anaie carna er as i Heru ná faire er óse.
Uşa *hrupuhtiello! Ilya hyana úcare ya atan pole care ná hroaryo ettesse, mal ye *hrupuhta úcára véra hroaryanna.
Hya ma ualde ista i hroalda ná corda i Aire Faireva lesse, ya samilde Erullo? Ente, ualde haryaldexer,
an anelde mancaine nonwen. Etta alde anta alcar Erun hroaldanen!
Pa i nati pa yar tencelde: Lá appie nís ná aşea neren,
mal pustien *hrupuhtie mauya i ilya nér same véra verirya ar ilya nís véra verurya.
Mauya i verun anta rohtarya veriryan, mal yando mauya i verin care i imya veruryan.
I veri ua same túre or véra hroarya, mal verurya same. Mí imya lé, yando i veru ua same túre or véra hroarya, mal verirya same.
Áva care quén i exe racine, hequa íre naste er sámo náve oa i exello, ter lúme, antien lent ecie hyamien. Tá tula uo ata, pustien Sátan şahtiello let peniestanen *immoturie.
Ono si ná nat ya lavin, lá nat ya canin.
Mal merin i ilye atani náner ve inye ná. Ananta ilquen same véra annarya Erullo, quén mi lé sina, exe mi lé tana.
Sí quetin innar uar evérie, ar i *verulórannar: Ná tien aşea qui en nalte ve yando inye ná.
Mal qui penilte *immoturie, nai veryuvalte, an qui veryalte, ta ná arya lá i uryalte yérenen.
I queninnar i evérier antan i canwa – ananta lá inye, mal i Heru – i ua lerta veri hehta verurya.
Mal qui é hehtases, mauya sen lemya ú nero, hya care raine as verurya; ar veru ua lerta hehta verirya.
Mal i exennar inye – lá i Heru – quete: Qui ea háno arwa verio ye ua save, ar i nís léra nirmeryanen mere mare óse, uas lerta hehta i nís.
Ar nís arwa veruo ye ua save, ananta i nér mere mare óse léra nirmeryanen, ua lerta hehta verurya.
An i veru ye ua save ná airinta i nissenen, ar i veri ye ua save ná airinta i hánonen. Qui lá, hínaldar anwave náner úpoice, mal sí nalte airi.
Mal qui i quén ye ua save autea, lava sen auta; háno hya néşa ua nútina qui nattor tarir sie, mal Eru ayálie le rainenna.
An, veri, ma istal qui rehtuval verulya? Hya, veru, ma istal qui rehtuval verilya?
Aqua ve i Heru etasátie ilquenen, sie nai vanta ilquen ve Eru ayálie se. Sie canin ilye i ocombessen.
Ma aiquen náne yálina *oscirna? Nai uas nauva *úoscirna. Ma aiquen náne yálina ú *osciriéno? Nai uas nauva *oscirna.
*Oscirie ua valdea, ar penie osciriéva ua valdea, mal cimie Eruo axani é ná valdea.
Nai ilquen lemyuva i sómasse yasse anes yálina.
Ma anel yálina ve mól? Áva na *tarastaina, mal qui yando polil náve leryaina, i mende á mapa i ecie.
An aiquen i Herusse ye náne yálina íre anes mól ná i Heruo leryaina; mi imya lé, ye náne yálina ve léra quén ná Hristo mól.
Anelde mancaine sen nonwen; áva na cárine móli atanin.
Nai ilquen, lá címala i sóma yasse anes yálina, lemyuva sasse, epe Eru.
Pa i vendi uan same canwa i Herullo, mal antan sanwenyar ve quén ye acámie i oravie i Herullo i nanye voronda.
Etta, címala i eala maure, sanan i si i arya ná: i lemya nér ve náse.
Ma nalye nútina verinna? Áva cesta lehtie. Ma nalye lehtaina verillo? Áva cesta veri.
Mal yando qui é veryanelye nissenna, ualye úcarumne. Ar qui vende veryane, uas úcarumne. Mal i carir sie samuvar şangier hráveltasse. Ono nevin le-rehta nyérello.
Ono nat sina quetin, hánor: I lemyala lúme şinta ná. Ho sí mauya in samir verili náve ve qui pennelte veri;
ente, i yaimeain náve ve qui únelte yaimie, ar in samir alasse náve ve qui pennelte alasse, ar in mancar ve i uar harya,
ar in *yuhtar i mar náve ve i uar aqua *yuhta sa, an mar sino ilce ahyea.
É merin i nauvalde lére *tarastiello. I nér ye ua evérie nissenna anta sámarya i Heruo nattoin, manen camuvas i Heruo laitale.
Mal ye evérie anta sámarya i mardo natin, manen camuvas veriryo laitale,
ar náse şanca. Ente, i nís ye ua evérie, hya i vende, anta sámarya i Heruo nattoin, lávala sen náve aire mi hroarya ar fairerya véla. Mal i nís ye evérie anta sámarya i mardo nattoin, manen polis came veruryo laitale.
Ono nat sina quétan pan ta ná aşea len, lá avalerien le, mal tyarien le same vanima lengie, yasse himyalde i Heru ú *perestiéno.
Mal qui aiquen sana i lengierya venderyanna ua vanima, qui náse han lostierya, ar maurenen euva sie, nai caruvas ya meris; uas úcare. Lava tún verya.
Mal qui aiqueno enda ná tulca, lá arwa maureo, mal samis túre or véra nirmerya ar acárie i cilme véra endaryasse i se-hepuvas vende, caruvas mai.
Sie ye verta venderya care mai, mal ye ua verta se, caruva arya.
Nís ná nauta ter i quanda lúma ya verurya ná coirea. Mal qui verurya effire, náse léra verien yenna meris, ómu eryave i Herusse.
Mal nauvas ambe valima qui lemyas erya, ninya lén saniéva. É sanan i yando samin Eruo faire.
Sí pa matsor *yácine cordonin: Istalve i illi mici vi samir istya. Istya care queni turquime, mal melme carasta te ama.
Qui aiquen sana i istas nat, en uas ista sa ve mauya sen ista sa.
Mal qui aiquen mele Eru, quén sina ná sinwa lo sé.
Sí pa i matie matsoiva *yácine cordonin: Istalve i cordon ná munta i mardesse, ar i ua ea aino hequa er.
An ómu ear té i nar estaine "ainor", menelde hya cemende, aqua ve ear rimbe "ainor" ar rimbe "heruvi",
ea elven erya Aino, i Atar yello ilye nati nar, ar ea erya Heru, Yésus Hristo yenen ilye nati nar, ar elve sénen.
Ananta illi uar same istya sina. Mal quelli, ion haime tenna sí ape i cordon, matir matso ve nat yácina cordonen, and *immotuntielta, nála milya, ná vahtaina.
Mal matso ua vi-caruva mani Eruo hendusse; qui ualve mate, ualve loita, ar qui matilve, ualve arye.
Mal cima i sina lertielda ua mi lé nauva casta taltiéva i milyain.
An qui aiquen cenuva lye, i quén arwa istyo, caitala ara i sarno matten cordasse i cordonion, ma i milya queno *immotuntie ua nauva carastaina ama, tenna matie matsor *yácine cordonin?
Istyalyanen i milya quén é nauva nancarna, hánolya yen Hristo qualle!
Mal íre sie úcarilde hánoldannar ar harnar *immotuntielta ya ná milya, úcáralde i Hristonna.
Etta, qui matso tyare hánonya talta, uan oi matuva hráve ata, pustien immo tyariello hánonya talta.
Ma uan léra? Ma uan apostel? Ma uan ecénie Yésus Herulva? Ma ualde molienya i Herusse?
Qui uan apostel exin, é nanye sie len, an nalde i *lihta *apostolienyo i Herusse.
Hanquenta sina antan in hentar ni:
Lertalme mate ar suce, lá?
Lertalme tala néşa ve veri, ve carir i hyane aposteli ar i Heruo hánor ar Céfas, lá?
Hya ma Varnavas ar inye erinque uat lerta náve lére moliello?
Man oi ná ohtar véra telperyanen? Man empane tarwa liantassion ar ua mate i yáveo sallo? Hya man ná mavar lámáreo ar ua suce i ilimo i lámárello?
Lau quetin sie mi atano lé? Hya ma i şanye ua yando quete nati sie?
An Móseo şanyesse ná técina: "Ávalye pusta mundo matiello íre rácas i ore." Ma mundor nar valde Erun?
Hya ma uas quete sie aqua márielvan? É elven ta náne técina, an vanima ná i mi estel i nér ca i hyar moluva, ar i neren ye race i ore ná vanima i caris sie arwa estelo i camuvas i yáveo molieryo.
Qui erérielme nati i faireva elden, ma ná hoa nat qui elme *cirihtuvar nati i hráveva lello?
Qui exeli samir ranta mi túre sina or le, ma elme laiar ambe sie? Ananta ualme *uyuhtie túre sina, mal cólalme ilqua, pustien imme queriello aiquen oa i evandilyonello pa i Hristo.
Ma ualde ista i camir i núror i airi nation laulestalta i Cordallo, ar i núror ara i *yangwa samir masselta i matsosse *yácina i *yangwasse?
Mí imya lé i Heru yando canne i té i carir i evandilyon sinwa camuvar laulestalta i evandilyonnen.
Mal uan *uyuhtie erya ion nati sine. É uan tecin quettar sine camien tai. Inyen náne arya quale! Uan lavuva aiquenen mapa oa castanya laitien imne.
An qui carin sinwa i evandilyon, ta ua nin casta laitien imne, an maure ná panyaina nisse. Horro nin qui uan carne sinwa i evandilyon!
Qui carin sie léra nirmenyanen, samin *paityale, mal qui carinyes nu mausta, en samin ortírie panyaina nisse.
Mana, tá, *paityalenya? Nat sina: i íre carin i evandilyon sinwa, antanyes ú *paityaleo, lá aqua *yuhtala i túre ya samin i evandilyonnen.
An ómu nanye léra ho illi, acárienyexe illion mól, rehtien i ambe rimbe.
Yúrain carnenyexe ve Yúra, ñetienyan Yúrar; in náner nu şanye carnenyexe ve quén nu şanye, ómu inye ua nu şanye, ñetienyan i nar nu şanye.
In ú şanyeo carnenyexe ve quén ú şanyeo, ómu uan quén úşanyeo Erun, mal nu Hristo şanye, ñetien i penir şanye.
I milyain carnenyexe milya, ñetienyan i milyar. Illin anaien carna ilye nati, ñetienyan i ampityasse quelli.
Mal carin ilqua i evandilyonen, samienyan ranta sasse.
Ma ualde ista i mi norme, ilye i queni norir, mal er erinqua came i anna apaireo? Nora mi lé ya lavuva len camitas!
Ente, ilya nér ye nore i normesse tana *immoturie mi ilye nati. Té carir sie camien ríe ya hesta, mal elve ríe ya ua oi hestuva.
Etta é uan nore mi útanca lé; palpan mi lé ya sie ná i uan pete i vista,
mal paimetan hroanya ar tulya sa ve mól, i inye ye peánie exin ua nauva quérina oa.
Uan mere i penilde istya sina, hánor, i atarilvar náner illi nu i lumbo ar illi lahtaner ter i ear
ar illi náner *tumyaine mir Móses i lumbunen ar i earnen;
ar illi manter i imya matso faireva,
ar illi suncer i imya yulda faireva. An suncelte i ondollo faireva ya hilyane te, ar ondo tana náne i Hristo.
Mal Eru ua sanne mai pa i ambe rimbar mici te, an túlelte mettaltanna i erumasse.
Nati sine nar panyaine epe vi ve *epemmar, vi-pustien meriello ulce nati, ve té merner tai.
Ente, áva *tyere cordoni, ve quelli mici té carner, ve ná técina: "I lie hamne undu matien ar sucien, ar tá orontelte tyalien."
Yando, ávalve *hrupuhta, ve quelli mici té *hrupuhtaner, ar lantanelte, húmi nelde ar *yúquean erya auresse.
Ente, ávalve tyasta i Héru, ve quelli mici té tyasanter se ar náner nancarne i leucainen.
Yando, áva na nurrula, ve quelli mici té nurruner ar náner nance lo i *Nancáro.
Nati sine martaner tien ve *epemmar, an anelte técine peantien ven pa raxelvar, elve i utúlier i rando mettanna.
Etta, nai ye sana i táras cimuva i uas lanta.
En ualde evélie úşahtie han ya ná şanya atanin. Mal Eru voronda ná, ar uas lavuva i nalde tyastaine han ya polilde cole, mal as i úşahtie yando caruvas i tie et sallo, lavien len náve tulce.
Etta, meldanyar, uşa *cordontyermello!
Carpan ve queninnar arwe terceno; nama inden ya quetin.
I yulma aistiéva ya aistalve, ma uas anta ven ranta i Hristo sercesse? I massa ya racilve, ma uas anta ven ranta i Hristo hroasse?
Pan ea er massa, nalve er hroa ómu nalve rimbe, an illi mici vi samir ranta mi er massa tana.
Á yéta ya ná Israel mí lé i hráveo: Ma i matir i *yancar uar same ranta as i *yangwa?
Mana, tá, quetuvan? I ta ya anaie *yácina cordonen ná *aiqua, hya i ná cordon *aiqua?
Ui, mal quetin i tai yar i nóri *yacir, *yacilte raucoin ar lá Erun, ar uan mere i samuvalde ranta as i raucor.
Ua ece len suce i Heruo yulmallo ar i raucoron yulmallo; ua ece len mate sarnollo i Heruva ar sarnollo i raucoiva.
Hya ma merilve valta i Heruo uryala şúle? Lau nalve torye lá sé?
Ilqua ná lávina, mal ilqua ua aşea. Ilqua ná lávina, mal ilqua ua carasta ama.
Mauya i ilquen cime, lá vére nattoryar, mal yando tai i hyana queno.
Ilqua ya ná vácina mi hrávemancanóme lertalde mate; ua mauya len maquete maquetier castanen *immotuntieldo,
an "i Heruo nar i cemen ar ya quate sa".
Qui aiquen mici i uar save yale le are merilde lelya, mata ilqua ya ná panyaina epe le, ú maquetiéron castanen *immotuntieldo.
Mal qui aiquen quete lenna: "Nat sina anaie *yácina," áva mate castanen yeo nyarne len ta, ar castanen *immotuntiéno.
*Immotuntie, quetin, lá véralya, mal ta i hyana queno. An manen ná i lérienya ná *námima hyana queno *immotuntiénen?
Mal qui mátan hantala Erun, manen ná i nanye naiquétina pa ta yan antan hantale?
Etta, lá címala qui matilde hya sucilde hya carir hyana nat, cara ilqua Eruo alcaren.
Hepa inde návello castar taltiéva, Yúrain ar Hellenyain ar Eruo ocomben véla,
ve yando inye care illi valime mi ilye nati – lá cestala ya ná aşea nin, mal ya nauva aşea i ambe rimbain, náveltan rehtaine.
Á vanta tienyasse, ve inye vanta i Hristo tiesse.
Laitan le pan mi ilye nati ni-hepilde sámasse, ar himyalde i haimi aqua ve antanenyet len.
Mal merin i istalde i ilya nero cas ná i Hristo, ar nisso cas ná i nér, ar i Hristo cas ná Eru.
Ilya nér ye hyame hya quete ve Erutercáno ú túpo caryasse nucume carya,
mal ilya nís ye hyame hya quete ve Erutercáno íre carya ua tópina, tala nucumie caryanna, an ta ná i imya nat ve qui findelerya náne aucirna.
An qui nís ua tope inse, mauya sen cire oa findelerya, mal qui ná nucumie nissen aucire findelerya, mauya sen tope inse.
An nér ua care mai qui topis carya, an náse Eruo emma ar alcar, mal i nís ná i nero alcar.
An nér ua et nissello, mal nís et nerello;
ente, nér úne ontaina i nissen, mal nís i neren.
Etta nissen ná vanima same caryasse tanwa túreo, castanen i valion.
Yando, i Herusse nís ná munta hequa nernen; mí imya lé, nér ná munta hequa nissenen.
An ve i nís ná et i nerello, sie yando i nér ea i nissenen, mal ilye nati nar et Erullo.
Nama elden: Ma ná vanima qui nís ú tópo hyame Erunna?
Ma ua nasse imma peanta len i qui nér same anda findele, ta ná sen nucumie,
mal qui nís same anda findele, nas alcar sen? An findelerya ná sen antaina ve vaşar.
Mal qui aiquen mere cos, ta ua haimelva, hya Eruo ocombion haime.
Mal antala canwar sine uan laita le, an ualde tule uo márien, mal ulcun.
Minyave, hlarin i íre tulilde uo i ocombesse ear şanciéli mici le, ar mi ranta savinyes.
Yando mauya i ear *rantier mici le, tanien man mici le nar sarte.
Etta, íre tulilde oa mir erya nóme, ualde tule matien i Heruo şinyemat.
An íre matildes, ilquen amátie véra şinyematterya nóvo, ar sie quén sina ná maita, íre exe ná *limpunqua.
Ma ualde same coar yassen ece len mate ar suce? Hya ma nattiril Eruo ocombe, ar nucumir i samir munta? Mana quetuvan len? Ma laituvan le? Mi natto sina uan laita le.
An camnelde i Herullo ya yando antanen olla len, i mí lóme yasse anes *vartaina, i Heru Yésus nampe massa,
ar apa antanes hantale, ranceses ar quente: "Si ná hroanya, ya ná antaina rá len. Cara si enyalien ní."
Apa i şinyemat carnes i imya i yulmanen, quétala: "Yulma sina ná i vinya vére sercenyasse. Cara si, mi ilya lú ya sucildes, enyalien ní."
An mi ilya lú ya matilde massa sina ar sucir yulma sina, carilde sinwa i Heruo qualme, tenna tulis.
Sie ilquen ye mate i massa hya suce i yulma i Heruo ú náveo valda, coluva cáma pa i Heruo hroa ar serce.
Mauya i quén tyasta immo, ar epeta lertas mate i masso ar suce i yulmallo.
An ye mate ar suce ú cimiéno i hroa, máta ar súca námie insenna.
Etta rimbali mici le nar milye ar engwe, ar fárea nóte aquálier.
Mal qui cimnelvexer, ualve náne námine.
Ono íre nalve námine lo i Heru, camilve paimesta i Herullo, vi-pustien návello námine ulce as i mar.
Etta, hánonyar, íre tulilde uo matien sa, á *larta quén i exen.
Qui aiquen ná maita, mauya sen mate coaryasse, hya tuluvalde uo námien. Mal i lemyala nattor *partuvan íre tuluvan tar.
Sí pa i annar i faireo, hánor: Uan merin i penuvalde istya.
Istalde i íre anelde i nórion, anelde túcine oa, tulyaine i cordonnar yar uar pole quete.
Etta merin i istalde i *úquen, íre quétas Eruo fairenen, pole quete: "Yésus ná húna!", ar *úquen pole quete: "Yésus ná Heru", hequa Aire Feanen.
Ear *alavéle nostaléli annaron, mal i faire ná i imya.
Ear *alavéle nostaléli núrumoliéva, mal i Heru ná i imya.
Ear *alavéle nostaléli moliéva, mal i imya Eru mole ilye i molier mi ilye queni.
Mal i tanie i faireva ná antaina ilquenen aşea mehten.
Er quenen ná antaina questa sailiéva, exen questa istyava, ve i imya faire tulya se;
exen, savie i imya fairello; exen, annar nestiéva i erya fairenen;
exen, molie elmendaiva, exen, quetie ve Erutercáno, exen, tercen i fairissen, exen, *alavéle lambi, exen, nyarie mana lambi tear.
Mal ilye nati sine nar móline lo i er ar i imya faire, etsátala tai ve meris ilya quenen.
An aqua ve i hroa ná er, mal same rimbe rantar, ar ilye i hroarantar, ómu nalte rimbe, nar erya hroa, sie yando ná i Hristo.
An é er fairenen anelve illi *tumyaine mir er hroa – lá címala qui nalve Yúrar hya Hellenyar, lá címala qui nalve móli hya lérar. Ar illi mici vi camner i imya faire sucien.
An i hroa é ua er ranta, mal rimbe.
Qui i tál quetumne: "Pan uan má, uan ranta mí hroa", uas sina castanen ettesse i hravo.
Ar qui i hlas quetumne: "Pan uan hen, uan ranta mí hroa", uas sina castanen ettesse i hravo.
Qui i quanda hroa náne hen, masse i hlarie? Qui ilqua náne hlarie, masse i nuste náne?
Mal sí Eru apánie i rantar i hroasse, ilya erya mici tai, aqua ve mernes.
Qui illi mici tai náner er ranta, masse i hroa?
Mal sí nalte rimbe hroarantar, ananta er hroa.
Ua ece i henden quete ana i má: "Maure lyéva uan same," hya ata i cas ana i talu: "Maure letwa uan same."
Ná ole ambe nanwa i ea maure tane hroarantaiva yar ilceltanen nar milye,
ar i hroarantar yar sanalve mis *laitime, tain antalve ta ambe úvea alcar, ar sie úvanime rantalvain samir ambe úvea vanesse,
íre vanime rantalvar samir maure muntava. Ananta Eru panyane uo i hroa, antala alcar ambe úvea i racina rantan,
pustien i hroa návello şanca, mal mauya rantaryain cime i exi ve cimiltexer.
Ar qui er ranta same naice, ilye i hyane rantar samir i imya naice; hya qui er ranta came alcar, ilye i hyane rantar samir alasse ósa.
Sí elde nar i Hristo hroa, ar illi mici le nar rantar i ilyasse.
Ar té i Eru apánie i ocombesse nar, minyave, aposteli, tatyave, Erutercánor, nelyave, *peantari; tá i carir elmendar, tá annar nestiéva, manyala molier, tulyala molier, nostaler lambaiva.
Lau illi nar aposteli? Lau illi nar Erutercánor? Lau illi carir elmendar?
Lau illi samir annar nestiéva? Lau illi quetir lambessen? Lau illi samir i anna nyariéva ya i lambi tear?
Mal á cesta i ambe túre annar uryala şúlenen! Ananta tanan len lahtala tie.
Qui quetin atanion ar valion lambessen, mal uan same melme, anaien ve lamyala ranta urusteva, hya tontilla cárala ran.
Ar qui samin i anna quetiéva ve Erutercáno, ar istan ilye i fóli ar ilya istya, ar qui samin savie camien oronti leve, mal uan same melme, nanye munta.
Ar qui antan ilye armanyar ve matso exin, ar qui antan olla hroanya náveryan urtaina, mal uan same melme, ta ua aşea nin aqua.
Melme same cóle ar moica ná. Melme ua same *hrucen, uas laitaxe, uas turquima,
uas lenga mi úvanima lé, uas cesta véraryar, uas talaina rúşenna. Uas hepe onotie ulcuva.
Uas same alasse mi úfailie, mal samis alasse as i nanwie.
Colis ilqua, savis ilqua, samis estel mi ilqua, perperis ilqua.
Melme ua oi lantuva oa. Qui nalte Erutercánoron quettar, nauvalte panyaine oa; qui nalte lambi, hautuvalte; qui nas istya, nauvas panyaina oa.
An istyalva ná mi ranta, ar quetilve ve Erutercánor mi ranta,
mal íre i ilvana tuluva, ta ya ea mi ranta nauva panyaina oa.
Íre anen lapse, quenten ve lapse, sannen ve lapse, namnen ve lapse; mal sí íre nanye veo, apánien oa i lapseo nati.
Sí cenilve ve mi cilintilla, nullave, mal tá cenuvalve cendelello cendelenna. Sí istan mi ranta, mal tá istuvan aqua, ve nanye yando sinwa.
Ono sí lemyar savie, estel, melme – nelde sine – mal i antúra mici tai ná melme.
Á roita melme, ananta uryala şúlenen á cesta i annar i faireo, or ilqua i poluvalde quete ve Erutercánor.
An ye quete lambesse quete, lá Atannar, mal Erunna, an ea *úquen lastala, mal quetis fóli i fairenen.
Mal ye quete ve Erutercáno carasta ama ar horta ar tiuta atani questaryanen.
Ye quete lambesse carasta inse ama, mal ye quete ve Erutercáno carasta ama ocombe.
Merin i illi mici le quetuvar lambessen, mal arya ná qui quetuvalde ve Erutercánor. Ye quete ve Erutercáno é ná túra lá ye quete lambessen – qui uas nyare mana questarya tea, sie lávala i ocomben came *amacarastie.
Mal sí, hánor, qui tulumnen lenna quétala lambessen, manen ta náne len aşea qui uan quente lenna apantiénen hya istyanen hya Erutercáno quettanen hya peantiénen?
Qui yando nati ú coiviéno yar antar lamma, ve ñande hya simpa, uar carir i lingi *alavéle, manen mo istuva mana ná tyálina i simpanen hya i ñandenen?
É man manwaxe mahtien qui i hyóla anta nulla lamma?
Mí imya lé yando elde: Qui ualde lambaldanen carpa mi questa ya ece quenin hanya, manen aiquen istuva mana ná quétina? É quetuvalde mir i vilya!
Ómu ear rimbe nostaler lambeleron i mardesse, ua ea erya nostale ya tea munta.
Qui uan ista ya i lambele tea, nauvan ettelea yen quete, ar sé nauva ettelea inyen.
Sie yando elde, pan uryala şúlenen merilde i faireo annar, á cesta samitat úvesse, carastien ama i ocombe.
Etta mauya yen quete lambesse hyame i istuvas nyare ya ta tea.
An qui hyáman lambesse, fairenya hyáma, mal sámanya ua anta yáve.
Mana caruvan? Hyamuvan i fairenen, mal yando hyamuvan sámanyanen.
An qui antalde laitale fairenen, manen i nér ye hára i şanya queno nómesse quetuva "násie" íre antalde hantale, pan uas hanya mana quétalde?
Elde é antar hantale mi mára lé, mal i hyana nér ua carastaina ama.
Hantan Erun, quetin lambessen rimbe lá illi mici elde quetir!
Ananta i ocombesse merin quete quettar lempe sámanyanen or quettar húmi quean lambesse.
Hánor, áva na hínar handesse, mal na lapsi ulcusse; ananta na ilvane handesse.
I Şanyesse ná técina: "Ettelearon lambínen ar ettelie quenion peunen quetuvan lie sinanna, ar yando tá ualte lastuva ninna, quete i Héru."
Sie lambi nar tanwa, lá in savir, mal in uar save; ono quetie ve Erutercáno ua in uar save, mal in savir.
Etta, qui i quanda ocombe ócome mi er nóme, ar illi mici te quetir lambessen, mal queni i penir istya hya uar save tulir minna, ma ualte quetuva i nalde ettesse sámaldo?
Mal qui illi mici te quetir ve Erutercánor, ar quén ye ua save hya ye pene istya tule minna, náse naityaina lo illi, náse hentaina lo illi,
endaryo fóli nar apantaine, ar sie, lantala cendeleryanna, *tyeruvas Eru ar *etequentuva: "Eru é ea mici elde!"
Tá mana caruvalde, hánor? Íre ocomilde, quén same aire líre, quén same peantie, quén same apantie, quén same lambe, quén istya nyare mana lambe tea. Mauya i ilqua martuva carastien ama.
Ar qui queni quetir lambessen, mauya i ualte rimbe lá atta hya – ve i anhalla *lávima nóte – nelde. Alte quete quén apa i exe, ar mauya i ea quén ye nyare mana questalta tea.
Mal qui ua ea aiquen antien i *teale, mauya sen náve quilda i ocombesse ar quete insenna ar Erunna.
Ar lava Erutercánoin atta hya nelde anta quettaltar, ar lava i exin name ya quétalte.
Mal qui exe came apantie íre hámas tasse, mauya i minyan náve quilda.
An polilde quete ve Erutercánor quén apa quén; sie illi nauvar hortaine.
Ar mauya i Erutercánoin ture faireltar.
An Eru ua Aino yó tier uar *partaine, mal Aino raineva.Ve ilye ocombessen i airion
mauya i nissin náve quilde i ocombessen, an ua lávina tien quete, mal mauya i nalte nu túre, ve i Şanye yando quete.
Mal qui merilte pare nat, mauya tien maquete vére verultar i maresse, an quetie i ocombesse ná nucumie nissen.
Mana? Ma lello Eruo quetta *etelende, hya eryave tenna elde arahties?
Qui aiquen sana i náse Erutercáno hya i samis i faire, mauya sen cime i nati yar técan lenna, an nalte i Heruo canwa.
Mal qui aiquen pene istya, ú istyo náse.
Etta, hánor, uryala şulenen á cesta anna i Erutercánova, ananta áva váquete i quetie lambessen.
Mal nai ilye nati sine martuvar mi vanima ar *partaina lé.
Sí carin sinwa len, hánor, i evandilyon ya antanen len, ya yando camnelde, yasse yando tarilde,
yanen nalde rehtaine, i questanen yanen carnen i evandilyon sinwa len, qui hepildes – qui ualde sáver muntan.
An antanen olla len, mici i minye nati, ta ya yando camnen, i qualle Hristo úcarilvain ve i Tehtele quete,
ar i anes talaina sapsaryanna, ar anaies ortaina ama i neldea auresse ve i Tehtele quete,
ar i tannesexe Céfan, tá i yunquen.
Epeta tannesexe hánoin or tuxar lempe, ion i anhoa nóte lemyar tenna aure sina, ómu quelli nar qualini.
Epeta tannesexe Yácoven, tá ilye i apostelin,
mal métima illion tannesexe yando inyen, ve lapsen nóna nó i lúme.
An nanye i ampitya i apostelion, ar uan valda náven estaina apostel, an oroitien Eruo ocombe.
Mal Eruo lissenen nanye ye nanye. Ar i lisse ya antanes nin úne muntan, mal mólen ambe lá te illi, ananta lá inye, mal Eruo lisse ya ea asinye.
Mal lá címala qui inye hya té carner i molie, sie *nyardalme ar sie asávielde.
Sí qui pa i Hristo ná *nyardaina i anaies ortaina qualinillon, manen ná i ear quelli mici elde i quetir i ua ea *enortale qualinillon?
Qui *enortale qualinillon ua ea, yando Hristo ua anaie ortaina.
Mal qui Hristo ua anaie ortaina, *nyardielva ná anwave lusta, ar savielva lusta ná.
Ente, nalve yando hírine ve húrala *vettoli Erunna, an *evettielve Erunna i ortanes i Hristo, ye uas ortane qui qualini uar ortaine.
An qui qualini uar ortaine, yando i Hristo ua anaie ortaina.
Ente, qui i Hristo ua anaie ortaina, savielda ná muntan; nalde en úcarildassen.
É yando i aquálier Hristosse váner.
Qui mi coivie sina erinqua apánielve estelelva Hristosse, nalve mici ilye atani i anvalde *ofelmeo.
Ono i Hristo é anaie ortaina qualinillon, minya yáve ion aquálier.
An ve qualme túle ter atan, yando *enortale qualiniva ná ter atan.
An ve Atande illi qualir, sie yando Hristosse illi nauvar cárine cuine.
Mal ilquen véra lúryasse: Hristo i minya yáve, epeta i nar Hristova tulesseryasse,
tá i metta, íre antas olla i aranie Erun ar i Ataren, apa eméties ilya túre ar mehte.
An mauya sen ture tenna Eru panya ilye ñottor nu taluryat.
Ve i métima ñotto qualme nauva metyaina.
An Eru "apánie ilye nati nu talyat". Mal íre quetis i ilye nati anaier panyaine nu túrerya, ná aşcénima i sé ye panyane ilye nati nu se ua nótina mici tai.
Mal íre ilye nati nauvar panyaine nu túrerya, tá i Yondo immo panyuvaxe nu túre yeo panyane ilye nati nu se, carien Eru ilqua illin.
Qui lá, mana caruvar i nar *tumyaine qualinin? Mana castalta náven *tumyaine tien qui qualini laume nauvar ortaine?
Ente, mana castalva panien inwe raxesse ilya lúmesse?
Ilya auresse quálan – ve nanwa ve ece nin laitaxe eldenen, hánor, mi Hristo Yésus Herulva.
Qui náne mi atanion lé i amahtien hravani celvannar Efesusse, manen ta ná aşea nin? Qui qualini uar nauva ortaine, alve mate ar suce, an enwa qualuvalve!
Áva na tyárine ranya: Qui mo termare mici ulce queni, máre haimi nauvar hastaine.
Ñeta laice mi faila lé ar áva úcare, an ear quelli i penir istya pa Eru. Quétan tyarien le fele naityane!
Mal quén quetuva: "Manen qualini nauvar ortaine? Mana nauva i nostale hroaron yassen tuluvalte?"
A úhanda quén! Ya reril ua carna coirea qui uas minyave quale;
ar ta ya reril ua i hroa ya euva, mal helda erde – cé orio, cé hyana nostale laimo –
ono Eru anta san hroa aqua ve ná mára henyatse, ar ilya erden véra hroarya.
Ilya hráve ua i imya hráve, mal atani samir nostalelta, ar ea hyana nostale hráve yaxion, ar hyana nostale hráveo aiwion, ar hyana nostale lingwion.
Ar ear meneldie hroar ar hroar cemende, mal i meneldie hroaron alcar ná hyana nostaleo, ar lá i imya nostale ve i alcar i hroaron cemende.
Anaro alcar ná véra nostaleryo, ar Işilo alcar ná exe, ar tinwion alcar ná exe; é tinwe ua ve hyana tinwe mi alcar.
Sie ná yando i *enortale qualiniva: Nas rérina mi *nancárima sóma; nas ortaina mi sóma han nancarie.
Nas rérina ú alcaro; nas ortaina alcaresse.
Nas rérina ve hroa ermava; nas ortaina ve hroa faireva.
Sie yando ná técina: "I minya nér, Atan, náne carna coirea onna." I métima Atan náne carna faire antala coivie.
Ananta i minya ua ta ya ná faireva, mal ta ya ná ermava; epeta tule ta ya ná faireva.
I minya atan ná et i cemello ar carna astova; i attea atan ná et menello.
Ve i quén cárina astova, sie nar yando i náner cárine astova, ar ve i meneldea quén ná, sie nar yando i nar meneldie.
Ar aqua ve ocólielve i emma i queno carna astova, coluvalve yando i meneldea queno emma.
Mal quetin, hánor: Hráve yo serce uat pole náve aryon araniéno Eruo; ente, ya ná *nancárima ua haryuva ta ya ná han nancarie.
Yé! Nyáran len fóle: Illi mici vi uar qualuva, mal illi mici vi nauvar vistaine,
mi erya lú, mi hendo tintilie, íre i métima hyóla nauva hlárina. An i hyóla lamyuva, ar qualini nauvar ortaine mi sóma han nancarie, ar elve nauvar vistaine.
An mauya *nancárima sinan panya insasse sóma nancarie pella, ar mauya fírima sinan panya insasse ilfirin sóma.
Mal íre fírima sina panya insasse ilfirin sóma, tá martuva i quetie ya ná técina: "Qualme anaie nancarna tennoio!
Qualme, masse apairelya? Qualme, masse nastalya?"
I nasta qualmeva ná úcare, mal úcareo túre ná i Şanye.
Mal hantale Erun, an antas ven i apaire ter Yésus Hristo Herulva!
Etta, hánonyar, na voronde, tulce – illume cárala úve mí Heruo molie, istala i molielda ua muntan i Herusse.
Pa i hostie i airin: Ve cannen i ocombin Alatiasse, sie yando elde cara!
Nai ilquen mici le, ilya minya auresse i otsolo, satuva telpe – i nonwe ya ece sen anta. Sie, íre tuluvan, ua euva maure care hostier tá.
Mal íre tuluvan tar, mentuvan cilmeldo neri Yérusalemenna arwe mentaron, colien annalda.
Qui ecuva yando inyen lelya, lelyuvalte asinye.
Mal tuluvan lenna íre eménien ter Maceronia – an tienen Maceronio tuluvan –
ar aselde cé termaruvan, hya lemyuvan ter i hríve. Tá poluvalde menta ni tienyasse ata, yanna polin lelya.
An uan merin vele le sí, lahtala ménalda, an samin i estel i lemyuvan aselde ter ambe anda lúme, qui i Heru lave.
Mal lemyan Efesusse tenna Aşar Otsolaron,
an hoa fenna molienna anaie latyaina, ar ear rimbali i tarir ninna.
Íre Timoşeo tuluva, cena i nauvas léra ruciéno mici le, an cáras i Heruo molie, ve care inye.
Etta lava *úquenen nattire se. Áse menta lendaryasse rainesse, tulieryan ninna, an *lartean sen, as i hánor.
Sí pa Apollo hánolva, hortanenyes rimbe quettalínen tule menna, as i hánor, ananta şelmarya úne tule sí, mal tuluvas íre samis ecie.
Na cuive, na tance i saviesse, na vie, na taure!
Tyara ilye nattoldar marta melmesse.
Sí hortan le, hánor: Istalde i nosserya Stefánas ná i minya yáve Acaiasse, ar i panyaneltexer náve núror i airin.
Sie mauya yando elden panya inde nu taiti queni ar illi i molir uo ar mótar.
Mal samin alasse pan Stefánas ar Fortunáto ar Acaico nar sisse, an ómu elde uar asinye, té aquátier maurenya.
An ceutanelte fairenya ar fairelda. Etta cama taiti queni mai.
I ocombe Asiasse *suila le. Aquila yo Prisca, as i ocombe, *suilar le holmo i Herusse.
Ilye i hánor *suilar le. Á suila quén i exe aire miquenen.
Sisse ea ninya, Paulo, *suilie, véra mányanen.
Qui aiquen ua mele i Heru, nai nauvas húna! Maran ata!
Nai i Heru Yésuo lisse euva aselde!
Nai melmenya euva as illi mici le mi Hristo Yésus.
Paulo, Yésus Hristo apostel ter Eruo indóme, ar Timoşeo hánolma, Eruo ocombenna ya ea Corintesse, as ilye i airi i ear mí quanda Acaia:
Lisse len, ar raine ho Eru Atarelva ar ho i Heru Yésus Hristo.
Nai Eru ar Yésus Hristo Herulvo Atar nauva aistana, i Atar oraviéron ar Aino ilya tiutaleo,
ye vi-tiuta şangielvasse. Etta ece ven tiuta i queni i perperir ilya nostale şangiéno, i tiutalénen yanen elve nar tiutaine lo Eru.
An ve i Hristo perperier nar úvie vesse, sie i tiutale ya camilve ná yando úvea i Hristonen.
Qui samilve şangie, ta ná tiutaleldan ar rehtieldan; hya qui nalve tiutaine, ta ná tiutaleldan ya móla tyarien le náve voronde i imye perperiessen yar yando elme perperir.
Ar sie estelelma len ná tanca, pan istalme i síve samilde ranta i şangiessen, tambe samuvalde yando ranta i tiutalesse.
An ualve mere i penilde istya, hánor, pa i şangie ya martane men Asiasse – i anelme rácine cólanen han poldorelma, tenna sannelme i ua enge lé rehtiéva coivielma.
É fellelme endalmasse i anelme nu námie qualmeva, mal ta martane tyarien me cesta varnasselma, lá immesse, mal Erusse ye orta i qualini.
Ho qualmeraxe ta túra etelehtanes me ar etelehtuvas me. Sesse apánielme estelelma i etelehtuvas me ata.
Yando elde polir manya arcandeldanen men, ar sie rimbali antuvar hantale rámen, i Eruannan antaina men ve hanquenta rimbe hyamiélin.
An pa si laitalmexer: *Immotuntielma *vetta i mi aire lé ar holmo Erun, lá sailiénen hráveva mal Erulissenen, elengielme i mardesse – or ilqua elden.
An tecilme lenna munta hequa i nati yar polilde henta ar hanya. Samin i estel i aqua hanyuvaldet tenna i metta,
ve ahánielde mi ranta, lávala len laita inde pa elme, ve ece elmen care pa elde, i Heru Yésuo auresse.
Nála tanca pa si mernen nóvo tule lenna, antien len attea casta alassen.
Apa hautie aselde mernen lelya Maceronianna, ar nanwénala Maceroniallo, náve manyaina lo elde i yestasse lendanyo Yúreanna.
Mérala ta, lau anen acca léra? Hya i nati yar carin mehtinya, ma mernen caritat mi lé i hráveo, ar sie enger asinye 'Ná, Ná' ar 'Ui, ui'?
Mal ve Eru ná sarta, questalma lenna ua Ná ananta Ui.
An i Eruion, Yésus Hristo, ye náne carna sinwa mici le ter elme – ta ná, ter inye ar Silváno ar Timoşeo – úne carna Ná ananta Ui, mal Ná teramárie sesse.
An lá címala manen rimbe Eruo vandar nar, sesse samilte Nálta. Etta yando ter sé i Násie ná quétina Erunna lo elve, alcareryan.
Mal ye acárie lé ar mé tance i Hriston, ar ye *ilívie vi, ná Eru –
sé ye yando apánie *lihtarya vesse ar ánie ven varnasse i faireo endalvasse.
Ono inye yale Eru ve astarmo véra cuilenyanna: Rehtien le nyérello uan utúlie Corintenna.
Lá i nalme heruvi savieldo, mal molilme aselde alassedan, an i saviénen tarilde.
An nat sina ná cilmenya: i uan tuluva lenna ata nyéresse.
An qui tyarin nyére len, man antuva nin alasse hequa i quén yen inye atyárie nyére?
Ar etta tencen ve carnen, i íre túlen uan samumne nyére i quenillon in mauyane tala nin alasse. An anen tanca pa elde illi, i alassenyasse illi mici le samumner ranta.
An et hoa şangiello, moiala endanyasse ar arwa rimbe nírion, tencen lenna – lá tyarien len nyére, mal lavien len ista i úvea melme ya samin len.
Mal qui aiquen atyárie nyére, uas atyárie sa inyen, mal mi lesta – lá carpien mi acca naraca lé – illin mici le.
Íre sie anaies naityana lo i amarimbar, ta farya taite neren.
Etta quera inde ar i mende ásen apsene ar áse tiuta, hya taite nér cé nauva ammátina acca tumna nyérenen.
Sie hortan le i tanuvalde melmelda séva.
An tana mehten tecin yando tyastien le, istien qui cimilde i canwar ilye natissen.
Ilqua ya apsenilde, yando inye apsene. Ilqua ya inye apesénie – qui apesénien *aiqua – anaie márieldan epe Hristo,
pustien Sátan rahtiello or vi; an ualve pene istya pa panoryar.
Sí íre túlen Troasenna carien i evandilyon sinwa, ar fenna náne nin latyaina i Herusse,
uan sáme sére fairenyasse pan uan hirne Títo hánonya; mal quenten namárie téna ar oante Maceronianna.
Mal hantale na Erun, ye illume tulyar me apairesse as i Hristo, ar ter elme vintas mi ilya nóme i ne i istyo pa sé!
An Erun nalme Hristo lisse ne, imíca i nauvar rehtaine ar i nauvar nancarne
– i teldain, qualmeo ne ya tala qualme, mal i minyain, coiviéno ne ya tala coivie. Ar man farya sine natin?
An ualme care mancale Eruo quettanen, ve rimbali carir, mal quetilme holmo, ve mentaine lo Eru ar tírine lo Eru, i Hristosse.
Ma ata antalme mára *vettie immen? Hya cé samilme maure mára *vettiéva len, hya camielman sa lello?
Mentalma elde ná, técina eldalmasse ar sinwa ar cendaina lo ilye atani.
An ná aşcénima i nalde menta Hristollo, yáve núromolielmo, técina lá moronen mal i coirea Eruo fairenen, lá *palmassen ondova, mal *palmassen hráveva – endassen.
Ar si ná i tanca savie ya samilme Erusse ter i Hristo.
Lá i elme, mi imme, faryar notien *aiqua ve qui túles immello, mal fárelma tule Erullo.
Sé carne me fárie núror vinya véreo, lá técina şanyeo, mal i faireo; an i técina şanye nahta, mal i faire anta coivie.
Mal qui i núromolie qualmeva i técina şanyenen náne tulcaina mi alcar ta túra i náne úcárima i Israelindin yéta Móseo cendele, i alcarnen ya enge cendeleryasse – alcar ya firumne –
ma ua i núromolie i faireva nauva alcarinqua ambela ta?
An qui i núrumolie yanen queni náner námine ulce náne alcarinqua, i núromolie ya care queni faile same alcar ole ambe úvea.
An mi natto sina, ta ya yá sáme alcar sí ua same alcar aqua, castanen i alcaro ya lahta sa.
An qui ta ya firumne náne tulcaina mi alcar, ole ambe ta ya lemya samuva alcar.
Etta, pan samilve taite estel, nalve ita canye,
ar ualve care ve Móses, ye panyane vaşar cendeleryasse, pustien i Israelindi ceniello metta i fifírula alcaro.
Mal sámalta náne cárina hranga. An tenna aure sina i imya vaşar lemya ar ua ortaina i hentiesse i yára véreva, pan nas panyaina oa i Hristonen.
É tenna aure sina, quiquie Móses ná hentaina, vaşar caita endaltanna.
Mal íre querilte inte i Herunna, i vaşar ná mapaina oa.
Mal i Heru ná i Faire, ar yasse i Heruo faire ea, ea lérie.
Ar íre illi mici vi, ú vaşaro tópala cendelelva, *nanantar i Heruo alcar ve cilintilla, nalve vistaine mir ilcerya, alcarello alcarenna – ve ná cárina lo i Heru, i Faire.
Etta, pan samilme núromolie sina i Erulissenen antaina men, ualme hehta huorelma.
Mal ehehtielme i nurtaine nati yar talar nucumie, an ualme lenga curunen. Ente, ualme *ñauta Eruo quetta, mal taniénen i nanwie antalme immen mára *vettie epe Eruo cendele, ilye atanion *immotuntien.
Qui evandilyonelma é ná halda, nas halda ho i nauvar nancarne.
An randa sino aino acárie *cénelóra i sáma ion uar save, pustien caltiello i cala ho i evandilyon pa i Hristo alcar, sé ye ná Eruo emma.
An ualme care imme sinwe, mal Hristo Yésus ve Heru, íre elme nar mólildar, Yésuo márien.
An Eru ná ye canne: "Cala á calta et i morniello", ar sé acaltie endalmanna, calyala i istya pa Eruo alcar mi Yésus Hristo cendele.
Mal samilme harma sina cemne venessen, tyárala i úvea túre náve Erullo ar lá elmello.
Nalme nírine ilya tiello, mal ualme nómesse ta náha i ualme pole leve; ualme ista mana caruvalme, mal ualme aqua ú uşweo;
nalme roitaine, mal lá ú estelo; nalme hátine undu, mal ualme nancarne.
Illume perperilme hroalmasse i nahtie Yésuva, i yando Yésuo coivie nauva tanaina hroalmasse.
An elme i nar coirie nar oi antaine olla qualmen castanen Yésuo, i mehten i yando Yésuo coivie nauva tanaina fírima hrávelmasse.
Sie qualme móla messe, mal coivie lesse.
Mal pan samilme i imya faire saviéva – ve ná técina, "Sáven, etta quenten" – yando elme savir, ar etta yando quetilme,
istala i sé ye ortane Yésus yando ortuva mé ar tyaruva me tare imíca le.
An ilqua ná márieldan, ar sie i Erulisse, rahtala queninnar oi ambe rimbe, taluva amba hantalenna, Eruo alcaren.
Etta ualme hehta huorelma, mal ómu etya atanelma yeryea, mitya atanelma ná envinyanta aurello aurenna.
An ómu i roitie ná autala ar ua lunga, manweas men oira lesta alcaro oi ambe úvea,
íre yétalme lá i cénimar, mal i úcénimar. An i nati yar mo cene autuvar, mal i úcénimar nar oire.
An istalme i qui coalma mariéva cemende nauva nancarna, camuvalme ataque Erullo, coa lá cárina mainen, oialea mi menel.
An marelma sinasse é ñonalme, milyala panya immesse meneldea marelma.
Sie, apa panie sa immesse, ualme nauva hírine helde.
Íre ealme *lancoa sinasse é ñonalme nu i cóla, an merilme, lá panya sa oa, mal panya messe i exe, tyárala ya ná fírima náve ammátina i coiviénen.
Mal ye acárie men nat sina ná Eru, ye ánie men i faire ve varnasse i camuvalmes.
Etta, illume arwe huoreo, istalme i íre marilme i hroasse, nalme oa i Herullo –
an vantalme saviénen, lá ceniénen.
Mal samilme huore ar merir ambe náve oa i hroallo ar mare as i Heru.
Etta mehtelma ná i nauvalme írime sen, lá címala qui marilme óse hya oa sello.
An illi mici vi taruvar epe i námohamma i Hristova, ar sie ilquen camuva *paityalerya i natin cárine i hroanen, máre hya ulce.
Etta, istala i rucie i Herullo tulyalme atani savielmannar, mal anaielme cárine úfanwie Erun. Ono samilme i estel i nalme yando úfanwie *immotuntieldan.
Ualme anta immen mára *vettie epe le, mal antalme len ecie laitien inde elmenen, samieldan hanquenta in laitar inte etya ilcen, mal lá i endan.
An qui anelme ettesse sámalmo, ta náne Erun; qui sámelme mále sámo, ta ná elden.
An i Hristo melme mauya men, an sie anámielme: Er qualle rimbalin, é illi qualler,
ar illin qualles, mérala i té i nar coirie uar ambe samuva coivie inten, mal sen ye qualle tien ar náne ortaina.
Etta ho sí ualme ista aiquen i hrávenen. Yando qui isintielme i Hristo i hrávenen, ualme ista se sie ambe.
Etta, qui aiquen ná i Hristosse, náse vinya onna; i yárar avánier; yé, utúlier vinyali!
Mal ilqua ná i Ainollo, ye carne raine imbi inse ar elme ter Hristo ar antane men i núromolie rainecariéva.
Sie ece Erun i Hristonen care raine imbi inse ar i mar, lá nótala téna ongweltar, ar antanes i quetta rainecariéno men.
Etta nalme *ráqueni rá Hriston, ve qui Eru carne arcande ter me. Hristo mende hortalme: Cara raine imbi lé ar Eru!
Sé ye ua sinte úcare carnes úcare elven, návelvan Eruo failie ter sé.
Mólala óse yando hortalme le: Áva came Eruo lisse muntan!
An quetis: "Írima lúmesse lye-hlassen, ar i auresse rehtiéva lye-manyanen." Yé, sí ea i írima lúme! Yé, sí ea i aure rehtiéva!
Laume carilme *aiqua ya queruva queni oa, lá mérala i núromolielma nauva vahtaina.
Mal mi ilya lé ñetilme mara *vettie ve Eruo núror – túra voronwiesse, şangiessen, mauressen, hrangiessen,
palpiessen, mandossen, *perestiessen, mótiessen, lómissen ú húmeo, peniessen matsova,
poiciesse, istyasse, cólesse, moiciesse, aire feasse, melmesse ú *imnetiéno,
nanwa questasse, Eruo túresse, arwe i carmar failiéva mi forma yo hyarma,
alcaresse ar *naityalesse, arwe mára esseo hya ulca esseo, ve queni i tyarir exi ranya ananta nar sarte,
ve úsinwe ananta mai sinwe, ve quálala ar yé! nalme coirie, ve cámala paimesta ananta lá nance,
ve arwe nyéreo, mal oi quante alasseo, ve penye, mal cárala rimbali lárie, ve qui samilme munta ananta samir ilqua.
Antonya ná láta len, Corintyar, endanya ná palyaina!
Elde uar mi náha nóme aselme, mal véra endalda náha ná.
Etta, antien men i imya ya elme antar len, á palya endalda!
Áva na panyaina nu *alavéla yanta as queni i uar save. An mana ná ya failie ar *şanyelórie samir uo? Hya mana i *uome imbi cala ar mornie?
Ar manen polir Hristo ar Vélial náve er sámo? Hya mana ranta same quén ye save as quén ye ua save?
An elve nar corda i Coirea Ainova, ve Eru equétie: "Maruvan aselte ar vantuvan imíca te, ar inye nauva Ainolta, ar té nauvar lienya.
Etta á auta tello ar á lenweta," quete i Héru, "ar áva appa i úpoica, ar inye camuva le.
Ar inye nauva len Atar, ar elde nauvar nin yondoli ar yeldeli," quete i Héru Ilúvala.
Etta, pan samilve vandar sine, meldar, alve poita inwe ilya vaxello hráveo ar faireo, cestala ilvana aire, ruciesse Erullo.
Ámen palya endalda! *Úquenen acárielme ulco, *úquen ahastielme, *úquennen acámielme ñetie!
Sie uan quete namien le ulce, an equétien nóvo i nalde endalmasse, uo qualmen ar coivien.
Samin túra lérie quetiéva len, túra *immolaitie eldenen. Nanye quátina tiutalénen, samin úvea alasse mi ilya roitielma.
É ualme sáme sére i hroan íre túlelme Maceronianna – enger etye costiéli, mitye ruciéli.
Ananta Eru, ye tiuta i naldar, tiutane mé Títo tuliénen,
ananta lá tulieryanen erinqua, mal yando i tiutalénen yanen anes tiutaina pa elde, íre nyarnes manen milyalde, manen samilde nyére, manen şúlelda urya nin – ya tyarne ni same alasse en ambe túra.
An yando qui antanen len nyére mentanyanen, uan same attie sanweli pa sa. Sámen attie sanweli yá – an cenin i mentanya tyarne len nyére, ómu şinta lúmen eryave –
ananta nanye valima sí, lá pan sámelde nyére, mal pan i nyére le-tulyane inwistenna. An i nyére ya sámelde náne Eruo, i munta nauva len vanwa elmenen.
An i nyére ya ná Eruo tyare inwis pa ya *úquen pole same attie sanwi, mal i mardo nyére tyare qualme.
An yé! nat sina imma – i sámelde nyére ya náne Eruo – manen túra ná i horme ya atyáries! Manen atárielde ama inden! Manen horyalde! Manen milyalde! Manen şúlelda urya! Manen merilde paimeta ulco! Ilquasse atánieldexer poice natto sinasse.
Ómu tencen lenna é uan carne sie márien i queno ye carne ulco, hya i quenen ye perpére i ulco, mal tencen carien hormelda elmen aşcénima imíca le, epe Eru.
Etta anaielme tiutaine.Mal napánina tiutalelman sámelme alasse en ambe úvea Títo alassenen, an fairerya anaie envinyanta lo le illi.
An qui alaitien le epe se, uan anaie naityana, mal ve equétielme ilye nati len nanwiesse, sie yando laitalelma epe Títo anaie tanaina nanwa.
Ente, samis en ambe tumne felmeli lenna íre enyalis manen cimnelde canwaryar, manen camneldes ruciesse ar paliesse.
Hirin alasse i istyasse i mi ilya lé polin same huore eldenen.
Sí cáralme sinwa len, hánor, i lisse ya Eru ánie Maceronio ocombin.
An íre perpérelte túra tyastie şangiénen, úvelta alasseva ar tumna penielta carnet úvea i lárea merie antiéva ya sámelte.
An antanelte ve ence tien; é nanye astarmo i antanelte han ya ence tien, ar véra nirmeltanen.
Arcanelte me rimbe arcandelínen, mérala came i lisse samiéva ranta i núromoliesse i airin.
Ualte antane eryave ve i estel ya sámelme, mal minyave antanelte inte i Herun ar elmen, Eruo indómenen.
Etta hortanelme Títo i telyumnes i anna ya yá *yestanes hosta imíca le.
Ve nalde úvie mi ilqua – mi savie ar quetta ar istya ar ilya horme, ar mí melme ya samilme len – nai nauvalde yando úvie mi antielda!
Uan quete ta ve canwa, mal exion hormenen merin tyasta qui melmelda ná yando nanwa.
An istalde Yésus Hristo Herulvo lisse – i ómu anes lárea anes cárina penya márieldan, náveldan cárine lárie penieryanen.
Ar pa ta antan sanwenyar, an ta nauva aşea elden i *yestaner, lá i carie erinqua, mal yando merielda caritas, er loa yá.
Sí, tá, á telya yando i carie! Tambe anelde mérala caritas, síve yando mára ná qui telyaldes, et yallo samilde.
An qui ea i nirme, nas írima yanen mo same, lá yanen mo ua same.
An mehtinya ua i exi nauvar manyaine, íre nati nar urde len,
mal i illi nauvar vávie. Lú sinasse elde samir úve ar polir manya i perperir maure. Mi hyana lú té samuvar úve ar polir manya lé, íre elde perperir maure. Sie illi nauvar vávie,
ve ná técina: "I quén arwa olyo ua sáme acca olya, ar i quén arwa pityo ua sáme acca pitya."
Hantale Erun, pan panyanes i imya horme elden Títo endasse,
an camnes i hortale, ar pan horyanes ole, ménas lenna véra cilmeryanen.
Mal mentealme óse háno yeo laitale ivintie ter ilye i ocombi molieryanen i evandilyonen.
Lá ta erinqua, mal anes yando panyaina lo i ocombi ve sartonya, lelyala aselme ire colilme i hostie ya ortirilme, Eruo alcaren ar tanien mára nirmelma.
Merilme pusta aiquen quetiello ulco pa me pa i anna ya samilme hepielmasse,
an mehtelma ná i caruvalme ya mára ná, lá mí Heruo hendu erinque, mal yando mi atanion hendu.
Ente, mentalme aselte hánolma, ye rimbave atyastielme rimbe nattolissen ar ihírier i tanas horme molieryasse. Savis tancave i nalde sarte, ar etta molis en ambe túra hormenen.
Pa Títo: Masserya ná asinye, ar molis asinye márieldan. Pa i hyane hánor, nalte mentaine lo i ocombi ar nar Hristo alcar.
Etta á tana tien melmelda ar lava tien ista i le-laitanen epe i ocombi mára castanen.
Sí pa i núromolie i airin: Uan same anwa maure tecien len,
an istan i nalde mérala. Etta le-laitan Maceroniasse, quétala i Acaia náne manwa er loa yá, ar uryala şúlelda avaltie i amarimbar.
Mal mentean i hánor, i laitalelma pa elde ua nauva lusta pa natto sina, mal ve equétien é nauvalde manwe.
Uan mere i tuluvar quelli Maceroniallo asinye ar hiruvar le úmanwaine, ar sie elme – lá quetien elde – nauvar nucumne pan anelme ta tance.
Etta sannen i mauyane menta i hánor epe ni lenna, carien manwa nóvo i aistana anna pa ya antanelde vanda yá. Sie é nauvas manwa ve aistana anna, ar lá maustanen.
Ya quétan ná si: Sé ye rere pitya yando samuva pitya yávie, ar sé ye rere úve yando samuva úvea yávie.
Lava ilquenen care ve icílies endaryasse, lá nurrula hya maustanen, an Eru mele ye ná valima antien.
Ar Eru pole anta len ilya lisse úvesse, tyarien le illume same ilqua fáresse ar úve ilya mára cardan,
ve anaie técina: "Ivinties, ánies penyain; failierya lemya tennoio."
Mal ye anta erde yen rere ar masta matien antuva len erdelda ar caruva sa úvea, ar failieldo yávi caruvas lárie.
Mi ilqua nalde cárine lárie, lávala len anta lérave mi ilya lé, ar sie Eru camuva hantale elvenen.
An núromolie sina ua carna eryave quatien i airion mauri, mal tyaris yando úve hantaléva Erun.
Núromolie sina tana voronwelda íre nalde tyastaine. Etta antalte alcar Erun, pan panyaldexer nu i Hristo evandilyon, ve pantave quetilde i carilde, ar antalde lérave tien and illin.
Hyamilte len ar milyar le, castanen i lahtala Erulisseo ya samilde.
Hantale Erun annaryan quettar pella!
Mí Hristo quilda ar moica lé inye, Paulo, ique lello – inye ye ná "caurea" íre cenin le cendelello cendelenna, mal "canya" lenna íre uan tasse aselde.
É iquin lello i íre tuluvan ua mauyuva nin lenga mí canya lé ya apacenin, innar sanar i vantalme i hrávenen.
An ómu vantalme i hrávesse, ualme ohtacare i hrávenen.
An ohtacarielmo carmar uar hráveva, mal nalte túrie Erunen nuquerien artar.
Nuquerilme sanweataqui ar ilya varanda nat ya ná ortaina i istyanna Eruo, ar panyalme ilya sanwe mandosse tyarien sa himya i Hristo canwar.
Ar nauvalme manwe paimetien ilya carda ya ua *canwacimya, apa elde anaier cárine aqua *canwacimye.
Yétalde nati ilceltanen. Qui aiquen ná tanca i náse i Hristova, nai enyaluvas i tambe sé i Hristova ná, síve yando elme nar.
An yando qui laitanyexe acca ole pa i túre ya i Heru antane men – carastien le ama ar lá narcien le undu – uan nauva nucumna.
Uan mere náve tuntaina ve névala ruhta le mentanyainen.
An, quetilte: "Mentaryar nar torye ar túrie, mal íre sé immo tule, náse milya, ar questarya *nattírima."
Nai taiti queni hanyuvar i tambe nalme mentalmassen íre nalme oa, síve nalme yando cardassen íre nalme aselde.
An ualme verya note imme imíca i quetir mai pa inte, hya sesta imme as té. Íre nalte véra lestalta, ar sestar inte as inte, ualte saile.
Mal elme uar laituvar imme han i lesta, mal i lestanen ya Eru apánie men: i arahtielme yando eldenna.
Ualme eménier acca hairave laitala imme, ve carumnelme qui ualme túle lenna. Mal é utúlielme lenna tálala i Hristo evandilyon.
Ualme mene han rénalmar laitiénen imme pa molie cárina lo exi, mal samilme i estel i íre savielda ale, yando elme nauvar cárine taure imíca le, i lestanen antaina men. Tá rahtuvalme en ambe hairave,
cárala i evandilyon sinwa i nóriessen han le. Sie uan laituva imne pa molie carna exeo ménasse, yasse nati nar manwaine nóvo.
"Mal ye laitaxe, nai laituvas inse i Hérusse."
An ye anta insen mára *vettie ua ye lahtuva i tyestiesse, mal sé yen i Héru anta mára *vettie.
Merin i lavumnelde pitya lesta alasailiéva nillo. Mal é lavilde sa nillo!
An şúlenya urya len valaina uriénen, an *ertanenye le as er veru, talienyan le i Hristonna ve poica vende.
Mal sí rucin ho i mi sina hya sana lé – ve i leuca şahtane Éve cururyanen – sámaldar nauvar hastaine ar talaine oa meliello i Hristo holmo ar poiciénen.
An lavilde sa mai qui quén tule ar quete pa hyana Hristo – lá i imya ye elme carner sinwa – hya ñetilde faire ya ua i imya ya eñétielde yá, hya evandilyon ya ua i imya ya acámielde.
An namin i muntasse loitan i antare aposteli.
Yando qui nanye úfinya pa quettar, uan sie pa istya, mal mi ilya lé atánielme ta len.
Lau carnen úcare íre carnenyexe nalda ortien lé, pan úpaityana carnen Eruo evandilyon sinwa?
Hyane ocombellon "pílen", cámala tulco tello carien núromolie elden.
Ar íre engen aselde ar sáme maure, únen cóla aiquenen, an maurenyar náner quátine lo i hánor i túler Maceroniallo. Mi ilya lé hempenyexe ar hepuvanyexe návello cólo len.
Ve Hristo nanwie ea nisse, uan nauva pustaina sie laitiello imne Maceronio ocombessen.
Ar manen sie? Pan uan mele le? Eru ista i carin!
Ar ya cáran yando caruvan, lá antien ecie in merir ecie, náveltan hírine vávie aselme mi ta pa ya laitaltexer.
An taiti neri nar úanwe aposteli, húrala *mólindor, vistala inte mir Hristo aposteli.
Ar ta ua elmenda, an Sátan immo vistaxe mir vala calava.
Etta ua túra nat qui núroryar vistar inte mir núror failiéno. Mal mettalta nauva ta ya hilya cardaltallon.
Quetin ata: Lava *úquenen sana i nanye auco! Mal qui é sanalde sie, cama ni yando ve qui nanye auco, lávala yando nin laita imne pitya lestasse!
Ya quetin uan quete i Herullo, mal ve lénen auco, íre laitanyexe ta tancave!
Pan rimbali laitar inte i hrávenen, yando inye laituvaxe!
An mi alasse lavilde aucoin care natiltar – elde i nar ta saile!
Lavilde sa íre quén care le móli, hya ammate ya samilde, hya mapa armaldar, hya ortaxe or elde, hya pete le i cendelenna!
Nanye nucumna quétala i elme anaier acca milye carien ta!Mal ilqua pa ya aiquen verya laita inse – quétan ve auco – yando inye verya laita inse pa ta!
Ma nalte Heveryar? Yando inye ná! Ma nalte Avrahámo erde? Yando inye ná!
Mal nalte Hristo núror? Quetin ve qui nanye ettesse sámanyo: Inye ná sie en ambe! Omótien ambe, anaien mandosse ambe rimbave, acámien ambe rimbe petiéli, anaien hare qualmenna rimbave.
Mi lúr lempe acámien Yúrallon petier *canaquean hequa er.
Nel anaien palpaina vandillínen; lú er anaien *saryaina. Nel i cirya yasse anen lende undu; ter lóme yo aure anaien i núre nenissen,
rimbe lendalissen, raxesse sírellon, raxesse pilullon, raxesse véra lienyallo, raxesse i hyane nórellon, raxesse mí osto, raxesse mí ravanda, raxesse earesse, raxesse úanwe hánollon,
mi mótie ar urdie, rimbave ú húmeo, maitiesse ar soiciesse, rimbave pénala matso, nála ringa ar ú lannion.
Ar napánina i natin i ettesse ea yando i ilaurea nirie ninna, i quárele ilye i ocombin.
Mal ná milya, ar inye ua milya? Man ná tyarna lanta ar inye ua urya?
Qui mauya i laituvanyexe, laituvanyexe pa i nati yassen nanye milya.
Yésus Hristo Herulvo Aino ar Atar, é ye nauva laitana tennoio, ista i uan húra.
Lamascusse i nóretur nu aran Aretas tirne i Lamascearon osto ni-mapien,
mal ter assa i rambasse anen mentaina undu colcasse ar úşe et máryalto.
Mauya nin laita imne. Uas aşea, mal sí tuluvan ceniennar ar apantiennar i Heruva.
Istan nér mi Hristo ye loar canaque yá – i hroasse hya ettesse i hravo uan ista, Eru ista – náne mapaina oa i nelya menelenna.
É istan taite nér – i hroasse hya ettesse i hravo uan ista, Eru ista –
anes mapaina oa Erumanna ar hlasse avaquétime quettali, yar atan ua lerta quete.
Pa taite nér laituvanyexe, mal lá pa inye, hequa pa milya sómanya.
An qui é merin laita imne, ta ua carumne ni auco, an quetumnen i nanwie. Mal laian, pustien aiquen saniello pa ni tárave lá ya cenis i nanye, hya hlaris nillo.
Pustien ni návello turquima i úvenen apantiéva acámien necel i hrávesse, Sátan vala, petieryan ni; sie uan nauva acca turquima.
Pa ta arcanen i Herullo nel i autumnes nillo,
mal i Heru quente ninna: "Lissenya farya lyen, an túrenya ná carna ilvana milya sómasse." Mi antúra alasse, etta, laituvanyexe pa milya sómanya, tyárala i Hristo túre caita ninna.
Etta nanye valima íre nanye milya, íre perperin orme, íre samin maure, íre nanye roitaina hya urdiessen, Hriston. An íre nanye milya, tá nanye torya.
Anaien auco! Elde mauyaner nin náve sie, an rohtalda nin náne quete mai pa ni. An mi munta loitan i antare aposteli, ómu nanye munta.
Nanwa apostelo tanwar náner cárine mici le ilya cólenen, tanwalínen ar elmendalínen ar túrie cardar.
An mi mana anelde cárine pitye lá i hyane ocombi, hequa i inye úne cólo len? Ánin apsene úfailie sina!
Yé, si ná i neldea lú yasse nanye manwa tulien lenna. Ar uan nauva cólo, an uan cesta ya samilde, mal cestan lé. An híni uar same rohta care haura ontarultant, mal ontaru hínaltain.
Ar mi alasse *yuhtuvan ya samin, ar nauvan *yuhtaina, fealdain. Qui melin le i ambe, ma nauvan mélina i mis?
Mal yando qui ta ná sie, inye úne cólo len – mal nála ruscuite cé nampen le fintalénen?
Lau carnen ñetie et lello aiquennen ye mentanen lenna?
Hortanen Títo ar mentane i háno óse. Lau Títo oi carne ñetie et lello? Ma uamme vantane i imya fairesse, i imya tiesse?
Ma ter quanda lúme sina asánielde i elden tarilme ama inwen? Erunna, i Hristosse, quetilme! Mal ilqua, meldanyar, ná carastien le ama.
An rucin ho i íre tuluvan, uan hiruva le ve polin mere, ar inye ua nauva len ve elde polir mere, mal i mende euvar mi sina hya sana lé costier, *hrúceni, rúşie felmi, costi, ulca quetie pa exi, hlustier, *valatie, rúcina sóma.
Cé, íre entuluvan, Ainonya nucumuva ni mici le, ar mauyuva nin same nyére pa rimbali i úcarner yá ar uar ihírie inwis pa i úpoicie ar i *hrupuhtie ar i lehta lengie yainen elengielte.
Si ná i nelya lú yasse túlan lenna. Astarmo atta hya neldeo antonen ilya natto nauva tulcaina.
Equétien yá, íre engen aselde apa túlen lenna mí attea lú, ar sí íre nanye oa quetin nóvo innar úacárier yá ar ilye i exennar: Qui oi nanwenuvan uan tanuva oravie,
pan cestalde tanwa tulcala i inyenen Hristo quete, sé ye ua milya lenna mal túrea mici lé.
É anes tarwestaina milya sómaryanen, mal náse coirea Eruo túrenen. Ar é elme nar milye óse, mal nauvalme coirie óse Eruo túrenen lenna.
Á tyasta inde qui nalde i saviesse; na tance pa mana nalde. Hya ualde hepe sámasse i Yésus Hristo ea lesse? Qui ualde oloitie i tyastiesse.
Mal samin i estel i hanyuvalde i elme uar oloitie i tyastiesse.
Hyamilme Erunna i caruvalde munta ulca, lá tanien i elme alahtier íre anelme tyestaine, mal carieldan márie, yando qui *şéyuva i elme oloitier.
An polilme care munta i nanwienna; polilme eryave manya i nanwie.
An nalme valime íre elme nar milye mal elde nar torye, an nat sinan hyámalme: i nauvalde cárine ilvane.
Etta técan nati sine íre nanye oa, pustien imne lengiello mi naraca lé íre euvan aselde, i túrenen ya i Heru antane nin – carastien ama ar lá narcien undu.
Teldave, hánor: Namárie! Na cárine ilvane, na tiutaine, na er sámo, mara rainesse; ar i Aino melmeva ar raineva euva aselde.
Á *suila quén i exe aire miquenen!
Ilye i airi *suilar le!
Nai i Heru Yésus Hristo lisse ar Eruo melme ar masselda i Aire Feasse euvar as illi mici le!
Paulo, apostel, lá atanillon hya ter atan, mal ter Yésus Hristo ar Eru i Atar, ye ortane se et qualinillon,
ar ilye i hánor asinye, i ocombennar Alatiasse:
Nai samuvalde lisse ar raine ho Eru Atarelva ar i Heru Yésus Hristo,
ye antane inse úcarelvain, etelehtieryan vi et i olca randallo ya ea sí, i indómenen Ainolvo ar Atarelvo,
yen na i alcar tennoio ar oi. Násie.
Nanye elmendasse i ta lintave anaielde quérine yello le-yalle mir i Hristo lisse hyana evandilyonna.
Mal ta ua hyana, ono ear queneli i le-valtar ar merir rice i evandilyon pa i Hristo.
Mal yando qui elme, hya vala menello, talar lenna hyana evandilyon lá ve i evandilyon ya carnelme sinwa len – nai nauvas húna!
Ve equétien nóvo, sí quetin ata: Aiquen ye tala lenna evandilyon han ya camnelde, nai nauvas húna!
Ma sí atanin névan tana i quetin nanwie, hya Erun? Qui en carnen atani valime, únen Hristo mól.
An lavin len ista, hánor, i sana evandilyon ya náne talaina len lo ní ua tule atanillon,
an inye ua camne sa ho atan hya náne peantaina sasse, hequa apantiénen Yésus Hristollo.
An ahlárielde pa lengienya i vanwiesse, manen veassenen roitanen Eruo ocombe ar néve nancaritas,
ar lenden andave Yúrangolmesse epe rimbali lienyasse i sámer i imya nóte loaron, nála ole ambe uryala i fairesse i haimin i atarillon.
Mal íre náne mára Erun, ye talle ni et amillinyo mónallo ar yalle ni lisseryanen,
apanta Yondorya inyenen – carienyan i evandilyon pa sé sinwa i nórin – uan same *artaquetie as hráve yo serce.
Ente, uan lende ama Yerúsalemennna innar náner aposteli nó inye, mal lenden oa mir Aravia, ar nanwennen Lamascusenna.
Epeta, apa loar nelde, lenden ama Yerúsalemenna velien Céfas, ar lemnen óse ter rí lepenque.
Mal uan velle aiquen hyana imíca i aposteli, mal eryave Yácov, i Heruo háno.
Yar técan len – yé, epe Eruo hendu, uan húra!
Epeta lenden mir i ménar Sírio ar Cilicio.
Mal únen sinwa cendelénen i ocombin Yúreasse mi Hristo;
eryave hlasselte: “I nér ye nóvo vi-roitane sí care sinwa i evandilyon ya néves nancare yá.”
Ar antanelte alcar Erun pa ní.
Epeta, apa loar canaque, lenden ata ama Yerúsalemenna as Varnavas, tálala yando Títo.
Mal lenden ama apa apantie. Ar nyarnen tien pa i evandilyon ya carin sinwa imíca i nóri – mal vérave, epe i náner minde neri, rúcala i mi sina hya tana lé norin hya onórien muntan.
Mal *úquen, yando lá Títo ye nánen asinye, náne *oscirna maustanen, pan anes Hellenya.
Ta martane castanen i úanwe hánoron talaine minna quildesse, i túler minna nuldave *ettirien i lérie ya samilve Yésus Hristonen, vi-carieltan móli.
Tien ualme lave same írelta, lá erya lún – tyarien i evandilyono nanwie termare aselde.
Mal pa i queni i *şéner náve valde – i nostale quenion ya anelte yá, ua valda nin, an Eru ua cime atano ilce – inyen sine valde queni nyarner munta vinya.
Úsie, íre cennelte manen i evandilyon náne panyaina hepienyasse talienyan sa i *úoscirnannar, ve Péter sáme sa i *oscirnain –
an Sé ye ontane Péteresse i túre náveo apostel i *oscirnain, antane túreli yando inyen in nar i nórion –
ar Yácov ar Céfas ar Yohannes túner i lisse antaina nin, té i *şéner náve tarmar, tá antanelte nin ar Varnavassen málta otornasseo: Emme menumnet i nórennar, mal té i *oscirnannar.
Eryave mauyane ment enyale i penyar, ar ta enévien care holmo.
Mal íre Céfas túle Antiocenna, tarnen senna cendelello cendelenna, an tarnes námina.
An nó i tulesse nerelion Yácovello mantes as queni i nórion, mal íre túlelte, quernes inse oa ar *ciltane inse, rúcala i *oscirnallon.
I hyane Yúrar yando hilyaner se *imnetiesse, tenna yando Varnavas náne tulyaina oa aselte *imnetieltasse.
Mal íre cennen i ualte vantane térave i nanwiénen i evandilyono, quenten Céfanna epe te illi: “Qui elye, ómu nalye Yúra, lenga ve quén i nórion ar laia ve Yúra, manen ná i elye mauya quenin i nórion hilya i Yúra lé?”
Elme nassenen nar Yúrar, ar lá úcarindor i nórellon,
ar istar atan ua samuva failie cardainen Şanyeo, mal eryave saviénen Hristo Yésusse – yando qui asávielve Yésus Hristosse samien failie saviénen Hristosse, ar lá cardainen Şanyeo. An ua ea hráve ya samuva failie cardainen Şanyeo.
Mal qui elme i cestar same failie Hristonen nar yando hírine úcarindor, ma Hristo nanwave úcareo núro ná? Laume!
An qui carastan ama i imye nati yar hanten undu, tanan i nanye *ongwemo.
An inye şanyenen qualle şanyen, návenyan coirea Erun.
Nanye tarwestaina as Hristo. Nanye coirea, ananta ta ua ní, mal Hristo ye ná coirea nisse. I coivie ya sí samin hrávesse, samin i saviénen yondosse Eruo, sé ye emélie ni ar ánie inse inyen.
Uan panya oa Eruo lisse, an qui failie ea şanyenen, i Hristo anwave qualle muntan.
A lamanwe Atatyar, man uluhtie le, elde epe ion hendu Hristo náne tanaina tarwestaina ve emmanen?
Sina nat erinqua merin pare lello: Ma camnelde i Faire cardainen şanyeo hya hláriénen mi savie?
Ma nalde ta lamanwe? Apa sámelde yestalda fairesse, ma sí nalde telyaine hrávesse?
Ma perpérelde ta rimbe nati muntan? Qui é martanes muntan.
Etta, sé ye anta len i Faire ar care taure cardali mici le, ma castarya carien sie cardar şanyeo, hya hlarie saviénen?
Mi imya lé Avraham “sáve i Hérusse, ar ta náne nótina sen ve failie”.
É istalde i té i himyar savie, té nar Avrahámo yondor.
Mal i Tehtele, apacénala i Eru saviénen *failatumne queneli i nórion, carne i evandilyon sinwa nóvo Avrahámen: “Elyenen ilye i nóri nauvar aistane.”
Sie i himyar savie nar aistane as Avraham i Sarta.
An illi i himyar cardar Şanyeo nar nu hútie, an ná técina: “Húna ná ilquen ye ua termare mi ilye i nati técine i parmasse i Şanyeo, carien tai.”
I *úquen ná carna faila epe Eru aşcénima ná, an “i faila samuva coivie saviénen”.
I Şanye ua himya savie, mal “ye care tai samuva coivie tainen”.
Hristo vi-mancane insen et i hútiello i Şanyeo návenen carna hútie nómelvasse, an ná técina: “Húna ná ilquen lingala tarwesse.”
Sie i aistie ya Avraham camne tulumne i nórennar Yésus Hristonen, camielvan saviénen i faire pa ya samilve vanda.
Hánor, quetin i lénen atanion: Apa vére anaie tulcaina, *úquen panya sa oa hya napane sanna, ómu nás atano vére.
Mal Avrahámen i vandar náner quétina, ar erderyan. Sa ua quete 'ar erderyain', ve qui ear rimbeli, mal ve qui ea er: 'Ar eldelyan,' ye ná i Hristo.
Ente, quetin: Pa i vanda nóvo tulcaina lo Eru, i Şanye ya túle loar *nelequean ar tuxi cantar apa sa ua sa-care lusta, nancárala i vanda.
An qui rantalva ve aryoni tule Şanyenen, uas en linga vandanen, mal Avrahámen Eru ánie sa vandanen.
Tá mana castan i Şanye enge? Anes napánina carien ongwi aşcénime, tenna tulesse i erdeo yen i vanda anaie carna, ar anes antaina ter valali, enelmo mánen.
Mal ua ea enelmo yasse ea er erinqua, mal Eru ná er.
Ma i Şanye tá tare ana Eruo vandar? Laume! An qui şanye náne antaina ya polle anta coivie, é enge failie şanyenen.
Mal i Tehtele panyane ilye nati uo i mandosse úcareo, ar sie i vanda ya tule saviello nauva antaina in savir.
Mal nó i savie túle, anelme ortíriesse nu şanye, nála mandosse uo, tenna i savie ya tulumne náne apantaina.
Sie i Şanye vi-peantane ar vi-tulyane Hristonna, návelvan cárine faile saviénen.
Mal sí íre savie utúlie, ualve ambe nu *peantar.
An illi mici le nar yondoli Eruo savieldanen Yésus Hristosse.
An ilye elde i náner *tumyaine mir Hristo apánier Hristo indesse.
Ua ea Yúra hya Hellenya, ua ea mól hya léra quén, ua ea nér hya nís; an nalde illi er, Hristo Yésusse.
Ente, qui nalde i Hristova, é nalde Avrahámo erde, aryoni i vandanen.
Sí quetin i íre i aryon ná lapse náse muntasse arya lá mól, ómu náse heru ilye nation.
Náse nu mardili tenna i aure ya atarya cille nóvo.
Mí imya lé yando elve, íre anelve lapsi, náner móli i penye nation i mardo.
Mal íre i quanta lúme túle, Eru etementane Yondorya, nóna lo nís, nóna nu Şanye,
mancieryan i náner nu Şanye insen, leryala te. Sie yando elven ece came nóme ve yondoryar.
Mal pan nalde yondor, Eru mentane Yondoryo faire mir endalva, yámala: “Appa, Atar!”
Sie ual ambe mól, mal yondo; ar qui yondo, yando aryon ter Eru.
Ananta, íre ualde sinte Eru, anelde móli in uar nassenen ainor.
Mal sí, apa sintelde Eru – hya ambe mai quétina, sí íre anaielde sinwe lo Eru – manen ná i nanquerilde inde i milye ar penye natinnar ar merir náve móli tain ata?
Hepilde auri ar astar ar lúmi ar loar.
Rucin i mi sina hya tana lé omólien muntan mici le.
Hánor, arcan lello: Na ve inye, an anen yando ve lé! Carnelde munta raica nin.
Mal istalde manen hlívenen hrávenyasse carnen i evandilyon sinwa len mí minya lú.
Ar ya náne tyastie len hrávenyasse ualde nattirne hya piutane sanna, mal ve Eru vala camnelde ni, ve Hristo Yésus.
Masse, tá, ea i alasse ya sámelde? An nanye astarmo len i qui ta náne cárima, hocirnelde vére henduldat antien tu nin.
Ve qui nanye sí cotumolda pan nyáran len i nanwie?
Cestalte le uryala írenen, lá márien, mal merilte cire le oa nillo, cestieldan té uryala írenen.
Mal mára ná náve cestaina mára castan, ar lá eryave íre ean aselde,
hínanyar, in ata samin naiceli tenna Hristo nauva cátina lesse.
Mal ece nin mere i engen tasse aselde sí, quetien aselde mi hyana lé, an uan ista mana caruvan pa le.
Nyara nin, elde i merir náve nu şanye: Ma ualde hlare i Şanye?
An ná técina i enget yondo atta as Avraham, er i *núrenen ar er i léra nissenen.
Ono ye enge i *núrenen náne nóna mí lé i hráveo, mal i exe lo i léra nís, vandanen.
Tai nar emma túlala nation. An nissi sine nar vére atta. I minya ná ho Oron Sínai, ya cole híni *mólienna, ar ya ná Háhar.
Sí Háhar sina ná Sínai, oron mi Aravia, ar náse emma i Yerúsalémo ya ea sí, an náse *móliesse as hínaryar.
Mal Yerúsalem Tarmende ná léra, ar sé amillelva ná.
An ná técina: “Na valima, a nís ye ua colle hína; sama alasse ar yama, elye ye ua sáme naice nóniéva! An i *yávelóra nisso híni nar rimbe lá i híni i nisso ye sáme i veru.”
Mal elve, hánor, nar híni i vandanen, ve náne Ísac.
Mal tambe ye náne nóna mí lé hráveo roitane ye náne nóna mí lé faireo, síve yando sí.
Ono mana i Tehtele quete? “Á menta oa i *núre ar yondorya, an i *núreo yondo laume nauva aryon as i léra nisso yondo.”
An mir taite lérie i Hristo leryane ve. Etta tara tulce, ar áva lave inden náve hépine nu yanta mólo.
Yé! Inye, Paulo, nyára len i qui nalde *oscírine, Hristo ua nauva aşea len.
Ente, *vettan ata ilquenen ye ná *oscírina i samis i rohta hepiéva i quanda Şanye.
Nalde şance Hristollo, elde i nevir náve cárine faile i Şanyenen; et i Erulissello alantielde.
An elme fairenen yétar ompa i failienna saviénen pa ya samilme estel.
An Hristo Yésusse *oscirie ar *úoscirie véla uar pole care *aiqua, mal savie melmenen pole.
Nornelde mai. Mana pustane le hiliello i nanwie en?
Ualde sie şahtaine lo ye yalle le.
Pitya *pulmaxe *pulta i quanda luppo.
Nanye tanca pa elde i nar i Herusse i ualde sanuva mi hyana lé, mal sé ye tyára hrangie len coluva námierya, lá címala man náse.
Ar pa ní, hánor, qui en *nyardean *oscirie, mana i casta roitien ni? Tá i *auquérala nat i tarweo anaie carna lusta.
Merin i té i valtar le ciruvar oa i quanda nat!
An anelde yáline mir lérie, hánor; eryave áva *yuhta lérie sina antien ecie i hráven, mal melmenen na móli quén i exen.
An i quanda Şanyeo nonwe ná er quetiesse: “Mela armarolya ve imle.”
Mal qui nacilde ar ammatir quén i exe, cima i ualde nar nancárine quén lo i exe!
Mal quetin: Á vanta fairenen, ar laume hilyuvalde i hráveo íre!
An i hráve tare i fairenna íreryasse, ar i faire i hrávenna; an natte cotye, er i exenna, pustien le cariello yar merilde.
Mal qui nalde tulyaine fairenen, ualde nu şanye.
I hráveo cardar nar aşcénine, ar nalte *hrupuhtie, úpoicie, lehta lengie,
*cordontyerie, ñúle, cos, mahtie, uryala felme, rúşe, costier, şancier, *ciltaine hereni,
*hrúcen, *limpunquie, *accasucier ar vávealtar. Pa nati sine nyarin len pa i raxe nóvo, ve yando nyarnen len yá: I carir taiti nati uar nauva aryoni i araniéno Eruo.
Mal i faireo yáve ná melme, alasse, raine, cóle, lisse, márie, savie,
moicie, *immoturie. Ana taiti nati ua ea şanye.
Ente, i nar Hristo Yésuo atarwestie i hráve as maileryar ar íreryar.
Qui samilve coivie fairenen, yando alve vanta i fairenen.
Ávalve cesta cumna alcar, valtala quén i exe, arwe *hrúceno quén i exenna.
Hánor, qui quén care sina hya tana loima, á tana sen i téra malle mi moica lé, elde i nar i faireo, íre ilquen mici le tire inse, hya cé yando elde nauvar úşahtaine.
Cola quén i exeo cólar, ar sie á hilya i Hristo şanye.
An qui quén sana i náse valda íre náse munta, nyaris huru insen.
Mal nai ilquen ceşe véra molierya, ar tá samuvas castarya alassen mi ta ya sé acárie, lá sestala inse as hyana quén.
An ilquen coluva véra cólarya.
Ente, nai ye pare i quetta lavuva yen peanta sen same ranta mi ilye máre nati.
Áva ranya: Mo ua lerta quete yaiwe Erunna. An ya nér rere, ta yando *cirihtuvas,
an ye rere hráveryasse *cirihtuva nancarie hráveryallo, mal ye rere i fairesse *cirihtuva oira coivie i fairello.
Etta ávalve na lumbe ar pusta care ya mára ná, an íre i lúme tuluva, *cirihtuvalme qui ualve yerya.
Nanwave, íre samilve ecie, nai caruvalve márie ilquenen, ar or ilqua in nar nosselva i saviesse.
Ela i hoe tengwar yar etécien len véra mányanen!
I merir mauya le náve *oscírine nar i merir care i hráve vanima, mérala uşe roitie i Hristo tarwenen.
An i nar *oscírine, té uar hepe i Şanye, mal merilte i yando elde nauvar *oscírine, samieltan casta laitien inte pa hrávelda.
Nai inye ua oi laituva imne, hequa i tarwenen Yésus Hristo, yenen i mar anaie atarwestaina inyen, ar inye i marden.
An ya ná valda ua *oscirie hya *úoscirie, mal náve vinya onna.
Illi i merir vanta sina şanyenen lengiéva, tien na raine ar lisse, ar Israélo Ainon.
Ho sí nai *úquen tyaruva nin urdie, an cólan hroanyasse i tanwar Yésus Hristova.
Nai Herulva Yésus Hristo lisse euva as fairelda, hánor. Násie!
