#!/bin/bash

dataset="$2"
init_model="$3"
scarcity="$4"
if [ -z "${dataset}" ]; then
    dataset="new_testament"
fi

logfile="outputs/log.txt"
vocab_dir="./vocab"
models="./models"

# avoid overwriting zip if it already exists
echo "Creating zip file and log file"
i=0
while true; do
    zipfile="outputs/${1}_${dataset}_${i}.zip"
    if [ -f $zipfile ]; then
        let i+=1
    else
        break
    fi
done

echo "" > $logfile
echo "" > outputs/test_outputs.txt
echo "" > outputs/test_outputs_local.txt
echo "Created $zipfile and $logfile"

# run training or testing
if [ "$1" = "train_cpu" ]; then
    python run.py train --train-src=./../../data/${dataset}/train.el --train-tgt=./../../data/${dataset}/train.en \
        --dev-src=./../../data/${dataset}/val.el --dev-tgt=./../../data/${dataset}/val.en --vocab=${vocab_dir}/el_8500_en_50000_vocab.json --batch-size=2 \
        --max-epoch=201 --valid-niter=100 2>&1 | tee $logfile
elif [ "$1" = "test_cpu" ]; then
    mkdir -p outputs
    touch outputs/test_local_outputs.txt
    python3 run.py decode model.bin ./../../data/${dataset}/test.el ./../../data/${dataset}/test.en outputs/test_outputs_local.txt 2>&1 | tee $logfile

# train on just the elvish data
elif [ "$1" = "train_gpu" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py train --train-src=./../../data/${dataset}/train.el --train-tgt=./../../data/${dataset}/train.en \
        --dev-src=./../../data/${dataset}/val.el --dev-tgt=./../../data/${dataset}/val.en --vocab=${vocab_dir}/el_8500_en_50000_vocab.json \
        --save-to=./models/el_en_8500_dropout_05_model.bin --dropout=0.5 --valid-niter=250 --cuda 2>&1 | tee $logfile
# train on just the elvish data
elif [ "$1" = "train_gpu_full" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py train \
        --train-src=./../../data/${dataset}/train.el \
        --train-tgt=./../../data/${dataset}/train.en \
        --dev-src=./../../data/${dataset}/val.el \
        --dev-tgt=./../../data/${dataset}/val.en \
        --vocab=${vocab_dir}/el_8500_en_50000_vocab.json \
        --save-to=./models/el_en_8500_model_full.bin \
        --dropout=0.5 \
        --valid-niter=250 \
        --max-epoch=200 \
        --test-niter=1000 \
        --cuda 2>&1 | tee $logfile
elif [ "$1" = "test_gpu" ]; then
    mkdir -p outputs
    touch outputs/test_outputs.txt
    CUDA_VISIBLE_DEVICES=0 python run.py decode ./models/${init_model} ./../../data/${dataset}/test.el ./../../data/${dataset}/test.en outputs/test_outputs.txt --cuda 2>&1 | tee $logfile

# train the full finnish data set
elif [ "$1" = "train_gpu_fin" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py train --train-src=./../../data/${dataset}/train.fi --train-tgt=./../../data/${dataset}/train.en \
        --dev-src=./../../data/${dataset}/val.fi --dev-tgt=./../../data/${dataset}/val.en --vocab=${vocab_dir}/fi_8500_en_50000_vocab.json \
        --batch-size=16 --dropout=0.2 --max-epoch=5 --valid-niter=250  --save-to=./fin_model.bin --cuda 2>&1 | tee $logfile

# the with init runs use the optimizer of the init model insteaf of resetting lr and decay
elif [ "$1" = "train_gpu_fin_with_init" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py train --train-src=./../../data/${dataset}/train.fi --train-tgt=./../../data/${dataset}/train.en \
        --dev-src=./../../data/${dataset}/val.fi --dev-tgt=./../../data/${dataset}/val.en --vocab=${vocab_dir}/fi_8500_en_50000_vocab.json \
        --batch-size=16 --dropout=0.2 --init=./models/fin_8500_model_5.bin --save-to=./models/fi_8500_model_10.bin --max-epoch=6 --valid-niter=250  --cuda 2>&1 | tee $logfile
elif [ "$1" = "train_gpu_with_init" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py train --train-src=./../../data/${dataset}/train.el --train-tgt=./../../data/${dataset}/train.en \
        --dev-src=./../../data/${dataset}/val.el --dev-tgt=./../../data/${dataset}/val.en  --vocab=${vocab_dir}/el_8500_en_50000_vocab.json\
        --init models/${init_model} --dropout=0.5 --max-epoch=30 --valid-niter=250  --save-to=./models/transfer_8500_5_keep_vocab_90.bin --cuda 2>&1 | tee $logfile

elif [ "$1" = "test_gpu_fin" ]; then
    mkdir -p outputs
    touch outputs/test_outputs.txt
    CUDA_VISIBLE_DEVICES=0 python run.py decode \
        ./models/fi_8500_model_10.bin \
        ./../../data/${dataset}/test.fi \
        ./../../data/${dataset}/test.en \
        outputs/test_outputs.txt 
        --cuda 2>&1 | tee $logfile

# train/test finnish like its a scarce language on 8000 examples (this is the same amount as the elvish data)
# or other levels of scarcity
# ./run.sh train_gpu_fin_scarce europarl None <100/1000/8000/100000>
# each of these come need their own vocab file
elif [ "$1" = "train_gpu_fin_scarce" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py train \
        --train-src=./../../data/${dataset}/train_${scarcity}.fi \
        --train-tgt=./../../data/${dataset}/train_${scarcity}.en \
        --dev-src=./../../data/${dataset}/val.fi \
        --dev-tgt=./../../data/${dataset}/val.en \
        --vocab=${vocab_dir}/fi_en_vocab_scarce_${scarcity}_exs.json \
        --save-to=${models}/fi_en_scarce_${scarcity}_exs.model.bin \
        --batch-size=16 \
        --dropout=0.3 \
        --valid-niter=250 \
        --max-epoch=30 \
        --cuda 2>&1 | tee $logfile
# ./run.sh test_gpu_fin_scarce europarl None <scarcity> (test set kept separate from europarl)
# ./run.sh test_gpu_fin_scarce finnish  None <scarcity> (alternate test set)
elif [ "$1" = "test_gpu_fin_scarce" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py decode \
        ${models}/fi_en_scarce_${scarcity}_exs.model.bin \
        ./../../data/${dataset}/test.fi \
        ./../../data/${dataset}/test.en \
        outputs/test_outputs.txt \
        --cuda 2>&1 | tee $logfile

# uses --transfer-learning flag which makes it so the prev optimizer state is not used
# ie resets learning rate and decay
elif [ "$1" = "transfer_train_gpu" ]; then
    CUDA_VISIBLE_DEVICES=0 python run.py train \
        --train-src=./../../data/${dataset}/train.el \
        --train-tgt=./../../data/${dataset}/train.en \
        --dev-src=./../../data/${dataset}/val.el \
        --dev-tgt=./../../data/${dataset}/val.en  \
        --vocab=${vocab_dir}/el_8500_en_50000_vocab.json \
        --init=./models/${init_model} \
        --dropout=0.5 \
        --max-epoch=200 \
        --valid-niter=250 \
        --save-to=./models/transfer_8500_model26.bin \
        --transfer-learning \
        --test-niter=1000 \
        --init-tgt-vocab \
        --cuda 2>&1 | tee $logfile


# train language model for data augmentation
elif [ "$1" = "train_lm" ]; then
    CUDA_VISIBLE_DEVICES=0 python run_lang_model.py train \
        --train=./../../data/${dataset}/train.en \
        --dev=./../../data/${dataset}/val.en  \
        --vocab=${vocab_dir}/el_8500_en_50000_vocab.json \
        --dropout=0.5 \
        --max-epoch=200 \
        --valid-niter=250 \
        --save-to=${models}/train_lang_model.bin \
        --test-niter=1000 \
        --batch-size=16 \
        --cuda 2>&1 #| tee $logfile

elif [ "$1" = "train_lm_backward" ]; then
    CUDA_VISIBLE_DEVICES=0 python run_lang_model.py train \
        --train=./../../data/${dataset}/train.en \
        --dev=./../../data/${dataset}/val.en  \
        --vocab=${vocab_dir}/el_8500_en_50000_vocab.json \
        --dropout=0.5 \
        --max-epoch=200 \
        --valid-niter=250 \
        --save-to=${models}/train_lang_model_backward.bin \
        --test-niter=1000 \
        --backward \
        --batch-size=16 \
        --cuda 2>&1 #| tee $logfile


elif [ "$1" = "test_lm" ]; then
    CUDA_VISIBLE_DEVICES=0 python3 run_lang_model.py generate \
        ${models}/train_lang_model.bin \
        ${models}/train_lang_model_backward.bin \
        ./../../data/${dataset}/train.en \
        outputs/test_outputs \
        --filter-file=./../../data/filtered_new_testament_words.txt \
        --blacklist-threshold 1000 \
        --k 3 \
        --cuda \
        2>&1 #| tee $logfile

else
	echo "Invalid Option Selected" | tee $logfile
fi

# put the commit hash in the log
commit_hash=$(git rev-parse HEAD)
echo "Commit hash $commit_hash" | tee -a $logfile

# snapshot the input and output data
#zip -r $zipfile ../../data/${dataset}/ outputs/test_outputs*.txt $logfile
