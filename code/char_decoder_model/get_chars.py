def get_all_chars():
  file_name = './../../data/europarl/train.fi'

  all_chars = set()
  with open(file_name, 'r', encoding='utf8') as file:
    for line in file:
      all_chars |= set("".join(line.split()))

    all_char_str = ''.join(sorted(list(all_chars)))

  with open("./chars.txt", 'w', encoding='utf8') as file:
    file.write(all_char_str)


get_all_chars()


    
