#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
"""

### YOUR CODE HERE for part 1h
import torch
import torch.nn as nn

class Highway(nn.Module):
    """ Highway network:
        - skip connection controlled by a dynamic gate
    """

    def __init__(self, embed_word, dropout_rate=0.2):
        """ Init Highway Model

        @param embed_word (int): Embedding size of word 
        @param dropout_rate (float): Dropout probability, for final word embedding

        """
        super(Highway, self).__init__()

        self.e_word = embed_word

        ### Define model
        ###
        ### - self.projection: computes x_proj
        ### - self.gate: computes x_gate
        ### - self.dropout: performs dropout to get final word embedding
        self.projection = nn.Linear(in_features=embed_word, out_features=embed_word, bias=True)
        self.relu = nn.ReLU()
        self.gate = nn.Linear(in_features=embed_word, out_features=embed_word, bias=True)
        self.sigmoid = nn.Sigmoid()
        self.dropout = nn.Dropout(p=dropout_rate)

    def forward(self, source: torch.Tensor) -> torch.Tensor:
        """ Take a tensor, which is output of the CNN, and uses highway network to
        output a word embedding

        @param source (Tensor): tensor of dimension (batch size, word embedding size)
                The output of CNN 

        @returns word_embedding (Tensor): tensor of dimension (batch size, word embedding size)
                The value of word embedding
        """
        x_proj = self.relu(self.projection(source))
        x_gate = self.sigmoid(self.gate(source))

        x_highway = x_gate * x_proj + (1 - x_gate) * source 

        x_word_embed = self.dropout(x_highway)

        return x_word_embed


### END YOUR CODE 

