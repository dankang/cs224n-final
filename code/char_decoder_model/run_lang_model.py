#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: VaLar NMT (final project)
run_lang_model.py: train language model used for data augmentation
Authors: Kais Kudrolli, Daniel Kang

Usage:
    run.py train --train=<file> --dev=<file> --vocab=<file> [options]
    run.py generate [options] MODEL_PATH BACK_MODEL_PATH TEST_FILE OUTPUT_FILE

Options:
    -h --help                               show this screen.
    --cuda                                  use GPU
    --train=<file>                          train file
    --backward                              train a backward language model
    --k=<int>                               k words from which to extract hypotheses [default: 3]
    --dev=<file>                            dev file
    --vocab=<file>                          vocab file
    --seed=<int>                            seed [default: 0]
    --batch-size=<int>                      batch size [default: 32]
    --embed-size=<int>                      embedding size [default: 256]
    --hidden-size=<int>                     hidden size [default: 256]
    --clip-grad=<float>                     gradient clipping [default: 5.0]
    --log-every=<int>                       log every [default: 10]
    --max-epoch=<int>                       max epoch [default: 30]
    --input-feed                            use input feeding
    --patience=<int>                        wait for how many iterations to decay learning rate [default: 5]
    --max-num-trial=<int>                   terminate training after how many trials [default: 5]
    --lr-decay=<float>                      learning rate decay [default: 0.5]
    --lr=<float>                            learning rate [default: 0.001]
    --uniform-init=<float>                  uniformly initialize all parameters [default: 0.1]
    --save-to=<file>                        model save path [default: model.bin]
    --valid-niter=<int>                     perform validation after how many iterations [default: 2000]
    --dropout=<float>                       dropout [default: 0.3]
    --max-decoding-time-step=<int>          maximum number of decoding time steps [default: 70]
    --no-char-decoder                       do not use the character decoder
    --init=<file>                           initialization of the model
    --init-vocab                            initialize the target vocabulary with given vocab
    --test-niter=<int>                      frequency to test our model by
    --filter-file=<file>                    file containing words to filter out of augment candidate search
    --blacklist-threshold=<int>             how many times a word can be a candidate for augmentation before not being used
"""
import math
import sys
import pickle
import time


from docopt import docopt
from language_model import LanguageModel
import numpy as np
from typing import List, Tuple, Dict, Set, Union
from tqdm import tqdm
from utils import read_corpus_lm, batch_iter_lm
from vocab import Vocab, VocabEntry

import torch
import torch.nn.utils
import json


# Questions:
# (1) when you train, do you just pass a whole line? no label? just get loss from outputs and min? 
#     ans: can pass in the whole sentence, no label
# (2) when text do you just start with seed text? what is the input to each time step?
#     ans: need to predict with seed text
# (3) how does a backward LSTM work? are both forward and backward bidirectional? or two layers??
#     ans: flip the input
# (4) am I ignoring padding correctly?
#     ans: dont need the enc_masks bc no attention
# (5) why are all the pads "proactive"
#     ans: no longer there
# (6) is ppl calculation correct!
#     ans: looks ok

# add to this list once a word has been used too much
#blacklisted_words = set()
word_usage = dict()

def get_filter_indices(model, filter_fname):
    filter_indices = []
    with open(filter_fname, 'r') as f: # computer gods please forgive me for opening this file every time
        for line in f:
            filter_word = line.strip()
            filter_index = model.vocab.tgt.word2id[filter_word]
            #if (filter_word not in blacklisted_words):
            filter_indices.append(filter_index)

            usage_count = word_usage.get(filter_word, 1)

    if (torch.cuda.is_available()): 
        return torch.LongTensor(filter_indices).cuda()
    else:
        return torch.LongTensor(filter_indices)

def extract_hypotheses(model, logits, k, filter_fname, blacklist_threshold):
    hypotheses = []
    #print(blacklisted_words)
    
    # make the dims (batch, src_len, vocab_size)
    logits = logits.permute(1, 0, 2)
    vocab_size = logits.size()[2]

    # take top k of the last dim
    filter_indices = get_filter_indices(model, filter_fname)

    if (torch.cuda.is_available()): 
        div_factors = torch.ones(logits.size()[2], dtype=torch.float).cuda()
    else:
        div_factors = torch.ones(logits.size()[2], dtype=torch.float)
    for i in filter_indices:
        div_factors[i] = word_usage.get(model.vocab.tgt.id2word[i.item()], 1)

    # enforce top x vocab words to have -infty values for logits
    # Note: there are 5,591 words in English in New Testament data that appear at least twice
    # And as this vocab is directly copied over to first 5591 of fi_8500_en_50000, we ignore about 6000
    #logits[:,:,:6000] = float('-inf')
    selected_logits = torch.index_select(logits, dim=-1, index=filter_indices)
    
    #mask = torch.zeros(logits.size()[2])
    #mask.scatter_(0, filter_indices, 1)
    #mask = mask.type(torch.LongTensor)

    if (torch.cuda.is_available()): 
        x = torch.Tensor(logits.size()).cuda().fill_(float('-inf'))
    else:
        x = torch.Tensor(logits.size()).fill_(float('-inf'))
    x.index_copy_(-1, filter_indices, selected_logits)
    x = x * div_factors.view(1, 1, vocab_size)
    
    #index = torch.ones(logits.size()[2])
    #index.scatter_(0, filter_indices, 0)
    #index = index.type(torch.LongTensor)

    #logits.index_fill_(-1, index, float('-inf'))

    aaa, indices = torch.topk(x, k, dim=-1, sorted=True)
    assert(indices.size() == (logits.size()[0], logits.size()[1], k))
    #indices = torch.argmax(logits, dim=-1, keepdim=True)
    #assert(indices.size() == (logits.size()[0], logits.size()[1], 1))

    # look up each index in vocab dictionary and form sentences
    num_sents, num_words, _ = indices.size()
    for b in range(num_sents): 
        sent = []
        for w in range(num_words):
            '''
            index = indices[b][w][0].item()
            word = model.vocab.tgt.id2word[index]
            sent.append(word)
            '''
            for i in range(k):
                index = indices[b][w][i].item()
                word = model.vocab.tgt.id2word[index]
                logit = x[b][w][index].item()
                
                sent.append((word, logit))

                # keep track of how many times we used a word has been used
                # if its been used too many times, blacklist it
                if (word in word_usage):
                    word_usage[word] += 1
                else:
                    word_usage[word] = 1

                #if (word_usage[word] > blacklist_threshold):
                #    blacklisted_words.add(word)

        hypotheses.append(sent)

    #print(hypotheses)
    return hypotheses

def evaluate_ppl(model, dev_data, batch_size=32, source_lengths=None, args=None, backward=False):
    """ Evaluate perplexity on dev sentences
    @param model (LanguageModel): language model
    @param dev_data (list of (src_sent, tgt_sent)): list of tuples containing source and target sentence
    @param batch_size (batch size)
    @returns ppl (perplixty on dev sentences)
    """
    hypotheses = []
    was_training = model.training
    model.eval()

    cum_loss = 0.
    cum_tgt_words = 0.

    # issues
    # (1) backward and forward are not predicting same word at each index of their hypotheses lists...one of these needs to be reversed, problem is its sorted by order
    #     the backward is actually ok, gets sorted by decreasing order of length, which is what we want, forward is wrong. the logits will come out in wrong order
    #     ans: I think this is resolved: just reverse the logits of the forward?

    # (2) topk doesn't work right now, just gets single characters
    # (3) how am I even training this, if give sentence I am Kais, I never give it a label of </s> to check against do I?
        # => this is computed by using cross entropy with the sentence you give. 
        # => e.g. At step "I am", it uses the next char "Kais" and compares it against the prediction of LSTM Model
    # (4) (minor, may not need to change) scores at test time reflect the score of the whole sentence not just the last word, even though only pulling out last word

    # no_grad() signals backend to throw away all gradients
    with torch.no_grad():
        for sents in batch_iter_lm(dev_data, batch_size, shuffle=False, source_lengths=source_lengths):
            scores, logits = model(sents, was_training) # calls language model here
            loss = -scores.sum()

            cum_loss += loss.item()
            tgt_word_num_to_predict = sum(len(s[1:]) for s in sents)  # omitting leading `<s>`
            cum_tgt_words += tgt_word_num_to_predict

            if not was_training:
                if (not backward):
                    # for forward logits come out in reverse order of how we want them, so reverse them
                    logits = torch.flip(logits, dims=[1])
                hypotheses.append(extract_hypotheses(model, logits, int(args['--k']), str(args['--filter-file']), int(args['--blacklist-threshold'])))

        ppl = np.exp(cum_loss / cum_tgt_words)

    if was_training:
        model.train()

    return ppl, hypotheses


def train(args: Dict):
    """ Train the Language Model.
    @param args (Dict): args from cmd line
    """
    train_data = read_corpus_lm(args['--train'], args['--backward'])
    dev_data = read_corpus_lm(args['--dev'], args['--backward'])

    train_batch_size = int(args['--batch-size'])

    clip_grad = float(args['--clip-grad'])
    valid_niter = int(args['--valid-niter'])
    log_every = int(args['--log-every'])
    model_save_path = args['--save-to']
    test_niter = int(args['--test-niter']) if args['--test-niter'] is not None else None

    vocab = Vocab.load(args['--vocab'])

    if (args['--init'] is None):
        print("Initializing new model")
        model = LanguageModel(embed_size=int(args['--embed-size']),
                              hidden_size=int(args['--hidden-size']),
                              vocab=vocab, 
                              dropout_rate=float(args['--dropout']))
        uniform_init = float(args['--uniform-init'])
        if np.abs(uniform_init) > 0.:
            print('uniformly initialize parameters [-%f, +%f]' % (uniform_init, uniform_init), file=sys.stderr)
            for p in model.parameters():
                p.data.uniform_(-uniform_init, uniform_init)
    else:
        print("load model from {}".format(args['--init']), file=sys.stderr)

        # if either of these flag is set, we use the corresponding vocabulary from the given vocab json file
        # otherwise, we use the vocab saved with the model
        init_vocab = args['--init-vocab'] or False

        print("Using vocabulary from {}".format(args['--vocab'] if init_vocab else "previous model"))

        model = LanguageModel.load(args['--init'], vocab=vocab, init_vocab=init_vocab)
        
    model.train()

    device = torch.device("cuda:0" if args['--cuda'] else "cpu")
    print('use device: %s' % device, file=sys.stderr)

    model = model.to(device)

    optimizer = torch.optim.Adam(model.parameters(), lr=float(args['--lr']))

    if (args['--init']):
        print("Using optimizer state from previous model")
        optimizer.load_state_dict(torch.load(args['--init'] + '.optim'))

    num_trial = 0
    train_iter = patience = cum_loss = report_loss = cum_tgt_words = report_tgt_words = 0
    cum_examples = report_examples = epoch = valid_num = 0
    hist_valid_scores = []
    train_time = begin_time = time.time()
    print('begin Maximum Likelihood training')

    bleu_in_context, bleu_out_context, test_iters = [], [], []
    output_in_context, output_out_context = None, None

    val_iters, val_loss, val_perplexity = [], [], []

    converged = False

    while not converged:
        epoch += 1
        
        if epoch > int(args['--max-epoch']):
            print('reached maximum number of epochs!', file=sys.stderr)
            converged = True
            break

        for sents in batch_iter_lm(train_data, batch_size=train_batch_size, shuffle=True):
            train_iter += 1

            optimizer.zero_grad()

            batch_size = len(sents)

            example_losses = -model(sents)[0] # (batch_size,)
            batch_loss = example_losses.sum()
            loss = batch_loss / batch_size

            loss.backward()

            # clip gradient
            grad_norm = torch.nn.utils.clip_grad_norm_(model.parameters(), clip_grad)

            optimizer.step()

            batch_losses_val = batch_loss.item()
            report_loss += batch_losses_val
            cum_loss += batch_losses_val

            tgt_words_num_to_predict = sum(len(s[1:]) for s in sents)  # omitting leading `<s>`
            report_tgt_words += tgt_words_num_to_predict
            cum_tgt_words += tgt_words_num_to_predict
            report_examples += batch_size
            cum_examples += batch_size

            if train_iter % log_every == 0:
                print('epoch %d, iter %d, avg. loss %.2f, avg. ppl %.2f ' \
                      'cum. examples %d, speed %.2f words/sec, time elapsed %.2f sec' % (epoch, train_iter,
                                                                                         report_loss / report_examples,
                                                                                         math.exp(report_loss / report_tgt_words),
                                                                                         cum_examples,
                                                                                         report_tgt_words / (time.time() - train_time),
                                                                                         time.time() - begin_time), file=sys.stderr)

                train_time = time.time()
                report_loss = report_tgt_words = report_examples = 0.

            # perform validation
            if train_iter % valid_niter == 0:
                print('epoch %d, iter %d, cum. loss %.2f, cum. ppl %.2f cum. examples %d' % (epoch, train_iter,
                                                                                         cum_loss / cum_examples,
                                                                                         np.exp(cum_loss / cum_tgt_words),
                                                                                         cum_examples), file=sys.stderr)
                val_iters.append(train_iter)
                val_loss.append(cum_loss / cum_examples)
                val_perplexity.append(np.exp(cum_loss / cum_tgt_words))

                cum_loss = cum_examples = cum_tgt_words = 0.
                valid_num += 1

                print('begin validation ...', file=sys.stderr)

                # compute dev. ppl and bleu
                dev_ppl, _ = evaluate_ppl(model, dev_data, batch_size=64)   # dev batch size can be a bit larger
                valid_metric = -dev_ppl

                print('validation: iter %d, dev. ppl %f' % (train_iter, dev_ppl), file=sys.stderr)

                is_better = len(hist_valid_scores) == 0 or valid_metric > max(hist_valid_scores)
                hist_valid_scores.append(valid_metric)

                if is_better:
                    patience = 0
                    print('save currently the best model to [%s]' % model_save_path, file=sys.stderr)
                    model.save(model_save_path)

                    # also save the optimizers' state
                    torch.save(optimizer.state_dict(), model_save_path + '.optim')
                elif patience < int(args['--patience']):
                    patience += 1
                    print('hit patience %d' % patience, file=sys.stderr)

                    if patience == int(args['--patience']):
                        num_trial += 1
                        print('hit #%d trial' % num_trial, file=sys.stderr)
                        if num_trial == int(args['--max-num-trial']):
                            print('early stop!', file=sys.stderr)
                            converged = True
                            break

                        # decay lr, and restore from previously best checkpoint
                        lr = optimizer.param_groups[0]['lr'] * float(args['--lr-decay'])
                        print('load previously best model and decay learning rate to %f' % lr, file=sys.stderr)

                        # load model
                        params = torch.load(model_save_path, map_location=lambda storage, loc: storage)
                        model.load_state_dict(params['state_dict'])
                        model = model.to(device)

                        print('restore parameters of the optimizers', file=sys.stderr)
                        optimizer.load_state_dict(torch.load(model_save_path + '.optim'))

                        # set new lr
                        for param_group in optimizer.param_groups:
                            param_group['lr'] = lr

                        # reset patience
                        patience = 0

            # perform testing
            #if test_niter is not None and train_iter % test_niter == 0:
            #    test_iters.append(train_iter)
            #    (output_in_context, output_out_context) = evaluate(model, bleu_in_context, bleu_out_context, args)
                
    if model_save_path:
        args_save_path = model_save_path + ".args"
        print("Saving arguments to %s" % args_save_path)
        with open(args_save_path, 'w') as outfile:
            json.dump(args, outfile)

        results_save_path = model_save_path + ".results"
        results = {
            'val_iters': val_iters,
            'val_loss': val_loss,
            'val_perplexity': val_perplexity
        }

        #if test_niter is not None:
        #    test_iters.append(train_iter)
        #    (output_in_context, output_out_context) = evaluate(model, bleu_in_context, bleu_out_context, args)

        #    results = {
        #        **results,
        #        'test_iters': test_iters, 
        #        'bleu_in_context': bleu_in_context, 
        #        'bleu_out_context': bleu_out_context,
        #        'output_in_context': output_in_context,
        #        'output_out_context': output_out_context
        #    }

        print("Saving training results to %s" % results_save_path)
        with open(results_save_path, 'w') as outfile:
            json.dump(results, outfile)
    
    exit(0)

def data2ngrams(data):
    ngram_data = []
    source_lengths = []
    for sent in data:
        ngram = []
    
        for word in sent[:-2]:
            ngram.append(word)
            ngram_data.append(ngram[:])
        source_lengths.append(len(sent) - 2)

    return ngram_data, source_lengths

def generate(args: Dict[str, str], backward=False):
    """ Performs decoding on a test set, and save the best-scoring decoding results.
    If the target gold-standard sentences are given, this function computes the corpus PPL
    @param args (Dict): args from cmd line
    """

    print("load test source sentences from [{}]".format(args['TEST_FILE']), file=sys.stderr)
    test_data = read_corpus_lm(args['TEST_FILE'], backward)
    test_data, source_lengths = data2ngrams(test_data)

    if (backward):
        model = LanguageModel.load(args['BACK_MODEL_PATH'])
        print("load model from {}".format(args['BACK_MODEL_PATH']), file=sys.stderr)
    else:
        model = LanguageModel.load(args['MODEL_PATH'])
        print("load model from {}".format(args['MODEL_PATH']), file=sys.stderr)

    if args['--cuda']:
        model = model.to(torch.device("cuda:0"))

    model.eval()

    if args['TEST_FILE']:
        # compute PPL
        ppl, hypotheses = evaluate_ppl(model, test_data, batch_size=64, source_lengths=source_lengths, args=args, backward=backward)
        print('Corpus PPL: {}'.format(ppl), file=sys.stderr)

    return hypotheses

def main():
    """ Main func.
    """
    args = docopt(__doc__)

    # Check pytorch version
    # assert(torch.__version__ == "1.0.0"), "Please update your installation of PyTorch. You have {} and you should have version 1.0.0".format(torch.__version__)

    # seed the random number generators
    seed = int(args['--seed'])
    torch.manual_seed(seed)
    if args['--cuda']:
        torch.cuda.manual_seed(seed)
    np.random.seed(seed * 13 // 7)

    if args['train']:
        train(args)
    elif args['generate']:
        fwd_hyps = generate(args, backward=False)
        bwd_hyps = generate(args, backward=True)

        # combine backward and forward hypothesis
        comb_hyps = []
        for f_sent, b_sent in zip(fwd_hyps, bwd_hyps):
            comb_hyps_sent = []
            for f, b in zip(f_sent, b_sent):
                f_cands = {key:val for key, val in f}
                b_cands = {key:val for key, val in b}

                set_fb = list(set(f_cands.keys()).intersection(set(b_cands.keys())))

                if len(set_fb) > 0:
                    best_fb = max([(f_cands[fb] + b_cands[fb], fb)for fb in set_fb])[1]
                    comb_hyps_sent.append([best_fb])
                else:
                    comb_hyps_sent.append([])

                # get intersection of two language models
                '''
                set_fb = set(f).intersection(set(b))
                comb_hyps_sent.append(list(set_fb))
                '''
                #comb_hyps_sent.append([f, b])

            comb_hyps.append(comb_hyps_sent)
            
        # use this to output in pickle format
        with open(args['OUTPUT_FILE'], 'wb') as f:
            pickle.dump(comb_hyps, f)

        # use this to output more human-friendly output
        with open(args['OUTPUT_FILE'] + '.txt', 'w', encoding='utf8') as f:
            for hyp_sent in comb_hyps:
                f.write(str(hyp_sent) + '\n')
        

    else:
        raise RuntimeError('invalid run mode')


if __name__ == '__main__':
    main()

