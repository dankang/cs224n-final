#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
"""

### YOUR CODE HERE for part 1i
import torch
import torch.nn as nn

class CNN(nn.Module):
    """ CNN network:
        - convolutional network that involves 1-dimensional convolution and max pooling
    """

    def __init__(self, char_embed, word_embed, k=5):
        """ Init CNN

        @param char_embed (int): dimension of character embedding
        @param word_embed (int): dimension of word embedding
        @param k (int): kernel size of CNN
        """
        super(CNN, self).__init__()
        self.char_embed = char_embed
        self.word_embed = word_embed
        self.k = k

        ### Define model
        ###
        ### - self.conv : convolutional network to compute x_conv
        ### - self.relu : computes relu
        self.conv = nn.Conv1d(in_channels=char_embed, out_channels=word_embed, kernel_size=k, bias=True)
        self.relu = nn.ReLU()

    def forward(self, source: torch.Tensor) -> torch.Tensor:
        """ Take a tensor, which is the character embedding,
            and output result after passing through convolutional layer and max-pool

        @param source (Tensor): tensor of dimension (batch size, e_char, m_word)
            The character embedding
        @returns x_conv_out (Tensor): tensor of dimension (batch_size, e_word)
        """
        x_conv = self.conv(source) # (batch size, f(=e_word), (m_word - k + 1))
        x_conv_out, _  = torch.max(self.relu(x_conv), 2) # (batch size, e_word)

        # sanity checks
        """
        [batch_size, e_char, m_word] = list(source.size())
        x_conv_size = list(x_conv.size())
        assert len(x_conv_size) == 3
        assert x_conv_size[0] == batch_size
        assert x_conv_size[1] == self.word_embed
        assert x_conv_size[2] == (m_word - self.k + 1)

        x_conv_out_size = list(x_conv_out.size())
        assert len(x_conv_out_size) == 2
        assert x_conv_out_size[0] == batch_size
        assert x_conv_out_size[1] == self.word_embed
        """

        return x_conv_out

### END YOUR CODE

