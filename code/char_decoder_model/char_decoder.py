#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
"""

import torch
import torch.nn as nn

class CharDecoder(nn.Module):
    def __init__(self, hidden_size, char_embedding_size=50, target_vocab=None):
        """ Init Character Decoder.

        @param hidden_size (int): Hidden size of the decoder LSTM
        @param char_embedding_size (int): dimensionality of character embeddings
        @param target_vocab (VocabEntry): vocabulary for the target language. See vocab.py for documentation.
        """
        ### YOUR CODE HERE for part 2a
        ### TODO - Initialize as an nn.Module.
        ###      - Initialize the following variables:
        ###        self.charDecoder: LSTM. Please use nn.LSTM() to construct this.
        ###        self.char_output_projection: Linear layer, called W_{dec} and b_{dec} in the PDF
        ###        self.decoderCharEmb: Embedding matrix of character embeddings
        ###        self.target_vocab: vocabulary for the target language
        ###
        ### Hint: - Use target_vocab.char2id to access the character vocabulary for the target language.
        ###       - Set the padding_idx argument of the embedding matrix.
        ###       - Create a new Embedding layer. Do not reuse embeddings created in Part 1 of this assignment.
        super(CharDecoder, self).__init__()

        V_char = len(target_vocab.char2id)
        pad_token_idx = target_vocab.char2id['<pad>']

        self.charDecoder = nn.LSTM(input_size=char_embedding_size, hidden_size=hidden_size) 
        self.char_output_projection = nn.Linear(in_features=hidden_size, out_features=V_char, bias=True)
        self.decoderCharEmb = nn.Embedding(num_embeddings=V_char, embedding_dim=char_embedding_size, padding_idx=pad_token_idx)
        self.target_vocab = target_vocab
        
        print("Freezing whole char decoder network")
        for param in self.charDecoder.parameters():
            param.requires_grad = False
        for param in self.char_output_projection.parameters():
            param.requires_grad = False
        
        print("Freezing target char embedding")
        self.decoderCharEmb.weight.requires_grad = False
        ### END YOUR CODE


    
    def forward(self, input, dec_hidden=None):
        """ Forward pass of character decoder.

        @param input: tensor of integers, shape (length, batch)
        @param dec_hidden: internal state of the LSTM before reading the input characters. A tuple of two tensors of shape (1, batch, hidden_size)

        @returns scores: called s_t in the PDF, shape (length, batch, self.vocab_size)
        @returns dec_hidden: internal state of the LSTM after reading the input characters. A tuple of two tensors of shape (1, batch, hidden_size)
        """
        ### YOUR CODE HERE for part 2b
        ### TODO - Implement the forward pass of the character decoder.

        X = self.decoderCharEmb(input) # (length, batch, char_embedding_size)
        output_batch, dec_hidden = self.charDecoder(X, dec_hidden)
        scores = self.char_output_projection(output_batch) # (length, batch, V_char)
        
        return scores, dec_hidden
        ### END YOUR CODE 


    def train_forward(self, char_sequence, dec_hidden=None):
        """ Forward computation during training.

        @param char_sequence: tensor of integers, shape (length, batch). Note that "length" here and in forward() need not be the same.
        @param dec_hidden: initial internal state of the LSTM, obtained from the output of the word-level decoder. A tuple of two tensors of shape (1, batch, hidden_size)

        @returns The cross-entropy loss, computed as the *sum* of cross-entropy losses of all the words in the batch.
        """
        ### YOUR CODE HERE for part 2c
        ### TODO - Implement training forward pass.
        ###
        ### Hint: - Make sure padding characters do not contribute to the cross-entropy loss.
        ###       - char_sequence corresponds to the sequence x_1 ... x_{n+1} from the handout (e.g., <START>,m,u,s,i,c,<END>).

        input = char_sequence[:-1, :]  # (length - 1, batch)
        scores, dec_hidden = self.forward(input, dec_hidden)  # (length - 1, batch, V_char)
        target = char_sequence[1:, :] # (length - 1, batch)

        [_, _, V_char] = list(scores.size())
        scores = scores.contiguous().view(-1, V_char) # ((length - 1) x batch, V_char)
        target = target.contiguous().view(-1) # ((length - 1) x batch)

        padding_idx = self.target_vocab.char2id['<pad>']
        loss_func = nn.CrossEntropyLoss(ignore_index=padding_idx, reduction='sum')
        loss = loss_func(scores, target)

        return loss

        ### END YOUR CODE

    def decode_greedy(self, initialStates, device, max_length=21):
        """ Greedy decoding
        @param initialStates: initial internal state of the LSTM, a tuple of two tensors of size (1, batch, hidden_size)
        @param device: torch.device (indicates whether the model is on CPU or GPU)
        @param max_length: maximum length of words to decode

        @returns decodedWords: a list (of length batch) of strings, each of which has length <= max_length.
                              The decoded strings should NOT contain the start-of-word and end-of-word characters.
        """

        ### YOUR CODE HERE for part 2d
        ### TODO - Implement greedy decoding.
        ### Hints:
        ###      - Use target_vocab.char2id and target_vocab.id2char to convert between integers and characters
        ###      - Use torch.tensor(..., device=device) to turn a list of character indices into a tensor.
        ###      - We use curly brackets as start-of-word and end-of-word characters. That is, use the character '{' for <START> and '}' for <END>.
        ###        Their indices are self.target_vocab.start_of_word and self.target_vocab.end_of_word, respectively.

        [_, batch, hidden_size] = list(initialStates[0].size())

        decoded_words = [""] * batch

        # define initial inputs
        input_batch = torch.tensor([self.target_vocab.start_of_word] * batch, device=device)

        states = initialStates
        for ind in range(max_length):
            input_batch = torch.unsqueeze(input_batch, 0)  # (1, batch)

            (scores, states) = self.forward(input_batch, states) # scores: (1, batch, V_char)

            scores = scores.squeeze(0) # scores: (batch, V_char)
            softmax_scores = nn.functional.softmax(scores, dim=1)

            # get the maximum index, which is also the input for next run
            input_batch = torch.max(softmax_scores, 1)[1] 

            max_inds = input_batch.data.tolist() # (batch)

            max_chars = [self.target_vocab.id2char[ind] for ind in max_inds]

            # update decoded words
            decoded_words = [first + second for first, second in zip(decoded_words, max_chars)]

        # truncate after end-of-word characters
        eow = self.target_vocab.id2char[self.target_vocab.end_of_word]
        for ind, word in enumerate(decoded_words):
            last_ind = word.find(eow)
            last_ind = max_length if last_ind == -1 else last_ind
            decoded_words[ind] = word[:last_ind]

        return decoded_words
        
        ### END YOUR CODE
