#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: VaLar NMT (final project)
Authors: Kais Kudrolli, Daniel Kang
"""

import sys

import torch
import torch.nn as nn
import torch.nn.functional as F
from typing import List, Tuple
from torch.nn.utils.rnn import pad_packed_sequence, pack_padded_sequence

class LanguageModel(nn.Module):
    """ LSTM Language Model 
    - a neural language model that can generate the next word in a sentence 
    - based on previous words in the sentence using a unidirectional LSTM
    """

    def __init__(self, embed_size, hidden_size, vocab, dropout_rate=0.2):
        """ Init Language Model

        @param embed_size (int): Embedding size of word 
        @param dropout_rate (float): Dropout probability, for final word embedding
        @param hidden_size (float): hidden size of lstm
        @param vocab (Vocab): vocabulary object, NOTE: has both src and tgt, but here only tgt used (English for us)

        """
        super(LanguageModel, self).__init__()

        self.embed_size = embed_size
        self.dropout_rate = dropout_rate
        pad_token_idx = vocab.tgt.word2id['<pad>']
        self.hidden_size = hidden_size
        self.vocab = vocab
        self.vocab_size = len(vocab.tgt.word2id)

        ### Define model
        ###
        ### - self.dropout: performs dropout to get final word embedding
        ### - self.lstm: at each time step encodes the sentence seen up until that time step
        ###              is one layer, number of time steps inferred from input length
        ### - self.word_embeddings: set of word embeddings trained with the LM
        ### - self.linear: small fc layer at the end 
        ###    
        self.word_embeddings = nn.Embedding(self.vocab_size, embed_size, pad_token_idx)
        self.lstm = nn.LSTM(embed_size, hidden_size, bidirectional=False)
        self.dropout = nn.Dropout(p=dropout_rate)
        self.linear = nn.Linear(hidden_size, self.vocab_size)

    def forward(self, input: List[List[str]], train=True) -> Tuple[torch.Tensor, torch.Tensor]:
        """ Take a mini-batch of input sentences, compute the log-likelihood of
        output sentence from the language model

        @param input (List[List[str]]): list of source input tokens (len(input) = b)

        @returns scores (Tensor): a variable/tensor of shape (b, ) representing the
                                    log-likelihood of generating the gold-standard target sentence for
                                    each example in the input batch. Here b = batch size.
        """
        enc_hiddens, dec_init_state = None, None

        b = len(input)
        e = self.embed_size
        h = self.hidden_size

        source_lengths = [len(s) for s in input]
        src_len = max(source_lengths)
        # pad input
        input_padded = self.vocab.tgt.to_input_tensor(input, device=self.device)
        assert(input_padded.size() == (src_len, b))

        # look up word embeddings for each word in each sentence
        X = self.word_embeddings(input_padded)
        assert(X.size() == (src_len, b, e))
        X = self.dropout(X)
        assert(X.size() == (src_len, b, e))

        # pass padded sentences of word embeddings through lstm
        X_packed = pack_padded_sequence(X, source_lengths)
        enc_hiddens, (last_hidden, last_cell) = self.lstm(X_packed)
        (enc_hiddens, _) = pad_packed_sequence(enc_hiddens)
        assert(enc_hiddens.size() == (src_len, b, h))
        
        enc_hiddens = self.dropout(enc_hiddens)
        assert(enc_hiddens.size() == (src_len, b, h))
        #enc_hiddens = enc_hiddens.permute(1, 0, 2)
        #assert(enc_hiddens.size() == (src_len, b, h))

        # run output through an fc layer
        outputs = self.linear(enc_hiddens)
        assert(outputs.size() == (src_len, b, self.vocab_size))

        # final output through log softmax
        P = F.log_softmax(outputs, dim=-1)
        assert(P.size() == (src_len, b, self.vocab_size))

        # Zero out, probabilities for which we have nothing in the target text
        target_masks = (input_padded != self.vocab.tgt['<pad>']).float()

        # Compute log probability of generating true target words
        target_gold_words_log_prob = torch.gather(P[:-1], index=input_padded[1:].unsqueeze(-1), dim=-1).squeeze(-1) * target_masks[1:]
        scores = target_gold_words_log_prob.sum(dim=0)
        assert(scores.size() == (b,))

        if (not train):
            src_len = 1
            # for every time step of P, pull out just the P for the last word
            P_new = P[source_lengths[0] - 1][0].unsqueeze(0).unsqueeze(0)
            for i in range(1,b):
                s = source_lengths[i]-1
                #print(P_new.size())
                #print(P[s][i].unsqueeze(0).unsqueeze(0).size())
                P_new = torch.cat((P_new, P[s][i].unsqueeze(0).unsqueeze(0)), dim=1)

            P = P_new

            #print(len(source_lengths))
            #print(P.size())
            #print("b = {}".format(b))
            #print("h = {}".format(h))
            assert(P.size() == (src_len, b, self.vocab_size))

        return scores, P

    @property
    def device(self) -> torch.device:
        """ Determine which device to place the Tensors upon, CPU or GPU.
        """
        return self.linear.weight.device

    @staticmethod
    def load(model_path: str, vocab=None, init_vocab=False):
        """ Load the model from a file.
        @param model_path (str): path to model
        """
        params = torch.load(model_path, map_location=lambda storage, loc: storage)
        args = params['args']

        lm_vocab = params['vocab']

        # use src vocab from given vocab file
        if init_vocab:
            lm_vocab.src = vocab.src
            lm_vocab.tgt = vocab.tgt

        # use tgt vocab from given vocab file
        if init_vocab: 
            lm_vocab.tgt = vocab.tgt

        model = LanguageModel(vocab=lm_vocab, **args)
        model.load_state_dict(params['state_dict'])

        return model

    def save(self, path: str):
        """ Save the odel to a file.
        @param path (str): path to the model
        """
        print('save model parameters to [%s]' % path, file=sys.stderr)

        params = {
            'args': dict(embed_size=self.embed_size, hidden_size=self.hidden_size, dropout_rate=self.dropout_rate),
            'vocab': self.vocab,
            'state_dict': self.state_dict()
        }

        torch.save(params, path)

