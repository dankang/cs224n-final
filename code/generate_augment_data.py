import pickle
import nltk
import re
from functools import reduce
from itertools import product

class Augment(object):
    def __init__(self, enel_dict, dist, threshold=0, substitute_multiple=False):
        self.dict = enel_dict
        self.dist = dist
        self.threshold = threshold
        self.substitute_multiple = substitute_multiple

    def clean_word(self, word):
        word = word.strip()
        word = re.sub('[!"#$%&\'()*+,-./:;<=>?@\[\]^_`|~«·»–‘’“”]','',word)
        return word

    '''
        Find the word that has minimum distance with any word in the sentence and that is not repeated
    '''
    def get_closest_word(self, target, tokenized_el):
        return min([(self.dist(word, target), word, ind) for ind, word in enumerate(tokenized_el) if tokenized_el.count(word) <= 1])


    '''
        Find the keys in en_el dictionary whose key is close enough
        to given english word
    '''
    def get_candidate_keys(self, en_word):
        # find an english key in dictionary that is close to dictionary key
        return [key for key in self.dict.keys() if self.dist(en_word, key) <= self.threshold]               

    '''
        Given set of candidate english keys with corresponding position in english sentence,
        look up if corresponding word exists in elvish sentence
    '''
    def get_el_en_pairs(self, tokenized_el, keys):
        pairs = set()
        for key in keys:
            elvish_set = self.dict[key]
                    
            for elv_word in elvish_set:
                thres, elv_closest_word, elv_ind = self.get_closest_word(elv_word, tokenized_el)
                if thres <= self.threshold:
                    pairs.add((key, elv_closest_word, elv_ind))
        
        return pairs
                
    '''
        - el: elvish sentence
        - en_word: english word we consider replacing
        - candidates: list of candidate words we could replace this en_word with
    '''

    def filter_candidate(self, candidate, actual, actual_clean):
        if candidate in [actual, actual_clean, "‘the"]:
            return False
        
        return True
                
    def search_candidate(self, tokenized_el, tokenized_en, en_ind, candidates):
        result = []
        tokenized_clean_el = [self.clean_word(w) for w in tokenized_el]
        tokenized_clean_en = [self.clean_word(w) for w in tokenized_en]
        en_word = tokenized_clean_en[en_ind]
        
        candidates = [x for x in candidates if self.filter_candidate(x, tokenized_en[en_ind], en_word)]
        
        if len(candidates) == 0:
            return result
        
        # if we have repetition, just return empty as we don't want to risk wrong translation
        if tokenized_clean_en.count(en_word) >= 2:
            return result
        
        # find an english key in dictionary that matches dictionary key
        candidate_english_keys = self.get_candidate_keys(en_word)
        
        if len(candidate_english_keys) == 0 :
            return result
        
        # find set of elvish words that match the word in last sentence
        el_en_pairs = self.get_el_en_pairs(tokenized_clean_el, candidate_english_keys)        
        if len(el_en_pairs) == 0:
            return result
        
        # find set of candidate keys in dictionary that we could use
        candidate_augment_keys = reduce((lambda x, y: x + self.get_candidate_keys(y)), candidates, [])
        
        if len(candidate_augment_keys) == 0:
            return result

        candidate_el_words = []
        for aug_key in candidate_augment_keys:
            new_word_set = self.dict[aug_key]
             candidate_el_words.append((aug_key, random.choice(new_word_set)))
            '''
            for new_word in new_word_set:
                candidate_el_words.append((aug_key, new_word))
            '''
                
        candidate_el_inds = [pair[-1] for pair in el_en_pairs]
        
        # Need to get product of 
        # 1. Candidate index of elvish word that correspond to current query english word
        # 2. Replacements
        
        # we need to return : [(en_ind, el_ind, new_en, new_el)] 
        
        operations = product(candidate_el_inds, candidate_el_words)
        operation_list = [(en_ind, el_ind, new_en, new_el) for (el_ind, (new_en, new_el)) in operations]
        
        return operation_list

    '''
        Given list of strings 'en' and 'el', use 'op' to replace words
    '''
    def change_word(self, en, el, op):
        en_ind, el_ind, new_en, new_el = op
        el_result = el[:el_ind] + [new_el] + el[el_ind+1:]
        en_result = en[:en_ind] + [new_en] + en[en_ind+1:]

        return en_result, el_result

    def get_augment(self, el, en, augment):
        tokenized_el = el.split(" ")
        tokenized_en = en.split(" ")

        assert len(tokenized_en) == len(augment)

        # get all possible operations we could do
        all_operations = []
        for ind, candidates in enumerate(augment):
            operation_list = self.search_candidate(tokenized_el, tokenized_en, ind, candidates)

            if len(operation_list) > 0:
                all_operations.append(operation_list)

        en_results = []
        el_results = []

        if len(all_operations) == 0:
            return en_results, el_results
        
        print(all_operations)

        if self.substitute_multiple:
            for ops in product(*all_operations):
                new_en = tokenized_en
                new_el = tokenized_el
            
                for op in ops:
                    new_en, new_el = self.change_word(new_en, new_el, op)
                    
                en_results.append(" ".join(new_en))
                el_results.append(" ".join(new_el))
        else:
            for ops in all_operations:
                for op in ops:
                    en_result, el_result = self.change_word(tokenized_en, tokenized_el, op)
                    en_results.append(" ".join(en_result))
                    el_results.append(" ".join(el_result))

        return en_results, el_results

# get data
def get_data():
    with open(el_data_path, 'r', encoding='utf8') as f:
        el_data = [line.strip() for line in f]
        
    with open(en_data_path, 'r', encoding='utf8') as f:
        en_data = [line.strip() for line in f]

    return (el_data, en_data)

def get_augment_list():
    with open(augment_path, 'rb') as f:
        augment_list = pickle.load(f)

    return augment_list


# Get el_en dictionary
def get_dict():
    original_chars = """ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,;.!?:'"/|_@#$%^&*~`+-=<>()[]"""
    other_chars = """‘’“”–«·»"""
    finnish_lower = """abcdefghijklmnopqrstuvwxyzåäöšž"""
    finnish_upper = """ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖŠŽ"""

    all_chars = original_chars + other_chars + finnish_lower + finnish_upper
    all_chars_final = ''.join(sorted(list(set(list(all_chars)))))
    #print(all_chars_final)

    all_non_alphs = """,;.!?:'"/|_@#$%^&*~`+-=<>()[]‘’“”–«·»"""
    all_non_alphs_final = ''.join(sorted(list(set(list(all_non_alphs)))))
    #print(all_non_alphs_final)

    def is_valid(line):
        for char in line:
            if all_chars_final.find(char) == -1:
                return False
        return True

    enel_dict = {}
    with open(elen_dict_path, 'r', encoding='utf8') as f:
        for line in f:
            (el, en) = line.strip().split(";")
            el_set = enel_dict.get(en, set())
            if is_valid(el):
                el_set.add(el)
            enel_dict[en] = el_set

    return enel_dict



#el_data_path = './../data/new_testament/val.el'
#en_data_path = './../data/new_testament/val.en'
#augment_path = "./char_decoder_model/outputs/new_testament_val"

#el_data_path = './../data/misc/test.el'
#en_data_path = './../data/misc/test.en'
#augment_path = "./char_decoder_model/outputs/misc_augment"

#output_path = "./../data/new_testament_augmented/val"

#el_data_path = './../data/misc/test.el'
#en_data_path = './../data/misc/test.en'
#augment_path = "./char_decoder_model/outputs/misc_augment"

el_data_path = './../data/new_testament/train.el'
en_data_path = './../data/new_testament/train.en'
augment_path = "./char_decoder_model/new_testament_augment_filtered"
output_path = "./../data/new_testament_augment_filtered_single/train"

elen_dict_path = './../data/el_en_dict'

def main():
    (el_data, en_data) = get_data()
    enel_dict = get_dict()
    augment_list = get_augment_list()
    dist = nltk.edit_distance
    threshold = 0    
    substitute_multiple = True

    print("Using Edit distance with threshold {}".format(threshold))
    print("Using substitute_multiple value of {}".format(substitute_multiple))

    augmenter = Augment(enel_dict, dist, threshold, substitute_multiple)

    assert len(el_data) == len(en_data) == len(augment_list)

    en_results = []
    el_results = []

    cnt = 0

    for (el, en, augment) in zip(el_data, en_data, augment_list):
        cnt += 1
        if cnt % 10 == 0:
            print(cnt)
        en_tokens = en.split(" ")
        
        assert len(en_tokens) == len(augment)

        en_result, el_result = augmenter.get_augment(el, en, augment)

        en_results.extend(en_result)
        el_results.extend(el_result)
        
        with open(output_path + ".en", 'a+', encoding='utf8') as f:
            if len(en_result) > 0:
                for res in en_result:
                    f.write(res + "\n")
            else:
                f.write(en + "\n")

        with open(output_path + ".el", 'a+', encoding='utf8') as f:
            if len(el_result) > 0:
                for res in el_result:
                    f.write(res + "\n")
            else:
                f.write(el + "\n")
        
        if len(el_result) > 0:
            print(el_result)
            print(en_result)

    # save_file
    with open(output_path + "_augment.en", 'w', encoding='utf8') as f:
        for en in en_results:
            f.write(en + "\n")
            
    with open(output_path + "_full.en", 'w', encoding='utf8') as f:
        for en in en_data:
            f.write(en + "\n")
        for en in en_results:
            f.write(en + "\n")
            
    with open(output_path + "_augment.el", 'w', encoding='utf8') as f:
        for el in el_results:
            f.write(el + "\n")
            
    with open(output_path + "_full.el", 'w', encoding='utf8') as f:
        for el in el_data:
            f.write(el + "\n")
        for el in el_results:
            f.write(el + "\n") 

if __name__ == '__main__':
    main()         
